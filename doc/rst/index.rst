====
Axl 
====

.. image:: img/tensor.png 
	   :width: 30%

.. image:: img/teapot.png 
	   :width: 38.5%

.. image:: img/propeller.png 
	   :width: 26%

*Axl* is an algebraic geometric modeler that aims at providing
“algebraic modeling” tools for the manipulation and computation with
curves, surfaces or volumes described by semi-algebraic
representations. :doc:`These <data/index>` include parametric and implicit representations of geometric objects. Axl also provides algorithms to compute intersection points or curves, singularities of algebraic curves or surfaces,  certified topology of curves and surfaces, etc. A :doc:`plugin mechanism <structure/index>` allows to extend easily the data types and functions available in the platform.


.. |download| image:: img/download.png 
	   :width: 75px

.. raw:: html

   <a href="installation.html"><img src="_images/download.png"   width="50"> Download & Install </a>


	 
Documentation
=============

.. toctree::
   :maxdepth: 1

   what_is_axl
   getting_started/index
   data/index
   structure/index
   use_extend/axl_use 
   use_extend/index
   Using Axl with Julia <http://axl.inria.fr/doc/Axl.jl/>
   mypkg/new_package 
   installation 
   Source code <http://axl.inria.fr/annotated.html>
   Git server <https://gitlab.inria.fr/agm/axl/>

.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`

