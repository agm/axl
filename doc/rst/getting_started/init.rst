First step using Axl 
=====================

When openning Axl application, a main window corresponding to the view appears. It visualizes 
the objects of a scene and allow to interact with them.

Three types of inspectors are available by clicking on the Inspector button on the top:

.. image:: ../img/content_inspector1.png 
   :align: center 

- the object inspectors (Ctrl-i, Ctrl-1): it lists the objects of the scene and allows to select and edit them.
- the process inspectors (Ctrl-i, Ctrl-2): it displays the tags of the processes and allows to select them and (see `how to apply a process here <process.html>`_)
- the viewer inspector (Ctrl-i, Ctrl-3): it provides access to the viewer characteristics (see `how to use it here <view.html>`_)


The application can be used as a command to read files and visualize geometric objects. Here is an example
using the file `ellipsoids.axl <http://axl.inria.fr/axel_data/77>`_::

  axl ellipsoids.axl

Openning the object inspectors (Ctrl-i), we can select the corresponding objects of the scene and edit them
(change parameters, color, opacity, shader, ...):

.. image:: ../img/content_inspector_objects.png 
   :align: center

Here the first object is selected. Its color in the view becomes whiter. To edit it, double click on the corresponding line in the inspector or type "e".

The view can be changed using the mouse or pad moves. To center again the view on the objects of the scene, type "r".

To change the visualization mode to wireframe mode, type "w" in the viewer window. To use smooth mode, type "s".

To save a scene in the Axl format, use the "Save" button in the menu "File" or Ctrl-s. 

  
  
