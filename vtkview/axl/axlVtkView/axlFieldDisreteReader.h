/* axlFieldDiscreteReader.h ---
 *
 * Author: Anais Ducoffe
 * Copyright (C) 2013 - Anais Ducoffe, Inria.
 */

/* Commentary:
 *
 */

/* Change log:
 *
 */

#ifndef AXLFIELDDISRETEREADER_H
#define AXLFIELDDISRETEREADER_H

#include <axlCore/axlAbstractDataReader.h>
#include <axlCore/axlDataDynamic.h>
#include <axlCore/axlAbstractDataComposite.h>

#include "vtkViewPluginExport.h"

class dtkAbstractData;

class VTKVIEWPLUGIN_EXPORT axlFieldDiscreteReader : public axlAbstractDataReader
{
    Q_OBJECT

public :
    axlFieldDiscreteReader(void);
    ~axlFieldDiscreteReader(void);

public:
    QString identifier(void) const;
    QString description(void) const;
    QStringList handled(void) const;

    dtkAbstractData *dataByReader(axlAbstractDataReader *axl_reader, const QDomNode& node);

    static bool registered(void);

public:
    bool accept(const QDomNode& node);
    bool reject(const QDomNode& node);

    dtkAbstractData *read(const QDomNode& node);
};

AXLCORE_EXPORT dtkAbstractDataReader *createaxlFieldDiscreteReader(void);

#endif// AXLFIELDDISRETEREADER_H
