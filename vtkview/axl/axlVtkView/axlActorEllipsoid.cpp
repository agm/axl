/* axlActorEllipsoid.cpp ---
 *
 * Author: Meriadeg Perrinel
 * Copyright (C) 2008 - Meriadeg Perrinel, Inria.
 * Created: Fri Dec 17 10:59:21 2010 (+0100)
 * Version: $Id$
 * Last-Updated: Mon Dec 17 15:03:23 2012 (+0100)
 *           By: Julien Wintz
 *     Update #: 29
 */

/* Commentary:
 *
 */

/* Change log:
 *
 */
#include "axlActorEllipsoid.h"

#include "axlActorComposite.h"
#include "axlInteractorStyleSwitch.h"

#include <axlCore/axlEllipsoid.h>
#include <axlCore/axlPoint.h>

#include <QVTKOpenGLWidget.h>

#include <vtkActor.h>
#include <vtkAssemblyPath.h>
//#include <vtkCellPicker.h>
#include <vtkCommand.h>
#include <vtkGlyph3D.h>
#include <vtkInteractorStyleSwitch.h>
#include <vtkLight.h>
#include <vtkLightCollection.h>
#include <vtkObjectFactory.h>
#include <vtkPoints.h>
#include <vtkLineWidget.h>
#include <vtkPolyData.h>
#include <vtkPolyDataMapper.h>
#include <vtkProperty.h>
#include <vtkRenderer.h>
#include <vtkRendererCollection.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkRenderWindow.h>
#include <vtkSmartPointer.h>
#include <vtkParametricEllipsoid.h>
#include <vtkParametricFunctionSource.h>
#include <vtkTransform.h>


class axlActorEllipsoidObserver : public vtkCommand {
public:
    static axlActorEllipsoidObserver *New(void) {
        return new axlActorEllipsoidObserver;
    }

    virtual void Execute(vtkObject *caller, unsigned long event, void *) {
        vtkRenderWindowInteractor *interactor = observerDataAssembly->getInteractor();

        if (event == vtkCommand::InteractionEvent) {
            observerData_ellipsoidSource->Modified();
            observerData_ellipsoidSource->Update();
            interactor->Render();

            // we need to select which line widget we are moving then update the axlEllipsoid and then run onUpdateGeometry method
            if(!lineXWidget || !lineYWidget || !lineZWidget)
                return;

            //now we select which line widget the user is moving
//            if (axlPoint::distance(&lXCenter, observerData_ellipsoid->center())> 0.0001) {
            if (caller == lineXWidget){
                // we are moving lineXWidget
                axlPoint lXP1(lineXWidget->GetPoint1()[0], lineXWidget->GetPoint1()[1], lineXWidget->GetPoint1()[2]);
                axlPoint lXP2(lineXWidget->GetPoint2()[0], lineXWidget->GetPoint2()[1], lineXWidget->GetPoint2()[2]);
                axlPoint lXCenter = (lXP1 + lXP2) / 2.0;

                axlPoint semiX = (lXP2 - lXP1)/2;
                observerData_ellipsoid->modifyCenter(lXCenter.coordinates());
                observerData_ellipsoid->modifySemiX(semiX.coordinates());
                observerData_ellipsoid->calculateYZ();
            } //else if (axlPoint::distance(&lYCenter, observerData_ellipsoid->center())> 0.0001) {
            else if (caller == lineYWidget){
                // we move lineYWidget
                axlPoint lYP1(lineYWidget->GetPoint1()[0], lineYWidget->GetPoint1()[1], lineYWidget->GetPoint1()[2]);
                axlPoint lYP2(lineYWidget->GetPoint2()[0], lineYWidget->GetPoint2()[1], lineYWidget->GetPoint2()[2]);
                axlPoint lYCenter = (lYP1 + lYP2) / 2.0;

                axlPoint semiY = (lYP2 - lYP1)/2;
                observerData_ellipsoid->modifyCenter(lYCenter.coordinates());
                observerData_ellipsoid->modifySemiY(semiY.coordinates());
                observerData_ellipsoid->calculateXZ();
            } //else if(axlPoint::distance(&lZCenter, observerData_ellipsoid->center())> 0.0001) {
            else if (caller == lineZWidget){
                // we move lineZWidget
                axlPoint lZP1(lineZWidget->GetPoint1()[0], lineZWidget->GetPoint1()[1], lineZWidget->GetPoint1()[2]);
                axlPoint lZP2(lineZWidget->GetPoint2()[0], lineZWidget->GetPoint2()[1], lineZWidget->GetPoint2()[2]);
                axlPoint lZCenter = (lZP1 + lZP2) / 2.0;

                axlPoint semiZ = (lZP2 - lZP1)/2;
                observerData_ellipsoid->modifyCenter(lZCenter.coordinates());
                observerData_ellipsoid->modifySemiZ(semiZ.coordinates());
                observerData_ellipsoid->calculateXY();
            }
            else
                return;

            observerDataAssembly->onUpdateGeometry();

        }

        //        if (event == vtkCommand::MouseMoveEvent) {
        //            if (observerData_ellipsoid) {
        //                // we need the matrix transform of this actor

        //                vtkAssemblyPath *path;
        //                vtkRenderWindow *renderWindow = interactor->GetRenderWindow();
        //                vtkRendererCollection * rendererCollection = renderWindow->GetRenderers();
        //                vtkRenderer *render = rendererCollection->GetFirstRenderer();
        //                axlInteractorStyle = dynamic_cast<axlInteractorStyleSwitch *>(interactor->GetInteractorStyle());

        //                int X = observerDataAssembly->getInteractor()->GetEventPosition()[0];
        //                int Y = observerDataAssembly->getInteractor()->GetEventPosition()[1];

        //                if (!render || !render->IsInViewport(X, Y)) {
        //                    //qDebug()<<" No renderer or bad cursor coordonates";
        //                }

        //                axlEllipsoidPicker->Pick(X,Y,0.0,render);
        //                path = axlEllipsoidPicker->GetPath();

        //                if (path != NULL) {
        //                    //qDebug()<< "axlActorEllipsoidObserver::MouseMoveEvent";

        //                    double *position = observerDataAssembly->GetPosition();
        //                    for (int j = 0; j < 3; j++) {
        //                        //qDebug()<<"axlActorEllipsoidObserver :: Execute "<<position[j];
        //                        //observerData_ellipsoid->coordinates()[j] = position[j];
        //                    }
        //                    //WARNING We considered there the scale is uniforn in all direction;
        //                    // observerData_ellipsoid->emitDataChanged();
        //                }
        //            }
        //        }
    }

    axlInteractorStyleSwitch *axlInteractorStyle;
//    vtkCellPicker *axlEllipsoidPicker;
    axlEllipsoid *observerData_ellipsoid;
    axlActorEllipsoid *observerDataAssembly;
    vtkParametricFunctionSource *observerData_ellipsoidSource;
    vtkParametricEllipsoid *vtkEllipsoid;

    vtkLineWidget *lineXWidget = nullptr;
    vtkLineWidget *lineYWidget = nullptr;
    vtkLineWidget *lineZWidget = nullptr;
};

// /////////////////////////////////////////////////////////////////
// axlActorEllipsoidPrivate
// /////////////////////////////////////////////////////////////////

class axlActorEllipsoidPrivate
{
public:
    axlEllipsoid *ellipsoid;
    vtkParametricFunctionSource *ellipsoidSource;
    vtkParametricEllipsoid *vtkEllipsoid;
    vtkLineWidget *lineXWidget;
    vtkLineWidget *lineYWidget;
    vtkLineWidget *lineZWidget;
    axlActorEllipsoidObserver *ellipsoidObserver;
    QVTKOpenGLWidget *widget;
//    vtkCellPicker *axlEllipsoidPicker ;

};

#if (VTK_MAJOR_VERSION <= 5)
vtkCxxRevisionMacro(axlActorEllipsoid, "$Revision: 0.0.1 $");
#endif

vtkStandardNewMacro(axlActorEllipsoid);

dtkAbstractData *axlActorEllipsoid::data(void) {
    return d->ellipsoid;
}

vtkParametricEllipsoid *axlActorEllipsoid::ellipsoid(void) {
    return d->vtkEllipsoid;
}

void axlActorEllipsoid::setQVTKWidget(QVTKOpenGLWidget *widget) {
    d->widget = widget;
}


void axlActorEllipsoid::setData(dtkAbstractData *ellipsoid1) {

    axlEllipsoid *ellipsoid = dynamic_cast<axlEllipsoid *>(ellipsoid1);

    if (ellipsoid) {
        d->ellipsoid = ellipsoid;

        double radiusX = d->ellipsoid->semiX()->norm();
        double radiusY = d->ellipsoid->semiY()->norm();
        double radiusZ = d->ellipsoid->semiZ()->norm();

        this->setMapper(vtkSmartPointer<vtkPolyDataMapper>::New());
        this->setActor(vtkSmartPointer<vtkActor>::New());

        d->ellipsoidSource = vtkParametricFunctionSource::New();

        d->vtkEllipsoid = vtkParametricEllipsoid::New();
        d->vtkEllipsoid->SetXRadius(radiusX);
        d->vtkEllipsoid->SetYRadius(radiusY);
        d->vtkEllipsoid->SetZRadius(radiusZ);
        d->ellipsoidSource->SetParametricFunction(d->vtkEllipsoid);

        // connection of data to actor
#if (VTK_MAJOR_VERSION <= 5)
        this->getMapper()->SetInput(d->ellipsoidSource->GetOutput());
#else
        this->getMapper()->SetInputData(d->ellipsoidSource->GetOutput());
#endif
        this->getActor()->SetMapper(this->getMapper());

        this->AddPart(this->getActor());

        if (!d->ellipsoidObserver) {
//            d->axlEllipsoidPicker = vtkCellPicker::New();
            d->ellipsoidObserver = axlActorEllipsoidObserver::New() ;
            this->getInteractor()->AddObserver(vtkCommand::InteractionEvent, d->ellipsoidObserver);
            this->getInteractor()->AddObserver(vtkCommand::MouseMoveEvent, d->ellipsoidObserver);

            d->ellipsoidObserver->observerDataAssembly = this;
//            d->ellipsoidObserver->axlEllipsoidPicker = d->axlEllipsoidPicker;
            d->ellipsoidObserver->observerData_ellipsoidSource = d->ellipsoidSource;
            d->ellipsoidObserver->observerData_ellipsoid = d->ellipsoid;
            d->ellipsoidObserver->axlInteractorStyle = dynamic_cast<axlInteractorStyleSwitch *> (this->getInteractor()->GetInteractorStyle());
        }

        this->onUpdateGeometry();
        QColor color = d->ellipsoid->color();
        this->getActor()->GetProperty()->SetColor(color.redF(), color.greenF(), color.blueF());

        QString shader = d->ellipsoid->shader();
        if(!shader.isEmpty())
            this->setShader(shader);

        this->setOpacity(d->ellipsoid->opacity());

        connect(d->ellipsoid,SIGNAL(modifiedGeometry()),this,SLOT(onUpdateGeometry()));
        connect(d->ellipsoid,SIGNAL(modifiedProperty()),this,SLOT(onUpdateProperty()));

    } else
        qDebug()<< "no axlEllipsoid available";
}

void axlActorEllipsoid::setDisplay(bool display) {
    axlActor::setDisplay(display);

    if (display && d->lineXWidget) {
        this->showEllipsoidWidget(true);
    }

    if (!display && d->lineXWidget) {
        this->showEllipsoidWidget(false);
    }
}

void axlActorEllipsoid::showEllipsoidWidget(bool show) {
    if (!d->lineXWidget || !d->lineYWidget || !d->lineZWidget) {
        qDebug() << "No tet actor computed for this axlActorCylinder.";
        return;
    }

    if (show) {
        d->lineXWidget->SetEnabled(1);
        d->lineYWidget->SetEnabled(1);
        d->lineZWidget->SetEnabled(1);
    }

    if (!show) {
        d->lineXWidget->SetEnabled(0);
        d->lineYWidget->SetEnabled(0);
        d->lineZWidget->SetEnabled(0);

    }
}

void axlActorEllipsoid::setEllipsoidWidget(bool ellipsoidWidget) {
    if (ellipsoidWidget) {
        if (!d->lineXWidget || !d->lineYWidget || !d->lineZWidget) {
            //widget drawing

            d->lineXWidget = vtkLineWidget::New();
            d->lineXWidget->SetInteractor(this->getInteractor());
            d->lineXWidget->SetProp3D(this->getActor());
            d->lineXWidget->PlaceWidget();

            d->lineYWidget = vtkLineWidget::New();
            d->lineYWidget->SetInteractor(this->getInteractor());
            d->lineYWidget->SetProp3D(this->getActor());
            d->lineYWidget->PlaceWidget();

            d->lineZWidget = vtkLineWidget::New();
            d->lineZWidget->SetInteractor(this->getInteractor());
            d->lineZWidget->SetProp3D(this->getActor());
            d->lineZWidget->PlaceWidget();

            axlPoint center = *(d->ellipsoid->center());

//            axlPoint orientationX((*this->getActor()->GetMatrix())[0][0], (*this->getActor()->GetMatrix())[1][0], (*this->getActor()->GetMatrix())[2][0]);
//            axlPoint orientationY((*this->getActor()->GetMatrix())[0][1], (*this->getActor()->GetMatrix())[1][1], (*this->getActor()->GetMatrix())[2][1]) ;
//            axlPoint orientationZ((*this->getActor()->GetMatrix())[0][2], (*this->getActor()->GetMatrix())[1][2], (*this->getActor()->GetMatrix())[2][2]) ;

            axlPoint orientationX(this->getActor()->GetMatrix()->GetElement(0,0), this->getActor()->GetMatrix()->GetElement(1,0), this->getActor()->GetMatrix()->GetElement(2,0));
            axlPoint orientationY(this->getActor()->GetMatrix()->GetElement(0,1), this->getActor()->GetMatrix()->GetElement(1,1), this->getActor()->GetMatrix()->GetElement(2,1)) ;
            axlPoint orientationZ(this->getActor()->GetMatrix()->GetElement(0,2), this->getActor()->GetMatrix()->GetElement(1,2), this->getActor()->GetMatrix()->GetElement(2,2)) ;

            orientationX.normalize();
            orientationY.normalize();
            orientationZ.normalize();

            d->lineXWidget->SetPoint1((center - (orientationX * d->ellipsoid->semiX()->norm())).coordinates());
            d->lineXWidget->SetPoint2((center + (orientationX * d->ellipsoid->semiX()->norm())).coordinates());

            d->lineYWidget->SetPoint1((center - (orientationY * d->ellipsoid->semiY()->norm())).coordinates());
            d->lineYWidget->SetPoint2((center + (orientationY * d->ellipsoid->semiY()->norm())).coordinates());

            d->lineZWidget->SetPoint1((center - (orientationZ * d->ellipsoid->semiZ()->norm())).coordinates());
            d->lineZWidget->SetPoint2((center + (orientationZ * d->ellipsoid->semiZ()->norm())).coordinates());
        }

        if (d->ellipsoidObserver) {
            d->lineXWidget->AddObserver(vtkCommand::InteractionEvent, d->ellipsoidObserver);
            d->lineYWidget->AddObserver(vtkCommand::InteractionEvent, d->ellipsoidObserver);
            d->lineZWidget->AddObserver(vtkCommand::InteractionEvent, d->ellipsoidObserver);


            d->ellipsoidObserver->lineXWidget = d->lineXWidget;
            d->ellipsoidObserver->lineYWidget = d->lineYWidget;
            d->ellipsoidObserver->lineZWidget = d->lineZWidget;
        }

        // there is always the controlPoints there
        d->lineXWidget->SetEnabled(true);
        d->lineYWidget->SetEnabled(true);
        d->lineZWidget->SetEnabled(true);


    } else { // (!pointWidget)
        if (this->getActor()) {
            if (d->lineXWidget) {
                d->lineXWidget->RemoveAllObservers();
                d->lineXWidget->SetEnabled(false);
                d->lineXWidget->Delete(); // warning not sure
                d->lineXWidget = NULL;
            }

            if (d->lineYWidget) {
                d->lineYWidget->RemoveAllObservers();
                d->lineYWidget->SetEnabled(false);
                d->lineYWidget->Delete(); // warning not sure
                d->lineYWidget = NULL;
            }

            if (d->lineZWidget) {
                d->lineZWidget->RemoveAllObservers();
                d->lineZWidget->SetEnabled(false);
                d->lineZWidget->Delete(); // warning not sure
                d->lineZWidget = NULL;
            }
        }
    }
    if (d->ellipsoidObserver){
        d->ellipsoidObserver->lineXWidget = d->lineXWidget;
        d->ellipsoidObserver->lineYWidget = d->lineYWidget;
        d->ellipsoidObserver->lineZWidget = d->lineZWidget;
    }
}

bool axlActorEllipsoid::isShowEllipsoidWidget(void) {
    if (!d->lineXWidget || !d->lineYWidget || !d->lineZWidget) {
        qDebug() << "No tet actor computed for this axlActorBSpline.";
        return false;
    }

    return d->lineXWidget->GetEnabled() && d->lineYWidget->GetEnabled() && d->lineZWidget->GetEnabled();
}


void axlActorEllipsoid::setMode(int state) {
    this->onModeChanged(state);
    emit stateChanged(this->data(), state);
}

void axlActorEllipsoid::onModeChanged(int state) {
    if (state == 0) {
        this->setState(axlActor::passive);
        this->getActor()->SetPickable(1);

        if (axlAbstractData *data = dynamic_cast<axlAbstractData *>(this->data())) {
            QColor color = data->color();
            this->getActor()->GetProperty()->SetColor(color.redF(), color.greenF(), color.blueF());
        }
        this->setEllipsoidWidget(false);

    } else if (state == 1) {
        this->setState(axlActor::selection);
        this->getActor()->SetPickable(1);
        vtkProperty *prop = this->getActor()->GetProperty();

        if(axlAbstractData *data = dynamic_cast<axlAbstractData *>(this->data())) {
            QColor color = data->color();
            qreal *h = new qreal(0.0);
            qreal *s = new qreal(0.0);
            qreal *l = new qreal(0.0);
            color.getHslF(h, s, l);
            color.setHslF(*h, *s, *l + (1.0 - *l) / 2.0);
            prop->SetColor(color.redF(), color.greenF(), color.blueF());
            delete l;
            delete s;
            delete h;
        }
        this->setEllipsoidWidget(false);
    } else if (state == 2) {
        this->setState(axlActor::edition);
        this->getActor()->SetPickable(0);
        vtkProperty *prop = this->getActor()->GetProperty();
        if(axlAbstractData *data = dynamic_cast<axlAbstractData *>(this->data()))
        {
            QColor color = data->color();
            qreal *h = new qreal(0.0);
            qreal *s = new qreal(0.0);
            qreal *l = new qreal(0.0);
            color.getHslF(h, s, l);
            color.setHslF(*h, *s, *l + (1.0 - *l)/2.0);
            prop->SetColor(color.redF(), color.greenF(), color.blueF());
            delete l;
            delete s;
            delete h;
        }
        this->setEllipsoidWidget(true);
    }

    this->Modified();
}


void axlActorEllipsoid::onRemoved() {
    
    // if on edit mode, change to selection mode (for stability)
    if (this->getState() == 2)
        setMode(1);
    
    //remove ellipsoid specificity
    if (d->ellipsoidObserver) {
        this->getInteractor()->RemoveObservers(vtkCommand::InteractionEvent, d->ellipsoidObserver);
        this->getInteractor()->RemoveObservers(vtkCommand::MouseMoveEvent, d->ellipsoidObserver);
        d->ellipsoidObserver->observerDataAssembly = NULL;
//        d->ellipsoidObserver->axlEllipsoidPicker = NULL;
        d->ellipsoidObserver->observerData_ellipsoidSource = NULL;
        d->ellipsoidObserver->observerData_ellipsoid = NULL;
        d->ellipsoidObserver->axlInteractorStyle = NULL;
        d->ellipsoidObserver->Delete();
        d->ellipsoidObserver = NULL;

//        d->axlEllipsoidPicker->Delete();
//        d->axlEllipsoidPicker = NULL;
    }
    if (d->ellipsoidSource) {
        d->ellipsoidSource->Delete();
        d->ellipsoidSource = NULL;
    }
    if (d->widget) {
        d->widget = NULL;
    }
    if (d->ellipsoid) {
        d->ellipsoid = NULL;
    }
    if (d->lineXWidget && d->lineYWidget && d->lineZWidget) {
        this->setEllipsoidWidget(false);
        d->lineXWidget = NULL;
        d->lineYWidget = NULL;
        d->lineZWidget = NULL;
    }

    //remove actor specificity
    this->RemoveAllObservers();
    this->RemovePart(this->getActor());
    this->getActor()->RemoveAllObservers();

    if(axlActorComposite *actorComposite = dynamic_cast<axlActorComposite *>(this->parent()))
        actorComposite->removeActorReference(this);
}

void axlActorEllipsoid::onUpdateGeometry(void) {
    d->ellipsoidSource->Modified();
    d->ellipsoidSource->Update();

    d->vtkEllipsoid->SetXRadius(d->ellipsoid->semiX()->norm());
    d->vtkEllipsoid->SetYRadius(d->ellipsoid->semiY()->norm());
    d->vtkEllipsoid->SetZRadius(d->ellipsoid->semiZ()->norm());

    vtkMatrix4x4 *actorMatrix = vtkMatrix4x4::New();
//    (*actorMatrix)[0][0] = d->ellipsoid->semiX()->x();
    actorMatrix->SetElement(0, 0, d->ellipsoid->semiX()->x());
//    (*actorMatrix)[0][1] = d->ellipsoid->semiY()->x();
    actorMatrix->SetElement(0, 1, d->ellipsoid->semiY()->x());
//    (*actorMatrix)[0][2] = d->ellipsoid->semiZ()->x();
    actorMatrix->SetElement(0, 2, d->ellipsoid->semiZ()->x());

//    (*actorMatrix)[1][0] = d->ellipsoid->semiX()->y();
    actorMatrix->SetElement(1, 0, d->ellipsoid->semiX()->y());
//    (*actorMatrix)[1][1] = d->ellipsoid->semiY()->y();
    actorMatrix->SetElement(1, 1, d->ellipsoid->semiY()->y());
//    (*actorMatrix)[1][2] = d->ellipsoid->semiZ()->y();
    actorMatrix->SetElement(1, 2, d->ellipsoid->semiZ()->y());

//    (*actorMatrix)[2][0] = d->ellipsoid->semiX()->z();
    actorMatrix->SetElement(2, 0, d->ellipsoid->semiX()->z());
//    (*actorMatrix)[2][1] = d->ellipsoid->semiY()->z();
    actorMatrix->SetElement(2, 1, d->ellipsoid->semiY()->z());
//    (*actorMatrix)[2][2] = d->ellipsoid->semiZ()->z();
    actorMatrix->SetElement(2, 2, d->ellipsoid->semiZ()->z());
    double orient[3];
    vtkTransform::GetOrientation(orient, actorMatrix);
    this->getActor()->SetOrientation(orient);

    this->getActor()->SetPosition(d->ellipsoid->center()->coordinates());

    if(d->lineXWidget && d->lineYWidget && d->lineZWidget) {
        axlPoint center = *(d->ellipsoid->center());

//        axlPoint orientationX((*this->getActor()->GetMatrix())[0][0], (*this->getActor()->GetMatrix())[1][0], (*this->getActor()->GetMatrix())[2][0]);
//        axlPoint orientationY((*this->getActor()->GetMatrix())[0][1], (*this->getActor()->GetMatrix())[1][1], (*this->getActor()->GetMatrix())[2][1]) ;
//        axlPoint orientationZ((*this->getActor()->GetMatrix())[0][2], (*this->getActor()->GetMatrix())[1][2], (*this->getActor()->GetMatrix())[2][2]) ;

        axlPoint orientationX(this->getActor()->GetMatrix()->GetElement(0,0), this->getActor()->GetMatrix()->GetElement(1,0), this->getActor()->GetMatrix()->GetElement(2,0));
        axlPoint orientationY(this->getActor()->GetMatrix()->GetElement(0,1), this->getActor()->GetMatrix()->GetElement(1,1), this->getActor()->GetMatrix()->GetElement(2,1)) ;
        axlPoint orientationZ(this->getActor()->GetMatrix()->GetElement(0,2), this->getActor()->GetMatrix()->GetElement(1,2), this->getActor()->GetMatrix()->GetElement(2,2)) ;

        orientationX.normalize();
        orientationY.normalize();
        orientationZ.normalize();

        d->lineXWidget->SetPoint1((center - (orientationX * (d->ellipsoid->semiX()->norm()))).coordinates());
        d->lineXWidget->SetPoint2((center + (orientationX * (d->ellipsoid->semiX()->norm()))).coordinates());

        d->lineYWidget->SetPoint1((center - (orientationY * (d->ellipsoid->semiY()->norm()))).coordinates());
        d->lineYWidget->SetPoint2((center + (orientationY * (d->ellipsoid->semiY()->norm()))).coordinates());

        d->lineZWidget->SetPoint1((center - (orientationZ * (d->ellipsoid->semiZ()->norm()))).coordinates());
        d->lineZWidget->SetPoint2((center + (orientationZ * (d->ellipsoid->semiZ()->norm()))).coordinates());
    }

    if(!d->ellipsoid->fields().isEmpty())
        d->ellipsoid->touchField();

    if(d->ellipsoid->updateView())
        emit updated();
}

axlActorEllipsoid::axlActorEllipsoid(void) : axlActor(), d(new axlActorEllipsoidPrivate) {
    d->ellipsoid = NULL;
    d->lineXWidget = NULL;
    d->lineYWidget = NULL;
    d->lineZWidget = NULL;
//    d->axlEllipsoidPicker = NULL;
    d->ellipsoidObserver = NULL;
    d->ellipsoidSource =NULL;
    d->widget = NULL;
}

axlActorEllipsoid::~axlActorEllipsoid(void) {
    delete d;
    d = NULL;
}

axlAbstractActor *createAxlActorEllipsoid(void){

    return axlActorEllipsoid::New();
}
