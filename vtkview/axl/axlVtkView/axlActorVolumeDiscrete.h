/* axlActorVolumeDiscrete.h ---
 *
 * Author: Anais Ducoffe
 * Copyright (C) 2013 - Anais Ducoffe, Inria.
 */

/* Commentary:
 *
 */

/* Change log:
 *
 */

#ifndef AXLACTORVOLUMEDISCRETE_H
#define AXLACTORVOLUMEDISCRETE_H

#include "axlVtkViewPluginExport.h"
#include "axlVolumeDiscrete.h"
#include "axlActor.h"



class axlActorVolumeDiscretePrivate;

class AXLVTKVIEWPLUGIN_EXPORT axlActorVolumeDiscrete : public axlActor
{
public:
#if (VTK_MAJOR_VERSION <= 5)
    vtkTypeRevisionMacro(axlActorVolumeDiscrete, vtkAssembly);
#endif
    //vtkTypeMacro(axlActorVolumeDiscrete, vtkAssembly);

    static axlActorVolumeDiscrete *New(void);

public:
    void setData(dtkAbstractData *volume);

    dtkAbstractData *data(void);

public slots:
    void touch(void);
    void update(void);

public :
    void setDisplay(bool display);
    void setMode(int state);

public slots:
    void onModeChanged(int state);
    void onRemoved();
    void onUpdateGeometry();

public slots:
    void outlineNone(void);
    void outlineCorners(void);
    void outlineBox(void);
    void outlineContour(void);

public:
    void *colorTransferFunction(void);
    void *opacityTransferFunction(void);
    void *mapper(void);
    void *vol(void);
    void *volumeProperty(void);
    void *image(void);

protected:
    axlActorVolumeDiscrete(void);
    ~axlActorVolumeDiscrete(void);

private:
    axlActorVolumeDiscrete(const axlActorVolumeDiscrete&); // Not implemented.
    void operator = (const axlActorVolumeDiscrete&); // Not implemented.

private:
    axlActorVolumeDiscretePrivate *d;
};

#endif // AXLACTORVOLUMEDISCRETE_H
