======================================================================
How to define arbitrary networks of control points for spline surfaces
======================================================================

*Anais Ducoffe*

Axl enables to interact with the control points network of your spline surfaces while displaying it in the Axl view. To get this functionnality, all you need is to make your data inherit from ``axlAbstractSurfaceBSpline``.

By default this control points network will be defined as a grid. Only the direct control points neighbors are connected as shown in the first picture.

.. image:: ../img/content_controlpointsnetworkdefaultexample.jpg
   :align: center 

To change this network configuration, new functions were added in ``axlAbstractSurfaceBSpline``.

- here is the most important function which defines a connection with the ith and the jth coefficient. This function must be called in your spline surface creator process::

      virtual void defineControlPointConnection(int i, int j);

- you can check that connections were defined by using ``connectionsDefined``. This function returns true if connections were made using ``defineControlPointConnection``, by default returns false::

      virtual bool connectionsAreDefined(void);
			 
- The connections are stored in a ``QHash<int, QList<int> >``. The key represents a coefficient index. The value represents a list of coefficient indices that were connected to the keyth coefficient. If one connection between two coefficients, for instance the ith and jth coefficient, is defined then it is stored only once. Let say, if i < j, then the connection is stored in the QList value reachable with the i key. The following function returns the value for the i key::                          

     virtual QList<int> getControlPointConnection(int i);

This second picture illustrates the result with a control points network which connections were made using the ``defineControlPointConnections`` function.

.. image:: ../img/content_mspline.png
	   :align: center 
