/* axlInspectorObjectVolumeDiscrete.cpp ---
 *
 * Author: Anais Ducoffe
 * Copyright (C) 2013 - Anais Ducoffe, Inria.

 */

/* Commentary:
 *
 */

/* Change log:
 *
 */

#include "axlInspectorObjectVolumeDiscrete.h"
#include "axlVolumeDiscrete.h"

#include <axlCore/axlAbstractView.h>
#include <axlCore/axlAbstractVolumeDiscrete.h>

#include <dtkCoreSupport/dtkAbstractData.h>

class axlInspectorObjectVolumeDiscretePrivate
{
public:
    axlVolumeDiscrete * volume;

    //    QLabel *labelDimension_x;
    //    QLabel *labelDimension_y;
    //    QLabel *labelDimension_z;
    //    QLabel *equation;
};

axlInspectorObjectVolumeDiscrete::axlInspectorObjectVolumeDiscrete(QWidget *parent) : axlInspectorObjectInterface(parent), d(new axlInspectorObjectVolumeDiscretePrivate)
{
    d->volume = NULL;

    //    d->labelDimension_x = new QLabel(this);
    //    QHBoxLayout *layoutDimension_x = new QHBoxLayout;
    //    layoutDimension_x->addWidget(new QLabel("X-Dimension",this));
    //    layoutDimension_x->addWidget(d->labelDimension_x);

    //    d->labelDimension_y = new QLabel(this);
    //    QHBoxLayout *layoutDimension_y = new QHBoxLayout;
    //    layoutDimension_y->addWidget(new QLabel("Y-Dimension",this));
    //    layoutDimension_y->addWidget(d->labelDimension_y);

    //    d->labelDimension_z = new QLabel(this);
    //    QHBoxLayout *layoutDimension_z = new QHBoxLayout;
    //    layoutDimension_z->addWidget(new QLabel("Z-Dimension",this));
    //    layoutDimension_z->addWidget(d->labelDimension_z);

    //    d->equation = new QLabel(this);
    //    QHBoxLayout *layoutEquation = new QHBoxLayout;
    //    layoutEquation->addWidget(new QLabel("Equation",this));
    //    layoutEquation->addWidget(d->equation);

    QVBoxLayout *layout = new QVBoxLayout(this);
    //layout->setContentsMargins(0, 0, 0, 0);
    layout->addWidget(new QLabel("axlInspectorObjectVolumeDiscrete", this));
    //    layout->addLayout(layoutDimension_x);
    //    layout->addLayout(layoutDimension_y);
    //    layout->addLayout(layoutDimension_z);
    //    layout->addLayout(layoutEquation);
}

axlInspectorObjectVolumeDiscrete::~axlInspectorObjectVolumeDiscrete(void)
{
    delete d;
}

void axlInspectorObjectVolumeDiscrete::setData(dtkAbstractData *data)
{
    if(axlVolumeDiscrete *volume = dynamic_cast<axlVolumeDiscrete *>(data)){
        d->volume = volume;

        //d->equation->setText(d->volume->equation());
    }
}


bool axlInspectorObjectVolumeDiscrete::registered(void)
{
    //qDebug()<<"goSurfaceBSplineDialog::registered()"<<axlInspectorObjectFactory::instance()<<m_objectFactorySingleton;
    return axlInspectorObjectFactory::instance()->registerInspectorObject("axlVolumeDiscrete", createaxlVolumeDiscreteDialog);
}

axlInspectorObjectInterface *createaxlVolumeDiscreteDialog(void)
{
    return new axlInspectorObjectVolumeDiscrete;
}
