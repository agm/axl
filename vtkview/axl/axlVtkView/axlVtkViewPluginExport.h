
#ifndef AXLVTKVIEWPLUGIN_EXPORT_H
#define AXLVTKVIEWPLUGIN_EXPORT_H

#ifdef AXLVTKVIEWPLUGIN_STATIC_DEFINE
#  define AXLVTKVIEWPLUGIN_EXPORT
#  define AXLVTKVIEWPLUGIN_NO_EXPORT
#else
#  ifndef AXLVTKVIEWPLUGIN_EXPORT
#    ifdef AXLVTKVIEWPLUGIN_EXPORTS
        /* We are building this library */
#      define AXLVTKVIEWPLUGIN_EXPORT
#    else
        /* We are using this library */
#      define AXLVTKVIEWPLUGIN_EXPORT
#    endif
#  endif

#  ifndef AXLVTKVIEWPLUGIN_NO_EXPORT
#    define AXLVTKVIEWPLUGIN_NO_EXPORT
#  endif
#endif

#ifndef AXLVTKVIEWPLUGIN_DEPRECATED
#  define AXLVTKVIEWPLUGIN_DEPRECATED __attribute__ ((__deprecated__))
#  define AXLVTKVIEWPLUGIN_DEPRECATED_EXPORT AXLVTKVIEWPLUGIN_EXPORT __attribute__ ((__deprecated__))
#  define AXLVTKVIEWPLUGIN_DEPRECATED_NO_EXPORT AXLVTKVIEWPLUGIN_NO_EXPORT __attribute__ ((__deprecated__))
#endif

#define DEFINE_NO_DEPRECATED 0
#if DEFINE_NO_DEPRECATED
# define AXLVTKVIEWPLUGIN_NO_DEPRECATED
#endif

#endif
