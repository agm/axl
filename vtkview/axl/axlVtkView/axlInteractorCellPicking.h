/* axlInteractorCellPicking.h ---
 *
 * Author: Anais Ducoffe
 * Copyright (C) 2014 - Anais Ducoffe, Inria.
 */

/* Commentary:
 *
 */

/* Change log:
 *
 */

#ifndef axlInteractorCellPicking_H
#define axlInteractorCellPicking_H

#include <vtkInteractorStyleTrackballCamera.h>
#include <vtkPolyData.h>
#include <vtkSmartPointer.h>
#include <QtCore/QObject>
#include "axlVtkViewPluginExport.h"

class axlInteractorCellPickingPrivate;

class AXLVTKVIEWPLUGIN_EXPORT axlInteractorCellPicking :public QObject, public vtkInteractorStyleTrackballCamera
{
    Q_OBJECT

public:
    static axlInteractorCellPicking *New();
    vtkTypeMacro(axlInteractorCellPicking,vtkInteractorStyleTrackballCamera);

    void OnLeftButtonDown();

    void setData(vtkSmartPointer<vtkPolyData> data);

protected:
    axlInteractorCellPicking(QObject *parent = 0);
   ~axlInteractorCellPicking(void);

private:
    axlInteractorCellPickingPrivate *d;
};

#endif // axlInteractorCellPicking_H
