/* axlActorFieldParametric.cpp ---
 *
 * Author: Anais Ducoffe
 * Copyright (C) 20013- Anais Ducoffe, Inria.
 */

/* Commentary:
 *
 */

/* Change log:
 *
 */

#include <axlCore/axlAbstractFieldParametric.h>
#include "axlActorFieldParametric.h"
#include "axlActor.h"
#include "axlActorDataDynamic.h"
#include <axlCore/axlFieldDiscrete.h>
#include <axlCore/axlFieldParametricCurve.h>
#include <axlCore/axlFieldParametricSurface.h>
#include <axlCore/axlFieldParametricVolume.h>
#include <axlCore/axlAbstractFieldParametricCurve.h>
#include <axlCore/axlAbstractFieldParametricSurface.h>
#include <axlCore/axlAbstractFieldParametricVolume.h>
#include <axlCore/axlFieldParametricNormalVector.h>
#include <axlCore/axlMesh.h>

#include <axlCore/axlAbstractActorField.h>
#include <axlCore/axlAbstractField.h>

#include <dtkCoreSupport/dtkGlobal.h>

#include <vtkActor.h>
#include <vtkArrowSource.h>
#include <vtkCommand.h>
#include <vtkContourFilter.h>
#include <vtkDataSetMapper.h>
#include <vtkCellData.h>
#include <vtkGlyph3D.h>
#include <vtkHedgeHog.h>
#include <vtkObjectFactory.h>
#include <vtkPointData.h>
#include <vtkPolyDataMapper.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkSmartPointer.h>
#include <vtkSphere.h>
#include <vtkSphereRepresentation.h>
#include <vtkSphereSource.h>
#include <vtkSphereWidget2.h>
#include <vtkStreamTracer.h>
#include <vtkTubeFilter.h>
#include <vtkPolyData.h>
#include <vtkUnstructuredGrid.h>

#include <vtkDataArray.h>
#include <vtkFloatArray.h>
#include <vtkIntArray.h>
#include <vtkDoubleArray.h>

#include <vtkExtractUnstructuredGrid.h>

// /////////////////////////////////////////////////////////////////
// axlActorFieldParametricStreamObserver
// /////////////////////////////////////////////////////////////////

class axlActorFieldParametricStreamObserver : public vtkCommand
{
public:
    static axlActorFieldParametricStreamObserver *New(void)
    {
        return new axlActorFieldParametricStreamObserver;
    }

    virtual void Execute(vtkObject *caller, unsigned long event, void *)
    {
//        if(event == vtkCommand::InteractionEvent) {

//            vector_stream_widget_representation->GetPolyData(vector_stream_widget_data);
//#if (VTK_MAJOR_VERSION <= 5)
//            vector_stream_tracer->SetSource(vector_stream_widget_data);
//#else
//            vector_stream_tracer->SetSourceData(vector_stream_widget_data);
//#endif
//            vector_stream_tracer->Update();
//        }
    }

    vtkSmartPointer<vtkSphereRepresentation> vector_stream_widget_representation;
    vtkSmartPointer<vtkPolyData> vector_stream_widget_data;
    vtkSmartPointer<vtkStreamTracer> vector_stream_tracer;
};

// /////////////////////////////////////////////////////////////////
// axlActorFieldParametricPrivate
// /////////////////////////////////////////////////////////////////

class axlActorFieldParametricPrivate
{
public:
    axlAbstractFieldParametric *field;
    axlActor *mesh;
    vtkDataArray *array;

public:
    bool scalar_display_as_iso;
    int scalar_iso_count;
    double scalar_iso_range[2];
    vtkSmartPointer<vtkTubeFilter> scalar_iso_tube_filter;
    double isoRadius;
    vtkSmartPointer<vtkContourFilter> scalar_iso;
    vtkSmartPointer<vtkPolyDataMapper> scalar_iso_mapper;
    vtkSmartPointer<vtkActor> scalar_iso_actor;

    vtkSmartPointer<vtkPolyDataMapper> scalar_iso_color_mapper;
    vtkSmartPointer<vtkActor> scalar_iso_color_actor;

    vtkSmartPointer<vtkScalarBarActor> scalar_bar_actor;

    double scalar_color_range[2];
    vtkSmartPointer<vtkDataSetMapper> scalar_color_mapper;
    vtkSmartPointer<vtkActor> scalar_color_actor;

    vtkSmartPointer<vtkHedgeHog> vector_hedgehog;
    vtkSmartPointer<vtkPolyDataMapper> vector_hedgehog_mapper;
    vtkSmartPointer<vtkActor> vector_hedgehog_actor;

    vtkSmartPointer<vtkArrowSource> vector_glyph_source;
    vtkSmartPointer<vtkGlyph3D> vector_glyph;
    vtkSmartPointer<vtkPolyDataMapper> vector_glyph_mapper;
    vtkSmartPointer<vtkActor> vector_glyph_actor;

    vtkSmartPointer<vtkSphereWidget2> vector_stream_widget;
    vtkSmartPointer<vtkSphereRepresentation> vector_stream_widget_representation;
    vtkSmartPointer<vtkPolyData> vector_stream_widget_data;
    vtkSmartPointer<vtkStreamTracer> vector_stream_tracer;
    vtkSmartPointer<vtkTubeFilter> vector_stream_filter;
    vtkSmartPointer<vtkPolyDataMapper> vector_stream_mapper;
    vtkSmartPointer<vtkActor> vector_stream_actor;
    vtkSmartPointer<axlActorFieldParametricStreamObserver> vector_stream_observer;

    vtkSmartPointer<vtkRenderWindowInteractor> interactor;
};

// /////////////////////////////////////////////////////////////////
// axlActorFieldParametric
// /////////////////////////////////////////////////////////////////

#if (VTK_MAJOR_VERSION <= 5)
vtkCxxRevisionMacro(axlActorFieldParametric, "$Revision: 0.0.1 $");
#endif

vtkStandardNewMacro(axlActorFieldParametric);

dtkAbstractData *axlActorFieldParametric::data(void)
{
    return NULL;
}

axlAbstractField *axlActorFieldParametric::field(void)
{
    if(d->field){
        return d->field;
    }
    return NULL;
}

axlAbstractField *axlActorFieldParametric::magnitude(void)
{
    if(d->mesh->fields().contains(QString("%1 magnitude").arg(d->field->objectName())))
        return NULL;

    QString field_magnitude_name = QString("%1 magnitude").arg(d->field->objectName());



    double size = d->field->size();
    // TO DO
    // double *tuple;
    axlFieldDiscrete *field_magnitude = new axlFieldDiscrete(field_magnitude_name, d->field->type(), axlFieldDiscrete::Scalar, d->field->support(), size);

    // utiliser les parametres pour evaluer.
    //    double u = d->field->start_u();
    //    double v = d->field->start_v();
    //    double w = d->field->start_w();

    //    double step_u = (d->field->end_u() - d->field->start_u())/(d->field->numbersample_u());
    //    double step_v = (d->field->end_v() - d->field->start_v())/(d->field->numbersample_v());
    //    double step_w = (d->field->end_w() - d->field->start_w())/(d->field->numbersample_w());
    for(int i = 0; i < size; i++){
        //        tuple = d->field->vector(u, v, w);
        //        field_magnitude->setScalar(i, qSqrt(tuple[0]*tuple[0]+tuple[0]*tuple[0]+tuple[0]*tuple[0]));
    }
    return field_magnitude;
}



axlAbstractActor *axlActorFieldParametric::actorField(void)
{
    return d->mesh;
}

vtkScalarBarActor *axlActorFieldParametric::scalarBar(void){
    return d->scalar_bar_actor;
}


double axlActorFieldParametric::colRangeMin(void)
{
    return d->scalar_color_range[0];
}

double axlActorFieldParametric::colRangeMax(void)
{
    return d->scalar_color_range[1];
}

int axlActorFieldParametric::isoCount(void)
{
    return d->scalar_iso->GetNumberOfContours();
}

double axlActorFieldParametric::isoRangeMin(void)
{
    return d->scalar_iso_range[0];
}

double axlActorFieldParametric::isoRangeMax(void)
{
    return d->scalar_iso_range[1];
}

double axlActorFieldParametric::glyphScale(void)
{
    return d->vector_glyph->GetScaleFactor();
}

double axlActorFieldParametric::streamRadius(void)
{
    return d->vector_stream_filter->GetRadius();
}

void axlActorFieldParametric::setInteractor(void *interactor)
{
    d->interactor = static_cast<vtkRenderWindowInteractor *>(interactor);

    this->setup();
}

void axlActorFieldParametric::updateArray(void){
    int size = 0;
    size = d->field->size();
    //qDebug() << Q_FUNC_INFO << 1 << size;
    d->array->SetNumberOfTuples(size);

    if(!d->mesh->getUnstructuredGrid() && !d->mesh->getPolyData()){
        if(dynamic_cast<axlActorDataDynamic *>(d->mesh)){
            axlActorDataDynamic *actorDD = dynamic_cast<axlActorDataDynamic *>(d->mesh);
            d->mesh->setPolyData(dynamic_cast<axlActor *>(actorDD->outputActor())->getMapper()->GetInput());
        }else{
            d->mesh->setPolyData(d->mesh->getMapper()->GetInput());
        }
    }

    //fill array according to the field support is a curve, a surface or a volume.


    //curve
    if(dynamic_cast<axlAbstractFieldParametricCurve *>(d->field)){

        axlAbstractFieldParametricCurve *fieldSurface = dynamic_cast<axlAbstractFieldParametricCurve *>(d->field);

        double start_u = fieldSurface->start_u();
        double end_u = fieldSurface->end_u();

        double u = start_u;

        int n_u = fieldSurface->numbersample_u();

        double step_u = (end_u - start_u)/(n_u);




        for(int i = 0; i < n_u - 1; i++)
        {

            if(d->field->kind()==axlAbstractFieldParametric::Scalar){
                d->array->SetTuple1( i, fieldSurface->scalar(u));
                d->array->Modified();
            }else if(d->field->kind()==axlAbstractFieldParametric::Vector){
                d->array->SetTuple( i, fieldSurface->vector(u));
                d->array->Modified();
            }else if(d->field->kind()==axlAbstractFieldParametric::Tensor){
                d->array->SetTuple( i, fieldSurface->tensor(u));
                d->array->Modified();
            }

            u += step_u;

        }

    }

    //surface
    else if(dynamic_cast<axlAbstractFieldParametricSurface *>(d->field)){

        axlAbstractFieldParametricSurface *fieldSurface = dynamic_cast<axlAbstractFieldParametricSurface *>(d->field);

        double start_u = fieldSurface->start_u();
        double start_v = fieldSurface->start_v();
        double end_u = fieldSurface->end_u();
        double end_v = fieldSurface->end_v();

        double u = start_u;
        double v = start_v;

        int n_u = fieldSurface->numbersample_u();
        int n_v = fieldSurface->numbersample_v();

        double step_u = (end_u - start_u)/(n_u);
        double step_v = (end_v - start_v)/(n_v);



        for(int j = 0; j < n_v - 1; j++)
        {
            for(int i = 0; i < n_u - 1; i++)
            {

                if(d->field->kind()==axlAbstractFieldParametric::Scalar){
                    d->array->SetTuple1(j * n_u + i, fieldSurface->scalar(u,v));
                    d->array->Modified();
                }else if(d->field->kind()==axlAbstractFieldParametric::Vector){
                    d->array->SetTuple(j * n_u + i, fieldSurface->vector(u,v) );
                    d->array->Modified();
                }else if(d->field->kind()==axlAbstractFieldParametric::Tensor){
                    d->array->SetTuple(j * n_u + i, fieldSurface->tensor(u,v));
                    d->array->Modified();
                }

                u += step_u;

            }

            if(d->field->kind()==axlAbstractFieldParametric::Scalar){
                d->array->SetTuple1(j * n_u +  n_u - 1 , fieldSurface->scalar(end_u,v));
                d->array->Modified();
            }else if(d->field->kind()==axlAbstractFieldParametric::Vector){
                d->array->SetTuple(j * n_u +  n_u - 1 , fieldSurface->vector(end_u,v));
                d->array->Modified();
            }else if(d->field->kind()==axlAbstractFieldParametric::Tensor){
                d->array->SetTuple(j * n_u +  n_u - 1 , fieldSurface->tensor(end_u,v));
                d->array->Modified();
            }

            u = start_u;
            v += step_v;
        }

        for(int i = 0; i < n_u - 1; i++)
        {
            if(d->field->kind()== axlAbstractFieldParametric::Scalar){
                d->array->SetTuple1((n_v - 1) * n_u + i,  fieldSurface->scalar(u,end_v));
                d->array->Modified();
            }else if(d->field->kind()== axlAbstractFieldParametric::Vector){
                d->array->SetTuple((n_v - 1) * n_u + i,  fieldSurface->vector(u,end_v));
                d->array->Modified();
            }else if(d->field->kind()== axlAbstractFieldParametric::Tensor){
                d->array->SetTuple((n_v - 1) * n_u + i,  fieldSurface->tensor(u,end_v));
                d->array->Modified();
            }

            u += step_u;
        }

        if(d->field->kind()== axlAbstractFieldParametric::Scalar){
            d->array->SetTuple1(n_v * n_u - 1,fieldSurface->scalar(end_u,end_v));
            d->array->Modified();
        }else if(d->field->kind()== axlAbstractFieldParametric::Vector){
            d->array->SetTuple(n_v * n_u - 1,fieldSurface->vector(end_u,end_v));
            d->array->Modified();
        }else if(d->field->kind()== axlAbstractFieldParametric::Tensor){
            d->array->SetTuple(n_v * n_u - 1,fieldSurface->tensor(end_u,end_v));
            d->array->Modified();
        }


    }//volume
    else if(dynamic_cast<axlAbstractFieldParametricVolume *>(d->field)){

        axlAbstractFieldParametricVolume *fieldVolume = dynamic_cast<axlAbstractFieldParametricVolume *>(d->field);
        double start_u = fieldVolume->start_u();
        double start_v = fieldVolume->start_v();
        double start_w = fieldVolume->start_w();
        double end_u = fieldVolume->end_u();
        double end_v = fieldVolume->end_v();
        double end_w = fieldVolume->end_w();

        double u = start_u;
        double v = start_v;
        double w = start_w;

        int n_u = fieldVolume->numbersample_u();
        int n_v = fieldVolume->numbersample_v();
        int n_w = fieldVolume->numbersample_w();

        double step_u = (end_u - start_u)/(n_u);
        double step_v = (end_v - start_v)/(n_v);
        double step_w = (end_w - start_w)/(n_w);

        int ind1 = 0;
        int ind2 = 0;
        int ind3 = 0;
        for(int k = 0; k < n_w - 1; k++)
        {
            ind1 = k * n_u * n_v;
            for(int i = 0; i < n_v - 1; i++)
            {
                ind2 = i * n_u;
                for(int j = 0; j < n_u - 1; j++)
                {
                    ind3 = ind1 + ind2 +j;
                    if(d->field->kind()== axlAbstractFieldParametric::Scalar){
                        d->array->SetTuple1(ind3,fieldVolume->scalar(u, v,w));
                        d->array->Modified();
                    }else if(d->field->kind()== axlAbstractFieldParametric::Vector){
                        //qDebug() << Q_FUNC_INFO << 2;
                        // double *tab = fieldVolume->vector(u, v,w);
                        d->array->Modified();
                        //qDebug() << Q_FUNC_INFO << 3 << " " << tab[0] << " "<< tab[1]<< " " << tab[2] ;
                        d->array->SetTuple(ind3,fieldVolume->vector(u, v,w));
                    }else if(d->field->kind()== axlAbstractFieldParametric::Tensor){
                        d->array->SetTuple(ind3,fieldVolume->tensor(u, v,w));
                        d->array->Modified();
                    }
                    u += step_u;
                }
                ind3 = ind1 + ind2 + (n_u - 1);
                if(d->field->kind()== axlAbstractFieldParametric::Scalar){
                    d->array->SetTuple1(ind3,fieldVolume->scalar(end_u, v, w));
                    d->array->Modified();
                }else if(d->field->kind()== axlAbstractFieldParametric::Vector){
                    d->array->SetTuple(ind3,fieldVolume->vector(end_u, v, w));
                    d->array->Modified();
                }else if(d->field->kind()== axlAbstractFieldParametric::Tensor){
                    d->array->SetTuple(ind3,fieldVolume->tensor(end_u, v, w));
                    d->array->Modified();
                }

                u = start_u;
                v += step_v;
            }
            ind3 = ind1 + (n_v -1)* n_u + (n_u - 1);
            if(d->field->kind()== axlAbstractFieldParametric::Scalar){
                d->array->SetTuple1(ind3,fieldVolume->scalar(end_u, end_v, w));
                d->array->Modified();
            }else if(d->field->kind()== axlAbstractFieldParametric::Vector){
                d->array->SetTuple(ind3,fieldVolume->vector(end_u, end_v, w));
                d->array->Modified();
            } else if(d->field->kind()== axlAbstractFieldParametric::Tensor){
                d->array->SetTuple(ind3,fieldVolume->tensor(end_u, end_v, w));
                d->array->Modified();
            }


            u = start_u;
            v = start_v;
            w += step_w;
        }


        ind1 = n_u * n_v * (n_w - 1);
        for(int j = 0; j < n_v - 1; j++)
        {
            ind2 = j * n_u;
            for(int i = 0; i < n_u - 1; i++)
            {
                ind3 = ind1 + ind2 + i;
                if(d->field->kind()== axlAbstractFieldParametric::Scalar){
                    d->array->SetTuple1(ind3, fieldVolume->scalar(u, v, end_w));
                    d->array->Modified();
                }else if(d->field->kind()== axlAbstractFieldParametric::Vector){
                    d->array->SetTuple(ind3, fieldVolume->vector(u, v, end_w));
                    d->array->Modified();
                }else if(d->field->kind()== axlAbstractFieldParametric::Tensor){
                    d->array->SetTuple(ind3, fieldVolume->tensor(u, v, end_w));
                    d->array->Modified();
                }
                u+=step_u;

            }
            ind3 = ind1 + ind2 + (n_u -1);
            if(d->field->kind()== axlAbstractFieldParametric::Scalar){
                d->array->SetTuple1(ind3,fieldVolume->scalar(end_u, v, end_w));
                d->array->Modified();
            }else if(d->field->kind()== axlAbstractFieldParametric::Vector){
                d->array->SetTuple(ind3,fieldVolume->vector(end_u, v, end_w));
                d->array->Modified();
            }else if(d->field->kind()== axlAbstractFieldParametric::Tensor){
                d->array->SetTuple(ind3,fieldVolume->tensor(end_u, v, end_w));
                d->array->Modified();
            }

            u=start_u;
            v+=step_v;
        }

        w=start_w;
        ind2 = (n_v-1) * n_u;

        for(int k = 0; k < n_w ; k++)
        {
            ind1 = n_u * n_v * k;
            for(int i = 0; i < n_u - 1; i++)
            {
                ind3 = ind1 + ind2 + i;
                if(d->field->kind()== axlAbstractFieldParametric::Scalar){
                    d->array->SetTuple1(ind3,fieldVolume->scalar(u, end_v, w));
                    d->array->Modified();
                }else if(d->field->kind()== axlAbstractFieldParametric::Vector){
                    d->array->SetTuple(ind3,fieldVolume->vector(u, end_v, w));
                    d->array->Modified();
                }else if(d->field->kind()== axlAbstractFieldParametric::Tensor){
                    d->array->SetTuple(ind3,fieldVolume->tensor(u, end_v, w));
                    d->array->Modified();
                }

                u+=step_u;

            }
            ind3 = ind2 + ind1 + (n_u-1);
            u=start_u;
            w+=step_w;
        }

        if(d->field->kind()== axlAbstractFieldParametric::Scalar){
            d->array->SetTuple1(n_u * n_v * n_w -1,fieldVolume->scalar(end_u, end_v, end_w));
            d->array->Modified();
        }else if(d->field->kind()== axlAbstractFieldParametric::Vector){
            d->array->SetTuple(n_u * n_v * n_w -1,fieldVolume->vector(end_u, end_v, end_w));
            d->array->Modified();
        }else if(d->field->kind()== axlAbstractFieldParametric::Tensor){
            d->array->SetTuple(n_u * n_v * n_w -1,fieldVolume->tensor(end_u, end_v, end_w));
            d->array->Modified();
        }
    }

    //sets the minimum and maximum values of the field.
    d->field->setMin(this->minValue());
    d->field->setMax(this->maxValue());

    //setRange
    if(d->scalar_color_range && d->scalar_color_mapper){
        this->setColRangeMin(d->field->minValue());
        this->setColRangeMax(d->field->maxValue());
    }
}


void axlActorFieldParametric::setData(dtkAbstractData *field)
{
    d->field = dynamic_cast<axlAbstractFieldParametric *>(field);

    if(d->field){
        //fill array
        axlAbstractData * parentData = dynamic_cast<axlAbstractData *>(d->field->parent());
        if(parentData){
            connect(parentData, SIGNAL(modifiedGeometry()), this, SLOT(update()));
        }
        axlAbstractData *axlData = dynamic_cast<axlAbstractData *>(d->mesh->data());
        connect(axlData, SIGNAL(modifiedGeometry()), this, SLOT(update()));

        if(d->field->type() == axlAbstractFieldParametric::Int){
            d->array = vtkIntArray::New();
        }else if(d->field->type() == axlAbstractFieldParametric::Float){
            d->array = vtkFloatArray::New();
        }else if(d->field->type() == axlAbstractFieldParametric::Double){
            d->array = vtkDoubleArray::New();
        }

        axlAbstractFieldParametric::Kind kind = d->field->kind();

        switch(kind) {
        case axlAbstractFieldParametric::Scalar:
            d->array->SetNumberOfComponents(1);
            break;
        case axlAbstractFieldParametric::Vector:
            d->array->SetNumberOfComponents(3);
            break;
        case axlAbstractFieldParametric::Tensor:
            d->array->SetNumberOfComponents(9);
            break;
        default:
            qDebug() << "Unsupported field kind";
        };

        QString name = d->field->objectName();
        d->array->SetName(qPrintable(name));

        this->updateArray();
        this->setup();
    }

}

void axlActorFieldParametric::setActorField(axlAbstractActor *actorfield)
{
    d->mesh = dynamic_cast<axlActor *>(actorfield);

    this->setup();
}

void axlActorFieldParametric::setColRangeMin(double min)
{
    d->scalar_color_range[0] = min;
    d->scalar_color_mapper->SetScalarRange(d->scalar_color_range);
    d->scalar_color_mapper->Update();
}

void axlActorFieldParametric::setColRangeMax(double max)
{
    d->scalar_color_range[1] = max;
    d->scalar_color_mapper->SetScalarRange(d->scalar_color_range);
    d->scalar_color_mapper->Update();
}

void axlActorFieldParametric::setIsoCount(int count)
{
    d->scalar_iso_count = count;
    d->scalar_iso->GenerateValues(d->scalar_iso_count, d->scalar_iso_range[0], d->scalar_iso_range[1]);
    d->scalar_iso->Update();
}

void axlActorFieldParametric::setIsoRangeMin(double min)
{
    d->scalar_iso_range[0] = min;
    d->scalar_iso->GenerateValues(d->scalar_iso_count, d->scalar_iso_range[0], d->scalar_iso_range[1]);
    d->scalar_iso->Update();
    d->scalar_iso_color_mapper->SetScalarRange(d->scalar_iso_range);
    d->scalar_iso_color_mapper->Update();
}

void axlActorFieldParametric::setIsoRangeMax(double max)
{
    d->scalar_iso_range[1] = max;
    d->scalar_iso->GenerateValues(d->scalar_iso_count, d->scalar_iso_range[0], d->scalar_iso_range[1]);
    d->scalar_iso->Update();
    d->scalar_iso_color_mapper->SetScalarRange(d->scalar_iso_range);
    d->scalar_iso_color_mapper->Update();
}

void axlActorFieldParametric::setGlyphScale(double scale)
{
    //compute size of controlPoints
    double bounds[6]; d->mesh->GetBounds(bounds);
    double side = qAbs(bounds[1]-bounds[0]);
    side += qAbs(bounds[3]-bounds[2]);
    side += qAbs(bounds[5]-bounds[4]);

    side /= 30;
    //d->vector_glyph_source->SetShaftRadius(scale * side);
    //d->vector_glyph_source->SetTipLength(200 *3 * 3.5 * scale * side);
    //d->vector_glyph_source->SetTipRadius(3 * scale * side);
    d->vector_glyph->SetScaleFactor(scale * side);
    d->vector_hedgehog->SetScaleFactor(scale * side * 0.1);
}

void axlActorFieldParametric::setStreamPropagation(double propagation)
{
    d->vector_stream_tracer->SetMaximumPropagation(propagation);
    d->vector_stream_tracer->Update();
}

void axlActorFieldParametric::setStreamRadius(double radius)
{
    d->vector_stream_filter->SetRadius(radius);
    d->vector_stream_filter->Update();
}

void axlActorFieldParametric::setStreamDirection(int direction)
{
    switch (direction) {
    case 0:
        d->vector_stream_tracer->SetIntegrationDirectionToForward();
        break;
    case 1:
        d->vector_stream_tracer->SetIntegrationDirectionToBackward();
        break;
    case 2:
        d->vector_stream_tracer->SetIntegrationDirectionToBoth();
        break;
    default:
        break;
    }

    d->vector_stream_tracer->Update();
}

void axlActorFieldParametric::displayAsColor(void)
{
    if (d->mesh->getUnstructuredGrid()) {
        d->mesh->getDataSetMapper()->ScalarVisibilityOn();
    } else{
        d->mesh->getMapper()->ScalarVisibilityOn();
    }

    if (d->field->kind() != axlAbstractFieldParametric::Scalar)
        return;

    if (d->field->support() == axlAbstractFieldParametric::Point) {
        if(d->mesh->getUnstructuredGrid()){
            static_cast<vtkUnstructuredGrid *>(d->mesh->getUnstructuredGrid())->GetPointData()->SetActiveScalars(qPrintable(d->field->objectName()));
        } else {
            static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetPointData()->SetActiveScalars(qPrintable(d->field->objectName()));
        }
    }
    if (d->field->support() == axlAbstractFieldParametric::Cell) {
        if(d->mesh->getUnstructuredGrid()){
            static_cast<vtkUnstructuredGrid *>(d->mesh->getUnstructuredGrid())->GetCellData()->SetActiveScalars(qPrintable(d->field->objectName()));
        } else {
            static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetCellData()->SetActiveScalars(qPrintable(d->field->objectName()));
        }
    }

    d->scalar_color_actor->SetVisibility(1);
    d->scalar_iso_actor->SetVisibility(0);
    d->scalar_iso_color_actor->SetVisibility(0);
    d->scalar_display_as_iso = false;
    d->scalar_bar_actor->SetVisibility(1);

}

void axlActorFieldParametric::displayAsNoneScalar(void)
{
    if(d->scalar_color_actor)
    {
        d->scalar_color_actor->SetVisibility(0);
        d->scalar_iso_actor->SetVisibility(0);
        d->scalar_iso_color_actor->SetVisibility(0);
        d->scalar_display_as_iso = false;
        d->scalar_bar_actor->SetVisibility(0);
    }
    if(d->mesh->getUnstructuredGrid()){
        d->mesh->getDataSetMapper()->ScalarVisibilityOn();
    }
    else{
        d->mesh->getMapper()->ScalarVisibilityOn();
    }
    if(d->field->support() == axlAbstractFieldParametric::Point) {
        if(d->mesh->getUnstructuredGrid()){
            static_cast<vtkUnstructuredGrid *>(d->mesh->getUnstructuredGrid())->GetPointData()->SetActiveScalars("mapperCollorArrayDefaultField");
        }else{
            static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetPointData()->SetActiveScalars("mapperCollorArrayDefaultField");
        }
    }
}

void axlActorFieldParametric::displayAsIso(void)
{
    if (d->field->kind() != axlAbstractFieldParametric::Scalar)
        return;

    if(d->field->support() == axlAbstractFieldParametric::Point) {
        if (d->mesh->getUnstructuredGrid()) {
            static_cast<vtkUnstructuredGrid *>(d->mesh->getUnstructuredGrid())->GetPointData()->SetActiveScalars(qPrintable(d->field->objectName()));
        } else {
            static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetPointData()->SetActiveScalars(qPrintable(d->field->objectName()));
        }
    }

    if(d->field->support() == axlAbstractFieldParametric::Cell) {
        if(d->mesh->getUnstructuredGrid()){
            static_cast<vtkUnstructuredGrid *>(d->mesh->getUnstructuredGrid())->GetCellData()->SetActiveScalars(qPrintable(d->field->objectName()));
        }
        else{
            static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetCellData()->SetActiveScalars(qPrintable(d->field->objectName()));
        }
    }

    if(d->field->support() == axlAbstractFieldParametric::Point) {
        if(d->mesh->getUnstructuredGrid()){
            d->mesh->getDataSetMapper()->ScalarVisibilityOff();
        }
        else{
            d->mesh->getMapper()->ScalarVisibilityOff();
        }
    }

    d->scalar_color_actor->SetVisibility(0);
    d->scalar_iso_actor->SetVisibility(1);
    d->scalar_iso_color_actor->SetVisibility(0);
    d->scalar_display_as_iso = true;
    d->scalar_bar_actor->SetVisibility(0);
}

void axlActorFieldParametric::displayAsNoneVector(void)
{
    if (d->field->kind() == axlAbstractFieldParametric::Vector ) {

        d->vector_hedgehog_actor->SetVisibility(0);
        d->vector_glyph_actor->SetVisibility(0);
        d->vector_stream_actor->SetVisibility(0);
        d->vector_stream_widget->Off();
    }
}

void axlActorFieldParametric::displayAsHedge(void)
{
    if (d->field->kind() == axlAbstractFieldParametric::Vector ) {

        d->vector_hedgehog_actor->SetVisibility(1);
        d->vector_glyph_actor->SetVisibility(0);
        d->vector_stream_actor->SetVisibility(0);
        d->vector_stream_widget->Off();
    }
}

void axlActorFieldParametric::displayAsGlyph(void)
{
    if (d->field->kind() == axlAbstractFieldParametric::Vector) {

        d->vector_hedgehog_actor->SetVisibility(0);
        d->vector_glyph_actor->SetVisibility(1);
        d->vector_stream_actor->SetVisibility(0);
        d->vector_stream_widget->Off();
    }
}

void axlActorFieldParametric::displayAsStream(void)
{
    if (d->field->kind() == axlAbstractFieldParametric::Vector) {

        d->vector_hedgehog_actor->SetVisibility(0);
        d->vector_glyph_actor->SetVisibility(0);
        d->vector_stream_actor->SetVisibility(1);
        d->vector_stream_widget->On();
    }
}

#include <axlCore/axlAbstractSurfaceBSpline.h>

void axlActorFieldParametric::update(void)
{
    if(!d->mesh)
        return;

    if(d->mesh->getUnstructuredGrid()){
        d->mesh->getUnstructuredGrid()->Modified();
#if (VTK_MAJOR_VERSION <= 5)
        d->mesh->getUnstructuredGrid()->Update();
#endif
    }else{
        d->mesh->getPolyData()->Modified();
#if (VTK_MAJOR_VERSION <= 5)
        d->mesh->getPolyData()->Update();
#endif
    }

    if(!d->field )
        return;

    this->updateArray();

    if(d->field->support() == axlAbstractFieldParametric::Point) {
        if(d->mesh->getUnstructuredGrid()){
            static_cast<vtkUnstructuredGrid *>(d->mesh->getUnstructuredGrid())->GetPointData()->RemoveArray(d->array->GetName());
            static_cast<vtkUnstructuredGrid *>(d->mesh->getUnstructuredGrid())->GetPointData()->AddArray(d->array);
        }else{
            static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetPointData()->RemoveArray(d->array->GetName());
            static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetPointData()->AddArray(d->array);
        }
        if(d->field->kind() == axlAbstractFieldParametric::Scalar){
            //update field vtkActor(s)
            d->scalar_iso_actor->SetOrientation(d->mesh->getActor()->GetOrientation());
            d->scalar_iso_actor->SetPosition(d->mesh->getActor()->GetPosition());
            d->scalar_iso_color_actor->SetOrientation(d->mesh->getActor()->GetOrientation());
            d->scalar_iso_color_actor->SetPosition(d->mesh->getActor()->GetPosition());
            d->scalar_color_actor->SetOrientation(d->mesh->getActor()->GetOrientation());
            d->scalar_color_actor->SetPosition(d->mesh->getActor()->GetPosition());

            if(d->mesh->getUnstructuredGrid()){
                static_cast<vtkUnstructuredGrid *>(d->mesh->getUnstructuredGrid())->GetPointData()->SetActiveScalars(d->array->GetName());
            }else{
                static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetPointData()->SetActiveScalars(d->array->GetName());
            }
        }else if (d->field->kind() == axlAbstractFieldParametric::Vector){
            //update field vtkActor(s)
            d->vector_glyph_actor->SetOrientation(d->mesh->getActor()->GetOrientation());
            d->vector_glyph_actor->SetPosition(d->mesh->getActor()->GetPosition());
            d->vector_hedgehog_actor->SetOrientation(d->mesh->getActor()->GetOrientation());
            d->vector_hedgehog_actor->SetPosition(d->mesh->getActor()->GetPosition());
            d->vector_stream_actor->SetOrientation(d->mesh->getActor()->GetOrientation());
            d->vector_stream_actor->SetPosition(d->mesh->getActor()->GetPosition());

            if(d->mesh->getUnstructuredGrid()){
                static_cast<vtkUnstructuredGrid *>(d->mesh->getUnstructuredGrid())->GetPointData()->SetActiveVectors(d->array->GetName());
            }else{
                static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetPointData()->SetActiveVectors(d->array->GetName());
            }
        }else if (d->field->kind() == axlAbstractFieldParametric::Tensor){
            if(d->mesh->getUnstructuredGrid()){
                static_cast<vtkUnstructuredGrid *>(d->mesh->getUnstructuredGrid())->GetPointData()->SetActiveTensors(d->array->GetName());
            }else{
                static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetPointData()->SetActiveTensors(d->array->GetName());
            }
        }


    }



    if(d->field->support() == axlAbstractFieldParametric::Cell) {
        static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetCellData()->RemoveArray(d->array->GetName());
        static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetCellData()->AddArray(d->array);
        if(d->field->kind() == axlAbstractFieldParametric::Scalar)
            static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetCellData()->SetActiveScalars(d->array->GetName());
        else if (d->field->kind() == axlAbstractFieldParametric::Vector)
            static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetCellData()->SetActiveVectors(d->array->GetName());
        else if (d->field->kind() == axlAbstractFieldParametric::Tensor)
            static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetCellData()->SetActiveTensors(d->array->GetName());


    }

}

void axlActorFieldParametric::setActiveFieldKind(void)
{
    if(d->mesh && d->field)
        if(d->field->support() == axlAbstractFieldParametric::Point) {
            if(d->field->kind() == axlAbstractFieldParametric::Scalar)
                if(d->mesh->getUnstructuredGrid()){
                    static_cast<vtkUnstructuredGrid *>(d->mesh->getUnstructuredGrid())->GetPointData()->SetActiveScalars(qPrintable(d->field->objectName()));
                }else{
                    static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetPointData()->SetActiveScalars(qPrintable(d->field->objectName()));
                }
            else if(d->field->kind() == axlAbstractFieldParametric::Vector){
                if(d->mesh->getUnstructuredGrid()){
                    static_cast<vtkUnstructuredGrid *>(d->mesh->getUnstructuredGrid())->GetPointData()->SetActiveVectors(qPrintable(d->field->objectName()));
                }else{
                    static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetPointData()->SetActiveVectors(qPrintable(d->field->objectName()));
                }
            }
            else if(d->field->kind() == axlAbstractFieldParametric::Tensor){
                if(d->mesh->getUnstructuredGrid()){
                    static_cast<vtkUnstructuredGrid *>(d->mesh->getUnstructuredGrid())->GetPointData()->SetActiveTensors(qPrintable(d->field->objectName()));
                }else{
                    static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetPointData()->SetActiveTensors(qPrintable(d->field->objectName()));
                }
            }
        }


    if(d->mesh && d->field)
        if(d->field->support() == axlAbstractFieldParametric::Cell) {
            if(d->field->kind() == axlAbstractFieldParametric::Scalar)
                static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetCellData()->SetActiveScalars(qPrintable(d->field->objectName()));
            else if(d->field->kind() == axlAbstractFieldParametric::Vector)
                static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetCellData()->SetActiveVectors(qPrintable(d->field->objectName()));
            else if(d->field->kind() == axlAbstractFieldParametric::Tensor)
                static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetCellData()->SetActiveTensors(qPrintable(d->field->objectName()));
        }

}

void *axlActorFieldParametric::scalarColorMapper(void)
{
    return d->scalar_color_mapper;
}

axlActorFieldParametric::axlActorFieldParametric(void) : axlAbstractActorField(), d(new axlActorFieldParametricPrivate)
{
    d->field = NULL;
    d->mesh = NULL;
    d->isoRadius = 0.01;
    d->array = NULL;
}

axlActorFieldParametric::~axlActorFieldParametric(void)
{
    delete d;

    d = NULL;
}

void axlActorFieldParametric::setup(void)
{

    if(!d->mesh)
        return;

    if(!d->field) {
        qDebug() << DTK_PRETTY_FUNCTION << "No field.";
        return;
    }


    if(!d->interactor)
        return;


    // -- Scalar field


    if(d->field->kind() == axlAbstractFieldParametric::Scalar) {

        if(d->scalar_color_mapper && d->scalar_color_actor && d->scalar_iso_color_actor)
            return;

        if(d->field->support() == axlAbstractFieldParametric::Point) {
            //remove first eventual array of the same name
            if(d->mesh->getPolyData()){
                static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetPointData()->RemoveArray(d->array->GetName());
                static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetPointData()->AddArray(d->array);
                static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetPointData()->SetActiveScalars(d->array->GetName());
            }
            if(d->mesh->getUnstructuredGrid()){
                static_cast<vtkUnstructuredGrid *>(d->mesh->getUnstructuredGrid())->GetPointData()->RemoveArray(d->array->GetName());
                static_cast<vtkUnstructuredGrid *>(d->mesh->getUnstructuredGrid())->GetPointData()->AddArray(d->array);
                static_cast<vtkUnstructuredGrid *>(d->mesh->getUnstructuredGrid())->GetPointData()->SetActiveScalars(d->array->GetName());
            }
        }

        if(d->field->support() == axlAbstractFieldParametric::Cell) {

            static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetCellData()->RemoveArray(d->array->GetName());
            static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetCellData()->AddArray(d->array);
            static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetPointData()->SetActiveScalars(d->array->GetName());

        }

        // -- iso contours

        d->scalar_display_as_iso = false;
        d->scalar_iso_count = 10;
        d->array->GetRange(d->scalar_iso_range);

        d->scalar_iso = vtkSmartPointer<vtkContourFilter>::New();
        if(d->mesh->getUnstructuredGrid()){
#if (VTK_MAJOR_VERSION <= 5)
            d->scalar_iso->SetInput(static_cast<vtkUnstructuredGrid *>(d->mesh->getUnstructuredGrid()));
#else
            d->scalar_iso->SetInputData(static_cast<vtkUnstructuredGrid *>(d->mesh->getUnstructuredGrid()));
#endif
        }else{
#if (VTK_MAJOR_VERSION <= 5)
            d->scalar_iso->SetInput(static_cast<vtkPolyData *>(d->mesh->getPolyData()));
#else
            d->scalar_iso->SetInputData(static_cast<vtkPolyData *>(d->mesh->getPolyData()));
#endif
        }
        d->scalar_iso->GenerateValues(d->scalar_iso_count, d->scalar_iso_range[0], d->scalar_iso_range[1]);

        d->scalar_iso_tube_filter = vtkSmartPointer<vtkTubeFilter>::New();
        d->scalar_iso_tube_filter->SetRadius(d->isoRadius);
        d->scalar_iso_tube_filter->SetNumberOfSides(8);
#if (VTK_MAJOR_VERSION <= 5)
        d->scalar_iso_tube_filter->SetInput(d->scalar_iso->GetOutput());
#else
        d->scalar_iso_tube_filter->SetInputData(d->scalar_iso->GetOutput());
#endif
        d->scalar_iso_mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
#if (VTK_MAJOR_VERSION <= 5)
        d->scalar_iso_mapper->SetInput(d->scalar_iso_tube_filter->GetOutput());
#else
        d->scalar_iso_mapper->SetInputData(d->scalar_iso_tube_filter->GetOutput());
#endif

        d->scalar_iso_actor = vtkSmartPointer<vtkActor>::New();
        d->scalar_iso_actor->SetOrientation(d->mesh->getActor()->GetOrientation());
        d->scalar_iso_actor->SetPosition(d->mesh->getActor()->GetPosition());
        d->scalar_iso_actor->SetMapper(d->scalar_iso_mapper);
        d->scalar_iso_actor->SetVisibility(0);

        this->AddPart(d->scalar_iso_actor);

        // -- iso color mapping

        d->scalar_iso_color_mapper = vtkPolyDataMapper::New();
#if (VTK_MAJOR_VERSION <= 5)
        d->scalar_iso_color_mapper->SetInput(d->scalar_iso->GetOutput());
#else
        d->scalar_iso_color_mapper->SetInputData(d->scalar_iso->GetOutput());
#endif
        d->scalar_iso_color_mapper->SetColorModeToMapScalars();

        if(d->field->support() == axlAbstractFieldParametric::Point)
            d->scalar_iso_color_mapper->SetScalarModeToUsePointData();

        if(d->field->support() == axlAbstractFieldParametric::Cell)
            d->scalar_iso_color_mapper->SetScalarModeToUseCellData();

        d->scalar_iso_color_mapper->SelectColorArray(d->array->GetName());
        d->scalar_iso_color_mapper->SetScalarVisibility(true);
        d->scalar_iso_color_mapper->SetScalarRange(d->scalar_iso_range);

        d->scalar_iso_color_actor = vtkSmartPointer<vtkActor>::New();
        d->scalar_iso_color_actor->SetOrientation(d->mesh->getActor()->GetOrientation());
        d->scalar_iso_color_actor->SetPosition(d->mesh->getActor()->GetPosition());
        d->scalar_iso_color_actor->SetMapper(d->scalar_iso_color_mapper);
        d->scalar_iso_color_actor->SetVisibility(0);

        this->AddPart(d->scalar_iso_color_actor);

        // -- color mapping

        d->array->GetRange(d->scalar_color_range);

        d->scalar_color_mapper = vtkDataSetMapper::New();
#if (VTK_MAJOR_VERSION <= 5)
        if(d->mesh->getUnstructuredGrid())
            d->scalar_color_mapper->SetInput(static_cast<vtkUnstructuredGrid *>(d->mesh->getUnstructuredGrid()));
        else
            d->scalar_color_mapper->SetInput(static_cast<vtkPolyData *>(d->mesh->getPolyData()));
#else
        if(d->mesh->getUnstructuredGrid())
            d->scalar_color_mapper->SetInputData(static_cast<vtkUnstructuredGrid *>(d->mesh->getUnstructuredGrid()));
        else
            d->scalar_color_mapper->SetInputData(static_cast<vtkPolyData *>(d->mesh->getPolyData()));
#endif

        d->scalar_color_mapper->SetColorModeToMapScalars();

        if(d->field->support() == axlAbstractFieldParametric::Point)
            d->scalar_color_mapper->SetScalarModeToUsePointData();

        if(d->field->support() == axlAbstractFieldParametric::Cell)
            d->scalar_color_mapper->SetScalarModeToUseCellData();

        d->scalar_color_mapper->SelectColorArray(d->array->GetName());
        d->scalar_color_mapper->SetScalarVisibility(true);
        d->scalar_color_mapper->SetScalarRange(d->scalar_color_range);

        d->scalar_color_actor = vtkSmartPointer<vtkActor>::New();
        d->scalar_color_actor->SetOrientation(d->mesh->getActor()->GetOrientation());
        d->scalar_color_actor->SetPosition(d->mesh->getActor()->GetPosition());
        d->scalar_color_actor->SetMapper(d->scalar_color_mapper);
        d->scalar_color_actor->SetVisibility(0);

        this->AddPart(d->scalar_color_actor);

        ////Add actor for color bar
        d->scalar_bar_actor = vtkSmartPointer<vtkScalarBarActor>::New();
        d->scalar_bar_actor ->SetLookupTable(d->scalar_color_mapper->GetLookupTable());
        d->scalar_bar_actor ->SetTitle(qPrintable(d->field->name()));
        d->scalar_bar_actor ->SetNumberOfLabels(4);
        d->scalar_bar_actor->SetVisibility(0);

    }

    // -- Vector field



    if(d->field->kind() == axlAbstractFieldParametric::Vector) {

        // -- Append field and its magnitude into vtkPolyData

        if(d->field->support() == axlAbstractFieldParametric::Point) {


            if(d->mesh->getPolyData()){
                static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetPointData()->RemoveArray(d->array->GetName());
                static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetPointData()->AddArray(d->array);
                static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetPointData()->SetActiveVectors(d->array->GetName());
            }
            if(d->mesh->getUnstructuredGrid()){
                static_cast<vtkUnstructuredGrid *>(d->mesh->getUnstructuredGrid())->GetPointData()->RemoveArray(d->array->GetName());
                static_cast<vtkUnstructuredGrid *>(d->mesh->getUnstructuredGrid())->GetPointData()->AddArray(d->array);
                static_cast<vtkUnstructuredGrid *>(d->mesh->getUnstructuredGrid())->GetPointData()->SetActiveVectors(d->array->GetName());
            }
        }

        if(d->field->support() == axlAbstractFieldParametric::Cell) {
            static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetPointData()->RemoveArray(d->array->GetName());
            static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetCellData()->AddArray(d->array);
            static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetCellData()->SetActiveVectors(d->array->GetName());


        }

        // -- hedgehog

        d->vector_hedgehog = vtkSmartPointer<vtkHedgeHog>::New();
#if (VTK_MAJOR_VERSION <= 5)
        if(d->mesh->getPolyData())
            d->vector_hedgehog->SetInput(static_cast<vtkPolyData *>(d->mesh->getPolyData()));
        else
            d->vector_hedgehog->SetInput(static_cast<vtkUnstructuredGrid *>(d->mesh->getUnstructuredGrid()));
#else
        if(d->mesh->getPolyData())
            d->vector_hedgehog->SetInputData(static_cast<vtkPolyData *>(d->mesh->getPolyData()));
        else
            d->vector_hedgehog->SetInputData(static_cast<vtkUnstructuredGrid *>(d->mesh->getUnstructuredGrid()));
#endif
        d->vector_hedgehog->SetVectorModeToUseVector();
        //d->vector_hedgehog->SetScaleFactor(0.05);

        d->vector_hedgehog_mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
        d->vector_hedgehog_mapper->SetInputConnection(d->vector_hedgehog->GetOutputPort());
        d->vector_hedgehog_mapper->ScalarVisibilityOn();

        d->vector_hedgehog_actor = vtkSmartPointer<vtkActor>::New();
        d->vector_hedgehog_actor->SetOrientation(d->mesh->getActor()->GetOrientation());
        d->vector_hedgehog_actor->SetPosition(d->mesh->getActor()->GetPosition());
        d->vector_hedgehog_actor->SetMapper(d->vector_hedgehog_mapper);
        d->vector_hedgehog_actor->SetVisibility(0);

        this->AddPart(d->vector_hedgehog_actor);

        // -- glyphs

        d->vector_glyph_source = vtkArrowSource::New();

        d->vector_glyph = vtkSmartPointer<vtkGlyph3D>::New();
#if (VTK_MAJOR_VERSION <= 5)
        if(d->mesh->getPolyData())
            d->vector_glyph->SetInput(static_cast<vtkPolyData *>(d->mesh->getPolyData()));
        else
            d->vector_glyph->SetInput(static_cast<vtkUnstructuredGrid *>(d->mesh->getUnstructuredGrid()));
        d->vector_glyph->SetSource(d->vector_glyph_source->GetOutput());
#else
        if(d->mesh->getPolyData())
            d->vector_glyph->SetInputData(static_cast<vtkPolyData *>(d->mesh->getPolyData()));
        else
            d->vector_glyph->SetInputData(static_cast<vtkUnstructuredGrid *>(d->mesh->getUnstructuredGrid()));
        d->vector_glyph->SetSourceData(d->vector_glyph_source->GetOutput());
#endif

        d->vector_glyph->SetColorModeToColorByVector();
        d->vector_glyph->SetVectorModeToUseVector();
        //d->vector_glyph->SetScaleModeToScaleByVector();
        d->vector_glyph->SetScaleModeToDataScalingOff();
        //d->vector_glyph->ScalingOff();
        this->setGlyphScale(0.1);
        d->vector_glyph->OrientOn();
        d->vector_glyph->Update();

        d->vector_glyph_mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
        d->vector_glyph_mapper->SetInputConnection(d->vector_glyph->GetOutputPort());

        // vtkSmartPointer<vtkExtractUnstructuredGrid> extractor = vtkSmartPointer<vtkExtractUnstructuredGrid>::New();
        // extractor->SetInput(static_cast<vtkPolyData *>(d->mesh->getPolyData()));

        // vtkSmartPointer<vtkGlyph3DMapper> gpuGlyphMapper = vtkSmartPointer<vtkGlyph3DMapper>::New();
        // gpuGlyphMapper->ScalingOn();
        // gpuGlyphMapper->SetScaleFactor(0.1);
        // gpuGlyphMapper->SetSourceConnection(d->vector_glyph_source->GetOutputPort());
        // gpuGlyphMapper->SetInputConnection(extractor->GetOutputPort());

        d->vector_glyph_actor = vtkSmartPointer<vtkActor>::New();
        d->vector_glyph_actor->SetOrientation(d->mesh->getActor()->GetOrientation());
        d->vector_glyph_actor->SetPosition(d->mesh->getActor()->GetPosition());
        d->vector_glyph_actor->SetMapper(d->vector_glyph_mapper);
        // d->vector_glyph_actor->SetMapper(gpuGlyphMapper);
        d->vector_glyph_actor->SetVisibility(0);

        this->AddPart(d->vector_glyph_actor);


        // -- streams

        d->vector_stream_widget = vtkSmartPointer<vtkSphereWidget2>::New();
        d->vector_stream_widget->SetInteractor(d->interactor);
        d->vector_stream_widget->CreateDefaultRepresentation();
        d->vector_stream_widget->SetTranslationEnabled(true);
        d->vector_stream_widget->SetScalingEnabled(true);
        d->vector_stream_widget->Off();

        d->vector_stream_widget_data = vtkPolyData::New();

        d->vector_stream_widget_representation = vtkSphereRepresentation::SafeDownCast(d->vector_stream_widget->GetRepresentation());
        d->vector_stream_widget_representation->HandleVisibilityOff();
        d->vector_stream_widget_representation->HandleTextOff();
        d->vector_stream_widget_representation->RadialLineOff();
        d->vector_stream_widget_representation->SetPhiResolution(64);
        d->vector_stream_widget_representation->SetThetaResolution(64);
        d->vector_stream_widget_representation->GetPolyData(d->vector_stream_widget_data);

        d->vector_stream_tracer = vtkStreamTracer::New();
#if (VTK_MAJOR_VERSION <= 5)
        d->vector_stream_tracer->SetInput(static_cast<vtkPolyData *>(d->mesh->getPolyData()));
        d->vector_stream_tracer->SetSource(d->vector_stream_widget_data);
#else
        d->vector_stream_tracer->SetInputData(static_cast<vtkPolyData *>(d->mesh->getPolyData()));
        d->vector_stream_tracer->SetSourceData(d->vector_stream_widget_data);
#endif
        d->vector_stream_tracer->SetMaximumPropagation(100);
        d->vector_stream_tracer->SetMinimumIntegrationStep(1.0e-4);
        d->vector_stream_tracer->SetMaximumIntegrationStep(100.0);
        d->vector_stream_tracer->SetIntegrationDirectionToBoth();

        d->vector_stream_filter = vtkTubeFilter::New();
#if (VTK_MAJOR_VERSION <= 5)
        d->vector_stream_filter->SetInput(d->vector_stream_tracer->GetOutput());
#else
        d->vector_stream_filter->SetInputData(d->vector_stream_tracer->GetOutput());
#endif
        d->vector_stream_filter->SetRadius(0.01);
        d->vector_stream_filter->SetNumberOfSides(8);

        d->vector_stream_mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
        d->vector_stream_mapper->SetInputConnection(d->vector_stream_filter->GetOutputPort());
        d->vector_stream_mapper->ScalarVisibilityOn();

        d->vector_stream_actor = vtkSmartPointer<vtkActor>::New();
        d->vector_stream_actor->SetOrientation(d->mesh->getActor()->GetOrientation());
        d->vector_stream_actor->SetPosition(d->mesh->getActor()->GetPosition());
        d->vector_stream_actor->SetMapper(d->vector_stream_mapper);
        d->vector_stream_actor->SetVisibility(0);

        d->vector_stream_observer = axlActorFieldParametricStreamObserver::New();
        d->vector_stream_observer->vector_stream_widget_representation = d->vector_stream_widget_representation;
        d->vector_stream_observer->vector_stream_widget_data = d->vector_stream_widget_data;
        d->vector_stream_observer->vector_stream_tracer = d->vector_stream_tracer;
        d->vector_stream_widget->AddObserver(vtkCommand::InteractionEvent, d->vector_stream_observer);

        this->AddPart(d->vector_stream_actor);

    }

}

double axlActorFieldParametric::minValue(void)
{
    double field_range[2];d->array->GetRange(field_range);
    return field_range[0];
}

double axlActorFieldParametric::maxValue(void)
{
    double field_range[2];d->array->GetRange(field_range);

    return field_range[1];
}

void axlActorFieldParametric::onIsoRadiusChanged(double radius)
{
    d->isoRadius = radius;
    d->scalar_iso_tube_filter->SetRadius(radius);
    d->scalar_iso_tube_filter->Update();
}
