/* axlRendererFactory.h ---
 *
 * Author: Meriadeg Perrinel
 * Copyright (C) 2008 - Meriadeg Perrinel, Inria.
 * Created: Tue Nov  9 16:58:59 2010 (+0100)
 * Version: $Id$
 * Last-Updated: Tue Nov  9 17:09:38 2010 (+0100)
 *           By: Meriadeg Perrinel
 *     Update #: 19
 */

/* Commentary:
 *
 */

/* Change log:
 *
 */


#ifndef AXLRENDERERFACTORY_H
#define AXLRENDERERFACTORY_H

#include <vtkObject.h>
#include <vtkObjectFactory.h>

#include "vtkViewPluginExport.h"

class VTKVIEWPLUGIN_EXPORT axlRendererFactory : public vtkObjectFactory
{
public:
    static axlRendererFactory *New(void);
public:
    virtual const char *GetVTKSourceVersion(void);
    virtual const char *GetDescription(void);
protected:
    axlRendererFactory(void);
protected:
    axlRendererFactory(const axlRendererFactory&);
    void operator=(const axlRendererFactory&);
};

#endif // AXLRENDERERFACTORY_H
