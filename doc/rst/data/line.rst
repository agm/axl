Line
====

A line (or segment) is defined by a starting point (axlPoint) and an ending point (axlPoint).

Type: ``axlLine``

Format:

.. literalinclude:: ../../../data/line.axl 
   :lines: 2-5
   :language: xml
