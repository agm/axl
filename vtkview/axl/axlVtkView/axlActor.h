/* axlActor.h ---
 *
 * Author: Meriadeg Perrinel
 * Copyright (C) 2008 - Meriadeg Perrinel, Inria.
 * Created: Fri Dec 17 10:58:38 2010 (+0100)
 * Version: $Id$
 * Last-Updated: Mon Dec 17 14:11:30 2012 (+0100)
 *           By: Julien Wintz
 *     Update #: 4
 */

/* Commentary:
 *
 */

/* Change log:
 *
 */

#ifndef AXLACTOR_H
#define AXLACTOR_H

#include <axlCore/axlAbstractActor.h>

#include <vtkAssembly.h>
#include <vtkSmartPointer.h>
#include <vtkVersion.h>

#include "axlVtkViewPluginExport.h"



class axlActorControlPolygonObserver;

class axlAbstractActor;

class vtkActor;
class vtkCellArray;
class vtkCellPicker;
class vtkDoubleArray;
class vtkPoints;
class vtkPolyData;
class vtkPolyDataMapper;
class vtkRenderWindowInteractor;
class vtkUnstructuredGrid;
class vtkVolume;
class vtkDataSetMapper;
class vtkDataSet;

class axlActorPrivate;


class AXLVTKVIEWPLUGIN_EXPORT axlActor : public axlAbstractActor, public vtkAssembly
{
public:
    enum ActorState {
        passive,
        selection,
        edition
    };

public:
    virtual dtkAbstractData *data(void) = 0;

public:
    virtual bool isVisible(void);
    virtual void setInteractor(void *interactor);
    virtual vtkRenderWindowInteractor *getInteractor(void);
    virtual int getState(void);
    virtual void setState(int state);
    virtual void setDisplay(bool display);
    virtual void setOpacity(double opacity);
    virtual void setSize(double size);
    virtual void setColor(double red, double green, double blue);
    virtual void setInterpolation(int interpolation);

    virtual void setShader(QString vsfile);
    virtual void setShaderFromString(QString vertex_shader_source,
                                     QString fragment_shader_source);
    virtual void onRemoved(void);

//    virtual axlAbstractActor *createActor(dtkAbstractData *data);

    QStringList fields(void);

protected:
    void drawAssymblyMatrix(void);

public slots:
    virtual void hide(void);
    virtual void show(void);
    virtual void update(void);

public :
    axlActor(void);
    ~axlActor(void);

public:
    dtkAbstractData * getObserverData(void);
    vtkSmartPointer<vtkPoints> getPoints(void);
    void setPoints(vtkSmartPointer<vtkPoints> points);
    vtkSmartPointer<vtkDoubleArray> getNormals(void);
    void setNormals(vtkSmartPointer<vtkDoubleArray> normals);
    vtkSmartPointer<vtkPolyData> getPolyData(void);
    void setPolyData(vtkSmartPointer<vtkPolyData> polyData);
    vtkSmartPointer<vtkCellArray> getCellArray(void);
    void setCellArray(vtkSmartPointer<vtkCellArray> cellArray);
    vtkSmartPointer<vtkActor> getActor(void);
    void setActor(vtkSmartPointer<vtkActor> actor);
    vtkSmartPointer<vtkVolume> getvtkVolume(void);
    void setvtkVolume(vtkSmartPointer<vtkVolume> actor);
    vtkSmartPointer<vtkPolyDataMapper> getMapper(void);
    void setMapper(vtkSmartPointer<vtkPolyDataMapper> mapper);
    vtkSmartPointer<vtkDataSetMapper> getDataSetMapper(void);
    void setDataSetMapper(vtkSmartPointer<vtkDataSetMapper> mapper);
    vtkSmartPointer<vtkCellPicker> getCellPicker();
    void setCellPicker(vtkSmartPointer<vtkCellPicker> cellPicker);
    axlActorControlPolygonObserver *getObserver(void);
    void setObserver(axlActorControlPolygonObserver *observer);
    vtkSmartPointer<vtkUnstructuredGrid> getUnstructuredGrid(void);
    void setUnstructuredGrid(vtkSmartPointer<vtkUnstructuredGrid> unstructuredGrid);
    void setObserverData(dtkAbstractData *data);
    void NewObserver(void);
    void deleteObserver(void);

private:
    axlActorPrivate *d;
};

#endif
