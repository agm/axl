#include <axl-config.h>        // For AXL_PLUGIN_DIR, AXL_DATA_DIR
#include <axlCore/axlReader.h>  // Generic data reader
#include <axlCore/axlWriter.h>  // Generic data writer
#include <axlCore/axlFactoryRegister.h>      // Factory of data
#include <dtkCoreSupport/dtkPluginManager.h> // Plugin manager
#include <axlCore/axlPoint.h>  // Definition of axlPoint
#include <axlCore/axlTorus.h>  // Definition of axlTorus

int main() {

//    // Loading of Axel plugins
//    dtkPluginManager::instance()->setPath(AXL_PLUGIN_DIR);
//    dtkPluginManager::instance()->initializeApplication();
//    dtkPluginManager::instance()->initialize();
//    axlFactoryRegister::initialized();

    //Reading input file
    qDebug()<<"Reading file"<<QString(AXL_DATA_DIR)+"/torus.axl";
    axlReader *obj_reader = new axlReader();
    obj_reader->read(QString(AXL_DATA_DIR)+"/torus.axl");
    QList<axlAbstractData *> list = obj_reader->dataSet();

    // Print description of list[0]
    qDebug()<<list.at(0)->description();

    //Convertion of generic object to a Torus
    axlTorus* axltorus = dynamic_cast<axlTorus*>(list.at(0));
    axlPoint* P= axltorus->centerPoint();
    P->setColor(1.0,0,0);
    P->setSize(0.08);
    qDebug()<< " Point:"<<P->x()<<P->y()<<P->z();

    //Writing output file
    axlWriter *obj_writer = new axlWriter();
    foreach(axlAbstractData* o, list)
        obj_writer->addDataToWrite(o);
    obj_writer->addDataToWrite(P);
    obj_writer->write("tmp.axl");

    //system("axel tmp.axl &");
}
