/* axlActorSphere.cpp ---
 *
 * Author: Meriadeg Perrinel
 * Copyright (C) 2008 - Meriadeg Perrinel, Inria.
 * Created: Fri Dec 17 10:59:21 2010 (+0100)
 * Version: $Id$
 * Last-Updated: Mon Dec 17 15:02:46 2012 (+0100)
 *           By: Julien Wintz
 *     Update #: 31
 */

/* Commentary:
 *
 */

/* Change log:
 *
 */
#include "axlActorSphere.h"

#include "axlActorComposite.h"
#include "axlInteractorStyleSwitch.h"

#include <axlCore/axlSphere.h>
#include <axlCore/axlPoint.h>

#include <QVTKOpenGLWidget.h>

#include <vtkActor.h>
#include <vtkAssemblyPath.h>
//#include <vtkCellPicker.h>
#include <vtkCommand.h>
#include <vtkGlyph3D.h>
#include <vtkInteractorStyleSwitch.h>
#include <vtkLight.h>
#include <vtkLightCollection.h>
#include <vtkObjectFactory.h>
#include <vtkPoints.h>
#include <vtkPointWidget.h>
#include <vtkPolyData.h>
#include <vtkPolyDataMapper.h>
#include <vtkProperty.h>
#include <vtkRenderer.h>
#include <vtkRendererCollection.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkSmartPointer.h>
#include <vtkSphereSource.h>
#include <vtkTransform.h>


class axlActorSphereObserver : public vtkCommand
{
public:
    static axlActorSphereObserver *New(void)
    {
        return new axlActorSphereObserver;
    }

    virtual void Execute(vtkObject *caller, unsigned long event, void *)
    {
        vtkRenderWindowInteractor *interactor = observerDataAssembly->getInteractor();

        if(event == vtkCommand::InteractionEvent)
        {
            observerData_sphereSource->Update();
            observerData_sphereSource->Modified();
            interactor->Render();

            if(!pointWidget || !radiusWidget)
                return;

            if (caller == pointWidget)
            {
//                    axlPoint p1(pointWidget->GetPosition()[0], pointWidget->GetPosition()[1], pointWidget->GetPosition()[2]);
//                    axlPoint center(observerData_sphere->coordinates()[0], observerData_sphere->coordinates()[1], observerData_sphere->coordinates()[2]);

//                    if (!(axlPoint::distance(&p1, &center) < 0.0001)) {// There we move the point widget
                observerData_sphere->setCenter(pointWidget->GetPosition()[0], pointWidget->GetPosition()[1], pointWidget->GetPosition()[2]);
                observerDataAssembly->onUpdateGeometry();
                // we want to the radius widget to be positioned on the x axis of the point widget
                radiusWidget->SetPosition(pointWidget->GetPosition()[0] + observerData_sphere->radius() * 1.1, pointWidget->GetPosition()[1], pointWidget->GetPosition()[2]);

//                    }
            }
            else if (caller == radiusWidget)
            {// There we move the radius widget
                axlPoint center(observerData_sphere->coordinates()[0], observerData_sphere->coordinates()[1], observerData_sphere->coordinates()[2]);

                // we want to the radius widget to be positioned on the x axis of the point widget
                axlPoint c(radiusWidget->GetPosition()[0], pointWidget->GetPosition()[1], pointWidget->GetPosition()[2]);

                radiusWidget->SetPosition(c.coordinates());
                double r = axlPoint::distance(&c, &center);

                observerData_sphere->setRadius(0.9 * r);
                observerData_sphereSource->SetRadius(0.9 * r);
                observerData_sphereSource->Modified();
                observerData_sphereSource->Update();
                if(!observerData_sphere->fields().isEmpty())
                    observerData_sphere->touchField();
            }
        }

//        if(event == vtkCommand::MouseMoveEvent)
//        {

//            if(observerData_sphere)
//            {
//                // we need the matrix transform of this actor

//                vtkAssemblyPath *path;
//                vtkRenderWindow *renderWindow = interactor->GetRenderWindow();
//                vtkRendererCollection * rendererCollection = renderWindow->GetRenderers();
//                vtkRenderer *render = rendererCollection->GetFirstRenderer();
//                axlInteractorStyle = dynamic_cast<axlInteractorStyleSwitch *>(interactor->GetInteractorStyle());

//                int X = observerDataAssembly->getInteractor()->GetEventPosition()[0];
//                int Y = observerDataAssembly->getInteractor()->GetEventPosition()[1];

//                if (!render || !render->IsInViewport(X, Y))
//                {
//                    //qDebug()<<" No renderer or bad cursor coordonates";
//                }

//                axlSpherePicker->Pick(X,Y,0.0,render);
//                path = axlSpherePicker->GetPath();

//                if ( path != NULL)
//                {
//                    //qDebug()<< "axlActorSphereObserver::MouseMoveEvent";

//                    double *position = observerDataAssembly->GetPosition();
//                    (void)position;

//                    for(int j=0;j<3;j++)
//                    {
//                        //qDebug()<<"axlActorSphereObserver :: Execute "<<position[j];
//                        //observerData_sphere->coordinates()[j] = position[j];
//                    }
//                    //WARNING We considered there the scale is uniforn in all direction;
//                    // observerData_sphere->emitDataChanged();
//                }

//            }
//        }
    }

    axlInteractorStyleSwitch *axlInteractorStyle;
//    vtkCellPicker *axlSpherePicker;
    axlSphere *observerData_sphere;
    axlActorSphere *observerDataAssembly;
    vtkSphereSource *observerData_sphereSource;
    
    vtkPointWidget *pointWidget = nullptr;
    vtkPointWidget *radiusWidget = nullptr;
};

// /////////////////////////////////////////////////////////////////
// axlActorSpherePrivate
// /////////////////////////////////////////////////////////////////

class axlActorSpherePrivate
{
public:
    axlSphere *sphere;
    vtkSphereSource *sphereSource;
    vtkPointWidget *pointWidget;
    vtkPointWidget *radiusWidget;
    axlActorSphereObserver *sphereObserver;
    QVTKOpenGLWidget *widget;
//    vtkCellPicker *axlSpherePicker ;

};

#if (VTK_MAJOR_VERSION <= 5)
vtkCxxRevisionMacro(axlActorSphere, "$Revision: 0.0.1 $");
#endif

vtkStandardNewMacro(axlActorSphere);

dtkAbstractData *axlActorSphere::data(void)
{
    return d->sphere;
}

vtkSphereSource *axlActorSphere::sphere(void)
{
    return d->sphereSource;
}

void axlActorSphere::setQVTKWidget(QVTKOpenGLWidget *widget)
{
    d->widget = widget;
}


void axlActorSphere::setData(dtkAbstractData *sphere1)
{
    axlSphere *sphere = dynamic_cast<axlSphere *>(sphere1);
    if(sphere)
    {
        d->sphere = sphere;
        this->setMapper(vtkSmartPointer<vtkPolyDataMapper>::New());
        this->setActor(vtkSmartPointer<vtkActor>::New());

        d->sphereSource = vtkSphereSource::New();
        d->sphereSource->SetPhiResolution(20);
        d->sphereSource->SetThetaResolution(20);
        d->sphereSource->SetRadius(d->sphere->radius());
        d->sphereSource->SetCenter( d->sphere->coordinates());

        this->onUpdateGeometry();
        // connection of data to actor
#if (VTK_MAJOR_VERSION <= 5)
        this->getMapper()->SetInput(d->sphereSource->GetOutput());
#else
        this->getMapper()->SetInputData(d->sphereSource->GetOutput());
#endif
        this->getActor()->SetMapper(this->getMapper());

        this->AddPart(this->getActor());

        if(!d->sphereObserver)
        {
//            d->axlSpherePicker = vtkCellPicker::New();
            d->sphereObserver = axlActorSphereObserver::New() ;
            this->getInteractor()->AddObserver(vtkCommand::InteractionEvent, d->sphereObserver);
            this->getInteractor()->AddObserver(vtkCommand::MouseMoveEvent, d->sphereObserver);

            d->sphereObserver->observerDataAssembly = this;
//            d->sphereObserver->axlSpherePicker = d->axlSpherePicker;
            d->sphereObserver->observerData_sphereSource = d->sphereSource;
            d->sphereObserver->observerData_sphere = d->sphere;
            d->sphereObserver->axlInteractorStyle = dynamic_cast<axlInteractorStyleSwitch *> (this->getInteractor()->GetInteractorStyle());
        }


        QColor color = d->sphere->color();
        this->getActor()->GetProperty()->SetColor(color.redF(), color.greenF(), color.blueF());

        QString shader = d->sphere->shader();
        if(!shader.isEmpty())
            this->setShader(shader);

        //this->getActor()->SetScale(d->sphere->radius());

        this->setOpacity(d->sphere->opacity());

        // refresh the actor
        this->onUpdateGeometry();

        // signals connecting
        connect(d->sphere, SIGNAL(modifiedGeometry()), this, SLOT(onUpdateGeometry()));
        connect(d->sphere, SIGNAL(modifiedProperty()), this, SLOT(onUpdateProperty()));
    }
    else
        qDebug()<< "no axlSphere available";


}

void axlActorSphere::setDisplay(bool display)
{
    axlActor::setDisplay(display);

    if(display && d->pointWidget){
        this->showSphereWidget(true);
    }

    if(!display && d->pointWidget){
        this->showSphereWidget(false);
    }
}

void axlActorSphere::showSphereWidget(bool show)
{
    if(!d->pointWidget || !d->radiusWidget) {
        qDebug() << "No tet actor computed for this axlActorCylinder.";
        return;
    }

    if(show){
        d->pointWidget->SetEnabled(1);
        d->radiusWidget->SetEnabled(1);

    }

    if(!show){
        d->pointWidget->SetEnabled(0);
        d->radiusWidget->SetEnabled(1);

    }
}

void axlActorSphere::setSphereWidget(bool sphereWidget)
{

    if(sphereWidget) {
        if(!d->pointWidget  || !d->radiusWidget)
        {
            //widget drawing
            d->pointWidget = vtkPointWidget::New();
            d->pointWidget->SetInteractor(this->getInteractor());
            d->pointWidget->SetProp3D(this->getActor());
            d->pointWidget->PlaceWidget();
            d->pointWidget->AllOff();
            d->pointWidget->SetTranslationMode(1);

            d->pointWidget->SetPosition(d->sphereSource->GetCenter());

            d->radiusWidget = vtkPointWidget::New();
            d->radiusWidget->SetInteractor(this->getInteractor());
            d->radiusWidget->SetProp3D(this->getActor());
            d->radiusWidget->PlaceWidget();
            d->radiusWidget->AllOff();
            d->radiusWidget->SetTranslationMode(1);

            // we want to the radius widget to be positioned on the x axis of the point widget
            d->radiusWidget->SetPosition(d->pointWidget->GetPosition()[0] + d->sphere->radius() * 1.1 , d->pointWidget->GetPosition()[1], d->pointWidget->GetPosition()[2]);

        }

        if(d->sphereObserver)
        {
            d->pointWidget->AddObserver(vtkCommand::InteractionEvent, d->sphereObserver);
            d->radiusWidget->AddObserver(vtkCommand::InteractionEvent, d->sphereObserver);

            d->sphereObserver->pointWidget = d->pointWidget;
            d->sphereObserver->radiusWidget = d->radiusWidget;

        }

        // there is always the controlPoints there
        d->pointWidget->SetEnabled(true);
        d->radiusWidget->SetEnabled(true);

    }
    else // (!pointWidget)
    {
        if (this->getActor()) {
            // this->getMapper()->SetInput(this->getPolyData());

            if(d->pointWidget)
            {
                d->pointWidget->RemoveAllObservers();
                d->pointWidget->SetEnabled(false);
                d->pointWidget->Delete(); // warning not sure
                d->pointWidget = NULL;
            }

            if(d->radiusWidget)
            {
                d->radiusWidget->RemoveAllObservers();
                d->radiusWidget->SetEnabled(false);
                d->radiusWidget->Delete(); // warning not sure
                d->radiusWidget = NULL;
            }
        }
    }
    if (d->sphereObserver){
        d->sphereObserver->pointWidget = d->pointWidget;
        d->sphereObserver->radiusWidget = d->radiusWidget;
    }
}

bool axlActorSphere::isShowSphereWidget(void)
{

    if(!d->pointWidget  || !d->radiusWidget) {
        qDebug() << "No tet actor computed for this axlActorBSpline.";
        return false;
    }

    return  d->pointWidget->GetEnabled()  && d->radiusWidget->GetEnabled();
}


void axlActorSphere::setMode(int state)
{
    this->onModeChanged(state);

    emit stateChanged(this->data(), state);
}

void axlActorSphere::onModeChanged(int state)
{
    if(state == 0)
    {
        this->setState(axlActor::passive);
        this->getActor()->SetPickable(1);

        if(axlAbstractData *data = dynamic_cast<axlAbstractData *>(this->data()))
        {
            QColor color = data->color();
            this->getActor()->GetProperty()->SetColor(color.redF(), color.greenF(), color.blueF());
        }
        this->setSphereWidget(false);

    }
    else if(state == 1)
    {

        this->setState(axlActor::selection);
        this->getActor()->SetPickable(1);
        vtkProperty *prop = this->getActor()->GetProperty();

        if(axlAbstractData *data = dynamic_cast<axlAbstractData *>(this->data()))
        {
            QColor color = data->color();
            qreal *h = new qreal(0.0);
            qreal *s = new qreal(0.0);
            qreal *l = new qreal(0.0);
            color.getHslF(h, s, l);
            color.setHslF(*h, *s, *l + (1.0 - *l) / 2.0);
            prop->SetColor(color.redF(), color.greenF(), color.blueF());
            delete l;
            delete s;
            delete h;
        }
        this->setSphereWidget(false);
    }
    else if(state == 2)
    {
        this->setState(axlActor::edition);
        this->getActor()->SetPickable(0);
        vtkProperty *prop = this->getActor()->GetProperty();
        if(axlAbstractData *data = dynamic_cast<axlAbstractData *>(this->data()))
        {
            QColor color = data->color();
            qreal *h = new qreal(0.0);
            qreal *s = new qreal(0.0);
            qreal *l = new qreal(0.0);
            color.getHslF(h, s, l);
            color.setHslF(*h, *s, *l + (1.0 - *l)/2.0);
            prop->SetColor(color.redF(), color.greenF(), color.blueF());
            delete l;
            delete s;
            delete h;
        }
        this->setSphereWidget(true);
    }

    this->Modified();
}


void axlActorSphere::onRemoved()
{
    // if on edit mode, change to selection mode (for stability)
    if (this->getState() == 2)
        setMode(1);
    
    //remove sphere specificity
    if(d->sphereObserver)
    {
        this->getInteractor()->RemoveObservers(vtkCommand::InteractionEvent, d->sphereObserver);
        this->getInteractor()->RemoveObservers(vtkCommand::MouseMoveEvent, d->sphereObserver);
        d->sphereObserver->observerDataAssembly = NULL;
//        d->sphereObserver->axlSpherePicker = NULL;
        d->sphereObserver->observerData_sphereSource = NULL;
        d->sphereObserver->observerData_sphere = NULL;
        d->sphereObserver->axlInteractorStyle = NULL;
        d->sphereObserver->Delete();
        d->sphereObserver = NULL;

//        d->axlSpherePicker->Delete();
//        d->axlSpherePicker = NULL;

    }
    if(d->sphereSource)
    {
        d->sphereSource->Delete();
        d->sphereSource = NULL;
    }
    if(d->widget)
    {
        d->widget = NULL;
    }
    if(d->sphere)
    {
        d->sphere = NULL;
    }
    if(d->pointWidget && d->radiusWidget)
    {
        this->setSphereWidget(false);
        d->pointWidget = NULL;
        d->radiusWidget = NULL;
    }

    //remove actor specificity
    this->RemoveAllObservers();
    this->RemovePart(this->getActor());
    this->getActor()->RemoveAllObservers();

    if(axlActorComposite *actorComposite = dynamic_cast<axlActorComposite *>(this->parent()) )
        actorComposite->removeActorReference(this);
}

void axlActorSphere::onUpdateGeometry(void)
{
    d->sphereSource->SetCenter(d->sphere->coordinates());
    d->sphereSource->SetRadius(d->sphere->radius());
    d->sphereSource->Modified();
    d->sphereSource->Update();
    if(d->pointWidget)
        d->pointWidget->SetPosition(d->sphereSource->GetCenter());

    if(!d->sphere->fields().isEmpty())
        d->sphere->touchField();

    if(d->sphere->updateView())
        emit updated();
}


axlActorSphere::axlActorSphere(void) : axlActor(), d(new axlActorSpherePrivate)
{
    d->sphere = NULL;
    d->pointWidget = NULL;
    d->radiusWidget = NULL;
//    d->axlSpherePicker = NULL;
    d->sphereObserver = NULL;
    d->sphereSource =NULL;
    d->widget = NULL;
}

axlActorSphere::~axlActorSphere(void)
{
    delete d;

    d = NULL;
}

axlAbstractActor *createAxlActorSphere(void){

    return axlActorSphere::New();
}
