==========
Axl tests
==========

*Anais Ducoffe*

Writing a class test will help you to organize better your programs. It helps to implement you own objects, algorithms ...You can also easily check that your programs have the appropriate behaviour, even if you can't display results on screen.

Moreover, it enables to make sure that what you added to axel software didn't break any of its functionalities. That's why it is recommanded to systematically use tests.

How to use axel tests ?
=======================

First you must change the variable Test value (TEST = ON) of your Axl CMake file::

  cmake . -DTEST=ON
  
When compiling Axl, binary file(s) will be created in the tests folder of the ``build_axel`` directory.

To run those tests you have three main possibilities:

- execute in ``path_to_build_axel`` the binary file(s)::

       ./axel/tests/axlCore/axlCoreTests
     
- or type in ``path_to_build_axel``::

       ctest

- or type in ``path_to_build_axel``::

       make test

The two lattest possibilities enable to run all test suites.

.. image:: ../img/content_ctestrun.png
   :align: center
   
If you want to run just one test class, or one test of one class, just execute the binary file which contains it.

.. image:: ../img/content_testdynamidata.png
   :align: center

How to create new Axl tests ?
==============================
You can easily create new test classes by using ``dtkTestGenerator``::

  ./path_to_build_axel/dtk/bin/dtkTestGenerator

Enter the name of the class you want to test. The directory of the tests files must be chosen in the tests folder of Axl. Then click on file-> Generate. The header file and the source file are automatically created.

.. image:: ../img/content_dtktestgenerator.png
   :align: center
      
Open the header file. The structure will always be the same. The test class can contain several tests. Those tests are slots , and their name always begins with test. initTestCase and cleanupTestCase will respectively be called before the first test of the file and after the last one. init and cleanup are called before and after each test of the file.

.. image:: ../img/content_axlpointtestheader.png
   :align: center
	   
Tests use `QtTest Macros <http://doc.qt.io/qt-5/qttest-index.html>`_.

.. image:: ../img/content_qttestuse.png
   :align: center
	   
If you need more information you can use the following link : http://dtk.inria.fr/guides/test
