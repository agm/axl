/* axlVolumeDiscreteWriter.cpp ---
 *
 * Author: Anais Ducoffe
 * Copyright (C) 2013 - Anais Ducoffe, Inria.
 */

/* Commentary:
 *
 */

/* Change log:
 *
 */

#include "axlVolumeDiscreteWriter.h"

#include "axlVolumeDiscrete.h"

#include <dtkCoreSupport/dtkAbstractData.h>
#include <dtkCoreSupport/dtkAbstractDataFactory.h>

#include "axlVtkViewPlugin.h"

// /////////////////////////////////////////////////////////////////
// axlVolumeDiscreteWriter
// /////////////////////////////////////////////////////////////////

axlVolumeDiscreteWriter::axlVolumeDiscreteWriter(void)
{
    this->setObjectName(this->description());
}

axlVolumeDiscreteWriter::~axlVolumeDiscreteWriter(void)
{

}

QString axlVolumeDiscreteWriter::identifier(void) const
{
    return "axlVolumeDiscreteWriter";
}

QString axlVolumeDiscreteWriter::description(void) const
{
    return "axlVolumeDiscreteWriter";
}

QStringList axlVolumeDiscreteWriter::handled(void) const
{
    return QStringList() << "axlVolumeDiscrete";
}

bool axlVolumeDiscreteWriter::registered(void)
{
    return axlVtkViewPlugin::dataFactSingleton->registerDataWriterType("axlVolumeDiscreteWriter", QStringList(), createaxlVolumeDiscreteWriter);
}

bool axlVolumeDiscreteWriter::accept(dtkAbstractData *data)
{
    axlVolumeDiscrete *volume = dynamic_cast<axlVolumeDiscrete *>(data);
    if(volume)
        return true;

    return false;
}

bool axlVolumeDiscreteWriter::reject(dtkAbstractData *data)
{
    return !this->accept(data);
}

QDomElement axlVolumeDiscreteWriter::write(QDomDocument *doc, dtkAbstractData *data)
{
    axlVolumeDiscrete *volume = dynamic_cast<axlVolumeDiscrete *>(data);

    /////Name
    QDomElement volumeElement = doc->createElement("volume");

    volumeElement.setAttribute("type","discrete");
    volumeElement.setAttribute("name",volume->name());


    //Write grid dimensions
    QDomElement dimension = doc->createElement("dimension");
    QString dimensionValue;
    QTextStream(&dimensionValue) << QString::number(volume->xDimension()) << " "<< QString::number(volume->yDimension()) << " "<< QString::number(volume->zDimension());

    QDomText dimensionDomText = doc->createTextNode(dimensionValue);

    dimension.appendChild(dimensionDomText);
    volumeElement.appendChild(dimension);


    //Write scalar values
    QDomElement values = doc->createElement("values");
    int nx = volume->xDimension();
    int ny = volume->yDimension();
    int nz = volume->zDimension();
    QString scalarValues;
    for(int i = 0;i < nx;i++){
        for(int j = 0;j < ny;j++){
            for(int k = 0;k < nz;k++){

                QTextStream(&scalarValues)<< "\n" << QString::number(volume->getValue(i,j,k)) ;
            }
        }
    }

    QTextStream(&scalarValues) << "\n";
    QDomText scalarDomText = doc->createTextNode(scalarValues);

    values.appendChild(scalarDomText);
    volumeElement.appendChild(values);



    return volumeElement;
}


QDomElement axlVolumeDiscreteWriter::elementByWriter(axlAbstractDataWriter *axl_writer, QDomDocument *doc, dtkAbstractData *data)
{
    QDomElement element;

    if(!axl_writer)
        return element;

    if(!axl_writer->accept(data))
        return element;

    element = axl_writer->write(doc, data);

    return element;
}

dtkAbstractDataWriter *createaxlVolumeDiscreteWriter(void)
{
    return new axlVolumeDiscreteWriter;
}
