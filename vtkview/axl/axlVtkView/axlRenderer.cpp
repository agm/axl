/* axlRenderer.cpp ---
 *
 * Author: Meriadeg Perrinel
 * Copyright (C) 2008 - Meriadeg Perrinel, Inria.
 * Created: Tue Nov  9 16:58:59 2010 (+0100)
 * Version: $Id$
 * Last-Updated: Tue Nov  9 17:09:38 2010 (+0100)
 *           By: Meriadeg Perrinel
 *     Update #: 19
 */

/* Commentary:
 *
 */

/* Change log:
 *
 */


#include "axlRenderer.h"


#include <vtkObjectFactory.h>

#include "vtkCamera.h"
#include "vtkCommand.h"
#include "vtkGraphicsFactory.h"
#include "vtkHardwareSelector.h"
#include "vtkMath.h"
#include "vtkRendererDelegate.h"
#include "vtkRenderPass.h"
#include "vtkRenderState.h"
#include "vtkRenderWindow.h"
#include "vtkTexture.h"

#include <QtGui>


//----------------------------------------------------------------------------
// Needed when we don't use the vtkStandardNewMacro.
//vtkStandardNewMacro(axlRenderer);
//----------------------------------------------------------------------------

//vtkStandardNewMacro(axlRenderer);


class axlRendererPrivate
{
public:

    bool isGrid;
};

axlRenderer::axlRenderer() : d(new axlRendererPrivate)
{
    qDebug()<<"axlRenderer::New()";
    d->isGrid = false;
}
axlRenderer::~axlRenderer()
{

}

void axlRenderer::useGrid(bool use)
{
    d->isGrid = use;
}

bool axlRenderer::grid(void)
{
   return d->isGrid;
}
void axlRenderer::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
}


//// Automatically set up the camera based on a specified bounding box
//// (xmin,xmax, ymin,ymax, zmin,zmax). Camera will reposition itself so
//// that its focal point is the center of the bounding box, and adjust its
//// distance and position to preserve its initial view plane normal
//// (i.e., vector defined from camera position to focal point). Note: if
//// the view plane is parallel to the view up axis, the view up axis will
//// be reset to one of the three coordinate axes.
//void vtkRenderer::ResetCamera(double bounds[6])
//{
//    qDebug()<< "axlRenderer::ResetCamera ";
//  double center[3];
//  double distance;
//  double vn[3], *vup;

//  this->GetActiveCamera();
//  if ( this->ActiveCamera != NULL )
//    {
//    this->ActiveCamera->GetViewPlaneNormal(vn);
//    }
//  else
//    {
//    vtkErrorMacro(<< "Trying to reset non-existant camera");
//    return;
//    }

//  center[0] = (bounds[0] + bounds[1])/2.0;
//  center[1] = (bounds[2] + bounds[3])/2.0;
//  center[2] = (bounds[4] + bounds[5])/2.0;

//  double w1 = bounds[1] - bounds[0];
//  double w2 = bounds[3] - bounds[2];
//  double w3 = bounds[5] - bounds[4];
//  w1 *= w1;
//  w2 *= w2;
//  w3 *= w3;
//  double radius = w1 + w2 + w3;

//  // If we have just a single point, pick a radius of 1.0
//  radius = (radius==0)?(1.0):(radius);

//  // compute the radius of the enclosing sphere
//  radius = sqrt(radius)*0.5;

//  // default so that the bounding sphere fits within the view fustrum

//  // compute the distance from the intersection of the view frustum with the
//  // bounding sphere. Basically in 2D draw a circle representing the bounding
//  // sphere in 2D then draw a horizontal line going out from the center of
//  // the circle. That is the camera view. Then draw a line from the camera
//  // position to the point where it intersects the circle. (it will be tangent
//  // to the circle at this point, this is important, only go to the tangent
//  // point, do not draw all the way to the view plane). Then draw the radius
//  // from the tangent point to the center of the circle. You will note that
//  // this forms a right triangle with one side being the radius, another being
//  // the target distance for the camera, then just find the target dist using
//  // a sin.
//  double angle=vtkMath::RadiansFromDegrees(this->ActiveCamera->GetViewAngle());
//  double parallelScale=radius;

//  this->ComputeAspect();
//  double aspect[2];
//  this->GetAspect(aspect);

//  if(aspect[0]>=1.0) // horizontal window, deal with vertical angle|scale
//    {
//    if(this->ActiveCamera->GetUseHorizontalViewAngle())
//      {
//      angle=2.0*atan(tan(angle*0.5)/aspect[0]);
//      }
//    }
//  else // vertical window, deal with horizontal angle|scale
//    {
//    if(!this->ActiveCamera->GetUseHorizontalViewAngle())
//      {
//      angle=2.0*atan(tan(angle*0.5)*aspect[0]);
//      }

//    parallelScale=parallelScale/aspect[0];
//    }

//  distance =radius/sin(angle*0.5);

//  // check view-up vector against view plane normal
//  vup = this->ActiveCamera->GetViewUp();
//  if ( fabs(vtkMath::Dot(vup,vn)) > 0.999 )
//    {
//    vtkWarningMacro(<<"Resetting view-up since view plane normal is parallel");
//    this->ActiveCamera->SetViewUp(-vup[2], vup[0], vup[1]);
//    }

//  // update the camera
//  this->ActiveCamera->SetFocalPoint(center[0],center[1],center[2]);
//  this->ActiveCamera->SetPosition(center[0]+distance*vn[0],
//                                  center[1]+distance*vn[1],
//                                  center[2]+distance*vn[2]);
//  bounds[0] = -500;
//  bounds[1] = +500;
//  bounds[2] = -500;
//  bounds[3] = +500;
//  bounds[4] = -500;
//  bounds[5] = +500;
//  this->ResetCameraClippingRange( bounds );

//  // setup default parallel scale
//  this->ActiveCamera->SetParallelScale(parallelScale);
//}


//// Automatically set up the camera based on the visible actors.
//// The camera will reposition itself to view the center point of the actors,
//// and move along its initial view plane normal (i.e., vector defined from
//// camera position to focal point) so that all of the actors can be seen.
////void vtkRenderer::ResetCamera()
////{
////    qDebug()<< "axlRenderer::ResetCamera 2 ";

////  double      allBounds[6];

////  this->ComputeVisiblePropBounds( allBounds );

////  if (!vtkMath::AreBoundsInitialized(allBounds))
////    {
////    vtkDebugMacro( << "Cannot reset camera!" );
////    }
////  else
////    {
////    this->ResetCamera(allBounds);
////    }

////  // Here to let parallel/distributed compositing intercept
////  // and do the right thing.
////  this->InvokeEvent(vtkCommand::ResetCameraEvent,this);
////}


////// Alternative version of ResetCamera(bounds[6]);
////void vtkRenderer::ResetCamera(double xmin, double xmax,
////                              double ymin, double ymax,
////                              double zmin, double zmax)
////{
////  double bounds[6];

////  bounds[0] = xmin;
////  bounds[1] = xmax;
////  bounds[2] = ymin;
////  bounds[3] = ymax;
////  bounds[4] = zmin;
////  bounds[5] = zmax;

////  this->ResetCamera(bounds);
////}


//// Reset the camera clipping range to include this entire bounding box
////void vtkRenderer::ResetCameraClippingRange( double bounds[6] )
////{
////    std::cout<< " reset vtk Camera"<<std::endl;
////  double  vn[3], position[3], a, b, c, d;
////  double  range[2], dist;
////  int     i, j, k;

////  // Don't reset the clipping range when we don't have any 3D visible props
////  if (!vtkMath::AreBoundsInitialized(bounds))
////    {
////    return;
////    }

////  this->GetActiveCameraAndResetIfCreated();
////  if ( this->ActiveCamera == NULL )
////    {
////    vtkErrorMacro(<< "Trying to reset clipping range of non-existant camera");
////    return;
////    }

////  // Find the plane equation for the camera view plane
////  this->ActiveCamera->GetViewPlaneNormal(vn);
////  this->ActiveCamera->GetPosition(position);
////  a = -vn[0];
////  b = -vn[1];
////  c = -vn[2];
////  d = -(a*position[0] + b*position[1] + c*position[2]);

////  // Set the max near clipping plane and the min far clipping plane
////  range[0] = a*bounds[0] + b*bounds[2] + c*bounds[4] + d;
////  range[1] = 1e-18;

////  // Find the closest / farthest bounding box vertex
////  for ( k = 0; k < 2; k++ )
////    {
////    for ( j = 0; j < 2; j++ )
////      {
////      for ( i = 0; i < 2; i++ )
////        {
////        dist = a*bounds[i] + b*bounds[2+j] + c*bounds[4+k] + d;
////        range[0] = (dist<range[0])?(dist):(range[0]);
////        range[1] = (dist>range[1])?(dist):(range[1]);
////        }
////      }
////    }

////  // Do not let the range behind the camera throw off the calculation.
////  if (range[0] < 0.0)
////    {
////    range[0] = 0.0;
////    }

////  // Give ourselves a little breathing room
////  range[0] = 0.99*range[0] - (range[1] - range[0])*0.5;
////  range[1] = 1.01*range[1] + (range[1] - range[0])*0.5;

////  // Make sure near is not bigger than far
////  range[0] = (range[0] >= range[1])?(0.01*range[1]):(range[0]);

////  // Make sure near is at least some fraction of far - this prevents near
////  // from being behind the camera or too close in front. How close is too
////  // close depends on the resolution of the depth buffer
////  if (!this->NearClippingPlaneTolerance)
////    {
////    this->NearClippingPlaneTolerance = 0.01;
////    if (this->RenderWindow)
////      {
////      int ZBufferDepth = this->RenderWindow->GetDepthBufferSize();
////      if ( ZBufferDepth > 16 )
////        {
////        this->NearClippingPlaneTolerance = 0.001;
////        }
////      }
////    }

////  // make sure the front clipping range is not too far from the far clippnig
////  // range, this is to make sure that the zbuffer resolution is effectively
////  // used
////  if (range[0] < this->NearClippingPlaneTolerance*range[1])
////    {
////    range[0] = this->NearClippingPlaneTolerance*range[1];
////    }

////  this->ActiveCamera->SetClippingRange( range );
////}

//// Automatically set the clipping range of the camera based on the
//// visible actors
//void vtkRenderer::ResetCameraClippingRange()
//{
//  double      allBounds[6];

//  this->ComputeVisiblePropBounds( allBounds );

//  allBounds[0] = -500;
//  allBounds[1] = +500;
//  allBounds[2] = -500;
//  allBounds[3] = +500;
//  allBounds[4] = -500;
//  allBounds[5] = +500;

//  if (!vtkMath::AreBoundsInitialized(allBounds))
//    {
//    vtkDebugMacro( << "Cannot reset camera clipping range!" );
//    }
//  else
//    {
//    this->ResetCameraClippingRange(allBounds);
//    }

//  // Here to let parallel/distributed compositing intercept
//  // and do the right thing.
//  this->InvokeEvent(vtkCommand::ResetCameraClippingRangeEvent,this);
//}

//////////////////////////////////////////////////////////////////////////////////////



// Automatically set up the camera based on a specified bounding box
// (xmin,xmax, ymin,ymax, zmin,zmax). Camera will reposition itself so
// that its focal point is the center of the bounding box, and adjust its
// distance and position to preserve its initial view plane normal
// (i.e., vector defined from camera position to focal point). Note: if
// the view plane is parallel to the view up axis, the view up axis will
// be reset to one of the three coordinate axes.
void axlRenderer::ResetCamera(double bounds[6])
{
    qDebug()<< "axlRenderer::ResetCamera ";
  double center[3];
  double distance;
  double vn[3], *vup;

  this->GetActiveCamera();
  if ( this->ActiveCamera != NULL )
    {
    this->ActiveCamera->GetViewPlaneNormal(vn);
    }
  else
    {
    vtkErrorMacro(<< "Trying to reset non-existant camera");
    return;
    }

  center[0] = (bounds[0] + bounds[1])/2.0;
  center[1] = (bounds[2] + bounds[3])/2.0;
  center[2] = (bounds[4] + bounds[5])/2.0;

  double w1 = bounds[1] - bounds[0];
  double w2 = bounds[3] - bounds[2];
  double w3 = bounds[5] - bounds[4];
  w1 *= w1;
  w2 *= w2;
  w3 *= w3;
  double radius = w1 + w2 + w3;

  // If we have just a single point, pick a radius of 1.0
  radius = (radius==0)?(1.0):(radius);

  // compute the radius of the enclosing sphere
  radius = sqrt(radius)*0.5;

  // default so that the bounding sphere fits within the view fustrum

  // compute the distance from the intersection of the view frustum with the
  // bounding sphere. Basically in 2D draw a circle representing the bounding
  // sphere in 2D then draw a horizontal line going out from the center of
  // the circle. That is the camera view. Then draw a line from the camera
  // position to the point where it intersects the circle. (it will be tangent
  // to the circle at this point, this is important, only go to the tangent
  // point, do not draw all the way to the view plane). Then draw the radius
  // from the tangent point to the center of the circle. You will note that
  // this forms a right triangle with one side being the radius, another being
  // the target distance for the camera, then just find the target dist using
  // a sin.
  double angle=vtkMath::RadiansFromDegrees(this->ActiveCamera->GetViewAngle());
  double parallelScale=radius;

  this->ComputeAspect();
  double aspect[2];
  this->GetAspect(aspect);

  if(aspect[0]>=1.0) // horizontal window, deal with vertical angle|scale
    {
    if(this->ActiveCamera->GetUseHorizontalViewAngle())
      {
      angle=2.0*atan(tan(angle*0.5)/aspect[0]);
      }
    }
  else // vertical window, deal with horizontal angle|scale
    {
    if(!this->ActiveCamera->GetUseHorizontalViewAngle())
      {
      angle=2.0*atan(tan(angle*0.5)*aspect[0]);
      }

    parallelScale=parallelScale/aspect[0];
    }

  distance =radius/sin(angle*0.5);

  // check view-up vector against view plane normal
  vup = this->ActiveCamera->GetViewUp();
  if ( fabs(vtkMath::Dot(vup,vn)) > 0.999 )
    {
    vtkWarningMacro(<<"Resetting view-up since view plane normal is parallel");
    this->ActiveCamera->SetViewUp(-vup[2], vup[0], vup[1]);
    }

  // update the camera
  this->ActiveCamera->SetFocalPoint(center[0],center[1],center[2]);
  this->ActiveCamera->SetPosition(center[0]+distance*vn[0],
                                  center[1]+distance*vn[1],
                                  center[2]+distance*vn[2]);

  if(d->isGrid)
  {
      bounds[0] = -500;
      bounds[1] = +500;
      bounds[2] = -500;
      bounds[3] = +500;
      bounds[4] = -500;
      bounds[5] = +500;
  }

  this->ResetCameraClippingRange( bounds );

  // setup default parallel scale
  this->ActiveCamera->SetParallelScale(parallelScale);
}


// Automatically set up the camera based on the visible actors.
// The camera will reposition itself to view the center point of the actors,
// and move along its initial view plane normal (i.e., vector defined from
// camera position to focal point) so that all of the actors can be seen.
void axlRenderer::ResetCamera()
{
    qDebug()<< "axlRenderer::ResetCamera 2 ";

  double      allBounds[6];

  this->ComputeVisiblePropBounds( allBounds );

  if (!vtkMath::AreBoundsInitialized(allBounds))
    {
    vtkDebugMacro( << "Cannot reset camera!" );
    }
  else
    {
    this->ResetCamera(allBounds);
    }

  // Here to let parallel/distributed compositing intercept
  // and do the right thing.
  this->InvokeEvent(vtkCommand::ResetCameraEvent,this);
}


// Alternative version of ResetCamera(bounds[6]);
void axlRenderer::ResetCamera(double xmin, double xmax,
                              double ymin, double ymax,
                              double zmin, double zmax)
{
  double bounds[6];

  bounds[0] = xmin;
  bounds[1] = xmax;
  bounds[2] = ymin;
  bounds[3] = ymax;
  bounds[4] = zmin;
  bounds[5] = zmax;

  this->ResetCamera(bounds);
}


// Reset the camera clipping ranger to include this entire bounding box
void axlRenderer::ResetCameraClippingRange( double bounds[6] )
{
    std::cout<< " reset vtk Camera"<<std::endl;
  double  vn[3], position[3], a, b, c, d;
  double  range[2], dist;
  int     i, j, k;

  // Don't reset the clipping range when we don't have any 3D visible props
  if (!vtkMath::AreBoundsInitialized(bounds))
    {
    return;
    }

  this->GetActiveCameraAndResetIfCreated();
  if ( this->ActiveCamera == NULL )
    {
    vtkErrorMacro(<< "Trying to reset clipping range of non-existant camera");
    return;
    }

  // Find the plane equation for the camera view plane
  this->ActiveCamera->GetViewPlaneNormal(vn);
  this->ActiveCamera->GetPosition(position);
  a = -vn[0];
  b = -vn[1];
  c = -vn[2];
  d = -(a*position[0] + b*position[1] + c*position[2]);

  // Set the max near clipping plane and the min far clipping plane
  range[0] = a*bounds[0] + b*bounds[2] + c*bounds[4] + d;
  range[1] = 1e-18;

  // Find the closest / farthest bounding box vertex
  for ( k = 0; k < 2; k++ )
    {
    for ( j = 0; j < 2; j++ )
      {
      for ( i = 0; i < 2; i++ )
        {
        dist = a*bounds[i] + b*bounds[2+j] + c*bounds[4+k] + d;
        range[0] = (dist<range[0])?(dist):(range[0]);
        range[1] = (dist>range[1])?(dist):(range[1]);
        }
      }
    }

  // Do not let the range behind the camera throw off the calculation.
  if (range[0] < 0.0)
    {
    range[0] = 0.0;
    }

  // Give ourselves a little breathing room
  range[0] = 0.99*range[0] - (range[1] - range[0])*0.5;
  range[1] = 1.01*range[1] + (range[1] - range[0])*0.5;

  // Make sure near is not bigger than far
  range[0] = (range[0] >= range[1])?(0.01*range[1]):(range[0]);

  // Make sure near is at least some fraction of far - this prevents near
  // from being behind the camera or too close in front. How close is too
  // close depends on the resolution of the depth buffer
  if (!this->NearClippingPlaneTolerance)
    {
    this->NearClippingPlaneTolerance = 0.01;
    if (this->RenderWindow)
      {
      int ZBufferDepth = this->RenderWindow->GetDepthBufferSize();
      if ( ZBufferDepth > 16 )
        {
        this->NearClippingPlaneTolerance = 0.001;
        }
      }
    }

  // make sure the front clipping range is not too far from the far clippnig
  // range, this is to make sure that the zbuffer resolution is effectively
  // used
  if (range[0] < this->NearClippingPlaneTolerance*range[1])
    {
    range[0] = this->NearClippingPlaneTolerance*range[1];
    }

  this->ActiveCamera->SetClippingRange( range );
}

// Automatically set the clipping range of the camera based on the
// visible actors
void axlRenderer::ResetCameraClippingRange()
{
  double      allBounds[6];

  this->ComputeVisiblePropBounds( allBounds );

  if(d->isGrid)
  {
      allBounds[0] = -500;
      allBounds[1] = +500;
      allBounds[2] = -500;
      allBounds[3] = +500;
      allBounds[4] = -500;
      allBounds[5] = +500;
  }

  if (!vtkMath::AreBoundsInitialized(allBounds))
    {
    vtkDebugMacro( << "Cannot reset camera clipping range!" );
    }
  else
    {
    this->ResetCameraClippingRange(allBounds);
    }

  // Here to let parallel/distributed compositing intercept
  // and do the right thing.
  this->InvokeEvent(vtkCommand::ResetCameraClippingRangeEvent,this);
}




//// return the correct type of Renderer
//axlRenderer *axlRenderer::New()
//{
//  // First try to create the object from the vtkObjectFactory
//  //vtkObject* ret = vtkObjectFactory::CreateInstance("axlRenderer");
//  return new axlRenderer;//static_cast<axlRenderer *>(ret);
//}
