/* axlActorEllipsoid.h ---
 *
 * Author: Meriadeg Perrinel
 * Copyright (C) 2008 - Meriadeg Perrinel, Inria.
 * Created: Fri Dec 17 10:58:38 2010 (+0100)
 * Version: $Id$
 * Last-Updated: Mon Dec 17 14:10:14 2012 (+0100)
 *           By: Julien Wintz
 *     Update #: 6
 */

/* Commentary:
 *
 */

/* Change log:
 *
 */

#ifndef AXLACTORELLIPSOID_H
#define AXLACTORELLIPSOID_H

#include "axlActor.h"

#include <dtkMathSupport/dtkQuaternion.h>
#include <QVTKOpenGLWidget.h>

#include "axlVtkViewPluginExport.h"

#include <vtkVersion.h>

class axlEllipsoid;
class vtkParametricEllipsoid;
class axlActorEllipsoidPrivate;

class AXLVTKVIEWPLUGIN_EXPORT axlActorEllipsoid : public axlActor
{
public:
#if (VTK_MAJOR_VERSION <= 5)
    vtkTypeRevisionMacro(axlActorEllipsoid, vtkAssembly)
#endif
    static axlActorEllipsoid *New(void);

public:
    dtkAbstractData *data(void);

    vtkParametricEllipsoid *ellipsoid(void);

public:
    virtual void setData(dtkAbstractData *ellipsoid1);
    void setMode(int state);
    void setQVTKWidget(QVTKOpenGLWidget *widget);
    void showEllipsoidWidget(bool ellipsoidWidget);
    bool isShowEllipsoidWidget(void);
    void setDisplay(bool display);
    void setEllipsoidWidget(bool ellipsoidWidget);

public slots:
    void onModeChanged(int state);
    void onRemoved(void);
    void onUpdateGeometry(void);

protected:
    axlActorEllipsoid(void);
    ~axlActorEllipsoid(void);

private:
    axlActorEllipsoid(const axlActorEllipsoid&); // Not implemented.
    void operator = (const axlActorEllipsoid&); // Not implemented.

private:
    axlActorEllipsoidPrivate *d;
};

axlAbstractActor *createAxlActorEllipsoid(void);

#endif //AXLACTORELLIPSOID_H
