/* axlActorDataDynamic.cpp ---
 *
 * Author: Anais Ducoffe
 * Copyright (C) 2013 - Anais Ducoffe, Inria.
 */

/* Commentary:
 *
 */

/* Change log:
 *
 */
#include "axlActorDataDynamic.h"

#include "axlInteractorStyleSwitch.h"


//all data types
#include <axlCore/axlDataDynamic.h>
#include "axlActorPoint.h"
#include "axlActorComposite.h"
#include <axlCore/axlPoint.h>
#include <axlCore/axlAbstractView.h>

#include "axlInspectorActorFactory.h"

#include <QVTKOpenGLWidget.h>

#include <vtkActor.h>
#include <vtkAssemblyPath.h>
#include <vtkCellPicker.h>
#include <vtkCommand.h>
#include <vtkGlyph3D.h>
#include <vtkInteractorStyleSwitch.h>
#include <vtkLight.h>
#include <vtkLightCollection.h>
#include <vtkLineSource.h>
#include <vtkConeSource.h>
#include <vtkCylinderSource.h>
#include <vtkPlaneSource.h>
#include <vtkSphereSource.h>
#include <vtkParametricFunctionSource.h>
#include <vtkParametricTorus.h>
#include <vtkPointWidget.h>
#include <vtkLineWidget.h>
#include <vtkObjectFactory.h>
#include <vtkPoints.h>
#include <vtkPolyData.h>
#include <vtkPolyDataMapper.h>
#include <vtkProperty.h>
#include <vtkRenderer.h>
#include <vtkRendererCollection.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkSmartPointer.h>
#include <vtkTransform.h>
#include <vtkTubeFilter.h>

class axlActorDataDynamicObserver : public vtkCommand
{
public:
    static axlActorDataDynamicObserver *New(void)
    {
        return new axlActorDataDynamicObserver;
    }

    virtual void Execute(vtkObject *caller, unsigned long event, void *)
    {
//        if(observerDataAssembly){
//            vtkRenderWindowInteractor *interactor = observerDataAssembly->getInteractor();


//            if(event == vtkCommand::MouseMoveEvent)
//            {
//                // we need the matrix transform of this actor

//                vtkAssemblyPath *path;
//                vtkRenderWindow *renderWindow = interactor->GetRenderWindow();
//                vtkRendererCollection * rendererCollection = renderWindow->GetRenderers();
//                vtkRenderer *render = rendererCollection->GetFirstRenderer();
//                axlInteractorStyle = dynamic_cast<axlInteractorStyleSwitch *>(interactor->GetInteractorStyle());

//                int X = observerDataAssembly->getInteractor()->GetEventPosition()[0];
//                int Y = observerDataAssembly->getInteractor()->GetEventPosition()[1];

//                if (!render || !render->IsInViewport(X, Y))
//                {
//                    qDebug()<<" No renderer or bad cursor coordonates in axlActorPoint";
//                }

//                axldynamicPicker->Pick(X,Y,0.0,render);
//                path = axldynamicPicker->GetPath();
//            }

//        }

    }

    axlInteractorStyleSwitch *axlInteractorStyle;
    vtkCellPicker *axldynamicPicker;
    axlDataDynamic *observerData_dynamic;
    axlActorDataDynamic *observerDataAssembly;

};

// /////////////////////////////////////////////////////////////////
// axlActorDataDynamicPrivate
// /////////////////////////////////////////////////////////////////

class axlActorDataDynamicPrivate
{
public:
    axlDataDynamic *dynamicData;
    QList<axlAbstractActor *>outputActors;
    axlActorDataDynamicObserver *dataObserver;
    QVTKOpenGLWidget *widget;
    vtkCellPicker *axlPicker ;

    axlAbstractView *view;
    vtkRenderer *renderer;


};

#if (VTK_MAJOR_VERSION <= 5)
vtkCxxRevisionMacro(axlActorDataDynamic, "$Revision: 0.0.1 $");
#endif

vtkStandardNewMacro(axlActorDataDynamic);

dtkAbstractData *axlActorDataDynamic::data(void)
{
    return d->dynamicData;
}

axlAbstractActor *axlActorDataDynamic::outputActor(void){

    return d->outputActors.at(0);
}

axlAbstractActor *axlActorDataDynamic::outputActors(int i){

    return d->outputActors.at(i);
}

void axlActorDataDynamic::setView(axlAbstractView *view)
{
    d->view = view;
}

void axlActorDataDynamic::setRenderer(vtkRenderer *renderer)
{
    d->renderer = renderer;
}

void axlActorDataDynamic::setQVTKWidget(QVTKOpenGLWidget *widget)
{
    d->widget = widget;
}


void axlActorDataDynamic::setData(dtkAbstractData *data1)
{
    axlDataDynamic *data = dynamic_cast<axlDataDynamic *>(data1);
    if(data){
        d->dynamicData = data;

        int n = d->dynamicData->numberOfChannels();
        for(int indice = 0; indice < n; indice++){
            if(d->dynamicData->outputs(indice)){
                axlInspectorActorFactory *factory= axlInspectorActorFactory::instance();
                d->outputActors.append(factory->create(d->dynamicData->outputs(indice)->identifier()));

                d->outputActors.last()->setInteractor(this->getInteractor());
                axlAbstractData *currentOutput = d->dynamicData->outputs(indice);
                d->outputActors.last()->setData(currentOutput);
                d->outputActors.last()->setParent(this);
                // pour creer l'actor qui va avec l'element repertorie dans les enfants dans l'inspecteur d'objet
                d->view->insert(currentOutput, d->outputActors.last());

                this->setMapper(vtkSmartPointer<vtkPolyDataMapper>::New());
                this->setActor(dynamic_cast<axlActor *>(d->outputActors.last())->getActor());
                this->getActor()->SetMapper(dynamic_cast<axlActor *>(d->outputActors.last())->getMapper());
                //this->getMapper()->SetInput(dynamic_cast<axlActor *>(d->outputActor)->getMapper()->GetInput());
                this->AddPart(this->getActor());

            }
        }

        if(!d->dataObserver)
        {
            d->axlPicker = vtkCellPicker::New();
            d->dataObserver = axlActorDataDynamicObserver::New() ;
            this->getInteractor()->AddObserver(vtkCommand::InteractionEvent, d->dataObserver);
            this->getInteractor()->AddObserver(vtkCommand::MouseMoveEvent, d->dataObserver);


            d->dataObserver->observerDataAssembly = this;
            d->dataObserver->axldynamicPicker = d->axlPicker;
            d->dataObserver->observerData_dynamic = d->dynamicData;
            d->dataObserver->axlInteractorStyle = dynamic_cast<axlInteractorStyleSwitch *> (this->getInteractor()->GetInteractorStyle());
        }

        // QColor color = d->dynamicData->color();
        //dynamic_cast<axlActor *>(d->outputActor)->getActor()->GetProperty()->SetColor(color.redF(), color.greenF(), color.blueF());
        //this->getActor()->GetProperty()->SetColor(color.redF(), color.greenF(), color.blueF());

        QString shader = d->dynamicData->shader();
        if(!shader.isEmpty()){
            //d->outputActor->setShader(shader);
            //this->setShader(shader);
        }

        //d->outputActor->setOpacity(d->dynamicData->opacity());
        //this->setOpacity(d->dynamicData->opacity());


    }else{
        qDebug()<< "no axlDataDynamic available";
    }

    connect(data, SIGNAL(modifiedGeometry()), this, SLOT(onUpdateGeometry()));

//    for (int i = 0;i < d->dynamicData->numberOfChannels();i++) {
//        connect(d->dynamicData->outputs(i), SIGNAL(modifiedGeometry()), this, SLOT(onUpdateGeometry()));
//    }
}


void axlActorDataDynamic::setDisplay(bool display)
{
    for(int i = 0; i < d->dynamicData->numberOfChannels();i++){
        d->outputActors.at(i)->setDisplay(display);
    }
}


void axlActorDataDynamic::setMode(int state)
{

    this->onModeChanged(state);

    emit stateChanged(this->data(), state);

}

//void axlActorDataDynamic::onModeChanged(int state)
//{

//    for(int indice = 0; indice < d->dynamicData->numberOfChannels();indice++){
//        axlAbstractData *axlData = d->dynamicData->outputs(indice);
//        if(axlData){
//            if((axlData->editable() == false) && (state == 2)){
//                state = 1;
//            }
//        }

//        qDebug() << "final state : " << state;

//        if(d->outputActors.at(indice)){
//            d->outputActors.at(indice)->onModeChanged(state);
//            //this->setState(d->outputActors.at(indice)->getState());
//            this->setState(state);
//            vtkProperty *prop = this->getActor()->GetProperty();
//            prop->SetColor(dynamic_cast<axlActor *>(d->outputActors.at(indice))->getActor()->GetProperty()->GetColor()[0],dynamic_cast<axlActor *>(d->outputActors.at(indice))->getActor()->GetProperty()->GetColor()[1],dynamic_cast<axlActor *>(d->outputActors.at(indice))->getActor()->GetProperty()->GetColor()[2] );
//            this->Modified();
//        }
//    }


//}


void axlActorDataDynamic::onModeChanged(int state)
{
    axlAbstractData *axlData = d->dynamicData->outputs();
    if(axlData){
        if((axlData->editable() == false) && (state == 2)){
            state = 1;
        }
    }

    if(state == 0)
    {

        this->setState(axlActorDataDynamic::passive);

        vtkSmartPointer<vtkPropCollection> actors = vtkSmartPointer<vtkPropCollection>::New();
        this->GetActors(actors);
        actors->InitTraversal();
//        QColor color;
//        if(axlDataDynamic *composite = dynamic_cast<axlDataDynamic *>(this->data()))
//            color = composite->color();
//        else
//            color.setRgb(1.0, 1.0, 1.0);

//        for(int i = 0; i< actors->GetNumberOfItems(); i++)
//        {
//            vtkProp *prop = actors->GetNextProp();
//            if(vtkActor *actor = dynamic_cast<vtkActor *>(prop))
//            {
//                actor->GetProperty()->SetColor(color.redF(), color.greenF(), color.blueF());
//            }
//        }

        // we need to add renderer the axlActorComposite
        // and to remove from the renderer and the  all of the axlActor child of the axlActorComposite
        d->renderer->AddActor(this);


        foreach(axlAbstractActor *abstractActor, d->outputActors)
        {
            if(axlActor *actorProp = dynamic_cast<axlActor *>(abstractActor))
            {
                disconnect(actorProp, SIGNAL(stateChanged(dtkAbstractData*,int)), d->view, SIGNAL(stateChanged(dtkAbstractData*,int)));
                //d->renderer->RemoveActor(actorProp);
            }
            if(axlActorDataDynamic *actorProp = dynamic_cast<axlActorDataDynamic *>(abstractActor))
            {
                disconnect(actorProp, SIGNAL(stateChanged(dtkAbstractData*,int)), d->view, SIGNAL(stateChanged(dtkAbstractData*,int)));
                //d->renderer->RemoveActor(actorProp);
            }
        }
    }
    else if(state == 1)
    {

        this->setState(axlActorDataDynamic::selection);

        vtkSmartPointer<vtkPropCollection> actors = vtkSmartPointer<vtkPropCollection>::New();
        this->GetActors(actors);
        actors->InitTraversal();

//        QColor color;
//        if(axlDataDynamic *composite = dynamic_cast<axlDataDynamic *>(this->data()))
//        {
//            color = composite->color();
//            qreal *h = new qreal(0.0);
//            qreal *s = new qreal(0.0);
//            qreal *l = new qreal(0.0);
//            color.getHslF(h, s, l);
//            color.setHslF(*h, *s, *l + (1.0 - *l) / 2.0);
//        }
//        else
//            color.setRgb(0.0, 0.0, 1.0);

//        for(int i = 0; i< actors->GetNumberOfItems(); i++)
//        {
//            vtkProp *prop = actors->GetNextProp();
//            if(vtkActor *actor = dynamic_cast<vtkActor *>(prop))
//                actor->GetProperty()->SetColor(color.redF(), color.greenF(), color.blueF());
//        }

        if(!d->renderer->HasViewProp(this))
        {
            d->renderer->AddActor(this);

            foreach(axlAbstractActor *abstractActor, d->outputActors)
            {
                if(axlActor *actorProp = dynamic_cast<axlActor *>(abstractActor))
                {
                    disconnect(actorProp, SIGNAL(stateChanged(dtkAbstractData*,int)), d->view, SIGNAL(stateChanged(dtkAbstractData*,int)));
                    d->renderer->RemoveActor(actorProp);

                }
                if(axlActorComposite *actorProp = dynamic_cast<axlActorComposite *>(abstractActor))
                {
                    disconnect(actorProp, SIGNAL(stateChanged(dtkAbstractData*,int)), d->view, SIGNAL(stateChanged(dtkAbstractData*,int)));
                    d->renderer->RemoveActor(actorProp);

                }
            }

        }
    }
    else if(state == 2)
    {
        this->setState(axlActorDataDynamic::edition);
        vtkSmartPointer<vtkPropCollection> actors = vtkSmartPointer<vtkPropCollection>::New();
        this->GetActors(actors);
        actors->InitTraversal();

//        QColor color;
//        if(axlDataDynamic *composite = dynamic_cast<axlDataDynamic *>(this->data()))
//        {
//            color = composite->color();
//            qreal *h = new qreal(0.0);
//            qreal *s = new qreal(0.0);
//            qreal *l = new qreal(0.0);
//            color.getHslF(h, s, l);
//            color.setHslF(*h, *s, *l + (1.0 - *l) / 2.0);
//        }
//        else
//            color.setRgb(0.0, 0.0, 1.0);

//        for(int i = 0; i< actors->GetNumberOfItems(); i++)
//        {

//            vtkProp *prop = actors->GetNextProp();
//            if(vtkActor *actor = dynamic_cast<vtkActor *>(prop))
//            {
//                actor->GetProperty()->SetColor(color.redF(), color.greenF(), color.blueF());
//            }
//        }
        // we need to add renderer all of the axlActor composing the composite
        // and to remove from the renderer the axlActorComposite
        d->renderer->RemoveActor(this);

        //vtkProp3DCollection *actors2 = this->GetParts();
        //actors2->InitTraversal();

        for(int i = 0 ; i < d->dynamicData->numberOfChannels(); i ++)
        {
            axlAbstractData *axlData = dynamic_cast<axlAbstractData *>(d->dynamicData->outputs(i));
            axlData->setColor(d->dynamicData->color());
        }

        foreach(axlAbstractActor *abstractActor, d->outputActors)
        {
            if(axlActor *actorProp = dynamic_cast<axlActor *>(abstractActor))
            {
                connect(actorProp, SIGNAL(stateChanged(dtkAbstractData*,int)), d->view, SIGNAL(stateChanged(dtkAbstractData*,int)));
                d->renderer->AddActor(actorProp);

            }
            if(axlActorComposite *actorProp = dynamic_cast<axlActorComposite *>(abstractActor))
            {
                connect(actorProp, SIGNAL(stateChanged(dtkAbstractData*,int)), d->view, SIGNAL(stateChanged(dtkAbstractData*,int)));
                d->renderer->AddActor(actorProp);
            }
        }
    }
}


void axlActorDataDynamic::onRemoved()
{

    if(d->dataObserver)
    {
        this->getInteractor()->RemoveObservers(vtkCommand::LeftButtonPressEvent, d->dataObserver);
        this->getInteractor()->RemoveObservers(vtkCommand::RightButtonPressEvent, d->dataObserver);
        //        this->getInteractor()->RemoveObservers(vtkCommand::InteractionEvent, d->dataObserver);
        //        this->getInteractor()->RemoveObservers(vtkCommand::KeyPressEvent, d->dataObserver);
        //        //this->RemoveObserver(d->dataObserver);
        d->dataObserver->observerDataAssembly = NULL;
        d->dataObserver->axldynamicPicker = NULL;
        d->dataObserver->observerData_dynamic = NULL;
        d->dataObserver->axlInteractorStyle = NULL;
        d->dataObserver->Delete();
        d->dataObserver = NULL;

        d->axlPicker->Delete();
        d->axlPicker = NULL;

    }

    if(d->widget)
    {
        d->widget = NULL;
    }
    if(d->dynamicData)
    {
        for(int i = 0;i < d->dynamicData->numberOfChannels();i++){
            axlAbstractData *output = d->dynamicData->outputs(i);
            if(output){
                disconnect(output,SIGNAL(modified()),this , SLOT(onUpdateGeometry()));
            }
        }

        for (int indice = 0; indice < d->dynamicData->numberOfChannels();indice++) {

            if((!(d->outputActors.at(indice) == NULL))){
                axlActor *actor = dynamic_cast<axlActor *>(d->outputActors.at(indice));
                if(actor){
                    actor->setState(this->getState());
                    actor->onRemoved();
                }
            }

        }

        d->dynamicData = NULL;
    }

    if(axlActorComposite *actorComposite = dynamic_cast<axlActorComposite *>(this->parent()) )
        actorComposite->removeActorReference(this);
}

void axlActorDataDynamic::onUpdateGeometry()
{
//    for(int indice = 0; indice < d->dynamicData->numberOfChannels(); indice++) {
//        d->outputActors.at(indice)->onUpdateGeometry();
}

axlActorDataDynamic::axlActorDataDynamic(void) : axlActor(), d(new axlActorDataDynamicPrivate)
{
    d->dynamicData = NULL;
    //    for(int indice =0; indice < d->dynamicData->numberOfChannels();indice++){
    //        d->outputActors.at(indice) = NULL;
    //    }
    d->axlPicker = NULL;
    d->dataObserver = NULL;
    d->widget = NULL;
    d->view = NULL;
    d->renderer = NULL;
}

axlActorDataDynamic::~axlActorDataDynamic(void)
{
    delete d;

    d = NULL;
}

axlAbstractActor *createAxlActorDataDynamic(void){

    return axlActorDataDynamic::New();
}
