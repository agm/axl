#!/bin/bash

cp -r /System/Library/Frameworks/Python.framework bin/axel.app/Contents/Frameworks/
install_name_tool -change /System/Library/Frameworks/Python.framework/Versions/2.7/Python @executable_path/../Frameworks/Python.framework/Versions/2.7/Python bin/axel.app/Contents/MacOS/axel
install_name_tool -change /System/Library/Frameworks/Python.framework/Versions/2.7/Python @executable_path/../Frameworks/Python.framework/Versions/2.7/Python bin/axel.app/Contents/Frameworks/libaxlCore.dylib
install_name_tool -change /System/Library/Frameworks/Python.framework/Versions/2.7/Python @executable_path/../Frameworks/Python.framework/Versions/2.7/Python bin/axel.app/Contents/Frameworks/libaxlGui.dylib
install_name_tool -change /System/Library/Frameworks/Python.framework/Versions/2.7/Python @executable_path/../Frameworks/Python.framework/Versions/2.7/Python bin/axel.app/Contents/Frameworks/libdtkCore.dylib
install_name_tool -change /System/Library/Frameworks/Python.framework/Versions/2.7/Python @executable_path/../Frameworks/Python.framework/Versions/2.7/Python bin/axel.app/Contents/Frameworks/libdtkGui.dylib
install_name_tool -change /System/Library/Frameworks/Python.framework/Versions/2.7/Python @executable_path/../Frameworks/Python.framework/Versions/2.7/Python bin/axel.app/Contents/Frameworks/libdtkLog.dylib
install_name_tool -change /System/Library/Frameworks/Python.framework/Versions/2.7/Python @executable_path/../Frameworks/Python.framework/Versions/2.7/Python bin/axel.app/Contents/Frameworks/libdtkMath.dylib
install_name_tool -change /System/Library/Frameworks/Python.framework/Versions/2.7/Python @executable_path/../Frameworks/Python.framework/Versions/2.7/Python bin/axel.app/Contents/Frameworks/libdtkScript.dylib
make dmg


