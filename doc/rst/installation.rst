==================
Download & Install
==================
	     
.. |linux| image:: img/linux.png 
	   :width: 50px
		   
.. |macos|  image:: img/macos.png 
	   :width: 50px
		   
.. |windows| image:: img/windows.png 
	   :width: 50px

.. |source| image:: img/source-code.png 
	   :width: 50px 

.. raw:: html
	 
	 <table width="100%"><tr>
	 <td align="center"><a href="https://gitlab.inria.fr/agm/axl/wikis/Download-binaries"><img src="_images/linux.png" width="75"></a></td>
	 <td align="center"><a href="https://gitlab.inria.fr/agm/axl/wikis/Download-binaries"><img src="_images/macos.png" width="75"></a></td>
	 <td align="center"><a href="https://gitlab.inria.fr/agm/axl/wikis/Download-binaries"><img src="_images/windows.png" width="75"></a></td>  
	 <td align="center"><a href="https://gitlab.inria.fr/agm/axl/wikis/Download-sources"><img src="_images/source-code.png" width="75"></a></td>
	 </tr>
	 <tr>
	 <td align="center"><a href="https://gitlab.inria.fr/agm/axl/wikis/Download-binaries">linux64</a></td>
	 <td align="center"><a href="https://gitlab.inria.fr/agm/axl/wikis/Download-binaries">osx64</a></td>
	 <td align="center"><a href="https://gitlab.inria.fr/agm/axl/wikis/Download-binaries">win64</a></td>
	 <td align="center"><a href="https://gitlab.inria.fr/agm/axl/wikis/Download-sources">source</a>
	 </tr></table>
	 <p></p>

   
Binaries
========
Binary distributions can be installed with ``conda`` for Linux, MacOS and Windows.
This installation requires first the installation of `conda <https://conda.io/docs/>`_:

Install ``conda``
-----------------

- Download `miniconda <https://conda.io/miniconda.html>`_ and install it::

     bash Miniconda*-*-*-*.sh

  Preferably use Miniconda3. See `here for more details <https://conda.io/docs/install/quick.html>`_.


- Set up your environment, by adding the path to the directory of ``conda`` in the variable ``PATH``.
   For ``Linux`` or ``MacOS``, you can add the corresponding line::

     export PATH=/path/to/where/you/have/installed/miniconda*/bin:$PATH

   at the end of your file  ``$HOME/.bashrc`` (or ``$HOME/.bash_profile``) to have the variable ``PATH`` set up, each time you open a terminal. To set up the environment after editing the file, use::

      source $HOME/.bashrc

   For ``Windows``, edit your environment variable ``PATH`` and add the path to the installation of  miniconda ``C:\\....\\Miniconda*\\...\\Library\\bin\\``.

Install ``Axl`` with ``conda``
------------------------------
Once ``conda`` is installed, you can use it to install ``Axl``::

     conda install axl -c agm-distrib

Check that ``axl`` is now running::

    axl

This should start the application ``axl``, with its initial view of a frame.

These binary distributions for ``linux64``, ``osx64`` and ``win64`` are from:

   https://anaconda.org/agm-distrib

where new releases will be made available. 

Update ``Axl`` version
----------------------
To installed a new version of ``axl``::

  conda update axl -c agm-distrib

See `here <https://gitlab.inria.fr/agm/axl/wikis/Download-binaries>`_ for more details.

Sources
=======

The sources can be downloaded from 

     http://gitlab.inria.fr/agm/axl

as follows::

   git clone https://gitlab.inria.fr/agm/axl.git
   cd axl
   ./script/init.cfg

See the `compilation instructions <https://gitlab.inria.fr/agm/axl/wikis/Compilation-of-sources>`_.


