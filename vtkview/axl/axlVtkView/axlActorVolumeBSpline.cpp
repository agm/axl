/* axlActorVolumeBSpline.cpp ---
 *
 * Author: Anais Ducoffe
 * Copyright (C) 2012 - Anais Ducoffe, Inria.
 * Created:
 * Version: $Id$
 * Last-Updated:
 *           By:
 *     Update #:
 */

/* Commentary:
 *
 */

/* Change log:
 *
 */

#include "axlActorVolumeBSpline.h"
#include "axlControlPointsWidget.h"

#include <axlCore/axlAbstractVolumeBSpline.h>
#include <axlCore/axlAbstractField.h>

#include <dtkMathSupport/dtkVector3D.h>

#include <vtkActor.h>
#include <vtkVolume.h>
#include <vtkActorCollection.h>
#include <vtkCellArray.h>
#include <vtkCellData.h>
#include <vtkCommand.h>
#include <vtkDoubleArray.h>
#include <vtkLookupTable.h>
#include <vtkObjectFactory.h>
//#include <vtkPainterPolyDataMapper.h>
#include <vtkPoints.h>
#include <vtkPointData.h>
#include <vtkPolyData.h>
#include <vtkUnstructuredGrid.h>
#include <vtkPolyVertex.h>
#include <vtkPolygon.h>
#include <vtkPolyDataMapper.h>
#include <vtkDataSetMapper.h>
#include <vtkPolyDataNormals.h>
#include <vtkProperty.h>
#include <vtkProp.h>
#include <vtkHexahedron.h>
#include <vtkQuad.h>
#include <vtkTexture.h>
#include <vtkTetra.h>
#include <vtkTriangleStrip.h>
#include <vtkTimerLog.h>
#include <vtkSmartPointer.h>
#include <vtkStripper.h>
#include <vtkPointData.h>
#include <vtkTriangleFilter.h>
#include <vtkPNGReader.h>
#include <vtkFloatArray.h>
//#include <vtkOpenGLHardwareSupport.h>
#include <vtkOpenGLRenderWindow.h>
//#include <vtkPainterPolyDataMapper.h>
#include <vtkProperty.h>
#include <vtkIdList.h>

// /////////////////////////////////////////////////////////////////
// axlActorVolumeBSplinePrivate
// /////////////////////////////////////////////////////////////////

class axlActorVolumeBSplinePrivate
{
public:
    axlAbstractVolumeBSpline *splineVolume;
};

#if (VTK_MAJOR_VERSION <= 5)
vtkCxxRevisionMacro(axlActorVolumeBSpline, "$Revision: 0.0.1 $");
#endif

vtkStandardNewMacro(axlActorVolumeBSpline);

dtkAbstractData *axlActorVolumeBSpline::data(void)
{
    return d->splineVolume;
}

void axlActorVolumeBSpline::setData(dtkAbstractData *spline_Volume1)
{
    axlAbstractVolumeBSpline *spline_Volume = dynamic_cast<axlAbstractVolumeBSpline *>(spline_Volume1);
    // we compute here points and hexahedrons
    d->splineVolume = spline_Volume;

    this->setPoints(vtkSmartPointer<vtkPoints>::New());

    this->setActor(vtkSmartPointer<vtkActor>::New());
    this->setCellArray(vtkSmartPointer<vtkCellArray>::New());
    this->setUnstructuredGrid(vtkSmartPointer<vtkUnstructuredGrid>::New());
    this->setDataSetMapper(vtkSmartPointer<vtkDataSetMapper>::New());

    this->pointsUpdate();
    this->UnstructuredGridUpdate();


    this->getUnstructuredGrid()->SetPoints(this->getPoints());
    //    this->getUnstructuredGrid()->SetCells(VTK_HEXAHEDRON,this->getCellArray());
    this->getUnstructuredGrid()->SetCells(VTK_QUAD,this->getCellArray());
    //    this->getUnstructuredGrid()->SetCells(VTK_TETRA,this->getCellArray());

#if (VTK_MAJOR_VERSION <= 5)
    this->getDataSetMapper()->SetInput(this->getUnstructuredGrid());
#else
    this->getDataSetMapper()->SetInputData(this->getUnstructuredGrid());
#endif
    this->getActor()->SetMapper(this->getDataSetMapper());

    this->getDataSetMapper()->ScalarVisibilityOff();

    // add sphere param
    this->AddPart(this->getActor());

    // add the observer
    if(!this->getObserver())
    {
        this->NewObserver();
        this->setObserverData(d->splineVolume);
    }

    QColor color = d->splineVolume->color();
    this->getActor()->GetProperty()->SetColor(color.redF(), color.greenF(), color.blueF());

    QString shader = d->splineVolume->shader();
    if(!shader.isEmpty())
        this->setShader(shader);

    connect(d->splineVolume, SIGNAL(modifiedGeometry()), this, SLOT(onUpdateGeometry()));
    connect(d->splineVolume, SIGNAL(modifiedProperty()), this, SLOT(onUpdateProperty()));
}


void axlActorVolumeBSpline::setMapperCollorArray(void)
{
    vtkSmartPointer<vtkDoubleArray> scalarArray = vtkSmartPointer<vtkDoubleArray>::New();
    scalarArray->SetName("mapperCollorArrayDefaultField");
    scalarArray->SetNumberOfComponents(1);

    double start_u = d->splineVolume->startParam_u();
    double start_v = d->splineVolume->startParam_v();
    double start_w = d->splineVolume->startParam_w();
    double end_u = d->splineVolume->endParam_u();
    double end_v = d->splineVolume->endParam_v();
    double end_w = d->splineVolume->endParam_w();
    double paramCourant_u = start_u;
    double paramCourant_v = start_v;
    double paramCourant_w = start_w;

    int n_u = d->splineVolume->numSamples_u();// need to be superior than 1
    int n_v = d->splineVolume->numSamples_v();
    int n_w = d->splineVolume->numSamples_w();

    scalarArray->SetNumberOfTuples(n_u * n_v * n_w);

    double interval_u = (double)(end_u - start_u) / (n_u - 1);
    double interval_v = (double)(end_v - start_v) / (n_v - 1);
    double interval_w = (double)(end_w - start_w) / (n_w - 1);


    for(int k = 0; k < n_w - 1 ; k++)
    {
        for(int i = 0; i < n_v - 1 ; i++)
        {
            for(int j = 0; j < n_u - 1 ; j++)
            {
                scalarArray->SetTuple1(k * n_u*n_v + i * n_u + j, d->splineVolume->scalarValue(paramCourant_u, paramCourant_v, paramCourant_w));
                paramCourant_u += interval_u;
            }

            scalarArray->SetTuple1(k * n_u*n_v + i * n_u + (n_u - 1), d->splineVolume->scalarValue(end_u, paramCourant_v, paramCourant_w));
            paramCourant_u = start_u;
            paramCourant_v += interval_v;
        }
        scalarArray->SetTuple1(k * n_u*n_v + (n_v-1) * n_u + (n_u - 1), d->splineVolume->scalarValue(end_u,end_v, paramCourant_w));
        paramCourant_u = start_u;
        paramCourant_v = start_v;
        paramCourant_w += interval_w;
    }

    for(int j = 0; j < n_v - 1; j++)
    {
        for(int i = 0; i < n_u - 1; i++)
        {
            scalarArray->SetTuple1( n_u * n_v * (n_w -1)+ n_u * j + i, d->splineVolume->scalarValue(paramCourant_u, paramCourant_v, end_w));
            paramCourant_u += interval_u;
        }
        scalarArray->SetTuple1( n_u * n_v * (n_w -1)+ n_u * j + (n_u-1), d->splineVolume->scalarValue(end_u,paramCourant_v, end_w));
        paramCourant_u = start_u;
        paramCourant_u += interval_u;
    }

    paramCourant_w = start_w;
    for(int k = 0; k < n_w ; k++)
    {
        for(int i = 0; i < n_u - 1; i++)
        {
            scalarArray->SetTuple1( n_u * n_v * k + n_u * (n_v-1) + i, d->splineVolume->scalarValue(paramCourant_u, end_v, paramCourant_w));
            paramCourant_u += interval_u;
        }

        paramCourant_u = start_u;
        paramCourant_w += interval_w;
    }

    scalarArray->SetTuple1(n_u * n_v * n_w - 1, d->splineVolume->scalarValue(end_u, end_v, end_w));


    vtkSmartPointer<vtkUnstructuredGrid> data = this->getUnstructuredGrid();
    data->GetPointData()->AddArray(scalarArray);
    data->GetPointData()->SetActiveScalars("mapperCollorArrayDefaultField");

    vtkSmartPointer<vtkLookupTable> lookupTable = vtkSmartPointer<vtkLookupTable>::New();
    lookupTable->SetRange(-1.0, 1.0);
    lookupTable->SetNumberOfTableValues(d->splineVolume->stripes());
    lookupTable->Build();
    for(int i= 0; i < d->splineVolume->stripes(); i+=2)
    {
        lookupTable->SetTableValue(i  , 1.0, 1.0, 1.0);
        lookupTable->SetTableValue(i+1, 0.0, 0.0, 0.0);
    }
    // test to get light direction .... TO DELETE

    vtkSmartPointer<vtkDoubleArray> lightArray = vtkSmartPointer<vtkDoubleArray>::New();
    lightArray->SetName("mapperLightDirection");
    lightArray->SetNumberOfComponents(3);

    paramCourant_u = start_u;
    paramCourant_v = start_v;

    lightArray->SetNumberOfTuples(n_u * n_v * n_w);
    double *currentLight = new double[3];
    currentLight[0] = 1.0;
    currentLight[1] = 0.6;
    currentLight[2] = 0.2;
    for(int k = 0; k < n_w - 1; k++)
    {
        for(int j = 0; j < n_v - 1; j++)
        {
            for(int i = 0; i < n_u - 1; i++)
            {
                lightArray->SetTuple(k * n_u* n_v + j * n_u + i, currentLight);
            }

            lightArray->SetTuple(k * n_u* n_v + j * n_u +  n_u - 1, currentLight);
            paramCourant_u = start_u;
            paramCourant_v += interval_v;
        }
        lightArray->SetTuple(k * n_u* n_v + (n_v -1) * n_u +  n_u - 1, currentLight);
        paramCourant_u = start_u;
        paramCourant_v = start_v;
        paramCourant_w += interval_w;
    }

    for(int j = 0; j < n_v - 1; j++)
    {
        for(int i = 0; i < n_u - 1; i++)
        {
            lightArray->SetTuple((n_w -1)* n_u* n_v + j * n_u + i, currentLight);
            paramCourant_u += interval_u;
        }
        lightArray->SetTuple((n_w -1)* n_u* n_v + j * n_u + n_u -1, currentLight);
        paramCourant_u += interval_u;
    }

    for(int k = 0; k < n_w ; k++)
    {
        for(int i = 0; i < n_u - 1; i++)
        {
            lightArray->SetTuple(k* n_u* n_v + (n_v-1) * n_u + i, currentLight);
            paramCourant_u += interval_u;
        }

        paramCourant_u += start_u;
        paramCourant_w += interval_w;
    }
    lightArray->SetTuple(n_w * n_u* n_v -1, currentLight);

    data = this->getUnstructuredGrid();
    data->GetPointData()->AddArray(lightArray);


    //add vertex attrib array
    vtkSmartPointer<vtkDataSetMapper> mapper = this->getDataSetMapper();
    vtkSmartPointer<vtkUnstructuredGrid> myUnstructuredGrid = this->getUnstructuredGrid();
    //mapper->MapDataArrayToVertexAttribute("scalarsattrib",  data->GetPointData()->GetArrayName(2), vtkDataObject::FIELD_ASSOCIATION_POINTS, -1);
    mapper->SetLookupTable(lookupTable);
    mapper->SetInterpolateScalarsBeforeMapping(true);
    mapper->UseLookupTableScalarRangeOn();


    this->Modified();
}


void axlActorVolumeBSpline::onSamplingChanged(void)
{
    if(d->splineVolume)
    {

        this->getDataSetMapper()->RemoveAllInputs();
        this->getUnstructuredGrid()->Initialize();

        // delete current vtkPoint and vtkCellArray
        this->getPoints()->Reset();
        this->getCellArray()->Reset();

        this->getPoints()->Squeeze();
        this->getCellArray()->Squeeze();

        this->pointsUpdate();

        this->UnstructuredGridUpdate();

        this->getUnstructuredGrid()->SetPoints(this->getPoints());
        //        this->getUnstructuredGrid()->SetCells(VTK_HEXAHEDRON,this->getCellArray());
        this->getUnstructuredGrid()->SetCells(VTK_QUAD,this->getCellArray());
        //        this->getUnstructuredGrid()->SetCells(VTK_TETRA,this->getCellArray());

#if (VTK_MAJOR_VERSION <= 5)
        this->getDataSetMapper()->SetInput(this->getUnstructuredGrid());
#else
        this->getDataSetMapper()->SetInputData(this->getUnstructuredGrid());
#endif


    }

    if(d->splineVolume->fields().count() != 0){
        d->splineVolume->touchField();
        d->splineVolume->touchGeometry();
    }

}

void axlActorVolumeBSpline::onUpdateGeometry()
{
    if (this->getControlPoints())
        this->getControlPoints()->SetControlPointRadius(d->splineVolume->size());

    emit updated();
}

void axlActorVolumeBSpline::pointsUpdate(void)
{
    if(d->splineVolume->rational())
    {
        d->splineVolume->updateRcoeff();
    }
    // we consider here that all memory vtk pipeline from points to unstructuredGrid are free but not deleted...
    double start_u = d->splineVolume->startParam_u();
    double start_v = d->splineVolume->startParam_v();
    double start_w = d->splineVolume->startParam_w();
    double end_u = d->splineVolume->endParam_u();
    double end_v = d->splineVolume->endParam_v();
    double end_w = d->splineVolume->endParam_w();
    double paramCourant_u = start_u;
    double paramCourant_v = start_v;
    double paramCourant_w = start_w;

    vtkPoints *points = this->getPoints();

    int n_u = d->splineVolume->numSamples_u();// need to be superior than 1
    int n_v = d->splineVolume->numSamples_v();
    int n_w = d->splineVolume->numSamples_w();
    points->SetNumberOfPoints(n_u * n_v * n_w);

    axlPoint *pointCourant = new axlPoint;
    double interval_u = (double)(end_u - start_u) / (n_u - 1);
    double interval_v = (double)(end_v - start_v) / (n_v - 1);
    double interval_w = (double)(end_w - start_w) / (n_w - 1);


    int ind1 = 0;
    int ind2 = 0;
    int ind3 = 0;
    for(int k = 0; k < n_w - 1; k++)
    {
        ind1 = k * n_u * n_v;
        for(int i = 0; i < n_v - 1; i++)
        {
            ind2 = i * n_u;
            for(int j = 0; j < n_u - 1; j++)
            {
                ind3 = ind1 + ind2 +j;
                d->splineVolume->eval(pointCourant, paramCourant_u, paramCourant_v, paramCourant_w);
                points->SetPoint(ind3, pointCourant->coordinates());
                paramCourant_u += interval_u;
            }
            ind3 = ind1 + ind2 + (n_u - 1);

            d->splineVolume->eval(pointCourant, end_u, paramCourant_v, paramCourant_w);
            points->SetPoint(ind3 , pointCourant->coordinates());

            paramCourant_u = start_u;
            paramCourant_v += interval_v;
        }
        ind3 = ind1 + (n_v -1)* n_u + (n_u - 1);

        d->splineVolume->eval(pointCourant, end_u, end_v, paramCourant_w);
        points->SetPoint(ind3 , pointCourant->coordinates());

        paramCourant_u = start_u;
        paramCourant_v = start_v;
        paramCourant_w += interval_w;
    }


    ind1 = n_u * n_v * (n_w - 1);
    for(int j = 0; j < n_v - 1; j++)
    {
        ind2 = j * n_u;
        for(int i = 0; i < n_u - 1; i++)
        {
            ind3 = ind1 + ind2 + i;
            d->splineVolume->eval(pointCourant, paramCourant_u,paramCourant_v, end_w);
            points->SetPoint(ind3, pointCourant->coordinates());


            paramCourant_u+=interval_u;

        }
        ind3 = ind1 + ind2 + (n_u -1);
        d->splineVolume->eval(pointCourant, end_u, paramCourant_v, end_w);
        points->SetPoint(ind3, pointCourant->coordinates());


        paramCourant_u=start_u;
        paramCourant_v+=interval_v;
    }

    paramCourant_w=start_w;

    ind2 = (n_v-1) * n_u;

    for(int k = 0; k < n_w-1 ; k++)
    {
        ind1 = n_u * n_v * k;
        for(int i = 0; i < n_u - 1; i++)
        {
            ind3 = ind1 + ind2 + i;
            d->splineVolume->eval(pointCourant, paramCourant_u,end_v, paramCourant_w);
            points->SetPoint(ind3, pointCourant->coordinates());


            paramCourant_u+=interval_u;

        }
        ind3 = ind2 + ind1 + (n_u-1);
        paramCourant_u=start_u;
        paramCourant_w+=interval_w;
    }
    paramCourant_w = end_w;
    ind1 = n_u * n_v * (n_w-1);
    for(int i = 0; i < n_u - 1; i++)
    {
        ind3 = ind1 + ind2 + i;
        d->splineVolume->eval(pointCourant, paramCourant_u,end_v, paramCourant_w);
        points->SetPoint(ind3, pointCourant->coordinates());


        paramCourant_u+=interval_u;

    }

    ind3 = n_u * n_v * n_w -1;
    d->splineVolume->eval(pointCourant, end_u, end_v, end_w);
    points->SetPoint(ind3, pointCourant->coordinates());


    delete pointCourant;
}

void axlActorVolumeBSpline::UnstructuredGridUpdate(void)
{
    vtkSmartPointer<vtkCellArray> cellArray = this->getCellArray();

    int n_u = d->splineVolume->numSamples_u();
    int n_v = d->splineVolume->numSamples_v();
    int n_w = d->splineVolume->numSamples_w();
    int ind1 = 0;
    int ind2 = 0;
    int ind3 = 0;
    int ind4 = 0;

    vtkSmartPointer<vtkHexahedron> currentHexa = vtkSmartPointer<vtkHexahedron>::New();
    currentHexa->GetPointIds()->SetNumberOfIds(8);

    vtkSmartPointer<vtkQuad> currentQuad = vtkSmartPointer<vtkQuad>::New();
    currentQuad->GetPointIds()->SetNumberOfIds(4);

    vtkSmartPointer<vtkTetra> currentTetra = vtkSmartPointer<vtkTetra>::New();
    currentTetra->GetPointIds()->SetNumberOfIds(4);

    for(int k = 0; k < n_w - 1; k++)
    {
        for(int j = 0; j < n_v - 1; j++)
        {
            for(int i= 0; i < n_u - 1; i++)
            {

                ind1 =  k * n_u * n_v + j * n_u + i;
                ind2 = ind1 + n_u;
                ind3 = ind1 + n_u * n_v;
                ind4 = ind2 + n_u * n_v;


                ////////////////////////////////////////////////////////////////////////////////
                //                // six tetrahedrons in one hexahedron.
                //                currentTetra->GetPointIds()->SetId(0, ind1+1 );
                //                currentTetra->GetPointIds()->SetId(1, ind3 );
                //                currentTetra->GetPointIds()->SetId(2,ind1 );
                //                currentTetra->GetPointIds()->SetId(3,ind2+1 );
                //                cellArray->InsertNextCell(currentTetra);

                //                currentTetra->GetPointIds()->SetId(0, ind1+1 );
                //                currentTetra->GetPointIds()->SetId(1, ind3+1 );
                //                currentTetra->GetPointIds()->SetId(2, ind3 );
                //                currentTetra->GetPointIds()->SetId(3, ind2+1 );
                //                cellArray->InsertNextCell(currentTetra);

                //                currentTetra->GetPointIds()->SetId(0, ind3+1 );
                //                currentTetra->GetPointIds()->SetId(1, ind4+1 );
                //                currentTetra->GetPointIds()->SetId(2, ind3 );
                //                currentTetra->GetPointIds()->SetId(3, ind2+1 );
                //                cellArray->InsertNextCell(currentTetra);

                //                currentTetra->GetPointIds()->SetId(0, ind4 );
                //                currentTetra->GetPointIds()->SetId(1, ind2 );
                //                currentTetra->GetPointIds()->SetId(2, ind2+1 );
                //                currentTetra->GetPointIds()->SetId(3, ind3 );
                //                cellArray->InsertNextCell(currentTetra);

                //                currentTetra->GetPointIds()->SetId(0, ind3 );
                //                currentTetra->GetPointIds()->SetId(1, ind1 );
                //                currentTetra->GetPointIds()->SetId(2, ind2+1 );
                //                currentTetra->GetPointIds()->SetId(3, ind2 );
                //                cellArray->InsertNextCell(currentTetra);

                //                currentTetra->GetPointIds()->SetId(0, ind4 );
                //                currentTetra->GetPointIds()->SetId(1, ind3 );
                //                currentTetra->GetPointIds()->SetId(2, ind2+1 );
                //                currentTetra->GetPointIds()->SetId(3, ind4+1 );
                //                cellArray->InsertNextCell(currentTetra);
                ////////////////////////////////////////////////////////////////////////////////
                //                currentHexa->GetPointIds()->SetId(0, ind1 );
                //                currentHexa->GetPointIds()->SetId(1, ind1+1);
                //                currentHexa->GetPointIds()->SetId(2, ind2+1 );
                //                currentHexa->GetPointIds()->SetId(3, ind2);


                //                // opposite normal knots in the same order
                //                currentHexa->GetPointIds()->SetId(4, ind3);
                //                currentHexa->GetPointIds()->SetId(5, ind3+1 );
                //                currentHexa->GetPointIds()->SetId(6, ind4+1);
                //                currentHexa->GetPointIds()->SetId(7, ind4);

                //                cellArray->InsertNextCell(currentHexa);


                ////////////////////////////////////////////////////////////////////////////////
                //front face
                if(k==0){
                    currentQuad->GetPointIds()->SetId(0, ind1 );
                    currentQuad->GetPointIds()->SetId(1, ind1+1);
                    currentQuad->GetPointIds()->SetId(2, ind2+1 );
                    currentQuad->GetPointIds()->SetId(3, ind2);

                    cellArray->InsertNextCell(currentQuad);
                }

                //bottom face
                if(j==0){
                    currentQuad->GetPointIds()->SetId(0, ind1 );
                    currentQuad->GetPointIds()->SetId(1, ind1+1);
                    currentQuad->GetPointIds()->SetId(2, ind3+1 );
                    currentQuad->GetPointIds()->SetId(3, ind3);

                    cellArray->InsertNextCell(currentQuad);
                }


                // left side
                if(i==0){
                    currentQuad->GetPointIds()->SetId(0, ind3 );
                    currentQuad->GetPointIds()->SetId(1, ind1);
                    currentQuad->GetPointIds()->SetId(2, ind2 );
                    currentQuad->GetPointIds()->SetId(3, ind4);

                    cellArray->InsertNextCell(currentQuad);
                }


                if(k< (n_w - 1)){
                    currentQuad->GetPointIds()->SetId(0, ind3 );
                    currentQuad->GetPointIds()->SetId(1, ind3+1);
                    currentQuad->GetPointIds()->SetId(2, ind4+1 );
                    currentQuad->GetPointIds()->SetId(3, ind4);

                    cellArray->InsertNextCell(currentQuad);
                }

                if(j <(n_v-1)){
                    currentQuad->GetPointIds()->SetId(0, ind2 );
                    currentQuad->GetPointIds()->SetId(1, ind2+1);
                    currentQuad->GetPointIds()->SetId(2, ind4+1 );
                    currentQuad->GetPointIds()->SetId(3, ind4);

                    cellArray->InsertNextCell(currentQuad);
                }


                if(i <(n_u-1)){
                    currentQuad->GetPointIds()->SetId(0, ind3+1 );
                    currentQuad->GetPointIds()->SetId(1, ind1+1);
                    currentQuad->GetPointIds()->SetId(2, ind2+1 );
                    currentQuad->GetPointIds()->SetId(3, ind4+1);

                    cellArray->InsertNextCell(currentQuad);
                }



            }
        }
    }
}

axlActorVolumeBSpline::axlActorVolumeBSpline(void) : axlActorBSpline(), d(new axlActorVolumeBSplinePrivate)
{

}

axlActorVolumeBSpline::~axlActorVolumeBSpline(void)
{
    delete d;

    d = NULL;
}

axlAbstractActor *createAxlActorVolumeBSpline(void){

    return axlActorVolumeBSpline::New();

}
