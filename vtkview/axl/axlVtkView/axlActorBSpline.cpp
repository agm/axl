/* axlActorBSpline.cpp ---
 *
 * Author: Meriadeg Perrinel
 * Copyright (C) 2008 - Meriadeg Perrinel, Inria.
 * Created: Fri Dec 17 10:59:21 2010 (+0100)
 * Version: $Id$
 * Last-Updated: Mon Dec 17 14:02:41 2012 (+0100)
 *           By: Julien Wintz
 *     Update #: 86
 */

/* Commentary:
 *
 */

/* Change log:
 *
 */

#include "axlActorBSpline.h"
#include "axlActorComposite.h"
#include "axlControlPointsWidget.h"
#include "axlActorCurveBSpline.h"

#include <axlCore/axlPoint.h>
#include <axlCore/axlAbstractCurveBSpline.h>
#include <axlCore/axlAbstractSurfaceBSpline.h>
#include <axlCore/axlAbstractVolumeBSpline.h>
#include <axlCore/axlShapeBSpline.h>

#include <vtkActor.h>
#include <vtkActorCollection.h>
#include <vtkCommand.h>
#include <vtkPolyDataMapper.h>
#include <vtkDataSetMapper.h>
#include <vtkPolyData.h>
#include <vtkUnstructuredGrid.h>
#include <vtkProperty.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkSmartPointer.h>
#include <vtkSphereSource.h>
#include <vtkVersion.h>

// /////////////////////////////////////////////////////////////////
// axlActorBSplinePrivate
// /////////////////////////////////////////////////////////////////

class axlActorBSplinePrivate
{
public:
    axlControlPointsWidget *controlPoints;
    vtkSmartPointer<vtkSphereSource> sphereParam;
    vtkSmartPointer<vtkDataSetMapper> mapperSphereParam;
    vtkSmartPointer<vtkActor> actorSphereParam;
};

void axlActorBSpline::setDisplay(bool display)
{
    axlActor::setDisplay(display);

    if(display && this->getControlPoints()){
        this->showControlPoints(true);
    }

    if(!display && this->getControlPoints()){
        this->showControlPoints(false);
    }
}

void axlActorBSpline::initCurrentPoint(void)
{
    d->sphereParam = vtkSmartPointer<vtkSphereSource>::New();
    d->sphereParam->SetPhiResolution(20);
    d->sphereParam->SetThetaResolution(20);
    d->sphereParam->SetRadius(0.05);
    d->sphereParam->SetCenter(0.0, 0.0, 0.0);

    d->mapperSphereParam = vtkSmartPointer<vtkDataSetMapper>::New();
#if (VTK_MAJOR_VERSION <= 5)
    d->mapperSphereParam->SetInput(d->sphereParam->GetOutput());
#else
    d->mapperSphereParam->SetInputData(d->sphereParam->GetOutput());
#endif

    d->actorSphereParam = vtkSmartPointer<vtkActor>::New();
    d->actorSphereParam->SetMapper(d->mapperSphereParam);
    d->actorSphereParam->SetVisibility(false);

    this->AddPart(d->actorSphereParam);
}

void axlActorBSpline::setMode(int state)
{
    this->onModeChanged(state);

    emit stateChanged(this->data(), state);
}

void axlActorBSpline::showControlPoints(bool show)
{
    if(!this->getControlPoints()) {
        qDebug() << "No tet actor computed for this axlActorBSpline.";
        return;
    }

    if(show){
        this->getControlPoints()->SetEnabled(1);
    }

    if(!show){
        this->getControlPoints()->SetEnabled(0);
    }

}

void axlActorBSpline::setControlPolygon(bool control)
{
    if(control) {

        if (!this->getControlPoints()) {
            //widget drawing
            this->setControlPoints(axlControlPointsWidget::New());
            this->getControlPoints()->SetInteractor(this->getInteractor());
            this->getControlPoints()->SetProp3D(this->getActor());
            this->getControlPoints()->setSpline(this->data());
            this->getControlPoints()->initializePoints();
            this->getControlPoints()->PlaceWidget();

            this->AddPart(this->getControlPoints()->netActor());

            this->getControlPoints()->ptsActors()->InitTraversal();

            for (vtkIdType i = 0; i < this->getControlPoints()->ptsActors()->GetNumberOfItems(); i++) {
                this->AddPart(this->getControlPoints()->ptsActors()->GetNextActor());
            }
        }

        if (this->getObserver()) {
            this->addToObserver(vtkCommand::InteractionEvent, (vtkCommand *)this->getObserver());
        }

       //there is always the controlPoints here
        this->getControlPoints()->SetEnabled(true);
    }

    if (!control) {
        if (this->getActor()) {
            if (this->getObserver()) {

#if (VTK_MAJOR_VERSION <= 5)
                if (dynamic_cast<axlAbstractVolumeBSpline *> (this->getObserverData())) {
                    this->getDataSetMapper()->SetInput(this->getUnstructuredGrid());
                } else if (axlActorCurveBSpline* c = dynamic_cast<axlActorCurveBSpline *> (this)) {
                        this->getMapper()->SetInputData(c->getCurveMapperInput());
                } else {
                    this->getMapper()->SetInputData(this->getPolyData());
                }
#else
                if (dynamic_cast<axlAbstractVolumeBSpline *> (this->getObserverData())) {
                    this->getDataSetMapper()->SetInputData(this->getUnstructuredGrid());
                } else if (axlActorCurveBSpline* c = dynamic_cast<axlActorCurveBSpline *> (this)) {
                        this->getMapper()->SetInputData(c->getCurveMapperInput());
                } else
                        this->getMapper()->SetInputData(this->getPolyData());
#endif
            }

            // if the control points are existing, just remove them
            if (this->getControlPoints()) {
                this->RemovePart(this->getControlPoints()->netActor());
                this->getControlPoints()->ptsActors()->InitTraversal();
                for (vtkIdType i = 0; i < this->getControlPoints()->ptsActors()->GetNumberOfItems(); i++) {
                    this->RemovePart(this->getControlPoints()->ptsActors()->GetNextActor());
                }

                this->getControlPoints()->SetEnabled(false);
                this->getControlPoints()->Delete(); // warning not sure
                this->setControlPoints(NULL);
            }
        }
    }
}

bool axlActorBSpline::isShowControlPoints(void)
{
    if(!this->getControlPoints()) {
        qDebug() << "No tet actor computed for this axlActorBSpline.";
        return false;
    }

    return this->getControlPoints()->GetEnabled();
}

axlControlPointsWidget *axlActorBSpline::getControlPoints(void)
{
    return d->controlPoints;
}

void axlActorBSpline::setControlPoints(axlControlPointsWidget *controlPoints)
{
    d->controlPoints = controlPoints;
}

void axlActorBSpline::onModeChanged(int state)
{

    if(state == 0)
    {
        this->setState(axlActor::passive);
        this->getActor()->SetPickable(1);
        this->getActor()->SetDragable(1);

        if(axlAbstractData *data = dynamic_cast<axlAbstractData *>(this->data()))
        {
            QColor color = data->color();
            this->getActor()->GetProperty()->SetColor(color.redF(), color.greenF(), color.blueF());
        }

        this->setControlPolygon(false);
    }
    else if(state == 1)
    {
        this->setState(axlActor::selection);
        this->setControlPolygon(false);
        this->getActor()->SetPickable(1);
        this->getActor()->SetDragable(1);
        vtkProperty *prop = this->getActor()->GetProperty();

        if(axlAbstractData *data = dynamic_cast<axlAbstractData *>(this->data()))
        {
            QColor color = data->color();
            qreal *h = new qreal(0.0);
            qreal *s = new qreal(0.0);
            qreal *l = new qreal(0.0);
            color.getHslF(h, s, l);
            color.setHslF(*h, *s, *l + (1.0 - *l) / 2.0);
            prop->SetColor(color.redF(), color.greenF(), color.blueF());
        }

    }
    else if(state == 2)
    {
        this->setState(axlActor::edition);
        this->setControlPolygon(true);
        this->getActor()->SetPickable(0);
        this->getActor()->SetDragable(0);
        vtkProperty *prop = this->getActor()->GetProperty();
        if(axlAbstractData *data = dynamic_cast<axlAbstractData *>(this->data()))
        {
            QColor color = data->color();
            qreal *h = new qreal(0.0);
            qreal *s = new qreal(0.0);
            qreal *l = new qreal(0.0);
            color.getHslF(h, s, l);
            color.setHslF(*h, *s, *l + (1.0 - *l)/2.0);
            prop->SetColor(color.redF(), color.greenF(), color.blueF());
        }
    }

    this->Modified();
}

void axlActorBSpline::onControlPointChanged()
{
    this->onSamplingChanged();
}

void axlActorBSpline::addToObserver(int idEvent, vtkCommand *observer)
{
    this->getControlPoints()->AddObserver(idEvent, observer);
}


void axlActorBSpline::showCurrentPoint(double u, double v, dtkAbstractData *data)
{
    if(data == this->data())
    {
        d->actorSphereParam->SetVisibility(true);
        d->actorSphereParam->Modified();
    }
}

void axlActorBSpline::moveCurrentPoint(double u, double v, dtkAbstractData *data)
{
    if(data == this->data())
    {
        axlPoint point;

        if(axlAbstractCurveBSpline *curve = dynamic_cast<axlAbstractCurveBSpline *>(this->data()))
            point = curve->eval(u);

        if(axlAbstractSurfaceBSpline *surface = dynamic_cast<axlAbstractSurfaceBSpline *>(this->data()))
            point = surface->eval(u,v);

        d->actorSphereParam->SetPosition(point.x(), point.y(), point.z());

        d->actorSphereParam->Modified();
    }
}

void axlActorBSpline::hideCurrentPoint(double u, double v, dtkAbstractData *data)
{
    if(data == this->data())
    {
        d->actorSphereParam->SetVisibility(false);

        d->actorSphereParam->Modified();
    }
}
void axlActorBSpline::onRemoved(void)
{
    this->deleteObserver();
    this->RemoveAllObservers();
    this->RemovePart(this->getActor());
    this->getActor()->RemoveAllObservers();

    if(axlActorComposite *actorComposite = dynamic_cast<axlActorComposite *>(this->parent()) )
        actorComposite->removeActorReference(this);

    if(d->controlPoints)
        this->setControlPolygon(false);
}


axlActorBSpline::axlActorBSpline(void) : axlActor(), d(new axlActorBSplinePrivate)
{
    d->controlPoints = NULL;
}

axlActorBSpline::~axlActorBSpline(void)
{
    delete d;

    d = NULL;
}
