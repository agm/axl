/* axlActorMesh.h ---
 *
 * Author: Meriadeg Perrinel
 * Copyright (C) 2008 - Meriadeg Perrinel, Inria.
 * Created: Fri Dec 17 10:58:38 2010 (+0100)
 * Version: $Id$
 * Last-Updated: Mon Dec 17 14:11:02 2012 (+0100)
 *           By: Julien Wintz
 *     Update #: 14
 */

/* Commentary:
 *
 */

/* Change log:
 *
 */

#ifndef AXLACTORMESH_H
#define AXLACTORMESH_H

#include "axlActor.h"

#include "axlVtkViewPluginExport.h"

#include <vtkVersion.h>

class axlMesh;
class axlActorMeshPrivate;

class AXLVTKVIEWPLUGIN_EXPORT axlActorMesh : public axlActor
{
public:
#if (VTK_MAJOR_VERSION <= 5)
    vtkTypeRevisionMacro(axlActorMesh, vtkAssembly);
#endif

    static axlActorMesh *New(void);

public:
    dtkAbstractData *data(void);

public:
    void setData(dtkAbstractData *data);

public:
    virtual void setMode(int state);

    virtual void setSize(double size);

    void computeNormals(void);

    QString identifier(void);

public slots:
    virtual void onModeChanged(int state);
    virtual void onTubeFilterRadiusChanged(double radius);
    virtual void onUpdateGeometry();
    //void onSelectionChanged(void);

protected:
    axlActorMesh(void);
    ~axlActorMesh(void);

private:
    axlActorMesh(const axlActorMesh&); // Not implemented.
    void operator = (const axlActorMesh&); // Not implemented.

private:
    axlActorMeshPrivate *d;
};

axlAbstractActor *createAxlActorMesh(void);

#endif //AXLACTORMesh_H
