/* axlActor.cpp ---
 *
 * Author: Meriadeg Perrinel
 * Copyright (C) 2008 - Meriadeg Perrinel, Inria.
 * Created: Fri Dec 17 10:59:21 2010 (+0100)
 * Version: $Id$
 * Last-Updated: Wed Mar 16 21:51:16 2011 (+0100)
 *           By: Meriadeg Perrinel
 *     Update #: 23
 */

/* Commentary:
 *
 */

/* Change log:
 *
 */

#include "axlActor.h"

#include <axlCore/axlAbstractCurveBSpline.h>
#include <axlCore/axlAbstractSurfaceBSpline.h>
#include <axlCore/axlAbstractVolumeBSpline.h>
#include <axlCore/axlShapeBSpline.h>
#include "axlCore/axlMesh.h"


#include "axlActorCurveBSpline.h"
#include "axlActorSurfaceBSpline.h"
#include "axlActorShapeBSpline.h"
#include "axlActorVolumeBSpline.h"
#include "axlActorComposite.h"


#include <vtkActor.h>
#include <vtkVolume.h>
#include <vtkAssemblyNode.h>
#include <vtkAssemblyPath.h>
#include <vtkCellArray.h>
#include <vtkCellData.h>
#include <vtkCellPicker.h>
#include <vtkDoubleArray.h>
#include <vtkCommand.h>
#include <vtkMatrix4x4.h>
#include <vtkPoints.h>
#include <vtkPointData.h>
#include <vtkPolyData.h>
#include <vtkDataSet.h>
#include <vtkPolyDataMapper.h>
#include <vtkDataSetMapper.h>
//#include <vtkUnstructuredGridVolumeMapper.h>
#include <vtkProp3DCollection.h>
#include <vtkProperty.h>
#include <vtkRenderer.h>
#include <vtkRendererCollection.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkSmartPointer.h>
#include <vtkTimerLog.h>
#include <vtkUnstructuredGrid.h>

#include <vtkShader.h>
//#include <vtkShader2Collection.h>
#include <vtkShaderProgram.h>
#include <vtkOpenGLProperty.h>

class axlActorControlPolygonObserver : public vtkCommand
{
public:
    static axlActorControlPolygonObserver *New(void)
    {
        return new axlActorControlPolygonObserver;
    }

    virtual void Execute(vtkObject *caller, unsigned long event, void *)
    {
        vtkRenderWindowInteractor *interactor = observerDataAssembly->getInteractor();
        if(event == vtkCommand::InteractionEvent) {
            if (dynamic_cast <axlAbstractCurveBSpline *>(observerData)) {
                if(axlActorCurveBSpline *actorCurveBSpline = dynamic_cast<axlActorCurveBSpline *>(observerDataAssembly))
                    actorCurveBSpline->pointsUpdate();
            }
            else if (dynamic_cast <axlAbstractSurfaceBSpline *>(observerData)) {
                if(axlActorSurfaceBSpline *actorSurfaceBSpline = dynamic_cast<axlActorSurfaceBSpline *>(observerDataAssembly))
                    actorSurfaceBSpline->pointsUpdate();

            }
            else if (dynamic_cast <axlAbstractVolumeBSpline *>(observerData)) {
                if(axlActorVolumeBSpline *actorVolumeBSpline = dynamic_cast<axlActorVolumeBSpline *>(observerDataAssembly))
                    actorVolumeBSpline->pointsUpdate();
            }
            else if (dynamic_cast <axlShapeBSpline *>(observerData)) {
                if(axlActorShapeBSpline *actorShapeBSpline = dynamic_cast<axlActorShapeBSpline *>(observerDataAssembly))
                    actorShapeBSpline->pointsUpdate();
            }

            if (dynamic_cast<axlAbstractData *>(observerData)->fields().count() != 0) {
                dynamic_cast<axlAbstractData *>(observerData)->touchField();
                dynamic_cast<axlAbstractData *>(observerData)->touchGeometry();
            }
            observerData_points->Modified();
        }// endif event == vtkCommand::InteractionEvent

        if (event == vtkCommand::LeftButtonPressEvent && interactor->GetShiftKey()) {
            // Actor State processing
            if (observerDataAssembly->getState() == axlActor::passive)
                this->pickAndSetMode(axlActor::selection,false);

            else if (observerDataAssembly->getState() == axlActor::selection)
                this->pickAndSetMode(axlActor::passive,false);
        }//end left button press event

        if(event == vtkCommand::RightButtonPressEvent && interactor->GetShiftKey()) {

            if (interactor->GetControlKey())
            {
                if(observerDataAssembly->getState()==axlActor::edition)
                     this->pickAndSetMode(axlActor::selection,true);
            }
            else
                this->pickAndSetMode(axlActor::passive,true);
        }//end if right button press event

        if(event == vtkCommand::KeyPressEvent) {
            vtkRenderWindowInteractor *interactor = observerDataAssembly->getInteractor();
            QString modifier(interactor->GetKeySym());
            if(modifier.compare("e") == 0 && observerDataAssembly->getState()==axlActor::selection)//Tab Active : mode editable change
                observerDataAssembly->setMode(axlActor::edition);

            if(modifier.compare("u") == 0 && observerDataAssembly->getState()==axlActor::edition)//Tab Active : mode editable change
                observerDataAssembly->setMode(axlActor::selection);
        }
    }//execute

    virtual void setData(dtkAbstractData *data)
    {
        observerData = data ;
    }

    virtual dtkAbstractData * getData(void)
    {
        return observerData;
    }

    // define differents behavior when user pick on a actor with keyboard active or not and mouse event.
    virtual void pickAndSetMode(int state, bool pathNull)
    {
        //double currentTime = vtkTimerLog::GetCPUTime();
        //double start = clock();
        // we need some Render coordonate (only one render for the moment)
        vtkSmartPointer<vtkAssemblyPath> path;
        vtkRenderWindowInteractor *interactor = observerDataAssembly->getInteractor();
        vtkSmartPointer<vtkRenderWindow> renderWindow = interactor->GetRenderWindow();
        vtkSmartPointer<vtkRendererCollection> rendererCollection = renderWindow->GetRenderers();
        vtkSmartPointer<vtkRenderer> render = rendererCollection->GetFirstRenderer();
        int X = interactor->GetEventPosition()[0];
        int Y = interactor->GetEventPosition()[1];

        if (!render || !render->IsInViewport(X, Y))
        {
            qDebug()<<" No renderer or bad cursor coordonates in axlActor";
        }

        /*vtkPropCollection *propSet = observerDataPicker->GetPickList();
        bool isNull = propSet->GetLastProp()==NULL;
        qDebug()<<"execute " <<propSet->GetNumberOfItems()<<isNull;*/

        //observerDataPicker->InitializePickList();
        //observerDataPicker->GetPickFromList();

        observerDataPicker->Pick(X,Y,0.0,render);

        path = observerDataPicker->GetPath();

        if (!pathNull && path != NULL)
        {
            //qDebug()<<"axlActor::pickAndSetMode"<<observerDataPicker<<observerDataPicker->GetProp3D()->GetPickable()<<path->GetLastNode()->GetViewProp();
            vtkCollectionSimpleIterator listPropIterator;
            path->InitTraversal(listPropIterator);
            vtkSmartPointer<vtkAssemblyNode> node;
            //bool found=0;
            /*for(int i = 0; i < path->GetNumberOfItems() && !found ; i++)
            {
                 node = path->GetNextNode(listPropIterator);
                 if(node->GetViewProp() == vtkProp::SafeDownCast(observerData_actor))
                 {
                     qDebug()<<"axlActor::pickAndSetMode";
                     observerDataAssembly->setMode(state);
                     found = 1;
                 }
            }**/
            // qDebug()<<" axlActor :: pickAndSetMode";
            if(path->GetLastNode()->GetViewProp() == vtkProp::SafeDownCast(observerData_actor))
            {
                //observerDataAssembly->setMode(state);
            }

            /*qDebug()<<"End Time pickAndSetMode "<<vtkTimerLog::GetCPUTime()<< vtkTimerLog::GetCPUTime() - currentTime;
            double end = clock();
            double cpuTime = difftime(end, start)/CLOCKS_PER_SEC;
            qDebug()<<"axlVtkView cpu time "<<cpuTime;    //d->widget->update() ;*/
        }
        else if(pathNull && path==NULL)
        {
            observerDataAssembly->setMode(state);
        }

    }
    axlActor *observerDataAssembly;
    vtkSmartPointer<vtkActor> observerData_actor;
    vtkSmartPointer<vtkCellPicker> observerDataPicker;
    dtkAbstractData *observerData;
    vtkSmartPointer<vtkPoints> observerData_points;
    vtkSmartPointer<vtkPolyData> observerData_polyData;
    // useful for axlActorVolumeBSpline
    vtkSmartPointer<vtkUnstructuredGrid> observerData_UnstructuredGrid;
};



// /////////////////////////////////////////////////////////////////
// axlActorPrivate
// /////////////////////////////////////////////////////////////////

class axlActorPrivate
{
public:
    vtkSmartPointer<vtkPoints> points;
    vtkSmartPointer<vtkPolyData> polyData;
    vtkSmartPointer<vtkUnstructuredGrid> unstructuredGrid;
    vtkSmartPointer<vtkCellArray> cellArray;
    vtkSmartPointer<vtkDoubleArray> normals;
    vtkSmartPointer<vtkActor> actor;
    vtkSmartPointer<vtkPolyDataMapper> mapper;
    //To use if necessary
    vtkSmartPointer<vtkVolume> actor3D;
    vtkSmartPointer<vtkDataSetMapper> mapper3D;
    vtkRenderWindowInteractor *interactor;
    vtkSmartPointer<vtkCellPicker> CpCursorPicker;
    axlActorControlPolygonObserver *observer;
    // axlActor::ActorState state;

    int State;

    QString active_scalars;
};

bool axlActor::isVisible(void)
{
    return (bool)(this->GetVisibility());
}

void axlActor::setInteractor(void *interactor)
{
    if(interactor)
        d->interactor = static_cast<vtkRenderWindowInteractor *>(interactor);
    else
        d->interactor = NULL;
}



vtkRenderWindowInteractor *axlActor::getInteractor(void)
{
    return d->interactor;
}


dtkAbstractData * axlActor::getObserverData(void)
{
    return d->observer->getData();
}

vtkSmartPointer<vtkPoints> axlActor::getPoints(void)
{
    return d->points;
}

void axlActor::setPoints(vtkSmartPointer<vtkPoints> points)
{
    d->points = points;
}

vtkSmartPointer<vtkDoubleArray> axlActor::getNormals(void)
{
    return d->normals;
}

void axlActor::setNormals(vtkSmartPointer<vtkDoubleArray> normals)
{
    d->normals = normals;
}

vtkSmartPointer<vtkPolyData> axlActor::getPolyData(void)
{
    return d->polyData;
}

void axlActor::setPolyData(vtkSmartPointer<vtkPolyData> polyData)
{
    d->polyData = polyData;
}



vtkSmartPointer<vtkCellArray> axlActor::getCellArray(void)
{
    return d->cellArray;
}

void axlActor::setCellArray(vtkSmartPointer<vtkCellArray> cellArray)
{
    d->cellArray = cellArray;
}

vtkSmartPointer<vtkActor> axlActor::getActor(void)
{
    return d->actor;
}

void axlActor::setActor(vtkSmartPointer<vtkActor> actor)
{
    d->actor = actor;
}

vtkSmartPointer<vtkPolyDataMapper> axlActor::getMapper(void)
{
    return d->mapper;
}

void axlActor::setMapper(vtkSmartPointer<vtkPolyDataMapper> mapper)
{
    d->mapper = mapper;
}


// To use if necessary
vtkSmartPointer<vtkVolume> axlActor::getvtkVolume(void){
    return d->actor3D;
}

void axlActor::setvtkVolume(vtkSmartPointer<vtkVolume> actor){
    d->actor3D = actor;
}

vtkSmartPointer<vtkDataSetMapper> axlActor::getDataSetMapper(void){
    return d->mapper3D;
}

void axlActor::setDataSetMapper(vtkSmartPointer<vtkDataSetMapper> mapper){
    d->mapper3D = mapper;
}


vtkSmartPointer<vtkCellPicker> axlActor::getCellPicker(void)
{
    return d->CpCursorPicker;
}

void axlActor::setCellPicker(vtkSmartPointer<vtkCellPicker> cellPicker)
{
    d->CpCursorPicker = cellPicker;
}

axlActorControlPolygonObserver *axlActor::getObserver(void)
{
    return d->observer;
}

void axlActor::setObserver(axlActorControlPolygonObserver *observer)
{
    d->observer = observer;
}

vtkSmartPointer<vtkUnstructuredGrid> axlActor::getUnstructuredGrid(void)
{
    return d->unstructuredGrid;
}

void axlActor::setUnstructuredGrid(vtkSmartPointer<vtkUnstructuredGrid> unstructuredGrid)
{
    d->unstructuredGrid = unstructuredGrid;
}

void axlActor::NewObserver()
{
    d->observer = axlActorControlPolygonObserver::New();

    d->observer->observerDataAssembly = this;
    d->observer->observerDataPicker = this->getCellPicker();
    d->observer->observerData_points = this->getPoints();

    d->observer->observerData_polyData = this->getPolyData();
    d->observer->observerData_UnstructuredGrid = this->getUnstructuredGrid();

    d->observer->observerData_actor = this->getActor();

    d->interactor->AddObserver(vtkCommand::LeftButtonPressEvent, d->observer);
    d->interactor->AddObserver(vtkCommand::RightButtonPressEvent, d->observer);
    d->interactor->AddObserver(vtkCommand::InteractionEvent, d->observer);
    d->interactor->AddObserver(vtkCommand::KeyPressEvent, d->observer);
}

void axlActor::deleteObserver()
{
    if(d->observer) {
        d->interactor->RemoveObservers(vtkCommand::LeftButtonPressEvent, d->observer);
        d->interactor->RemoveObservers(vtkCommand::RightButtonPressEvent, d->observer);
        d->interactor->RemoveObservers(vtkCommand::InteractionEvent, d->observer);
        d->interactor->RemoveObservers(vtkCommand::KeyPressEvent, d->observer);

        d->observer->observerDataAssembly = NULL;
        d->observer->observerDataPicker = NULL;
        d->observer->observerData_points = NULL;
        d->observer->observerData_polyData = NULL;
        d->observer->observerData_UnstructuredGrid = NULL;
        d->observer->observerData_actor = NULL;
        d->observer->Delete();
        d->observer = NULL;
    }
}

void axlActor::setObserverData(dtkAbstractData *data)
{
    d->observer->setData(data);
}

int axlActor::getState(void)
{
    return d->State;
}

void axlActor::setState(int state)
{
    d->State = state;
}


void axlActor::setDisplay(bool display)
{
    if (!this->getActor()) {
        qDebug() << "No tet actor computed for this axlActor";
        return;
    }

    if (display) {
        if (!this->GetParts()->IsItemPresent(this->getActor())) {
            this->VisibilityOn();
        }
    }

    if (!display){
        if (this->GetParts()->IsItemPresent(this->getActor())!=0) {
            this->VisibilityOff();
        }
    }
}


void axlActor::setOpacity(double opacity)
{
    Q_UNUSED(opacity);

    if (axlAbstractData *data = dynamic_cast<axlAbstractData *>(this->data())) {
        double opa = data->opacity();
        d->actor->GetProperty()->SetOpacity(opa);
    }
}

void axlActor::setSize(double size)
{
//    Q_UNUSED(size);

    // if(axlAbstractData *data = dynamic_cast<axlAbstractData *>(this->data()))
    if (dynamic_cast<axlAbstractData *>(this->data())) {
//        double opa = data->size();
        d->actor->SetScale(size);
    }
}

void axlActor::setInterpolation(int interpolation)
{
    d->actor->GetProperty()->SetInterpolation(interpolation);

    if (axlMesh *data = dynamic_cast<axlMesh *>(this->data()))
        data->setInterpolation(interpolation);
}

void axlActor::setColor(double red, double green, double blue)
{
    Q_UNUSED(red);
    Q_UNUSED(green);
    Q_UNUSED(blue);

    if (axlAbstractData *data = dynamic_cast<axlAbstractData *>(this->data())) {
        vtkProperty *prop = this->getActor()->GetProperty();
        QColor color = data->color();

        if (this->getState() == axlActor::passive) {
            prop->SetColor(color.redF(), color.greenF(), color.blueF());

        }
        else if (this->getState() == axlActor::selection || this->getState() == axlActor::edition) {
            qreal *h = new qreal(0.0);
            qreal *s = new qreal(0.0);
            qreal *l = new qreal(0.0);
            color.getHslF(h, s, l);
            color.setHslF(*h, *s, *l + (1.0 - *l) / 2.0);
            prop->SetColor(color.redF(), color.greenF(), color.blueF());
        }
    }

    this->Modified();
}

void axlActor::setShader(QString vsfile)
{
    // verify first that the vertex shader file exist :
    vtkProperty *prop = this->getActor()->GetProperty();

    if (QFile::exists(vsfile)) {
        QFileInfo file_info = QFileInfo(vsfile);

        QString shader_vertex_extension = ".vs";
        QString shader_fragment_extension = ".fs";

        QString vertex_shader_source = dtkReadFile(file_info.path() + "/" +
                                                   file_info.completeBaseName() +
                                                   shader_vertex_extension);

        QString fragment_shader_source = dtkReadFile(file_info.path() + "/" +
                                                     file_info.completeBaseName() +
                                                     shader_fragment_extension);

        this->setShaderFromString(vertex_shader_source, fragment_shader_source);
        prop->GetShading();
    }
    else {
        prop->vtkProperty::ShadingOff();
    }

    this->Modified();
}

void axlActor::setShaderFromString(QString vertex_shader_source,
                                   QString fragment_shader_source)
{
    vtkProperty *prop = this->getActor()->GetProperty();

    if ((vertex_shader_source != "") && (fragment_shader_source != "")) {
        //The GLSL binded program
        vtkSmartPointer<vtkShaderProgram> program = vtkSmartPointer<vtkShaderProgram>::New();
        // program->SetContext(renWin);

        // Prepare the fragment shader
        vtkSmartPointer<vtkShader> fragment_shader = vtkSmartPointer<vtkShader>::New();

//        fragment_shader->SetType(VTK_SHADER_TYPE_FRAGMENT);
        fragment_shader->SetType(vtkShader::Type::Fragment);
//        fragment_shader->SetSourceCode(fragment_shader_source.toStdString().c_str());
        fragment_shader->SetSource(fragment_shader_source.toStdString().c_str());
//        fragment_shader->SetContext(program->GetContext());

        // Prepare the vertex shader
        vtkSmartPointer<vtkShader> vertex_shader = vtkSmartPointer<vtkShader>::New();

//        vertex_shader->SetType(VTK_SHADER_TYPE_VERTEX);
        vertex_shader->SetType(vtkShader::Type::Vertex);
//        vertex_shader->SetSourceCode(vertex_shader_source.toStdString().c_str());
        vertex_shader->SetSource(vertex_shader_source.toStdString().c_str());
//        vertex_shader->SetContext(program->GetContext());

//        program->GetShaders()->AddItem(vertex_shader);
        program->SetVertexShader(vertex_shader);
//        program->GetShaders()->AddItem(fragment_shader);
        program->SetFragmentShader(fragment_shader);

        // TODO:: fix this for openGL2
        vtkSmartPointer<vtkOpenGLProperty> openGLproperty = static_cast<vtkOpenGLProperty*>(prop);
//        openGLproperty->SetPropProgram(program);

        // activate the shading
        prop->vtkProperty::ShadingOn();
    }
    else {
        prop->vtkProperty::ShadingOff();
    }

}


// slots---------------------------

void axlActor::hide(void)
{
    this->setMode(0);
    this->VisibilityOff();
    this->Modified();
}

void axlActor::show(void)
{
    this->VisibilityOn();
    this->Modified();
}

void axlActor::update(void)
{
    this->Modified();
}

void axlActor::onRemoved(void)
{
    this->deleteObserver();
    this->RemoveAllObservers();
    this->RemovePart(this->getActor());
    this->getActor()->RemoveAllObservers();

    if(axlActorComposite *actorComposite = dynamic_cast<axlActorComposite *>(this->parent()) )
        actorComposite->removeActorReference(this);

    //this->getPoints()->Delete();
    //this->getCellArray()->Delete();
    // delete current vtkPoint and vtkCellArray
    //this->getPoints()->Reset();
    //->Reset();

    //this->getPoints()->Squeeze();
    //this->getCellArray()->Squeeze();


    //this->getPolyData()->Reset();
    //this->getPolyData()->Squeeze();

    //this->getMapper()->Delete();
    //this->getActor()->Delete();

    //this->Delete();
}


//axlAbstractActor *axlActor::createActor(dtkAbstractData *data){

//    if (axlPoint *point = dynamic_cast<axlPoint *>(data)) {

//        axlActorPoint *actor = axlActorPoint::New();
//        return actor;
//    }

//    else if (axlCone *cone = dynamic_cast<axlCone *>(data)) {

//        axlActorCone *actor = axlActorCone::New();
//        return actor;
//    }

//    else if (axlCylinder *cylinder = dynamic_cast<axlCylinder *>(data)) {

//        axlActorCylinder *actor = axlActorCylinder::New();
//        return actor;
//    }

//    else if (axlTorus *torus = dynamic_cast<axlTorus *>(data)) {

//        axlActorTorus *actor = axlActorTorus::New();
//        return actor;
//    }

//    //    else if (axlCircleArc *arc = dynamic_cast<axlCircleArc *>(data)) {

//    //        axlActorCircleArc *actor = axlActorCircleArc::New();
//    //        return actor;
//    //    }

//    else if (axlEllipsoid *ellipsoid = dynamic_cast<axlEllipsoid *>(data)) {

//        axlActorEllipsoid *actor = axlActorEllipsoid::New();
//        return actor;
//    }
//    else if (axlLine *line = dynamic_cast<axlLine *>(data)) {

//        axlActorLine *actor = axlActorLine::New();
//        return actor;
//    }
//    else if (axlPlane *plane = dynamic_cast<axlPlane *>(data)) {

//        axlActorPlane *actor = axlActorPlane::New();
//        return actor;
//    }
//    else if (axlSphere *sphere = dynamic_cast<axlSphere *>(data)) {

//        axlActorSphere *actor = axlActorSphere::New();
//        return actor;
//    }

//    else if (axlAbstractCurveBSpline *spline_curve = dynamic_cast<axlAbstractCurveBSpline *>(data)) {

//        axlActorCurveBSpline *actor = axlActorCurveBSpline::New();
//        return actor;
//    }

//    else if(axlAbstractSurfaceBSpline *spline_surface = dynamic_cast<axlAbstractSurfaceBSpline *>(data)) {

//        axlActorSurfaceBSpline *actor = axlActorSurfaceBSpline::New();
//        return actor;
//    }
//    else if(axlAbstractVolumeBSpline *spline_volume = dynamic_cast<axlAbstractVolumeBSpline *>(data)) {

//        axlActorVolumeBSpline *actor = axlActorVolumeBSpline::New();
//        return actor;
//    }

//    else if(axlAbstractDataComposite *composite = dynamic_cast<axlAbstractDataComposite *>(data))
//    {
//        axlActorComposite *actor = axlActorComposite::New();
//        return actor;
//    }else if (axlDataDynamic *line = dynamic_cast<axlDataDynamic *>(data)) {

//        axlActorDataDynamic *actor = axlActorDataDynamic::New();
//        return actor;
//    }else
//    {
//        axlActorMesh *actor = axlActorMesh::New();
//        return actor;
//    }

//}




QStringList axlActor::fields(void)
{
    QStringList fields;

    if(d->polyData){
        for(int i = 0; i < d->polyData->GetPointData()->GetNumberOfArrays(); i++)
            fields << QString(d->polyData->GetPointData()->GetArray(i)->GetName());

        for(int i = 0; i < d->polyData->GetCellData()->GetNumberOfArrays(); i++)
            fields << QString(d->polyData->GetCellData()->GetArray(i)->GetName());

        for(int i = 0; i < d->polyData->GetFieldData()->GetNumberOfArrays(); i++)
            fields << QString(d->polyData->GetFieldData()->GetArray(i)->GetName());
    }
    if(d->unstructuredGrid){
        for(int i = 0; i < d->unstructuredGrid->GetPointData()->GetNumberOfArrays(); i++)
            fields << QString(d->unstructuredGrid->GetPointData()->GetArray(i)->GetName());

        for(int i = 0; i < d->unstructuredGrid->GetCellData()->GetNumberOfArrays(); i++)
            fields << QString(d->unstructuredGrid->GetCellData()->GetArray(i)->GetName());

        for(int i = 0; i < d->unstructuredGrid->GetFieldData()->GetNumberOfArrays(); i++)
            fields << QString(d->unstructuredGrid->GetFieldData()->GetArray(i)->GetName());
    }


    return fields;
}

void axlActor::drawAssymblyMatrix(void)
{
    double matrix[16]; this->GetMatrix(matrix);
    qDebug()<< " Matrix of the assembly " <<this;
    for(int i = 0 ; i < 4 ; i++)
        qDebug()<<matrix[4 * i + 0]<<matrix[4 * i + 1]<<matrix[4 * i + 2]<<matrix[4 * i + 3];
    qDebug()<< "\n";
}


axlActor::axlActor(void) : axlAbstractActor(), vtkAssembly(), d(new axlActorPrivate)
{
    d->State = axlActor::passive;
    d->CpCursorPicker = vtkSmartPointer<vtkCellPicker>::New();
    d->CpCursorPicker->SetTolerance(0.001);
    d->observer = NULL;
}

axlActor::~axlActor(void)
{
    delete d;

    d = NULL;
}
