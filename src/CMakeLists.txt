### CMakeLists.txt --- 
## 
## Author: Julien Wintz
## Copyright (C) 2008 - Julien Wintz, Inria.
## Created: Tue Nov  9 16:47:48 2010 (+0100)
## Version: $Id$
## Last-Updated: Tue Nov  9 16:48:01 2010 (+0100)
##           By: Julien Wintz
##     Update #: 2
######################################################################
## 
### Commentary: 
## 
######################################################################
## 
### Change log:
## 
######################################################################

add_subdirectory(axlCore)
add_subdirectory(axlGui)

if(JSON_RPC)
if(NOT QJSONRPC_USED)
  find_package(QJSonRpc REQUIRED)
endif(NOT QJSONRPC_USED)
if(QJSONRPC_FOUND OR QJSONRPC_USED)
   include_directories(${QJSONRPC_INCLUDE_DIR})
   set(${PKG_NAME}_INCLUDE_DIR ${${PKG_NAME}_INCLUDE_DIR} ${QJSONRPC_INCLUDE_DIR}
        CACHE STRING "" FORCE)
   link_directories(${QJSONRPC_LIBRARY_DIR})
   set(${PKG_NAME}_LIBRARY_DIR ${${PKG_NAME}_LIBRARY_DIR} ${QJSONRPC_LIBRARY_DIR}
        CACHE STRING "" FORCE)
   link_libraries(${QJSONRPC_LIBRARIES})
   add_subdirectory(axlRpc)
else(QJSONRPC_FOUND OR QJSONRPC_USED)
   message(FATAL_ERROR "QJSONRPC NOT FOUND" )
endif(QJSONRPC_FOUND OR QJSONRPC_USED)
endif(JSON_RPC)


## Installation

#install(TARGET Qt5Core LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR} )

