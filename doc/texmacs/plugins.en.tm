<TeXmacs|1.99.1>

<style|<tuple|mmxdoc|mathemagix|doxygen>>

<\body>
  <\doc-data|<doc-title|<with|font-series|bold|>The structure of a
  plugin<strong|>>|<doc-author|<author-data|<author-name|M. Perrinel>>>>
    \;
  </doc-data>

  <name|Axl> is a cross platform sofware composed of a main application and
  several plugins. In this article, we describe the structure of the code
  required for an <name|Axl> plugin. A plugin consists of a set of classes
  and some configurations files. Let's see an example of a plugin generated
  by the axel-plugin project wizard (named "<verbatim|MyPlugin>") with data
  (named "<verbatim|Default>") and a process (named "<verbatim|offset>").
  I'll explain (for each of the following classes) the important things to
  respect if you want to create your own plugin:

  <\itemize>
    <item><verbatim|MyPluginDefaultProcess>\ 

    <item><verbatim|MyPluginDefaultProcessDialog>\ 

    <item><verbatim|MyPluginDefaultData>\ 

    <item><verbatim|MyPluginDefaultDataDialog>\ 

    <item><verbatim|MyPluginDefaultDataReader>\ 

    <item><verbatim|MyPluginDefaultDataWriter>\ 

    <item><verbatim|MyPluginDefaultDataConverter>\ 

    <item><verbatim|MyPluginDefaultDataCreatorProcessDialog>\ 

    <item><verbatim|MyPluginPlugin>\ 

    <item><verbatim|MyPluginPluginExport>\ 

    <item><verbatim|CMakeList>
  </itemize>

  Before analyzing in detail each class, we first have a look at the taxonomy
  of the plugin :

  <verbatim|MyPluginDefaultProcess> and <verbatim|MyPluginDefaultProcessDialog>
  correspond to the process named <verbatim|Default>. Their generic names are
  <verbatim|\<less\>plugin_name\<gtr\>\<less\>process_name\<gtr\>Process> and
  <verbatim|\<less\>plugin_name\<gtr\>\<less\>process_name\<gtr\>ProcessDialog>,\ 

  <\itemize>
    <item><verbatim|MyPluginDefaultData>,\ 

    <item><verbatim|MyPluginDefaultDataDialog>,

    <item><verbatim|MyPluginDefaultDataReader>,\ 

    <item><verbatim|MyPluginDefaultDataWriter>,

    <item><verbatim|MyPluginDefaultDataConverter>,\ 

    <item><verbatim|MyPluginDefaultDataCreatorProcessDialog>
  </itemize>

  for the data named <verbatim|DefaultData>. Their generic names are
  <verbatim|\<less\>plugin_project_name\<gtr\>\<less\>data_name\<gtr\>Data>,
  <verbatim|\<less\>plugin_project_name\<gtr\>><verbatim|\<less\>data_name\<gtr\>><verbatim|DataDialog>,
  etc. <verbatim|MyPluginPlugin> and <verbatim|MyPluginPluginExport> files
  are for the plugin configuration and initialisation. Their generic names
  are <verbatim|\<less\>plugin_project_name\<gtr\>Plugin> and
  <verbatim|\<less\>plugin_project_name\<gtr\>><verbatim|PluginExport>.
  <verbatim|CMakeList> is its generic name.

  As you can see, a plugin for axel consists of a lot of classes. If the
  plugin config pattern is always necessary and unique, data pattern or
  process pattern isn't. You can create a plugin just for one or more
  processes and/or one ore more data. In addition, you probably already know
  that Axl directly depends of dtk library. Most of Axl classes inherit
  from one of the dtk classes. you will find more information about dtk
  library there.

  I'll continue on this example to explain plugin classes. In this
  documentation, you will see how to generate a first plugin based on this
  example. I recommend you to follow explanations of classes with the example
  opened in QtCreator.

  <subsection|Process pattern>

  <with|color|red|<verbatim|MyPluginDefaultProcess>>: this class inherits
  from <verbatim|dtkAbstractProcess>. You will create the core of your
  process there. For that you need to override or implement these methods :

  <\cpp-code>
    virtual QString description (void) const;\ 

    // The description of your process\ 

    \;

    virtual QString identifier (void) const;\ 

    // return the name of the class : MyPluginDefaultProcess\ 

    \;

    static bool registered (void);\ 

    // register the process into the dtkAbstractProcessFactory\ 

    \;

    int update (void);\ 

    // use input data or parameters to create your output.\ 

    // This method is the core of your process\ 

    \;

    void setInput (dtkAbstractData *data, int channel);\ 

    // To set you attribute input data,\ 

    // you need to override from dtkAbstractProcess all virtual method you
    need\ 

    \;

    dtkAbstractData *output (void);

    // To get you attribute output data, you need to override from\ 

    // dtkAbstractProcess all virtual method you need
  </cpp-code>

  <with|color|red|<verbatim|<strong|MyPluginDefaultProcessDialog>>:> this
  class inherits from <verbatim|axlInspectorToolInterface>. You will create
  the GUI of your process there that you can see on Axl if you select you
  process from the tool inspector (see Apply a process on a selected data if
  you don't know what I mean by tool inspector). For that you need to
  implement these methods and declare a signal :

  <\cpp-code>
    static bool registered (void);\ 

    // register the process into the dtkAbstractProcessFactory\ 

    \;

    int run (void);\ 

    // This method is called when the user clicks on the "run" button in
    Axl.\ 

    // Using widgets of GUI defined into MyPluginDefaultProcessDialog
    constructors,\ 

    // MyPluginDefaultProcess methods are called to set process attributes\ 

    // and run its update method, then you will use the dataInserted signal\ 

    // to emit new data created if existed .

    \;

    signals:\ 

    \ \ \ \ void dataInserted (axlAbstractData *data);\ 

    // Use it in run method of MyPluginDefaultProcessDialog to insert\ 

    // new data in Axl
  </cpp-code>

  \;

  <subsection|Data pattern>

  <with|color|red|<verbatim|MyPluginDefaultData:>> this class inherits from
  <code*|<hlink|axlAbstractData|file:///Users/mourrain/Devel/buildaxeldoc/html/classaxl_abstract_data.html>>.
  You will create the core of your data there. For that you need to override
  or implement virtual methods of <code*|<hlink|axlAbstractData|file:///Users/mourrain/Devel/buildaxeldoc/html/classaxl_abstract_data.html>>:

  <\cpp-code>
    virtual QString description (void) const; // The description of your data

    \;

    virtual QString identifier (void) const; // return the name of the class
    : MyPluginDefaultData

    \;

    static bool registered (void); // register the process into the
    dtkAbstractDataFactory
  </cpp-code>

  <\note>
    When you add a data in your plugin, you have to extend one of the
    abstract data class given into the abstract taxonomy of Axl data. For
    instance, BSpline surface data implementation given with BSplineTools
    plugin extend axkAbstractSurfaceBSpline directly instead of
    <code*|<hlink|axlAbstractData|file:///Users/mourrain/Devel/buildaxeldoc/html/classaxl_abstract_data.html>>
    that is more abstract. That mean you have to override all virtual method
    of <code*|<hlink|axlAbstractSurfaceBSpline|file:///Users/mourrain/Devel/buildaxeldoc/html/classaxl_abstract_surface_b_spline.html>>
    and its parents classes in addition.
  </note>

  <verbatim|<with|color|red|MyPluginDefaultDataDialog:>> this class inherits
  from <code*|axlInspectorObjectInterface>. You will create the GUI of your
  data there.that you can see on Axl if you select you process from the
  object inspector (see Apply a process on a selected data if you don't know
  what I mean by object inspector). For that you need to add all GUI elements
  you need as attributes. Some of there come from parents classes and other
  come from your specific data.

  <\cpp-code>
    static bool registered (void); // register the process into the
    axlInspectorObjectFactory

    \;

    public slots:

    void setData (<hlink|dtkAbstractData|file:///Users/mourrain/Devel/buildaxeldoc/html/classdtk_abstract_data.html>
    *data); // Initialize your data attributes

    // and run the initWidget

    \;

    private :

    void initWidget (void); // The most important method of the class.

    // All axlAbstractData have some basic properties :

    \;

    // color, opacity, size and shader. Using the data and other private
    method of the class,

    // GUI elements are initialized.

    \;

    // In addition, initialize new GUI elements you added depending of your
    specific data.

    \;

    // Then Create all connections needed to link in real time your

    // data logic representation with data graphic representation
  </cpp-code>

  <\note>
    There are some signals and other private methods I didn't mentionned
    there, because they are already implemented. There are usefull for one
    part of <verbatim|initWidget> method which will initialize
    <code*|<hlink|axlAbstractData|file:///Users/mourrain/Devel/buildaxeldoc/html/classaxl_abstract_data.html>>
    properties. Moreover, their connections are already created to. If you
    create by your self and from scratch this class, you have to add them
    manually and as identical. In other way, if you use axel-plugin wizard,
    the <verbatim|MyPluginDefaultDataDialog> will be generated with them.
  </note>

  <with|color|red|<verbatim|MyPluginDefaultDataReader:>> this class inherits
  from <code*|<hlink|axlAbstractDataReader|file:///Users/mourrain/Devel/buildaxeldoc/html/classaxl_abstract_data_reader.html>>.
  If it is not necessary to create a data plugin, it permits to create data
  by the reading an axl file. You will find more complements informations
  there. Let's see what methods you have to implements:

  <\cpp-code>
    virtual QString description (void) const; // The description of your
    reader

    \;

    virtual QString identifier (void) const; // return the name of the class
    : MyPluginDefaultDataReader

    \;

    static bool registered (void); // register the process into the
    dtkAbstractDataFactory

    \;

    virtual QStringList handled (void) const; // return list of data
    identifier that it can handle

    \;

    public:

    bool accept (const QDomNode& node); // condition of QDomNode to recognize
    your data type

    bool reject (const QDomNode& node); // return ! (this-\<gtr\>accept);

    \;

    <hlink|dtkAbstractData|file:///Users/mourrain/Devel/buildaxeldoc/html/classdtk_abstract_data.html>
    *read (const QDomNode& node); // If QDomNode is accepted,

    \;

    // you have to use parsed informations to create you data
  </cpp-code>

  <verbatim|<with|color|red|MyPluginDefaultDataWriter:>> this class inherits
  from <code*|<hlink|axlAbstractDataWriter|file:///Users/mourrain/Devel/buildaxeldoc/html/classaxl_abstract_data_writer.html>>.
  If it not necessary to create a data plugin, it permits to create an axl
  file from your data opened in Axl. You will find more complements
  informations there. Let's see what methods you have to implements:

  <\cpp-code>
    virtual QString description (void) const; // The description of your
    reader

    \;

    virtual QString identifier (void) const; // return the name of the class
    : MyPluginDefaultDataWriter

    static bool registered (void); // register the process into the
    dtkAbstractDataFactory

    \;

    virtual QStringList handled (void) const; // return list of data
    identifier that it can handle

    \;

    public:

    bool accept (<hlink|dtkAbstractData|file:///Users/mourrain/Devel/buildaxeldoc/html/classdtk_abstract_data.html>
    *data); // return true if we can\ 

    // dynamic_cast\<less\>MyPluginDefaultData *\<gtr\> (data)

    bool reject (<hlink|dtkAbstractData|file:///Users/mourrain/Devel/buildaxeldoc/html/classdtk_abstract_data.html>
    *data); // return ! (this-\<gtr\>accept);

    \;

    QDomElement write (QDomDocument *doc,
    <hlink|dtkAbstractData|file:///Users/mourrain/Devel/buildaxeldoc/html/classdtk_abstract_data.html>
    *data); // if the data is accepted

    \;

    // you have to write data attributes into a QDomElement
  </cpp-code>

  <verbatim|<with|color|red|MyPluginDefaultDataConverter:>> this class
  inherits from <code*|<hlink|axlAbstractDataConverter|file:///Users/mourrain/Devel/buildaxeldoc/html/classaxl_abstract_data_converter.html>>.
  You have already create the data, but how Axl can show your data inside
  the view ? It's possible because of this class. You will convert there your
  <verbatim|MyPluginDefaultData> into an <code*|<hlink|axlMesh|file:///Users/mourrain/Devel/buildaxeldoc/html/classaxl_mesh.html>>
  which can be displayed inside Axl view. For that you have to implements:

  <\cpp-code>
    virtual QString description (void) const; // The description of your
    converter

    \;

    static bool registered (void); // register the process into the
    dtkAbstractDataFactory

    \;

    virtual QString toType (void) const; // return the type of the output
    converter

    // : axlMesh

    \;

    virtual QStringList fromTypes (void) const; // return list of data types
    that your inherit

    \;

    public:

    \;

    <hlink|axlMesh|file:///Users/mourrain/Devel/buildaxeldoc/html/classaxl_mesh.html>*
    toMesh (void); // you have to implement it\ 

    // create a mesh from your data and use axlMesh to store it
  </cpp-code>

  <with|color|red|<verbatim|MyPluginDefaultDataCreatorProcessDialog:>> this
  class inherits from <code*|axlInspectorToolInterface> and respect pattern
  of processDialog. You will create the GUI necessary to create a
  <verbatim|MyPluginDefaultData>, that you can see on Axl if you select your
  generator from the tool inspector (see Open a data from a generator tool.
  if you don't know how to do. For this processDialog, you don't have to
  create a class <verbatim|MyPluginDefaultDataCreatorProcess>, because the
  Dialog will do that for you. For that you need to implement these methods
  and declare a signal:

  <\cpp-code>
    static bool registered (void); // register the process into the
    dtkAbstractProcessFactory

    \;

    int run (void);

    // This method is called when the user clicks on run button in Axl.

    // using widgets of GUI defined into MyPluginDefaultDataCreatorProcessDialog
    constructors,

    // MyPluginDefaultData will be created then inserted by the dataInserted
    signa,

    \;

    signals:

    void dataInserted (<hlink|axlAbstractData|file:///Users/mourrain/Devel/buildaxeldoc/html/classaxl_abstract_data.html>
    *data);

    // Use it in run method of MyPluginDefaultProcessDialog to insert new
    data in Axl
  </cpp-code>

  <subsection| Plugin Config pattern>

  <with|color|red|<verbatim|MyPluginPlugin:>> this class inherits from
  <code*|<hlink|dtkPlugin|file:///Users/mourrain/Devel/buildaxeldoc/html/classdtk_plugin.html>>.
  You will include all files of process pattern and data pattern. Then in the
  initialize method, you will call for each of them their register method. In
  addition, static variables will be defined to access singleton instance of
  used factories in your plugin:

  <\cpp-code>
    virtual QString description (void) const; // The description of your
    plugin

    \;

    virtual QString name (void) const; // return the name of the class :
    plugPlugin

    \;

    virtual QStringList types (void) const; // List data and process classes
    embedded in your plugin

    \;

    virtual bool initialize (void); // call static registered method of all
    data and process clases

    // and initialize static variables to their correspondant factory

    \;

    public:

    static <hlink|dtkAbstractDataFactory|file:///Users/mourrain/Devel/buildaxeldoc/html/classdtk_abstract_data_factory.html>
    *dataFactSingleton;

    static <hlink|dtkAbstractProcessFactory|file:///Users/mourrain/Devel/buildaxeldoc/html/classdtk_abstract_process_factory.html>
    *processFactSingleton;
  </cpp-code>

  <\note>
    Two other methods need to be implemented but are not yet used in this
    plugin. So just implement them as you want for the moment:

    <\cpp-code>
      virtual QStringList tags (void) const;

      virtual bool uninitialize (void);
    </cpp-code>
  </note>

  <with|color|red|<verbatim|MyPluginPluginExport:>> In fact, there, you have
  only one file <verbatim|plugPluginExport.h> and it's not a class. This
  files contain some preprocessor directives that permit to you plugin to get
  the cross compilation on windows with static compilation. Then for each
  class header, you add the preprocessor value (initialized by this file).
  For instance:

  <\cpp-code>
    class plugPlugin : public <hlink|dtkPlugin|file:///Users/mourrain/Devel/buildaxeldoc/html/classdtk_plugin.html>
  </cpp-code>

  becomes

  <\cpp-code>
    class PLUGPLUGIN_EXPORT plugPlugin : public
    <hlink|dtkPlugin|file:///Users/mourrain/Devel/buildaxeldoc/html/classdtk_plugin.html>
  </cpp-code>
</body>