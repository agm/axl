How to create a data and interact with it
=========================================

*Meriadeg Perrinel*

We describe basic actions in Axl to create a geometric object and manipulate it. In this example, a cylinder is created by a creator tool from the process inspector and the object is deformed from the view interactors.

1) Open Axl and click on the scene inspector (Ctrl-i) like follow :

.. image:: ../img/content_tuto1-1.png 
   :align: center
   
2) Open the tool inspector (Ctrl-2) and select the Creator tag. All creators and processes are now listed, you can select the cylinder process for example, set some parameters then click on create :

.. image:: ../img/content_tuto1-2.png 
   :align: center

3) Now, your first object is created, go to object inspector (Ctrl-1) to see it. You can switch from passive mode to selection mode from  the view or from the object inspector with a simple mouse left click. Then you can change its parameters from the GUI adapted to it :

.. image:: ../img/content_tuto1-3.png 
   :align: center

4) Interact with the cylinder directly on the view. For that, you need to switch the cylinder to the edition mode. You can double click on the cylinder item of the object inspector or click on E key if you mouse focus is under the view. Then you can see some controls widgets on the the view, please move them to see the object being transformed. To swich back from edition mode to selection mode, you can double click again on the cylinder item from the object inspector or click on U key if you mouse focus is under the view :

.. image:: ../img/content_tuto1-4.png 
   :align: center
