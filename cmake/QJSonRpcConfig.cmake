### FindQJSonRpcConfig.cmake --- 
## 
## Author: Anais Ducoffe
## Copyright (C) 2014, Anais Ducoffe Inria.
######################################################################
## 
### Commentary: 
## 
######################################################################
## 
### Change log:
## 
######################################################################

# QJSONRPC_FOUND - system has QJsonRpc
# QJSONRPC_INCLUDE_DIR - where to find QJsonRpc.h
# QJSONRPC_LIBRARIES - the libraries to link against to use QJsonRpc
# QJSONRPC_LIBRARY - where to find the QJsonRpc library (not for general use)

set(QJSONRPC_FOUND "NO")

#if(QT4_FOUND)

  find_path(QJSONRPC_INCLUDE_DIR qjsonrpcservice.h
    /usr/local/include/qjsonrpc
    /usr/include/qjsonrpc)

  set(QJSONRPC_NAMES ${QJSONRPC_NAMES} qjsonrpc)

  find_library(QJSONRPC_LIBRARY
    NAMES ${QJSONRPC_NAMES}
    PATHS /usr/local/lib /usr/lib)

  if(QJSONRPC_INCLUDE_DIR AND QJSONRPC_LIBRARY)
    set(QJSONRPC_LIBRARIES ${QJSONRPC_LIBRARY})
    set(QJSONRPC_FOUND "YES")
    
    if (CYGWIN)
      if(NOT BUILD_SHARED_LIBS)
        set(QJSONRPC_DEFINITIONS --DQJSONRPC_STATIC)
      endif(NOT BUILD_SHARED_LIBS)
    endif(CYGWIN)
    
  endif(QJSONRPC_INCLUDE_DIR AND QJSONRPC_LIBRARY)
#endif(QT4_FOUND)

mark_as_advanced(QJSONRPC_INCLUDE_DIR QJSONRPC_LIBRARY QJsonRpc_DIR)
