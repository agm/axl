#include <vtkVersion.h>
#include <vtkRendererCollection.h>
#include <vtkDataSetMapper.h>
#include <vtkUnstructuredGrid.h>
#include <vtkIdTypeArray.h>
#include <vtkTriangleFilter.h>
#include <vtkPolyDataMapper.h>
#include <vtkActor.h>
#include <vtkCommand.h>
#include <vtkRenderWindow.h>
#include <vtkRenderer.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkPoints.h>
#include <vtkCellArray.h>
#include <vtkPlaneSource.h>
#include <vtkCellPicker.h>
#include <vtkInteractorStyleTrackballCamera.h>
#include <vtkProperty.h>
#include <vtkSelectionNode.h>
#include <vtkSelection.h>
#include <vtkExtractSelection.h>
#include <vtkObjectFactory.h>

#include "axlInteractorCellPicking.h"

vtkStandardNewMacro(axlInteractorCellPicking);

#define VTKISRBP_ORIENT 0
#define VTKISRBP_SELECT 1

class axlInteractorCellPickingPrivate {
public :

    vtkSmartPointer<vtkPolyData> Data;
    vtkSmartPointer<vtkDataSetMapper> selectedMapper;
    vtkSmartPointer<vtkActor> selectedActor;

};


axlInteractorCellPicking::axlInteractorCellPicking(QObject *parent) : QObject(parent),d(new axlInteractorCellPickingPrivate){
    d->selectedMapper = vtkSmartPointer<vtkDataSetMapper>::New();
    d->selectedActor = vtkSmartPointer<vtkActor>::New();
}

axlInteractorCellPicking::~axlInteractorCellPicking(){
    delete d;
    d = NULL;
}

void axlInteractorCellPicking::setData(vtkSmartPointer<vtkPolyData> data){
    d->Data = data;
}

void axlInteractorCellPicking::OnLeftButtonDown(){
    // Get the location of the click (in window coordinates)
    int* pos = this->GetInteractor()->GetEventPosition();

    vtkSmartPointer<vtkCellPicker> picker =
            vtkSmartPointer<vtkCellPicker>::New();
    picker->SetTolerance(0.0005);

    // Pick from this location.
    picker->Pick(pos[0], pos[1], 0, this->GetDefaultRenderer());

    double* worldPosition = picker->GetPickPosition();
    std::cout << "Cell id is: " << picker->GetCellId() << std::endl;

    if(picker->GetCellId() != -1)
    {

        std::cout << "Pick position is: " << worldPosition[0] << " " << worldPosition[1]
                  << " " << worldPosition[2] << endl;

        vtkSmartPointer<vtkIdTypeArray> ids =
                vtkSmartPointer<vtkIdTypeArray>::New();
        ids->SetNumberOfComponents(1);
        ids->InsertNextValue(picker->GetCellId());

        vtkSmartPointer<vtkSelectionNode> selectionNode =
                vtkSmartPointer<vtkSelectionNode>::New();
        selectionNode->SetFieldType(vtkSelectionNode::CELL);
        selectionNode->SetContentType(vtkSelectionNode::INDICES);
        selectionNode->SetSelectionList(ids);

        vtkSmartPointer<vtkSelection> selection =
                vtkSmartPointer<vtkSelection>::New();
        selection->AddNode(selectionNode);

        vtkSmartPointer<vtkExtractSelection> extractSelection =
                vtkSmartPointer<vtkExtractSelection>::New();
#if VTK_MAJOR_VERSION <= 5
        extractSelection->SetInput(0, d->Data);
        extractSelection->SetInput(1, selection);
#else
        extractSelection->SetInputData(0, d->Data);
        extractSelection->SetInputData(1, selection);
#endif
        extractSelection->Update();

        // In selection
        vtkSmartPointer<vtkUnstructuredGrid> selected =
                vtkSmartPointer<vtkUnstructuredGrid>::New();
        selected->ShallowCopy(extractSelection->GetOutput());

        std::cout << "There are " << selected->GetNumberOfPoints()
                  << " points in the selection." << std::endl;
        std::cout << "There are " << selected->GetNumberOfCells()
                  << " cells in the selection." << std::endl;


#if VTK_MAJOR_VERSION <= 5
        d->selectedMapper->SetInputConnection(
                    selected->GetProducerPort());
#else
        d->selectedMapper->SetInputData(selected);
#endif

        d->selectedActor->SetMapper(d->selectedMapper);
        d->selectedActor->GetProperty()->EdgeVisibilityOn();
        d->selectedActor->GetProperty()->SetEdgeColor(1,0,0);
        d->selectedActor->GetProperty()->SetLineWidth(3);

        this->Interactor->GetRenderWindow()->GetRenderers()->GetFirstRenderer()->AddActor(d->selectedActor);

    }
    // Forward events
    vtkInteractorStyleTrackballCamera::OnLeftButtonDown();
}
