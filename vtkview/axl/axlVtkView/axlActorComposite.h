/* axlActorComposite.h ---
 *
 * Author: Meriadeg Perrinel
 * Copyright (C) 2008 - Meriadeg Perrinel, Inria.
 * Created: Fri Dec 17 10:58:38 2010 (+0100)
 * Version: $Id$
 * Last-Updated: Mon Dec 17 13:59:24 2012 (+0100)
 *           By: Julien Wintz
 *     Update #: 7
 */

/* Commentary:
 *
 */

/* Change log:
 *
 */

#ifndef AXLACTORCOMPOSITE_H
#define AXLACTORCOMPOSITE_H

#include <axlCore/axlAbstractActor.h>

#include <vtkAssembly.h>
#include <vtkVersion.h>

#include "axlVtkViewPluginExport.h"

class axlActorCompositeObserver;

class axlAbstractActor;

class vtkActor;
class vtkCellArray;
class vtkCellPicker;
class vtkPoints;
class vtkPolyData;
class vtkPolyDataMapper;
class vtkRenderWindowInteractor;

class axlAbstractDataComposite;
class axlAbstractView;

class axlActorCompositePrivate;


class AXLVTKVIEWPLUGIN_EXPORT axlActorComposite : public axlAbstractActor, public vtkAssembly
{
public:
   enum ActorState {
       passive,
       selection,
       edition
   };

public:
#if (VTK_MAJOR_VERSION <= 5)
    vtkTypeRevisionMacro(axlActorComposite, vtkAssembly);
#endif

    static axlActorComposite *New(void);

public:

virtual dtkAbstractData *data(void);
virtual void setData(dtkAbstractData *data1);
bool removeActorReference(axlAbstractActor *actor);

public:

    void setView(axlAbstractView *view);
    void setRenderer(vtkRenderer *renderer);

public:
    virtual void setMode(int state);
    virtual bool isVisible(void);
    virtual void setInteractor(void *interactor);
    virtual vtkRenderWindowInteractor *getInteractor(void);
    virtual int getState(void);
    virtual void setState(int state);
    virtual void setDisplay(bool display);
    virtual void setOpacity(double opacity);
    virtual void setColor(double red, double green, double blue);
    virtual void setShader(QString xmlfile);
    virtual void setShaderFromString(QString xmlfile);

public slots:
    virtual void onUpdateGeometry(void) {};


    virtual void onControlPointChanged();
    virtual void onSamplingChanged(void);

    virtual void onModeChanged(int state);

    virtual void hide(void);
    virtual void show(void);
    virtual void update(void);
    virtual void onRemoved(void);

protected:
     axlActorComposite(void);
    ~axlActorComposite(void);

protected:
   void NewObserver(void);
   void deleteObserver(void);

private:
    axlActorCompositePrivate *d;
};

axlAbstractActor *createAxlActorComposite(void);

#endif
