/* axlRendererDelegate.h ---
 *
 * Author: Meriadeg Perrinel
 * Copyright (C) 2008 - Meriadeg Perrinel, Inria.
 * Created: Tue Nov  9 16:58:59 2010 (+0100)
 * Version: $Id$
 * Last-Updated: Tue Nov  9 17:09:38 2010 (+0100)
 *           By: Meriadeg Perrinel
 *     Update #: 19
 */

/* Commentary:
 *
 */

/* Change log:
 *
 */

/*=========================================================================

  Program:   Visualization Toolkit
  Module:    axlRendererDelegate.h

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME axlRendererDelegate - Render the props of a axlRenderer
// .SECTION Description
// axlRendererDelegate is an abstract class with a pure virtual method Render.
// This method replaces the Render method of axlRenderer to allow custom
// rendering from an external project. A RendererDelegate is connected to
// a axlRenderer with method SetDelegate(). An external project just
// has to provide a concrete implementation of axlRendererDelegate.

// .SECTION See Also
// axlRenderer

#ifndef __axlRendererDelegate_h
#define __axlRendererDelegate_h

#include "vtkObject.h"

class axlRenderer;

class VTK_RENDERING_EXPORT axlRendererDelegate : public vtkObject
{
public:
  vtkTypeMacro(axlRendererDelegate,vtkObject);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Render the props of axlRenderer if Used is on.
  virtual void Render(axlRenderer *r)=0;

  // Description:
  // Tells if the delegate has to be used by the renderer or not.
  // Initial value is off.
  vtkSetMacro(Used, bool);
  vtkGetMacro(Used, bool);
  vtkBooleanMacro(Used, bool);

protected:
  axlRendererDelegate();
  virtual ~axlRendererDelegate();

  bool Used;

private:
  axlRendererDelegate(const axlRendererDelegate&);  // Not implemented.
  void operator=(const axlRendererDelegate&);  // Not implemented.
};

#endif
