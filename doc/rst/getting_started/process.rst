How to apply a process on a selected data
=========================================

*Meriadeg Perrinel*

This section describes how to apply a process on a data. In the following example, a BSpline surface is created and an offset surface is constructed. It assumes that the plugin ``BSplineTools`` is loaded.
 

1) Open Axl and create a BSpline Surface using the creator tool, provided e.g. by the plugin ``BSplineTools`` (See `how to create a data here <interactor.html>`_):

.. image:: ../img/content_tuto2-1.png 
   :align: center 

2) Offset process juste needs one BSpline surface as input, that's why you just have to go back to object inspector and select it. In fact, to use a process, you have to select every data you need as input of your process from the object inspector :

.. image:: ../img/content_tuto2-2.png 
   :align: center

3) Open the tool inspector and select Offset tag, then choose the process adapted for your input data (``goProcessOffsetSurfaceBSpline`` in our case). Then set the offset distance parameter and apply the process by clicking on the run button :

.. image:: ../img/content_tuto2-3.png 
   :align: center

Congratulation, the corresponding offset data has been created. You can now use it as a new data for what ever you want.

 

