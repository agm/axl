/* axlCore.i --- Core layer swig interface definition
*
* Author: Meriadeg Perrinel
* Copyright (C) 2013 - Meriadeg Perrinel, Inria.
* Created: Tue Jan  8 13:04:15 2009 (+0100)
* Version: $Id$
* Last-Updated: Jan  8 13:04:15 2013 (+0100)
*           By: Meriadeg Perrinel
*     Update #:
*/

/* Commentary:
*
*/

/* Change log:
*
*/

#ifndef AXL_I
#define AXL_I

%module axel
%include "dtkCore.i"

%{

#include <../app/axel/axlMainWindow.h>
#include <axlRpc/axlServer.h>

%}

// /////////////////////////////////////////////////////////////////
// Wrapper input
// /////////////////////////////////////////////////////////////////

%include "axlMainWindow.h"
//%include "axlMainWindowProxy.h"

// /////////////////////////////////////////////////////////////////
// Helper functions
// /////////////////////////////////////////////////////////////////

%{


int run(void)
{
int argc = 0;
char* argv[] = {"axel"};

    QApplication *app = new QApplication(argc, argv);
    app->setApplicationName("axel");
    app->setApplicationVersion("2.2.0");
    app->setOrganizationName("inria");
    app->setOrganizationDomain("fr");
    qDebug()<<QThread::currentThread()<<QCoreApplication::instance()->thread();

    dtkPluginManager::instance()->initialize();

    axlMainWindow* window = new axlMainWindow;
    window->show();
    window->raise();

    axlServer *server = new axlServer();
    server->setView(window->view());
    server->setInspector(window->inspector());

    int status = app->exec();

    delete server;
    delete window;


    return status;

}


%}

int run(void);


#endif
