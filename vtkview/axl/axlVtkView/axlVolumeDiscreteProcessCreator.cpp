/* axlVolumeDiscreteProcessCreator.cpp ---
 *
 * Author: Anais Ducoffe
 * Copyright (C) 2013 - Anais Ducoffe, Inria.
 */

/* Commentary:
 *
 */

/* Change log:
 *
 */

#include "axlVolumeDiscreteProcessCreator.h"
#include "axlVolumeDiscrete.h"
#include <dtkCoreSupport/dtkAbstractProcessFactory.h>

#include <axlCore/axlAbstractSurfaceImplicit.h>

#include <vtkImageData.h>
#include "axlVtkViewPlugin.h"

// ///////////////////////////////////////////////////////////////////
// axlVolumeDiscreteProcessCreatorPrivate
// ///////////////////////////////////////////////////////////////////

class axlVolumeDiscreteProcessCreatorPrivate
{
public:

    unsigned int x, y,z; // } grid dimension parameters
    axlAbstractSurfaceImplicit *input;
    axlVolumeDiscrete *output;

};
// ///////////////////////////////////////////////////////////////////
// axlVolumeDiscreteProcessCreator implementation
// ///////////////////////////////////////////////////////////////////

axlVolumeDiscreteProcessCreator::axlVolumeDiscreteProcessCreator(void) : axlAbstractCreatorProcess(), d(new axlVolumeDiscreteProcessCreatorPrivate)
{
    d->input = NULL;
    d->output = NULL;
}

axlVolumeDiscreteProcessCreator::~axlVolumeDiscreteProcessCreator(void)
{
    delete d;
    d= NULL;

}

//axlAbstractData *axlVolumeDiscreteProcessCreator::getInput(int channel) const{
//    return d->input;
//}



//not finished yet
void axlVolumeDiscreteProcessCreator::copyProcess(axlAbstractProcess *process){
    if(process->output()){
        d->output = dynamic_cast<axlVolumeDiscrete *>(process->output());
    }

}

bool axlVolumeDiscreteProcessCreator::hasParameters(void){
    return true;
}


////parse equation to calculate value at i,j,k cells.
//double axlVolumeDiscreteProcessCreator::equationToValue(int i, int j, int k){
//    return 0;
//}



dtkAbstractData *axlVolumeDiscreteProcessCreator::output(void){
    return d->output;
}



void axlVolumeDiscreteProcessCreator::setInput(dtkAbstractData *newData, int channel){
    if(dynamic_cast<axlAbstractSurfaceImplicit *>(newData)){
        d->input = dynamic_cast<axlAbstractSurfaceImplicit *>(newData);
    }
}


void axlVolumeDiscreteProcessCreator::setParameter(int value, int channel){
    if(channel == 0){
        d->x = value;
    }else if(channel == 1){
        d->y = value;
    }else if(channel == 2){
        d->z = value;
    }else{

    }

}

int axlVolumeDiscreteProcessCreator::update(void){

    if(!d->input)
        return 0;

    d->output = new axlVolumeDiscrete();
    d->output->setDimensions(d->x, d->y, d->z);

    vtkImageData *grid = static_cast<vtkImageData *>(d->output->data());

    int nx = d->output->xDimension();
    int ny = d->output->yDimension();
    int nz = d->output->zDimension();
    int value = 0;
    int indice = 0;
    double *coordinates = new double[3];


    for(int i = 0; i < nx ;i++){
        for(int j = 0; j < ny ;j++){
            for(int k = 0; k < nz ;k++){
                //get the point of the volume at indices i,j,k
                indice = k*nx*ny+j*nx+i;
                grid->GetPoint(indice, coordinates);
                //compute the value obtained with the implicit surface.
                value = d->input->eval(coordinates);
                //set value
                d->output->setValue(value, i, j,k);
            }
        }
    }

    return 1;

}

QString axlVolumeDiscreteProcessCreator::description(void) const{
    return "creates an axlVolume";
}


QString axlVolumeDiscreteProcessCreator::identifier(void) const{
    return "axlVolumeDiscreteProcessCreator";
}

bool axlVolumeDiscreteProcessCreator::registered(void){
    return axlVtkViewPlugin::processFactSingleton->registerProcessType("axlVolumeDiscreteProcessCreator", createaxlVolumeDiscreteProcessCreator,"axlAbstractCreator");
}

// /////////////////////////////////////////////////////////////////
// Type instanciation
// /////////////////////////////////////////////////////////////////

dtkAbstractProcess *createaxlVolumeDiscreteProcessCreator(void)
{
    return new axlVolumeDiscreteProcessCreator;
}
