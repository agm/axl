/* axlVolumeDiscreteEditor.cpp ---
 *
 * Author: Julien Wintz
 * Created: Thu Sep 26 14:09:18 2013 (+0200)
 * Version:
 * Last-Updated: Wed Oct  9 19:53:34 2013 (+0200)
 *           By: Julien Wintz
 *     Update #: 1681
 */

/* Change Log:
 *
 */

#include <axlGui/axlInspectorSeparator.h>
#include "axlVolumeDiscreteEditor.h"

#include "axlVtkView.h"
#include "axlVolumeDiscrete.h"
#include "axlActorVolumeDiscrete.h"

#include <vtkColorTransferFunction.h>
#include <vtkImageAccumulate.h>
#include <vtkImageData.h>
#include <vtkPiecewiseFunction.h>
#include <vtkVolume.h>
#include <vtkVolumeProperty.h>

#include <ctime>

// ///////////////////////////////////////////////////////////////////
// Definitions
// ///////////////////////////////////////////////////////////////////

#define axl_BIN_WIDTH 8

// ///////////////////////////////////////////////////////////////////
// axlVolumeDiscreteEditorMapper
// ///////////////////////////////////////////////////////////////////

class axlVolumeDiscreteEditorMapper
{
public:
    void setSceneRect(const QRectF& rect);
    void setHistgRect(const QRectF& rect);
    void setTableRect(const QRectF& rect);

public:
    QPointF mapFromSceneToHistg(const QPointF& point);
    QPointF mapFromSceneToTable(const QPointF& point);

public:
    QPointF mapFromHistgToScene(const QPointF& point);
    QPointF mapFromTableToScene(const QPointF& point);

public:
    QRectF scene_rect;
    QRectF histg_rect;
    QRectF table_rect;

public:
    bool use_log;
    bool use_scl;
};

// ///////////////////////////////////////////////////////////////////
// axlVolumeDiscreteEditorVertex
// ///////////////////////////////////////////////////////////////////

class axlVolumeDiscreteEditorVertex : public QGraphicsItem
{
public:
    axlVolumeDiscreteEditorVertex(QGraphicsItem *parent = 0);
    ~axlVolumeDiscreteEditorVertex(void);

public:
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0);

public:
    QRectF boundingRect(void) const;

protected:
    void mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event);
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event);

public:
    QColor foreground_color;
    QColor background_color;
};

// ///////////////////////////////////////////////////////////////////
// axlVolumeDiscreteEditorTable
// ///////////////////////////////////////////////////////////////////

class axlVolumeDiscreteEditorTable : public QGraphicsItem
{
public:
    axlVolumeDiscreteEditorTable(QGraphicsItem *parent = 0);
    ~axlVolumeDiscreteEditorTable(void);

public:
    void append(axlVolumeDiscreteEditorVertex *vertex);

public:
    void update(void);

public:
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0);

public:
    QRectF boundingRect(void) const;

public:
    QList<axlVolumeDiscreteEditorVertex *> vertices;

public:
    axlAbstractView *view;
    axlActorVolumeDiscrete *actor;
    axlVolumeDiscreteEditorMapper *mapper;

private:
    QPointF *points;
    int point_count;

private:
    QBrush brush;
};

// ///////////////////////////////////////////////////////////////////
// axlVolumeDiscreteEditorScene
// ///////////////////////////////////////////////////////////////////

class axlVolumeDiscreteEditorScene : public QGraphicsScene
{
public:
    axlVolumeDiscreteEditorScene(QObject *parent = 0);
    ~axlVolumeDiscreteEditorScene(void);

public:
    void setActor(axlActorVolumeDiscrete *actor);
    void setSceneRect(const QRectF& rect);
    void setUseLog(bool use);
    void setUseScl(bool use);

public:
    void drawBackground(QPainter *painter, const QRectF& rect);

public:
    void mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event);

public:
    axlVolumeDiscreteEditorTable *table;
    axlVolumeDiscreteEditorMapper *mapper;

public:
    int *bins;
    int bin_min, hst_min;
    int bin_max, hst_max;

private:
    bool use_log;
    bool use_scl;

private:
    qreal fct_scl;
};

// ///////////////////////////////////////////////////////////////////
// axlVolumeDiscreteEditorView
// ///////////////////////////////////////////////////////////////////

class axlVolumeDiscreteEditorView : public QGraphicsView
{
public:
    axlVolumeDiscreteEditorView(QWidget *parent = 0);
    ~axlVolumeDiscreteEditorView(void);

public:
    void setScene(axlVolumeDiscreteEditorScene *scene);

public:
    void resize(const QSize& size);
    void resizeEvent(QResizeEvent *event);

protected:
    void mouseMoveEvent(QMouseEvent *event);

public:
    axlVolumeDiscreteEditorScene *scene;
};

// ///////////////////////////////////////////////////////////////////
// axlVolumeDiscreteEditorPrivate
// ///////////////////////////////////////////////////////////////////

class axlVolumeDiscreteEditorPrivate
{
public:
    axlAbstractView *view;
    axlVolumeDiscrete *volume;
    axlVolumeDiscreteEditorView *volume_view;
    axlVolumeDiscreteEditorScene *volume_scene;

public:
    QLineEdit *dim_x;
    QLineEdit *dim_y;
    QLineEdit *dim_z;

public:
    QLineEdit *sca_min;
    QLineEdit *sca_max;

public:
    QRadioButton *log_radio;
    QRadioButton *scl_radio;
    QRadioButton *shd_radio;
};

// ///////////////////////////////////////////////////////////////////
// axlVolumeDiscreteEditorMapper
// ///////////////////////////////////////////////////////////////////

void axlVolumeDiscreteEditorMapper::setSceneRect(const QRectF& rect)
{
    this->scene_rect = rect;
}

void axlVolumeDiscreteEditorMapper::setHistgRect(const QRectF& rect)
{
    this->histg_rect = rect;
}

void axlVolumeDiscreteEditorMapper::setTableRect(const QRectF& rect)
{
    this->table_rect = rect;
}

QPointF axlVolumeDiscreteEditorMapper::mapFromSceneToHistg(const QPointF& point)
{
    qreal x = point.x();
    qreal y = point.y();

    qreal s_x = this->scene_rect.x();
    qreal s_y = this->scene_rect.y();
    qreal s_w = this->scene_rect.width();
    qreal s_h = this->scene_rect.height();

    qreal h_x = this->histg_rect.x();
    qreal h_y = this->histg_rect.y();
    qreal h_w = this->histg_rect.width();
    qreal h_h = this->histg_rect.height();

    if(this->use_log) {
        if (y > 0)
            y = log(y);
        h_y = log(h_y);
        h_h = log(h_h);
    }

    qreal a_x = +h_w/s_w;
    qreal a_y = -h_h/s_h;
    qreal b_x = h_x-(h_w/s_w)*s_x;
    qreal b_y = (s_h*(h_y+h_h)+h_h*s_y)/s_h;

    return QPointF(a_x*x+b_x, a_y*y+b_y);
}

QPointF axlVolumeDiscreteEditorMapper::mapFromSceneToTable(const QPointF& point)
{
    qreal x = point.x();
    qreal y = point.y();

    qreal s_x = this->scene_rect.x();
    qreal s_y = this->scene_rect.y();
    qreal s_w = this->scene_rect.width();
    qreal s_h = this->scene_rect.height();

    qreal h_x = this->table_rect.x();
    qreal h_y = this->table_rect.y();
    qreal h_w = this->table_rect.width();
    qreal h_h = this->table_rect.height();

    qreal a_x = +h_w/s_w;
    qreal a_y = -h_h/s_h;
    qreal b_x = h_x-(h_w/s_w)*s_x;
    qreal b_y = (s_h*(h_y+h_h)+h_h*s_y)/s_h;

    return QPointF(a_x*x+b_x, a_y*y+b_y);
}

QPointF axlVolumeDiscreteEditorMapper::mapFromHistgToScene(const QPointF& point)
{
    qreal x = point.x();
    qreal y = point.y();

    qreal s_x = this->scene_rect.x();
    qreal s_y = this->scene_rect.y();
    qreal s_w = this->scene_rect.width();
    qreal s_h = this->scene_rect.height();

    qreal h_x = this->histg_rect.x();
    qreal h_y = this->histg_rect.y();
    qreal h_w = this->histg_rect.width();
    qreal h_h = this->histg_rect.height();

    if(this->use_log) {
        if (y > 0)
            y = log(y);
        h_y = log(h_y);
        h_h = log(h_h);
    }

    qreal a_x = +s_w/h_w;
    qreal a_y = -s_h/h_h;
    qreal b_x = s_x-(s_w/h_w)*h_x;
    qreal b_y = (s_h*(h_y+h_h)+h_h*s_y)/h_h;

    return QPointF(a_x*x+b_x, a_y*y+b_y);
}

QPointF axlVolumeDiscreteEditorMapper::mapFromTableToScene(const QPointF& point)
{
    qreal x = point.x();
    qreal y = point.y();

    qreal s_x = this->scene_rect.x();
    qreal s_y = this->scene_rect.y();
    qreal s_w = this->scene_rect.width();
    qreal s_h = this->scene_rect.height();

    qreal h_x = this->table_rect.x();
    qreal h_y = this->table_rect.y();
    qreal h_w = this->table_rect.width();
    qreal h_h = this->table_rect.height();

    qreal a_x = +s_w/h_w;
    qreal a_y = -s_h/h_h;
    qreal b_x = s_x-(s_w/h_w)*h_x;
    qreal b_y = (s_h*(h_y+h_h)+h_h*s_y)/h_h;

    return QPointF(a_x*x+b_x, a_y*y+b_y);
}

// ///////////////////////////////////////////////////////////////////
// axlVolumeDiscreteEditorVertex
// ///////////////////////////////////////////////////////////////////

axlVolumeDiscreteEditorVertex::axlVolumeDiscreteEditorVertex(QGraphicsItem *parent) : QGraphicsItem(parent)
{
    this->foreground_color = QColor(0x00, 0x00, 0xff);
    this->background_color = QColor(0x33, 0x33, 0x33);

    this->setPos(0, 0);

    this->setFlag(QGraphicsItem::ItemIsMovable, true);
    this->setFlag(QGraphicsItem::ItemIsSelectable, true);

    this->setCursor(Qt::ArrowCursor);
}

axlVolumeDiscreteEditorVertex::~axlVolumeDiscreteEditorVertex(void)
{

}

void axlVolumeDiscreteEditorVertex::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);

    painter->setRenderHints(QPainter::Antialiasing, true);

    if(this->isSelected())
        painter->setPen(Qt::magenta);
    else
        painter->setPen(QColor(0x55, 0x55, 0x55));

    painter->setBrush(this->background_color); painter->drawEllipse(-10.0, -10.0, 20.0, 20.0);
    painter->setBrush(this->foreground_color); painter->drawEllipse(-05.0, -05.0, 10.0, 10.0);
}

QRectF axlVolumeDiscreteEditorVertex::boundingRect(void) const
{
    return QRectF(-10.0, -10.0, 20.0, 20.0);
}

void axlVolumeDiscreteEditorVertex::mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event)
{
    Q_UNUSED(event);

    this->foreground_color = QColorDialog::getColor(this->foreground_color, 0, "Choose vertex color");
    this->update();

    if(axlVolumeDiscreteEditorTable *table = dynamic_cast<axlVolumeDiscreteEditorTable *>(this->parentItem()))
        table->update();
}

void axlVolumeDiscreteEditorVertex::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    QGraphicsItem::mouseMoveEvent(event);

    axlVolumeDiscreteEditorTable *table = dynamic_cast<axlVolumeDiscreteEditorTable *>(this->parentItem());

    if (table)
        table->update();
}

// ///////////////////////////////////////////////////////////////////
// axlVolumeDiscreteEditorTable
// ///////////////////////////////////////////////////////////////////

axlVolumeDiscreteEditorTable::axlVolumeDiscreteEditorTable(QGraphicsItem *parent) : QGraphicsItem(parent)
{
    this->view = NULL;
    this->actor = NULL;
    this->brush = QColor::fromRgb(255, 0, 0, 128);

    this->points = NULL;
    this->point_count = 0;
}

axlVolumeDiscreteEditorTable::~axlVolumeDiscreteEditorTable(void)
{

}

static QColor axlVolumeDiscreteEditorTableColor(axlVolumeDiscreteEditorVertex *vertex)
{
    qreal h = vertex->scene()->sceneRect().height();
    qreal y = vertex->scenePos().y();
    qreal r = 1.0 - y/h;

    if (r > 1.0)
        r = 1.0;

    if (r < 0.0)
        r = 0.0;

    QColor c = vertex->foreground_color; c.setAlphaF(r);

    return c;
}

static bool axlVolumeDiscreteEditorTableSort(const axlVolumeDiscreteEditorVertex *v1, const axlVolumeDiscreteEditorVertex *v2) {
    return (v1->pos().x() < v2->pos().x());
}

void axlVolumeDiscreteEditorTable::append(axlVolumeDiscreteEditorVertex *vertex)
{
    this->vertices << vertex;

    vertex->setParentItem(this);

    this->update();
}

QPointF axlVolumeDiscreteEditorTableClamp(const QPointF& point, const QRectF& rect)
{
    QPointF value = point;

    if (value.y() < rect.y())
        value.setY(rect.y());

    if(value.y() > rect.height())
        value.setY(rect.height());

    return value;
}

void axlVolumeDiscreteEditorTable::update(void)
{
    // ///////////////////////////////////////////////////////////////////
    // vtk side
    // ///////////////////////////////////////////////////////////////////

    vtkColorTransferFunction *color = static_cast<vtkColorTransferFunction *>(this->actor->colorTransferFunction());
    color->RemoveAllPoints();

    vtkPiecewiseFunction *opacity = static_cast<vtkPiecewiseFunction *>(this->actor->opacityTransferFunction());
    opacity->RemoveAllPoints();

    // ///////////////////////////////////////////////////////////////////

    if(this->vertices.isEmpty())
        return;

    if(this->points)
        delete [] this->points;

    qSort(vertices.begin(), vertices.end(), axlVolumeDiscreteEditorTableSort);

    this->point_count = this->vertices.size()+4;
    this->points = new QPointF[this->point_count];

    points[0] = QPointF(0, this->scene()->sceneRect().height());
    points[1] = QPointF(0, this->vertices.first()->pos().y());

    QLinearGradient gradient(0, 0, this->scene()->sceneRect().width(), 0.0);
    gradient.setColorAt(0, axlVolumeDiscreteEditorTableColor(this->vertices.first()));

    for(int i = 0 ; i < this->vertices.count() ; i++) {

        points[i+2] = this->vertices.at(i)->pos();

        qreal x = this->vertices.at(i)->pos().x();
        qreal w = this->scene()->sceneRect().width();
        qreal r = x/w;

        if (r < 0.0)
            r = 0.0;

        if (r > 1.0)
            r = 1.0;

        gradient.setColorAt(r, axlVolumeDiscreteEditorTableColor(this->vertices.at(i)));

        // ///////////////////////////////////////////////////////////////////
        // vtk side
        // ///////////////////////////////////////////////////////////////////

        QPointF value = this->mapper->mapFromSceneToTable(axlVolumeDiscreteEditorTableClamp(this->vertices.at(i)->scenePos(), this->scene()->sceneRect()));

        color->AddRGBPoint(value.x(),
                           this->vertices.at(i)->foreground_color.redF(),
                           this->vertices.at(i)->foreground_color.greenF(),
                           this->vertices.at(i)->foreground_color.blueF());

        opacity->AddPoint(value.x(), value.y());

        // ///////////////////////////////////////////////////////////////////
    }

    points[this->vertices.count()+2] = QPointF(this->scene()->sceneRect().width(), this->vertices.last()->pos().y());
    points[this->vertices.count()+3] = QPointF(this->scene()->sceneRect().width(), this->scene()->sceneRect().height());

    gradient.setColorAt(1.0, axlVolumeDiscreteEditorTableColor(this->vertices.last()));

    this->brush = QBrush(gradient);

    QGraphicsItem::update();

    // ///////////////////////////////////////////////////////////////////
    // vtk side
    // ///////////////////////////////////////////////////////////////////

    static_cast<vtkVolumeProperty *>(this->actor->volumeProperty())->Modified();
    static_cast<vtkVolume         *>(this->actor->vol())->Update();

    // ///////////////////////////////////////////////////////////////////

    if (view)
        view->update();
}

void axlVolumeDiscreteEditorTable::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);

    if(this->vertices.isEmpty())
        return;

    painter->setPen(Qt::NoPen);
    painter->setBrush(this->brush);
    painter->setCompositionMode(QPainter::CompositionMode_SourceOver);
    painter->setRenderHints(QPainter::Antialiasing, true);
    painter->drawPolygon(this->points, this->point_count);
}

QRectF axlVolumeDiscreteEditorTable::boundingRect(void) const
{
    if(!this->point_count)
        return QRectF();

    return QRectF(this->points[0].x(), 0.0, this->points[this->point_count-1].x(), this->scene()->sceneRect().height());
}

// ///////////////////////////////////////////////////////////////////
// axlVolumeDiscreteEditorScene
// ///////////////////////////////////////////////////////////////////

axlVolumeDiscreteEditorScene::axlVolumeDiscreteEditorScene(QObject *parent) : QGraphicsScene(parent)
{
    this->bins = NULL;
    this->bin_min = 0;
    this->bin_max = 0;
    this->use_log = true;
    this->use_scl = false;
    this->mapper = new axlVolumeDiscreteEditorMapper;
    this->mapper->use_log = true;
    this->mapper->use_scl = false;
    this->table = new axlVolumeDiscreteEditorTable;
    this->table->mapper = this->mapper;

    this->addItem(this->table);
    this->setBackgroundBrush(QColor(0x00, 0x00, 0x00));
}

axlVolumeDiscreteEditorScene::~axlVolumeDiscreteEditorScene(void)
{
    delete this->mapper;
}

void axlVolumeDiscreteEditorScene::setActor(axlActorVolumeDiscrete *actor)
{
    vtkImageData *image = static_cast<vtkImageData *>(actor->image());

    //    this->bin_min = image->GetScalarRange()[0];
    //    this->bin_max = image->GetScalarRange()[1];
    this->bin_min = dynamic_cast<axlVolumeDiscrete *>(actor->data())->minValue();
    this->bin_max = dynamic_cast<axlVolumeDiscrete *>(actor->data())->maxValue();

    vtkImageAccumulate *histogram = vtkImageAccumulate::New();
    histogram->SetComponentExtent(this->bin_min, this->bin_max, 0, 0, 0, 0);
#if (VTK_MAJOR_VERSION <= 5)
    histogram->SetInput(image);
#else
    histogram->SetInputData(image);
#endif
    histogram->SetIgnoreZero(true);
    histogram->Update();

    this->bins = static_cast<int*>(histogram->GetOutput()->GetScalarPointer());

    this->hst_min = INT_MAX;
    this->hst_max = INT_MIN;

    for(int i = this->bin_min; i < this->bin_max; i++) {
        this->hst_min = qMin(this->hst_min, this->bins[i-bin_min]);
        this->hst_max = qMax(this->hst_max, this->bins[i-bin_min]);
    }


    this->mapper->setHistgRect(QRectF(this->bin_min, this->hst_min, this->bin_max-this->bin_min, this->hst_max-this->hst_min));
    this->mapper->setTableRect(QRectF(this->bin_min, 0, this->bin_max-this->bin_min, 1.0));

    this->table->actor = actor;
}

void axlVolumeDiscreteEditorScene::setSceneRect(const QRectF& rect)
{
    QRectF scene_rect = rect;

    QPointF blc = mapper->mapFromHistgToScene(QPointF(0, 0));
    QPointF brc = mapper->mapFromHistgToScene(QPointF(1, 0));

    if(this->use_scl)
        scene_rect.setWidth(rect.width() * this->fct_scl);

    this->mapper->use_log = this->use_log;
    this->mapper->use_scl = this->use_scl;
    this->mapper->setSceneRect(scene_rect);

    QGraphicsScene::setSceneRect(scene_rect);

    if(!this->table->vertices.empty())
        return;

    qreal min_x =  this->bin_min;
    qreal mid_x = (this->bin_max-this->bin_min)/2.0;
    qreal max_x =  this->bin_max-this->bin_min;

    axlVolumeDiscreteEditorVertex *min = new axlVolumeDiscreteEditorVertex;
    min->foreground_color = Qt::blue;
    min->setPos(mapper->mapFromHistgToScene(QPointF(min_x, 0.0)));
    min->setPos(min->scenePos().x(), rect.height()/2.0);

    axlVolumeDiscreteEditorVertex *mid = new axlVolumeDiscreteEditorVertex;
    mid->foreground_color = Qt::green;
    mid->setPos(mapper->mapFromHistgToScene(QPointF(mid_x, 0.0)));
    mid->setPos(mid->scenePos().x(), rect.height()/2.0);

    axlVolumeDiscreteEditorVertex *max = new axlVolumeDiscreteEditorVertex;
    max->foreground_color = Qt::red;
    max->setPos(mapper->mapFromHistgToScene(QPointF(max_x, 0.0)));
    max->setPos(max->scenePos().x(), rect.height()/2.0);

    this->table->append(min);
    this->table->append(mid);
    this->table->append(max);
}
void axlVolumeDiscreteEditorScene::setUseLog(bool use)
{
    this->use_log = use;
    this->mapper->use_log = use;
}

void axlVolumeDiscreteEditorScene::setUseScl(bool use)
{
    this->use_scl = use;
    this->mapper->use_scl = use;

    if (use) {
        QPointF blc = mapper->mapFromHistgToScene(QPointF(0, 0));
        QPointF brc = mapper->mapFromHistgToScene(QPointF(1, 0));
        this->fct_scl = axl_BIN_WIDTH / (brc.x() - blc.x());
    } else {
        this->fct_scl = 1 / this->fct_scl;
    }

    foreach(axlVolumeDiscreteEditorVertex *vertex, this->table->vertices)
        vertex->setPos(vertex->pos().x()*this->fct_scl, vertex->pos().y());
}

void axlVolumeDiscreteEditorScene::drawBackground(QPainter *painter, const QRectF& rect)
{
    QGraphicsScene::drawBackground(painter, rect);

    qreal h_x = this->mapper->histg_rect.x();
    // qreal h_y = this->mapper->histg_rect.y();
    qreal h_w = this->mapper->histg_rect.width();
    // qreal h_h = this->mapper->histg_rect.height();

    if (this->use_scl)
        painter->setPen(QColor(0x35, 0x35, 0x35));
    else
        painter->setPen(Qt::NoPen);

    painter->setBrush(QColor(0x77, 0x77, 0xf7));

    for(int i = h_x; i < h_x+h_w; i++) {
        QPointF b_l = mapper->mapFromHistgToScene(QPointF(i+0, 0));
        QPointF t_r = mapper->mapFromHistgToScene(QPointF(i+1, bins[i-int(h_x)]));
        painter->drawRect(QRectF(b_l, t_r));
    }
}

void axlVolumeDiscreteEditorScene::mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event)
{
    QGraphicsItem *item = this->items(event->scenePos()).first();

    if(item && dynamic_cast<axlVolumeDiscreteEditorVertex *>(item)) {
        QGraphicsScene::mouseDoubleClickEvent(event);
        return;
    }

    axlVolumeDiscreteEditorVertex *vertex = new axlVolumeDiscreteEditorVertex;
    vertex->setPos(event->scenePos());
    vertex->setSelected(true);

    this->table->append(vertex);
}

// ///////////////////////////////////////////////////////////////////
// axlVolumeDiscreteEditorView
// ///////////////////////////////////////////////////////////////////

axlVolumeDiscreteEditorView::axlVolumeDiscreteEditorView(QWidget *parent) : QGraphicsView(parent)
{
    this->scene = NULL;

    this->setAttribute(Qt::WA_MacShowFocusRect, false);
    this->setCursor(Qt::CrossCursor);
    this->setFrameShape(QFrame::NoFrame);
    this->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
    this->setMouseTracking(true);
    this->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    this->setViewportUpdateMode(QGraphicsView::FullViewportUpdate);

    this->setMinimumHeight(300);
}

axlVolumeDiscreteEditorView::~axlVolumeDiscreteEditorView(void)
{

}

void axlVolumeDiscreteEditorView::setScene(axlVolumeDiscreteEditorScene *scene)
{
    QGraphicsView::setScene(scene);

    this->scene = scene;
}

void axlVolumeDiscreteEditorView::mouseMoveEvent(QMouseEvent *event)
{
    QGraphicsView::mouseMoveEvent(event);

    QPointF point = scene->mapper->mapFromSceneToTable(this->mapToScene(event->pos()));

    QString x = QString::number(point.x(), 'f', 2);
    QString y = QString::number(point.y(), 'f', 2);

    this->setToolTip(QString("(%1, %2)").arg(x).arg(y));
}

void axlVolumeDiscreteEditorView::resize(const QSize& size)
{
    QGraphicsView::resize(size);

    this->scene->setSceneRect(QRectF(QPointF(0, 0), size));
    this->scene->update();
}

void axlVolumeDiscreteEditorView::resizeEvent(QResizeEvent *event)
{
    QGraphicsView::resizeEvent(event);

    this->scene->setSceneRect(QRectF(QPointF(0, 0), event->size()));
}

// ///////////////////////////////////////////////////////////////////
// axlVolumeDiscreteEditor
// ///////////////////////////////////////////////////////////////////

axlVolumeDiscreteEditor::axlVolumeDiscreteEditor(QWidget *parent) : axlAbstractVolumeDiscreteEditor(parent), d(new axlVolumeDiscreteEditorPrivate)
{
    d->volume = NULL;
    d->view = NULL;

    d->volume_scene = new axlVolumeDiscreteEditorScene(this);

    d->volume_view = new axlVolumeDiscreteEditorView(this);
    d->volume_view->setScene(d->volume_scene);
    d->volume_view->setAlignment(Qt::AlignLeft | Qt::AlignBottom);

    d->dim_x = new QLineEdit(this);
    d->dim_x->setReadOnly(true);

    d->dim_y = new QLineEdit(this);
    d->dim_y->setReadOnly(true);

    d->dim_z = new QLineEdit(this);
    d->dim_z->setReadOnly(true);

    d->sca_min = new QLineEdit(this);
    d->sca_min->setReadOnly(true);

    d->sca_max = new QLineEdit(this);
    d->sca_max->setReadOnly(true);

    d->log_radio = new QRadioButton(this);
    d->log_radio->setAutoExclusive(false);
    d->log_radio->setChecked(true);

    d->scl_radio = new QRadioButton(this);
    d->scl_radio->setAutoExclusive(false);
    d->scl_radio->setChecked(false);

    d->shd_radio = new QRadioButton(this);
    d->shd_radio->setAutoExclusive(false);
    d->shd_radio->setChecked(false);

    QPushButton *outline_none = new QPushButton("None", this);
    outline_none->setCheckable(true);
    outline_none->setChecked(true);
    outline_none->setObjectName("left");

    QPushButton *outline_corners = new QPushButton("Corners", this);
    outline_corners->setCheckable(true);
    outline_corners->setChecked(false);
    outline_corners->setObjectName("middle");

    QPushButton *outline_box = new QPushButton("Box", this);
    outline_box->setCheckable(true);
    outline_box->setChecked(false);
    outline_box->setObjectName("middle");

    QPushButton *outline_contour = new QPushButton("Contour", this);
    outline_contour->setCheckable(true);
    outline_contour->setChecked(false);
    outline_contour->setObjectName("right");

    QButtonGroup *outline_group = new QButtonGroup(this);
    outline_group->addButton(outline_none);
    outline_group->addButton(outline_corners);
    outline_group->addButton(outline_box);
    outline_group->addButton(outline_contour);

    QHBoxLayout *outline_layout = new QHBoxLayout;
    outline_layout->setSpacing(0);
    outline_layout->addWidget(outline_none);
    outline_layout->addWidget(outline_corners);
    outline_layout->addWidget(outline_box);
    outline_layout->addWidget(outline_contour);

    QHBoxLayout *dim_l = new QHBoxLayout;
    dim_l->addWidget(d->dim_x);
    dim_l->addWidget(d->dim_y);
    dim_l->addWidget(d->dim_z);

    QHBoxLayout *sca_l = new QHBoxLayout;
    sca_l->addWidget(d->sca_min);
    sca_l->addWidget(d->sca_max);

    QFormLayout *b_layout = new QFormLayout;
    b_layout->setContentsMargins(10, 10, 10, 10);
    b_layout->setSpacing(10);
    b_layout->addRow("Dimensions", dim_l);
    b_layout->addRow("Scalar range", sca_l);
    b_layout->addRow("Logarithmic count", d->log_radio);
    b_layout->addRow("Scaled values", d->scl_radio);
    b_layout->addRow("Shaded rendering", d->shd_radio);

    QVBoxLayout *layout = new QVBoxLayout(this);
    layout->setContentsMargins(0, 0, 0, 0);
    layout->setSpacing(0);
    layout->addWidget(d->volume_view);
    layout->addWidget(new axlInspectorSeparator(this));
    layout->addLayout(b_layout);
    layout->addLayout(outline_layout);

    connect(d->log_radio, SIGNAL(toggled(bool)), this, SLOT(onLogChecked(bool)));
    connect(d->scl_radio, SIGNAL(toggled(bool)), this, SLOT(onSclChecked(bool)));
    connect(d->shd_radio, SIGNAL(toggled(bool)), this, SLOT(onShdChecked(bool)));

    connect(outline_none, SIGNAL(clicked()), this, SLOT(outlineNone()));
    connect(outline_corners, SIGNAL(clicked()), this, SLOT(outlineCorners()));
    connect(outline_box, SIGNAL(clicked()), this, SLOT(outlineBox()));
    connect(outline_contour, SIGNAL(clicked()), this, SLOT(outlineContour()));
}

axlVolumeDiscreteEditor::~axlVolumeDiscreteEditor(void)
{
    delete d;
}


bool axlVolumeDiscreteEditor::registered(void)
{
    return axlInspectorObjectFactory::instance()->registerInspectorObject("axlVolumeDiscreteEditor", createaxlVolumeDiscreteEditor);
}

axlInspectorObjectInterface *createaxlVolumeDiscreteEditor(void)
{
    return new axlVolumeDiscreteEditor;
}



void axlVolumeDiscreteEditor::setVolume(axlAbstractVolumeDiscrete *volume)
{
    if(!(d->volume = dynamic_cast<axlVolumeDiscrete *>(volume)))
        return;

    d->dim_x->setText(QString::number(d->volume->xDimension()));
    d->dim_y->setText(QString::number(d->volume->yDimension()));
    d->dim_z->setText(QString::number(d->volume->zDimension()));

    if(!d->view){
        qDebug() << Q_FUNC_INFO << "no view selected";
    }
    if(!d->view->actor(d->volume)){
        qDebug() << Q_FUNC_INFO << "no actor found";
    }
    axlActorVolumeDiscrete *actor = dynamic_cast<axlActorVolumeDiscrete *>(d->view->actor(d->volume));

    if (actor)
        d->volume_scene->setActor(actor);

    d->sca_min->setText(QString::number(d->volume_scene->bin_min));
    d->sca_max->setText(QString::number(d->volume_scene->bin_max));
}

void axlVolumeDiscreteEditor::setView(axlAbstractView *view)
{
    d->view = view;
    d->volume_scene->table->view = view;
}

void axlVolumeDiscreteEditor::onLogChecked(bool checked)
{
    d->volume_scene->setUseLog(checked);
    d->volume_scene->mapper->use_log = checked;
    d->volume_scene->update();
}

void axlVolumeDiscreteEditor::onSclChecked(bool checked)
{
    d->volume_scene->setUseScl(checked);
    d->volume_view->resize(d->volume_view->size());
    d->volume_scene->table->update();
    d->volume_scene->update();
}

void axlVolumeDiscreteEditor::onShdChecked(bool checked)
{
    if(!d->view)
        return;

    axlActorVolumeDiscrete *actor = dynamic_cast<axlActorVolumeDiscrete *>(d->view->actor(d->volume));

    vtkVolumeProperty *property = static_cast<vtkVolumeProperty *>(actor->volumeProperty());
    property->SetShade(checked);
    property->Modified();

    vtkVolume *volume = static_cast<vtkVolume *>(actor->vol());
    volume->Update();

    d->view->update();
}

void axlVolumeDiscreteEditor::outlineNone(void)
{
    if(!d->view)
        return;

    axlActorVolumeDiscrete *actor = dynamic_cast<axlActorVolumeDiscrete *>(d->view->actor(d->volume));

    actor->outlineNone();

    d->view->update();
}

void axlVolumeDiscreteEditor::outlineCorners(void)
{
    if(!d->view)
        return;

    axlActorVolumeDiscrete *actor = dynamic_cast<axlActorVolumeDiscrete *>(d->view->actor(d->volume));

    actor->outlineCorners();

    d->view->update();
}

void axlVolumeDiscreteEditor::outlineBox(void)
{
    if(!d->view)
        return;

    axlActorVolumeDiscrete *actor = dynamic_cast<axlActorVolumeDiscrete *>(d->view->actor(d->volume));

    actor->outlineBox();

    d->view->update();
}

void axlVolumeDiscreteEditor::outlineContour(void)
{
    if(!d->view)
        return;

    axlActorVolumeDiscrete *actor = dynamic_cast<axlActorVolumeDiscrete *>(d->view->actor(d->volume));

    actor->outlineContour();

    d->view->update();
}
