/* axlRendererFactory.cpp ---
 *
 * Author: Meriadeg Perrinel
 * Copyright (C) 2008 - Meriadeg Perrinel, Inria.
 * Created: Tue Nov  9 16:58:59 2010 (+0100)
 * Version: $Id$
 * Last-Updated: Tue Nov  9 17:09:38 2010 (+0100)
 *           By: Meriadeg Perrinel
 *     Update #: 19
 */

/* Commentary:
 *
 */

/* Change log:
 *
 */


#include "axlRendererFactory.h"
#include "axlRenderer.h"
#include <vtkVersion.h>

VTK_CREATE_CREATE_FUNCTION(axlRenderer);

VTK_FACTORY_INTERFACE_IMPLEMENT(axlRendererFactory);

vtkStandardNewMacro(axlRendererFactory);


axlRendererFactory::axlRendererFactory()
{
    std::cout<<"axlRendererFactory"<<std::endl;
    this->RegisterOverride("vtkOpenGLRenderer", "axlRenderer", "", 1, vtkObjectFactoryCreateaxlRenderer);
}

const char* axlRendererFactory::GetVTKSourceVersion(void)
{
    return VTK_SOURCE_VERSION;
}

const char* axlRendererFactory::GetDescription(void)
{
    return "";
}
