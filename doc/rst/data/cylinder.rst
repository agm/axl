
Cylinder
========

A cylinder is defined by a starting point (axlPoint) and an ending point (axlPoint) and a radius (double).

Type: ``axlCylinder``

Format:

.. literalinclude:: ../../../data/cylinder.axl 
   :lines: 2-6
   :language: xml
	      
.. image:: ../img/cylinder.png
   :align: center
