/* axlVolumeDiscreteConverter.h ---
 *
 * Author: Anais Ducoffe
 * Copyright (C) 2013 - Anais Ducoffe, Inria.
 */

/* Commentary:
 *
 */

/* Change log:
 *
 */

#ifndef AXLVOLUMEDISCRETECONVERTER_H
#define AXLVOLUMEDISCRETECONVERTER_H



#include "axlVtkViewPluginExport.h"
#include <axlCore/axlAbstractDataConverter.h>

class axlVolumeDiscreteConverterPrivate;

// ///////////////////////////////////////////////////////////////////
// axlVolume declaration
// ///////////////////////////////////////////////////////////////////

class AXLVTKVIEWPLUGIN_EXPORT axlVolumeDiscreteConverter : public axlAbstractDataConverter
{
    Q_OBJECT

public:
    axlVolumeDiscreteConverter(void);
    ~axlVolumeDiscreteConverter(void);


public :
    QString identifier (void) const;
    QString  description (void) const;
    QStringList fromTypes(void) const;
    QString       toType (void) const;

public:
    static bool registered(void);

public slots:
    axlMesh *toMesh(void);

public:
    void setData(dtkAbstractData *data);

private:
    axlVolumeDiscreteConverterPrivate *d;
};

// /////////////////////////////////////////////////////////////////
// Instanciation function
// /////////////////////////////////////////////////////////////////

AXLVTKVIEWPLUGIN_EXPORT dtkAbstractDataConverter *createaxlVolumeDiscreteConverter(void);

#endif // AXLVOLUMEDISCRETECONVERTER_H
