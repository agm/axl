/* axlActorPointSet.cpp ---
 *
 * Author: Meriadeg Perrinel
 * Copyright (C) 2008 - Meriadeg Perrinel, Inria.
 * Created: Fri Dec 17 10:59:21 2010 (+0100)
 * Version: $Id$
 * Last-Updated: Wed Mar 16 21:51:16 2011 (+0100)
 *           By: Meriadeg Perrinel
 *     Update #: 23
 */

/* Commentary:
 *
 */

/* Change log:
 *
 */
#include "axlActorPointSet.h"

#include <axlCore/axlPoint.h>
#include <axlCore/axlPointSet.h>

#include <vtkActor.h>
#include <vtkCommand.h>
#include <vtkCellArray.h>
#include <vtkGlyph3D.h>

#include <vtkIntArray.h>
#include <vtkLookupTable.h>
#include <vtkObjectFactory.h>
#include <vtkPoints.h>
#include <vtkPointData.h>
#include <vtkPolyData.h>
#include <vtkPolyDataMapper.h>
#include <vtkProperty.h>
#include <vtkSmartPointer.h>
#include <vtkSphereSource.h>
#include <vtkVertex.h>


// /////////////////////////////////////////////////////////////////
// axlActorPointSetPrivate
// /////////////////////////////////////////////////////////////////

class axlActorPointSetPrivate
{
public:
    axlPointSet *pointSet;

    vtkSmartPointer<vtkSphereSource> sphere;
    vtkSmartPointer<vtkGlyph3D> glyph;

};

vtkCxxRevisionMacro(axlActorPointSet, "$Revision: 0.0.1 $");
vtkStandardNewMacro(axlActorPointSet);

dtkAbstractData *axlActorPointSet::data(void)
{
    return d->pointSet;
}


void axlActorPointSet::setPointSet(axlPointSet *axlPointSet)
{
    if(axlPointSet)
    {
        d->pointSet = axlPointSet;

        this->setPoints(vtkSmartPointer<vtkPoints>::New());
        this->setMapper(vtkSmartPointer<vtkPolyDataMapper>::New());
        this->setActor(vtkSmartPointer<vtkActor>::New());
        this->setPolyData(vtkSmartPointer<vtkPolyData>::New());
        this->setCellArray(vtkSmartPointer<vtkCellArray>::New());


        vtkSmartPointer<vtkIntArray> scalarArray = vtkSmartPointer<vtkIntArray>::New();
        scalarArray->SetName("mapperCollorArrayDefaultField");
        scalarArray->SetNumberOfComponents(1);
        scalarArray->SetNumberOfTuples(d->pointSet->numberOfPoints());

        vtkSmartPointer<vtkLookupTable> lookupTable = vtkSmartPointer<vtkLookupTable>::New();
        lookupTable->SetRange(0.0, d->pointSet->numberOfPoints());
        lookupTable->SetNumberOfTableValues(d->pointSet->numberOfPoints());

        for(int i = 0; i < d->pointSet->numberOfPoints(); i++)
        {
            this->getPoints()->InsertNextPoint(d->pointSet->value(i)->x(), d->pointSet->value(i)->y(), d->pointSet->value(i)->z());
            scalarArray->SetTuple1(i, i);
            //if(d->pointSet->value(i)->hasProperty("color"))

        }// all points are stocked into the vtkPoints
        lookupTable->Build();


        vtkSmartPointer<vtkVertex> currentVertex = vtkSmartPointer<vtkVertex>::New();

        for(int i = 0; i < d->pointSet->numberOfPoints(); i++)
        {
            currentVertex->GetPointIds()->SetId(0, i);
            this->getCellArray()->InsertNextCell(currentVertex);
        }

        if(!d->pointSet->isUniqueColor())
        {
            for(int i = 0; i < d->pointSet->numberOfPoints(); i++)
            {
                lookupTable->SetTableValue(i , d->pointSet->value(i)->color().redF(),d->pointSet->value(i)->color().greenF(), d->pointSet->value(i)->color().blueF(), 1.0);

            }

        }


        this->getPolyData()->SetPoints(this->getPoints());
        this->getPolyData()->SetVerts(this->getCellArray());



        if(!d->pointSet->isUniqueColor())
        {
            this->getPolyData()->GetPointData()->AddArray(scalarArray);
            this->getPolyData()->GetPointData()->SetActiveScalars("mapperCollorArrayDefaultField");

            this->getMapper()->SetLookupTable(lookupTable);
            this->getMapper()->SetInterpolateScalarsBeforeMapping(true);
            this->getMapper()->UseLookupTableScalarRangeOn();
            this->getMapper()->SetScalarModeToUsePointData();
        }


        // connection of data to actor
        this->getMapper()->SetInput( this->getPolyData() );

        this->getActor()->SetMapper(this->getMapper());

        // glyph iniatialize
//        if (d->pointSet->numberOfPoints() < 100)
//        {
//            d->sphere = vtkSmartPointer<vtkSphereSource>::New();
//            d->sphere->SetPhiResolution(40);
//            d->sphere->SetThetaResolution(40);
//            d->sphere->SetRadius(0.1);
//            d->glyph = vtkSmartPointer<vtkGlyph3D>::New();
//            d->glyph->SetInput(this->getPolyData());
//            d->glyph->SetSource(d->sphere->GetOutput());
//            d->glyph->SetScaleModeToDataScalingOff();

//            this->getMapper()->SetInput(  d->glyph->GetOutput());
//        }
        //d->glyph->SetScaleFactor(0.1);
        if(d->pointSet->isUniqueColor())
        {
            QColor color = d->pointSet->color();
            this->getActor()->GetProperty()->SetColor(color.redF(), color.greenF(), color.blueF());

            QString shader = d->pointSet->shader();
            if(!shader.isEmpty())
                this->setShader(shader);

            this->setOpacity(d->pointSet->opacity());
        }

        this->setSize(d->pointSet->size());

        this->AddPart(this->getActor());

    }
    else
        qDebug()<< "no axlPointSet available";


}


void axlActorPointSet::setMode(int state)
{
    this->onModeChanged(state);

    emit stateChanged(this->data(), state);
}

void axlActorPointSet::onModeChanged(int state)
{

    if(state == 0)
    {
        this->setState(axlActor::passive);
        this->getActor()->SetPickable(1);

        if(axlAbstractData *data = dynamic_cast<axlAbstractData *>(this->data()))
        {
            QColor color = data->color();
            this->getActor()->GetProperty()->SetColor(color.redF(), color.greenF(), color.blueF());
        }

    }
    else if(state == 1)
    {

        this->setState(axlActor::selection);
        this->getActor()->SetPickable(1);
        vtkProperty *prop = this->getActor()->GetProperty();

        if(axlAbstractData *data = dynamic_cast<axlAbstractData *>(this->data()))
        {
            QColor color = data->color();
            qreal *h = new qreal(0.0);
            qreal *s = new qreal(0.0);
            qreal *l = new qreal(0.0);
            color.getHslF(h, s, l);
            color.setHslF(*h, *s, *l + (1.0 - *l) / 2.0);
            prop->SetColor(color.redF(), color.greenF(), color.blueF());
        }

    }
    else if(state == 2)
    {
        this->setState(axlActor::edition);
        this->getActor()->SetPickable(0);
        vtkProperty *prop = this->getActor()->GetProperty();
        if(axlAbstractData *data = dynamic_cast<axlAbstractData *>(this->data()))
        {
            QColor color = data->color();
            qreal *h = new qreal(0.0);
            qreal *s = new qreal(0.0);
            qreal *l = new qreal(0.0);
            color.getHslF(h, s, l);
            color.setHslF(*h, *s, *l + (1.0 - *l)/2.0);
            prop->SetColor(color.redF(), color.greenF(), color.blueF());
        }
    }

    this->Modified();
}

void axlActorPointSet::setSize(double size)
{
    this->getActor()->GetProperty()->SetPointSize(size);
}

axlActorPointSet::axlActorPointSet(void) : axlActor(), d(new axlActorPointSetPrivate)
{

}

axlActorPointSet::~axlActorPointSet(void)
{
    delete d;

    d = NULL;
}
