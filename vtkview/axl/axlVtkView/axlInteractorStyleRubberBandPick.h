/* axlInteractorStyleRubberBandPick.h ---
 *
 * Author: Meriadeg Perrinel
 * Copyright (C) 2008 - Meriadeg Perrinel, Inria.
 * Created: Fri Feb 18 17:25:39 2011 (+0100)
 * Version: $Id$
 * Last-Updated: Fri Feb 18 17:28:34 2011 (+0100)
 *           By: Meriadeg Perrinel
 *     Update #: 18
 */

/* Commentary:
 *
 */

/* Change log:
 *
 */

#ifndef AXLINTERACTORSTYLERUBBERBANDPICK_H
#define AXLINTERACTORSTYLERUBBERBANDPICK_H

#include <vtkInteractorStyleRubberBandPick.h>
#include <vtkSmartPointer.h>
#include <QObject>
#include <QVector>

#include "axlVtkViewPluginExport.h"

class vtkPolyData;
class axlInteractorStyleRubberBandPickPrivate;


class AXLVTKVIEWPLUGIN_EXPORT axlInteractorStyleRubberBandPick : public QObject,public vtkInteractorStyleRubberBandPick
{
    Q_OBJECT

public:
    static axlInteractorStyleRubberBandPick* New();
    vtkTypeMacro(axlInteractorStyleRubberBandPick,vtkInteractorStyleRubberBandPick);

    axlInteractorStyleRubberBandPick(QObject *parent= 0);
    ~axlInteractorStyleRubberBandPick(void);

    virtual void OnLeftButtonUp();

    void SetPolyData(vtkSmartPointer<vtkPolyData> points);
    QVector<int> ids(void);

signals:
    void IdsSelectedChanged(void);

private:
    axlInteractorStyleRubberBandPickPrivate *d;

};
//vtkStandardNewMacro(axlInteractorStyleRubberBandPick);

#endif // AXLINTERACTORSTYLERUBBERBANDPICK_H
