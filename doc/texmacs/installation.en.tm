<TeXmacs|1.0.7.3>                                              

<style|<tuple|mmxdoc|mathemagix>>

<\body><tmdoc-title|How to install and use <name|axel_modeler>>

  <subsection|Dependencies>


  Here is the list of packages to be installed before configuring <name|axel_modeler>:

  <\with|par-mode|center>
    <block*|<tformat|<table|<row|<cell|<verbatim|src>>|<cell|<verbatim|rpm>>|<cell|<verbatim|deb>>|<cell|<verbatim|port (Mac)>>>|<row|<cell|<hlink|<verbatim|sisl>|../../../sisl/doc/texmacs/index.en.tm>>|<cell|<verbatim|sisl>>|<cell|<verbatim|-->>|<cell|<verbatim|-->>>|<row|<cell|<hlink|<verbatim|gotools>|../../../gotools/doc/texmacs/index.en.tm>>|<cell|<verbatim|gotools>>|<cell|<verbatim|-->>|<cell|<verbatim|-->>>|<row|<cell|<hlink|<verbatim|dtk>|../../../dtk/doc/texmacs/index.en.tm>>|<cell|<verbatim|dtk>>|<cell|<verbatim|-->>|<cell|<verbatim|-->>>>>>
  </with>
  
  To construct the plugins for axel (<verbatim|AXL=ON>), the following packages should be installed:

  <\with|par-mode|center>
    <block*|<tformat|<table|<row|<cell|<verbatim|src>>|<cell|<verbatim|rpm>>|<cell|<verbatim|deb>>|<cell|<verbatim|port (Mac)>>>|<row|<cell|<hlink|<verbatim|VTK>|../../../VTK/doc/texmacs/index.en.tm>>|<cell|<verbatim|VTK>>|<cell|<verbatim|-->>|<cell|<verbatim|-->>>>>>
  </with>

  <subsection|Download>
  
     You can either

  <\itemize-dot>
    <item>get the version under development with:

    <\code>
     <\shell-fragment>
      svn checkout svn://scm.gforge.inria.fr/svn/axel/axel_modeler

cd axel_modeler; ./init_packages; cd ..

     </shell-fragment>                                                 
    </code>   
  
    <item>or download a source distribution if it is available at

   <\with|par-mode|center>   <verbatim|<hlink|http://gforge.inria.fr/project/axel|http://gforge.inria.fr/frs/?group_id=360>>
  </with>
 

   and uncompress it with:

  <\shell-fragment>
    tar zxvf axel_modeler-0.1-Source.tar.gz
  </shell-fragment>

  </itemize-dot>

  <subsection|Configuration>

  To run the configuration, <hlink|<verbatim|<with|color|red|cmake>>|http://www.cmake.org/>
  (version <math|\<geqslant\> \ 2.6>) should first be installed.

  <strong|Local installation.> The package can be configured
  out-of-source (eg. in a folder <verbatim|build>) as follows:

  <\code>
    <\shell-fragment>
      mkdir build;

      cd build;

      cmake <with|color|brown|\<less\>axel_modeler_source_dir\<gtr\>>

      make
    </shell-fragment>                                                    
  </code>                                                                

  where 

  where <verbatim|<with|color|brown|\<less\>realroot_source_dir\<gtr\>>> is
  the name of the folder of the package source 
(eg. <verbatim|../axel_modeler-0.1-Source> or <verbatim|../axel_modeler>),

   <strong|Global installation.> If you want to install it globally in your
  environment after it is compiled, you can run the following instructions
  (possibly with the <em|superuser> rights):

  <\code>
    <\shell-fragment>
      mkdir build;

      cd build;

      cmake <with|color|brown|\<less\>realroot_source_dir\<gtr\>>
      -DCMAKE_INSTALL_PREFIX=<with|color|brown|\<less\>install_dir\<gtr\>>

      make && make install
    </shell-fragment>
  </code>

  If not specified,\ 

  <\code>
    <\shell-fragment>
      cmake <with|color|brown|\<less\>axel_modeler_source_dir\<gtr\>>>
    </shell-fragment>
  </code>

  where <verbatim|<with|color|brown|\<less\>install_dir\<gtr\>>> is the
  folder where to install the package so that the libraries go in
  <verbatim|<with|color|brown|\<less\>install_dir\<gtr\>>/lib>; and the
  headers in <verbatim|<with|color|brown|\<less\>install_dir\<gtr\>>/include>.
  If not specified, the package is installed by default in
  <verbatim|/usr/local>. For more details, on the different options see
  <hlink|here|../../mmxtools/doc/texmacs/configure_mmx_pkg.en.tm>.

  To see the operations performed during the <verbatim|make> command, you can use:\ 

  <\code>
    <\shell-fragment>
      make VERBOSE=1
    </shell-fragment>
  </code>

  To see the other available targets, you can use:\ 

  <\code>
    <\shell-fragment>
      make help
    </shell-fragment>
  </code>

  <subsection|How to set locally your environment variables>

  In case you have not run <verbatim|make install>, you can set up your environment locally in the folder <verbatim|build> with:

  <\code>
   <\shell-fragment>
     source local_env
   </shell-fragment>
  </code>

  <subsection|Using axel_modeler>

  To use the package <name|axel_modeler> in a <verbatim|cmake> project, you can
  simply add the following lines in your file <verbatim|CMakeLists.txt>:

  <\code>
    <\cpp-fragment>
      find_package(Axl_modeler)

      include_directories (${AXL_MODELER_INCLUDE_DIR})

      link_libraries (${AXL_MODELER_LIBRARIES})

      link_library_dir (${AXL_MODELER_LIBRARY_DIR})
    </cpp-fragment>
  </code>
</body>
