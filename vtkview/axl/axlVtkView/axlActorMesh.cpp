/* axlActorMesh.cpp ---
 *
 * Author: Meriadeg Perrinel
 * Copyright (C) 2008 - Meriadeg Perrinel, Inria.
 * Created: Fri Dec 17 10:59:21 2010 (+0100)
 * Version: $Id$
 * Last-Updated: Mon Dec 17 15:01:45 2012 (+0100)
 *           By: Julien Wintz
 *     Update #: 130
 */

/* Commentary:
 *
 */

/* Change log:
 *
 */
#include "axlActorMesh.h"

#include "axlVtkViewPlugin.h"

#include "axlControlPointsWidget.h"

#include <axlCore/axlAbstractData.h>
#include <axlCore/axlAbstractDataConverter.h>
#include <dtkCoreSupport/dtkAbstractDataConverter.h>
#include <axlCore/axlPoint.h>
#include <axlCore/axlMesh.h>

#include "axlInteractorStyleRubberBandPick.h"
#include "axlInteractorCellPicking.h"

#include <dtkCoreSupport/dtkAbstractDataFactory.h>

#include <vtkActor.h>
#include <vtkCommand.h>
#include <vtkCellArray.h>
#include <vtkCleanPolyData.h>
#include <vtkDoubleArray.h>
#include <vtkLine.h>
#include <vtkObjectFactory.h>
#include <vtkDataSetMapper.h>
#include <vtkPointData.h>
#include <vtkPolyDataMapper.h>
#include <vtkPolyDataNormals.h>
#include <vtkPolygon.h>
#include <vtkProperty.h>
#include <vtkSmartPointer.h>
#include <vtkSphereSource.h>
#include <vtkTubeFilter.h>
#include <vtkUnstructuredGrid.h>
#include <vtkVertex.h>

#include <vtkIntArray.h>
#include <vtkLookupTable.h>
#include <vtkAreaPicker.h>
#include <vtkCellPicker.h>

#include <vtkRenderWindow.h>
#include <vtkRenderer.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkIdFilter.h>
#include <vtkDataSetSurfaceFilter.h>
#include <vtkTriangleFilter.h>

#include <vtkFeatureEdges.h>
#include <vtkExtractEdges.h>

#include <vtkRendererCollection.h>
#include <vtkPoints.h>

#include <QVector>

// /////////////////////////////////////////////////////////////////
// axlActorMeshPrivate
// /////////////////////////////////////////////////////////////////

class axlActorMeshPrivate
{
public:
    vtkSmartPointer<vtkDataSetMapper> dataSetMapper;
    vtkSmartPointer<vtkActor> actorSphereParam;
    //vtkSmartPointer<vtkTubeFilter> meshLineTubeFilter;
    vtkSmartPointer<vtkPolyDataNormals> polyDataNormals;

    axlAbstractData *data;
    axlMesh *mesh;

    axlInteractorStyleRubberBandPick *style;
    axlInteractorCellPicking *picker;

};

#if (VTK_MAJOR_VERSION <= 5)
vtkCxxRevisionMacro(axlActorMesh, "$Revision: 0.0.1 $");
#endif

vtkStandardNewMacro(axlActorMesh);


QString axlActorMesh::identifier(void){
    return "axlActorMesh";
}


dtkAbstractData *axlActorMesh::data(void)
{
    return d->data;
}

void axlActorMesh::setData(dtkAbstractData *data)
{
    d->data = dynamic_cast<axlAbstractData *>(data);

    this->setActor(vtkSmartPointer<vtkActor>::New());
    this->setCellArray(vtkSmartPointer<vtkCellArray>::New());
    this->setPolyData(vtkSmartPointer<vtkPolyData>::New());
    this->setMapper(vtkSmartPointer<vtkPolyDataMapper>::New());
    this->setPoints(vtkSmartPointer<vtkPoints>::New());
    this->getMapper()->ScalarVisibilityOff();

    this->onUpdateGeometry();

    //normals computation
    if (!d->mesh->normal_count()){
        computeNormals();
        this->getActor()->GetProperty()->SetInterpolationToFlat();
    }else{
        d->mesh->setInterpolation(2);
        this->getActor()->GetProperty()->SetInterpolationToPhong();
    }

    this->getActor()->SetMapper(this->getMapper());

    this->AddPart(this->getActor());

    QColor color = d->data->color();
    this->getActor()->GetProperty()->SetColor(color.redF(), color.greenF(), color.blueF());

    this->setSize(d->data->size());
    QString shader = d->data->shader();
    if(!shader.isEmpty())
        this->setShader(shader);

    // signals connecting
    connect(d->data, SIGNAL(modifiedGeometry()), this, SLOT(onUpdateGeometry()));
    connect(d->data, SIGNAL(modifiedProperty()), this, SLOT(onUpdateProperty()));
}

void axlActorMesh::computeNormals(void){
        // Generate normals
#if VTK_MAJOR_VERSION <= 5
        d->polyDataNormals->SetInput(polydata);
#else
        d->polyDataNormals->SetInputData(this->getPolyData());
#endif
        d->polyDataNormals->ComputePointNormalsOn();
        d->polyDataNormals->ComputeCellNormalsOff();
        d->polyDataNormals->SetSplitting(0); // We want exactly one normal per vertex

        d->polyDataNormals->Update();
        /*
        // Optional settings
        normalGenerator->SetFeatureAngle(0.1);
        normalGenerator->SetSplitting(1);
        normalGenerator->SetConsistency(0);
        normalGenerator->SetAutoOrientNormals(0);
        normalGenerator->SetComputePointNormals(1);
        normalGenerator->SetComputeCellNormals(0);
        normalGenerator->SetFlipNormals(0);
        normalGenerator->SetNonManifoldTraversal(1);
         */
        this->setPolyData(d->polyDataNormals->GetOutput());

        d->mesh->clearNormals();

        // Generic type point normals
        vtkDataArray* normalsGeneric = this->getPolyData()->GetPointData()->GetNormals();
        if (!normalsGeneric)
            return;

        double normalDouble[3];
        if (normalsGeneric->GetNumberOfTuples() != d->mesh->vertex_count()){
            dtkWarn() << "Couldn't compute normals.";
            return;
        }

        // pass normals to mesh
        for (int i = 0; i < normalsGeneric->GetNumberOfTuples(); i++) {
            normalsGeneric->GetTuple(i, normalDouble);
//                std::cout << "--Double " << i << " :" << normalDouble[0] << " " << normalDouble[1] << " " << normalDouble[2] << std::endl;
            d->mesh->push_back_normal(normalDouble);
        }

        // enable use of normals
        //d->mesh->normal_used()=0;
        // update geometry to use normals
        this->onUpdateGeometry();
}

void axlActorMesh::setMode(int state)
{
    this->onModeChanged(state);

    emit stateChanged(this->data(), state);
}

void axlActorMesh::setSize(double size)
{
    //onTubeFilterRadiusChanged(size);
    this->getActor()->GetProperty()->SetPointSize(10 * size);
    this->getActor()->GetProperty()->SetLineWidth(10 * size);

}

void axlActorMesh::onModeChanged(int state)
{
    if(state == 0)
    {
        this->setState(axlActor::passive);
        this->getActor()->SetPickable(1);

        if(d->data)
        {
            QColor color = d->data->color();
            this->getActor()->GetProperty()->SetColor(color.redF(), color.greenF(), color.blueF());
        }
    }
    else if(state == 1)
    {

        this->setState(axlActor::selection);
        this->getActor()->SetPickable(1);
        vtkProperty *prop = this->getActor()->GetProperty();

        if(d->data)
        {
            QColor color = d->data->color();
            qreal *h = new qreal(0.0);
            qreal *s = new qreal(0.0);
            qreal *l = new qreal(0.0);
            color.getHslF(h, s, l);
            color.setHslF(*h, *s, *l + (1.0 - *l) / 2.0);
            prop->SetColor(color.redF(), color.greenF(), color.blueF());
            delete h;
            delete s;
            delete l;
        }
    }
    else if(state == 2)
    {
        this->setState(axlActor::edition);
        this->getActor()->SetPickable(0);
        vtkProperty *prop = this->getActor()->GetProperty();
        if(d->data)
        {
            QColor color = d->data->color();
            qreal *h = new qreal(0.0);
            qreal *s = new qreal(0.0);
            qreal *l = new qreal(0.0);
            color.getHslF(h, s, l);
            color.setHslF(*h, *s, *l + (1.0 - *l)/2.0);
            prop->SetColor(color.redF(), color.greenF(), color.blueF());
            delete h;
            delete s;
            delete l;
        }
    }

    this->Modified();
}

void axlActorMesh::onTubeFilterRadiusChanged(double radius)
{
    Q_UNUSED(radius);
    if(d->mesh &&d->data)
    {
        bool isNormal = (d->mesh->normal_used() && d->mesh->vertex_count() == d->mesh->normal_count());

        if(isNormal)
        {

            if(d->data->size() >= 0.001 &&  d->mesh->edge_show() && (!d->mesh->face_show()))
            {
                //d->meshLineTubeFilter->SetInput(this->getPolyData());
                //this->getMapper()->SetInput(d->meshLineTubeFilter->GetOutput());
#if (VTK_MAJOR_VERSION <= 5)
                this->getMapper()->SetInput(this->getPolyData());
#else
                this->getMapper()->SetInputData(this->getPolyData());
#endif
                //d->meshLineTubeFilter->SetRadius(d->data->size());
                //d->meshLineTubeFilter->Update();
            }
            else
            {
#if (VTK_MAJOR_VERSION <= 5)
                this->getMapper()->SetInput(this->getPolyData());
#else
                this->getMapper()->SetInputData(this->getPolyData());
#endif
            }

        }
        else
        {
            if(d->data->size() >= 0.001 &&  d->mesh->edge_show() && (!d->mesh->face_show()))
            {
                //d->meshLineTubeFilter->SetInput(d->polyDataNormals->GetOutput());
#if (VTK_MAJOR_VERSION <= 5)
                this->getMapper()->SetInput(d->polyDataNormals->GetOutput());
#else
                this->getMapper()->SetInputData(d->polyDataNormals->GetOutput());
#endif
                //d->meshLineTubeFilter->SetRadius(d->data->size());
                //d->meshLineTubeFilter->Update();
            }
            else
            {
#if (VTK_MAJOR_VERSION <= 5)
                this->getMapper()->SetInput(d->polyDataNormals->GetOutput());
#else
                this->getMapper()->SetInputData(d->polyDataNormals->GetOutput());
#endif
            }
        }
    }
}

void axlActorMesh::onUpdateGeometry()
{
    // delete current vtkPoint and vtkCellArray
    this->getMapper()->RemoveAllInputs();
    this->getPolyData()->Initialize();

    // delete current vtkPoint and vtkCellArray
    this->getPoints()->Reset();
    this->getCellArray()->Reset();

    this->getPoints()->Squeeze();
    this->getCellArray()->Squeeze();

    axlMesh *mesh;

//    this->getActor()->GetProperty()->SetInterpolationToFlat();

    //    if(dynamic_cast<axlMesh *>(d->data)){
    //        mesh = dynamic_cast<axlMesh *>(d->data);

    //    }

    //if(d->mesh == NULL){
    if(!(mesh = dynamic_cast<axlMesh *>(d->data)) ){
        // we have to transform the data in a mesh
        foreach(QString t, axlVtkViewPlugin::dataFactSingleton->converters())
        {
            if(!mesh)
            {
                axlAbstractDataConverter *converter = dynamic_cast<axlAbstractDataConverter *>(axlVtkViewPlugin::dataFactSingleton->converter(t));
                if(converter){
                    dtkWarn() << Q_FUNC_INFO << t ;
                    converter->setData(d->data);
                    mesh = converter->toMesh();
                }
            }
        }
        this->getActor()->GetProperty()->SetInterpolationToGouraud();
    }

    if(!mesh)
        return;

    d->mesh = mesh;
    d->data->setMesh(mesh);
    //}

    //    d->mesh->noDuplicateVertices();
    //    qDebug()<< "axlActorMesh : number or unique points "<< d->mesh->verticesUniques_count();
    dtkWarn()<<"axlActorMesh vertices"<<d->mesh->vertex_count();
    dtkWarn()<<"axlActorMesh edges"<<d->mesh->edge_count();
    dtkWarn()<<"axlActorMesh faces"<<d->mesh->face_count();

    axlPoint pointCourant;
    this->getPoints()->SetNumberOfPoints(d->mesh->vertex_count());
    // WITH POLYDATA GRID ---------------------------------------------------


    // VTK POINTS
    for (int i = 0; i < d->mesh->vertex_count(); i++){
        this->getPoints()->SetPoint(i, d->mesh->vertex2(i, &pointCourant)->coordinates());
    }

    //init PolyData geom
    vtkSmartPointer<vtkCellArray> cellArray = this->getCellArray();
    vtkSmartPointer<vtkPolygon> currentPolygon = vtkSmartPointer<vtkPolygon>::New();
    vtkSmartPointer<vtkLine> currentLine = vtkSmartPointer<vtkLine>::New();
    vtkSmartPointer<vtkVertex> currentVertex = vtkSmartPointer<vtkVertex>::New();
    vtkSmartPointer<vtkCellArray> cellArrayVertex = vtkSmartPointer<vtkCellArray>::New();
    vtkSmartPointer<vtkDoubleArray> normals = vtkSmartPointer<vtkDoubleArray>::New();

    // POLYDATA : VERTEX and NORMALS
    bool isNormal = (d->mesh->normal_used() && d->mesh->vertex_count() == d->mesh->normal_count());
    if(isNormal)
    {
        dtkWarn()<<"axlActorMesh::normal 1 ";
        normals->SetNumberOfComponents(3);
        normals->SetNumberOfTuples(d->mesh->normal_count());
        normals->SetName("normalArray");
    }

    axlPoint currentNormal;
    for (int i = 0; i < d->mesh->vertex_count(); i++)
    {
        currentVertex->GetPointIds()->SetId(0, i);
        cellArrayVertex->InsertNextCell(currentVertex);

        if(isNormal)
        {
            d->mesh->normal(i, currentNormal);
            //qDebug()<<mesh->normal(i)->coordinates()[0]<<mesh->normal(i)->coordinates()[1]<<mesh->normal(i)->coordinates()[2];
            normals->SetTuple(i, currentNormal.coordinates());
        }
    }

    //MAPPER LOOKUP TABLE : COLORS
    if(d->mesh->color_count() > 0)
    {
        vtkSmartPointer<vtkIntArray> scalarArray = vtkSmartPointer<vtkIntArray>::New();
        scalarArray->SetName("mapperCollorArrayDefaultField");
        scalarArray->SetNumberOfComponents(1);
        scalarArray->SetNumberOfTuples(d->mesh->vertex_count());

        vtkSmartPointer<vtkLookupTable> lookupTable = vtkSmartPointer<vtkLookupTable>::New();
        lookupTable->SetRange(0.0, d->mesh->vertex_count());
        lookupTable->SetNumberOfTableValues(d->mesh->vertex_count());

        for(int i = 0; i < d->mesh->vertex_count(); i++)
        {
            scalarArray->SetTuple1(i, i);
            lookupTable->SetTableValue(i , d->mesh->colorRF(i), d->mesh->colorGF(i), d->mesh->colorBF(i), 1.0);
        }
        this->getPolyData()->GetPointData()->RemoveArray(scalarArray->GetName());
        this->getPolyData()->GetPointData()->AddArray(scalarArray);
        this->getPolyData()->GetPointData()->SetActiveScalars("mapperCollorArrayDefaultField");

        this->getMapper()->SelectColorArray("mapperCollorArrayDefaultField");
        this->getMapper()->SetLookupTable(lookupTable);
        //        this->getMapper()->SetInterpolateScalarsBeforeMapping(true);
        this->getMapper()->UseLookupTableScalarRangeOn();
        this->getMapper()->SetScalarModeToUsePointData();

        this->getMapper()->ScalarVisibilityOn();

    }



    // POLYDATA : LINES
    vtkSmartPointer<vtkCellArray> cellArrayLine = vtkSmartPointer<vtkCellArray>::New();

    if(d->mesh->edge_show())
    {
        for(int i = 0; i <d->mesh->edge_count(); i++)
        {
            axlMesh::Edge currentEdge = d->mesh->edge(i);

            for(int j=0; j< currentEdge.size()-1;j++) {
                currentLine->GetPointIds()->SetId(0 , currentEdge.at(j));
                currentLine->GetPointIds()->SetId(1 , currentEdge.at(j+1));
                cellArrayLine->InsertNextCell(currentLine);
            }

        }
    }

    // POLYDATA : FACES
    if(d->mesh->face_show())
    {
        for(int i = 0; i <d->mesh->face_count(); i++)
        {
            axlMesh::Face currentPoly = d->mesh->face(i);

            int sizeface = currentPoly.size();
            currentPolygon->GetPointIds()->SetNumberOfIds(sizeface);


            for(int j = 0; j < sizeface; j++)
            {
                currentPolygon->GetPointIds()->SetId(j , currentPoly.at(j));
            }

            if(sizeface !=0){
                cellArray->InsertNextCell(currentPolygon);
            }

        }

    }



    this->getPolyData()->SetPoints(this->getPoints());

    if(d->mesh->vertex_show())
        this->getPolyData()->SetVerts(cellArrayVertex);
    if(d->mesh->edge_show())
        this->getPolyData()->SetLines(cellArrayLine);
    if(d->mesh->face_show())
        this->getPolyData()->SetPolys(this->getCellArray());


    if(isNormal)
    {
        this->getPolyData()->GetPointData()->SetNormals(normals);
#if (VTK_MAJOR_VERSION <= 5)
        this->getMapper()->SetInput(this->getPolyData());
#else
        this->getMapper()->SetInputData(this->getPolyData());
#endif

        if(d->data->size() >= 0.001 &&  d->mesh->edge_show() &&  (!d->mesh->face_show()))
        {
            //d->meshLineTubeFilter->SetInput(this->getPolyData());
            //this->getMapper()->SetInput(d->meshLineTubeFilter->GetOutput());
#if (VTK_MAJOR_VERSION <= 5)
            this->getMapper()->SetInput(this->getPolyData());
#else
            this->getMapper()->SetInputData(this->getPolyData());
#endif
            //d->meshLineTubeFilter->SetRadius(d->data->size());
            //d->meshLineTubeFilter->Update();
        }

    }
    else
    {

        if(false)
        {
            // In the case of vtk Create normal for polygons, we ensure that vtkPoints is cleaned.
            // for each new vtk version. Please juste connec polydata to normal and try to create default rationnal surface with Axel.
            vtkSmartPointer<vtkCleanPolyData> cleanPolyData = vtkSmartPointer<vtkCleanPolyData>::New();
#if (VTK_MAJOR_VERSION <= 5)
            cleanPolyData->SetInput(this->getPolyData());
            d->polyDataNormals->SetInput(cleanPolyData->GetOutput());
            this->getMapper()->SetInput(d->polyDataNormals->GetOutput());
#else
            cleanPolyData->SetInputData(this->getPolyData());
            d->polyDataNormals->SetInputData(cleanPolyData->GetOutput());
            this->getMapper()->SetInputData(d->polyDataNormals->GetOutput());
#endif
        } else {
#if (VTK_MAJOR_VERSION <= 5)
            this->getMapper()->SetInput(this->getPolyData());
#else
            this->getMapper()->SetInputData(this->getPolyData());
#endif
        }
        //this->getMapper()->SetInput(this->getPolyData());

        if(d->data->size() >= 0.001 &&  d->mesh->edge_show() && (!d->mesh->face_show()))
        {
            //d->meshLineTubeFilter->SetInput(this->getPolyData());
            //this->getMapper()->SetInput(d->meshLineTubeFilter->GetOutput());
#if (VTK_MAJOR_VERSION <= 5)
            this->getMapper()->SetInput(this->getPolyData());
#else
            this->getMapper()->SetInputData(this->getPolyData());
#endif
            //d->meshLineTubeFilter->SetRadius(d->data->size());
            //d->meshLineTubeFilter->Update();
        }

    }

    if(!(d->mesh->fields().isEmpty())){
        d->mesh->touchField();
    }

    emit updated();
}

//void axlActorMesh::selectCells(vtkSmartPointer<vtkPolyData> data){


//    vtkSmartPointer<vtkTriangleFilter> triangleFilter =
//            vtkSmartPointer<vtkTriangleFilter>::New();
//#if (VTK_MAJOR_VERSION <= 5)
//    //triangleFilter->SetInput(this->getPolyData());
//    triangleFilter->SetInput(data);
//#else
//    //triangleFilter->SetInputData(this->getPolyData());
//    triangleFilter->SetInputData(data);
//#endif
//    triangleFilter->Update();

//    vtkSmartPointer<vtkPolyDataMapper> mapper =
//            vtkSmartPointer<vtkPolyDataMapper>::New();
//#if (VTK_MAJOR_VERSION <= 5)
//    //mapper->SetInput(this->getPolyData());
//    mapper->SetInput(data);
//#else
//    //mapper->SetInputData(this->getPolyData());
//    mapper->SetInputData(data);
//#endif

//    vtkSmartPointer<vtkActor> actor =
//            vtkSmartPointer<vtkActor>::New();
//    //if(d->data)
//    //actor->GetProperty()->SetColor(d->data->color().red(), d->data->color().green(), d->data->color().blue());
//    actor->SetMapper(mapper);

//    vtkSmartPointer<vtkRenderWindowInteractor> renderWindowInteractor =this->getInteractor();

//    //Create picker
//    if(!d->picker)
//        d->picker  = axlInteractorCellPicking::New();

//    d->picker->SetDefaultRenderer(this->getInteractor()->GetRenderWindow()->GetRenderers()->GetFirstRenderer());
//    d->picker->setData(triangleFilter->GetOutput());


//    renderWindowInteractor->SetInteractorStyle(d->picker);

//    this->AddPart(actor);
//    //    renderer->ResetCamera();

//    //    renderer->SetBackground(0,0,1); // Blue

//    //    renderWindow->Render();
//    //    renderWindowInteractor->Start();
//}

//void axlActorMesh::selectEdges(void){


//    vtkSmartPointer<vtkIdFilter> idFilter =
//            vtkSmartPointer<vtkIdFilter>::New();
//#if (VTK_MAJOR_VERSION <= 5)
//    idFilter->SetInput(this->getPolyData());
//#else
//    idFilter->SetInputData(this->getPolyData());
//#endif
//    idFilter->SetIdsArrayName("OriginalIds");
//    idFilter->Update();

//    vtkSmartPointer<vtkDataSetSurfaceFilter> surfaceFilter =
//            vtkSmartPointer<vtkDataSetSurfaceFilter>::New();
//    surfaceFilter->SetInputConnection(idFilter->GetOutputPort());
//    surfaceFilter->Update();

//    vtkPolyData* input = surfaceFilter->GetOutput();



//    vtkSmartPointer<vtkAreaPicker> areaPicker =
//            vtkSmartPointer<vtkAreaPicker>::New();
//    vtkSmartPointer<vtkRenderWindowInteractor> renderWindowInteractor = this->getInteractor();
//    renderWindowInteractor->SetPicker(areaPicker);

//    //Create picker
//    if(!d->style){
//        d->style  = axlInteractorStyleRubberBandPick::New();
//        connect(d->style, SIGNAL(IdsSelectedChanged()),this,SLOT(onSelectionChanged()));
//    }

//    d->style->SetPolyData(input);

//    renderWindowInteractor->SetInteractorStyle( d->style );
//    //renderWindowInteractor->Start();

//}

//void axlActorMesh::selectBoundaryEdges(void){

//    vtkSmartPointer<vtkFeatureEdges> featureEdges =
//            vtkSmartPointer<vtkFeatureEdges>::New();
//#if (VTK_MAJOR_VERSION <= 5)
//    featureEdges->SetInput(this->getPolyData());
//#else
//    featureEdges->SetInputData(this->getPolyData());
//#endif
//    featureEdges->BoundaryEdgesOn();
//    featureEdges->FeatureEdgesOff();
//    featureEdges->ManifoldEdgesOff();
//    featureEdges->NonManifoldEdgesOff();
//    featureEdges->Update();

//    vtkSmartPointer<vtkPolyDataMapper> edgeMapper =
//            vtkSmartPointer<vtkPolyDataMapper>::New();
//    edgeMapper->SetInputConnection(featureEdges->GetOutputPort());
//    vtkSmartPointer<vtkActor> edgeActor =
//            vtkSmartPointer<vtkActor>::New();
//    edgeActor->SetMapper(edgeMapper);

//    vtkSmartPointer<vtkPolyData> poly = featureEdges->GetOutput();

//    int nbLines = poly->GetNumberOfLines();
//    vtkSmartPointer<vtkIdTypeArray> ids = poly->GetLines()->GetData();

//    qDebug() << Q_FUNC_INFO << nbLines;

//    //selectEdges(featureEdges->GetOutput());

//}

//void axlActorMesh::onSelectionChanged(void){
//    qDebug() << Q_FUNC_INFO;
//    if(d->mesh)
//        d->mesh->setSelectedVertices(d->style->ids());
//}

axlActorMesh::axlActorMesh(void) : axlActor(), d(new axlActorMeshPrivate)
{
    d->polyDataNormals = vtkSmartPointer<vtkPolyDataNormals>::New();
    d->polyDataNormals->AutoOrientNormalsOn();
    d->data = NULL;
    d->mesh = NULL;
    d->style = NULL;
    d->picker = NULL;

}

axlActorMesh::~axlActorMesh(void)
{
    disconnect(d->style, SIGNAL(IdsSelectedChanged()),this,SLOT(onSelectionChanged()));
    delete d;

    d = NULL;
}

axlAbstractActor *createAxlActorMesh(void){

    return axlActorMesh::New();

}
