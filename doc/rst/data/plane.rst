Plane
=====
A plane is defined by an origin (axlPoint) and a normal vector (axlPoint).

Type: ``axlPlane``

Format:

.. literalinclude:: ../../../data/plane.axl 
   :lines: 2-5
   :language: xml
