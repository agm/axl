/* axlActorSurfaceTrimmed.cpp ---
 *
 * Author: Meriadeg Perrinel
 * Copyright (C) 2008 - Meriadeg Perrinel, Inria.
 * Created: Fri Dec 17 11:06:08 2010 (+0100)
 * Version: $Id$
 * Last-Updated: Mon Dec 17 15:04:32 2012 (+0100)
 *           By: Julien Wintz
 *     Update #: 76
 */

/* Commentary:
 *
 */

/* Change log:
 *
 */

#include "axlActorSurfaceTrimmed.h"
#include "axlControlPointsWidget.h"

#include <axlCore/axlAbstractSurfaceTrimmed.h>

#include <dtkMathSupport/dtkVector3D.h>

#include <vtkActor.h>
#include <vtkActorCollection.h>
#include <vtkCellArray.h>
#include <vtkCellData.h>
#include <vtkCommand.h>
#include <vtkDoubleArray.h>
#include <vtkLookupTable.h>
#include <vtkObjectFactory.h>
//#include <vtkPainterPolyDataMapper.h>
#include <vtkPoints.h>
#include <vtkPointData.h>
#include <vtkPolyData.h>
#include <vtkPolyVertex.h>
#include <vtkPolygon.h>
#include <vtkPolyDataMapper.h>
#include <vtkPolyDataNormals.h>
#include <vtkProperty.h>
#include <vtkProp.h>
#include <vtkQuad.h>
#include <vtkTexture.h>
#include <vtkTriangle.h>
#include <vtkTimerLog.h>
#include <vtkSmartPointer.h>

// /////////////////////////////////////////////////////////////////
// axlActorSurfaceTrimmedPrivate
// /////////////////////////////////////////////////////////////////

class axlActorSurfaceTrimmedPrivate
{
public:
    axlAbstractSurfaceTrimmed *splineSurface;
};

#if (VTK_MAJOR_VERSION <= 5)
vtkCxxRevisionMacro(axlActorSurfaceTrimmed, "$Revision: 0.0.1 $");
#endif

vtkStandardNewMacro(axlActorSurfaceTrimmed);

dtkAbstractData *axlActorSurfaceTrimmed::data(void)
{
    return d->splineSurface;
}

void axlActorSurfaceTrimmed::setSurface(axlAbstractSurfaceTrimmed *spline_Surface)
{
    // we compute here points, triangles, and normals
    d->splineSurface = spline_Surface;

    this->setPoints(vtkSmartPointer<vtkPoints>::New());
    this->setActor(vtkSmartPointer<vtkActor>::New());
    this->setCellArray(vtkSmartPointer<vtkCellArray>::New());
    this->setPolyData(vtkSmartPointer<vtkPolyData>::New());
    this->setMapper(vtkSmartPointer<vtkPolyDataMapper>::New());


    this->mehsProcess();

   this->setMapperCollorArray();


    /*// add sphere param
    this->initCurrentPoint();*/
    vtkProp3D *prop3d =  this->getActor();
    this->AddPart(prop3d);

    // add the observer
    if(!this->getObserver())
    {
        this->NewObserver();
        this->setObserverData(d->splineSurface);
    }

    QString shader = d->splineSurface->shader();
    if(!shader.isEmpty())
        this->setShader(shader);


}

void axlActorSurfaceTrimmed::mehsProcess(void)
{

    double start_u = d->splineSurface->startParam_u();
    double start_v = d->splineSurface->startParam_v();
    double end_u = d->splineSurface->endParam_u();
    double end_v = d->splineSurface->endParam_v();
    double paramCourant_u = start_u;
    double paramCourant_v = start_v;

    axlPoint pointCourant;

    int n_u = d->splineSurface->numSamples_u();// need to be superior than 1
    int n_v = d->splineSurface->numSamples_v();


    double interval_u = (double)(end_u - start_u) / (n_u - 1);
    double interval_v = (double)(end_v - start_v) / (n_v - 1);

    vtkSmartPointer<vtkDoubleArray> normals = vtkSmartPointer<vtkDoubleArray>::New();
    normals->SetNumberOfComponents(3);
    normals->SetNumberOfTuples(n_u * n_v);
    normals->SetName("normalArray");
    double *currentNormal = new double[3];

    dtkDeprecated::dtkVector<bool> inDomain;
    inDomain.allocate(n_u * n_v);

    dtkDeprecated::dtkVector<double> normal;
    for(int j = 0; j < n_v - 1; j++)
    {
        for(int i = 0; i < n_u - 1; i++)
        {
            if(d->splineSurface->inDomain(paramCourant_u, paramCourant_v))
                inDomain[j * n_u + i] = true;
            else
                inDomain[j * n_u + i] = false;

            pointCourant = d->splineSurface->eval(paramCourant_u, paramCourant_v);
            normal = d->splineSurface->normal(paramCourant_u, paramCourant_v).unit();
            currentNormal[0] = normal[0];
            currentNormal[1] = normal[1];
            currentNormal[2] = normal[2];
            normals->SetTuple(j * n_u + i, currentNormal);

            this->getPoints()->InsertNextPoint(pointCourant.x(), pointCourant.y(), pointCourant.z());
            paramCourant_u += interval_u;
        }

        if(d->splineSurface->inDomain(end_u, paramCourant_v))
            inDomain[j * n_u +  n_u - 1] = true;
        else
            inDomain[j * n_u +  n_u - 1] = false;

        normal = d->splineSurface->normal(end_u, paramCourant_v).unit();
        currentNormal[0] = normal[0];
        currentNormal[1] = normal[1];
        currentNormal[2] = normal[2];
        normals->SetTuple(j * n_u +  n_u - 1, currentNormal);

        pointCourant = d->splineSurface->eval(end_u, paramCourant_v);
        this->getPoints()->InsertNextPoint(pointCourant.x(), pointCourant.y(), pointCourant.z());
        paramCourant_u = start_u;
        paramCourant_v += interval_v;
    }

    for(int i = 0; i < n_u - 1; i++)
    {
        if(d->splineSurface->inDomain(paramCourant_u, end_v))
            inDomain[(n_v - 1) * n_u + i] = true;
        else
            inDomain[(n_v - 1) * n_u + i] = false;


        normal = d->splineSurface->normal(paramCourant_u, end_v).unit();
        currentNormal[0] = normal[0];
        currentNormal[1] = normal[1];
        currentNormal[2] = normal[2];
        normals->SetTuple((n_v - 1) * n_u + i, currentNormal);

        pointCourant=d->splineSurface->eval(paramCourant_u, end_v);
        this->getPoints()->InsertNextPoint(pointCourant.x(), pointCourant.y(), pointCourant.z());
        paramCourant_u += interval_u;
    }

    if(d->splineSurface->inDomain(end_u, end_v))
        inDomain[n_v * n_u - 1] = true;
    else
        inDomain[n_v * n_u - 1] = false;


    normal = d->splineSurface->normal(end_u, end_v).unit();
    currentNormal[0] = normal[0];
    currentNormal[1] = normal[1];
    currentNormal[2] = normal[2];
    normals->SetTuple(n_v * n_u - 1, currentNormal);

    pointCourant = d->splineSurface->eval(end_u, end_v);

    this->getPoints()->InsertNextPoint(pointCourant.x() , pointCourant.y(), pointCourant.z());

    int ind1 = 0;
    int ind2 = 0;

    vtkSmartPointer<vtkTriangle> currentTriangle = vtkSmartPointer<vtkTriangle>::New();
    currentTriangle->GetPointIds()->SetNumberOfIds(3);

    for(int j = 0; j < n_v - 1; j++)
    {
        for(int i= 0; i <n_u - 1; i++)
        {
            ind1 =  j * n_u + i;
            ind2 = ind1 + n_u;

            currentTriangle->GetPointIds()->SetId(0, ind1);
            currentTriangle->GetPointIds()->SetId(1, ind1 + 1);
            currentTriangle->GetPointIds()->SetId(2, ind2);

            if(inDomain[ind1] + inDomain[ind1 + 1] + inDomain[ind2] > 1)
                this->getCellArray()->InsertNextCell(currentTriangle);

            currentTriangle->GetPointIds()->SetId(1, ind1 + 1);
            currentTriangle->GetPointIds()->SetId(0, ind2);
            currentTriangle->GetPointIds()->SetId(2, ind2 + 1);

            if(inDomain[ind1] + inDomain[ind1 + 1] + inDomain[ind2] > 1)
                this->getCellArray()->InsertNextCell(currentTriangle);
        }
    }

    this->getPolyData()->SetPoints(this->getPoints());
    this->getPolyData()->SetPolys(this->getCellArray());
    //this->getPolyData()->GetPointData()->SetNormals(normals);

    vtkSmartPointer<vtkPolyDataNormals> polyDataNormals = vtkSmartPointer<vtkPolyDataNormals>::New();
    polyDataNormals->AutoOrientNormalsOn();
    polyDataNormals->FlipNormalsOn();
#if (VTK_MAJOR_VERSION <= 5)
    polyDataNormals->SetInput(this->getPolyData());
    this->getMapper()->SetInput(polyDataNormals->GetOutput());
#else
    polyDataNormals->SetInputData(this->getPolyData());
    this->getMapper()->SetInputData(polyDataNormals->GetOutput());
#endif
    this->getActor()->SetMapper(this->getMapper());

    if(d->splineSurface->fields().count() != 0)
        d->splineSurface->touchField();

}



void axlActorSurfaceTrimmed::setMapperCollorArray(void)
{
    vtkSmartPointer<vtkDoubleArray> scalarArray = vtkSmartPointer<vtkDoubleArray>::New();
    scalarArray->SetName("mapperCollorArrayDefaultField");
    scalarArray->SetNumberOfComponents(1);

    double start_u = d->splineSurface->startParam_u();
    double start_v = d->splineSurface->startParam_v();
    double end_u = d->splineSurface->endParam_u();
    double end_v = d->splineSurface->endParam_v();
    double paramCourant_u = start_u;
    double paramCourant_v = start_v;

    int n_u = d->splineSurface->numSamples_u();// need to be superior than 1
    int n_v = d->splineSurface->numSamples_v();

    scalarArray->SetNumberOfTuples(n_u * n_v);

    double interval_u = (double)(end_u - start_u) / (n_u - 1);
    double interval_v = (double)(end_v - start_v) / (n_v - 1);

    for(int i = 0; i < n_v - 1 ; i++)
    {
        for(int j = 0; j < n_u - 1 ; j++)
        {
            scalarArray->SetTuple1(i * n_u + j, d->splineSurface->inDomain(paramCourant_u, paramCourant_v) ? 1:0);
            paramCourant_u += interval_u;
        }

        scalarArray->SetTuple1(i * n_u + (n_u - 1), d->splineSurface->inDomain(end_u, paramCourant_v) ? 1:0);
        paramCourant_u = start_u;
        paramCourant_v += interval_v;
    }
    for(int i = 0; i < n_u - 1; i++)
    {
        scalarArray->SetTuple1(n_u * (n_v - 1) + i, d->splineSurface->inDomain(paramCourant_u, end_v) ? 1:0);
        paramCourant_u += interval_u;
    }
    scalarArray->SetTuple1(n_u * n_v - 1, d->splineSurface->inDomain(end_u, end_v) ? 1:0);


    vtkSmartPointer<vtkPolyData> data = this->getPolyData();
    data->GetPointData()->AddArray(scalarArray);
    data->GetPointData()->SetActiveScalars("mapperCollorArrayDefaultField");

    vtkSmartPointer<vtkLookupTable> lookupTable = vtkSmartPointer<vtkLookupTable>::New();
    lookupTable->SetRange(0.0, 1.0);
    lookupTable->SetNumberOfTableValues(2);
    lookupTable->Build();

    lookupTable->SetTableValue(0 , 0.0, 0.0, 0.0, 0.0);
    lookupTable->SetTableValue(1 , 1.0, 1.0, 1.0, 1.0);


    //add vertex attrib array
    vtkSmartPointer<vtkPolyDataMapper> mapper = this->getMapper();
    vtkSmartPointer<vtkPolyData> polyData = this->getPolyData();
    mapper->SetLookupTable(lookupTable);
    mapper->SetInterpolateScalarsBeforeMapping(true);
    mapper->UseLookupTableScalarRangeOn();

    this->Modified();
}

void axlActorSurfaceTrimmed::setControlPolygon(bool control)
{
    if(control) {
        if(!this->getControlPoints())
        {
            //widget drawing

            this->setControlPoints(axlControlPointsWidget::New());
            this->getControlPoints()->SetInteractor(this->getInteractor());
            this->getControlPoints()->SetProp3D(this->getActor());
            this->getControlPoints()->setSpline(d->splineSurface);
            this->getControlPoints()->initializePoints();
            this->getControlPoints()->PlaceWidget();

            this->AddPart(this->getControlPoints()->netActor());

            this->getControlPoints()->ptsActors()->InitTraversal();

            for(vtkIdType i = 0; i < this->getControlPoints()->ptsActors()->GetNumberOfItems(); i++)
            {
                this->AddPart(this->getControlPoints()->ptsActors()->GetNextActor());
            }
        }

        if(this->getObserver())
        {
            this->addToObserver(vtkCommand::InteractionEvent, (vtkCommand *)this->getObserver());
        }


        // there is always the controlPoints there
        this->getControlPoints()->SetEnabled(true);
    }

    if (!control)
    {
        if (this->getActor()) {
#if (VTK_MAJOR_VERSION <= 5)
            this->getMapper()->SetInput(this->getPolyData());
#else
            this->getMapper()->SetInputData(this->getPolyData());
#endif

            if(this->getControlPoints())
            {
                this->RemovePart(this->getControlPoints()->netActor());
                this->getControlPoints()->ptsActors()->InitTraversal();
                for(vtkIdType i = 0; i < this->getControlPoints()->ptsActors()->GetNumberOfItems(); i++)
                {
                    this->RemovePart(this->getControlPoints()->ptsActors()->GetNextActor());
                }

                this->getControlPoints()->SetEnabled(false);
                this->setControlPoints(NULL);
            }
        }
    }
}

void axlActorSurfaceTrimmed::onSamplingChanged(void)
{

    //qDebug()<<"axlActorSurfaceTrimmed::onSamplingChanged";
    if(d->splineSurface)
    {
        this->getMapper()->RemoveAllInputs();
        this->getPolyData()->Initialize();

        // delete current vtkPoint and vtkCellArray
        this->getPoints()->Reset();
        this->getCellArray()->Reset();

        this->getPoints()->Squeeze();
        this->getCellArray()->Squeeze();

       this->mehsProcess();

    }
}

axlActorSurfaceTrimmed::axlActorSurfaceTrimmed(void) : axlActorSurfaceBSpline(), d(new axlActorSurfaceTrimmedPrivate)
{

}

axlActorSurfaceTrimmed::~axlActorSurfaceTrimmed(void)
{
    delete d;

    d = NULL;
}
