cd C:\builds\workspace\axelVisualStudio2010\
if exist "C:\builds\workspace\axelVisualStudio2010\dtk" "C:\Program Files\Git\bin\sh.exe" --login -i -c "rm -rf dtk"
if exist "C:\builds\workspace\axelVisualStudio2010\axel" "C:\Program Files\Git\bin\sh.exe" --login -i -c "rm -rf axel"
if exist "C:\builds\workspace\axelVisualStudio2010\vtkview" "C:\Program Files\Git\bin\sh.exe" --login -i -c "rm -rf vtkview"
"C:\Program Files\Git\bin\sh.exe" --login -i -c "git clone git://dtk.inria.fr/axel/axel.git"
"C:\Program Files\Git\bin\sh.exe" --login -i -c "git clone https://github.com/d-tk/dtk.git dtk"
"C:\Program Files\Git\bin\sh.exe" --login -i -c "git clone git://dtk.inria.fr/axel/vtkview.git"
