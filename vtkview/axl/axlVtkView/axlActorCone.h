/* axlActorCone.h ---
 *
 * Author: Meriadeg Perrinel
 * Copyright (C) 2008 - Meriadeg Perrinel, Inria.
 * Created: Fri Dec 17 10:58:38 2010 (+0100)
 * Version: $Id$
 * Last-Updated: Mon Dec 17 14:04:15 2012 (+0100)
 *           By: Julien Wintz
 *     Update #: 9
 */

/* Commentary:
 *
 */

/* Change log:
 *
 */

#ifndef AXLACTORCONE_H
#define AXLACTORCONE_H

#include "axlActor.h"

#include <QVTKOpenGLWidget.h>

#include "axlVtkViewPluginExport.h"

#include <vtkVersion.h>

class axlActorConePrivate;
class axlCone;

class vtkConeSource;

class AXLVTKVIEWPLUGIN_EXPORT axlActorCone : public axlActor
{
public:
#if (VTK_MAJOR_VERSION <= 5)
    vtkTypeRevisionMacro(axlActorCone, vtkAssembly);
#endif

    static axlActorCone *New(void);

public:
   dtkAbstractData *data(void);
   vtkConeSource *cone(void);
   vtkSmartPointer<vtkPolyData> getPolyData();


public:
    void setDisplay(bool display);
    void showConeWidget(bool show);
    void setConeWidget(bool coneWidget);
    bool isShowConeWidget(void);
    virtual void setData(dtkAbstractData *cone1);
    void setMode(int state);
    void setQVTKWidget(QVTKOpenGLWidget *widget);

public slots:
    void onModeChanged(int state);
    void onRemoved();
    void onUpdateGeometry();

protected:
     axlActorCone(void);
    ~axlActorCone(void);

private:
    axlActorCone(const axlActorCone&); // Not implemented.
    void operator = (const axlActorCone&); // Not implemented.

private:
    axlActorConePrivate *d;
};

axlAbstractActor *createAxlActorCone(void);

#endif //AXLACTORCONE_H
