======================
Using Axl from Python
======================

*Anais Ducoffe*

Thanks to a JSON RPC protocol, using the library qjsonrpc, Axl can be called from a python application. For further information about what is a JSON RPC protocol you can take a look here: https://en.wikipedia.org/wiki/Remote_procedure_call.
This document explains how you can use this functionality.

How I can get this functionality ?
==================================

This protocol was added in the version 2.3.2 of Axl. If you get the sources you must have installed qjsonrpc and put the cmake variable JSON_RPC to ON. That's way Axl embeds a server which listens to all messages send by ``axlClient`` data.

Requirements
============

To communicate with Axl, the python application must use a client to send messages to the Axl server. The client must be of type ``axlClient`` defined in Axl kernel application. To use those kind of data you should import some part of Axl such as described below:

.. code-block:: python

   import sys 
   sys.path.append("your_path_to_axl_modules") 
   # To get some axl data 
   import axlcore 
   from axlcore import * 
   # To get axlClient methods 
   import axlrpc 
   from axlrpc import *

You'll find Axl modules folder in the build folder of Axl if you are using sources. Otherwise it must be installed on your computer system, for instance C:\Program Files (x86)\axl-2.3.2\modules on Windows. You should also initialize factories for getting Axl plugins data and processes.

.. code-block:: python
		
   #Initialize factories for plugins
   dtkPluginManager = axlcore.dtkPluginManager.instance()
   dtkPluginManager.initializeApplication() 
   dtkPluginManager.initialize() 
   dtkDataFactory = axlcore.dtkAbstractDataFactory.instance() 
   dtkProcessFactory = axlcore.dtkAbstractProcessFactory.instance()

A more comprehensive example is given in section 4.
Once Axl has been configured,  the file axl.py is generated in the build directory. It contains the previous instructions with the adequate corresponding variables. To set up the axl environment, one can use the following command:

.. code-block:: python

   from axl import *

The following variables are accessible:
   - axl_dir: name of the folder which contains the module
   - axl_app: complete path of the application axl.  

What kind of communication is possible ?
========================================
The communication is bi-directionnal as you can get data created in Axl with your python application and sending python application data to Axl view. Your python application can also ask Axl to compute an axl algorithm/process, with specific inputs and parameters, and getting the result(s).

Sending data to the Axl View
-----------------------------
 - ``sendData(axlAbstractData *data)``: send the object in the Axl view. 
 - ``modifyData(axlAbstractData *data)``: modify the properties of the object in Axl view.
 - ``deleteData(axlAbstractData *data)``: delete the object representation in Axl view. 

Getting data from Axl
----------------------
 - ``update(axlAbstractData *data)``: update the properties of the corresponding object in the python application. If the object was deleted in Axl view delete it. 
 - ``getData(QString name)``: an object was created in Axl, create the corresponding object in python application. 

Asking Axl to run a process
----------------------------
 - ``callProcess("processName",listInput, listParam)``: Axl computes the algorithm with the inputs list and parameters given. Returns the output. ListInput and listParameter are of type ``axlAbstractDataComposite``. 


A python script example
=======================

.. literalinclude:: expl.py

