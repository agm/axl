/* axlActorVolumeBSpline.h ---
 *
 * Author: Anais Ducoffe
 * Copyright (C) 2012 - Anais Ducoffe, Inria.
 * Created:
 * Version: $Id$
 * Last-Updated:
 *           By:
 *     Update #:
 */

/* Commentary:
 *
 */

/* Change log:
 *
 */

#ifndef AXLACTORVOLUMEBSPLINE_H
#define AXLACTORVOLUMEBSPLINE_H

#include "axlActorBSpline.h"

#include "axlVtkViewPluginExport.h"

#include <vtkVersion.h>

class axlAbstractVolumeBSpline;
class axlActorVolumeBSplinePrivate;

class AXLVTKVIEWPLUGIN_EXPORT axlActorVolumeBSpline : public axlActorBSpline
{
public:
#if (VTK_MAJOR_VERSION <= 5)
    vtkTypeRevisionMacro(axlActorVolumeBSpline, vtkAssembly);
#endif

    static axlActorVolumeBSpline *New(void);

public:
    dtkAbstractData *data(void);

public:
    virtual void setData(dtkAbstractData *spline_Volume1);
    void setMapperCollorArray(void);
    void meshProcess(void);
    void pointsUpdate(void);
    void UnstructuredGridUpdate(void);


public slots:
    virtual void onSamplingChanged(void);
    virtual void onUpdateGeometry(void);

protected:
     axlActorVolumeBSpline(void);
    ~axlActorVolumeBSpline(void);

private:
    axlActorVolumeBSpline(const axlActorVolumeBSpline&); // Not implemented.
        void operator = (const axlActorVolumeBSpline&); // Not implemented.

private:
    axlActorVolumeBSplinePrivate *d;
};

axlAbstractActor *createAxlActorVolumeBSpline(void);

#endif
