/* axlActorPoint.h ---
 *
 * Author: Meriadeg Perrinel
 * Copyright (C) 2008 - Meriadeg Perrinel, Inria.
 * Created: Fri Dec 17 10:58:38 2010 (+0100)
 * Version: $Id$
 * Last-Updated: Mon Dec 17 14:11:44 2012 (+0100)
 *           By: Julien Wintz
 *     Update #: 5
 */

/* Commentary:
 *
 */

/* Change log:
 *
 */

#ifndef AXLACTORPOINT_H
#define AXLACTORPOINT_H

#include "axlActor.h"
#include <QVTKOpenGLWidget.h>

#include "axlVtkViewPluginExport.h"

class axlPoint;

class vtkSphereSource;

class axlActorPointPrivate;

class AXLVTKVIEWPLUGIN_EXPORT axlActorPoint : public axlActor
{
public:
#if (VTK_MAJOR_VERSION <= 5)
    vtkTypeRevisionMacro(axlActorPoint, vtkAssembly);
#endif

    static axlActorPoint *New(void);

public:
   dtkAbstractData *data(void);
   vtkSphereSource *sphere(void);


public:
    virtual void setData(dtkAbstractData *point1);
    void setMode(int state);
    void setQVTKWidget(QVTKOpenGLWidget *widget);

    void showPointWidget(bool show);
    void setPointWidget(bool pointWidget);
    void setDisplay(bool display);
    bool isShowPointWidget();

    void setSize(double size);

public slots:
    void onModeChanged(int state);
    void onRemoved();
    void onUpdateGeometry(void);

protected:
     axlActorPoint(void);
    ~axlActorPoint(void);

private:
    axlActorPoint(const axlActorPoint&); // Not implemented.
    void operator = (const axlActorPoint&); // Not implemented.

private:
    axlActorPointPrivate *d;
};

axlAbstractActor *createAxlActorPoint(void);

#endif //AXLACTORPoint_H
