/* axlActorPlane.cpp ---
 *
 * Author: Meriadeg Perrinel
 * Copyright (C) 2008 - Meriadeg Perrinel, Inria.
 * Created: Fri Dec 17 10:59:21 2010 (+0100)
 * Version: $Id$
 * Last-Updated: Mon Dec 17 15:02:10 2012 (+0100)
 *           By: Julien Wintz
 *     Update #: 32
 */

/* Commentary:
 *
 */

/* Change log:
 *
 */
#include "axlActorPlane.h"

#include "axlActorComposite.h"
#include "axlInteractorStyleSwitch.h"

#include <axlCore/axlPlane.h>
#include <axlCore/axlPoint.h>

#include <QVTKOpenGLWidget.h>

#include <vtkActor.h>
#include <vtkAssemblyPath.h>
//#include <vtkCellPicker.h>
#include <vtkCommand.h>
#include <vtkGlyph3D.h>
#include <vtkInteractorStyleSwitch.h>
#include <vtkLight.h>
#include <vtkLightCollection.h>
#include <vtkLineWidget.h>
#include <vtkObjectFactory.h>
#include <vtkPoints.h>
#include <vtkPolyData.h>
#include <vtkPolyDataMapper.h>
#include <vtkProperty.h>
#include <vtkRenderer.h>
#include <vtkRendererCollection.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkSmartPointer.h>
#include <vtkPlaneSource.h>
#include <vtkTransform.h>


class axlActorPlaneObserver : public vtkCommand
{
public:
    static axlActorPlaneObserver *New(void)
    {
        return new axlActorPlaneObserver;
    }

    virtual void Execute(vtkObject *caller, unsigned long event, void *)
    {
        vtkRenderWindowInteractor *interactor = observerDataAssembly->getInteractor();            //interactor->Render();
        (void)interactor;

        if(event == vtkCommand::InteractionEvent)
        {

            if(!lineWidget)
                return;

            if (caller == lineWidget)
            {
                observerData_planeSource->Modified();
                observerData_planeSource->Update();
                axlPoint p1(lineWidget->GetPoint1()[0], lineWidget->GetPoint1()[1], lineWidget->GetPoint1()[2]);
                axlPoint p2(lineWidget->GetPoint2()[0], lineWidget->GetPoint2()[1], lineWidget->GetPoint2()[2]);
                axlPoint normal = p2 - p1;
                observerData_plane->setPoint(lineWidget->GetPoint1());
                observerData_plane->setNormal(normal.coordinates());
                observerData_plane->touchGeometry();
                observerDataAssembly->onUpdateGeometry();

            }

        }


    }

    axlInteractorStyleSwitch *axlInteractorStyle;
//    vtkCellPicker *axlPlanePicker;
    axlPlane *observerData_plane;
    axlActorPlane *observerDataAssembly;
    vtkPlaneSource *observerData_planeSource;

    vtkLineWidget *lineWidget = nullptr;
};

// /////////////////////////////////////////////////////////////////
// axlActorPlanePrivate
// /////////////////////////////////////////////////////////////////

class axlActorPlanePrivate
{
public:
    axlPlane *plane;
    vtkPlaneSource *planeSource;
    vtkLineWidget *lineWidget;
    axlActorPlaneObserver *planeObserver;
    QVTKOpenGLWidget *widget;
//    vtkCellPicker *axlPlanePicker ;

    vtkSmartPointer<vtkRenderer> renderer;
};

#if (VTK_MAJOR_VERSION <= 5)
vtkCxxRevisionMacro(axlActorPlane, "$Revision: 0.0.1 $");
#endif

vtkStandardNewMacro(axlActorPlane);

dtkAbstractData *axlActorPlane::data(void)
{
    return d->plane;
}

vtkPlaneSource *axlActorPlane::plane(void)
{
    return d->planeSource;
}

void axlActorPlane::setQVTKWidget(QVTKOpenGLWidget *widget)
{
    d->widget = widget;
}


void axlActorPlane::setDisplay(bool display)
{
    axlActor::setDisplay(display);

    if(display && d->lineWidget){
        this->showPlaneWidget(true);
    }

    if(!display && d->lineWidget){
        this->showPlaneWidget(false);
    }
}

void axlActorPlane::showPlaneWidget(bool show)
{
    if(!d->lineWidget ) {
        qDebug() << "No tet actor computed for this axlActorPlane.";
        return;
    }

    if(show){
        d->lineWidget->SetEnabled(1);
    }

    if(!show){
        d->lineWidget->SetEnabled(0);

    }

}

void axlActorPlane::setPlaneWidget(bool planeWidget)
{
    if(planeWidget) {
        if(!d->lineWidget)
        {
            //widget drawing

            d->lineWidget = vtkLineWidget::New();
            d->lineWidget->SetInteractor(this->getInteractor());
            d->lineWidget->SetProp3D(this->getActor());
            d->lineWidget->PlaceWidget();
            d->lineWidget->SetPoint1(d->planeSource->GetCenter());
            axlPoint center(d->planeSource->GetCenter()[0], d->planeSource->GetCenter()[1], d->planeSource->GetCenter()[2]);
            axlPoint normal(d->planeSource->GetNormal()[0], d->planeSource->GetNormal()[1], d->planeSource->GetNormal()[2]);
            center += normal;
            d->lineWidget->SetPoint2(center.coordinates());
        }

        if(d->planeObserver)
        {
            d->lineWidget->AddObserver(vtkCommand::InteractionEvent, d->planeObserver);


            d->planeObserver->lineWidget = d->lineWidget;
        }

        // there is always the controlPoints there
        d->lineWidget->SetEnabled(true);

    }

    if (!planeWidget)
    {
        if (this->getActor()) {
            // this->getMapper()->SetInput(this->getPolyData());

            if(d->lineWidget)
            {
                d->lineWidget->RemoveAllObservers();
                d->lineWidget->SetEnabled(false);
                d->lineWidget->Delete(); // warning not sure
                d->lineWidget = NULL;
            }
        }
    }
    if (d->planeObserver)
        d->planeObserver->lineWidget = d->lineWidget;
}

void axlActorPlane::setData(dtkAbstractData *plane1)
{
    axlPlane *plane = dynamic_cast<axlPlane *>(plane1);
    if(plane)
    {
        d->plane = plane;
        this->setMapper(vtkSmartPointer<vtkPolyDataMapper>::New());
        this->setActor(vtkSmartPointer<vtkActor>::New());

        d->planeSource = vtkPlaneSource::New();
        d->planeSource->SetResolution(1, 1);

        //this->onUpdateGeometry();
#if (VTK_MAJOR_VERSION <= 5)
        this->getMapper()->SetInput(d->planeSource->GetOutput());
#else
        this->getMapper()->SetInputData(d->planeSource->GetOutput());
#endif

        this->getActor()->SetMapper(this->getMapper());

        this->AddPart(this->getActor());

        if(!d->planeObserver)
        {
//            d->axlPlanePicker = vtkCellPicker::New();
            d->planeObserver = axlActorPlaneObserver::New() ;
            this->getInteractor()->AddObserver(vtkCommand::InteractionEvent, d->planeObserver);
            //this->getInteractor()->AddObserver(vtkCommand::MouseMoveEvent, d->planeObserver);

            d->planeObserver->observerDataAssembly = this;
//            d->planeObserver->axlPlanePicker = d->axlPlanePicker;
            d->planeObserver->observerData_planeSource = d->planeSource;
            d->planeObserver->observerData_plane = d->plane;
            d->planeObserver->axlInteractorStyle = dynamic_cast<axlInteractorStyleSwitch *> (this->getInteractor()->GetInteractorStyle());
        }

        QColor color = d->plane->color();
        this->getActor()->GetProperty()->SetColor(color.redF(), color.greenF(), color.blueF());

        QString shader = d->plane->shader();
        if(!shader.isEmpty())
            this->setShader(shader);

        this->setOpacity(d->plane->opacity());

        // update the actor
        this->onUpdateGeometry();
        // signals connecting
        connect(d->plane, SIGNAL(modifiedGeometry()), this, SLOT(onUpdateGeometry()));
        connect(d->plane, SIGNAL(modifiedProperty()), this, SLOT(onUpdateProperty()));
    }
    else
        qDebug()<< "no axlPlane available";

}

void axlActorPlane::setRenderer(vtkRenderer *renderer)
{
    d->renderer = renderer;
}


void axlActorPlane::setMode(int state)
{
    this->onModeChanged(state);

    emit stateChanged(this->data(), state);
}

void axlActorPlane::onModeChanged(int state)
{
    if(state == 0)
    {
        this->setState(axlActor::passive);
        this->getActor()->SetPickable(1);

        if(axlAbstractData *data = dynamic_cast<axlAbstractData *>(this->data()))
        {
            QColor color = data->color();
            this->getActor()->GetProperty()->SetColor(color.redF(), color.greenF(), color.blueF());
        }
        this->setPlaneWidget(false);

    }
    else if(state == 1)
    {

        this->setState(axlActor::selection);
        this->setPlaneWidget(false);
        this->getActor()->SetPickable(1);
        vtkProperty *prop = this->getActor()->GetProperty();

        if(axlAbstractData *data = dynamic_cast<axlAbstractData *>(this->data()))
        {
            QColor color = data->color();
            qreal *h = new qreal(0.0);
            qreal *s = new qreal(0.0);
            qreal *l = new qreal(0.0);
            color.getHslF(h, s, l);
            color.setHslF(*h, *s, *l + (1.0 - *l) / 2.0);
            prop->SetColor(color.redF(), color.greenF(), color.blueF());
            delete l;
            delete s;
            delete h;
        }

    }
    else if(state == 2)
    {
        this->setState(axlActor::edition);
        this->setPlaneWidget(true);

        this->getActor()->SetPickable(0);
        vtkProperty *prop = this->getActor()->GetProperty();
        if(axlAbstractData *data = dynamic_cast<axlAbstractData *>(this->data()))
        {
            QColor color = data->color();
            qreal *h = new qreal(0.0);
            qreal *s = new qreal(0.0);
            qreal *l = new qreal(0.0);
            color.getHslF(h, s, l);
            color.setHslF(*h, *s, *l + (1.0 - *l)/2.0);
            prop->SetColor(color.redF(), color.greenF(), color.blueF());
            delete l;
            delete s;
            delete h;
        }
    }

    this->Modified();
}


void axlActorPlane::onRemoved()
{
    // if on edit mode, change to selection mode (for stability)
    if (this->getState() == 2)
        setMode(1);
    
    //remove plane specificity
    if(d->planeObserver)
    {
        this->getInteractor()->RemoveObservers(vtkCommand::InteractionEvent, d->planeObserver);
        this->getInteractor()->RemoveObservers(vtkCommand::MouseMoveEvent, d->planeObserver);
        d->planeObserver->observerDataAssembly = NULL;
//        d->planeObserver->axlPlanePicker = NULL;
        d->planeObserver->observerData_planeSource = NULL;
        d->planeObserver->observerData_plane = NULL;
        d->planeObserver->axlInteractorStyle = NULL;
        d->planeObserver->Delete();
        d->planeObserver = NULL;

//        d->axlPlanePicker->Delete();
//        d->axlPlanePicker = NULL;

    }
    if(d->planeSource)
    {
        d->planeSource->Delete();
        d->planeSource = NULL;
    }
    if(d->widget)
    {
        d->widget = NULL;
    }
    if(d->plane)
    {
        d->plane = NULL;
    }
    if(d->lineWidget)
    {
        this->setPlaneWidget(false);
        d->lineWidget = NULL;
    }

    //remove actor specificity
    this->RemoveAllObservers();
    this->RemovePart(this->getActor());
    this->getActor()->RemoveAllObservers();

    if(axlActorComposite *actorComposite = dynamic_cast<axlActorComposite *>(this->parent()) )
        actorComposite->removeActorReference(this);
}

void axlActorPlane::onUpdateGeometry()
{

    // vtk need to reinitialize Origin and the two point before changing them again
    d->planeSource->SetOrigin(0.0, 0.0, 0.0);
    d->planeSource->SetPoint1(d->plane->size() * 100, 0.0, 0.0);
    d->planeSource->SetPoint2(0.0, d->plane->size() * 100, 0.0);

    d->planeSource->SetCenter(d->plane->point()->coordinates());
    d->planeSource->SetNormal(d->plane->normal()->coordinates());
    d->planeSource->Update();
    d->planeSource->Modified();

    if(d->lineWidget)
    {
        d->lineWidget->SetPoint1(d->plane->point()->coordinates());
        axlPoint p2 = (*(d->plane->point())) + (*(d->plane->normal()));
        d->lineWidget->SetPoint2(p2.coordinates());
    }


    if(!d->plane->fields().isEmpty())
        d->plane->touchField();

    if(d->plane->updateView())
        emit updated();
}


void axlActorPlane::setSize(double size){

    // vtk need to reinitialize Origin and the two point before changing them again
    d->planeSource->SetOrigin(0.0, 0.0, 0.0);
    d->planeSource->SetPoint1(d->plane->size() * 100, 0.0, 0.0);
    d->planeSource->SetPoint2(0.0, d->plane->size() * 100, 0.0);

    d->planeSource->SetCenter(d->plane->point()->coordinates());
    d->planeSource->SetNormal(d->plane->normal()->coordinates());
    d->planeSource->Update();
    d->planeSource->Modified();

    if(!d->plane->fields().isEmpty())
        d->plane->touchField();
}

axlActorPlane::axlActorPlane(void) : axlActor(), d(new axlActorPlanePrivate)
{
    d->plane = NULL;
    d->lineWidget = NULL;
//    d->axlPlanePicker = NULL;
    d->planeObserver = NULL;
    d->planeSource =NULL;
    d->widget = NULL;
}

axlActorPlane::~axlActorPlane(void)
{
    delete d;

    d = NULL;
}

bool axlActorPlane::isShowPlaneWidget(void)
{

    if(!d->lineWidget) {
        qDebug() << "No tet actor computed for this axlActorBSpline.";
        return false;
    }

    return d->lineWidget->GetEnabled();
}


axlAbstractActor *createAxlActorPlane(void){

    return axlActorPlane::New();
}
