The documentation in Axl is using

- `doxygen` for the source code documentation,
- `sphinx` for the user documentation.

## Configuration

To build the documentation
```
cmake . -DDOC=ON
make html
```

The html documentation is built in the folder `${CMAKE_BINARY_DIR}/html`

The documentation in latex can be produced with

```
make latex 
```
It produces  a latex file `Axl.tex` and a pdf file `Axl-${VERSION}.pdf` in the folder  `${CMAKE_BINARY_DIR}/latex`.