/* axlInteractorStyleSwitch.h ---
 *
 * Author: Meriadeg Perrinel
 * Copyright (C) 2008 - Meriadeg Perrinel, Inria.
 * Created: Fri Feb 18 17:25:39 2011 (+0100)
 * Version: $Id$
 * Last-Updated: Fri Feb 18 17:28:34 2011 (+0100)
 *           By: Meriadeg Perrinel
 *     Update #: 18
 */

/* Commentary:
 *
 */

/* Change log:
 *
 */
#ifndef AXLINTERACTORSSTYLEWITCH
#define AXLINTERACTORSSTYLEWITCH

#include "vtkInteractorStyle.h"
#include <QtCore/QObject>

#include "axlVtkViewPluginExport.h"


#define VTKIS_JOYSTICK  0
#define VTKIS_TRACKBALL 1

#define VTKIS_CAMERA    0
#define VTKIS_ACTOR     1

class vtkInteractorStyleJoystickActor;
class vtkInteractorStyleJoystickCamera;
class vtkInteractorStyleTrackballActor;
class vtkInteractorStyleTrackballCamera;

class axlInteractorStyleSwitchPrivate;

class AXLVTKVIEWPLUGIN_EXPORT axlInteractorStyleSwitch :public QObject, public vtkInteractorStyle
{
    Q_OBJECT

public:
  static axlInteractorStyleSwitch *New();
  vtkTypeMacro(axlInteractorStyleSwitch, vtkInteractorStyle);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // The sub styles need the interactor too.
  void SetInteractor(vtkRenderWindowInteractor *iren);

  // Description:
  // We must override this method in order to pass the setting down to
  // the underlying styles
  void SetAutoAdjustCameraClippingRange( int value ) override;

  // Description:
  // Set/Get current style
  vtkGetObjectMacro(CurrentStyle, vtkInteractorStyle);
  void SetCurrentStyleToJoystickActor();
  void SetCurrentStyleToJoystickCamera();
  void SetCurrentStyleToTrackballActor();
  void SetCurrentStyleToTrackballCamera();

  // Description:
  // Only care about the char event, which is used to switch between
  // different styles.
  virtual void OnChar();

  // Description:
  // Overridden from vtkInteractorObserver because the interactor styles
  // used by this class must also be updated.
  virtual void SetDefaultRenderer(vtkRenderer*) override;
  virtual void SetCurrentRenderer(vtkRenderer*) override;

protected:
   axlInteractorStyleSwitch(QObject *parent = 0);
  ~axlInteractorStyleSwitch();

  void SetCurrentStyle();

  vtkInteractorStyleJoystickActor *JoystickActor;
  vtkInteractorStyleJoystickCamera *JoystickCamera;
  vtkInteractorStyleTrackballActor *TrackballActor;
  vtkInteractorStyleTrackballCamera *TrackballCamera;
  vtkInteractorStyle* CurrentStyle;

  int JoystickOrTrackball;
  int CameraOrActor;

private:
  axlInteractorStyleSwitch(const axlInteractorStyleSwitch&);  // Not implemented.
  void operator=(const axlInteractorStyleSwitch&);  // Not implemented.

private:
    axlInteractorStyleSwitchPrivate *d;
};

#endif //AXLINTERACTORSSTYLEWITCH
