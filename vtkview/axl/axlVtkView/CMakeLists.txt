### CMakeLists.txt ---
##
## Author: Julien Wintz
## Copyright (C) 2008 - Julien Wintz, Inria.
## Created: Fri Feb 18 17:14:13 2011 (+0100)
## Version: $Id$
## Last-Updated: Mon Dec 17 18:46:59 2012 (+0100)
##           By: Julien Wintz
##     Update #: 34
######################################################################
##
### Commentary:
##
######################################################################
##
### Change log:
##
######################################################################

set(LIB_NAME axlVtkView)

set (CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/plugins)

if (APPLE)
 set (CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin/${AXL_APP}.app/Contents/PlugIns)
endif (APPLE)

if (WIN32)
    set (CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)
    set (CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/plugins)

    set (ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)
    set (RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/plugins)
endif (WIN32)

set(${PROJECT_NAME}_HEADERS
  axlActor.h
  axlActorBSpline.h
  axlActorCone.h
  axlActorCylinder.h
  axlActorTorus.h
  axlActorComposite.h
  axlActorCurveBSpline.h
  axlActorEllipsoid.h
  axlActorLine.h
  axlActorFieldDiscrete.h
  axlActorMesh.h
  axlActorPlane.h
  axlActorPoint.h
  #axlActorPointSet.h
  axlActorShapeBSpline.h
  axlActorSphere.h
  axlActorSurfaceBSpline.h
  axlActorSurfaceTrimmed.h
  axlControlPointsWidget.h
  #axlFieldDiscrete.h
  #axlFieldDiscreteWriter.h
  #axlFieldDiscreteReader.h
  axlInteractorStyleRubberBandPick.h
  axlInteractorStyleSwitch.h
  axlLightsWidget.h
  axlVtkView.h
  axlVtkViewPlugin.h
  axlVtkViewPluginExport.h
  axlActorVolumeBSpline.h
  axlActorDataDynamic.h
  axlInspectorActorFactory.h
  axlActorFieldSpatial.h
  axlActorFieldParametric.h
  axlVolumeDiscrete.h
  axlVolumeDiscreteWriter.h
  axlVolumeDiscreteReader.h
  axlVolumeDiscreteEditor.h
  axlVolumeDiscreteConverter.h
  axlVolumeDiscreteProcessCreator.h
  axlActorVolumeDiscrete.h
  axlInspectorObjectVolumeDiscrete.h
  axlInteractorCellPicking.h)

set(${PROJECT_NAME}_HEADERS_MOC
  axlControlPointsWidget.h
  #axlFieldDiscrete.h
  #axlFieldDiscreteWriter.h
  #axlFieldDiscreteReader.h
  axlInteractorStyleRubberBandPick.h
  axlInteractorStyleSwitch.h
  axlLightsWidget.h
  axlInspectorActorFactory.h
  axlVolumeDiscrete.h
  axlVolumeDiscreteWriter.h
  axlVolumeDiscreteReader.h
  axlVolumeDiscreteEditor.h
  axlVolumeDiscreteConverter.h
  axlVolumeDiscreteProcessCreator.h
  axlInspectorObjectVolumeDiscrete.h
  axlVtkView.h
  axlVtkViewPlugin.h
  axlInteractorCellPicking.h)

set(${PROJECT_NAME}_SOURCES
  axlActor.cpp
  axlActorBSpline.cpp
  axlActorCone.cpp
  axlActorCylinder.cpp
  axlActorTorus.cpp
  axlActorComposite.cpp
  axlActorCurveBSpline.cpp
  axlActorEllipsoid.cpp
  axlActorFieldDiscrete.cpp
  axlActorLine.cpp
  axlActorMesh.cpp
  axlActorPlane.cpp
  axlActorPoint.cpp
  #axlActorPointSet.cpp
  axlActorShapeBSpline.cpp
  axlActorSphere.cpp
  axlActorSurfaceBSpline.cpp
  axlActorSurfaceTrimmed.cpp
  axlControlPointsWidget.cpp
  #axlFieldDiscrete.cpp
  #axlFieldDiscreteWriter.cpp
  #axlFieldDiscreteReader.cpp
  axlInteractorStyleRubberBandPick.cpp
  axlInteractorStyleSwitch.cpp
  axlLightsWidget.cpp
  axlVtkView.cpp
  axlVtkViewPlugin.cpp
  axlActorVolumeBSpline.cpp
  axlActorDataDynamic.cpp
  axlInspectorActorFactory.cpp
  axlActorFieldSpatial.cpp
  axlActorFieldParametric.cpp
  axlVolumeDiscrete.cpp
  axlVolumeDiscreteWriter.cpp
  axlVolumeDiscreteReader.cpp
  axlVolumeDiscreteEditor.cpp
  axlVolumeDiscreteConverter.cpp
  axlVolumeDiscreteProcessCreator.cpp
  axlActorVolumeDiscrete.cpp
  axlInspectorObjectVolumeDiscrete.cpp
  axlInteractorCellPicking.cpp)

add_library(${LIB_NAME} SHARED
#  ${${PROJECT_NAME}_SOURCES_MOC}
  ${${PROJECT_NAME}_SOURCES})

if(${VTK_MAJOR_VERSION} LESS 6)
  target_link_libraries(${LIB_NAME}
    dtkLog
    dtkCoreSupport
    axlCore
    vtkCommon
    vtkGraphics
    vtkFiltering
    vtkRendering
    QVTK)

else(${VTK_MAJOR_VERSION} LESS 6)
 target_link_libraries(${LIB_NAME}
    dtkLog
    dtkCoreSupport
    axlCore
    ${VTK_LIBRARIES})
endif(${VTK_MAJOR_VERSION} LESS 6)

qt5_use_modules(${LIB_NAME} Core)
qt5_use_modules(${LIB_NAME} Gui)
qt5_use_modules(${LIB_NAME} OpenGL)
qt5_use_modules(${LIB_NAME} Widgets)

## #################################################################
## Target properties
## #################################################################

set_target_properties(${LIB_NAME} PROPERTIES MACOSX_RPATH 0)
#set_target_properties(${LIB_NAME} PROPERTIES INSTALL_NAME_DIR "${CMAKE_INSTALL_PREFIX}/${CMAKE_INSTALL_LIBDIR}")
#set_target_properties(${LIB_NAME} PROPERTIES INSTALL_RPATH    "${CMAKE_INSTALL_PREFIX}/${CMAKE_INSTALL_LIBDIR}")

if(APPLE)
install(TARGETS ${LIB_NAME}
  RUNTIME DESTINATION bin/${AXL_APP}.app/Contents/PlugIns
  LIBRARY DESTINATION bin/${AXL_APP}.app/Contents/PlugIns
  ARCHIVE DESTINATION bin/${AXL_APP}.app/Contents/PlugIns)
elseif(WIN32)
install(TARGETS ${LIB_NAME}
  RUNTIME DESTINATION plugins
  LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
  ARCHIVE DESTINATION ${CMAKE_INSTALL_LIBDIR})
else(APPLE)
install(TARGETS ${LIB_NAME}
  RUNTIME DESTINATION plugins
  LIBRARY DESTINATION plugins
  ARCHIVE DESTINATION plugins)
endif(APPLE)
