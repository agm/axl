/* axlVolumeDiscrete.cpp ---
 *
 * Author: Anais Ducoffe
 * Copyright (C) 2008 - Anais Ducoffe, Inria.
 */

/* Commentary:
 *
 */

/* Change log:
 *
 */

#include "axlVolumeDiscrete.h"

#include <dtkCoreSupport/dtkGlobal.h>
#include <dtkCoreSupport/dtkAbstractData_p.h>

#include <vtkSmartPointer.h>
#include <vtkImageData.h>

// ///////////////////////////////////////////////////////////////////
// axlVolumeDiscretePrivate
// ///////////////////////////////////////////////////////////////////

class axlVolumeDiscretePrivate
{
public:
    vtkImageData *grid;

public:
    double *values;
    double min_field_value;
    double mid_field_value;
    double max_field_value;
};

// ///////////////////////////////////////////////////////////////////
// axlVolumeDiscrete protected constructors
// ///////////////////////////////////////////////////////////////////

//DTK_IMPLEMENT_PRIVATE(axlVolumeDiscrete, dtkAbstractData);

// ///////////////////////////////////////////////////////////////////
// axlVolumeDiscrete implementation
// ///////////////////////////////////////////////////////////////////

axlVolumeDiscrete::axlVolumeDiscrete(void) : axlAbstractVolumeDiscrete(), d(new axlVolumeDiscretePrivate)
{
    d->values = NULL;
    d->grid = vtkImageData::New();
    //d->grid->SetScalarType(VTK_DOUBLE);
    d->min_field_value = d->grid->GetScalarTypeMax();
    d->max_field_value = d->grid->GetScalarTypeMin();
    d->mid_field_value = (maxValue() + minValue())/2.0;
    this->setObjectName("axlVolumeDiscrete");

}

axlVolumeDiscrete::axlVolumeDiscrete(const axlVolumeDiscrete& other) : axlAbstractVolumeDiscrete(), d(new axlVolumeDiscretePrivate)
{
    d->values = new double[other.xDimension() *other.yDimension() *other.zDimension()];

    vtkImageData *grid = static_cast<vtkImageData *>(other.data());
    if(grid){
        d->grid->DeepCopy(grid);
    }else{
        d->grid = vtkSmartPointer<vtkImageData>::New();
        //d->grid->SetScalarType(VTK_DOUBLE);
    }

    d->min_field_value = other.minValue();
    d->max_field_value = other.maxValue();
    d->mid_field_value = other.midValue();
    this->setObjectName("axlVolumeDiscrete");
}

axlVolumeDiscrete::~axlVolumeDiscrete(void)
{
    d->grid->Delete();
}

axlVolumeDiscrete *axlVolumeDiscrete::clone(void) const
{
    return new axlVolumeDiscrete(*this);
}

axlVolumeDiscrete& axlVolumeDiscrete::operator = (const axlVolumeDiscrete& other)
{
    vtkImageData *grid = static_cast<vtkImageData *>(other.data());
    if(!grid){
        d->grid = vtkSmartPointer<vtkImageData>::New();
        //d->grid->SetScalarType(VTK_DOUBLE);
    }

    d->grid->DeepCopy(grid);

    return (*this);
}

QString axlVolumeDiscrete::identifier(void) const
{
    return "axlVolumeDiscrete";
}

void axlVolumeDiscrete::setData(void *data)
{
    if(vtkImageData *grid = static_cast<vtkImageData *>(data)){
        if(grid){
            d->grid->DeepCopy(grid);
        }else{
            d->grid = vtkSmartPointer<vtkImageData>::New();
            //d->grid->SetScalarType(VTK_DOUBLE);
        }
    }

}

void *axlVolumeDiscrete::data(void) const
{

    return d->grid;

}

int axlVolumeDiscrete::xDimension(void) const
{
    //DTK_D(const axlVolumeDiscrete);

    if(!d->grid)
        return -1;

    return d->grid->GetDimensions()[0];
}

int axlVolumeDiscrete::yDimension(void) const
{
    //DTK_D(const axlVolumeDiscrete);

    if(!d->grid)
        return -1;

    return d->grid->GetDimensions()[1];
}

int axlVolumeDiscrete::zDimension(void) const
{
    //DTK_D(const axlVolumeDiscrete);

    if(!d->grid)
        return -1;

    return d->grid->GetDimensions()[2];
}

void axlVolumeDiscrete::setDimensions(unsigned int x, unsigned int y,unsigned int z){
    if(d->grid){
        d->grid->Initialize();
        d->grid->SetExtent(0,x-1,0,y-1,0,z-1);
        d->grid->SetOrigin(-0.5*x, -0.5*y, -0.5*z);
        //d->grid->SetNumberOfScalarComponents(1);
    }
}

double axlVolumeDiscrete::minValue(void) const
{

    //    double min = 0.0;
    //    for(int i = 0; i < xDimension() ;i++){
    //        for(int j = 0; j < yDimension() ;j++){
    //            for(int k = 0; k < zDimension() ;k++){
    //                if(i== 0 & j== 0 & k ==0){
    //                    min = getValue(i,j,k);
    //                }else{
    //                    if(min > getValue(i,j,k)){
    //                        min = getValue(i,j,k);
    //                    }
    //                }
    //            }
    //        }
    //    }
    //    qDebug() << Q_FUNC_INFO << min;
    //    return min;
    //    double scalar_range[2];d->grid->GetScalarRange(scalar_range);
    //    qDebug() << Q_FUNC_INFO << scalar_range[0];
    //    return scalar_range[0];
    return d->min_field_value;
}

double axlVolumeDiscrete::midValue(void) const
{
    //return (maxValue() + minValue())/2.0;

    return d->mid_field_value;
}

double axlVolumeDiscrete::maxValue(void) const
{
    //    double max = 0.0;
    //    for(int i = 0; i < xDimension() ;i++){
    //        for(int j = 0; j < yDimension() ;j++){
    //            for(int k = 0; k < zDimension() ;k++){
    //                if(max < getValue(i,j,k))
    //                    max = getValue(i,j,k);
    //            }
    //        }
    //    }
    //    qDebug() << Q_FUNC_INFO << max;
    //    return max;
    //    double scalar_range[2];d->grid->GetScalarRange(scalar_range);
    //    qDebug() << Q_FUNC_INFO << scalar_range[1];
    //    return scalar_range[1];
    return d->max_field_value;
}

void axlVolumeDiscrete::setMinValue(double min_field_value)
{
    d->min_field_value = min_field_value;
}

void axlVolumeDiscrete::setMidValue(double mid_field_value)
{
    //DTK_D(axlVolumeDiscrete);

    d->mid_field_value = mid_field_value;
}

void axlVolumeDiscrete::setMaxValue(double max_field_value)
{
    //DTK_D(axlVolumeDiscrete);
    d->max_field_value = max_field_value;
}


double axlVolumeDiscrete::getValue(int i, int j, int k) const{
    // int nx = xDimension();
    // int ny = yDimension();
    // int nz = zDimension();
    // int indice = k* nx *ny + j * nx + i;
    return d->grid->GetScalarComponentAsDouble(i,j,k,0);
}


void axlVolumeDiscrete::setValue(double value, int i, int j, int k){
    // int nx = xDimension();
    // int ny = yDimension();
    // int nz = zDimension();
    // int indice = k* nx *ny + j * nx + i;
    d->grid->SetScalarComponentFromDouble(i,j,k,0, value);
    //d->grid->Update();

    if(value > maxValue()){
        d->max_field_value = value;
    }
    if(value < minValue()){
        d->min_field_value = value;
    }

    d->mid_field_value = (maxValue() + minValue())/2.0;

}

// /////////////////////////////////////////////////////////////////
// Instanciation function
// /////////////////////////////////////////////////////////////////

dtkAbstractData *createaxlVolumeDiscrete(void)
{
    return new axlVolumeDiscrete;
};
