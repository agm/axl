/* axlInspectorActorFactory.cpp ---
 *
 * Author: Anais Ducoffe
 * Copyright (C) 2013 - Anais Ducoffe, Inria.
 */


/* Commentary:
 *
 */

/* Change log:
 *
 */

#include "axlInspectorActorFactory.h"

//all actor types
#include "axlActorPoint.h"
#include "axlActorCone.h"
#include "axlActorCylinder.h"
#include "axlActorEllipsoid.h"
#include "axlActorLine.h"
#include "axlActorPlane.h"
#include "axlActorSphere.h"
#include "axlActorTorus.h"
#include "axlActorDataDynamic.h"
#include "axlActorCurveBSpline.h"
#include "axlActorSurfaceBSpline.h"
#include "axlActorVolumeBSpline.h"
#include "axlActorShapeBSpline.h"
#include "axlActorComposite.h"
#include "axlActorLine.h"
#include "axlActorPlane.h"
#include "axlActorSphere.h"
#include "axlActorTorus.h"
#include "axlActorDataDynamic.h"
#include "axlActorCurveBSpline.h"
#include "axlActorSurfaceBSpline.h"
#include "axlActorVolumeBSpline.h"
#include "axlActorComposite.h"
#include "axlActorMesh.h"

// /////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////

axlInspectorActorInterface::axlInspectorActorInterface(QWidget *parent) : QFrame(parent)
{
    m_actorFactorySingleton = axlInspectorActorFactory::instance();
}

axlInspectorActorInterface::~axlInspectorActorInterface(void)
{

}

void axlInspectorActorInterface::setActorFactorySingleton(axlInspectorActorFactory *actorFactorySingleton)
{
    m_actorFactorySingleton = actorFactorySingleton;
}

axlInspectorActorFactory *axlInspectorActorInterface::actorFactorySingleton(void)
{
    return m_actorFactorySingleton;
}

void axlInspectorActorInterface::setData(axlAbstractActor *data)
{
    qDebug()<<"axlInspectorActorInterface::setData : Default implementation";
}

// /////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////

class axlInspectorActorFactoryPrivate
{
public:
    axlInspectorActorFactory::axlInspectorActorCreatorHash creators;
};

axlInspectorActorFactory *axlInspectorActorFactory::instance(void)
{
    if(!s_instance)
        s_instance = new axlInspectorActorFactory;

    return s_instance;
}

bool axlInspectorActorFactory::registerInspectorActor(const QString& interface_name, axlInspectorActorCreator func)
{
    if(!d->creators.contains(interface_name))
        d->creators.insert(interface_name, func);

    return true;
}


void axlInspectorActorFactory::initialize(void)
{

    d->creators.insert("axlPoint", createAxlActorPoint);
    d->creators.insert("axlCone",createAxlActorCone );
    d->creators.insert("axlCylinder", createAxlActorCylinder);
    d->creators.insert("axlTorus", createAxlActorTorus);
    d->creators.insert("axlEllipsoid", createAxlActorEllipsoid);
    d->creators.insert("axlLine", createAxlActorLine);
    d->creators.insert("axlPlane", createAxlActorPlane);
    d->creators.insert("axlSphere", createAxlActorSphere);
    d->creators.insert("goCurveBSpline", createAxlActorCurveBSpline);
    d->creators.insert("goSurfaceBSpline", createAxlActorSurfaceBSpline);
    d->creators.insert("goVolumeBSpline", createAxlActorVolumeBSpline);
    d->creators.insert("Composite Data", createAxlActorComposite);
    d->creators.insert("axlDataDynamic",createAxlActorDataDynamic);
    d->creators.insert("axlMesh", createAxlActorMesh);
    d->creators.insert("axlShapeBSpline", createAxlActorShapeBSpline);

}

axlAbstractActor *axlInspectorActorFactory::create(const QString& interface_name)
{
    if(!d->creators.contains(interface_name))
        return d->creators["axlMesh"]();

    return d->creators[interface_name]();

}

axlInspectorActorFactory::axlInspectorActorFactory(void) : QObject(), d(new axlInspectorActorFactoryPrivate)
{
    initialize();

}

axlInspectorActorFactory::~axlInspectorActorFactory(void)
{
    delete d;

    d = NULL;
}

axlInspectorActorFactory *axlInspectorActorFactory::s_instance = NULL;
