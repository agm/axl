/* axlActorFieldDiscrete.cpp ---
 *
 * Author: Thibaud Kloczko
 * Copyright (C) 2008 - Thibaud Kloczko, Inria.
 * Created: Mon Dec  6 20:20:58 2010 (+0100)
 * Version: $Id$
 * Last-Updated: Mon Dec 17 15:01:58 2012 (+0100)
 *           By: Julien Wintz
 *     Update #: 715
 */

/* Commentary:
 *
 */

/* Change log:
 *
 */

#include <axlCore/axlFieldDiscrete.h>
#include "axlActorFieldDiscrete.h"
#include "axlActor.h"

#include <axlCore/axlAbstractActorField.h>
#include <axlCore/axlAbstractField.h>

#include <dtkCoreSupport/dtkGlobal.h>

#include <vtkActor.h>
#include <vtkArrowSource.h>
#include <vtkCommand.h>
#include <vtkContourFilter.h>
#include <vtkDataSetMapper.h>
#include <vtkCellData.h>
#include <vtkGlyph3D.h>
#include <vtkHedgeHog.h>
#include <vtkObjectFactory.h>
#include <vtkPointData.h>
#include <vtkPolyDataMapper.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkSmartPointer.h>
#include <vtkSphere.h>
#include <vtkSphereRepresentation.h>
#include <vtkSphereSource.h>
#include <vtkSphereWidget2.h>
#include <vtkStreamTracer.h>
#include <vtkTubeFilter.h>
#include <vtkPolyData.h>
#include <vtkUnstructuredGrid.h>

#include <vtkExtractUnstructuredGrid.h>

#include "axlActorDataDynamic.h"
#include <axlCore/axlAbstractFieldDiscrete.h>

#include <vtkIntArray.h>
#include <vtkFloatArray.h>
#include <vtkDoubleArray.h>
#include <vtkDataArray.h>

// /////////////////////////////////////////////////////////////////
// axlActorFieldDiscreteStreamObserver
// /////////////////////////////////////////////////////////////////

class axlActorFieldDiscreteStreamObserver : public vtkCommand
{
public:
    static axlActorFieldDiscreteStreamObserver *New(void)
    {
        return new axlActorFieldDiscreteStreamObserver;
    }

    virtual void Execute(vtkObject *caller, unsigned long event, void *)
    {
        //        if(event == vtkCommand::InteractionEvent) {

        //            vector_stream_widget_representation->GetPolyData(vector_stream_widget_data);
        //#if (VTK_MAJOR_VERSION <= 5)
        //            vector_stream_tracer->SetSource(vector_stream_widget_data);
        //#else
        //            vector_stream_tracer->SetSourceData(vector_stream_widget_data);
        //#endif
        //            vector_stream_tracer->Update();
        //        }
    }

    vtkSmartPointer<vtkSphereRepresentation> vector_stream_widget_representation;
    vtkSmartPointer<vtkPolyData> vector_stream_widget_data;
    vtkSmartPointer<vtkStreamTracer> vector_stream_tracer;
};

// /////////////////////////////////////////////////////////////////
// axlActorFieldDiscretePrivate
// /////////////////////////////////////////////////////////////////

class axlActorFieldDiscretePrivate
{
public:
    axlAbstractFieldDiscrete *field;
    axlActor *mesh;
    vtkDataArray *array;

public:
    bool scalar_display_as_iso;
    int scalar_iso_count;
    double scalar_iso_range[2];
    vtkSmartPointer<vtkTubeFilter> scalar_iso_tube_filter;
    double isoRadius;
    vtkSmartPointer<vtkContourFilter> scalar_iso;
    vtkSmartPointer<vtkPolyDataMapper> scalar_iso_mapper;
    vtkSmartPointer<vtkActor> scalar_iso_actor;

    vtkSmartPointer<vtkPolyDataMapper> scalar_iso_color_mapper;
    vtkSmartPointer<vtkActor> scalar_iso_color_actor;


    vtkSmartPointer<vtkScalarBarActor> scalar_bar_actor;

    double scalar_color_range[2];
    vtkSmartPointer<vtkDataSetMapper> scalar_color_mapper;
    vtkSmartPointer<vtkActor> scalar_color_actor;

    vtkSmartPointer<vtkHedgeHog> vector_hedgehog;
    vtkSmartPointer<vtkPolyDataMapper> vector_hedgehog_mapper;
    vtkSmartPointer<vtkActor> vector_hedgehog_actor;

    vtkSmartPointer<vtkArrowSource> vector_glyph_source;
    vtkSmartPointer<vtkGlyph3D> vector_glyph;
    vtkSmartPointer<vtkPolyDataMapper> vector_glyph_mapper;
    vtkSmartPointer<vtkActor> vector_glyph_actor;

    vtkSmartPointer<vtkSphereWidget2> vector_stream_widget;
    vtkSmartPointer<vtkSphereRepresentation> vector_stream_widget_representation;
    vtkSmartPointer<vtkPolyData> vector_stream_widget_data;
    vtkSmartPointer<vtkStreamTracer> vector_stream_tracer;
    vtkSmartPointer<vtkTubeFilter> vector_stream_filter;
    vtkSmartPointer<vtkPolyDataMapper> vector_stream_mapper;
    vtkSmartPointer<vtkActor> vector_stream_actor;
    vtkSmartPointer<axlActorFieldDiscreteStreamObserver> vector_stream_observer;

    vtkSmartPointer<vtkRenderWindowInteractor> interactor;
};

// /////////////////////////////////////////////////////////////////
// axlActorFieldDiscrete
// /////////////////////////////////////////////////////////////////

#if (VTK_MAJOR_VERSION <= 5)
vtkCxxRevisionMacro(axlActorFieldDiscrete, "$Revision: 0.0.1 $");
#endif

vtkStandardNewMacro(axlActorFieldDiscrete);

dtkAbstractData *axlActorFieldDiscrete::data(void)
{
    return NULL;
}

axlAbstractField *axlActorFieldDiscrete::field(void)
{
    if(d->field){
        return d->field;
    }
    return NULL;
}

axlAbstractField *axlActorFieldDiscrete::magnitude(void)
{
    if(d->mesh->fields().contains(QString("%1 magnitude").arg(d->field->objectName())))
        return NULL;

    QString field_magnitude_name = QString("%1 magnitude").arg(d->field->objectName());

    axlFieldDiscrete *field_magnitude = new axlFieldDiscrete(field_magnitude_name, d->field->type(), axlFieldDiscrete::Scalar, d->field->support(), d->field->size());

    double *tuple; for(int i = 0; i < d->field->size(); i++) {
        tuple = d->field->vector(i);
        field_magnitude->setScalar(i, qSqrt(tuple[0]*tuple[0]+tuple[0]*tuple[0]+tuple[0]*tuple[0]));
    }
    return field_magnitude;
}



axlAbstractActor *axlActorFieldDiscrete::actorField(void)
{
    return d->mesh;
}


vtkScalarBarActor *axlActorFieldDiscrete::scalarBar(void){
    return d->scalar_bar_actor;
}

double axlActorFieldDiscrete::colRangeMin(void)
{
    return d->scalar_color_range[0];
}

double axlActorFieldDiscrete::colRangeMax(void)
{
    return d->scalar_color_range[1];
}

int axlActorFieldDiscrete::isoCount(void)
{
    return d->scalar_iso->GetNumberOfContours();
}

double axlActorFieldDiscrete::isoRangeMin(void)
{
    return d->scalar_iso_range[0];
}

double axlActorFieldDiscrete::isoRangeMax(void)
{
    return d->scalar_iso_range[1];
}

double axlActorFieldDiscrete::glyphScale(void)
{
    return d->vector_glyph->GetScaleFactor();
}

double axlActorFieldDiscrete::streamRadius(void)
{
    return d->vector_stream_filter->GetRadius();
}

void axlActorFieldDiscrete::setInteractor(void *interactor)
{
    d->interactor = static_cast<vtkRenderWindowInteractor *>(interactor);

    this->setup();
}



void axlActorFieldDiscrete::updateArray(void){


    if(d->field){
        int size = 0;
        if(d->mesh->getUnstructuredGrid()){
            size = d->mesh->getUnstructuredGrid()->GetNumberOfPoints();
        }else if(d->mesh->getPolyData()){
            size = d->mesh->getPolyData()->GetNumberOfPoints();
        }else{
            if(dynamic_cast<axlActorDataDynamic *>(d->mesh)){
                axlActorDataDynamic *actorDD = dynamic_cast<axlActorDataDynamic *>(d->mesh);
                size = dynamic_cast<axlActor *>(actorDD->outputActor())->getMapper()->GetInput()->GetNumberOfPoints();
                d->mesh->setPolyData(dynamic_cast<axlActor *>(actorDD->outputActor())->getMapper()->GetInput());
                //d->mesh->setPoints(d->mesh->getPolyData()->GetPoints());
            }else{
                size = d->mesh->getMapper()->GetInput()->GetNumberOfPoints();
                d->mesh->setPolyData(d->mesh->getMapper()->GetInput());
                //d->mesh->setPoints(d->mesh->getPolyData()->GetPoints());
            }

        }

        d->array->SetNumberOfTuples(size);


        double *tuple1 = NULL;
        double tuple2 = 0;
        for(int i = 0; i < size; i++){
            if(d->field->kind() == axlAbstractFieldDiscrete::Scalar){

                tuple2 = d->field->scalar(i);

                d->array->SetTuple1(i, tuple2);
                d->array->Modified();
            }
            else if(d->field->kind() == axlAbstractFieldDiscrete::Vector){


                tuple1 = d->field->vector(i);

                d->array->SetTuple(i, tuple1);
                d->array->Modified();
            }
            else if(d->field->kind() == axlAbstractFieldDiscrete::Tensor){

                tuple1 = d->field->tensor(i);

                d->array->SetTuple(i, tuple1);
                d->array->Modified();
            }

        }
    }


    //sets the minimum and maximum values of the field.
    d->field->setMin(this->minValue());
    d->field->setMax(this->maxValue());

    //setRange
    if(d->scalar_color_range && d->scalar_color_mapper){
        this->setColRangeMin(d->field->minValue());
        this->setColRangeMax(d->field->maxValue());
    }

}

void axlActorFieldDiscrete::setData(dtkAbstractData *field)
{
    d->field = dynamic_cast<axlAbstractFieldDiscrete *>(field);

    if(d->field){


        axlAbstractData * parentData = dynamic_cast<axlAbstractData *>(d->field->parent());
        if(parentData){
            connect(parentData, SIGNAL(modifiedGeometry()), this, SLOT(update()));
        }
        axlAbstractData *axlData = dynamic_cast<axlAbstractData *>(d->mesh->data());
        connect(axlData, SIGNAL(modifiedGeometry()), this, SLOT(update()));

        if(d->field->type() == axlAbstractFieldDiscrete::Int)
            d->array = vtkIntArray::New();
        else if(d->field->type() == axlAbstractFieldDiscrete::Float)
            d->array = vtkFloatArray::New();
        else if(d->field->type() == axlAbstractFieldDiscrete::Double)
            d->array = vtkDoubleArray::New();

        axlAbstractFieldDiscrete::Kind kind = d->field->kind();

        switch(kind) {
        case axlAbstractFieldDiscrete::Scalar:
            d->array->SetNumberOfComponents(1);
            break;
        case axlAbstractFieldDiscrete::Vector:
            d->array->SetNumberOfComponents(3);
            break;
        case axlAbstractFieldDiscrete::Tensor:
            d->array->SetNumberOfComponents(9);
            break;
        default:
            qDebug() << "Unsupported field kind";
        };

        QString name = d->field->objectName();
        d->array->SetName(qPrintable(name));



        this->updateArray();
        this->setup();
    }

}

void axlActorFieldDiscrete::setActorField(axlAbstractActor *actorfield)
{
    d->mesh = dynamic_cast<axlActor *>(actorfield);

    this->setup();
}

void axlActorFieldDiscrete::setColRangeMin(double min)
{
    d->scalar_color_range[0] = min;
    d->scalar_color_mapper->SetScalarRange(d->scalar_color_range);
    d->scalar_color_mapper->Update();
}

void axlActorFieldDiscrete::setColRangeMax(double max)
{
    d->scalar_color_range[1] = max;
    d->scalar_color_mapper->SetScalarRange(d->scalar_color_range);
    d->scalar_color_mapper->Update();
}

void axlActorFieldDiscrete::setIsoCount(int count)
{
    d->scalar_iso_count = count;
    d->scalar_iso->GenerateValues(d->scalar_iso_count, d->scalar_iso_range[0], d->scalar_iso_range[1]);
    d->scalar_iso->Update();

    d->scalar_iso_tube_filter->Update();
}

void axlActorFieldDiscrete::setIsoRangeMin(double min)
{
    d->scalar_iso_range[0] = min;
    d->scalar_iso->GenerateValues(d->scalar_iso_count, d->scalar_iso_range[0], d->scalar_iso_range[1]);
    d->scalar_iso->Update();
    d->scalar_iso_color_mapper->SetScalarRange(d->scalar_iso_range);
    d->scalar_iso_color_mapper->Update();

    d->scalar_iso_tube_filter->Update();
}

void axlActorFieldDiscrete::setIsoRangeMax(double max)
{
    d->scalar_iso_range[1] = max;
    d->scalar_iso->GenerateValues(d->scalar_iso_count, d->scalar_iso_range[0], d->scalar_iso_range[1]);
    d->scalar_iso->Update();
    d->scalar_iso_color_mapper->SetScalarRange(d->scalar_iso_range);
    d->scalar_iso_color_mapper->Update();

    d->scalar_iso_tube_filter->Update();
}

void axlActorFieldDiscrete::setGlyphScale(double scale)
{
    //compute size of controlPoints
    double bounds[6]; d->mesh->GetBounds(bounds);
    double side = qAbs(bounds[1]-bounds[0]);
    side += qAbs(bounds[3]-bounds[2]);
    side += qAbs(bounds[5]-bounds[4]);

    side /= 30;
    d->vector_glyph->SetScaleFactor(scale * side);
    d->vector_hedgehog->SetScaleFactor(scale * side * 0.1);
}

void axlActorFieldDiscrete::setStreamPropagation(double propagation)
{
    d->vector_stream_tracer->SetMaximumPropagation(propagation);
    d->vector_stream_tracer->Update();
}

void axlActorFieldDiscrete::setStreamRadius(double radius)
{
    d->vector_stream_filter->SetRadius(radius);
    d->vector_stream_filter->Update();
}

void axlActorFieldDiscrete::setStreamDirection(int direction)
{
    switch (direction) {
    case 0:
        d->vector_stream_tracer->SetIntegrationDirectionToForward();
        break;
    case 1:
        d->vector_stream_tracer->SetIntegrationDirectionToBackward();
        break;
    case 2:
        d->vector_stream_tracer->SetIntegrationDirectionToBoth();
        break;
    default:
        break;
    }

    d->vector_stream_tracer->Update();
}

void axlActorFieldDiscrete::displayAsColor(void)
{
    if(d->mesh->getUnstructuredGrid()){
        d->mesh->getDataSetMapper()->ScalarVisibilityOn();
    }else{
        d->mesh->getMapper()->ScalarVisibilityOn();
    }

    if (d->field->kind() != axlFieldDiscrete::Scalar)
        return;

    if(d->field->support() == axlFieldDiscrete::Point) {
        if(d->mesh->getUnstructuredGrid()){
            static_cast<vtkUnstructuredGrid *>(d->mesh->getUnstructuredGrid())->GetPointData()->SetActiveScalars(qPrintable(d->field->objectName()));
        }else{
            static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetPointData()->SetActiveScalars(qPrintable(d->field->objectName()));
        }
    }
    if(d->field->support() == axlFieldDiscrete::Cell)
        static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetCellData()->SetActiveScalars(qPrintable(d->field->objectName()));


    d->scalar_color_actor->SetVisibility(1);
    d->scalar_iso_actor->SetVisibility(0);
    d->scalar_iso_color_actor->SetVisibility(0);
    d->scalar_display_as_iso = false;
    d->scalar_bar_actor->SetVisibility(1);

}

void axlActorFieldDiscrete::displayAsNoneScalar(void)
{
    if(d->scalar_color_actor)
    {
        d->scalar_color_actor->SetVisibility(0);
        d->scalar_iso_actor->SetVisibility(0);
        d->scalar_iso_color_actor->SetVisibility(0);
        d->scalar_display_as_iso = false;
        d->scalar_bar_actor->SetVisibility(0);
    }

    if(d->mesh->getUnstructuredGrid()){
        d->mesh->getDataSetMapper()->ScalarVisibilityOn();
    }
    else{
        d->mesh->getMapper()->ScalarVisibilityOn();
    }

    if(d->field->support() == axlFieldDiscrete::Point) {
        if(d->mesh->getUnstructuredGrid()){
            static_cast<vtkUnstructuredGrid *>(d->mesh->getUnstructuredGrid())->GetPointData()->SetActiveScalars("mapperCollorArrayDefaultField");
        }else{
            static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetPointData()->SetActiveScalars("mapperCollorArrayDefaultField");
        }
    }
}

void axlActorFieldDiscrete::displayAsIso(void)
{
    if (d->field->kind() != axlFieldDiscrete::Scalar)
        return;

    if(d->field->support() == axlFieldDiscrete::Point) {
        if(d->mesh->getUnstructuredGrid()){
            static_cast<vtkUnstructuredGrid *>(d->mesh->getUnstructuredGrid())->GetPointData()->SetActiveScalars(qPrintable(d->field->objectName()));
        }
        else{
            static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetPointData()->SetActiveScalars(qPrintable(d->field->objectName()));
        }
    }
    if(d->field->support() == axlFieldDiscrete::Cell)
        static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetCellData()->SetActiveScalars(qPrintable(d->field->objectName()));

    if (d->field->support() == axlFieldDiscrete::Point) {
        if(d->mesh->getUnstructuredGrid()){
            d->mesh->getDataSetMapper()->ScalarVisibilityOff();
        }
        else{
            d->mesh->getMapper()->ScalarVisibilityOff();
        }
    }

    this->setIsoCount(d->scalar_iso_count);

    d->scalar_color_actor->SetVisibility(0);
    d->scalar_iso_actor->SetVisibility(1);
    d->scalar_iso_color_actor->SetVisibility(0);
    d->scalar_display_as_iso = true;
    d->scalar_bar_actor->SetVisibility(0);
}

void axlActorFieldDiscrete::displayAsNoneVector(void)
{
    if (d->field->kind() == axlFieldDiscrete::Vector ) {

        d->vector_hedgehog_actor->SetVisibility(0);
        d->vector_glyph_actor->SetVisibility(0);
        d->vector_stream_actor->SetVisibility(0);
        d->vector_stream_widget->Off();
    }
}

void axlActorFieldDiscrete::displayAsHedge(void)
{
    if (d->field->kind() == axlFieldDiscrete::Vector ) {

        d->vector_hedgehog_actor->SetVisibility(1);
        d->vector_glyph_actor->SetVisibility(0);
        d->vector_stream_actor->SetVisibility(0);
        d->vector_stream_widget->Off();
    }
}

void axlActorFieldDiscrete::displayAsGlyph(void)
{
    if (d->field->kind() == axlFieldDiscrete::Vector) {
        d->vector_hedgehog_actor->SetVisibility(0);
        d->vector_glyph_actor->SetVisibility(1);
        d->vector_stream_actor->SetVisibility(0);
        d->vector_stream_widget->Off();
    }
}

void axlActorFieldDiscrete::displayAsStream(void)
{
    if (d->field->kind() == axlFieldDiscrete::Vector) {

        d->vector_hedgehog_actor->SetVisibility(0);
        d->vector_glyph_actor->SetVisibility(0);
        d->vector_stream_actor->SetVisibility(1);
        d->vector_stream_widget->On();
    }
}

#include <axlCore/axlAbstractSurfaceBSpline.h>

void axlActorFieldDiscrete::update(void)
{
    if(!d->mesh)
        return;

    if(d->mesh->getUnstructuredGrid()){
        d->mesh->getUnstructuredGrid()->Modified();
#if (VTK_MAJOR_VERSION <= 5)
        d->mesh->getUnstructuredGrid()->Update();
#endif
    }else{
        d->mesh->getPolyData()->Modified();
#if (VTK_MAJOR_VERSION <= 5)
        d->mesh->getPolyData()->Update();
#endif
    }

    if(!d->field )
        return;

    //update the value of the field.
    this->updateArray();

    if(d->field->support() == axlFieldDiscrete::Point) {
        if(d->mesh->getUnstructuredGrid()){
            static_cast<vtkUnstructuredGrid *>(d->mesh->getUnstructuredGrid())->GetPointData()->RemoveArray(static_cast<vtkDataArray *>(d->array)->GetName());
            static_cast<vtkUnstructuredGrid *>(d->mesh->getUnstructuredGrid())->GetPointData()->AddArray(static_cast<vtkDataArray *>(d->array));
        }else{
            static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetPointData()->RemoveArray(static_cast<vtkDataArray *>(d->array)->GetName());
            static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetPointData()->AddArray(static_cast<vtkDataArray *>(d->array));
        }
        if(d->field->kind() == axlFieldDiscrete::Scalar)
            if(d->mesh->getUnstructuredGrid()){
                static_cast<vtkUnstructuredGrid *>(d->mesh->getUnstructuredGrid())->GetPointData()->SetActiveScalars(static_cast<vtkDataArray *>(d->array)->GetName());
            }else{
                static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetPointData()->SetActiveScalars(static_cast<vtkDataArray *>(d->array)->GetName());
            }
        else if (d->field->kind() == axlFieldDiscrete::Vector)
            static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetPointData()->SetActiveVectors(static_cast<vtkDataArray *>(d->array)->GetName());
        else if (d->field->kind() == axlFieldDiscrete::Tensor)
            static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetPointData()->SetActiveTensors(static_cast<vtkDataArray *>(d->array)->GetName());

    }



    if(d->field->support() == axlFieldDiscrete::Cell) {
        static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetCellData()->RemoveArray(static_cast<vtkDataArray *>(d->array)->GetName());
        static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetCellData()->AddArray(static_cast<vtkDataArray *>(d->array));
        if(d->field->kind() == axlFieldDiscrete::Scalar)
            static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetCellData()->SetActiveScalars(static_cast<vtkDataArray *>(d->array)->GetName());
        else if (d->field->kind() == axlFieldDiscrete::Vector)
            static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetCellData()->SetActiveVectors(static_cast<vtkDataArray *>(d->array)->GetName());
        else if (d->field->kind() == axlFieldDiscrete::Tensor)
            static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetCellData()->SetActiveTensors(static_cast<vtkDataArray *>(d->array)->GetName());


    }

    emit updated();

}

void axlActorFieldDiscrete::setActiveFieldKind(void)
{
    if(d->mesh && d->field)
        if(d->field->support() == axlFieldDiscrete::Point) {
            if(d->field->kind() == axlFieldDiscrete::Scalar)
                if(d->mesh->getUnstructuredGrid()){
                    static_cast<vtkUnstructuredGrid *>(d->mesh->getUnstructuredGrid())->GetPointData()->SetActiveScalars(qPrintable(d->field->objectName()));
                }else{
                    static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetPointData()->SetActiveScalars(qPrintable(d->field->objectName()));
                }
            else if(d->field->kind() == axlFieldDiscrete::Vector)
                static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetPointData()->SetActiveVectors(qPrintable(d->field->objectName()));
            else if(d->field->kind() == axlFieldDiscrete::Tensor)
                static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetPointData()->SetActiveTensors(qPrintable(d->field->objectName()));
        }


    if(d->mesh && d->field)
        if(d->field->support() == axlFieldDiscrete::Cell) {
            if(d->field->kind() == axlFieldDiscrete::Scalar)
                static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetCellData()->SetActiveScalars(qPrintable(d->field->objectName()));
            else if(d->field->kind() == axlFieldDiscrete::Vector)
                static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetCellData()->SetActiveVectors(qPrintable(d->field->objectName()));
            else if(d->field->kind() == axlFieldDiscrete::Tensor)
                static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetCellData()->SetActiveTensors(qPrintable(d->field->objectName()));
        }

}

void *axlActorFieldDiscrete::scalarColorMapper(void)
{
    return d->scalar_color_mapper;
}

axlActorFieldDiscrete::axlActorFieldDiscrete(void) : axlAbstractActorField(), d(new axlActorFieldDiscretePrivate)
{
    d->field = NULL;
    d->mesh = NULL;
    d->isoRadius = 0.01;
}

axlActorFieldDiscrete::~axlActorFieldDiscrete(void)
{
    delete d;

    d = NULL;
}

void axlActorFieldDiscrete::setup(void)
{

    if(!d->mesh)
        return;

    if(!d->field) {
        qDebug() << DTK_PRETTY_FUNCTION << "No field.";
        return;
    }


    if(!d->interactor)
        return;


    // -- Scalar field


    if(d->field->kind() == axlFieldDiscrete::Scalar) {

        if(d->scalar_color_mapper && d->scalar_color_actor && d->scalar_iso_color_actor)
            return;

        if(d->field->support() == axlFieldDiscrete::Point) {
            //remove first eventual array of the same name
            if(d->mesh->getPolyData()){
                static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetPointData()->RemoveArray(static_cast<vtkDataArray *>(d->array)->GetName());
                static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetPointData()->AddArray(static_cast<vtkDataArray *>(d->array));
                static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetPointData()->SetActiveScalars(static_cast<vtkDataArray *>(d->array)->GetName());
            }
            if(d->mesh->getUnstructuredGrid()){
                static_cast<vtkUnstructuredGrid *>(d->mesh->getUnstructuredGrid())->GetPointData()->RemoveArray(static_cast<vtkDataArray *>(d->array)->GetName());
                static_cast<vtkUnstructuredGrid *>(d->mesh->getUnstructuredGrid())->GetPointData()->AddArray(static_cast<vtkDataArray *>(d->array));
                static_cast<vtkUnstructuredGrid *>(d->mesh->getUnstructuredGrid())->GetPointData()->SetActiveScalars(static_cast<vtkDataArray *>(d->array)->GetName());
            }
        }

        if(d->field->support() == axlFieldDiscrete::Cell) {

            static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetCellData()->RemoveArray(static_cast<vtkDataArray *>(d->array)->GetName());
            static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetCellData()->AddArray(static_cast<vtkDataArray *>(d->array));
            static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetPointData()->SetActiveScalars(static_cast<vtkDataArray *>(d->array)->GetName());

        }

        // -- iso contours

        d->scalar_display_as_iso = false;
        d->scalar_iso_count = 10;
        static_cast<vtkDataArray *>(d->array)->GetRange(d->scalar_iso_range);

        d->scalar_iso = vtkSmartPointer<vtkContourFilter>::New();
        if(d->mesh->getUnstructuredGrid()){
#if (VTK_MAJOR_VERSION <= 5)
            d->scalar_iso->SetInput(static_cast<vtkUnstructuredGrid *>(d->mesh->getUnstructuredGrid()));
#else
            d->scalar_iso->SetInputData(static_cast<vtkUnstructuredGrid *>(d->mesh->getUnstructuredGrid()));
#endif
        }else{
#if (VTK_MAJOR_VERSION <= 5)
            d->scalar_iso->SetInput(static_cast<vtkPolyData *>(d->mesh->getPolyData()));
#else
            d->scalar_iso->SetInputData(static_cast<vtkPolyData *>(d->mesh->getPolyData()));
#endif
        }
        d->scalar_iso->GenerateValues(d->scalar_iso_count, d->scalar_iso_range[0], d->scalar_iso_range[1]);

        d->scalar_iso_tube_filter = vtkSmartPointer<vtkTubeFilter>::New();
        d->scalar_iso_tube_filter->SetRadius(d->isoRadius);
        d->scalar_iso_tube_filter->SetNumberOfSides(8);
#if (VTK_MAJOR_VERSION <= 5)
        d->scalar_iso_tube_filter->SetInput(d->scalar_iso->GetOutput());
#else
        d->scalar_iso_tube_filter->SetInputData(d->scalar_iso->GetOutput());
#endif
        d->scalar_iso_mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
#if (VTK_MAJOR_VERSION <= 5)
        d->scalar_iso_mapper->SetInput(d->scalar_iso_tube_filter->GetOutput());
#else
        d->scalar_iso_mapper->SetInputData(d->scalar_iso_tube_filter->GetOutput());
#endif

        d->scalar_iso_actor = vtkSmartPointer<vtkActor>::New();
        d->scalar_iso_actor->SetMapper(d->scalar_iso_mapper);
        d->scalar_iso_actor->SetVisibility(0);

        this->AddPart(d->scalar_iso_actor);

        // -- iso color mapping

        d->scalar_iso_color_mapper = vtkPolyDataMapper::New();
#if (VTK_MAJOR_VERSION <= 5)
        d->scalar_iso_color_mapper->SetInput(d->scalar_iso->GetOutput());
#else
        d->scalar_iso_color_mapper->SetInputData(d->scalar_iso->GetOutput());
#endif
        d->scalar_iso_color_mapper->SetColorModeToMapScalars();

        if(d->field->support() == axlFieldDiscrete::Point)
            d->scalar_iso_color_mapper->SetScalarModeToUsePointData();

        if(d->field->support() == axlFieldDiscrete::Cell)
            d->scalar_iso_color_mapper->SetScalarModeToUseCellData();

        d->scalar_iso_color_mapper->SelectColorArray(static_cast<vtkDataArray *>(d->array)->GetName());
        d->scalar_iso_color_mapper->SetScalarVisibility(true);
        d->scalar_iso_color_mapper->SetScalarRange(d->scalar_iso_range);

        d->scalar_iso_color_actor = vtkSmartPointer<vtkActor>::New();
        d->scalar_iso_color_actor->SetMapper(d->scalar_iso_color_mapper);
        d->scalar_iso_color_actor->SetVisibility(0);

        this->AddPart(d->scalar_iso_color_actor);

        // -- color mapping

        static_cast<vtkDataArray *>(d->array)->GetRange(d->scalar_color_range);

        d->scalar_color_mapper = vtkDataSetMapper::New();
#if (VTK_MAJOR_VERSION <= 5)
        if(d->mesh->getUnstructuredGrid())
            d->scalar_color_mapper->SetInput(static_cast<vtkUnstructuredGrid *>(d->mesh->getUnstructuredGrid()));
        else
            d->scalar_color_mapper->SetInput(static_cast<vtkPolyData *>(d->mesh->getPolyData()));
#else
        if(d->mesh->getUnstructuredGrid())
            d->scalar_color_mapper->SetInputData(static_cast<vtkUnstructuredGrid *>(d->mesh->getUnstructuredGrid()));
        else
            d->scalar_color_mapper->SetInputData(static_cast<vtkPolyData *>(d->mesh->getPolyData()));
#endif

        d->scalar_color_mapper->SetColorModeToMapScalars();

        if(d->field->support() == axlFieldDiscrete::Point)
            d->scalar_color_mapper->SetScalarModeToUsePointData();

        if(d->field->support() == axlFieldDiscrete::Cell)
            d->scalar_color_mapper->SetScalarModeToUseCellData();

        d->scalar_color_mapper->SelectColorArray(static_cast<vtkDataArray *>(d->array)->GetName());
        d->scalar_color_mapper->SetScalarVisibility(true);
        d->scalar_color_mapper->SetScalarRange(d->scalar_color_range);

        d->scalar_color_actor = vtkSmartPointer<vtkActor>::New();
        d->scalar_color_actor->SetMapper(d->scalar_color_mapper);
        d->scalar_color_actor->SetVisibility(0);

        this->AddPart(d->scalar_color_actor);



        ////Add actor for color bar
        d->scalar_bar_actor = vtkSmartPointer<vtkScalarBarActor>::New();
        d->scalar_bar_actor ->SetLookupTable(d->scalar_color_mapper->GetLookupTable());
        d->scalar_bar_actor ->SetTitle(qPrintable(d->field->name()));
        d->scalar_bar_actor ->SetNumberOfLabels(4);
        d->scalar_bar_actor->SetVisibility(0);



    }

    // -- Vector field


    if(d->field->kind() == axlFieldDiscrete::Vector) {

        // -- Append field and its magnitude into vtkPolyData

        if(d->field->support() == axlFieldDiscrete::Point) {

            static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetPointData()->RemoveArray(static_cast<vtkDataArray *>(d->array)->GetName());
            static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetPointData()->AddArray(static_cast<vtkDataArray *>(d->array));
            static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetPointData()->SetActiveVectors(static_cast<vtkDataArray *>(d->array)->GetName());

        }

        if(d->field->support() == axlFieldDiscrete::Cell) {
            static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetPointData()->RemoveArray(static_cast<vtkDataArray *>(d->array)->GetName());
            static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetCellData()->AddArray(static_cast<vtkDataArray *>(d->array));
            static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetCellData()->SetActiveVectors(static_cast<vtkDataArray *>(d->array)->GetName());


        }

        // -- hedgehog

        if(! d->vector_hedgehog)
            d->vector_hedgehog = vtkSmartPointer<vtkHedgeHog>::New();

#if (VTK_MAJOR_VERSION <= 5)
        d->vector_hedgehog->SetInput(static_cast<vtkPolyData *>(d->mesh->getPolyData()));
#else
        d->vector_hedgehog->SetInputData(static_cast<vtkPolyData *>(d->mesh->getPolyData()));
#endif
        d->vector_hedgehog->SetVectorModeToUseVector();
        d->vector_hedgehog->SetScaleFactor(0.05);

        if(!d->vector_hedgehog_mapper) {
            d->vector_hedgehog_mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
            d->vector_hedgehog_mapper->SetInputConnection(d->vector_hedgehog->GetOutputPort());
            d->vector_hedgehog_mapper->ScalarVisibilityOn();
        }

        if(!d->vector_hedgehog_actor)
            d->vector_hedgehog_actor = vtkSmartPointer<vtkActor>::New();
        d->vector_hedgehog_actor->SetMapper(d->vector_hedgehog_mapper);
        d->vector_hedgehog_actor->SetVisibility(0);

        this->AddPart(d->vector_hedgehog_actor);

        // -- glyphs

        if(!d->vector_glyph)
            d->vector_glyph = vtkSmartPointer<vtkGlyph3D>::New();

        if(!d->vector_glyph_source)
            d->vector_glyph_source = vtkArrowSource::New();


#if (VTK_MAJOR_VERSION <= 5)
        d->vector_glyph->SetInput(static_cast<vtkPolyData *>(d->mesh->getPolyData()));
        d->vector_glyph->SetSource(d->vector_glyph_source->GetOutput());
#else
        d->vector_glyph->SetSourceData(d->vector_glyph_source->GetOutput());
        d->vector_glyph->SetSourceConnection(d->vector_glyph_source->GetOutputPort());
        d->vector_glyph->SetInputData(static_cast<vtkPolyData *>(d->mesh->getPolyData()));
#endif

        //d->vector_glyph->SetColorModeToColorByScale();
        d->vector_glyph->SetColorModeToColorByVector();
        d->vector_glyph->SetScaleModeToScaleByVector();
        d->vector_glyph->SetVectorModeToUseVector();
        d->vector_glyph->SetScaleModeToDataScalingOff();
        //d->vector_glyph->ScalingOff();
        //d->vector_glyph->SetScaleFactor(2);
        this->setGlyphScale(2);
        d->vector_glyph->OrientOn();

        if(!d->vector_glyph_mapper) {
            d->vector_glyph_mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
            d->vector_glyph_mapper->SetInputConnection(d->vector_glyph->GetOutputPort());
            d->vector_glyph_mapper->ScalarVisibilityOn();
        }


        if(!d->vector_glyph_actor){
            d->vector_glyph_actor = vtkSmartPointer<vtkActor>::New();
            d->vector_glyph_actor->SetMapper(d->vector_glyph_mapper);
            d->vector_glyph_actor->SetVisibility(0);
        }

        this->AddPart(d->vector_glyph_actor);

        // -- streams

        d->vector_stream_widget = vtkSmartPointer<vtkSphereWidget2>::New();
        d->vector_stream_widget->SetInteractor(d->interactor);
        d->vector_stream_widget->CreateDefaultRepresentation();
        d->vector_stream_widget->SetTranslationEnabled(true);
        d->vector_stream_widget->SetScalingEnabled(true);
        d->vector_stream_widget->Off();

        d->vector_stream_widget_data = vtkPolyData::New();

        d->vector_stream_widget_representation = vtkSphereRepresentation::SafeDownCast(d->vector_stream_widget->GetRepresentation());
        d->vector_stream_widget_representation->HandleVisibilityOff();
        d->vector_stream_widget_representation->HandleTextOff();
        d->vector_stream_widget_representation->RadialLineOff();
        d->vector_stream_widget_representation->SetPhiResolution(64);
        d->vector_stream_widget_representation->SetThetaResolution(64);
        d->vector_stream_widget_representation->GetPolyData(d->vector_stream_widget_data);

        d->vector_stream_tracer = vtkStreamTracer::New();
#if (VTK_MAJOR_VERSION <= 5)
        d->vector_stream_tracer->SetInput(static_cast<vtkPolyData *>(d->mesh->getPolyData()));
        d->vector_stream_tracer->SetSource(d->vector_stream_widget_data);
#else
        d->vector_stream_tracer->SetInputData(static_cast<vtkPolyData *>(d->mesh->getPolyData()));
        d->vector_stream_tracer->SetSourceData(d->vector_stream_widget_data);
#endif
        d->vector_stream_tracer->SetMaximumPropagation(100);
        d->vector_stream_tracer->SetMinimumIntegrationStep(1.0e-4);
        d->vector_stream_tracer->SetMaximumIntegrationStep(100.0);
        d->vector_stream_tracer->SetIntegrationDirectionToBoth();

        d->vector_stream_filter = vtkTubeFilter::New();
#if (VTK_MAJOR_VERSION <= 5)
        d->vector_stream_filter->SetInput(d->vector_stream_tracer->GetOutput());
#else
        d->vector_stream_filter->SetInputData(d->vector_stream_tracer->GetOutput());
#endif
        d->vector_stream_filter->SetRadius(0.01);
        d->vector_stream_filter->SetNumberOfSides(8);

        d->vector_stream_mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
        d->vector_stream_mapper->SetInputConnection(d->vector_stream_filter->GetOutputPort());
        d->vector_stream_mapper->ScalarVisibilityOn();

        d->vector_stream_actor = vtkSmartPointer<vtkActor>::New();
        d->vector_stream_actor->SetMapper(d->vector_stream_mapper);
        d->vector_stream_actor->SetVisibility(0);

        d->vector_stream_observer = axlActorFieldDiscreteStreamObserver::New();
        d->vector_stream_observer->vector_stream_widget_representation = d->vector_stream_widget_representation;
        d->vector_stream_observer->vector_stream_widget_data = d->vector_stream_widget_data;
        d->vector_stream_observer->vector_stream_tracer = d->vector_stream_tracer;
        d->vector_stream_widget->AddObserver(vtkCommand::InteractionEvent, d->vector_stream_observer);

        this->AddPart(d->vector_stream_actor);
    }

}

double axlActorFieldDiscrete::minValue(void)
{
    double field_range[2];static_cast<vtkDataArray *>(d->array)->GetRange(field_range);
    return field_range[0];
}

double axlActorFieldDiscrete::maxValue(void)
{
    double field_range[2];static_cast<vtkDataArray *>(d->array)->GetRange(field_range);

    return field_range[1];
}

void axlActorFieldDiscrete::onIsoRadiusChanged(double radius)
{
    d->isoRadius = radius;
    d->scalar_iso_tube_filter->SetRadius(radius);
    d->scalar_iso_tube_filter->Update();
}
