Magic keyboard shortcuts
========================

Here is the table of shortcuts and the corresponding actions in Axl viewer. 

======== ========
Key       Action
======== ========
Ctrl-o   Open a file
Ctrl-s   Save to a file
M-Ctrl-s Save as a (new) file
Ctrl-q   Quit
Ctrl-i   Open the object inspector
M-Click  Select an object
suppr 	 Delete the selected object(s)
e 	 Change the selected object(s) into editable mode
u 	 Change the edited object(s) into selected mode
r 	 Recenter the view
p        Bounding box of the selected object
x        Add/remove the coordinate axes in the view
w 	 Wireframe visualization
s 	 Smooth visualization
======== ========

where

  - Ctr means the control key (Linux) or the cmd key (MacOS or Windows).
  - M means the shift key.

