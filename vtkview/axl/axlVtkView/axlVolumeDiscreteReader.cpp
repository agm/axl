/* axlVolumeDiscreteReader.cpp ---
 *
 * Author: Anais Ducoffe
 * Copyright (C) 2013 - Anais Ducoffe, Inria.
 */

/* Commentary:
 *
 */

/* Change log:
 *
 */

#include "axlVolumeDiscreteReader.h"

#include "axlVolumeDiscrete.h"

#include <dtkCoreSupport/dtkAbstractData.h>
#include <dtkCoreSupport/dtkAbstractDataFactory.h>
#include <axlCore/axlAbstractDataReader.h>
#include "axlVtkViewPlugin.h"

// /////////////////////////////////////////////////////////////////
// axlVolumeDiscreteReader
// /////////////////////////////////////////////////////////////////

axlVolumeDiscreteReader::axlVolumeDiscreteReader(void)
{
    this->setObjectName(this->identifier());
}

axlVolumeDiscreteReader::~axlVolumeDiscreteReader(void)
{

}

QString axlVolumeDiscreteReader::identifier(void) const
{
    return "axlVolumeDiscreteReader";
}

QString axlVolumeDiscreteReader::description(void) const
{
    return "axlVolumeDiscreteReader";
}

QStringList axlVolumeDiscreteReader::handled(void) const
{
    return QStringList() << "axlVolumeDiscrete";
}

bool axlVolumeDiscreteReader::registered(void)
{
    return axlVtkViewPlugin::dataFactSingleton->registerDataReaderType("axlVolumeDiscreteReader", QStringList(), createaxlVolumeDiscreteReader);
}

bool axlVolumeDiscreteReader::accept(const QDomNode& node)
{
    QDomElement element = node.toElement();

    if(element.tagName() != "volume")
        return false;

    if(element.attribute("type") != "discrete")
        return false;

    if(!hasChildNode(element, "dimension"))
        return false;

    if(!hasChildNode(element, "values"))
        return false;

    return true;
}

bool axlVolumeDiscreteReader::reject(const QDomNode& node)
{
    return !this->accept(node);
}

axlAbstractData *axlVolumeDiscreteReader::read(const QDomNode& node)
{
    QDomElement element = node.toElement();

    axlVolumeDiscrete *currentVolume = new axlVolumeDiscrete();

    QString name = element.attribute("name");
    if(!name.isEmpty())
    {
        currentVolume->setObjectName(name);
    }

    //Set the 3 dimensions.
    QDomNodeList dimension = element.elementsByTagName("dimension");
    if(!dimension.isEmpty()){
        QDomElement dimensionElement = dimension.at(0).toElement();

        QStringList dimensions = dimensionElement.text().simplified().split(QRegExp("\\s+"));

        if(dimensions.size() == 3)
        {
            currentVolume->setDimensions(dimensions[0].toInt(),dimensions[1].toInt(),dimensions[2].toInt());
        }

    }

    //Set the scalar values.
    QDomNodeList values = element.elementsByTagName("values");
    QStringList scalar;
    if(!values.isEmpty()){
        QDomElement scalarElement = values.at(0).toElement();
        scalar = scalarElement.text().simplified().split(QRegExp("\\s+"));
    }

    int nx = currentVolume->xDimension();
    int ny = currentVolume->yDimension();
    int nz = currentVolume->zDimension();
    double number = 0;
    for(int i = 0;i < nx;i++){
        for(int j = 0;j < ny;j++){
            for(int k = 0;k < nz;k++){
                number = scalar[nx*ny*k+ nx*j + i].toDouble();
                currentVolume->setValue(number,i,j,k);
            }
        }
    }

    return currentVolume;

}


axlAbstractData *axlVolumeDiscreteReader::dataByReader(axlAbstractDataReader *axl_reader, const QDomNode& node)
{
    if(!axl_reader)
        return NULL;

    if(!axl_reader->accept(node))
        return NULL;

    axl_reader->dtkAbstractDataReader::read(this->file());

    if(axlAbstractData *data = axl_reader->read(node))
        return data;


    return NULL;
}



dtkAbstractDataReader *createaxlVolumeDiscreteReader(void)
{
    return new axlVolumeDiscreteReader;
}

