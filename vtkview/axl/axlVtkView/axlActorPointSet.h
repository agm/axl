/* axlActorPointSet.h ---
 *
 * Author: Meriadeg Perrinel
 * Copyright (C) 2008 - Meriadeg Perrinel, Inria.
 * Created: Fri Dec 17 10:58:38 2010 (+0100)
 * Version: $Id$
 * Last-Updated: Mon Dec 17 14:11:50 2012 (+0100)
 *           By: Julien Wintz
 *     Update #: 5
 */

/* Commentary:
 *
 */

/* Change log:
 *
 */

#ifndef AXLACTORPOINTSET_H
#define AXLACTORPOINTSET_H

#include "axlActor.h"

#include "axlVtkViewPluginExport.h"

class axlPointSet;
class axlActorPointSetPrivate;

class AXLVTKVIEWPLUGIN_EXPORT axlActorPointSet : public axlActor
{
public:
#if (VTK_MAJOR_VERSION <= 5)
    vtkTypeRevisionMacro(axlActorPointSet, vtkAssembly);
#endif

    static axlActorPointSet *New(void);

public:
   dtkAbstractData *data(void);

public:
    void setPointSet(axlPointSet *axlPointsSet);
    void setMode(int state); 
    void setSize(double size);
public slots:
    void onModeChanged(int state);

protected:
     axlActorPointSet(void);
    ~axlActorPointSet(void);

private:
    axlActorPointSet(const axlActorPointSet&); // Not implemented.
    void operator = (const axlActorPointSet&); // Not implemented.

private:
    axlActorPointSetPrivate *d;
};

#endif //AXLACTORPOINTSET_H
