// /////////////////////////////////////////////////////////////////
// Generated by dtkTestGenerator
// /////////////////////////////////////////////////////////////////

#include "axlConeTest.h"
#include <axlCore/axlCone.h>
#include <axlCore/axlPoint.h>
#include <axlCore/axlDouble.h>
#include <axlCore/axlConeCreator.h>
#include <QtDebug>

axlConeTestObject::axlConeTestObject(void)
{

}

axlConeTestObject::~axlConeTestObject(void)
{

}

void axlConeTestObject::initTestCase(void)
{

}

void axlConeTestObject::init(void)
{

}

void axlConeTestObject::cleanup(void)
{

}

void axlConeTestObject::cleanupTestCase(void)
{

}


void axlConeTestObject::testCreation(void)
{
    axlCone cone;
    QVERIFY(cone.identifier() == "axlCone");
    axlCone *cone1 = new axlCone();
    QVERIFY(cone1);
    axlPoint *basePoint = new axlPoint();
    axlPoint *apexPoint = new axlPoint(1.0,0.0);
    double radius = 2.0;
    axlCone *cone2 = new axlCone(apexPoint, basePoint, radius);
    QVERIFY(cone2);
    axlCone *cone3 = new axlCone(cone);
    QVERIFY(cone3);

    delete cone3;
    delete cone2;
    delete cone1;
    delete basePoint;
    delete apexPoint;


}

// check that the cone cerator process works for dynamic objects.
void axlConeTestObject::testCreatorProcess(void)
{
    axlConeCreator *coneCreator = new axlConeCreator();
    QVERIFY(coneCreator);
    axlPoint *basePoint = new axlPoint();
    axlPoint *apexPoint = new axlPoint(1.0,0.0);
    double radius = 2.0;

    delete coneCreator;
    delete basePoint;
    delete apexPoint;


}

void axlConeTestObject::testBasePoint(void)
{
    axlCone cone;
    QCOMPARE(cone.basePoint()->x(),1.0);
    QCOMPARE(cone.basePoint()->y(),0.0);
    QCOMPARE(cone.basePoint()->z(),0.0);
    axlCone *cone1 = new axlCone();
    QCOMPARE(cone1->basePoint()->x(),1.0);
    QCOMPARE(cone1->basePoint()->y(),0.0);
    QCOMPARE(cone1->basePoint()->z(),0.0);
    axlPoint *basePoint = new axlPoint();
    axlPoint *apexPoint = new axlPoint(1.0,0.0);
    double radius = 2.0;
    axlCone *cone2 = new axlCone(apexPoint, basePoint, radius);
    QVERIFY(cone2->basePoint()->x() == 0.0 );
    QVERIFY(cone2->basePoint()->y() == 0.0);
    QVERIFY(cone2->basePoint()->z() == 0.0);
    axlCone *cone3 = new axlCone(cone);
    QCOMPARE(cone3->basePoint()->x(),1.0);
    QCOMPARE(cone3->basePoint()->y(),0.0);
    QCOMPARE(cone3->basePoint()->z(),0.0);

    delete cone3;
    delete cone2;
    delete cone1;
    delete basePoint;
    delete apexPoint;
}

void axlConeTestObject::testApex(void)
{
    axlCone cone;
    QCOMPARE(cone.apex()->x(),0.0);
    QCOMPARE(cone.apex()->y(),0.0);
    QCOMPARE(cone.apex()->z(),0.0);
    axlCone *cone1 = new axlCone();
    QCOMPARE(cone1->apex()->x(),0.0);
    QCOMPARE(cone1->apex()->y(),0.0);
    QCOMPARE(cone1->apex()->z(),0.0);
    axlPoint *basePoint = new axlPoint();
    axlPoint *apexPoint = new axlPoint(1.0,0.0);
    double radius = 2.0;
    axlCone *cone2 = new axlCone(apexPoint, basePoint, radius);
    QVERIFY(cone2->apex()->x() == 1.0 );
    QVERIFY(cone2->apex()->y() == 0.0);
    QVERIFY(cone2->apex()->z() == 0.0);
    axlCone *cone3 = new axlCone(cone);
    QCOMPARE(cone3->apex()->x(),0.0);
    QCOMPARE(cone3->apex()->y(),0.0);
    QCOMPARE(cone3->apex()->z(),0.0);

    delete cone3;
    delete cone2;
    delete cone1;
    delete basePoint;
    delete apexPoint;
}

void axlConeTestObject::testRadius(void)
{
    axlPoint *basePoint = new axlPoint();
    axlPoint *apexPoint = new axlPoint(1.0,0.0);
    double radius = 2.0;
    axlCone *cone2 = new axlCone(apexPoint, basePoint, radius);
    QCOMPARE(cone2->radius(), radius);

    delete cone2;
    delete basePoint;
    delete apexPoint;

}


DTKTEST_NOGUI_MAIN(axlConeTest,axlConeTestObject)
