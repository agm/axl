/* axlActorTorus.h ---
 *
 * Author: Valentin Michelet
 * Copyright (C) 2008 - Valentin Michelet, Inria.
 * Created: Tue Nov  9 16:58:59 2010 (+0100)
 * Version: $Id$
 * Last-Updated: Mon Dec 17 14:08:18 2012 (+0100)
 *           By: Julien Wintz
 *     Update #: 26
 */

/* Commentary:
 *
 */

/* Change log:
 *
 */

#ifndef AXLACTORTORUS_H
#define AXLACTORTORUS_H

#include "axlActor.h"
#include <QVTKOpenGLWidget.h>

#include "axlVtkViewPluginExport.h"

#include <vtkVersion.h>

class axlTorus;

class vtkParametricFunctionSource;

class axlActorTorusPrivate;

class AXLVTKVIEWPLUGIN_EXPORT axlActorTorus : public axlActor {

public:
#if (VTK_MAJOR_VERSION <= 5)
    vtkTypeRevisionMacro(axlActorTorus, vtkAssembly);
#endif

    static axlActorTorus *New(void);

    dtkAbstractData *data(void);
    vtkParametricFunctionSource *torus(void);

    void setDisplay(bool display);
    void showTorusWidget(bool show);
    void setTorusWidget(bool torusWidget);
    bool isShowTorusWidget(void);
    virtual void setData(dtkAbstractData *torus1);
    void setMode(int state);
    void setQVTKWidget(QVTKOpenGLWidget *widget);

public slots:
    void onModeChanged(int state);
    void onRemoved();
    void onUpdateGeometry();

protected:
     axlActorTorus(void);
    ~axlActorTorus(void);

private:
    axlActorTorus(const axlActorTorus&); // Not implemented.
    void operator = (const axlActorTorus&); // Not implemented.

private:
    axlActorTorusPrivate *d;
};

axlAbstractActor *createAxlActorTorus(void);

#endif // AXLACTORTORUS_H
