/* axlActorCylinder.h ---
 *
 * Author: Meriadeg Perrinel
 * Copyright (C) 2008 - Meriadeg Perrinel, Inria.
 * Created: Fri Dec 17 10:58:38 2010 (+0100)
 * Version: $Id$
 * Last-Updated: Mon Dec 17 14:06:44 2012 (+0100)
 *           By: Julien Wintz
 *     Update #: 14
 */

/* Commentary:
 *
 */

/* Change log:
 *
 */

#ifndef AXLACTORCYLINDER_H
#define AXLACTORCYLINDER_H

#include "axlActor.h"

#include "axlVtkViewPluginExport.h"

#include <QVTKOpenGLWidget.h>

#include <vtkVersion.h>

class axlActorCylinderPrivate;
class axlCylinder;

class vtkCylinderSource;

class AXLVTKVIEWPLUGIN_EXPORT axlActorCylinder : public axlActor
{
public:
#if (VTK_MAJOR_VERSION <= 5)
    vtkTypeRevisionMacro(axlActorCylinder, vtkAssembly);
#endif

    static axlActorCylinder *New(void);

public:
    dtkAbstractData *data(void);
    vtkCylinderSource *cylinder(void);


public:
    void setDisplay(bool display);
    void showCylinderWidget(bool show);
    void setCylinderWidget(bool cylinderWidget);
    bool isShowCylinderWidget(void);
    virtual void setData(dtkAbstractData *cylinder1);
    void setMode(int state);
    void setQVTKWidget(QVTKOpenGLWidget *widget);

public slots:
    void onModeChanged(int state);
    void onRemoved();
    void onUpdateGeometry();

protected:
    axlActorCylinder(void);
    ~axlActorCylinder(void);

private:
    axlActorCylinder(const axlActorCylinder&); // Not implemented.
    void operator = (const axlActorCylinder&); // Not implemented.

private:
    axlActorCylinderPrivate *d;
};

axlAbstractActor *createAxlActorCylinder(void);

#endif //AXLACTORCYLINDER_H
