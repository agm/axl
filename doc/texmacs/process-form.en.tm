<TeXmacs|1.99.1>

<style|<tuple|mmxdoc|british>>

<\body>
  <doc-data|<doc-title|How to generate a dialog interface for a
  process>|<doc-author|<\author-data|<author-name|A. Ducoffe & B. Mourrain>>
    \;
  </author-data>>>

  To run a process from the Axl Application Interface, you need a dialog
  class which is used to select the input and the parameters of the process
  and then execute it.

  Using the mechanism implemented in the <verbatim|axlInspectorToolGeneric
  class> will help you to generate your dialog thanks to a description
  provided in the process, and will avoid to write a dedicated dialog process
  class (though this is still possible).

  To use this tool you must first check (or create) two things:

  <\enumerate>
    <item>the process must implement the method virtual QString
    <hlink|axlAbstractProcess::form(void)
    const|file:///Users/mourrain/Devel/buildaxeldoc/html/classaxl_abstract_process.html#a185bcad9d78a7f9d7d655d4b7cb5ca3a>;

    <item>the process has to be registered in the factory of processes.
  </enumerate>

  <chapter|Examples>

  Here are exemples of classes which uses this mechanism:

  <\itemize>
    <item><hlink|axlFieldSpatialPointDistanceCreator|file:///Users/mourrain/Devel/buildaxeldoc/html/classaxl_field_spatial_point_distance_creator.html>

    <item><hlink|axlBarycenterProcess|file:///Users/mourrain/Devel/buildaxeldoc/html/classaxl_barycenter_process.html>
    ...
  </itemize>

  <chapter|<label|sec_formalism>Formalism>

  Overload <hlink|axlAbstractProcess::form(void)
  const|file:///Users/mourrain/Devel/buildaxeldoc/html/classaxl_abstract_process.html#a185bcad9d78a7f9d7d655d4b7cb5ca3a>with
  a QString following this convention:

  <\itemize>
    <item>a line per input, formatted as follows:

    <\cpp-code>
      "INPUT [--optional CHANNEL] TYPE LABEL"
    </cpp-code>

    where:

    <\itemize>
      <item>CHANNEL is a number identifying the rank of an input, see
      <hlink|dtkAbstractProcess|file:///Users/mourrain/Devel/buildaxeldoc/html/classdtk_abstract_process.html>.
      Write this value only if the process method setInput uses a channel in
      its parameters !

      <item>TYPE is either "int, double or data"

      <item>LABEL is the input's name
    </itemize>
  </itemize>

  <\itemize>
    <item>a line per parameter, formatted as follows:

    <\cpp-code>
      "PARAMETER [--optional CHANNEL] TYPE LABEL"
    </cpp-code>

    where:

    <\itemize>
      <item>CHANNEL is a number identifying the rank of an parameter, see
      <hlink|dtkAbstractProcess|file:///Users/mourrain/Devel/buildaxeldoc/html/classdtk_abstract_process.html>.
      Write this value only if the process method setParameter uses a channel
      in its parameters !

      <item>TYPE is either "int or double"

      <item>LABEL is the parameter's name
    </itemize>
  </itemize>

  <\itemize>
    <item>a line per output, formatted as follows:

    <\cpp-code>
      "OUTPUT [--optional CHANNEL] LABEL"
    </cpp-code>

    where:

    <\itemize>
      <item>CHANNEL is a number identifying the rank of an output, see
      <hlink|dtkAbstractProcess|file:///Users/mourrain/Devel/buildaxeldoc/html/classdtk_abstract_process.html>.

      <item>LABEL is the output's name
    </itemize>
  </itemize>

  Here is an example of such a declaration:

  <\cpp-code>
    QString form(void) const

    {

    \ \ \ \ return QString(

    \ \ \ \ \ \ \ " INPUT 0 data StartPoint \\n"

    \ \ \ \ \ \ \ " INPUT 1 data EndPoint \\n"

    \ \ \ \ \ \ \ " PARAMETER 0 double Radius \\n"

    \ \ \ \ \ \ \ " OUTPUT Cylinder ");

    }
  </cpp-code>

  \;

  Another example with the axel class <hlink|axlBarycenterProcess|file:///Users/mourrain/Devel/buildaxeldoc/html/classaxl_barycenter_process.html>,
  which uses <hlink|axlBarycenterProcess::setParameter(double
  value)|file:///Users/mourrain/Devel/buildaxeldoc/html/classaxl_barycenter_process.html#a6f72c487aeb891808149de5458d15e59>
  and not <hlink|axlBarycenterProcess::setParameter(double value, int
  channel)|file:///Users/mourrain/Devel/buildaxeldoc/html/classdtk_abstract_process.html#ae92068aa7c3c69454452dae71be15678>:

  <\cpp-code>
    QString form(void) const

    {

    \ \ \ \ \ return QString(

    \ \ \ \ \ \ \ \ " INPUT 0 data line \\n"

    \ \ \ \ \ \ \ \ " PARAMETER double coefficient \\n"

    \ \ \ \ \ \ \ \ " OUTPUT 0 barycenterPoint");

    }
  </cpp-code>

  \;

  <with|font-size|0.83| >
</body>