/* axlActorDataDynamic.h ---
 *
 * Author: Anais Ducoffe
 * Copyright (C) 2013 - Anais Ducoffe, Inria.
 */

/* Commentary:
 *
 */

/* Change log:
 *
 */

#ifndef AXLACTORDATADYNAMIC_H
#define AXLACTORDATADYNAMIC_H

#include "axlActor.h"
#include "axlActorComposite.h"

#include <QVTKOpenGLWidget.h>

#include "axlVtkViewPluginExport.h"


class axlLine;
class axlDataDynamic;

class vtkLineSource;
class axlActorDataDynamicPrivate;

class AXLVTKVIEWPLUGIN_EXPORT axlActorDataDynamic : public axlActor
{
public:
#if (VTK_MAJOR_VERSION <= 5)
    vtkTypeRevisionMacro(axlActorDataDynamic, vtkAssembly);
#endif

    static axlActorDataDynamic *New(void);

public:
    dtkAbstractData *data(void);
    axlAbstractActor *outputActor(void);
    axlAbstractActor *outputActors(int i);


public:

    void setView(axlAbstractView *view);
    void setRenderer(vtkRenderer *renderer);

public:
    void setDisplay(bool display);
    virtual void setData(dtkAbstractData *data1);
    void setMode(int state);
    void setQVTKWidget(QVTKOpenGLWidget *widget);

    //axlActor *createActor(dtkAbstractData *data);

public slots:
    void onModeChanged(int state);
    void onRemoved();
    void onUpdateGeometry();

protected:
     axlActorDataDynamic(void);
    ~axlActorDataDynamic(void);

private:
    axlActorDataDynamic(const axlActorDataDynamic&); // Not implemented.
    void operator = (const axlActorDataDynamic&); // Not implemented.

private:
    axlActorDataDynamicPrivate *d;
};

axlAbstractActor *createAxlActorDataDynamic(void);

#endif// AXLACTORDATADYNAMIC_H
