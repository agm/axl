/* axlMainWindow.cpp ---
 *
 * Author: Julien Wintz
 * Copyright (C) 2008 - Julien Wintz, Inria.
 * Created: Mon Dec  6 14:35:08 2010 (+0100)
 * Version: $Id$
 * Last-Updated: Tue Dec 27 13:03:58 2011 (+0100)
 *           By: Julien Wintz
 *     Update #: 224
 */

/* Commentary:
 *
 */

/* Change log:
 *
 */

#include "axlMainWindow.h"

//readers and writers.
#include <axlCore/axlAbstractDataReader.h>
#include <axlCore/axlAbstractDataWriter.h>

#include <dtkCoreSupport/dtkAbstractDataReader.h>
#include <dtkCoreSupport/dtkAbstractDataWriter.h>

//#include <axlCore/axlAbstractView.h>
#include <axlCore/axlAbstractActor.h>
#include <axlCore/axlPoint.h>
#include <axlCore/axlAbstractDataComposite.h>
#include <axlCore/axlReader.h>
#include <axlCore/axlWriter.h>
#include <axlCore/axlViewDefault.h>
#include <axlCore/axlAbstractField.h>
#include <axlCore/axlMenuFactory.h>

#include <axlCore/axlOFFReader.h>

#include <axlGui/axlColorDialog.h>
//#include <axlGui/axlInspector.h>
#include <axlGui/axlInspectorLight.h>
#include <dtkCoreSupport/dtkAbstractDataFactory.h>
#include <dtkCoreSupport/dtkAbstractProcess.h>
#include <dtkCoreSupport/dtkAbstractProcessFactory.h>
#include <dtkCoreSupport/dtkAbstractViewFactory.h>
#include <dtkCoreSupport/dtkGlobal.h>
#include <dtkGuiSupport/dtkRecentFilesMenu.h>
#include <dtkConfig.h>

#if defined(DTK_BUILD_WRAPPERS) && defined(DTK_HAVE_PYTHON)
#include <dtkGuiSupport/dtkInterpreter.h>
#include <dtkScriptSupport/dtkScriptInterpreter.h>
#include <dtkScriptSupport/dtkScriptInterpreterPython.h>
#endif
#include <QtGui>

#include <cstring>

class axlMainWindowPrivate
{
public:
    QMenu *fileMenu;
    QMenu *scriptMenu;
    QMenu *fileImport;
    QMenu *fileExport;
    QAction *fileOpen;
    QAction *scriptInterpreterPython;
    QAction *fileImportCurveAction;
    QAction *fileImportSurfaceAction;
    QAction *fileImportOFFAction;
    QAction *fileImportIGESAction;
    QAction *fileExportTo_OFFAction;
    QAction *fileSaveSplineAction;
    QAction *fileSaveAsSplineAction;
    QAction *clearScene;
    QAction *quit;

    QString windowTitle;

    dtkRecentFilesMenu *recentFilesMenu;

    axlInspector *inspector;

#if defined(DTK_BUILD_WRAPPERS) && defined(DTK_HAVE_PYTHON)
    //script
    dtkInterpreter *interpreter;
    QDockWidget *scriptDockWidget;
    QBoxLayout *scriptTitleLayout;
#endif


public:
    axlAbstractView *view;
};


axlMainWindow::axlMainWindow(QWidget *parent) : QMainWindow(parent), d(new axlMainWindowPrivate)
{
    QSettings settings("inria", "axel");
    settings.beginGroup("axlMainWindow");
    settings.setValue("currentFileName", "");
    settings.endGroup();

    d->view = NULL;
    d->view = dynamic_cast<axlAbstractView *>(dtkAbstractViewFactory::instance()->create("axlVtkView"));

    if(!d->view)
        d->view = new axlViewDefault;

    d->fileOpen = new QAction("Open", this);
    d->fileOpen->setShortcut(Qt::ControlModifier + Qt::Key_O);
    connect(d->fileOpen, SIGNAL(triggered()), this, SLOT(fileOpen()));


    d->scriptInterpreterPython = new QAction("Python Interpreter", this);
    connect(d->scriptInterpreterPython, SIGNAL(triggered()), this, SLOT(scriptInterpreterPython()));


    d->fileImport = new QMenu("Import", this);
    d->fileExport = new QMenu("Export", this);


    ////Read other kind of files than axl files.
    ////We check in the reader factory if the appropriated readers exist.
    d->fileImportCurveAction = new QAction("BSpline Curve (.g2, .go)", this);
    connect(d->fileImportCurveAction, SIGNAL(triggered()), this, SLOT(fileImportCurve()));

    d->fileImportSurfaceAction = new QAction("BSpline Surface (.g2, .go)", this);
    connect(d->fileImportSurfaceAction, SIGNAL(triggered()), this, SLOT(fileImportSurface()));

    d->fileImportOFFAction = new QAction("OFF files (.OFF, .off)", this);
    connect(d->fileImportOFFAction, SIGNAL(triggered()), this, SLOT(fileImportOFF()));

    d->fileImportIGESAction = new QAction("IGES file (.iges, .igs, .IGES, .IGS)", this);
    d->fileImportIGESAction->setShortcut(QKeySequence("CTRL+SHIFT+G"));
    connect(d->fileImportIGESAction, SIGNAL(triggered()), this, SLOT(fileImportIGES()));

    d->fileSaveSplineAction = new QAction("Save", this);
    d->fileSaveSplineAction->setShortcut(Qt::ControlModifier + Qt::Key_S);
    connect(d->fileSaveSplineAction, SIGNAL(triggered()), this, SLOT(fileSave()));

    d->fileSaveAsSplineAction = new QAction("Save as", this);
    d->fileSaveAsSplineAction->setShortcut(Qt::ControlModifier + Qt::ShiftModifier + Qt::Key_S);
    connect(d->fileSaveAsSplineAction, SIGNAL(triggered()), this, SLOT(fileSaveAs()));


    d->fileExportTo_OFFAction = new QAction("Export (.OFF)", this);
    connect(d->fileExportTo_OFFAction, SIGNAL(triggered()), this, SLOT(fileExportTo_OFF()));

    d->recentFilesMenu = new dtkRecentFilesMenu("Open recent", this);
    connect(d->recentFilesMenu, SIGNAL(recentFileTriggered(const QString&)), this, SLOT(fileOpen(const QString&)));

    d->clearScene = new QAction("Clear", this);
    connect(d->clearScene, SIGNAL(triggered()), this, SLOT(clearScene()));

    d->quit = new QAction("Quit", this);
    connect(d->quit, SIGNAL(triggered()), this, SLOT(close()));

    d->fileMenu = this->menuBar()->addMenu("File");
    d->fileMenu->addAction(d->fileOpen);
    d->fileMenu->addMenu(d->recentFilesMenu);
    d->fileMenu->addAction(d->fileSaveSplineAction);
    d->fileMenu->addAction(d->fileSaveAsSplineAction);
    d->fileMenu->addSeparator();
    d->fileMenu->addMenu(d->fileImport);
    d->fileMenu->addMenu(d->fileExport);
    d->fileMenu->addSeparator();
    d->fileMenu->addAction(d->clearScene);
    d->fileMenu->addSeparator();
    d->fileMenu->addAction(d->quit);


    //we assume that we don't know all the readers that are not axlAbstractDataReader. Same idea for writers.

    ////IMPORT FILES
    // test for all reader registered in the factory
    foreach(QString reader, dtkAbstractDataFactory::instance()->readers()) {
        dtkAbstractDataReader *dtkreader = dtkAbstractDataFactory::instance()->reader(reader);
        bool res = (dynamic_cast<axlAbstractDataReader *>(dtkreader));
        if(!res){
            //add the appropriated QAction in file Import menu
            //actionName represents the text menu of the action.
            QString actionName = "Import ";
            QString inter;
            int lenghtExtensionTypes = dtkreader->handled().size();
            for(int indice=0;indice < lenghtExtensionTypes-1;indice++){
                actionName.append(dtkreader->handled().at(indice));
                actionName.append(" ");
            }
            inter.append(dtkreader->handled().at(lenghtExtensionTypes-1));
            actionName.append("file") ;
            //objectName will be use to know what type of extension the user can select.
            QString objectName = inter + " (";
            for(int indice=0;indice < lenghtExtensionTypes-1;indice++){
                objectName.append("*");
                objectName.append(dtkreader->handled().at(indice));
                objectName.append(" ");
            }
            objectName.append(")");
            QAction *action = new QAction(actionName , this);
            action->setObjectName(objectName);
            action->setCheckable(true);
            connect(action, SIGNAL(triggered()), this, SLOT(fileImport()));
            d->fileImport->addAction(action);
        }
        delete dtkreader;

    }


    ////EXPORT FILES
    // test for all writer registered in the factory
    foreach(QString writer, dtkAbstractDataFactory::instance()->writers()) {
        dtkAbstractDataWriter *dtkwriter = dtkAbstractDataFactory::instance()->writer(writer);
        if(!dynamic_cast<axlAbstractDataWriter *>(dtkwriter)){
            //add the appropriated QAction in file Export menu
            QString actionName = "Export to ";
            QString inter;
            int lenghtExtensionTypes = dtkwriter->handled().size();
            for(int indice=0;indice < lenghtExtensionTypes-1;indice++){
                actionName.append(dtkwriter->handled().at(indice));
                actionName.append(" ");
            }
            inter.append(dtkwriter->handled().at(lenghtExtensionTypes-1));
            actionName.append("file") ;
            //objectName will be use to know what type of extension the user can select.
            QString objectName = inter + " (";
            for(int indice=0;indice < lenghtExtensionTypes-1;indice++){
                objectName.append("*");
                objectName.append(dtkwriter->handled().at(indice));
                objectName.append(" ");
            }
            objectName.append(")");
            QAction *action = new QAction(actionName, this);
            action->setObjectName(objectName);
            action->setCheckable(true);
            connect(action, SIGNAL(triggered()), this, SLOT(fileExport()));
            d->fileExport->addAction(action);
        }

        delete dtkwriter;

    }
    
    d->scriptMenu = this->menuBar()->addMenu("Script");
    d->scriptMenu->addAction(d->scriptInterpreterPython);


    // Register menus
    foreach(QMenu* menu, axlMenuFactory::instance()->menus() ) {
        this->menuBar()->addMenu(menu);
    }

#if defined(DTK_BUILD_WRAPPERS) && defined(DTK_HAVE_PYTHON)
    d->interpreter = NULL;
    d->scriptDockWidget = NULL;
#endif

    d->inspector = new axlInspector(this);
    d->inspector->setView(d->view);


    QHBoxLayout *main_layout = new QHBoxLayout;
    main_layout->setContentsMargins(0, 0, 0, 0);
    main_layout->setSpacing(0);
    main_layout->addWidget(d->view->widget());
    main_layout->addWidget(d->inspector);

    QWidget *main = new QWidget(this);
    main->setLayout(main_layout);

    if (d->view && d->view->widget())
        this->setCentralWidget(main);

//    this->setUnifiedTitleAndToolBarOnMac(true);
    this->readSettings();
    d->windowTitle = "Axl ";
    d->windowTitle += AXL_VERSION;
    this->setWindowTitle(d->windowTitle);

    connect(d->inspector, SIGNAL(stateChanged(dtkAbstractData *, int)), d->view, SLOT(onStateChanged(dtkAbstractData*,int)));
    connect(d->inspector, SIGNAL(actorVisibilityChanged(dtkAbstractData *, bool)), d->view, SLOT(onActorVisibilityChanged(dtkAbstractData*,bool)));

    connect(d->inspector, SIGNAL(update()), this, SLOT(update()));
    connect(d->inspector, SIGNAL(dataInserted(axlAbstractData *)), this, SLOT(onDataInserted(axlAbstractData *)));
    connect(d->inspector, SIGNAL(dataChanged(dtkAbstractData *)), d->view, SLOT(showColorMapping(dtkAbstractData *)));
    connect(d->inspector, SIGNAL(dataChangedByControlPoints(dtkAbstractData *)), d->view, SLOT(onControlPointChanged(dtkAbstractData *)));

    // connect(d->inspector, SIGNAL(dataChangedByGeometry(dtkAbstractData *)), d->view, SLOT(dataChangedByGeometry(dtkAbstractData *)));


    //    connect(d->inspector, SIGNAL(dataChangedByShader(dtkAbstractData*, QString)), d->view, SLOT(onShaderChanged(dtkAbstractData*,QString)));
    //    connect(d->inspector, SIGNAL(dataChangedByShaderFromString(dtkAbstractData*, QString)), d->view, SLOT(processIsophoteByShader(dtkAbstractData *, QString)));
    //    connect(d->inspector, SIGNAL(dataChangedByOpacity(dtkAbstractData*, double)), d->view, SLOT(dataChangedByOpacity(dtkAbstractData *, double)));
    //    connect(d->inspector, SIGNAL(dataChangedBySize(dtkAbstractData*, double)), d->view, SLOT(dataChangedBySize(dtkAbstractData *, double)));
    connect(d->inspector, SIGNAL(interpolationChanded(dtkAbstractData*,int)), d->view, SLOT(onInterpolationChanded(dtkAbstractData*,int)));

    //    connect(d->inspector, SIGNAL(dataChangedByColor(dtkAbstractData *, double, double, double)),  d->view, SLOT(dataChangedByColor(dtkAbstractData *, double, double, double)));
    connect(d->inspector, SIGNAL(modifiedProperty(dtkAbstractData *,int)),  d->view, SLOT(dataChangedByProperty(dtkAbstractData *,int)));

    connect(d->inspector, SIGNAL(dataSetRemoved(QList<dtkAbstractData*>)), d->view, SLOT(ondataSetRemoved(QList<dtkAbstractData*>)));

    connect(d->inspector, SIGNAL(showCurrentPoint(double, double, dtkAbstractData *)), this, SLOT(onShowCurrentPoint(double, double, dtkAbstractData *)));
    connect(d->inspector, SIGNAL(moveCurrentPoint(double, double, dtkAbstractData *)), this, SLOT(onMoveCurrentPoint(double, double ,dtkAbstractData *)));
    connect(d->inspector, SIGNAL(hideCurrentPoint(double, double, dtkAbstractData *)), this, SLOT(onHideCurrentPoint(double, double, dtkAbstractData *)));

    connect(d->view, SIGNAL(stateChanged(dtkAbstractData *,int)), d->inspector, SLOT(onStateChanged(dtkAbstractData *, int)));
    connect(d->view, SIGNAL(insertData(axlAbstractData *)), this, SLOT(onDataInserted(axlAbstractData *)));


    connect(d->inspector, SIGNAL(worldCamera()),d->view,SLOT(setWorldCamera()));
    connect(d->inspector, SIGNAL(objectCamera()),d->view,SLOT(setObjectCamera()));
    connect(d->inspector, SIGNAL(trackballCamera()),d->view,SLOT(setTrackballInteractor()));
    connect(d->inspector, SIGNAL(joystickCamera()),d->view,SLOT(setJoystickInteractor()));
    connect(d->inspector, SIGNAL(switchFullScreen()), this, SLOT(switchFullNormalScreen()));
}

axlMainWindow::~axlMainWindow(void)
{

    if(d->view){
        delete d->view;
        d->view = NULL;
    }


    delete d;
    d = NULL;
}

void axlMainWindow::update()
{
    d->view->update();
}

void axlMainWindow::readSettings(void)
{
    QSettings settings("inria", "axel");
    settings.beginGroup("axlMainWindow");
    QPoint pos = settings.value("pos", QPoint(200, 200)).toPoint();
    QSize size = settings.value("size", QSize(600, 400)).toSize();
    move(pos);
    resize(size);
    settings.endGroup();
}

void axlMainWindow::writeSettings(void)
{
    QSettings settings("inria", "axel");
    settings.beginGroup("axlMainWindow");
    settings.setValue("pos", pos());
    settings.setValue("size", size());
    settings.endGroup();
}

void axlMainWindow::fileOpen(const QString& file) {
    QStringList filesToOpen;
    //    if(file.endsWith(".axl")) {
    //        filesToOpen += file;
    //    } else {
    if (file.isEmpty()) {
        QSettings settings("inria", "axel");
        settings.beginGroup("axlMainWindow");
        filesToOpen = QFileDialog::getOpenFileNames(this, tr("Open"), settings.value("open_path").toString(), tr("axel document (*.axl)"));
        if (filesToOpen.isEmpty())
            return;
        // save path from last file opened
        QFileInfo fileInfo(filesToOpen.last());
        settings.setValue("open_path", fileInfo.path());
        settings.endGroup();
    } else {
        filesToOpen += file;
    }
    //    }

    for (QStringList::iterator it = filesToOpen.begin(); it != filesToOpen.end(); ++it) {
        axlReader *reader = new axlReader;

        if (reader) {
            connect(reader, SIGNAL(dataSetInserted(QList<axlAbstractData *>)), this, SLOT(onDataSetInserted(QList<axlAbstractData *>)));
            connect(reader, SIGNAL(dataSetFieldsChanged(QList<axlAbstractData*>,QString)), this, SLOT(onFieldsInserted(QList<axlAbstractData*>,QString)));
            connect(reader, SIGNAL(dataSceneRead(QDomElement)), this, SLOT(readScene(QDomElement)));
            reader->read(*it);
            disconnect(reader, SIGNAL(dataSetInserted(QList<axlAbstractData *>)), this, SLOT(onDataSetInserted(QList<axlAbstractData *>)));
            disconnect(reader, SIGNAL(dataSetFieldsChanged(QList<axlAbstractData*>,QString)), this, SLOT(onFieldsInserted(QList<axlAbstractData*>,QString)));
            disconnect(reader, SIGNAL(dataSceneRead(QDomElement)), this, SLOT(readScene(QDomElement)));
        }
        d->recentFilesMenu->addRecentFile(*it);
        delete reader;
        // the Current Filename has been changed
        this->setCurrentFileSetting(QString());
    }
}

void axlMainWindow::readScene(QDomElement element){

    QDomNodeList node = element.elementsByTagName("color");
    QDomElement curElement = node.item(0).toElement();
    QStringList color = curElement.text().simplified().split(QRegExp("\\s+"));
    if(color.size() == 3)
        d->view->setBackgroundColor(color[0].toDouble(),color[1].toDouble(),color[2].toDouble());

    node = element.elementsByTagName("gradient");
    curElement = node.item(0).toElement();
    if (curElement.text() == "on")
        d->view->onBackgroundGradientChange(1);
    else if (curElement.text() == "off")
        d->view->onBackgroundGradientChange(0);

    node = element.elementsByTagName("axis");
    curElement = node.item(0).toElement();
    if (curElement.text() == "on")
        d->view->onShowAxis(1);
    else if (curElement.text() == "off")
        d->view->onShowAxis(0);

    node = element.elementsByTagName("mode");
    curElement = node.item(0).toElement();
    if (curElement.text() == "parallel")
        d->view->setParallelCamera(1);
    else if (curElement.text() == "perspective")
        d->view->setParallelCamera(0);

    node = element.elementsByTagName("camera");
    curElement = node.item(0).toElement();

    QDomNodeList nodeCamera = curElement.elementsByTagName("focal");
    QDomElement cameraElement = nodeCamera.item(0).toElement();
    d->view->setCameraFocalPoint(cameraElement.attribute("x").toDouble(),cameraElement.attribute("y").toDouble(),cameraElement.attribute("z").toDouble());

    nodeCamera = curElement.elementsByTagName("viewUp");
    cameraElement = nodeCamera.item(0).toElement();
    d->view->setCameraUp(cameraElement.attribute("x").toDouble(),cameraElement.attribute("y").toDouble(),cameraElement.attribute("z").toDouble());

    nodeCamera = curElement.elementsByTagName("viewAngle");
    cameraElement = nodeCamera.item(0).toElement();
    d->view->setCameraViewAngle(cameraElement.text().toDouble());

    if (d->view->getParallelProjection()){
        nodeCamera = curElement.elementsByTagName("parallelScale");
        cameraElement = nodeCamera.item(0).toElement();
        d->view->setParallelScale(cameraElement.text().toDouble());
    }

    // set position last (because at setCameraPosition we ResetCameraClippingRange)
    nodeCamera = curElement.elementsByTagName("position");
    cameraElement = nodeCamera.item(0).toElement();
    d->view->setCameraPosition(cameraElement.attribute("x").toDouble(),cameraElement.attribute("y").toDouble(),cameraElement.attribute("z").toDouble());

    d->inspector->updateView();
}

void axlMainWindow::clearScene(void) {
    if ((QMessageBox::question(this, "Axl: Clear all",
            "All objects will be removed.\nDo you want to continue?")) == QMessageBox::No)
        return;
    d->inspector->clearScene();
}


//! Open the  file which is not an axl file, with the appropriate reader.
/*!
 *
 */
void axlMainWindow::fileImport(const QString& file){
    QString fileToOpen;

    if(file.isEmpty()) {
        QSettings settings("inria", "axel");
        settings.beginGroup("axlMainWindow");
        QString rightExtension;
        foreach(QAction *action, d->fileImport->actions()){
            if(action->isChecked()){
                rightExtension = action->objectName();
                action->setChecked(false);
            }
        }
        const char * extension = rightExtension.toStdString().c_str();
        fileToOpen = QFileDialog::getOpenFileName(this, tr("Import file"), settings.value("import_path").toString(), tr(extension));
        if(!fileToOpen.isEmpty())
        {
            QFileInfo fileInfo(fileToOpen);
            settings.setValue("import__path", fileInfo.path());
        }
        settings.endGroup();
    } else {
        fileToOpen = file;
    }

    //search for the appropriate reader in the factory.
    bool res = false;
    bool reader_found = false;
    foreach(QString reader, dtkAbstractDataFactory::instance()->readers()) {
        if(!reader_found){
            dtkAbstractDataReader *dtkreader = dtkAbstractDataFactory::instance()->reader(reader);

            if(dtkreader->canRead(fileToOpen)){
                if(!dynamic_cast<axlAbstractDataReader *>(dtkreader)){
                    res = dtkreader->read(fileToOpen);
                    reader_found = res;
                }
            }
            if(res){
                if(dtkreader->data()){
                    this->onDataInserted(dynamic_cast<axlAbstractData *>(dtkreader->data()));
                }else{
                    dtkWarn() << "no data available to send to the view";
                }
            }
        }
    }
    if(!reader_found){
        dtkWarn() << "no reader was found";
    }

    // the Current Filename has been changed
    this->setCurrentFileSetting(QString());
}


//! Write the file which is not an axl file, with the appropriate writer.
/*!
 *
 */
void axlMainWindow::fileExport(const QString& file){

    QString fileToWrite;

    if(file.isEmpty()) {

        QString rightExtension;
        QString dotExtension;
        foreach(QAction *action, d->fileExport->actions()){
            if(action->isChecked()){

                rightExtension = action->objectName();
                action->setChecked(false);

                //To obtain the extension (for instance .off)
                qDebug() << Q_FUNC_INFO << rightExtension;
                QStringList tokens = rightExtension.simplified().split(QString("(*"));
                QString interExtension = tokens.last();
                qDebug() << Q_FUNC_INFO << interExtension;
                QStringList tokens2 = interExtension.simplified().split(QString(" "));
                dotExtension = tokens2.first();
                qDebug() << Q_FUNC_INFO << dotExtension;
            }
        }
        const char * extension = rightExtension.toStdString().c_str();
        fileToWrite = QFileDialog::getSaveFileName(this, tr("Export file"), QDir::homePath(), tr(extension));

        if (fileToWrite.isEmpty()) {
            return;
        } else if (!fileToWrite.endsWith(dotExtension)) {
            fileToWrite.append(dotExtension);
        }
    } else {
        fileToWrite = file;
    }



    QList<dtkAbstractData *> dataToWrite = d->view->controller()->data();
    axlAbstractDataComposite *listData = new axlAbstractDataComposite();
    for(int i =0; i < dataToWrite.size();i++){
        listData->add(static_cast<axlAbstractData*>(dataToWrite.at(i)));
    }


    //search for the appropriate writer in the factory.
    bool writer_found = false;
    foreach(QString writer, dtkAbstractDataFactory::instance()->writers()) {
        if(!writer_found){
            dtkAbstractDataWriter *dtkwriter = dtkAbstractDataFactory::instance()->writer(writer);

            //See how to use the fact that we can know the extension.
            if(dtkwriter->canWrite(fileToWrite)){
                dtkwriter->setData(listData);
                writer_found = dtkwriter->write(fileToWrite);
            }

            //            if (writer_found && !fileToWrite.endsWith(dtkwriter->handled().at(0))) {
            //                fileToWrite.append(dtkwriter->handled().at(0));
            //            }

            delete dtkwriter;
        }
    }

    dtkWarn() << Q_FUNC_INFO << fileToWrite;

}


void axlMainWindow::fileImportCurve(const QString& file)
{
    QString fileToOpen;

    if(file.isEmpty()) {
        QSettings settings("inria", "axel");
        settings.beginGroup("axlMainWindow");
        fileToOpen = QFileDialog::getOpenFileName(this, tr("Import Curve"), settings.value("import_curve_path").toString(), tr("go document (*.g2 *.go)"));
        if(!fileToOpen.isEmpty())
        {
            QFileInfo fileInfo(fileToOpen);
            settings.setValue("import_curve_path", fileInfo.path());
        }
        settings.endGroup();
    } else {
        fileToOpen = file;
    }

    dtkAbstractData *data = dtkAbstractDataFactory::instance()->create("goCurveBSpline");
    if(axlAbstractData *axlData = qobject_cast<axlAbstractData *>(data))
    {
        if (axlData && data->read(fileToOpen)){
            axlAbstractActor *actor = d->view->insert(axlData);
            actor->data();
            d->inspector->insertData(axlData);
        }
    }

    // the Current Filename has been changed
    this->setCurrentFileSetting(QString());

}

void axlMainWindow::fileImportSurface(const QString& file)
{
    QString fileToOpen;

    if(file.isEmpty()) {
        QSettings settings("inria", "axel");
        settings.beginGroup("axlMainWindow");
        fileToOpen = QFileDialog::getOpenFileName(this, tr("Import Surface"), settings.value("import_surface_path").toString(), tr("go document (*.g2 *.go)"));
        if(!fileToOpen.isEmpty())
        {
            QFileInfo fileInfo(fileToOpen);
            settings.setValue("import_surface_path", fileInfo.path());
        }
        settings.endGroup();
    } else {
        fileToOpen = file;
    }

    dtkAbstractData *data = dtkAbstractDataFactory::instance()->create("goSurfaceBSpline");

    if(axlAbstractData *axlData = qobject_cast<axlAbstractData *>(data))
    {
        if (axlData && axlData->read(fileToOpen)){
            d->view->insert(axlData);
            d->inspector->insertData(axlData);
        }
    }

    // the Current Filaname has been changed
    this->setCurrentFileSetting(QString());
}

void axlMainWindow::fileImportOFF(const QString &file)
{
    QString fileToOpen;

    if(file.isEmpty()) {
        QSettings settings("inria", "axel");
        settings.beginGroup("axlMainWindow");
        fileToOpen = QFileDialog::getOpenFileName(this, tr("Import OFF file"), settings.value("import_off_path").toString(), tr("off document (*.OFF *.off)"));
        if(!fileToOpen.isEmpty())
        {
            QFileInfo fileInfo(fileToOpen);
            settings.setValue("import_off_path", fileInfo.path());
        }
        settings.endGroup();
    } else {
        fileToOpen = file;
    }

    bool res = false;
    axlOFFReader *reader = new axlOFFReader();
    if(reader->canRead(fileToOpen)){
        res = reader->read(fileToOpen);
    }
    if(res){
        this->onDataInserted(dynamic_cast<axlAbstractData *>(reader->data()));
    }

    // the Current Filaname has been changed
    this->setCurrentFileSetting(QString());
}

// Python script input of axl
void axlMainWindow::fileReadPythonScript(const QString &file)
{
    // firt open script interpret if not already exist then run the python script parameter
#if defined(DTK_BUILD_WRAPPERS) && defined(DTK_HAVE_PYTHON)
    this->scriptInterpreterPython();

    if(d->interpreter)
    {
        int res;
        d->interpreter->appendPlainText(d->interpreter->interpreter()->interpret(dtkReadFile(file), &res));
    }
#endif

}

void axlMainWindow::fileImportIGES(const QString& file) {
    QString fileToOpen;

    if (file.isEmpty()) {
        QSettings settings("inria", "axel");
        settings.beginGroup("axlMainWindow");
        fileToOpen = QFileDialog::getOpenFileName(this, tr("Import IGES file"), settings.value("import_iges_path").toString(), tr("IGES document (*.IGES *.IGS *.iges *.igs)"));
        if (!fileToOpen.isEmpty()) {
            QFileInfo fileInfo(fileToOpen);
            settings.setValue("import_iges_path", fileInfo.path());
        }
        settings.endGroup();
    } else {
        fileToOpen = file;
    }

    dtkAbstractDataReader* reader = dtkAbstractDataFactory::instance()->reader("CADReaderIGESDataReader");
    if (reader) {
        connect(reader, SIGNAL(dataSetInserted(QList<axlAbstractData*>)), this, SLOT(onDataSetInserted(QList<axlAbstractData*>)));
        connect(reader, SIGNAL(dataSetFieldsChanged(QList<axlAbstractData*>,QString)), this, SLOT(onFieldsInserted(QList<axlAbstractData*>,QString)));
        reader->read(fileToOpen);
        disconnect(reader, SIGNAL(dataSetInserted(QList<axlAbstractData *>)), this, SLOT(onDataSetInserted(QList<axlAbstractData *>)));
        disconnect(reader, SIGNAL(dataSetFieldsChanged(QList<axlAbstractData*>,QString)), this, SLOT(onFieldsInserted(QList<axlAbstractData*>,QString)));
    } else {
        qDebug() << "Error within axlMainWindow: you might not have installed CAD Reader plugin. Therefore, you cannot open an IGES file.";
    }

    // the Current Filaname has been changed
    this->setCurrentFileSetting(QString());
}

void axlMainWindow::onShowCurrentPoint(double u, double v, dtkAbstractData *data)
{
    d->view->actor(data)->showCurrentPoint(u, v, data);
}

void axlMainWindow::onMoveCurrentPoint(double u, double v,dtkAbstractData *data)
{
    d->view->actor(data)->moveCurrentPoint(u, v, data);
}

void axlMainWindow::onHideCurrentPoint(double u, double v, dtkAbstractData *data)
{
    d->view->actor(data)->hideCurrentPoint(u, v, data);
}

void axlMainWindow::onDataSetInserted(QList<axlAbstractData *> dataSet)
{
    d->view->insertSet(dataSet);
    d->inspector->insertDataSet(dataSet);
    ////to display field, the first one of the list, while opening file.
    int sizeList = dataSet.size();
    for(int i = 0; i < sizeList; i++){
        if(!dataSet.at(i)->fields().isEmpty()){
            QList<axlAbstractData *>a;
            axlAbstractField *field = dataSet.at(i)->fields().at(0);
            a << dataSet.at(i);
            //d->view->onUpdateActorField(a,field->name());
            this->onFieldsInserted(a, field->name());
            if(i == sizeList-1){
                d->inspector->sendFieldSignal(a, field->name());
            }

        }
    }
    d->view->update();

}

void axlMainWindow::onFieldsInserted(QList<axlAbstractData *> dataSet, QString fieldName)
{
    d->view->onUpdateActorField(dataSet, fieldName);
    d->view->update();
}

void axlMainWindow::onDataInserted(axlAbstractData *data)
{
    d->view->insert(data);
    d->inspector->insertData(data);
    d->view->update();
}

void axlMainWindow::setCurrentFileSetting(QString filename)
{
    QSettings settings("inria", "axel");
    settings.beginGroup("axlMainWindow");
    settings.setValue("currentFileName", filename);
    settings.endGroup();

    if(filename.isEmpty())
        this->setWindowTitle(d->windowTitle);
    else
        this->setWindowTitle(d->windowTitle + " " + QFileInfo(filename).fileName());

}

void axlMainWindow::fileSave(const QString &file)
{
    QSettings settings("inria", "axel");
    settings.beginGroup("axlMainWindow");
    QString fileToWrite = settings.value("currentFileName",QString("")).toString();
    settings.endGroup();

    if(fileToWrite.isEmpty()) {
        this->fileSaveAs("");
    } else {

        QList<dtkAbstractData *> dataToWrite = d->inspector->dataSet();

        if (dataToWrite.size() == 0) {
            qDebug() << " No item selected in the axlScene";
            return;
        }

        axlWriter writer;
        writer.setDataToWrite(dataToWrite);

        // prepare scene to write
        QDomDocument *doc = new QDomDocument();
        this->fileSetScene(doc);
        // set scene to writter
        writer.setSceneToWrite(doc->elementsByTagName("view").at(0).toElement());

        writer.write(fileToWrite);

        delete doc;
        doc = NULL;

        qDebug() <<fileToWrite<<" has been saved";
    }
}


void axlMainWindow::fileSaveAs(const QString &file)
{
    QString fileToWrite;

    if(file.isEmpty()) {
        fileToWrite = QFileDialog::getSaveFileName(this, tr("Save as"), QDir::homePath(), tr("Axl (*.axl)"));

        if (fileToWrite.isEmpty()) {
            return;
        } else if (!fileToWrite.endsWith(".axl")) {
            fileToWrite.append(".axl");
        }
    } else {
        fileToWrite = file;
    }

    qDebug() << fileToWrite;

    QList<dtkAbstractData *> dataToWrite = d->inspector->dataSet();

    if(dataToWrite.size() == 0){
        qDebug()<<" No item selected in the axlScene";
        return;
    }

    axlWriter writer;
    writer.setDataToWrite(dataToWrite);

    // prepare scene to write
    QDomDocument *doc = new QDomDocument();
    this->fileSetScene(doc);
    // set scene to writter
    writer.setSceneToWrite(doc->elementsByTagName("view").at(0).toElement());

    writer.write(fileToWrite);

    delete doc;
    doc = NULL;

    //save filenam as current file setting

    this->setCurrentFileSetting(fileToWrite);

    qDebug() <<fileToWrite<<" has been saved";
}

void axlMainWindow::fileSetScene(QDomDocument *doc){

    QDomElement view = doc->createElement("view");
    QDomElement camera = doc->createElement("camera");
    QDomElement position = doc->createElement("position");

    QDomElement color = doc->createElement("color");
    QString attributeStr;
    QTextStream(&attributeStr)  << QString::number(d->view->getBackgroundColor()[0]) << " "
                            << QString::number(d->view->getBackgroundColor()[1]) << " "
                            << QString::number(d->view->getBackgroundColor()[2]);
    QDomText colDomText = doc->createTextNode(attributeStr);
    color.appendChild(colDomText);

    // gradient
    QDomElement gradient = doc->createElement("gradient");
    QDomText gradientDomText = doc->createTextNode((d->view->getBackgroundGradient() ? "on":"off"));
    gradient.appendChild(gradientDomText);

    // axis
    QDomElement axis = doc->createElement("axis");
    QDomText axisDomText = doc->createTextNode((d->view->getAxesVisibility() ? "on":"off"));
    axis.appendChild(axisDomText);

    //mode
    QDomElement mode = doc->createElement("mode");
    QDomText modeDomText = doc->createTextNode((d->view->getParallelProjection() ? "parallel":"perspective"));
    mode.appendChild(modeDomText);

    // position
//    attributeStr.clear();
//    QTextStream(&attributeStr) << "x=\"" << QString::number(d->view->getWorldCameraPosition()[0]) << "\" "
//                            << "y=\"" << QString::number(d->view->getWorldCameraPosition()[1]) << "\" "
//                            << "z=\"" << QString::number(d->view->getWorldCameraPosition()[2]) << "\"";
//    QDomText posDomText = doc->createTextNode(attributeStr);
//    position.appendChild(posDomText);
    position.setAttribute("x",QString::number(d->view->getWorldCameraPosition()[0]));
    position.setAttribute("y",QString::number(d->view->getWorldCameraPosition()[1]));
    position.setAttribute("z",QString::number(d->view->getWorldCameraPosition()[2]));

    // focal point
    QDomElement focal = doc->createElement("focal");
    focal.setAttribute("x",QString::number(d->view->getCameraFocalPoint()[0]));
    focal.setAttribute("y",QString::number(d->view->getCameraFocalPoint()[1]));
    focal.setAttribute("z",QString::number(d->view->getCameraFocalPoint()[2]));

    // viewUp
    QDomElement viewUp = doc->createElement("viewUp");
    viewUp.setAttribute("x",QString::number(d->view->getCameraUp()[0]));
    viewUp.setAttribute("y",QString::number(d->view->getCameraUp()[1]));
    viewUp.setAttribute("z",QString::number(d->view->getCameraUp()[2]));

    // angle
    QDomElement viewAngle = doc->createElement("viewAngle");
    QDomText viewAngleDomText = doc->createTextNode(QString::number(d->view->getCameraViewAngle()));
    viewAngle.appendChild(viewAngleDomText);

    camera.appendChild(position);
    camera.appendChild(focal);
    camera.appendChild(viewUp);
    camera.appendChild(viewAngle);
    if (d->view->getParallelProjection()){
        // parallel scale (only for parallel projection)
        QDomElement parallelScale = doc->createElement("parallelScale");
        QDomText parallelScaleDomText = doc->createTextNode(QString::number(d->view->getParallelScale()));
        parallelScale.appendChild(parallelScaleDomText);
        camera.appendChild(parallelScale);
    }

    view.appendChild(color);
    view.appendChild(gradient);
    view.appendChild(axis);
    view.appendChild(mode);
    view.appendChild(camera);
    doc->appendChild(view);
}

void axlMainWindow::fileExportTo_OFF(const QString &file)
{
    QString fileToWrite;

    if(file.isEmpty()) {
        fileToWrite = QFileDialog::getSaveFileName(this, tr("Export to .OFF"), QDir::homePath(), tr("OFF files (*.off, *.Off, *.OFF)"));

        if (fileToWrite.isEmpty()) {
            return;
        } else if (!fileToWrite.endsWith(".off")) {
            fileToWrite.append(".off");
        }
    } else {
        fileToWrite = file;
    }

    QList<dtkAbstractData *> dataToWrite = d->view->controller()->data();

    axlWriter writer;
    writer.setDataToWrite(dataToWrite);

    if(dataToWrite.size() == 0)
        qDebug()<<" No item selected in the axlScene";
    else
        writer.exportOFF(fileToWrite);

    qDebug() << fileToWrite;
}


////////Script SLOT

void axlMainWindow::scriptInterpreterPython()
{


#if defined(DTK_BUILD_WRAPPERS) && defined(DTK_HAVE_PYTHON)
    if(!d->scriptDockWidget)
    {
        d->scriptDockWidget = new QDockWidget("Python Interpreter", this);
        this->addDockWidget(Qt::BottomDockWidgetArea, d->scriptDockWidget, Qt::Horizontal);
        d->scriptDockWidget->setStyleSheet(dtkReadFile(":axlGui/axlInspector.qss"));
        d->scriptDockWidget->setAttribute(Qt::WA_DeleteOnClose);
        this->setStyleSheet("QMainWindow::separator {background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 0,stop: 0.0 #5a5a5a, stop: 1.0 #414141);width: 1px; height: 0px;}");


        d->interpreter = new dtkInterpreter(d->scriptDockWidget);
        d->scriptDockWidget->setWidget(d->interpreter);

        { // Python interpreter dock title bar

            QLabel *pythonInterpreter = new QLabel(d->scriptDockWidget->windowTitle(), d->scriptDockWidget);
            pythonInterpreter->setStyleSheet("* {color: white;border-image: none; background-color: rgba(255,125,100,0.0) }");

            QString buttonStyleSheet("* {height:21; border-image: none; background-color: rgba(255,125,100,0.0) }");
            QPushButton *loadScript = new QPushButton(d->scriptDockWidget);
            loadScript->setToolTip("Open a script...");
            loadScript->setIcon(QIcon(QString(":axlGui/pixmaps/document_empty_64.png")));
            loadScript->setIconSize(QSize(21, 21));
            loadScript->setStyleSheet(buttonStyleSheet);

            QPushButton *saveScript = new QPushButton(d->scriptDockWidget);
            saveScript->setToolTip("Save a script...");
            saveScript->setIcon(QIcon(QString(":axlGui/pixmaps/save_64.png")));
            saveScript->setIconSize(QSize(21, 21));
            saveScript->setStyleSheet(buttonStyleSheet);

            QPushButton *floatScript = new QPushButton(d->scriptDockWidget);
            floatScript->setIcon(QIcon(QString(":axlGui/pixmaps/resize_64.png")));
            floatScript->setIconSize(QSize(21, 21));
            floatScript->setStyleSheet(buttonStyleSheet);

            QPushButton *closeScript = new QPushButton(d->scriptDockWidget);
            closeScript->setIcon(QIcon(QString(":axlGui/pixmaps/delete_64.png")));
            closeScript->setIconSize(QSize(21, 21));
            closeScript->setStyleSheet(buttonStyleSheet);


            d->scriptTitleLayout = new QBoxLayout(QBoxLayout::LeftToRight);
            d->scriptTitleLayout->setContentsMargins(5, 5, 5, 5);
            d->scriptTitleLayout->addWidget(pythonInterpreter);
            d->scriptTitleLayout->addWidget(loadScript);
            d->scriptTitleLayout->addWidget(saveScript);
            d->scriptTitleLayout->addStretch();
            d->scriptTitleLayout->addWidget(floatScript);
            d->scriptTitleLayout->addWidget(closeScript);
            d->scriptTitleLayout->setDirection((d->scriptDockWidget->features() & QDockWidget::DockWidgetVerticalTitleBar) ? QBoxLayout::TopToBottom : QBoxLayout::LeftToRight);

            QFrame *titleFrame = new QFrame(d->scriptDockWidget);
            titleFrame->setStyleSheet("background-image: none; background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0.0 #5a5a5a, stop: 1.0 #414141);border: 0px;");
            titleFrame->setLayout(d->scriptTitleLayout);

            d->scriptDockWidget->setTitleBarWidget(titleFrame);

            connect(d->scriptDockWidget, SIGNAL(featuresChanged(QDockWidget::DockWidgetFeatures)), this, SLOT(onScriptDockWidgetFeaturesChanged(QDockWidget::DockWidgetFeatures)));
            connect(closeScript, SIGNAL(clicked()), this, SLOT(onCloseDockWidget()));
            connect(loadScript, SIGNAL(clicked()), this, SLOT(onLoadScript()));
            connect(floatScript, SIGNAL(clicked()), this, SLOT(onFloatScript()));

            connect(saveScript, SIGNAL(clicked()), this, SLOT(onSaveScript()));


        }

        // end Testing

        d->interpreter->registerInterpreter(new dtkScriptInterpreterPython(d->interpreter));

        int result;

        // running import script to acces axlcore

        QString modulePath;
#if defined(Q_WS_WIN)
        modulePath = AXL_MODULE_WIN_INSTALL_PATH;
#elif defined(Q_OS_MAC) && defined(BUILD_FOR_RELEASE)
        // -
        // - This requires symlinking @executable_path/Frameworks/libaxlCore.dylib to @executable_path/Modules/_axlcore.so
        // -
        //modulePath = QString("%1/../Modules/").arg(qApp->applicationDirPath());
        modulePath = QString("/Applications/axel.app/Contents/Modules/");
#else
        modulePath = AXL_MODULE_PATH;
#endif


        d->interpreter->interpreter()->interpret("import sys",&result);
        d->interpreter->interpreter()->interpret("sys.path.append(\""+modulePath+"\")\n",&result);
        d->interpreter->interpreter()->interpret("import axlcore\n",&result);
        d->interpreter->interpreter()->interpret("from axlcore import *\n",&result);
        d->interpreter->interpreter()->interpret("import axel\n",&result);
        d->interpreter->interpreter()->interpret("from axel import *\n",&result);
        d->interpreter->interpreter()->interpret("dtkDataFactory = axlcore.dtkAbstractDataFactory.instance()",&result);
        d->interpreter->interpreter()->interpret("dtkProcessFactory = axlcore.dtkAbstractProcessFactory.instance()",&result);
        d->interpreter->interpreter()->interpret("dtkVtkView = axlcore.dtkAbstractViewFactory.instance().view(\"vtkView0\")",&result);
        d->interpreter->interpreter()->interpret("axlView = axlcore.to_axl_view(dtkVtkView)", &result);
    }

#else
    qDebug()<< "Sorry cannot open Python Interpreter";
#endif

}




// Others

void axlMainWindow::onCloseDockWidget()
{
#if defined(DTK_BUILD_WRAPPERS) && defined(DTK_HAVE_PYTHON)
    d->scriptDockWidget->close();
    d->scriptDockWidget = NULL;

#endif
}

void axlMainWindow::onFloatScript()
{
#if defined(DTK_BUILD_WRAPPERS) && defined(DTK_HAVE_PYTHON)
    if(d->interpreter)
    {
        if(d->scriptDockWidget->isFloating())
            d->scriptDockWidget->setFloating(false);
        else
            d->scriptDockWidget->setFloating(true);
    }

#endif
}

void axlMainWindow::onLoadScript()
{
#if defined(DTK_BUILD_WRAPPERS) && defined(DTK_HAVE_PYTHON)
    if(d->interpreter)
    {
        int res;
        QString scripToOpen = dtkReadFile(QFileDialog::getOpenFileName(this, tr("Open Script")));
        d->interpreter->appendPlainText(d->interpreter->interpreter()->interpret(scripToOpen, &res));

    }
#endif
}

void axlMainWindow::onSaveScript()
{
#if defined(DTK_BUILD_WRAPPERS) && defined(DTK_HAVE_PYTHON)
    if(d->interpreter)
    {
        int res;
        QString scripToSave = d->interpreter->toPlainText();
        //qDebug() << "axlMainWindow::onSaveScript() "<<scripToSave;
        scripToSave.replace("$ ","");
        QString fileToSave = QFileDialog::getSaveFileName(this, tr("Save Script as"), QDir::homePath());

        QFile file(fileToSave);
        file.open(QIODevice::WriteOnly | QIODevice::Text);

        QTextStream out(&file);
        out << scripToSave;
    }
#endif
}

void axlMainWindow::onScriptDockWidgetFeaturesChanged(QDockWidget::DockWidgetFeatures)
{
#if defined(DTK_BUILD_WRAPPERS) && defined(DTK_HAVE_PYTHON)
    d->scriptTitleLayout->setDirection((d->scriptDockWidget->features() & QDockWidget::DockWidgetVerticalTitleBar) ? QBoxLayout::TopToBottom : QBoxLayout::LeftToRight);
#endif
}

int axlMainWindow::runMySelf()
{
    int argc = 0;
    char* argv[] = {""};

    QApplication *app = new QApplication(argc, argv);
    app->setApplicationName("Axl");
    app->setApplicationVersion(AXL_VERSION);
    app->setOrganizationName("inria");
    app->setOrganizationDomain("fr");

    axlMainWindow* window = new axlMainWindow;
    window->show();
    window->raise();

    //int status =
    app->exec();

    delete window;
    delete app;

    return 0;

}



void axlMainWindow::onBackGroundColorSelected(QColor color)
{
    d->view->setBackgroundColor(color.redF(), color.greenF(), color.blueF());
}

void axlMainWindow::showInspector(void)
{
    d->inspector->setVisible(!d->inspector->isVisible());
}

void axlMainWindow::switchFullNormalScreen(void)
{
    if(!this->isFullScreen())
        this->showFullScreen();
    else
        this->showNormal();
}

void axlMainWindow::closeEvent(QCloseEvent *event)
{
    this->writeSettings();

    event->accept();
}


axlAbstractView * axlMainWindow::view(void){
    return d->view;

}

axlInspector *axlMainWindow::inspector(void){
    return d->inspector;
}
