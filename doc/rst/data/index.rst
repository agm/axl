.. highlight:: xml

=================
Geometric objects
=================

The geometric objects of ``Axl`` can be vizualized in the view,
manipulated by the interactors of the view or by the data manager.
They share the following attributes, which may appear optionnaly in their description:

    - name (eg. name="P1") which specifies the name used for the object;
    - color (e.g. color="255 0 0 0.5" which specifies the color of the object in red-green-blue enconding (here it is red) and its opacity (a floating point value between 0 and 1).
    - shader which specifies the name of the shader applied to the object.
    - size which describes the pixel size of the curve when this is applicable.

Different types of geometric objects can be used in Axl. They are stored in xml format, with a node indicating their type. Here is an example::

  <myobj name="o1" size="0.1" color="255 0 0 0.5">
  ...
  </myobj>

It is defining an object of type ``myobj`` named "o1", of size 0.1, of red color and transparency 0.5.
If the shader attribute is used, then the color and transparency values are not taken into account.

The following types of objects can be created and manipulated directly in Axl:

.. toctree::
   :maxdepth: 1 

   point.rst
   line.rst
   plane.rst
   circle_arc.rst
   sphere.rst 
   cylinder.rst
   cone.rst
   ellipsoid.rst
   torus.rst 
   mesh.rst

Spline curves, surfaces, volumes
================================
The following objects can be manipulated with the plugin BSplineTools.

- B-spline curves
- B-spline surfaces
- B-spline volumes
  
Algebraic curves and surfaces
=============================
The plugin SemiAlgebraicTools provides the following types of objects:

- `algebraic curves in the 2-dimensional space <http://axl.inria.fr/axel_data/76>`_ defined by one polynomial equation,
- `algebraic curves in the 3-dimensional space <http://axl.inria.fr/axel_data/89>`_ defined by two polynomial equations,
- `algebraic surfaces <http://axl.inria.fr/axel_data/87>`_ in the 3-dimensional space defined by on polynomial equation.




   




