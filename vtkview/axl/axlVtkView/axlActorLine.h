/* axlActorLine.h ---
 *
 * Author: Meriadeg Perrinel
 * Copyright (C) 2008 - Meriadeg Perrinel, Inria.
 * Created: Fri Dec 17 10:58:38 2010 (+0100)
 * Version: $Id$
 * Last-Updated: Mon Dec 17 14:11:14 2012 (+0100)
 *           By: Julien Wintz
 *     Update #: 7
 */

/* Commentary:
 *
 */

/* Change log:
 *
 */

#ifndef AXLACTORLINE_H
#define AXLACTORLINE_H

#include "axlActor.h"

#include <QVTKOpenGLWidget.h>

#include "axlVtkViewPluginExport.h"

class axlLine;

class vtkLineSource;

class axlActorLinePrivate;

class AXLVTKVIEWPLUGIN_EXPORT axlActorLine : public axlActor
{
public:
#if (VTK_MAJOR_VERSION <= 5)
    vtkTypeRevisionMacro(axlActorLine, vtkAssembly);
#endif

    static axlActorLine *New(void);

public:
   dtkAbstractData *data(void);
   vtkLineSource *line(void);

public:
    void setDisplay(bool display);
    void showLineWidget(bool show);
    void setLineWidget(bool lineWidget);
    bool isShowLineWidget(void);
    virtual void setData(dtkAbstractData *line1);
    void setMode(int state);
    void setQVTKWidget(QVTKOpenGLWidget *widget);
    void setSize(double size);


public slots:
    void update(void);
    void onModeChanged(int state);
    void onRemoved();
    void onUpdateGeometry();

protected:
     axlActorLine(void);
    ~axlActorLine(void);

private:
    axlActorLine(const axlActorLine&); // Not implemented.
    void operator = (const axlActorLine&); // Not implemented.

private:
    axlActorLinePrivate *d;
};

// /////////////////////////////////////////////////////////////////
// Type instanciation
// /////////////////////////////////////////////////////////////////

axlAbstractActor *createAxlActorLine(void);



#endif //AXLACTORLINE_H
