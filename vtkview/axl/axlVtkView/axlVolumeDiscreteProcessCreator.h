/* axlVolumeDiscreteProcessCreator.h ---
 *
 * Author: Anais Ducoffe
 * Copyright (C) 2013 - Anais Ducoffe, Inria.
 */

/* Commentary:
 *
 */

/* Change log:
 *
 */

#ifndef AXLVOLUMEDISCRETEPROCESSCREATOR_H
#define AXLVOLUMEDISCRETEPROCESSCREATOR_H



#include "axlVtkViewPluginExport.h"
#include <axlCore/axlAbstractCreatorProcess.h>
#include <dtkCoreSupport/dtkAbstractProcess.h>

class axlVolumeDiscreteProcessCreatorPrivate;

// ///////////////////////////////////////////////////////////////////
// axlVolume declaration
// ///////////////////////////////////////////////////////////////////

class AXLVTKVIEWPLUGIN_EXPORT axlVolumeDiscreteProcessCreator : public axlAbstractCreatorProcess
{
    Q_OBJECT

public:
    axlVolumeDiscreteProcessCreator(void);

public:
    ~axlVolumeDiscreteProcessCreator(void);

public :
    //virtual axlAbstractData * getInput(int channel) const;
    void copyProcess(axlAbstractProcess *process);
    bool hasParameters(void);

    //public:
    //    double equationToValue(int i, int j, int k);


    /* dtk overload */
public :
    virtual int update(void);

public :

    virtual QString description(void) const;
    virtual QString identifier(void) const;


public :
    static bool registered(void);


    QString form(void) const
    {
        return QString(
                    "INPUT 0 data implicit_surface:\n"
                    "PARAMETER 0 int xDimension: 10\n"
                    "PARAMETER 1 int yDimension: 10\n"
                    "PARAMETER 2 int zDimension: 10\n"
                    "OUTPUT 0 volume"
                    );
    }

public :
    virtual void setInput(dtkAbstractData *newData, int channel);
    virtual void setParameter(int value, int channel);
    virtual dtkAbstractData *output(void);



private:
    axlVolumeDiscreteProcessCreatorPrivate *d;
};

// /////////////////////////////////////////////////////////////////
// Instanciation function
// /////////////////////////////////////////////////////////////////

AXLVTKVIEWPLUGIN_EXPORT dtkAbstractProcess *createaxlVolumeDiscreteProcessCreator(void);

#endif // AXLVOLUMEDISCRETEPROCESSCREATOR_H
