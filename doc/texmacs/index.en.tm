<TeXmacs|1.99.1>

<style|<tuple|mmxdoc|mathemagix|doxygen>>

<\body>
  \;

  <doc-data|<doc-title|AXL>>

  <\strong>
    <section|How to use Axl>
  </strong>

  <\itemize>
    <item><hlink|The basic objects of <name|Axl>|axel-data.en.tm>

    <item>The processes of <name|Axl>

    <item>Fields on surfaces and volumes
  </itemize>

  <section|How to develop Axl>

  <\itemize>
    <item><hlink|The structure of a plugin|plugins.en.tm>

    <item><hlink|How to generate a dialog interface for a
    process.|process-form.en.tm>
  </itemize>
</body>