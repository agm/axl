/* axlActorCircleArc.h ---
 *
 * Author: Valentin Michelet
 * Copyright (C) 2008 - Valentin Michelet, Inria.
 * Created: Tue Nov  9 16:58:59 2010 (+0100)
 * Version: $Id$
 * Last-Updated: Mon Dec  6 15:46:18 2010 (+0100)
 *           By: Valentin Michelet
 *     Update #: 21
 */

/* Commentary:
 *
 */

/* Change log:
 *
 */

#ifndef AXLACTORCIRCLEARC_H
#define AXLACTORCIRCLEARC_H

#include "axlActor.h"
#include <QVTKOpenGLWidget.h>

#include "vtkViewPluginExport.h"

class axlCircleArc;

class vtkArcSource;

class axlActorCircleArcPrivate;

class VTKVIEWPLUGIN_EXPORT axlActorCircleArc : public axlActor {

public:
    vtkTypeRevisionMacro(axlActorCircleArc, vtkAssembly);
    static axlActorCircleArc *New(void);

    dtkAbstractData *data(void);
    vtkArcSource *arc(void);

    void setDisplay(bool display);
    void showCircleArcWidget(bool show);
    void setCircleArcWidget(bool arcWidget);
    bool isShowCircleArcWidget(void);
    void setCircleArc(axlCircleArc *arc);
    void setMode(int state);
    void setQVTKWidget(QVTKOpenGLWidget *widget);

public slots:
    void onModeChanged(int state);
    void onRemoved();
    void onUpdateGeometry();

protected:
     axlActorCircleArc(void);
    ~axlActorCircleArc(void);

private:
    axlActorCircleArc(const axlActorCircleArc&); // Not implemented.
    void operator = (const axlActorCircleArc&); // Not implemented.

private:
    axlActorCircleArcPrivate *d;
};

#endif // AXLACTORCIRCLEARC_H
