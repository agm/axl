/* axlFieldSpatialCoordinatesWriter.h ---
 *
 * Author: Anais Ducoffe
 * Copyright (C) 2013 - Anais Ducoffe, Inria.
 */

/* Commentary:
 *
 */

/* Change log:
 *
 */

#ifndef AXLFIELDSPATIALCOORDINATESWRITER_H
#define AXLFIELDSPATIALCOORDINATESWRITER_H

#include <axlCore/axlAbstractDataWriter.h>
#include "axlCoreExport.h"


class dtkAbstractData;

class AXLCORE_EXPORT axlFieldSpatialCoordinatesWriter : public axlAbstractDataWriter
{
    Q_OBJECT

public :
      axlFieldSpatialCoordinatesWriter(void);
     ~axlFieldSpatialCoordinatesWriter(void);

public:
    QString identifier(void) const;
    QString description(void) const;
    QStringList handled(void) const;

    static bool registered(void);

public:
    bool accept(dtkAbstractData *data);
    bool reject(dtkAbstractData *data);

    QDomElement write(QDomDocument *doc, dtkAbstractData *data);

private :
    QDomElement elementByWriter(axlAbstractDataWriter *axl_writer, QDomDocument *doc, dtkAbstractData *data);
};

dtkAbstractDataWriter *createaxlFieldSpatialCoordinatesWriter(void);

#endif  // AXLFIELDSPATIALCOORDINATESWRITER_H
