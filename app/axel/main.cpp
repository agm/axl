/* main.cpp ---
 *
 * Author: Julien Wintz
 * Copyright (C) 2008 - Julien Wintz, Inria.
 * Created: Tue Nov  9 16:48:53 2010 (+0100)
 * Version: $Id$
 * Last-Updated: Wed Nov  9 11:10:40 2011 (+0100)
 *           By: Julien Wintz
 *     Update #: 58
 */

/* Commentary:
 *
 */

/* Change log:
 *
 */

#include <QtCore>
#include <QtGui>
#include <QtDebug>
#include <QtOpenGL>

#include "axlMainWindow.h"

#include <axlCore/axlAbstractView.h>
#include <dtkCoreSupport/dtkGlobal.h>
#include <dtkCoreSupport/dtkPluginManager.h>

//factories
#include <dtkCoreSupport/dtkAbstractProcessFactory.h>
#include <dtkCoreSupport/dtkAbstractDataFactory.h>
#include <dtkCoreSupport/dtkAbstractDataReader.h>
#include <axlCore/axlFactoryRegister.h>
#include <axlGui/axlInspectorToolFactory.h>

#if defined(JSON_RPC)
#include <axlRpc/axlServer.h>
#endif


int main(int argc, char **argv)
{
    QApplication application(argc, argv);
    application.setApplicationName("axel");
    application.setApplicationVersion(AXL_VERSION);
    application.setOrganizationName("inria");
    application.setOrganizationDomain("fr");
#ifdef Q_WS_WIN
    application.setWindowIcon(QIcon(":axel/axel.ico"));
#endif


    if (application.arguments().contains("--help")) {
        qDebug() << "Usage: axel [--stereo < ANAGLYPHIC | QUAD_BUFFER | HORIZONTAL_SPLIT | HORIZONTAL_INTERLACE > ] [--verbose] [-c] [-s] [-i] [--igs] [--off] [file.axl] [file.g2] [file.go] [file.off] [-py]";
        return 1;
    }

    if (application.arguments().contains("--version")) {
        qDebug() << "Axl" << AXL_VERSION;
        return 1;
    }

    //Q_INIT_RESOURCE(axel);

    if(qApp->arguments().contains("--stereo")) {
      QGLFormat format;
      format.setStereo(true);
      format.setDirectRendering(true);
      QGLFormat::setDefaultFormat(format);
    }

    if (application.arguments().contains("--verbose")) {
        dtkPluginManager::instance()->setVerboseLoading(true);
        dtkLogger::instance().setLevel("trace");

        dtkLogger::instance().attachConsole();
    }


    //test for linux system and Mac OS.
//#if defined(Q_WS_WIN)
//    #if defined(BUILD_FOR_RELEASE)
//        QString pathPlugins  = QString("C:/Program\ Files\ (x86)/axel-2.3.2/bin/");
//    #else
//        QString pathPlugins  = qApp->applicationDirPath();
//    #endif
//#elif defined(Q_OS_MAC)
#if defined(Q_OS_MAC)
    QString pathPlugins  = QString("%1/../PlugIns/").arg(qApp->applicationDirPath());
#else
    QString pathPlugins  = QString("%1/../plugins/").arg(qApp->applicationDirPath());
#endif
    dtkPluginManager::instance()->setPath(pathPlugins);
    //Initialized all factories
    dtkPluginManager::instance()->initialize();
    axlFactoryRegister::initialized();
    axlInspectorToolFactory::instance()->initialize();

    dtkLogger::instance().setLevel("info");

    axlMainWindow* window = new axlMainWindow;
    window->show();
    window->raise();


    foreach(QString file, application.arguments()) {
        if(file.endsWith(".axl")){
            window->fileOpen(file);
        }else if(file.endsWith(".py")){
            window->fileReadPythonScript(file);
        }else if(!file.isEmpty() && !file.endsWith("/axel")){
            window->fileImport(file);
        }
    }

#if defined(JSON_RPC)
    axlServer *server = new axlServer();
    server->setView(window->view());
    server->setInspector(window->inspector());
#endif

    int status = application.exec();


#if defined(JSON_RPC)
    delete server;
#endif
    delete window;

    dtkPluginManager::instance()->uninitialize();

    return status;
}
