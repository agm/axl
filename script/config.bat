cd C:\builds\workspace\axel-sdk-windows\
if exist "C:\builds\workspace\axel-sdk-windows\dtk" "C:\Program Files (x86)\Git\bin\sh.exe" --login -i -c "rm -rf dtk"
if exist "C:\builds\workspace\axel-sdk-windows\axel" "C:\Program Files (x86)\Git\bin\sh.exe" --login -i -c "rm -rf axel"
if exist "C:\builds\workspace\axel-sdk-windows\vtkview" "C:\Program Files (x86)\Git\bin\sh.exe" --login -i -c "rm -rf vtkview"
"C:\Program Files (x86)\Git\bin\sh.exe" --login -i -c "git clone git://dtk.inria.fr/axel/axel.git"
"C:\Program Files (x86)\Git\bin\sh.exe" --login -i -c "git clone https://github.com/d-tk/dtk.git dtk"
"C:\Program Files (x86)\Git\bin\sh.exe" --login -i -c "git clone git://dtk.inria.fr/axel/vtkview.git"
