.. highlight:: xml

Torus
=====
A torus is defined by a center (axlPoint), a normal (axlPoint) to the plane of the big circle, a ring radius (double) and a cross-section radius (double).

Type: ``axlTorus``

Format:

.. literalinclude:: ../../../data/torus.axl
   :lines: 2-7
   :language: xml
	      
.. image:: ../img/torus.png
   :height: 100px
   :align: center
