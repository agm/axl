/* axlControlPointsWidget.cpp ---
 *
 * Author: Meriadeg Perrinel
 * Copyright (C) 2008 - Meriadeg Perrinel, Inria.
 * Created: Fri Feb 18 17:29:04 2011 (+0100)
 * Version: $Id$
 * Last-Updated: Mon Dec 17 14:40:31 2012 (+0100)
 *           By: Julien Wintz
 *     Update #: 130
 */

/* Commentary:
 *
 */

/* Change log:
 *
 */

#include "axlActorCurveBSpline.h"
#include "axlActorSurfaceBSpline.h"
#include "axlActorShapeBSpline.h"
#include "axlControlPointsWidget.h"
#include "axlInteractorStyleRubberBandPick.h"

#include <axlCore/axlPoint.h>
#include <axlCore/axlAbstractCurveBSpline.h>
#include <axlCore/axlAbstractSurfaceBSpline.h>
#include <axlCore/axlAbstractVolumeBSpline.h>
#include <axlCore/axlShapeBSpline.h>


#include <vtkActor.h>
#include <vtkAssemblyNode.h>
#include <vtkAssemblyPath.h>
#include <vtkCallbackCommand.h>
#include <vtkCamera.h>
#include <vtkCellArray.h>
#include <vtkCellPicker.h>
#include <vtkAreaPicker.h>
#include <vtkDoubleArray.h>
#include <vtkFloatArray.h>
#include <vtkLine.h>
#include <vtkObjectFactory.h>
#include <vtkPlanes.h>
#include <vtkPoints.h>
#include <vtkPolyData.h>
#include <vtkPolyDataMapper.h>
#include <vtkDataSetMapper.h>
#include <vtkMapper.h>
#include <vtkUnstructuredGrid.h>
#include <vtkDataSetMapper.h>
#include <vtkProperty.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkRenderer.h>
#include <vtkRendererCollection.h>
#include <vtkRenderWindow.h>
#include <vtkSphereSource.h>
#include <vtkTransform.h>
#include <vtkGlyph3D.h>
#include <vtkSmartPointer.h>
#include <vtkTransform.h>
#include <vtkCursor3D.h>
#include <vtkProp3DCollection.h>
#include <vtkProp.h>
#include <vtkPropAssembly.h>
#include <vtkCollectionIterator.h>
#include <vtkCommand.h>

#include <vtkRenderWindow.h>

#include <vtkDistanceToCamera.h>
#include <vtkArrowSource.h>
#include <vtkPointSource.h>

vtkStandardNewMacro(axlControlPointsWidget);

class axlControlPointsWidgetPrivate
{
public:

    vtkActor          **CpActor;
    vtkPolyDataMapper **CpMapper;
    vtkDataSetMapper ** CpMapper3D;
    vtkSphereSource   **CpSphere;

    vtkActor          *CpCurrentActor;
    int               CpCurrentActorIndex;
    vtkPoints         *CpPoints;  //used by others as well
    vtkCellPicker     *CpCursorPicker;
    vtkAreaPicker     *CpRectanglePicker;
    axlInteractorStyleRubberBandPick *CpRubberBanPick;
    vtkProperty       *CpHandleProperty;
    vtkProperty       *CpSelectedHandleProperty;
    dtkAbstractData   *CpSpline;

    vtkAssemblyNode   *CpCurrentAssemblyNode;

    vtkActorCollection *CpActorCollection;

    // Number of control points
    int                numCps;
    // control points size scale
    double             radiusScaleCps;

    // rectangle behavior
    int                XStartPosition;
    int                YStartPosition;

    //lines between points
    vtkCellArray      *CpCellAray;
    vtkPolyData       *CpPolyData;
    vtkPolyDataMapper *CpPolyDataMapper;
    vtkUnstructuredGrid *CpUnstructuredGrid;
    vtkDataSetMapper *CpDataSetMapper;
    vtkActor          *CpPolyDataActor;

    //the 3dCursor
    vtkPolyDataMapper *CpCursorPolyDataMapper;
    vtkActor          *CpCursorActor;

public:

    ~axlControlPointsWidgetPrivate()
    {

        CpPoints->Delete();
        CpCellAray->Delete();
        CpPolyData->Delete();
        CpUnstructuredGrid->Delete();

        CpPolyDataMapper->Delete();

        CpPolyDataActor->Delete();
        CpCursorPicker->Delete();
        CpRectanglePicker->Delete();

        CpActorCollection->InitTraversal();
        vtkObject * o = CpActorCollection->GetNextItem();
        int k = 0;// .. to numCps
        while(o!=NULL)
        {
            CpSphere[k]->Delete();
            CpActor[k]->Delete();
            //CpMapper3D[k]->Delete();// ! only for volumes

            ++k;// Next
            o = CpActorCollection->GetNextItem();
        }

        CpActorCollection->Delete();

        // delete CpActor;
        // delete CpMapper3D;
        // delete CpSphere;
    }

};

axlControlPointsWidget::axlControlPointsWidget() :d(new axlControlPointsWidgetPrivate)
{
    this->State = axlControlPointsWidget::Start;
    this->EventCallbackCommand->SetCallback(axlControlPointsWidget::ProcessEvents);
    // Set up the initial properties
    this->CreateDefaultProperties();

    // connection for the cursor
    d->CpCursorPolyDataMapper = vtkPolyDataMapper::New();
    d->CpCursorActor = vtkActor::New();
    d->CpCursorActor->VisibilityOff();
    d->CpCursorActor->SetMapper(d->CpCursorPolyDataMapper);


    // Define the point coordinates
    double bounds[6];
    bounds[0] = -0.5;
    bounds[1] = 0.5;
    bounds[2] = -0.5;
    bounds[3] = 0.5;
    bounds[4] = -0.5;
    bounds[5] = 0.5;

    d->XStartPosition =-1;
    d->YStartPosition =-1;

    this->PlaceWidget(bounds);
}

axlControlPointsWidget::~axlControlPointsWidget()
{
    delete d;

    d = NULL;
}

void axlControlPointsWidget::setSpline(dtkAbstractData *spline)
{
    d->CpSpline = spline;
    d->numCps   = 0;
    d->radiusScaleCps = 1.15;
    if(axlAbstractCurveBSpline *spl = dynamic_cast<axlAbstractCurveBSpline *>(d->CpSpline))
    {
        d->numCps = spl->countControlPoints();
    }
    if(axlAbstractSurfaceBSpline *spl = dynamic_cast<axlAbstractSurfaceBSpline *>(d->CpSpline))
    {
        d->numCps = spl->countControlPoints();
    }
    if(axlAbstractVolumeBSpline *spl= dynamic_cast<axlAbstractVolumeBSpline *>(d->CpSpline))
    {
        d->numCps = spl->countControlPoints();
    }
    if(axlShapeBSpline *spl= dynamic_cast<axlShapeBSpline *>(d->CpSpline))
    {
        d->numCps = spl->countControlPoints();
    }
}

void axlControlPointsWidget::initializePoints()
{
    d->CpCurrentActor = NULL;

    // construct initial points CellAray and PolyData or UnstructuredGrid
    d->CpPoints = vtkPoints::New();
    d->CpCellAray = vtkCellArray::New();
    d->CpPolyData = vtkPolyData::New();
    d->CpUnstructuredGrid = vtkUnstructuredGrid::New();

    // connection for the line
    d->CpPolyDataMapper = vtkPolyDataMapper::New();
#if (VTK_MAJOR_VERSION <= 5)
    d->CpPolyDataMapper->SetInput(d->CpPolyData);
#else
    d->CpPolyDataMapper->SetInputData(d->CpPolyData);
#endif
    d->CpDataSetMapper = vtkDataSetMapper::New();
#if (VTK_MAJOR_VERSION <= 5)
    d->CpDataSetMapper->SetInput(d->CpUnstructuredGrid);
#else
    d->CpDataSetMapper->SetInputData(d->CpUnstructuredGrid);
#endif
    d->CpPolyDataActor = vtkActor::New();
    if(dynamic_cast<axlAbstractVolumeBSpline*> (d->CpSpline))
    {
        d->CpPolyDataActor->SetMapper(d->CpDataSetMapper);
    }else {
        d->CpPolyDataActor->SetMapper(d->CpPolyDataMapper);
    }
    d->CpPolyDataActor->SetPickable(0);

    //Manage the picking stuff
    d->CpCursorPicker = vtkCellPicker::New();
    d->CpRectanglePicker = vtkAreaPicker::New();
    //d->CpRubberBanPick  = dynamic_cast<axlInteractorStyleRubberBandPick *>(this->Interactor->GetInteractorStyle());//axlInteractorStyleRubberBandPick::New();
    this->Interactor->SetPicker(d->CpRectanglePicker);

    //compute side of controlPoints
    double bounds[6];this->Prop3D->GetBounds(bounds);
    double side = qAbs(bounds[1]-bounds[0]);
    side += qAbs(bounds[3]-bounds[2]);
    side += qAbs(bounds[5]-bounds[4]);

    side/=600;

    // Storage of the control Points depend of the kind of Spline
    if(axlAbstractCurveBSpline *splineCurve = dynamic_cast<axlAbstractCurveBSpline *>(d->CpSpline))
    {
        d->CpMapper = new vtkPolyDataMapper *[splineCurve->numCoefs()];
        d->CpSphere = new vtkSphereSource *[splineCurve->numCoefs()];
        d->CpActor = new vtkActor *[splineCurve->numCoefs()];
        d->CpActorCollection = vtkActorCollection::New();

        side = (((splineCurve->size()*d->radiusScaleCps)>side) ? (splineCurve->size()*d->radiusScaleCps):side);

        for (int i = 0; i < splineCurve->numCoefs(); i++)
        {
            //storage of points
            axlPoint pointCourant = splineCurve->getCoef(i + 1);
            d->CpPoints->InsertNextPoint(pointCourant.x(), pointCourant.y(), pointCourant.z());

            // Construct the poly data representing control points
            d->CpMapper[i] = vtkPolyDataMapper::New();
            d->CpSphere[i] =vtkSphereSource::New();
            d->CpSphere[i]->SetPhiResolution(15);
            d->CpSphere[i]->SetThetaResolution(15);
            d->CpSphere[i]->SetRadius(side);
            d->CpSphere[i]->SetCenter(d->CpPoints->GetPoint(i));
#if (VTK_MAJOR_VERSION <= 5)
            d->CpMapper[i]->SetInput( d->CpSphere[i]->GetOutput());
#else
            d->CpMapper[i]->SetInputData( d->CpSphere[i]->GetOutput());
#endif
            d->CpActor[i] = vtkActor::New();
            d->CpActor[i]->SetMapper(d->CpMapper[i]);
            d->CpActor[i]->VisibilityOn();

            d->CpSphere[i]->Modified();
            d->CpSphere[i]->Update();

            ////----------------------------------------------------------------------------------------------------
            //// add 04 february 2014
            //// example from http://www.vtk.org/Wiki/VTK/Examples/Cxx/Visualization/DistanceToCamera

//            //Calculate the distance to the camera of each CpSphere.
//            vtkSmartPointer<vtkDistanceToCamera> distanceToCamera = vtkSmartPointer<
//                    vtkDistanceToCamera>::New();
//            distanceToCamera->SetInputConnection(d->CpSphere[i]->GetOutputPort());
//            distanceToCamera->SetScreenSize(10.0);


//            // Glyph each point with a sphere.
//            vtkSmartPointer<vtkSphereSource> spheres =
//                    vtkSmartPointer<vtkSphereSource>::New();
//            vtkSmartPointer<vtkGlyph3D> fixedGlyph = vtkSmartPointer<vtkGlyph3D>::New();
//            fixedGlyph->SetInputConnection(distanceToCamera->GetOutputPort());
//            fixedGlyph->SetSourceConnection(spheres->GetOutputPort());

//            // Scale each point.
//            fixedGlyph->SetScaleModeToScaleByScalar();
//            fixedGlyph->SetInputArrayToProcess(0, 0, 0,
//                                               vtkDataObject::FIELD_ASSOCIATION_POINTS, "DistanceToCamera");

//            // Create a mapper.
//            vtkSmartPointer<vtkPolyDataMapper> fixedMapper = vtkSmartPointer<
//                    vtkPolyDataMapper>::New();
//            fixedMapper->SetInputConnection(fixedGlyph->GetOutputPort());
//            fixedMapper->SetScalarVisibility(false);

//            //create a renderer if NULL
//            if(!this->CurrentRenderer){
//                this->SetCurrentRenderer(this->Interactor->FindPokedRenderer(
//                                             this->Interactor->GetLastEventPosition()[0],
//                                             this->Interactor->GetLastEventPosition()[1]));
//            }
//            distanceToCamera->SetRenderer(this->CurrentRenderer);

//            d->CpActor[i]->SetMapper(fixedMapper);
            ////----------------------------------------------------------------------------------------------------

            d->CpActorCollection->AddItem(d->CpActor[i]);


        }

        //Here are made the line connection, there is one and only way to connect points together. We can just connect neighbors.
        //That's what we will have to change
        //        for(int i = 0; i < splineCurve->numCoefs() - 1; i++)
        //        {
        //            vtkSmartPointer<vtkLine> currentline = vtkSmartPointer<vtkLine>::New();
        //            currentline->GetPointIds()->SetId(0, i);
        //            currentline->GetPointIds()->SetId(1, i + 1);
        //            d->CpCellAray->InsertNextCell(currentline);
        //        }
        for(int i = 0; i < splineCurve->numCoefs() - 1; i++)
        {
            if(splineCurve->connectionsAreDefined()){
                QList<int> connections = splineCurve->getControlPointConnection(i);
                foreach(int value, connections){
                    vtkSmartPointer<vtkLine> currentline = vtkSmartPointer<vtkLine>::New();
                    currentline->GetPointIds()->SetId(0, i);
                    currentline->GetPointIds()->SetId(1, value);
                    d->CpCellAray->InsertNextCell(currentline);
                }
            }else{
                vtkSmartPointer<vtkLine> currentline = vtkSmartPointer<vtkLine>::New();
                currentline->GetPointIds()->SetId(0, i);
                currentline->GetPointIds()->SetId(1, i + 1);
                d->CpCellAray->InsertNextCell(currentline);
            }
        }

        // association of the glyph and the control Points
        d->CpPolyData->SetPoints(d->CpPoints);
        d->CpPolyData->SetLines(d->CpCellAray);
    }

    if(axlAbstractSurfaceBSpline *splineSurface =dynamic_cast<axlAbstractSurfaceBSpline *>(d->CpSpline))
    {
        d->CpMapper = new vtkPolyDataMapper *[d->numCps];
        d->CpSphere = new vtkSphereSource *[d->numCps];
        d->CpActor = new vtkActor *[d->numCps];
        d->CpActorCollection = vtkActorCollection::New();

        //        for(int i = 0; i < splineSurface->countControlPoints_v(); i++)
        //        {
        //            for(int j = 0; j < nbCoeffs_u; j++)
        //            {
        //                axlPoint pointCourant = splineSurface->getCoef(j + 1, i + 1);
        //                d->CpPoints->InsertNextPoint(pointCourant.x(), pointCourant.y(), pointCourant.z());

        //                // Construct the poly data representing control points
        //                d->CpMapper[i * nbCoeffs_u + j] = vtkPolyDataMapper::New();
        //                d->CpSphere[i * nbCoeffs_u + j] =vtkSphereSource::New();
        //                d->CpSphere[i * nbCoeffs_u + j]->SetPhiResolution(15);
        //                d->CpSphere[i * nbCoeffs_u + j]->SetThetaResolution(15);
        //                d->CpSphere[i * nbCoeffs_u + j]->SetRadius(side);
        //                d->CpSphere[i * nbCoeffs_u + j]->SetCenter(d->CpPoints->GetPoint(i * nbCoeffs_u + j));
        //#if (VTK_MAJOR_VERSION <= 5)
        //                d->CpMapper[i * nbCoeffs_u + j]->SetInput( d->CpSphere[i * nbCoeffs_u + j]->GetOutput());
        //#else
        //                d->CpMapper[i * nbCoeffs_u + j]->SetInputData( d->CpSphere[i * nbCoeffs_u + j]->GetOutput());
        //#endif
        //                d->CpActor[i * nbCoeffs_u + j] = vtkActor::New();
        //                d->CpActor[i * nbCoeffs_u + j]->SetMapper(d->CpMapper[i * nbCoeffs_u + j]);
        //                d->CpActor[i * nbCoeffs_u + j]->VisibilityOn();
        //                // d->CpCursorPicker->AddPickList(d->CpActor[i*nbCoeffs_u+j]);
        //                // d->CpRectanglePicker->AddPickList(d->CpActor[i*nbCoeffs_u+j]);
        //                d->CpCursorPicker->SetTolerance(0.001); //need some fluff

        //                d->CpActorCollection->AddItem(d->CpActor[i * nbCoeffs_u + j]);
        //            }
        //        }

        side = (splineSurface->size()>side ? splineSurface->size():side);

        //To generalize.
        for(int i = 0; i < d->numCps; i++)
        {

            axlPoint pointCourant = splineSurface->getCoef(i+1);
            d->CpPoints->InsertNextPoint(pointCourant.x(), pointCourant.y(), pointCourant.z());

            // Construct the poly data representing control points
            d->CpMapper[i] = vtkPolyDataMapper::New();
            d->CpSphere[i] =vtkSphereSource::New();
            d->CpSphere[i]->SetPhiResolution(15);
            d->CpSphere[i]->SetThetaResolution(15);
            d->CpSphere[i]->SetRadius(side);
            d->CpSphere[i]->SetCenter(d->CpPoints->GetPoint(i));
#if (VTK_MAJOR_VERSION <= 5)
            d->CpMapper[i]->SetInput( d->CpSphere[i]->GetOutput());
#else
            d->CpMapper[i]->SetInputData( d->CpSphere[i]->GetOutput());
#endif
            d->CpActor[i] = vtkActor::New();
            d->CpActor[i]->SetMapper(d->CpMapper[i]);
            d->CpActor[i]->VisibilityOn();
            // d->CpCursorPicker->AddPickList(d->CpActor[i*nbCoeffs_u+j]);
            // d->CpRectanglePicker->AddPickList(d->CpActor[i*nbCoeffs_u+j]);
            d->CpCursorPicker->SetTolerance(0.001); //need some fluff

            d->CpSphere[i]->Modified();
            d->CpSphere[i]->Update();



            ////----------------------------------------------------------------------------------------------------
            //// add 04 february 2014
            //// example from http://www.vtk.org/Wiki/VTK/Examples/Cxx/Visualization/DistanceToCamera

//            //Calculate the distance to the camera of each CpSphere.
//            vtkSmartPointer<vtkDistanceToCamera> distanceToCamera = vtkSmartPointer<
//                    vtkDistanceToCamera>::New();
//            distanceToCamera->SetInputConnection(d->CpSphere[i]->GetOutputPort());
//            distanceToCamera->SetScreenSize(10.0);


//            // Glyph each point with a sphere.
//            vtkSmartPointer<vtkSphereSource> spheres =
//                    vtkSmartPointer<vtkSphereSource>::New();
//            vtkSmartPointer<vtkGlyph3D> fixedGlyph = vtkSmartPointer<vtkGlyph3D>::New();
//            fixedGlyph->SetInputConnection(distanceToCamera->GetOutputPort());
//            fixedGlyph->SetSourceConnection(spheres->GetOutputPort());

//            // Scale each point.
//            fixedGlyph->SetScaleModeToScaleByScalar();
//            fixedGlyph->SetInputArrayToProcess(0, 0, 0,
//                                               vtkDataObject::FIELD_ASSOCIATION_POINTS, "DistanceToCamera");

//            // Create a mapper.
//            vtkSmartPointer<vtkPolyDataMapper> fixedMapper = vtkSmartPointer<
//                    vtkPolyDataMapper>::New();
//            fixedMapper->SetInputConnection(fixedGlyph->GetOutputPort());
//            fixedMapper->SetScalarVisibility(false);

//            //create a renderer if NULL
//            if(!this->CurrentRenderer){
//                this->SetCurrentRenderer(this->Interactor->FindPokedRenderer(
//                                             this->Interactor->GetLastEventPosition()[0],
//                                             this->Interactor->GetLastEventPosition()[1]));
//            }
//            distanceToCamera->SetRenderer(this->CurrentRenderer);

//            d->CpActor[i]->SetMapper(fixedMapper);
            ////----------------------------------------------------------------------------------------------------

            d->CpActorCollection->AddItem(d->CpActor[i]);

        }

        if(!splineSurface->connectionsAreDefined()){
            //Default display is a grid.
            int nbCoeffs_u = splineSurface->countControlPoints_u();// little optimization;

            for(int i = 0; i < splineSurface->countControlPoints_v() - 1; i++)
            {

                for(int j = 0; j < nbCoeffs_u - 1; j++)
                {
                    vtkSmartPointer<vtkLine> currentline = vtkSmartPointer<vtkLine>::New();
                    currentline->GetPointIds()->SetId(0, i * nbCoeffs_u + j);
                    currentline->GetPointIds()->SetId(1, i * nbCoeffs_u + j + 1);

                    vtkSmartPointer<vtkLine> currentline2 = vtkSmartPointer<vtkLine>::New();
                    currentline2->GetPointIds()->SetId(0, i * nbCoeffs_u + j);
                    currentline2->GetPointIds()->SetId(1, (i + 1) * nbCoeffs_u + j);

                    d->CpCellAray->InsertNextCell(currentline);
                    d->CpCellAray->InsertNextCell(currentline2);
                }

                // we had some boundary lines
                vtkSmartPointer<vtkLine> currentline = vtkSmartPointer<vtkLine>::New();
                currentline->GetPointIds()->SetId(0, i * nbCoeffs_u + nbCoeffs_u - 1);
                currentline->GetPointIds()->SetId(1,(i + 1) * nbCoeffs_u + nbCoeffs_u - 1);
                d->CpCellAray->InsertNextCell(currentline);
            }

            // had of the boundary lines of the other side
            for(int i = 0; i < nbCoeffs_u - 1; i++)
            {
                vtkSmartPointer<vtkLine> currentline = vtkSmartPointer<vtkLine>::New();
                currentline->GetPointIds()->SetId(0, (splineSurface->countControlPoints_v() - 1) * nbCoeffs_u + i);
                currentline->GetPointIds()->SetId(1, (splineSurface->countControlPoints_v() - 1) * nbCoeffs_u + i + 1);
                d->CpCellAray->InsertNextCell(currentline);
            }
        }
        else
        {
            for(int i = 0 ; i < d->numCps; i++){
                QList<int> connection = splineSurface->getControlPointConnection(i);
                foreach(int value, connection){
                    vtkSmartPointer<vtkLine> currentline = vtkSmartPointer<vtkLine>::New();
                    currentline->GetPointIds()->SetId(0, i);
                    currentline->GetPointIds()->SetId(1, value);
                    d->CpCellAray->InsertNextCell(currentline);
                }
            }
        }

        // association of the glyph and the control Points
        d->CpPolyData->SetPoints(d->CpPoints);
        d->CpPolyData->SetLines(d->CpCellAray);
        // d->CpCursorPicker->PickFromListOn();
        // d->CpRectanglePicker->PickFromListOn();
    }

    if(axlShapeBSpline *splineShape =dynamic_cast<axlShapeBSpline *>(d->CpSpline))
    {
        d->CpMapper = new vtkPolyDataMapper *[d->numCps];
        d->CpSphere = new vtkSphereSource *[d->numCps];
        d->CpActor = new vtkActor *[d->numCps];
        d->CpActorCollection = vtkActorCollection::New();

        //To generalize.
        for(int i = 0; i < d->numCps; i++)
        {

            axlPoint pointCourant = splineShape->getCoef(i+1);
            d->CpPoints->InsertNextPoint(pointCourant.x(), pointCourant.y(), pointCourant.z());

            // Construct the poly data representing control points
            d->CpMapper[i]= vtkPolyDataMapper::New();
            d->CpSphere[i]= vtkSphereSource::New();
            d->CpSphere[i]->SetPhiResolution(15);
            d->CpSphere[i]->SetThetaResolution(15);
            d->CpSphere[i]->SetRadius(side);
            d->CpSphere[i]->SetCenter(d->CpPoints->GetPoint(i));
#if (VTK_MAJOR_VERSION <= 5)
            d->CpMapper[i]->SetInput( d->CpSphere[i]->GetOutput());
#else
            d->CpMapper[i]->SetInputData( d->CpSphere[i]->GetOutput());
#endif
            d->CpActor[i] = vtkActor::New();
            d->CpActor[i]->SetMapper(d->CpMapper[i]);
            d->CpActor[i]->VisibilityOn();
            // d->CpCursorPicker->AddPickList(d->CpActor[i*nbCoeffs_u+j]);
            // d->CpRectanglePicker->AddPickList(d->CpActor[i*nbCoeffs_u+j]);
            d->CpCursorPicker->SetTolerance(0.001); //need some fluff

            d->CpSphere[i]->Modified();
            d->CpSphere[i]->Update();

            d->CpActorCollection->AddItem(d->CpActor[i]);

        }

        if(!splineShape->connectionsAreDefined()){
            //Default display is a grid.
            int nbCoeffs_u = splineShape->countControlPoints_u();// little optimization;

            for(int i = 0; i < splineShape->countControlPoints_v() - 1; i++)
            {

                for(int j = 0; j < nbCoeffs_u - 1; j++)
                {
                    vtkSmartPointer<vtkLine> currentline = vtkSmartPointer<vtkLine>::New();
                    currentline->GetPointIds()->SetId(0, i * nbCoeffs_u + j);
                    currentline->GetPointIds()->SetId(1, i * nbCoeffs_u + j + 1);

                    vtkSmartPointer<vtkLine> currentline2 = vtkSmartPointer<vtkLine>::New();
                    currentline2->GetPointIds()->SetId(0, i * nbCoeffs_u + j);
                    currentline2->GetPointIds()->SetId(1, (i + 1) * nbCoeffs_u + j);

                    d->CpCellAray->InsertNextCell(currentline);
                    d->CpCellAray->InsertNextCell(currentline2);
                }

                // we had some boundary lines
                vtkSmartPointer<vtkLine> currentline = vtkSmartPointer<vtkLine>::New();
                currentline->GetPointIds()->SetId(0, i * nbCoeffs_u + nbCoeffs_u - 1);
                currentline->GetPointIds()->SetId(1,(i + 1) * nbCoeffs_u + nbCoeffs_u - 1);
                d->CpCellAray->InsertNextCell(currentline);
            }

            // had of the boundary lines of the other side
            for(int i = 0; i < nbCoeffs_u - 1; i++)
            {
                vtkSmartPointer<vtkLine> currentline = vtkSmartPointer<vtkLine>::New();
                currentline->GetPointIds()->SetId(0, (splineShape->countControlPoints_v() - 1) * nbCoeffs_u + i);
                currentline->GetPointIds()->SetId(1, (splineShape->countControlPoints_v() - 1) * nbCoeffs_u + i + 1);
                d->CpCellAray->InsertNextCell(currentline);
            }
        }
        else
        {
            for(int i = 0 ; i < d->numCps; i++){
                QList<int> connection = splineShape->getControlPointConnection(i);
                foreach(int value, connection){
                    vtkSmartPointer<vtkLine> currentline = vtkSmartPointer<vtkLine>::New();
                    currentline->GetPointIds()->SetId(0, i);
                    currentline->GetPointIds()->SetId(1, value);
                    d->CpCellAray->InsertNextCell(currentline);
                }
            }
        }

        // association of the glyph and the control Points
        d->CpPolyData->SetPoints(d->CpPoints);
        d->CpPolyData->SetLines(d->CpCellAray);
        // d->CpCursorPicker->PickFromListOn();
        // d->CpRectanglePicker->PickFromListOn();
    }

    if(axlAbstractVolumeBSpline *splineVolume =dynamic_cast<axlAbstractVolumeBSpline *>(d->CpSpline))
    {
        d->CpMapper3D = new vtkDataSetMapper *[d->numCps];
        d->CpSphere = new vtkSphereSource *[d->numCps];
        d->CpActor = new vtkActor *[d->numCps];
        d->CpActorCollection = vtkActorCollection::New();


        //        for(int k = 0; k < nbCoeffs_w; k++)
        //        {
        //            for(int i = 0; i < nbCoeffs_v; i++)
        //            {
        //                for(int j = 0; j < nbCoeffs_u; j++)
        //                {
        //                    axlPoint pointCourant = splineVolume->getCoef(j + 1, i + 1, k+1);
        //                    d->CpPoints->InsertNextPoint(pointCourant.x(), pointCourant.y(), pointCourant.z());

        //                    // Construct the unstructured grid representing control points
        //                    d->CpMapper3D[k *nbCoeffs_u *nbCoeffs_v + i * nbCoeffs_u + j] = vtkDataSetMapper::New();
        //                    d->CpSphere[k *nbCoeffs_u *nbCoeffs_v + i * nbCoeffs_u + j] =vtkSphereSource::New();
        //                    d->CpSphere[k *nbCoeffs_u *nbCoeffs_v + i * nbCoeffs_u + j]->SetPhiResolution(15);
        //                    d->CpSphere[k *nbCoeffs_u *nbCoeffs_v + i * nbCoeffs_u + j]->SetThetaResolution(15);
        //                    d->CpSphere[k *nbCoeffs_u *nbCoeffs_v + i * nbCoeffs_u + j]->SetRadius(side);
        //                    d->CpSphere[k *nbCoeffs_u *nbCoeffs_v + i * nbCoeffs_u + j]->SetCenter(d->CpPoints->GetPoint(k *nbCoeffs_u *nbCoeffs_v + i * nbCoeffs_u + j));
        //#if (VTK_MAJOR_VERSION <= 5)
        //                    d->CpMapper3D[k *nbCoeffs_u *nbCoeffs_v + i * nbCoeffs_u + j]->SetInput( d->CpSphere[k *nbCoeffs_u *nbCoeffs_v + i * nbCoeffs_u + j]->GetOutput());
        //#else
        //                    d->CpMapper3D[k *nbCoeffs_u *nbCoeffs_v + i * nbCoeffs_u + j]->SetInputData( d->CpSphere[k *nbCoeffs_u *nbCoeffs_v + i * nbCoeffs_u + j]->GetOutput());
        //#endif
        //                    d->CpActor[k *nbCoeffs_u *nbCoeffs_v + i * nbCoeffs_u + j] = vtkActor::New();
        //                    d->CpActor[k *nbCoeffs_u *nbCoeffs_v + i * nbCoeffs_u + j]->SetMapper(d->CpMapper3D[k *nbCoeffs_u *nbCoeffs_v + i * nbCoeffs_u + j]);
        //                    d->CpActor[k *nbCoeffs_u *nbCoeffs_v + i * nbCoeffs_u + j]->VisibilityOn();
        //                    d->CpCursorPicker->SetTolerance(0.001);

        //                    d->CpActorCollection->AddItem(d->CpActor[k *nbCoeffs_u *nbCoeffs_v + i * nbCoeffs_u + j]);
        //                }
        //            }
        //        }

        side = (splineVolume->size()>side ? splineVolume->size():side);

        for(int k = 0; k < d->numCps; k++)
        {

            axlPoint pointCourant = splineVolume->getCoef(k+1);
            d->CpPoints->InsertNextPoint(pointCourant.x(), pointCourant.y(), pointCourant.z());

            // Construct the unstructured grid representing control points
            d->CpMapper3D[k] = vtkDataSetMapper::New();
            d->CpSphere[k ] =vtkSphereSource::New();
            d->CpSphere[k ]->SetPhiResolution(15);
            d->CpSphere[k ]->SetThetaResolution(15);
            d->CpSphere[k ]->SetRadius(side);
            d->CpSphere[k ]->SetCenter(d->CpPoints->GetPoint(k));
#if (VTK_MAJOR_VERSION <= 5)
            d->CpMapper3D[k]->SetInput( d->CpSphere[k]->GetOutput());
#else
            d->CpMapper3D[k]->SetInputData( d->CpSphere[k]->GetOutput());
#endif
            d->CpActor[k] = vtkActor::New();
            d->CpActor[k]->SetMapper(d->CpMapper3D[k]);
            d->CpActor[k]->VisibilityOn();
            d->CpCursorPicker->SetTolerance(0.001);

            d->CpSphere[k]->Modified();
            d->CpSphere[k]->Update();

            ////----------------------------------------------------------------------------------------------------
            //// add 04 february 2014
            //// example from http://www.vtk.org/Wiki/VTK/Examples/Cxx/Visualization/DistanceToCamera

//            //Calculate the distance to the camera of each CpSphere.
//            vtkSmartPointer<vtkDistanceToCamera> distanceToCamera = vtkSmartPointer<
//                    vtkDistanceToCamera>::New();
//            distanceToCamera->SetInputConnection(d->CpSphere[k]->GetOutputPort());
//            distanceToCamera->SetScreenSize(10.0);


//            // Glyph each point with a sphere.
//            vtkSmartPointer<vtkSphereSource> spheres =
//                    vtkSmartPointer<vtkSphereSource>::New();
//            vtkSmartPointer<vtkGlyph3D> fixedGlyph = vtkSmartPointer<vtkGlyph3D>::New();
//            fixedGlyph->SetInputConnection(distanceToCamera->GetOutputPort());
//            fixedGlyph->SetSourceConnection(spheres->GetOutputPort());

//            // Scale each point.
//            fixedGlyph->SetScaleModeToScaleByScalar();
//            fixedGlyph->SetInputArrayToProcess(0, 0, 0,
//                                               vtkDataObject::FIELD_ASSOCIATION_POINTS, "DistanceToCamera");

//            // Create a mapper.
//            vtkSmartPointer<vtkPolyDataMapper> fixedMapper = vtkSmartPointer<
//                    vtkPolyDataMapper>::New();
//            fixedMapper->SetInputConnection(fixedGlyph->GetOutputPort());
//            fixedMapper->SetScalarVisibility(false);

//            //create a renderer if NULL
//            if(!this->CurrentRenderer){
//                this->SetCurrentRenderer(this->Interactor->FindPokedRenderer(
//                                             this->Interactor->GetLastEventPosition()[0],
//                                             this->Interactor->GetLastEventPosition()[1]));
//            }
//            distanceToCamera->SetRenderer(this->CurrentRenderer);

//            d->CpActor[k]->SetMapper(fixedMapper);
            ////----------------------------------------------------------------------------------------------------


            d->CpActorCollection->AddItem(d->CpActor[k]);
        }

        if(!splineVolume->connectionsAreDefined()){
            //Default display is a grid.
            int nbCoeffs_u = splineVolume->countControlPoints_u();// little optimization;
            int nbCoeffs_v = splineVolume->countControlPoints_v();
            int nbCoeffs_w = splineVolume->countControlPoints_w();

            for(int k = 0; k < nbCoeffs_w - 1; k++)
            {
                for(int i = 0; i < nbCoeffs_v - 1; i++)
                {
                    for(int j = 0; j < nbCoeffs_u - 1; j++)
                    {
                        vtkSmartPointer<vtkLine> currentline = vtkSmartPointer<vtkLine>::New();
                        currentline->GetPointIds()->SetId(0, k *nbCoeffs_u *nbCoeffs_v + i * nbCoeffs_u + j);
                        currentline->GetPointIds()->SetId(1, k *nbCoeffs_u *nbCoeffs_v + i * nbCoeffs_u + j+1);

                        vtkSmartPointer<vtkLine> currentline2 = vtkSmartPointer<vtkLine>::New();
                        currentline2->GetPointIds()->SetId(0, k *nbCoeffs_u *nbCoeffs_v + i * nbCoeffs_u + j);
                        currentline2->GetPointIds()->SetId(1, k *nbCoeffs_u *nbCoeffs_v + i * nbCoeffs_u + j+ nbCoeffs_u);

                        vtkSmartPointer<vtkLine> currentline3 = vtkSmartPointer<vtkLine>::New();
                        currentline3->GetPointIds()->SetId(0, k *nbCoeffs_u *nbCoeffs_v + i * nbCoeffs_u + j);
                        currentline3->GetPointIds()->SetId(1, k *nbCoeffs_u *nbCoeffs_v + i * nbCoeffs_u + j + nbCoeffs_u* nbCoeffs_v);

                        d->CpCellAray->InsertNextCell(currentline);
                        d->CpCellAray->InsertNextCell(currentline2);
                        d->CpCellAray->InsertNextCell(currentline3);
                    }

                    // we had some boundary lines
                    vtkSmartPointer<vtkLine> currentline = vtkSmartPointer<vtkLine>::New();
                    currentline->GetPointIds()->SetId(0, k *nbCoeffs_u *nbCoeffs_v + i * nbCoeffs_u + nbCoeffs_u - 1);
                    currentline->GetPointIds()->SetId(1,k *nbCoeffs_u *nbCoeffs_v + (i+1) * nbCoeffs_u + nbCoeffs_u - 1);

                    vtkSmartPointer<vtkLine> currentline2 = vtkSmartPointer<vtkLine>::New();
                    currentline2->GetPointIds()->SetId(0, k *nbCoeffs_u *nbCoeffs_v + i * nbCoeffs_u + nbCoeffs_u - 1 );
                    currentline2->GetPointIds()->SetId(1,k *nbCoeffs_u *nbCoeffs_v + i * nbCoeffs_u + nbCoeffs_u - 1+ nbCoeffs_u* nbCoeffs_v);

                    d->CpCellAray->InsertNextCell(currentline);
                    d->CpCellAray->InsertNextCell(currentline2);
                }

                // we had some boundary lines
                vtkSmartPointer<vtkLine> currentline = vtkSmartPointer<vtkLine>::New();
                currentline->GetPointIds()->SetId(0, k *nbCoeffs_u *nbCoeffs_v + (nbCoeffs_v - 1) * nbCoeffs_u + nbCoeffs_u - 1 );
                currentline->GetPointIds()->SetId(1,(k+1) *nbCoeffs_u *nbCoeffs_v + (nbCoeffs_v - 1) * nbCoeffs_u + nbCoeffs_u - 1);
                d->CpCellAray->InsertNextCell(currentline);
            }

            // had of the boundary lines of the other side
            for(int j = 0; j < nbCoeffs_v - 1; j++)
            {
                for(int i = 0; i < nbCoeffs_u - 1; i++)
                {
                    vtkSmartPointer<vtkLine> currentline = vtkSmartPointer<vtkLine>::New();
                    currentline->GetPointIds()->SetId(0, ( (nbCoeffs_w -1 ) *nbCoeffs_u *nbCoeffs_v + j * nbCoeffs_u + i));
                    currentline->GetPointIds()->SetId(1, ((nbCoeffs_w -1 ) *nbCoeffs_u *nbCoeffs_v + j * nbCoeffs_u + i +1));

                    vtkSmartPointer<vtkLine> currentline2 = vtkSmartPointer<vtkLine>::New();
                    currentline2->GetPointIds()->SetId(0, ((nbCoeffs_w -1 ) *nbCoeffs_u *nbCoeffs_v + j * nbCoeffs_u + i));
                    currentline2->GetPointIds()->SetId(1, ((nbCoeffs_w -1 ) *nbCoeffs_u *nbCoeffs_v + (j+1) * nbCoeffs_u + i));

                    d->CpCellAray->InsertNextCell(currentline);
                    d->CpCellAray->InsertNextCell(currentline2);
                }

                vtkSmartPointer<vtkLine> currentline = vtkSmartPointer<vtkLine>::New();
                currentline->GetPointIds()->SetId(0, ((nbCoeffs_w -1 ) *nbCoeffs_u *nbCoeffs_v + j * nbCoeffs_u + nbCoeffs_u -1));
                currentline->GetPointIds()->SetId(1, ((nbCoeffs_w -1 ) *nbCoeffs_u *nbCoeffs_v + (j+1) * nbCoeffs_u + nbCoeffs_u -1));

                d->CpCellAray->InsertNextCell(currentline);
            }

            // had of the boundary lines of the other side
            for(int i = 0; i < nbCoeffs_u - 1; i++)
            {
                for(int k = 0; k < nbCoeffs_w - 1; k++)
                {
                    vtkSmartPointer<vtkLine> currentline = vtkSmartPointer<vtkLine>::New();
                    currentline->GetPointIds()->SetId(0, ( k *nbCoeffs_u *nbCoeffs_v + (nbCoeffs_v -1) * nbCoeffs_u + i));
                    currentline->GetPointIds()->SetId(1, (k *nbCoeffs_u *nbCoeffs_v + (nbCoeffs_v -1) * nbCoeffs_u + i+1));

                    vtkSmartPointer<vtkLine> currentline2 = vtkSmartPointer<vtkLine>::New();
                    currentline2->GetPointIds()->SetId(0, (k *nbCoeffs_u *nbCoeffs_v + (nbCoeffs_v -1) * nbCoeffs_u + i));
                    currentline2->GetPointIds()->SetId(1, ((k+1) *nbCoeffs_u *nbCoeffs_v + (nbCoeffs_v -1) * nbCoeffs_u + i));

                    d->CpCellAray->InsertNextCell(currentline);
                    d->CpCellAray->InsertNextCell(currentline2);
                }

                vtkSmartPointer<vtkLine> currentline = vtkSmartPointer<vtkLine>::New();
                currentline->GetPointIds()->SetId(0, ((nbCoeffs_w -1 ) *nbCoeffs_u *nbCoeffs_v + (nbCoeffs_v -1) * nbCoeffs_u + i));
                currentline->GetPointIds()->SetId(1, ((nbCoeffs_w -1 ) *nbCoeffs_u *nbCoeffs_v + (nbCoeffs_v -1) * nbCoeffs_u + i+1));

                d->CpCellAray->InsertNextCell(currentline);
            }

        } else{
            for(int i = 0 ; i < d->numCps; i++){
                QList<int> connection = splineVolume->getControlPointConnection(i);
                foreach(int value, connection){
                    vtkSmartPointer<vtkLine> currentline = vtkSmartPointer<vtkLine>::New();
                    currentline->GetPointIds()->SetId(0, i);
                    currentline->GetPointIds()->SetId(1, value);
                    d->CpCellAray->InsertNextCell(currentline);
                }
            }
        }

        // association of the glyph and the control Points
        d->CpUnstructuredGrid->SetPoints(d->CpPoints);
        d->CpUnstructuredGrid->SetCells(VTK_LINE, d->CpCellAray); //A REVOIR
    }
}

void axlControlPointsWidget::resetProperty(void)
{
    for (int i=0; i< d->numCps; i++)
    {
        d->CpActor[i]->SetProperty(d->CpHandleProperty);
    }
}

vtkActor *axlControlPointsWidget::netActor(void)
{
    return d->CpPolyDataActor;
}

vtkActorCollection *axlControlPointsWidget::ptsActors(void)
{
    return d->CpActorCollection;
}

void axlControlPointsWidget::SetEnabled(int enabling)
{

    if ( ! this->Interactor )
    {
        vtkErrorMacro(<<"The interactor must be set prior to enabling/disabling widget");
        return;
    }

    if ( enabling ) //------------------------------------------------------------
    {
        vtkDebugMacro(<<"Enabling widget");

        if ( this->Enabled ) //already enabled, just return
        {
            return;
        }

        if ( ! this->CurrentRenderer )
        {
            this->SetCurrentRenderer(this->Interactor->FindPokedRenderer(
                                         this->Interactor->GetLastEventPosition()[0],
                                         this->Interactor->GetLastEventPosition()[1]));
            if (this->CurrentRenderer == NULL)
            {
                return;
            }
        }

        this->Enabled = 1;

        // listen to the following events
        vtkRenderWindowInteractor *i = this->Interactor;
        i->AddObserver(vtkCommand::MouseMoveEvent, this->EventCallbackCommand,
                       this->Priority);
        i->AddObserver(vtkCommand::LeftButtonPressEvent,
                       this->EventCallbackCommand, this->Priority);
        i->AddObserver(vtkCommand::LeftButtonReleaseEvent,
                       this->EventCallbackCommand, this->Priority);

        for (int i=0; i< d->numCps; i++)
        {
            d->CpActor[i]->SetVisibility(true);
        }

        d->CpPolyDataActor->SetVisibility(true);
        d->CpCurrentActorIndex =-1;
        d->XStartPosition =-1;
        d->YStartPosition =-1;


        this->InvokeEvent(vtkCommand::EnableEvent,NULL);
    }

    else //disabling-------------------------------------------------------------
    {
        vtkDebugMacro(<<"Disabling widget");

        if ( ! this->Enabled ) //already disabled, just return
        {
            return;
        }

        this->Enabled = 0;

        // don't listen for events any more
        this->Interactor->RemoveObserver(this->EventCallbackCommand);

        // remove CpActor
        for (int i=0; i<d->numCps; i++)
        {
            d->CpActor[i]->SetVisibility(false);
        }

        d->CpPolyDataActor->SetVisibility(false);

        d->CpCurrentActorIndex =-1;
        d->XStartPosition =-1;
        d->YStartPosition =-1;

        this->InvokeEvent(vtkCommand::DisableEvent,NULL);
        this->SetCurrentRenderer(NULL);
    }

    this->Interactor->Render();
}

void axlControlPointsWidget::SetControlPointRadius(double cpSize){

    double bounds[6];this->Prop3D->GetBounds(bounds);
    double side = qAbs(bounds[1]-bounds[0]);
    side += qAbs(bounds[3]-bounds[2]);
    side += qAbs(bounds[5]-bounds[4]);

    side/=600;

    if(dynamic_cast<axlAbstractCurveBSpline *>(d->CpSpline))
        side = ((cpSize*d->radiusScaleCps)>side ? (cpSize*d->radiusScaleCps):side);
    else
        side = (cpSize>side ? cpSize:side);

    for(int i = 0 ; i < d->numCps; i++){
        d->CpSphere[i]->SetRadius(side);

        d->CpSphere[i]->Modified();
        d->CpSphere[i]->Update();
    }
}

int axlControlPointsWidget::HighlightHandle(vtkProp *prop)
{
    //    if(d->CpRubberBanPick->getCurrentMode()==0)
    //    {//We select point with the cursor
    // first unhighlight anything picked
    this->resetProperty();

    vtkActor *currentActor = static_cast<vtkActor *>(prop);

    if ( currentActor )
    {


        for (int i=0; i<d->numCps; i++)
        {
            if ( currentActor == d->CpActor[i] )
            {
                currentActor->SetProperty(d->CpSelectedHandleProperty);
                d->CpCursorPicker->GetPickPosition(this->LastPickPosition);
                d->CpRectanglePicker->GetPickPosition(this->LastPickPosition);
                return i;
            }
        }
    }

    //    }
    //    else// we select points with rectangle
    //    {
    //        d->CpCurrentActor = static_cast<vtkActor *>(prop);
    //        if(d->CpCurrentActor->GetProperty()!=d->CpSelectedHandleProperty)
    //        {
    //            this->resetProperty();
    //            this->State = axlControlPointsWidget::Outside;
    //        }
    //    }


    return -1;
}



void axlControlPointsWidget::ProcessEvents(vtkObject* vtkNotUsed(object), unsigned long event, void *clientdata, void *vtkNotUsed(calldata))
{
    axlControlPointsWidget* self = reinterpret_cast<axlControlPointsWidget *>( clientdata );

    //okay, let's do the right thing
    switch(event)
    {
    case vtkCommand::LeftButtonPressEvent:
        self->OnLeftButtonDown();
        break;
    case vtkCommand::LeftButtonReleaseEvent:
        self->OnLeftButtonUp();
        break;
    case vtkCommand::MouseMoveEvent:
        self->OnMouseMove();
        break;
    }
}

void axlControlPointsWidget::PlaceWidget(double bds[6])
{
    int i;
    double bounds[6], center[3];

    this->AdjustBounds(bds, bounds, center);

    for (i=0; i<6; i++)
    {
        this->InitialBounds[i] = bounds[i];
    }

    this->InitialLength = sqrt((bounds[1]-bounds[0])*(bounds[1]-bounds[0]) +
                               (bounds[3]-bounds[2])*(bounds[3]-bounds[2]) +
                               (bounds[5]-bounds[4])*(bounds[5]-bounds[4]));
}

void axlControlPointsWidget::PrintSelf(ostream& os, vtkIndent indent)
{
    this->Superclass::PrintSelf(os,indent);
}

#define VTK_AVERAGE(a,b,c) \
    c[0] = (a[0] + b[0])/2.0; \
    c[1] = (a[1] + b[1])/2.0; \
    c[2] = (a[2] + b[2])/2.0;

#undef VTK_AVERAGE

void axlControlPointsWidget::OnLeftButtonDown(void)
{
    // d->interaction = true;
    int X = this->Interactor->GetEventPosition()[0];
    int Y = this->Interactor->GetEventPosition()[1];

    // Okay, we can process this. Try to pick handles first;
    // if no handles picked, then pick the bounding box.
    if (!this->CurrentRenderer || !this->CurrentRenderer->IsInViewport(X, Y))
    {
        this->State = axlControlPointsWidget::Outside;
        return;
    }

    d->XStartPosition = X;
    d->YStartPosition = Y;

    vtkAssemblyPath *path = NULL;
    d->CpCursorPicker->Pick(X, Y, 0.0, this->CurrentRenderer);

    path = d->CpCursorPicker->GetPath();

    if ( path != NULL)
    {
        this->State = axlControlPointsWidget::Moving;
        d->CpCurrentActorIndex=this->HighlightHandle(path->GetLastNode()->GetViewProp());
        d->CpCurrentAssemblyNode = path->GetLastNode();
        d->CpCursorPicker->GetPickPosition(this->LastPickPosition);

        if( d->CpCurrentActorIndex == -1)
        {
            this->resetProperty();
            this->State = axlControlPointsWidget::Outside;
            return;
        }


    }
    else
    {
        this->resetProperty();
        this->State = axlControlPointsWidget::Outside;
        return;
    }
    this->EventCallbackCommand->SetAbortFlag(1);
    this->StartInteraction();
    //this->InvokeEvent(vtkCommand::StartInteractionEvent, NULL);
    //this->Interactor->Render();


    // G+Smo : giving back the control point index
    //d->CpSpline->setParameter(d->CpCurrentActorIndex);
    if (axlAbstractCurveBSpline *splineCurve =dynamic_cast<axlAbstractCurveBSpline *>(d->CpSpline)) {
        splineCurve->selectIndex(d->CpCurrentActorIndex);
    }
    else if (axlAbstractSurfaceBSpline *splineSurface =dynamic_cast<axlAbstractSurfaceBSpline *>(d->CpSpline)) {
        splineSurface->selectIndex(d->CpCurrentActorIndex);
    }
    else if (axlAbstractVolumeBSpline *splineVolume =dynamic_cast<axlAbstractVolumeBSpline *>(d->CpSpline))
    {
        splineVolume->selectIndex(d->CpCurrentActorIndex);
    }
    else if (axlShapeBSpline *splineSurface = dynamic_cast<axlShapeBSpline *>(d->CpSpline)) {
        splineSurface->selectIndex(d->CpCurrentActorIndex);
    }
}

void axlControlPointsWidget::OnLeftButtonUp()
{
    // d->interaction = true;
    int X = this->Interactor->GetEventPosition()[0];
    int Y = this->Interactor->GetEventPosition()[1];
    // Okay, we can process this. Try to pick handles first;
    // if no handles picked, then pick the bounding box.
    if (!this->CurrentRenderer || !this->CurrentRenderer->IsInViewport(X, Y))
    {
        this->State = axlControlPointsWidget::Outside;
        return;
    }

    /*if(d->CpRubberBanPick->getCurrentMode()==1)// mode rectangle selection activated
    {
        vtkProp3DCollection *listProp;
        d->CpRectanglePicker->AreaPick(d->CpRubberBanPick->getStartPositionX(),d->CpRubberBanPick->getStartPositionY(),d->CpRubberBanPick->getEndPositionX(),d->CpRubberBanPick->getEndPositionY(),this->CurrentRenderer);
        listProp = d->CpRectanglePicker->GetProp3Ds();
        if ( listProp != NULL )
        {
            vtkCollectionIterator *listPropIterator =listProp->NewIterator();
            listPropIterator->InitTraversal();
            while(!listPropIterator->IsDoneWithTraversal())
            {
                static_cast<vtkActor *>(listPropIterator->GetCurrentObject())->SetProperty(d->CpSelectedHandleProperty);
                listPropIterator->GoToNextItem();
            }
        }

    }*/

    if ( this->State == axlControlPointsWidget::Outside || this->State == axlControlPointsWidget::Start )
    {
        return;
    }


    this->State = axlControlPointsWidget::Start;

    this->EventCallbackCommand->SetAbortFlag(0);
    this->EndInteraction();
    this->InvokeEvent(vtkCommand::EndInteractionEvent, NULL);
    //this->Interactor->Render();
}

void axlControlPointsWidget::OnMouseMove()
{
    // See whether we're active
    if ( this->State == axlControlPointsWidget::Outside ||
         this->State == axlControlPointsWidget::Start )
    {
        return;
    }

    int X = this->Interactor->GetEventPosition()[0];
    int Y = this->Interactor->GetEventPosition()[1];

    // Do different things depending on state
    // Calculations everybody does
    double focalPoint[4], pickPoint[4], prevPickPoint[4], motionVector[4];
    double z;

    vtkCamera *camera = this->CurrentRenderer->GetActiveCamera();
    if ( !camera )
    {
        return;
    }

    // Compute the two points defining the motion vector
    this->ComputeWorldToDisplay(this->LastPickPosition[0], this->LastPickPosition[1], this->LastPickPosition[2], focalPoint);
    z = focalPoint[2];
    this->ComputeDisplayToWorld(double(this->Interactor->GetLastEventPosition()[0]),double(this->Interactor->GetLastEventPosition()[1]),
                                z, prevPickPoint);
    this->ComputeDisplayToWorld(double(X), double(Y), z, pickPoint);
    for(int i=0;i<4;i++)
    {
        motionVector[i]=pickPoint[i]-prevPickPoint[i];
    }

    vtkMatrix4x4 *motion = vtkMatrix4x4::New();

    if(d->CpCurrentAssemblyNode)
        motion->DeepCopy(d->CpCurrentAssemblyNode->GetMatrix());
    motion->Invert();


    if ( this->State == axlControlPointsWidget::Moving )
    { // Okay to process
        if(axlAbstractCurveBSpline *splineCurve =dynamic_cast<axlAbstractCurveBSpline *>(d->CpSpline))
        {
            double *newMotion = motion->MultiplyDoublePoint(motionVector);
            double newPosition[4];
            for(int j = 0; j < 4; j++)
            {
                newPosition[j]=d->CpPoints->GetPoint(d->CpCurrentActorIndex)[j]+newMotion[j];
            }
            d->CpPoints->SetPoint(d->CpCurrentActorIndex, newPosition[0], newPosition[1], newPosition[2]);
            d->CpPoints->Modified();
            d->CpSphere[d->CpCurrentActorIndex]->SetCenter(d->CpPoints->GetPoint(d->CpCurrentActorIndex));
            d->CpSphere[d->CpCurrentActorIndex]->Update();
            d->CpSphere[d->CpCurrentActorIndex]->Modified();
            //modification on the spline
            splineCurve->setCoef(d->CpCurrentActorIndex + 1,newPosition);

        }

        if(axlAbstractSurfaceBSpline *splineSurface =dynamic_cast<axlAbstractSurfaceBSpline *>(d->CpSpline))
        {

            double *newMotion = motion->MultiplyDoublePoint(motionVector);
            double newPosition[4];
            for(int j = 0 ;j < 4; j++)
            {
                newPosition[j]=d->CpPoints->GetPoint(d->CpCurrentActorIndex)[j]+newMotion[j];
            }
            d->CpPoints->SetPoint(d->CpCurrentActorIndex, newPosition[0], newPosition[1], newPosition[2]);
            d->CpPoints->Modified();
            d->CpSphere[d->CpCurrentActorIndex]->SetCenter(d->CpPoints->GetPoint(d->CpCurrentActorIndex));
            d->CpSphere[d->CpCurrentActorIndex]->Update();
            d->CpSphere[d->CpCurrentActorIndex]->Modified();
            //modification on the spline
            splineSurface->setCoef(d->CpCurrentActorIndex + 1,newPosition);
        }

        if(axlShapeBSpline *splineShape = dynamic_cast<axlShapeBSpline *>(d->CpSpline))
        {

            double *newMotion = motion->MultiplyDoublePoint(motionVector);
            double newPosition[4];
            for(int j = 0 ;j < 4; j++)
            {
                newPosition[j]=d->CpPoints->GetPoint(d->CpCurrentActorIndex)[j]+newMotion[j];
            }
            d->CpPoints->SetPoint(d->CpCurrentActorIndex, newPosition[0], newPosition[1], newPosition[2]);
            d->CpPoints->Modified();
            d->CpSphere[d->CpCurrentActorIndex]->SetCenter(d->CpPoints->GetPoint(d->CpCurrentActorIndex));
            d->CpSphere[d->CpCurrentActorIndex]->Update();
            d->CpSphere[d->CpCurrentActorIndex]->Modified();
            //modification on the spline
            splineShape->setCoef(d->CpCurrentActorIndex + 1,newPosition);
        }

        if(axlAbstractVolumeBSpline *splineVolume =dynamic_cast<axlAbstractVolumeBSpline *>(d->CpSpline))
        {

            double *newMotion = motion->MultiplyDoublePoint(motionVector);
            double newPosition[4];
            for(int j = 0 ;j < 4; j++)
            {
                newPosition[j]=d->CpPoints->GetPoint(d->CpCurrentActorIndex)[j]+newMotion[j];
            }
            d->CpPoints->SetPoint(d->CpCurrentActorIndex, newPosition[0], newPosition[1], newPosition[2]);
            d->CpPoints->Modified();
            d->CpSphere[d->CpCurrentActorIndex]->SetCenter(d->CpPoints->GetPoint(d->CpCurrentActorIndex));
            d->CpSphere[d->CpCurrentActorIndex]->Update();
            d->CpSphere[d->CpCurrentActorIndex]->Modified();
            //modification on the spline
            splineVolume->setCoef(d->CpCurrentActorIndex + 1,newPosition);
        }
    }
    else
    {
        return; //avoid the extra render
    }

    motion->Delete();

    this->EventCallbackCommand->SetAbortFlag(1);
    this->InvokeEvent(vtkCommand::InteractionEvent,NULL);
    this->Interactor->Render();
}

void axlControlPointsWidget::CreateDefaultProperties()
{
    // Handle properties
    d->CpHandleProperty = vtkProperty::New();
    d->CpHandleProperty->SetColor(1,1,1);

    d->CpSelectedHandleProperty = vtkProperty::New();
    d->CpSelectedHandleProperty->SetColor(1,0,0);
}
