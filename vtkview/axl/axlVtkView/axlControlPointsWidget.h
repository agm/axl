/* axlControlPointsWidget.h ---
 *
 * Author: Meriadeg Perrinel
 * Copyright (C) 2008 - Meriadeg Perrinel, Inria.
 * Created: Fri Feb 18 17:25:39 2011 (+0100)
 * Version: $Id$
 * Last-Updated: Wed Mar 16 21:46:46 2011 (+0100)
 *           By: Julien Wintz
 *     Update #: 32
 */

/* Commentary:
 *
 */

/* Change log:
 *
 */

#ifndef AXLCONTROLPOINTSWIDGET_H
#define AXLCONTROLPOINTSWIDGET_H

#include <vtk3DWidget.h>

#include <axlCore/axlAbstractData.h>

#include "axlVtkViewPluginExport.h"

class axlControlPointsWidgetPrivate;

class vtkActor;
class vtkActorCollection;
class vtkCellPicker;
class vtkPlanes;
class vtkPoints;
class vtkPolyData;
class vtkPolyDataMapper;
class vtkProp;
class vtkProperty;
class vtkSphereSource;
class vtkTransform;

class AXLVTKVIEWPLUGIN_EXPORT axlControlPointsWidget : public vtk3DWidget
{

public:
    static axlControlPointsWidget *New();

    vtkTypeMacro(axlControlPointsWidget,vtk3DWidget);

    void PrintSelf(ostream& os, vtkIndent indent);

    void PlaceWidget(double bounds[6]) override;
    void PlaceWidget(void) override{
        this->Superclass::PlaceWidget();
    }
    void PlaceWidget(double xmin, double xmax, double ymin, double ymax, double zmin, double zmax) override{
        this->Superclass::PlaceWidget(xmin,xmax,ymin,ymax,zmin,zmax);
    }

    void SetEnabled(int) override;
    void setSpline(dtkAbstractData *spline);

    void initializePoints(void);
    void resetProperty(void);
    void SetControlPointRadius(double cpSize);

    vtkActor           *netActor(void);
    vtkActorCollection *ptsActors(void);

protected:
     axlControlPointsWidget();
    ~axlControlPointsWidget(void);

    int State;

    enum WidgetState {
        Start,
        Moving,
        Scaling,
        Translating,
        Outside
    };


    int HighlightHandle(vtkProp *prop);

    static void ProcessEvents(vtkObject* object, unsigned long event, void* clientdata, void* calldata);

    virtual void OnMouseMove(void);
    virtual void OnLeftButtonDown(void);
    virtual void OnLeftButtonUp(void);

    void CreateDefaultProperties(void);


private:
    axlControlPointsWidget(const axlControlPointsWidget&); // Not implemented
            void operator=(const axlControlPointsWidget&); // Not implemented

private:
    axlControlPointsWidgetPrivate *d;
};

#endif
