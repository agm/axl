What is Axl ?
==============

Axl is a cross platform software to visualize, manipulate and compute 3D objects. It is composed of a main application and several plugins.

The main application provides:

  - atomic data: point, line, plane, cylinder, ...
  - atomic process: pointCreator, planeCreator, ...
  - a viewer based on VTK.
  - a GUI to handle objects, to select data, to apply process on them and to visualize the results.

The plugins provides:

  - more data with their reader, writer, converter and interactors;
  - more processes on the new or atomic data.

Axl can be used with linux, windows or mac. See  the download page to get the application and its plugins.

It can be used to read and write axl files as follows::

  axl file1.axl file2.axl ...

Axl files can also be opened from the application (top menu File/Open or keyboard shortcut  is C-0) and data can be saved in Axl format (top menu File/Save or keyboard shortcut  is  C-S).
