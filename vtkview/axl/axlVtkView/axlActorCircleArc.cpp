/* axlActorCircleArc.cpp ---
 *
 * Author: Valentin Michelet
 * Copyright (C) 2008 - Valentin Michelet, Inria.
 * Created: Tue Nov  9 16:58:59 2010 (+0100)
 * Version: $Id$
 * Last-Updated: Tue Nov  9 17:09:38 2010 (+0100)
 *           By: Valentin Michelet
 *     Update #: 19
 */

/* Commentary:
 *
 */

/* Change log:
 *
 */

#include "axlActorCircleArc.h"

#include "axlActorComposite.h"
#include "axlInteractorStyleSwitch.h"

#include <axlCore/axlCircleArc.h>
#include <axlCore/axlPoint.h>

#include <QVTKOpenGLWidget.h>

#include <vtkActor.h>
#include <vtkAssemblyPath.h>
#include <vtkCellPicker.h>
#include <vtkCommand.h>
#include <vtkGlyph3D.h>
#include <vtkInteractorStyleSwitch.h>
#include <vtkLight.h>
#include <vtkLightCollection.h>
#include <vtkParametricFunctionSource.h>.h>
#include <vtkArcSource.h>
#include <vtkLineWidget.h>
#include <vtkMath.h>
#include <vtkObjectFactory.h>
#include <vtkPoints.h>
#include <vtkPointWidget.h>
#include <vtkPolyData.h>
#include <vtkPolyDataMapper.h>
#include <vtkProperty.h>
#include <vtkRenderer.h>
#include <vtkRendererCollection.h>
#include <vtkRenderWindow.h>
#include <vtkSmartPointer.h>
#include <vtkTransform.h>


class axlActorCircleArcObserver : public vtkCommand {
public:
    static axlActorCircleArcObserver *New(void) {
        return new axlActorCircleArcObserver;
    }

    virtual void Execute(vtkObject *caller, unsigned long event, void *) {
        vtkRenderWindowInteractor *interactor = observerDataAssembly->getInteractor();            //interactor->Render();

        if(event == vtkCommand::InteractionEvent) {
            observerData_arcSource->Update();
            observerData_arcSource->Modified();
            interactor->Render();

            if (centerPointWidget && segmentWidget) {
                axlPoint center = *(observerData_arc->center());
                axlPoint centerPW(centerPointWidget->GetPosition());

                if (axlPoint::distance(&center, &centerPW) > 0.001) {
                    // We are moving the center
                    axlPoint C = *(observerData_arc->center());
                    axlPoint M = (*(observerData_arc->point1()) + *(observerData_arc->point2())) / 2;
                    axlPoint Cp(centerPointWidget->GetPosition());
                    axlPoint MC = C-M;
                    axlPoint u(MC);
                    axlPoint MCp = Cp - M;
                    u.normalize();
                    double normMCH = (u.x()*MCp.x() + u.y()*MCp.y() + u.z()*MCp.z());
                    axlPoint MCH = u*normMCH;
                    axlPoint CH = M+MCH;

                    observerData_arc->modifyCenter(CH.coordinates());
                } else {
                    // We are moving one of the edge
                    observerData_arc->modifyPoint1(segmentWidget->GetPoint1());
                    observerData_arc->modifyPoint2(segmentWidget->GetPoint2());
                    axlPoint neoNormal(axlPoint(segmentWidget->GetPoint1()[1]*segmentWidget->GetPoint1()[2] - segmentWidget->GetPoint1()[2]*segmentWidget->GetPoint1()[1],
                                                segmentWidget->GetPoint1()[2]*segmentWidget->GetPoint1()[0] - segmentWidget->GetPoint1()[0]*segmentWidget->GetPoint1()[2],
                                                segmentWidget->GetPoint1()[0]*segmentWidget->GetPoint1()[1] - segmentWidget->GetPoint1()[1]*segmentWidget->GetPoint1()[0]));
                    observerData_arc->modifyNormal(neoNormal.coordinates());
                    observerData_arc->calculateCenter();
                }
            }
            observerDataAssembly->onUpdateGeometry();
        }

        if(event == vtkCommand::MouseMoveEvent) {
            //qDebug() << "vtkCommand::MouseMoveEvent";
            if (observerData_arc) {
                vtkAssemblyPath *path;
                vtkRenderWindow *renderWindow = interactor->GetRenderWindow();
                vtkRendererCollection * rendererCollection = renderWindow->GetRenderers();
                vtkRenderer *render = rendererCollection->GetFirstRenderer();
                axlInteractorStyle = dynamic_cast<axlInteractorStyleSwitch *>(interactor->GetInteractorStyle());

                int X = observerDataAssembly->getInteractor()->GetEventPosition()[0];
                int Y = observerDataAssembly->getInteractor()->GetEventPosition()[1];

                if (!render || !render->IsInViewport(X, Y)) {
                    //qDebug()<<" No renderer or bad cursor coordonates";
                }

                axlArcPicker->Pick(X,Y,0.0,render);
                path = axlArcPicker->GetPath();

                if (path != NULL) {
                    double *position = observerDataAssembly->GetPosition();

                    for(int j=0;j<3;j++) {
                        //qDebug()<<"axlActorCylinderObserver :: Execute "<<position[j];
                        //observerData_line->coordinates()[j] = position[j];
                    }
                }
            }
        }
    }
    axlInteractorStyleSwitch *axlInteractorStyle;
    vtkCellPicker *axlArcPicker;
    axlCircleArc *observerData_arc;
    axlActorCircleArc *observerDataAssembly;
    vtkArcSource *observerData_arcSource;

    vtkPointWidget *centerPointWidget;
    vtkLineWidget *segmentWidget;
};

//// /////////////////////////////////////////////////////////////////
//// axlActorCircleArcPrivate
//// /////////////////////////////////////////////////////////////////

class axlActorCircleArcPrivate {
public:
    axlCircleArc *arc;
    vtkArcSource *arcSource;
    axlActorCircleArcObserver *arcObserver;
    QVTKOpenGLWidget *widget;
    vtkCellPicker *axlArcPicker;

    vtkPointWidget *centerPointWidget;
    vtkLineWidget *segmentWidget;
};

vtkCxxRevisionMacro(axlActorCircleArc, "$Revision: 0.0.1 $");
vtkStandardNewMacro(axlActorCircleArc);

dtkAbstractData *axlActorCircleArc::data(void) {
    return d->arc;
}

vtkArcSource *axlActorCircleArc::arc(void) {
    return d->arcSource;
}

void axlActorCircleArc::setQVTKWidget(QVTKOpenGLWidget *widget) {
    d->widget = widget;
}

void axlActorCircleArc::setCircleArc(axlCircleArc *arc) {
    if(arc) {
        d->arc = arc;
        this->setMapper(vtkSmartPointer<vtkPolyDataMapper>::New());
        this->setActor(vtkSmartPointer<vtkActor>::New());

        d->arcSource = vtkArcSource::New();

        d->arcSource->SetResolution(20);

        this->onUpdateGeometry();
        this->getMapper()->SetInput(d->arcSource->GetOutput());
        this->getActor()->SetMapper(this->getMapper());

        this->AddPart(this->getActor());

        if(!d->arcObserver) {
            d->axlArcPicker = vtkCellPicker::New();
            d->arcObserver = axlActorCircleArcObserver::New() ;
            this->getInteractor()->AddObserver(vtkCommand::InteractionEvent, d->arcObserver);
            this->getInteractor()->AddObserver(vtkCommand::MouseMoveEvent, d->arcObserver);

            d->arcObserver->observerDataAssembly = this;
            d->arcObserver->axlArcPicker = d->axlArcPicker;
            d->arcObserver->observerData_arcSource = d->arcSource;
            d->arcObserver->observerData_arc = d->arc;
            d->arcObserver->axlInteractorStyle = dynamic_cast<axlInteractorStyleSwitch *> (this->getInteractor()->GetInteractorStyle());
        }

        QColor color = d->arc->color();
        this->getActor()->GetProperty()->SetColor(color.redF(), color.greenF(), color.blueF());

        QString shader = d->arc->shader();
        if(!shader.isEmpty())
            this->setShader(shader);

        this->setOpacity(d->arc->opacity());
    }
    else
        qDebug()<< "no axlCircleArc available";
}

void axlActorCircleArc::setDisplay(bool display) {
    axlActor::setDisplay(display);

    if (display && d->centerPointWidget) {
        this->showCircleArcWidget(true);
    }

    if (!display && d->centerPointWidget) {
        this->showCircleArcWidget(false);
    }
}

void axlActorCircleArc::showCircleArcWidget(bool show) {
    if (!d->centerPointWidget) {
        qDebug() << "No actor computed for this axlActorCircleArc.";
        return;
    }

    if (show) {
        d->centerPointWidget->SetEnabled(1);
        d->segmentWidget->SetEnabled(1);
    }

    if (!show) {
        d->centerPointWidget->SetEnabled(0);
        d->segmentWidget->SetEnabled(0);
    }
}

void axlActorCircleArc::setCircleArcWidget(bool arcWidget) {
    if(arcWidget) {
        if (!d->centerPointWidget || !d->segmentWidget) {
            //Center pointWidget
            d->centerPointWidget = vtkPointWidget::New();
            d->centerPointWidget->SetInteractor(this->getInteractor());
            d->centerPointWidget->SetProp3D(this->getActor());
            d->centerPointWidget->PlaceWidget();
            d->centerPointWidget->AllOff();
            d->centerPointWidget->SetTranslationMode(1);

            d->centerPointWidget->SetPosition(d->arc->center()->coordinates());

            //Point1 pointWidget
            d->segmentWidget = vtkLineWidget::New();
            d->segmentWidget->SetInteractor(this->getInteractor());
            d->segmentWidget->SetProp3D(this->getActor());
            d->segmentWidget->PlaceWidget();

            d->segmentWidget->SetPoint1(d->arc->point1()->coordinates());
            d->segmentWidget->SetPoint2(d->arc->point2()->coordinates());
        }

        if(d->arcObserver) {
            d->centerPointWidget->AddObserver(vtkCommand::InteractionEvent, d->arcObserver);
            d->segmentWidget->AddObserver(vtkCommand::InteractionEvent, d->arcObserver);

            d->arcObserver->centerPointWidget = d->centerPointWidget;
            d->arcObserver->segmentWidget = d->segmentWidget;
        }

        // there is always the controlPoints there
        d->centerPointWidget->SetEnabled(true);
        d->segmentWidget->SetEnabled(true);
    }

    if (!arcWidget) {
        if (this->getActor()) {
            if(d->centerPointWidget) {
                d->centerPointWidget->RemoveAllObservers();
                d->centerPointWidget->SetEnabled(false);
                d->centerPointWidget->Delete(); // warning not sure
                d->centerPointWidget = NULL;
            }

            if(d->segmentWidget) {
                d->segmentWidget->RemoveAllObservers();
                d->segmentWidget->SetEnabled(false);
                d->segmentWidget->Delete(); // warning not sure
                d->segmentWidget = NULL;
            }
        }
    }
}

bool axlActorCircleArc::isShowCircleArcWidget(void) {
    if(!d->centerPointWidget || !d->segmentWidget) {
        qDebug() << "No actor computed for this axlActorBSpline.";
        return false;
    }

    return d->centerPointWidget->GetEnabled() && d->segmentWidget->GetEnabled();
}


void axlActorCircleArc::setMode(int state) {
    this->onModeChanged(state);
    emit stateChanged(this->data(), state);
}

void axlActorCircleArc::onModeChanged(int state) {
    if(state == 0) {
        this->setState(axlActor::passive);
        this->getActor()->SetPickable(1);

        if(axlAbstractData *data = dynamic_cast<axlAbstractData *>(this->data())) {
            QColor color = data->color();
            this->getActor()->GetProperty()->SetColor(color.redF(), color.greenF(), color.blueF());
        }
        this->setCircleArcWidget(false);

    } else if(state == 1) {

        this->setState(axlActor::selection);
        this->setCircleArcWidget(false);
        this->getActor()->SetPickable(1);
        vtkProperty *prop = this->getActor()->GetProperty();

        if(axlAbstractData *data = dynamic_cast<axlAbstractData *>(this->data())) {
            QColor color = data->color();
            qreal *h = new qreal(0.0);
            qreal *s = new qreal(0.0);
            qreal *l = new qreal(0.0);
            color.getHslF(h, s, l);
            color.setHslF(*h, *s, *l + (1.0 - *l) / 2.0);
            prop->SetColor(color.redF(), color.greenF(), color.blueF());
            delete l;
            delete s;
            delete h;
        }

    } else if(state == 2) {
        this->setState(axlActor::edition);
        this->setCircleArcWidget(true);
        this->getActor()->SetPickable(0);
        vtkProperty *prop = this->getActor()->GetProperty();
        if(axlAbstractData *data = dynamic_cast<axlAbstractData *>(this->data())) {
            QColor color = data->color();
            qreal *h = new qreal(0.0);
            qreal *s = new qreal(0.0);
            qreal *l = new qreal(0.0);
            color.getHslF(h, s, l);
            color.setHslF(*h, *s, *l + (1.0 - *l)/2.0);
            prop->SetColor(color.redF(), color.greenF(), color.blueF());
            delete l;
            delete s;
            delete h;
        }
    }

    this->Modified();
}


void axlActorCircleArc::onRemoved() {
    //remove line specificity
    if(d->arcObserver) {
        this->getInteractor()->RemoveObservers(vtkCommand::InteractionEvent, d->arcObserver);
        this->getInteractor()->RemoveObservers(vtkCommand::MouseMoveEvent, d->arcObserver);
        d->arcObserver->observerDataAssembly = NULL;
        d->arcObserver->axlArcPicker = NULL;
        d->arcObserver->observerData_arcSource = NULL;
        d->arcObserver->observerData_arc = NULL;
        d->arcObserver->axlInteractorStyle = NULL;
        d->arcObserver->Delete();
        d->arcObserver = NULL;

        d->axlArcPicker->Delete();
        d->axlArcPicker = NULL;

    }
    if(d->arcSource) {
        d->arcSource->Delete();
        d->arcSource = NULL;
    }
    if(d->widget) {
        d->widget = NULL;
    }
    if(d->arc) {
        d->arc = NULL;
    }
    if(d->arcSource) {
        d->arcSource->Delete();
        d->arcSource = NULL;
    }
    if(d->centerPointWidget && d->segmentWidget) {
        this->setCircleArcWidget(false);
        d->centerPointWidget = NULL;
        d->segmentWidget = NULL;
    }

    //remove actor specificity
    this->RemoveAllObservers();
    this->RemovePart(this->getActor());
    this->getActor()->RemoveAllObservers();

    if(axlActorComposite *actorComposite = dynamic_cast<axlActorComposite *>(this->parent()) )
        actorComposite->removeActorReference(this);
}

void axlActorCircleArc::onUpdateGeometry() {
    axlPoint A = *(d->arc->point1()); // First point
    axlPoint B = *(d->arc->point2()); // Second point
    axlPoint C = *(d->arc->center()); // Center of arc

    //Set center and points
    d->arcSource->SetCenter(C.coordinates());
    d->arcSource->SetPoint1(A.coordinates());
    d->arcSource->SetPoint2(B.coordinates());

    if (d->centerPointWidget)
        d->centerPointWidget->SetPosition(C.coordinates());

    if (d->segmentWidget) {
        d->segmentWidget->SetPoint1(A.coordinates());
        d->segmentWidget->SetPoint2(B.coordinates());
    }
}

axlActorCircleArc::axlActorCircleArc(void) : axlActor(), d(new axlActorCircleArcPrivate) {
    d->arc = NULL;
    d->axlArcPicker = NULL;
    d->arcObserver = NULL;
    d->arcSource = NULL;
    d->widget = NULL;
    d->centerPointWidget = NULL;
    d->segmentWidget = NULL;
}

axlActorCircleArc::~axlActorCircleArc(void) {
    delete d;
    d = NULL;
}
