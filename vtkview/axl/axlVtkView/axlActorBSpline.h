/* axlActorBSpline.h ---
 *
 * Author: Meriadeg Perrinel
 * Copyright (C) 2008 - Meriadeg Perrinel, Inria.
 * Created: Fri Dec 17 10:58:38 2010 (+0100)
 * Version: $Id$
 * Last-Updated: Mon Mar 28 17:11:18 2011 (+0200)
 *           By: Julien Wintz
 *     Update #: 12
 */

/* Commentary:
 *
 */

/* Change log:
 *
 */

#ifndef AXLACTORBSPLINE_H
#define AXLACTORBSPLINE_H

#include "axlActor.h"

#include "axlVtkViewPluginExport.h"

class axlControlPointsWidget;

class vtkCommand;
class vtkSphereSource;


class axlActorBSplinePrivate;

class AXLVTKVIEWPLUGIN_EXPORT axlActorBSpline : public axlActor
{

public:
    virtual dtkAbstractData *data(void) = 0;

public:
    virtual void setDisplay(bool display);
    virtual void setMode(int state);
    virtual axlControlPointsWidget *getControlPoints(void);
    virtual void setControlPoints(axlControlPointsWidget *controlPoints);
    virtual void showControlPoints(bool show);

    virtual void setControlPolygon(bool control);
    virtual bool isShowControlPoints(void);

    virtual void addToObserver(int idEvent, vtkCommand *observer);


    virtual void onUpdateGeometry(void) {
        qDebug() << Q_FUNC_INFO <<  "NOT IMPLEMENTED";
    }

    virtual void onUpdateStructure(void) {
        this->setControlPolygon(false);
        this->setControlPolygon(true);
    }

public slots:

    virtual void initCurrentPoint(void);
    virtual void onModeChanged(int state);

    virtual void onControlPointChanged();

    virtual void onRemoved(void);
    virtual void showCurrentPoint(double u, double v, dtkAbstractData *data);
    virtual void moveCurrentPoint(double u, double v, dtkAbstractData *data);
    virtual void hideCurrentPoint(double u, double v, dtkAbstractData *data);

protected:
     axlActorBSpline(void);
    ~axlActorBSpline(void);

private:
    axlActorBSplinePrivate *d;
};

#endif //AXLACTORBSPLINE_H
