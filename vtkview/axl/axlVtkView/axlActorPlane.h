/* axlActorPlane.h ---
 *
 * Author: Meriadeg Perrinel
 * Copyright (C) 2008 - Meriadeg Perrinel, Inria.
 * Created: Fri Dec 17 10:58:38 2010 (+0100)
 * Version: $Id$
 * Last-Updated: Mon Dec 17 14:11:38 2012 (+0100)
 *           By: Julien Wintz
 *     Update #: 5
 */

/* Commentary:
 *
 */

/* Change log:
 *
 */

#ifndef AXLACTORPLANE_H
#define AXLACTORPLANE_H

#include "axlActor.h"
#include <QVTKOpenGLWidget.h>

#include "axlVtkViewPluginExport.h"

class axlPlane;

class vtkPlaneSource;

class axlActorPlanePrivate;

class AXLVTKVIEWPLUGIN_EXPORT axlActorPlane : public axlActor
{
public:
#if (VTK_MAJOR_VERSION <= 5)
    vtkTypeRevisionMacro(axlActorPlane, vtkAssembly);
#endif

    static axlActorPlane *New(void);

public:
   dtkAbstractData *data(void);
   vtkPlaneSource *plane(void);


public:
    virtual void setData(dtkAbstractData *plane1);
    void setMode(int state);
    void setQVTKWidget(QVTKOpenGLWidget *widget);

    void showPlaneWidget(bool show);
    void setPlaneWidget(bool planeWidget);
    void setDisplay(bool display);
    bool isShowPlaneWidget(void);

    void setSize(double size);
    void setRenderer(vtkRenderer *renderer);

public slots:
    void onModeChanged(int state);
    void onRemoved();
    void onUpdateGeometry();

protected:
     axlActorPlane(void);
    ~axlActorPlane(void);

private:
    axlActorPlane(const axlActorPlane&); // Not implemented.
    void operator = (const axlActorPlane&); // Not implemented.

private:
    axlActorPlanePrivate *d;
};

axlAbstractActor *createAxlActorPlane(void);

#endif //AXLACTORPLANE_H
