=====================================
The taxonomy of Axl abstract classes
=====================================


The geometric types in Axl can be grouped into basic data types and abstract data types. The basic data types are comming with their implementation, providing tools
for their construction and manipulations
The abstract data types are interface classes and usally not specialized in the core of Axl. The specialization is provided by external plugins.
We describe the taxonomy of these classes, which all derive from the generic class ``AxlAbstractData``.

Abstract data are types of generic geometric objects which do not have necessarily an implementation. 


AxlAbstractCurve 
================

 - `axlAbstractCurveParametric <../classaxl_abstract_curve_parametric.html>`_: curve parameterized by one real parameter. 

   - `axlAbstractCurveBSpline <../classaxl_abstract_curve_b_spline.html>`_: parametric curve represented in a basis of spline functions, which are piecewise polynomial between nodes.
   - `axlAbstractCurveNurbs <../classaxl_abstract_curve_nurbs.html>`_: parametric curve, which coordinate functions are the ratio of B-spline functions. 
   - `axlAbstractCurveRational <../classaxl_abstract_curve_rational.html>`_: parametric curve, which coordinate functions are the ratio of polynomial functions. 

 - `axlAbstractCurveImplicit <../classaxl_abstract_curve_implicit.html>`_: curve defined by one of several implicit equations.

   - `axlAbstractCurveAlgebraic <../classaxl_abstract_curve_algebraic.html>`_: implicit curve defined by one or two polynomial equations. 


AxlAbstractSurface
==================

 - `axlAbstractSurfaceParametric <../classaxl_abstract_surface_parametric.html>`_: surface parameterized by two real parameters. 

   - `axlAbstractSurfaceBSpline <../classaxl_abstract_surface_b_spline.html>`_: parametric surface represented in a tensor-product basis of spline functions, which are piecewise polynomial functions between a grid of nodes.
   - `axlAbstractSurfaceNurbs <../classaxl_abstract_surface_nurbs.html>`_: parametric surface, which coordinate functions are the ratio of B-spline functions. 
   - `axlAbstractSurfaceRational <../classaxl_abstract_surface_rational.html>`_: parametric surface, which coordinate functions are the ratio of polynomial functions. 

   - `axlAbstractSurfaceTrimmed <../classaxl_abstract_surface_trimmed.html>`_: NURBS surface with trimmed curves in the parameter domain.

 - `axlAbstractSurfaceImplicit <../classaxl_abstract_surface_implicit.html>`_: surface defined by one implicit equations.

   - `axlAbstractSurfaceAlgebraic <../classaxl_abstract_surface_algebraic.html>`_: implicit surface defined by one polynomial equation. 

AxlAbstractVolume
=================

 - `axlAbstractVolumeParametric <../classaxl_abstract_volume_parametric.html>`_: volume parameterized by three real parameters. 

   - `axlAbstractVolumeDiscrete <../classaxl_abstract_volume_discrete.html>`_: 
   - `axlAbstractVolumeBSpline <../classaxl_abstract_volume_b_spline.html>`_: parametric surface represented in a tensor-product basis of spline functions
   - `axlAbstractVolumeNurbs <../classaxl_abstract_volume_nurbs.html>`_: parametric volume, which coordinate functions are the ratio of B-spline functions. 
   - `axlAbstractVolumeRational <../classaxl_abstract_volume_rational.html>`_: parametric volume, which coordinate functions are the ratio of polynomial functions. 
