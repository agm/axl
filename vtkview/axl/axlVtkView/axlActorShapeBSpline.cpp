/* axlActorShapeBSpline.cpp ---
 *
 * Author: Meriadeg Perrinel
 * Copyright (C) 2008 - Meriadeg Perrinel, Inria.
 * Created: Fri Dec 17 11:06:08 2010 (+0100)
 * Version: $Id$
 * Last-Updated: Mon Dec 17 15:04:10 2012 (+0100)
 *           By: Julien Wintz
 *     Update #: 78
 */

/* Commentary:
 *
 */

/* Change log:
 *
 */

#include "axlActorShapeBSpline.h"
#include "axlControlPointsWidget.h"

#include <axlCore/axlShapeBSpline.h>
#include <axlCore/axlAbstractField.h>

#include <dtkMathSupport/dtkVector3D.h>

#include <vtkActor.h>
#include <vtkActorCollection.h>
#include <vtkCellArray.h>
#include <vtkCellData.h>
#include <vtkCommand.h>
#include <vtkDoubleArray.h>
#include <vtkLookupTable.h>
#include <vtkObjectFactory.h>
//#include <vtkPainterPolyDataMapper.h>
#include <vtkPoints.h>
#include <vtkPointData.h>
#include <vtkPolyData.h>
#include <vtkPolyVertex.h>
#include <vtkPolygon.h>
#include <vtkPolyDataMapper.h>
#include <vtkPolyDataNormals.h>
#include <vtkProperty.h>
#include <vtkProp.h>
#include <vtkQuad.h>
#include <vtkTexture.h>
//#include <vtkTriangle.h>
#include <vtkTriangleStrip.h>
#include <vtkTimerLog.h>
#include <vtkSmartPointer.h>
#include <vtkStripper.h>
#include <vtkPointData.h>
#include <vtkTriangleFilter.h>
#include <vtkPNGReader.h>
#include <vtkFloatArray.h>
//#include <vtkOpenGLHardwareSupport.h>
#include <vtkOpenGLRenderWindow.h>
//#include <vtkPainterPolyDataMapper.h>
#include <vtkProperty.h>

#include <vtkFeatureEdges.h>
#include <vtkExtractEdges.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkRendererCollection.h>


//for meshing and create poly data structure.
#include <axlCore/axlAbstractDataConverter.h>
#include <dtkCoreSupport/dtkAbstractDataFactory.h>
#include <axlCore/axlMesh.h>

#include <vtkLine.h>
#include <vtkTriangle.h>

// /////////////////////////////////////////////////////////////////
// axlActorShapeBSplinePrivate
// /////////////////////////////////////////////////////////////////

class axlActorShapeBSplinePrivate
{
public:
    axlShapeBSpline *splineShape;
    vtkSmartPointer<vtkActor> edgeActor;

    vtkSmartPointer<vtkCellArray> lines;
};

#if (VTK_MAJOR_VERSION <= 5)
vtkCxxRevisionMacro(axlActorShapeBSpline, "$Revision: 0.0.1 $");
#endif

vtkStandardNewMacro(axlActorShapeBSpline);

axlActorShapeBSpline::axlActorShapeBSpline(void) : axlActorBSpline(), d(new axlActorShapeBSplinePrivate)
{
    d->splineShape = NULL;
    d->edgeActor = vtkSmartPointer<vtkActor>::New();
    d->edgeActor->SetVisibility(false);
    this->AddPart(d->edgeActor);

    d->lines = vtkSmartPointer<vtkCellArray>::New();

}

axlActorShapeBSpline::~axlActorShapeBSpline(void)
{

    if(d->splineShape->identifier() == "axlShapeBSpline")
        disconnect(d->splineShape,SIGNAL(edgeSelected(int)), this, SLOT(onSelectBoundaryEdge(int)));

    delete d;
    d = NULL;
}

dtkAbstractData *axlActorShapeBSpline::data(void)
{
    return d->splineShape;
}

void axlActorShapeBSpline::setData(dtkAbstractData *spline_shape1)
{
    axlShapeBSpline *spline_Shape = dynamic_cast<axlShapeBSpline *>(spline_shape1);
    // we compute here points, triangles, and normals
    d->splineShape = spline_Shape;

    this->setPoints(vtkSmartPointer<vtkPoints>::New());

    this->setNormals(vtkSmartPointer<vtkDoubleArray>::New());

    // qDebug()<<"axlActorShapeBSpline::setShape 2"<<vtkTimerLog::GetCPUTime()<<vtkTimerLog::GetCPUTime()-currentTime;

    this->setActor(vtkSmartPointer<vtkActor>::New());
    this->setCellArray(vtkSmartPointer<vtkCellArray>::New());
    this->setPolyData(vtkSmartPointer<vtkPolyData>::New());
    this->setMapper(vtkSmartPointer<vtkPolyDataMapper>::New());

    //this->pointsUpdate();
    this->normalsUpdate();
    this->polyDataUpdate();

    this->getPolyData()->SetPoints(this->getPoints());
    //this->getPolyData()->SetLines(d->lines);
    this->getPolyData()->SetPolys(this->getCellArray());
    this->getPolyData()->GetPointData()->SetNormals(this->getNormals());

    //vtkSmartPointer<vtkPolyDataNormals> polyDataNormals = vtkSmartPointer<vtkPolyDataNormals>::New();
    //polyDataNormals->AutoOrientNormalsOn();
    //polyDataNormals->FlipNormalsOn();
    //polyDataNormals->SetInput(this->getPolyData());
#if (VTK_MAJOR_VERSION <= 5)
    this->getMapper()->SetInput(this->getPolyData());
#else
    this->getMapper()->SetInputData(this->getPolyData());
#endif
    this->getActor()->SetMapper(this->getMapper());

    this->getMapper()->ScalarVisibilityOff();

    // add sphere param
    // this->initCurrentPoint();
    //vtkSmartPointer<vtkProp3D> prop3d =  this->getActor();
    this->AddPart(this->getActor());

    // add the observer
    if(!this->getObserver())
    {
        this->NewObserver();
        this->setObserverData(d->splineShape);
    }

    QColor color = d->splineShape->color();
    this->getActor()->GetProperty()->SetColor(color.redF(), color.greenF(), color.blueF());

    QString shader = d->splineShape->shader();
    if(!shader.isEmpty())
        this->setShader(shader);

    connect(d->splineShape, SIGNAL(edgeSelected(int,int,int)), this, SLOT(onSelectBoundaryEdge(int,int,int)));

    // For updating the geometry (ie. the mesh representation of the spline)
    connect(d->splineShape, SIGNAL(modifiedGeometry()), this, SLOT(onUpdateGeometry()));
    connect(d->splineShape, SIGNAL(modifiedProperty()), this, SLOT(onUpdateProperty()));
    // For redrawing the control point widget
    connect(d->splineShape, SIGNAL(modifiedStructure()), this, SLOT(onUpdateStructure()));
}


void axlActorShapeBSpline::setMapperCollorArray(void)
{
    vtkSmartPointer<vtkDoubleArray> scalarArray = vtkSmartPointer<vtkDoubleArray>::New();
    scalarArray->SetName("mapperCollorArrayDefaultField");
    scalarArray->SetNumberOfComponents(1);

    if(d->splineShape->hasFaces()) {

        int addIndex = 0;
        for(int indice = 0; indice < d->splineShape->countFaces(); indice++){

            double start_u = d->splineShape->startParam_u(indice);
            double start_v = d->splineShape->startParam_v(indice);
            double end_u = d->splineShape->endParam_u(indice);
            double end_v = d->splineShape->endParam_v(indice);
            double paramCourant_u = start_u;
            double paramCourant_v = start_v;

            int n_u = d->splineShape->numSamples_u(indice);// need to be superior than 1
            int n_v = d->splineShape->numSamples_v(indice);

            scalarArray->SetNumberOfTuples(n_u * n_v);

            double interval_u = (double)(end_u - start_u) / (n_u - 1);
            double interval_v = (double)(end_v - start_v) / (n_v - 1);

            for(int i = 0; i < n_v - 1 ; i++)
            {
                for(int j = 0; j < n_u - 1 ; j++)
                {
                    scalarArray->SetTuple1(i * n_u + j+addIndex, d->splineShape->scalarValue(paramCourant_u, paramCourant_v));
                    paramCourant_u += interval_u;
                }

                scalarArray->SetTuple1(i * n_u + (n_u - 1)+addIndex, d->splineShape->scalarValue(end_u, paramCourant_v));
                paramCourant_u = start_u;
                paramCourant_v += interval_v;
            }
            for(int i = 0; i < n_u - 1; i++)
            {
                scalarArray->SetTuple1(n_u * (n_v - 1) + i+addIndex, d->splineShape->scalarValue(paramCourant_u, end_v));
                paramCourant_u += interval_u;
            }
            scalarArray->SetTuple1(n_u * n_v - 1+addIndex, d->splineShape->scalarValue(end_u, end_v));


            vtkSmartPointer<vtkPolyData> data = this->getPolyData();
            data->GetPointData()->AddArray(scalarArray);
            data->GetPointData()->SetActiveScalars("mapperCollorArrayDefaultField");

            vtkSmartPointer<vtkLookupTable> lookupTable = vtkSmartPointer<vtkLookupTable>::New();
            lookupTable->SetRange(-1.0, 1.0);
            lookupTable->SetNumberOfTableValues(d->splineShape->stripes());
            lookupTable->Build();
            for(int i= 0; i < d->splineShape->stripes(); i+=2)
            {
                lookupTable->SetTableValue(i  , 1.0, 1.0, 1.0);
                lookupTable->SetTableValue(i+1, 0.0, 0.0, 0.0);
            }
            // test to get light direction .... TO DELETE

            vtkSmartPointer<vtkDoubleArray> lightArray = vtkSmartPointer<vtkDoubleArray>::New();
            lightArray->SetName("mapperLightDirection");
            lightArray->SetNumberOfComponents(3);

            paramCourant_u = start_u;
            paramCourant_v = start_v;

            lightArray->SetNumberOfTuples(n_u * n_v);
            double *currentLight = new double[3];
            currentLight[0] = 1.0;
            currentLight[1] = 0.6;
            currentLight[2] = 0.2;
            for(int j = 0; j < n_v - 1; j++)
            {
                for(int i = 0; i < n_u - 1; i++)
                {
                    lightArray->SetTuple(j * n_u + i+addIndex, currentLight);
                }

                lightArray->SetTuple(j * n_u +  n_u - 1+addIndex, currentLight);
                paramCourant_u = start_u;
                paramCourant_v += interval_v;
            }

            for(int i = 0; i < n_u - 1; i++)
            {
                lightArray->SetTuple((n_v - 1) * n_u + i+addIndex, currentLight);
                paramCourant_u += interval_u;
            }
            lightArray->SetTuple(n_v * n_u - 1+addIndex, currentLight);

            data = this->getPolyData();
            data->GetPointData()->AddArray(lightArray);


            //add vertex attrib array
            vtkSmartPointer<vtkPolyDataMapper> mapper = this->getMapper();
            vtkSmartPointer<vtkPolyData> polyData = this->getPolyData();
            mapper->MapDataArrayToVertexAttribute("scalarsattrib",  data->GetPointData()->GetArrayName(2), vtkDataObject::FIELD_ASSOCIATION_POINTS, -1);
            mapper->SetLookupTable(lookupTable);
            mapper->SetInterpolateScalarsBeforeMapping(true);
            mapper->UseLookupTableScalarRangeOn();
            addIndex = addIndex + n_u*n_v;

        }

    }

    this->Modified();
}


void axlActorShapeBSpline::onSamplingChanged(void)
{

    if(d->splineShape)
    {
        this->getMapper()->RemoveAllInputs();
        this->getPolyData()->Initialize();

        // delete current vtkPoint and vtkCellArray
        this->getPoints()->Reset();
        this->getCellArray()->Reset();

        this->getPoints()->Squeeze();
        this->getCellArray()->Squeeze();

        this->getNormals()->Initialize();
        //this->getPolyData()->GetPointData()->RemoveArray("mapperCollorArrayIsophote");//back to solid color

        //this->pointsUpdate();
        this->normalsUpdate();

        this->getPolyData()->GetPointData()->SetNormals(this->getNormals());

        this->polyDataUpdate();


        this->getPolyData()->SetPoints(this->getPoints());
        this->getPolyData()->SetPolys(this->getCellArray());

#if (VTK_MAJOR_VERSION <= 5)
        this->getMapper()->SetInput(this->getPolyData());
#else
        this->getMapper()->SetInputData(this->getPolyData());
#endif

    }

    if(d->splineShape->fields().count() != 0){
        d->splineShape->touchField();
        d->splineShape->touchGeometry();
    }

}

void axlActorShapeBSpline::normalsUpdate(void){
    // we consider here that all memory vtk pipeline from points to polydata are free but not deleted...
    if(d->splineShape->hasFaces()) {

        int numCell = d->splineShape->countFaces();
        int addIndex = 0;
        int dataArraysSize =0;

        for(int in = 0; in <numCell;in++){
            int n_u = d->splineShape->numSamples_u(in);// need to be superior than 1
            int n_v = d->splineShape->numSamples_v(in);
            dataArraysSize = dataArraysSize + n_u*n_v;

        }

        vtkSmartPointer<vtkDoubleArray> normals = this->getNormals();
        normals->SetNumberOfComponents(3);
        normals->SetNumberOfTuples(dataArraysSize);
        normals->SetName("normalArray");

        for(int indice =0;indice < numCell;indice ++){

            double start_u = d->splineShape->startParam_u(indice);
            double start_v = d->splineShape->startParam_v(indice);
            double end_u = d->splineShape->endParam_u(indice);
            double end_v = d->splineShape->endParam_v(indice);
            double paramCourant_u = start_u;
            double paramCourant_v = start_v;

            int n_u = d->splineShape->numSamples_u(indice);// need to be superior than 1
            int n_v = d->splineShape->numSamples_v(indice);


            double interval_u = (double)(end_u - start_u) / (n_u - 1);
            double interval_v = (double)(end_v - start_v) / (n_v - 1);

            axlPoint *currentNormal = new axlPoint(0.,0.,1.);

            //double currentTime = vtkTimerLog::GetCPUTime();

            // qDebug()<<"axlActorShapeBSpline::setShape 1" <<currentTime;
            int ind1 = 0;
            int ind2 = 0;
            for(int i = 0; i < n_v - 1; i++)
            {
                ind1 = i * n_u;
                for(int j = 0; j < n_u - 1; j++)
                {
                    ind2 = ind1 +j;

                    d->splineShape->normal(currentNormal ,paramCourant_u, paramCourant_v,indice);
                    currentNormal->normalize();

                    normals->SetTuple(ind2+addIndex, currentNormal->coordinates());

                    paramCourant_u += interval_u;
                }
                ind2 = ind1 + (n_u - 1);

                d->splineShape->normal(currentNormal, end_u, paramCourant_v,indice);
                currentNormal->normalize();

                normals->SetTuple(ind2+addIndex, currentNormal->coordinates());

                paramCourant_u = start_u;
                paramCourant_v += interval_v;
            }
            ind1 = n_u * (n_v - 1);
            for(int i = 0; i < n_u - 1; i++)
            {
                ind2 = ind1 + i;

                d->splineShape->normal(currentNormal, paramCourant_u, end_v,indice);
                currentNormal->normalize();

                normals->SetTuple(ind2 +addIndex, currentNormal->coordinates());

                paramCourant_u+=interval_u;
                //qDebug()<< "ind 3"<<ind3;

            }
            ind1 = n_u * n_v - 1;

            d->splineShape->normal(currentNormal, end_u, end_v,indice);
            currentNormal->normalize();

            normals->SetTuple(ind1+addIndex, currentNormal->coordinates());

            delete currentNormal;

            addIndex = addIndex + n_u*n_v;

        }
    }
}

void axlActorShapeBSpline::pointsUpdate(void)
{
    // we consider here that all memory vtk pipeline from points to polydata are free but not deleted...
    if(d->splineShape->hasFaces()){

        int numCell = d->splineShape->countFaces();
        int addIndex = 0;
        int dataArraysSize =0;

        for(int in = 0; in <numCell;in++){
            int n_u = d->splineShape->numSamples_u(in);// need to be superior than 1
            int n_v = d->splineShape->numSamples_v(in);
            dataArraysSize = dataArraysSize + n_u*n_v;

        }

        vtkPoints *points = this->getPoints();
        points->SetNumberOfPoints(dataArraysSize);

        for(int indice =0;indice < numCell;indice ++){

            double start_u = d->splineShape->startParam_u(indice);
            double start_v = d->splineShape->startParam_v(indice);
            double end_u = d->splineShape->endParam_u(indice);
            double end_v = d->splineShape->endParam_v(indice);
            double paramCourant_u = start_u;
            double paramCourant_v = start_v;

            int n_u = d->splineShape->numSamples_u(indice);// need to be superior than 1
            int n_v = d->splineShape->numSamples_v(indice);

            axlPoint *pointCourant = new axlPoint(0.,0.,0.);
            double interval_u = (double)(end_u - start_u) / (n_u - 1);
            double interval_v = (double)(end_v - start_v) / (n_v - 1);


            //double currentTime = vtkTimerLog::GetCPUTime();

            // qDebug()<<"axlActorShapeBSpline::setShape 1" <<currentTime;
            int ind1 = 0;
            int ind2 = 0;
            for(int i = 0; i < n_v - 1; i++)
            {
                ind1 = i * n_u;
                for(int j = 0; j < n_u - 1; j++)
                {
                    ind2 = ind1 +j;
                    d->splineShape->eval(pointCourant, paramCourant_u, paramCourant_v, indice);
                    points->SetPoint(ind2+addIndex, pointCourant->coordinates());


                    paramCourant_u += interval_u;
                }
                ind2 = ind1 + (n_u - 1);

                d->splineShape->eval(pointCourant, end_u, paramCourant_v,indice);
                points->SetPoint(ind2+addIndex , pointCourant->coordinates());


                paramCourant_u = start_u;
                paramCourant_v += interval_v;
            }
            ind1 = n_u * (n_v - 1);
            for(int i = 0; i < n_u - 1; i++)
            {
                ind2 = ind1 + i;
                d->splineShape->eval(pointCourant, paramCourant_u, end_v,indice);
                points->SetPoint(ind2 +addIndex, pointCourant->coordinates());


                paramCourant_u+=interval_u;
                //qDebug()<< "ind 3"<<ind3;

            }
            ind1 = n_u * n_v - 1;
            d->splineShape->eval(pointCourant, end_u, end_v,indice);
            points->SetPoint(ind1+addIndex, pointCourant->coordinates());

            delete pointCourant;

            addIndex = addIndex + n_u*n_v;

        }
    }

}

void axlActorShapeBSpline::polyDataUpdate(void){

    vtkSmartPointer<vtkCellArray> cellArray = vtkSmartPointer<vtkCellArray>::New();
    //this->getCellArray();
    vtkSmartPointer<vtkCellArray> linesArray = d->lines;
    //vtkSmartPointer<vtkPoints> pointLines = d->linePoints;

    this->pointsUpdate();
    if(d->splineShape->hasFaces()) {

        int addIndex = 0;
        for(int indice = 0;indice < d->splineShape->countFaces();indice++){
            int n_u = d->splineShape->numSamples_u(indice);
            int n_v = d->splineShape->numSamples_v(indice);

            int ind1 = 0;
            int ind2 = 0;

            vtkSmartPointer<vtkQuad> currentQuad = vtkSmartPointer<vtkQuad>::New();
            currentQuad->GetPointIds()->SetNumberOfIds(4);

            for(int j = 0; j < n_v - 1; j++)
            {
                for(int i= 0; i < n_u - 1; i++)
                {
                    ind1 =  j * n_u + i + addIndex;
                    ind2 = ind1 + n_u ;

                    currentQuad->GetPointIds()->SetId(0, ind1);
                    currentQuad->GetPointIds()->SetId(1, ind1 + 1);
                    currentQuad->GetPointIds()->SetId(2, ind2 + 1);
                    currentQuad->GetPointIds()->SetId(3, ind2);

                    cellArray->InsertNextCell(currentQuad);
                }
            }
            addIndex = addIndex + n_u*n_v;
        }
    }

    this->setCellArray(cellArray);
}

void axlActorShapeBSpline::onUpdateGeometry()
{
    // this->pointsUpdate();
    // this->normalsUpdate();
    // this->polyDataUpdate();

    // Note: ideally we should not need to call this ?
    this->onSamplingChanged();
}


void axlActorShapeBSpline::onSelectBoundaryEdge(int numEdge, int previous, int n)
{
    if(numEdge == -1){
        if(d->edgeActor)
            d->edgeActor->SetVisibility(false);
    }else{

        vtkSmartPointer<vtkPolyData> selectedEdge = vtkSmartPointer<vtkPolyData>::New();
        selectedEdge->SetPoints(this->getPoints());
        selectedEdge->SetLines(d->lines);
        int ne = selectedEdge->GetNumberOfLines();

        selectedEdge->BuildLinks();
        for(int i = 0; i < previous;i++){
            selectedEdge->DeleteCell(i);
        }
        for(int i = previous-1+n; i < ne;i++){
            selectedEdge->DeleteCell(i);
        }
        selectedEdge->RemoveDeletedCells();

        // Visualize
        vtkSmartPointer<vtkPolyDataMapper> edgeMapper =
                vtkSmartPointer<vtkPolyDataMapper>::New();
#if (VTK_MAJOR_VERSION <= 5)
        edgeMapper->SetInput(selectedEdge);
#else
        edgeMapper->SetInputData(selectedEdge);
#endif

        d->edgeActor->SetMapper(edgeMapper);
        d->edgeActor->GetProperty()->EdgeVisibilityOn();
        d->edgeActor->GetProperty()->SetEdgeColor(1,0,0);

        d->edgeActor->GetProperty()->SetLineWidth(6);
        d->edgeActor->SetVisibility(true);
    }

    emit updated();


}

axlAbstractActor *createAxlActorShapeBSpline(void){

    return axlActorShapeBSpline::New();
}
