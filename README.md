
Axl is an algebraic geometric modeler for the manipulation and computation with curves, surfaces or volumes described by semi-algebraic representations, such as mesh, splines, algebraic curves and surfaces.

# Download

- [Binary distributions](https://gitlab.inria.fr/agm/axl/wikis/Download-binaries) are available on the channel [agm-distrib](https://anaconda.org/agm-distrib/axl) of anaconda.org, for linux64, osx64 and win64. 

# Configuration and compilation

- [Get the source code](https://gitlab.inria.fr/agm/axl/wikis/Download-sources) 
- [Requirements for the source code compilation](https://gitlab.inria.fr/agm/axl/wikis/Requirements). 
- [Compilation of the source code](https://gitlab.inria.fr/agm/axl/wikis/Compilation-of-sources). 


# Documentation

- [LATEST](http://axl.inria.fr/)


# Web sites

- [axl.inria.fr](http://axl.inria.fr/) 
- [gitlab](http://gitlab.inria.fr/agm/axl) 
- [Wiki](https://gitlab.inria.fr/agm/axl/wikis/home)

