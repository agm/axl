/* axlActorCurveBSpline.cpp ---
 *
 * Author: Meriadeg Perrinel
 * Copyright (C) 2008 - Meriadeg Perrinel, Inria.
 * Created: Fri Dec 17 11:06:08 2010 (+0100)
 * Version: $Id$
 * Last-Updated: Mon Dec 17 15:03:49 2012 (+0100)
 *           By: Julien Wintz
 *     Update #: 71
 */

/* Commentary:
 *
 */

/* Change log:
 *
 */

#include "axlActorCurveBSpline.h"
#include "axlControlPointsWidget.h"

#include <axlCore/axlAbstractCurveBSpline.h>

#include <vtkActor.h>
#include <vtkActorCollection.h>
#include <vtkCellArray.h>
#include <vtkCommand.h>
#include <vtkLine.h>
#include <vtkObjectFactory.h>
#include <vtkPoints.h>
#include <vtkPolyLine.h>
#include <vtkPolyData.h>
#include <vtkPolyDataMapper.h>
#include <vtkProperty.h>
#include <vtkSmartPointer.h>
#include <vtkTubeFilter.h>

// /////////////////////////////////////////////////////////////////
// vtk related
// /////////////////////////////////////////////////////////////////
#if (VTK_MAJOR_VERSION <= 5)
vtkCxxRevisionMacro(axlActorCurveBSpline, "$Revision: 0.0.1 $");
#endif

vtkStandardNewMacro(axlActorCurveBSpline);

// /////////////////////////////////////////////////////////////////
// axlActorCurveBSplinePrivate
// /////////////////////////////////////////////////////////////////

class axlActorCurveBSplinePrivate
{
public:
    axlAbstractCurveBSpline *splineCurve;
    vtkSmartPointer<vtkTubeFilter> splineCurveTubeFilter;
};

// /////////////////////////////////////////////////////////////////
// axlActorCurveBSpline
// /////////////////////////////////////////////////////////////////
axlActorCurveBSpline::axlActorCurveBSpline(void) : axlActorBSpline(), d(new axlActorCurveBSplinePrivate)
{
    d->splineCurveTubeFilter = vtkSmartPointer<vtkTubeFilter>::New();
    d->splineCurveTubeFilter->SetNumberOfSides(8);
}

axlActorCurveBSpline::~axlActorCurveBSpline(void)
{
    delete d;

    d = NULL;
}

dtkAbstractData *axlActorCurveBSpline::data(void)
{
    return d->splineCurve;
}

void axlActorCurveBSpline::setData(dtkAbstractData *spline_curve1)
{
    axlAbstractCurveBSpline *spline_curve = dynamic_cast<axlAbstractCurveBSpline *>(spline_curve1);
    d->splineCurve = spline_curve;

    this->setPoints(vtkSmartPointer<vtkPoints>::New());
    this->setMapper(vtkSmartPointer<vtkPolyDataMapper>::New());
    this->setActor(vtkSmartPointer<vtkActor>::New());
    this->setPolyData(vtkSmartPointer<vtkPolyData>::New());
    this->setCellArray(vtkSmartPointer<vtkCellArray>::New());

    this->pointsUpdate();
    this->polyDataUpdate();

    this->getPolyData()->SetPoints(this->getPoints());
    this->getPolyData()->SetLines(this->getCellArray());

#if (VTK_MAJOR_VERSION <= 5)
    this->getMapper()->SetInput(this->getPolyData());
#else
    this->getMapper()->SetInputData(this->getPolyData());
#endif
    this->getActor()->SetMapper(this->getMapper());

    // add sphere param
    //this->initCurrentPoint();

    if(!this->getObserver())
    {
        this->NewObserver();
        this->setObserverData(d->splineCurve);
    }

    this->AddPart(this->getActor());

    QColor color = d->splineCurve->color();
    this->getActor()->GetProperty()->SetColor(color.redF(), color.greenF(), color.blueF());

    QString shader = d->splineCurve->shader();
    if(!shader.isEmpty())
        this->setShader(shader);

    // refresh the actor
    this->onUpdateGeometry();

    // signals connecting
    connect(d->splineCurve, SIGNAL(modifiedGeometry()), this, SLOT(onUpdateGeometry()));
    connect(d->splineCurve, SIGNAL(modifiedProperty()), this, SLOT(onUpdateProperty()));
}

void axlActorCurveBSpline::onUpdateGeometry()
{
    this->pointsUpdate();
    this->polyDataUpdate();

    this->onSamplingChanged();

#if (VTK_MAJOR_VERSION <= 5)
    d->splineCurveTubeFilter->SetInput(this->getPolyData());
    this->getMapper()->SetInput(d->splineCurveTubeFilter->GetOutput());
#else
    d->splineCurveTubeFilter->SetInputData(this->getPolyData());
    this->getMapper()->SetInputData(d->splineCurveTubeFilter->GetOutput());
#endif
    d->splineCurveTubeFilter->SetRadius(d->splineCurve->size());
    d->splineCurveTubeFilter->Update();
}


void axlActorCurveBSpline::pointsUpdate(void)
{
    //std::cout<<"axlActorCurveBSpline::pointsUpdate(void)"<<std::endl;

//update rcoeff if the curve is a nurbs
    if(d->splineCurve->rational())
    {
        d->splineCurve->updateRcoeff();

    }

    double start = d->splineCurve->startParam();
    double end = d->splineCurve->endParam();
    double paramCourant = start;

    vtkPoints *points = this->getPoints();

    axlPoint *pointCourant = new axlPoint;

    int n = d->splineCurve->numSamples();
    points->SetNumberOfPoints(n);

    double interval = (double)(end-start) / (n -1);

    for (int i = 0; i < n-1; i++) {
        d->splineCurve->eval(pointCourant, paramCourant);
        points->SetPoint(i, pointCourant->coordinates());
        paramCourant += interval;
    }

    // boundary traitement
    d->splineCurve->eval(pointCourant, end);
    points->SetPoint(n - 1, pointCourant->coordinates());

    delete pointCourant;
}

void axlActorCurveBSpline::polyDataUpdate()
{
    int n = d->splineCurve->numSamples();

    vtkSmartPointer<vtkLine> currentline = vtkSmartPointer<vtkLine>::New();
    currentline->GetPointIds()->SetNumberOfIds(2);

    for(int i= 0; i < n - 1; i++) {
        currentline->GetPointIds()->SetId(0 , i);
        currentline->GetPointIds()->SetId(1 , i + 1);
        this->getCellArray()->InsertNextCell(currentline);
    }
}

void axlActorCurveBSpline::onSamplingChanged(void)
{
    if (d->splineCurve) {
        // delete current vtkPoint and vtkCellArray
        this->getMapper()->RemoveAllInputs();
        this->getPolyData()->Initialize();

        // delete current vtkPoint and vtkCellArray
        this->getPoints()->Reset();
        this->getCellArray()->Reset();

        this->getPoints()->Squeeze();
        this->getCellArray()->Squeeze();

        this->pointsUpdate();
        this->polyDataUpdate();

        this->getPolyData()->SetPoints(this->getPoints());
        this->getPolyData()->SetLines(this->getCellArray());

//#if (VTK_MAJOR_VERSION <= 5)
//        this->getMapper()->SetInput(this->getPolyData());
//#else
//        this->getMapper()->SetInputData(this->getPolyData());
//#endif
#if (VTK_MAJOR_VERSION <= 5)
        d->splineCurveTubeFilter->SetInput(this->getPolyData());
        this->getMapper()->SetInput(d->splineCurveTubeFilter->GetOutput());
#else
        d->splineCurveTubeFilter->SetInputData(this->getPolyData());
        this->getMapper()->SetInputData(d->splineCurveTubeFilter->GetOutput());
#endif
    
        d->splineCurveTubeFilter->SetRadius(d->splineCurve->size());
        d->splineCurveTubeFilter->Update();

        if (d->splineCurve->fields().count() != 0)
            d->splineCurve->touchField();
    }

}

void axlActorCurveBSpline::onTubeFilterRadiusChanged(double radius)
{

#if (VTK_MAJOR_VERSION <= 5)
        d->splineCurveTubeFilter->SetInput(this->getPolyData());
        this->getMapper()->SetInput(d->splineCurveTubeFilter->GetOutput());
#else
        d->splineCurveTubeFilter->SetInputData(this->getPolyData());
        this->getMapper()->SetInputData(d->splineCurveTubeFilter->GetOutput());
#endif

        d->splineCurveTubeFilter->SetRadius(radius);
//        d->splineCurveTubeFilter->SetRadius(radius * side);
        d->splineCurveTubeFilter->Update();

        if (this->getControlPoints())
            this->getControlPoints()->SetControlPointRadius(radius);
}

vtkPolyData *axlActorCurveBSpline::getCurveMapperInput(void)
{
        return d->splineCurveTubeFilter->GetOutput();
}

axlAbstractActor *createAxlActorCurveBSpline(void)
{
    return axlActorCurveBSpline::New();
}
