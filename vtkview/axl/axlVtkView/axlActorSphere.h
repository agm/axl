/* axlActorSphere.h ---
 *
 * Author: Meriadeg Perrinel
 * Copyright (C) 2008 - Meriadeg Perrinel, Inria.
 * Created: Fri Dec 17 10:58:38 2010 (+0100)
 * Version: $Id$
 * Last-Updated: Mon Dec 17 14:11:57 2012 (+0100)
 *           By: Julien Wintz
 *     Update #: 5
 */

/* Commentary:
 *
 */

/* Change log:
 *
 */

#ifndef AXLACTORSPHERE_H
#define AXLACTORSPHERE_H

#include "axlActor.h"
#include <QVTKOpenGLWidget.h>

#include "axlVtkViewPluginExport.h"

class axlSphere;

class vtkSphereSource;

class axlActorSpherePrivate;

class AXLVTKVIEWPLUGIN_EXPORT axlActorSphere : public axlActor
{
public:
#if (VTK_MAJOR_VERSION <= 5)
    vtkTypeRevisionMacro(axlActorSphere, vtkAssembly);
#endif

    static axlActorSphere *New(void);

public:
   dtkAbstractData *data(void);
   vtkSphereSource *sphere(void);


public:
    virtual void setData(dtkAbstractData *sphere1);
    void setMode(int state);
    void setQVTKWidget(QVTKOpenGLWidget *widget);

    void showSphereWidget(bool sphereWidget);
    bool isShowSphereWidget(void);
    void setDisplay(bool display);
    void setSphereWidget(bool sphereWidget);
public slots:
    void onModeChanged(int state);
    void onRemoved();
    void onUpdateGeometry(void);

protected:
     axlActorSphere(void);
    ~axlActorSphere(void);

private:
    axlActorSphere(const axlActorSphere&); // Not implemented.
    void operator = (const axlActorSphere&); // Not implemented.

private:
    axlActorSpherePrivate *d;
};

axlAbstractActor *createAxlActorSphere(void);

#endif //AXLACTORSPHERE_H
