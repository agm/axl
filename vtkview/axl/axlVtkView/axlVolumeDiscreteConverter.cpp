/* axlVolumeDiscreteConverter.cpp ---
 *
 * Author: Anais Ducoffe
 * Copyright (C) 2013 - Anais Ducoffe, Inria.
 */

/* Commentary:
 *
 */

/* Change log:
 *
 */

#include "axlVolumeDiscreteConverter.h"
#include "axlVolumeDiscrete.h"
#include <dtkCoreSupport/dtkAbstractDataFactory.h>
#include <axlCore/axlMesh.h>
#include <axlCore/axlPoint.h>

#include <vtkImageData.h>
#include <vtkIdList.h>
#include <vtkCell.h>
#include <vtkPoints.h>

#include <algorithm>
#include <QVector>

#include "axlVtkViewPlugin.h"

//#namespace std

// ///////////////////////////////////////////////////////////////////
// axlVolumeDiscreteConverterPrivate
// ///////////////////////////////////////////////////////////////////

class axlVolumeDiscreteConverterPrivate
{
public:
    axlVolumeDiscrete *data;
};
// ///////////////////////////////////////////////////////////////////
// axlVolumeDiscreteConverter implementation
// ///////////////////////////////////////////////////////////////////

axlVolumeDiscreteConverter::axlVolumeDiscreteConverter(void) : axlAbstractDataConverter(), d(new axlVolumeDiscreteConverterPrivate)
{
    d->data = NULL;
}

axlVolumeDiscreteConverter::~axlVolumeDiscreteConverter(void)
{

    delete d;
    d = NULL;
}

QString axlVolumeDiscreteConverter::identifier(void) const
{
    return "axlVolumeDiscreteConverter";
}

QString axlVolumeDiscreteConverter::description(void) const
{
    return "Converter from axlVolume to axlMesh";
}

QStringList axlVolumeDiscreteConverter::fromTypes(void) const
{
    return QStringList() << "axlVolumeDiscreteConverter" << "axlVolume" ;
}

QString axlVolumeDiscreteConverter::toType (void) const
{
    return "axlMesh";
}

bool axlVolumeDiscreteConverter::registered(void)
{
    return axlVtkViewPlugin::dataFactSingleton->registerDataConverterType("axlVolumeDiscreteConverter", QStringList(), "axlMesh", createaxlVolumeDiscreteConverter);
}

axlMesh *axlVolumeDiscreteConverter::toMesh(void)
{
    if(!d->data)
        return NULL;

    axlMesh *mesh = new axlMesh;
    vtkImageData *grid = static_cast<vtkImageData *>(d->data->data());

    //grid points list
    QVector<axlPoint *> listPoints;
    int nbPoints = grid->GetNumberOfPoints();

    for(int i = 0; i <nbPoints;i++){
        axlPoint *point = new axlPoint(grid->GetPoint(i));
        listPoints << point;
    }

    mesh->setVertices(listPoints);


    QVector<int> listFaces;
    QVector<int> listEdges;

    vtkCell *currentCell;
    vtkCell *currentFace;
    vtkCell *currentEdge;
    vtkPoints *list;
    int id= 0;
    int points = 0;
    int faces = 0;
    int edges = 0;

    int first = 0;
    int second = 0;
    int third = 0;
    int last = 0;

    int nbCells = grid->GetNumberOfCells();
    for(int ic = 0 ; ic < nbCells ; ic++){
        currentCell  = grid->GetCell(ic);
        faces = currentCell->GetNumberOfFaces();
        edges = currentCell->GetNumberOfEdges();

        //fill faces for this cell
        for(int jf= 0 ;jf < faces;jf++){
            currentFace = currentCell->GetFace(jf);
            list = currentFace->GetPoints();
            points = list->GetNumberOfPoints();
            if(!listFaces.empty()){
                listFaces.clear();
            }
            for (int jp = 0; jp < points; jp ++){
                id = currentFace->GetPointId(jp);
                listFaces.append(id);
            }
            //order ids
            std::vector<int> vectorFace = listFaces.toStdVector();
            std::sort (vectorFace.begin(), vectorFace.end());
            listFaces = QVector<int>::fromStdVector(vectorFace);
            first = listFaces.first();
            second = listFaces.at(1);
            third = listFaces.at(2);
            last = listFaces.last();

            mesh->push_back_face(first,second,third);
            mesh->push_back_face(second,third,last);
        }

        //fill edges for this cell
        for(int je= 0 ;je < edges;je++){
            currentEdge = currentCell->GetEdge(je);
            list = currentEdge->GetPoints();
            points = list->GetNumberOfPoints();
            if(!listEdges.empty()){
                listEdges.clear();
            }
            for (int jp = 0; jp < points; jp ++){
                id = currentEdge->GetPointId(jp);
                listEdges.append(id);
            }
            mesh->push_back_edge(listEdges.first(), listEdges.last());
        }
    }

    mesh->noDuplicateVertices();

    return mesh;
}

void axlVolumeDiscreteConverter::setData(dtkAbstractData *data)
{
    if(axlVolumeDiscrete *volData = dynamic_cast<axlVolumeDiscrete *>(data)){
        d->data = volData;
    }
}



dtkAbstractDataConverter *createaxlVolumeDiscreteConverter(void){
    return new axlVolumeDiscreteConverter;
}
