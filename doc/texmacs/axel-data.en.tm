<TeXmacs|1.99.1>

<style|<tuple|mmxdoc|mathemagix|doxygen>>

<\body>
  <doc-data|<doc-title|The geometric objects of <name|Axl>>>

  <subsection|Linear objects>

  <subsubsection|Point>

  A point is defined by 3 coordinates (<verbatim|double>).

  <strong|Type:> <verbatim|axlPoint>

  <strong|Format:>

  <\cpp-code>
    \<less\>point\<gtr\>0 0 0\<less\>/point\<gtr\>
  </cpp-code>

  <subsubsection|Line>

  A line (<strong|>or segment) is defined by a starting point
  (<verbatim|axlPoint>) and an ending point (<verbatim|axlPoint>).

  <strong|Type:> <verbatim|axlLine>

  <strong|Format:>

  <\cpp-code>
    \<less\>line\<gtr\>

    \ \ \ \<less\>point\<gtr\>0 0 0\<less\>/point\<gtr\>

    \ \ \ \<less\>point\<gtr\>1 0 0\<less\>/point\<gtr\>

    \<less\>/line\<gtr\>
  </cpp-code>

  <subsubsection|Plane>

  A plane is defined by an <with|font-series|bold|>origin
  (<verbatim|axlPoint>) and a normal vector (<verbatim|axlPoint>).

  <strong|Type:> <verbatim|axlPlane>

  <strong|Format:>

  <\cpp-code>
    \<less\>plane\<gtr\>

    \ \ \ \<less\>point\<gtr\>0 0 0\<less\>/point\<gtr\>

    \ \ \ \<less\>point\<gtr\>1 0 0\<less\>/point\<gtr\>

    \<less\>/plane\<gtr\>
  </cpp-code>

  <with|font-series|bold|><subsection|Algebraic objects>

  <subsubsection|Circle Arc>

  \ A circle arc is defined by a starting point (<verbatim|axlPoint>), an
  ending point (<verbatim|axlPoint>), a normal to the plane containing the
  arc, \ a center (<verbatim|axlPoint>) or a radius (<verbatim|double>) and
  an orientation (interior or exterior).<with|font-series|bold|>\ 

  <strong|Type:> <verbatim|axlCircleArc>

  <strong|Format:>

  <\cpp-code>
    \<less\>arc\<gtr\>

    \ \ \ \<less\>point1\<gtr\>-2 0 0\<less\>/point1\<gtr\>

    \ \ \ \<less\>point2\<gtr\>0 2 0\<less\>/point2\<gtr\>

    \ \ \ \<less\>center\<gtr\>0 0 0\<less\>/center\<gtr\>

    \ \ \ \<less\>normal\<gtr\>0 0 1\<less\>/normal\<gtr\>

    \ \ \ \<less\>ccw\<gtr\>1\<less\>/ccw\<gtr\>

    \<less\>/arc\<gtr\>
  </cpp-code>

  <subsubsection|Sp<strong|>here>

  A sphere is defined by a center (<verbatim|axlPoint>) and a radius
  (<verbatim|double>).

  <strong|Type:> <verbatim|axlSphere>

  <strong|Format:>

  <\cpp-code>
    \<less\>sphere\<gtr\>

    \ \ \ \<less\>center\<gtr\>0 0 0\<less\>/center\<gtr\>

    \ \ \ \<less\>radius\<gtr\>1\<less\>/radius\<gtr\>

    \<less\>/sphere\<gtr\>
  </cpp-code>

  <center|<image|img/sphere.png|60ex|||>>

  <subsubsection|Cylinder>

  A cylinder is defined by a starting point (<verbatim|axlPoint>) and an
  ending point (<verbatim|axlPoint>) and a radius
  (<verbatim|double>).<strong|>

  <center|<image|img/cylinder.png|90ex|||><with|font-series|bold|>>

  <tabbed|<tformat|<table|<row|<cell|<strong|Type:>
  <verbatim|axlCylinder>>>>>>

  <strong|Format:>

  <\cpp-code>
    \<less\>cylinder\<gtr\>

    \ \ \ \<less\>point\<gtr\>0 0 0\<less\>/point\<gtr\>

    \ \ \ \<less\>point\<gtr\>5 4 3\<less\>/point\<gtr\>

    \ \ \ \<less\>radius\<gtr\>2\<less\>/radius\<gtr\>

    \<less\>/cylinder\<gtr\>
  </cpp-code>

  <subsubsection|Cone>

  A cone is defined by a base point (<verbatim|axlPoint>) and an apex point
  (<verbatim|axlPoint>) and a radius (<verbatim|double>) of the base circle.

  <center|<image|img/cone.png|90ex|||>>

  \;

  <tabbed|<tformat|<table|<row|<\cell>
    <tabbed|<tformat|<table|<row|<cell|<strong|Type:> <verbatim|axlCone>>>>>>
  </cell>>>>>

  <tabbed|<tformat|<table|<row|<\cell>
    <strong|Format:>
  </cell>>>>>

  <\cpp-code>
    \<less\>cone\<gtr\>

    \ \ \ \<less\>point\<gtr\>0 0 0\<less\>/point\<gtr\>

    \ \ \ \<less\>point\<gtr\>0 0 2\<less\>/point\<gtr\>

    \ \ \ \<less\>radius\<gtr\>1\<less\>/radius\<gtr\>

    \<less\>/cone\<gtr\>
  </cpp-code>

  <subsubsection|Ellipsoid>

  An ellipsoid is defined by a center (<verbatim|axlPoint>), 3 orthogonal
  vectors (<verbatim|axlPoint>) corresponding to the semi-axes of the
  ellipsoid.

  <center|<image|img/ellipsoid.png|90ex|||>>

  <tabbed|<tformat|<table|<row|<cell|<strong|Type:>
  <verbatim|axlEllipsoid><strong|>>>>>>

  <strong|Format:>

  <\cpp-code>
    \<less\>ellipsoid\<gtr\>

    \ \ \ \<less\>center\<gtr\>1 2 0\<less\>/center\<gtr\>

    \ \ \ \<less\>semix\<gtr\>0.6 0 0 \<less\>/semix\<gtr\>

    \ \ \ \<less\>semiy\<gtr\>0 0.4 0\<less\>/semiy\<gtr\>

    \ \ \ \<less\>semiz\<gtr\>0 0 0.6\<less\>/semiz\<gtr\>

    \<less\>/ellipsoid\<gtr\>
  </cpp-code>

  <subsubsection|Torus>

  A torus is defined by a center (<verbatim|axlPoint>), a normal
  (<verbatim|axlPoint>) to the plane of the big circle, a ring radius
  (<verbatim|double>) and a cross-section radius (<verbatim|double>).

  <center|<image|img/torus.png|90ex|||>>

  <tabbed|<tformat|<table|<row|<cell|<strong|Type:>
  <verbatim|axlT<with|font-series|bold|>orus>>>>>>

  <strong|Format:>

  <\cpp-code>
    \<less\>torus\<gtr\>

    \ \ \ \<less\>center\<gtr\>0 0 0\<less\>/center\<gtr\>

    \ \ \ \<less\>direction\<gtr\>0 0 1\<less\>/direction\<gtr\>

    \ \ \ \<less\>ringRadius\<gtr\>1\<less\>/ringRadius\<gtr\>

    \ \ \ \<less\>crossSectionRadius\<gtr\>0.2\<less\>/crossSectionRadius\<gtr\>

    \<less\>/torus\<gtr\>
  </cpp-code>

  <subsection|Composite objects>

  <subsubsection|Mesh<strong|>>

  A mesh is defined by its number of \ vertices, edges and faces, a sequence
  of point coordinates, a sequence of egdes and a sequence of faces.\ 

  <\itemize>
    <item>The sequence of point coordinates is given as a sequence of
    coordinnates <math|x<rsub|i>,y<rsub|i>,z<rsub|i>>.

    <item>An edge is described by its number of vertices and the sequence of
    indices of its vertices.

    <item>A face is described by its number of vertices and the loop of
    indices of its vertices.
  </itemize>

  An array of colors can also be optionanly given. The <math|i>th color of
  this array is given to the <math|i>th vertex.\ 

  <tabbed|<tformat|<table|<row|<cell|<strong|Type:> <verbatim|axlMesh>>>>>>

  <strong|Format:>

  <\cpp-code>
    \<less\>mesh\<gtr\>

    \ \ \<less\>count\<gtr\>4 1 4\<less\>/count\<gtr\>

    \ \ \<less\>points\<gtr\>

    \ \ 0 0 0

    \ \ 1 0 0

    \ \ 0 1 0

    \ \ 0 0 1

    \ \ \<less\>/points\<gtr\>

    \ \ \<less\>colors\<gtr\>

    \ \ 100 100 100

    \ \ 255 0 0

    \ \ 0 255 0

    \ \ 0 0 255

    \ \ \<less\>/colors\<gtr\> \ \ 

    \ \ \<less\>edges\<gtr\>

    \ \ 4 0 1 2 3

    \ \ \<less\>/edges\<gtr\>

    \ \ \<less\>faces\<gtr\>

    \ \ 3 0 1 2\ 

    \ \ 3 1 2 3

    \ \ 3 2 3 0

    \ \ 3 1 0 3\ 

    \ \ \<less\>/faces\<gtr\>

    \<less\>/mesh\<gtr\>
  </cpp-code>

  <center|<image|img/mesh.png|90ex|||>>

  This example describes a mesh with 4 vertices, 1 edge and 4 faces. The egde
  is connecting the vertices of indices 0, 1, 2, 3. The faces are 4 triangles
  which are connecting 3 vertices among the 4. This mesh corresponds to a
  tetrahedron with an edge containing 4 triangle sides \ ``marked'' on it.\ 

  <subsubsection|Shape>

  <tabbed|<tformat|<table|<row|<cell|<strong|Type:>
  <verbatim|axl<with|font-series|bold|Shape>>>>>>>

  <strong|Format:>

  <\cpp-code>
    \;
  </cpp-code>

  \;

  \;
</body>

<initial|<\collection>
</collection>>