.. highlight:: xml

Point
=====

A point is defined by 3 coordinates (double).

Type: ``axlPoint``

Format:: 

   <point>0 0 0</point>
