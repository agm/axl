/* axlActorCylinder.cpp ---
 *
 * Author: Meriadeg Perrinel
 * Copyright (C) 2008 - Meriadeg Perrinel, Inria.
 * Created: Fri Dec 17 10:59:21 2010 (+0100)
 * Version: $Id$
 * Last-Updated: Mon Dec 17 15:03:08 2012 (+0100)
 *           By: Julien Wintz
 *     Update #: 32
 */

/* Commentary:
 *
 */

/* Change log:
 *
 */
#include "axlActorCylinder.h"

#include "axlActorComposite.h"
#include "axlInteractorStyleSwitch.h"

#include <axlCore/axlCylinder.h>
#include <axlCore/axlPoint.h>

#include <QVTKOpenGLWidget.h>

#include <vtkActor.h>
#include <vtkAssemblyPath.h>
//#include <vtkCellPicker.h>
#include <vtkCommand.h>
#include <vtkGlyph3D.h>
#include <vtkInteractorStyleSwitch.h>
#include <vtkLight.h>
#include <vtkLightCollection.h>
#include <vtkCylinderSource.h>
#include <vtkLineWidget.h>
#include <vtkObjectFactory.h>
#include <vtkPoints.h>
#include <vtkPointWidget.h>
#include <vtkPolyData.h>
#include <vtkPolyDataMapper.h>
#include <vtkProperty.h>
#include <vtkRenderer.h>
#include <vtkRendererCollection.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkSmartPointer.h>
#include <vtkTransform.h>


class axlActorCylinderObserver : public vtkCommand
{
public:    
    static axlActorCylinderObserver *New(void)
    {
        return new axlActorCylinderObserver;
    }

    virtual void Execute(vtkObject *caller, unsigned long event, void *)
    {
        // vtkRenderWindowInteractor *interactor = observerDataAssembly->getInteractor();            //interactor->Render();


        if(event == vtkCommand::InteractionEvent)
        {
            if(!lineWidget || !radiusWidget)
                return;

////            {// NOTICE : We can improve this code...
//                axlPoint p1(lineWidget->GetPoint1()[0], lineWidget->GetPoint1()[1], lineWidget->GetPoint1()[2]);
//                axlPoint p2(lineWidget->GetPoint2()[0], lineWidget->GetPoint2()[1], lineWidget->GetPoint2()[2]);
//                if(!(axlPoint::distance(&p1, observerData_cylinder->firstPoint())< 0.0001 && axlPoint::distance(&p2, observerData_cylinder->secondPoint())<0.0001))
//                {// There we move the line widget
            if (caller == lineWidget){
                observerData_cylinderSource->Modified();
                observerData_cylinderSource->Update();
                observerData_cylinder->setFirstPoint(lineWidget->GetPoint1());
                observerData_cylinder->setSecondPoint(lineWidget->GetPoint2());
                observerData_cylinder->touchGeometry();
                observerDataAssembly->onUpdateGeometry();
            }
            else if (caller == radiusWidget)
            {// There we move the point widget

                axlPoint center = (*(observerData_cylinder->firstPoint())+(*(observerData_cylinder->secondPoint()))) / 2.0;
                axlPoint p3(radiusWidget->GetPosition()[0], radiusWidget->GetPosition()[1], radiusWidget->GetPosition()[2]);
                axlPoint mp3 = p3 - center;
                axlPoint p1p2 = (*(observerData_cylinder->secondPoint())-(*(observerData_cylinder->firstPoint())));
                double pv = (mp3.x() * p1p2.x() + mp3.y() * p1p2.y() + mp3.z() * p1p2.z());

                axlPoint ps = p1p2 * pv;
                double norm = observerData_cylinder->length();

                axlPoint c = p3 - (ps /= (norm*norm));

                radiusWidget->SetPosition(c.coordinates());
                double r = axlPoint::distance(&c, &center);

                observerData_cylinder->setRadius(r);
                observerData_cylinderSource->SetRadius(r);
                observerData_cylinderSource->Modified();
                observerData_cylinderSource->Update();
                observerData_cylinder->touchGeometry();
                if(!observerData_cylinder->fields().isEmpty())
                    observerData_cylinder->touchField();
            }
//            }
        }

//        if(event == vtkCommand::MouseMoveEvent)
//        {
//            if(observerData_cylinder)
//            {
//                // we need the matrix transform of this actor

//                vtkAssemblyPath *path;
//                vtkRenderWindow *renderWindow = interactor->GetRenderWindow();
//                vtkRendererCollection * rendererCollection = renderWindow->GetRenderers();
//                vtkRenderer *render = rendererCollection->GetFirstRenderer();
//                axlInteractorStyle = dynamic_cast<axlInteractorStyleSwitch *>(interactor->GetInteractorStyle());

//                int X = observerDataAssembly->getInteractor()->GetEventPosition()[0];
//                int Y = observerDataAssembly->getInteractor()->GetEventPosition()[1];

//                if (!render || !render->IsInViewport(X, Y))
//                {
//                    //qDebug()<<" No renderer or bad cursor coordonates";
//                }

//                axlCylinderPicker->Pick(X,Y,0.0,render);
//                path = axlCylinderPicker->GetPath();

//                if ( path != NULL)
//                {
//                    double *position = observerDataAssembly->GetPosition();
//                    (void)position;

//                    for(int j=0;j<3;j++)
//                    {
//                        //qDebug()<<"axlActorCylinderObserver :: Execute "<<position[j];
//                        //observerData_line->coordinates()[j] = position[j];
//                    }

//                }

//            }
//        }
    }

    axlInteractorStyleSwitch *axlInteractorStyle;
//    vtkCellPicker *axlCylinderPicker;
    axlCylinder *observerData_cylinder;
    axlActorCylinder *observerDataAssembly;
    vtkCylinderSource *observerData_cylinderSource;

    vtkLineWidget *lineWidget = nullptr;
    vtkPointWidget *radiusWidget = nullptr;


};

// /////////////////////////////////////////////////////////////////
// axlActorCylinderPrivate
// /////////////////////////////////////////////////////////////////

class axlActorCylinderPrivate
{
public:
    axlCylinder *cylinder;
    vtkCylinderSource *cylinderSource;
    vtkPointWidget *radiusWidget;
    vtkLineWidget *lineWidget;
    axlActorCylinderObserver *cylinderObserver;
    QVTKOpenGLWidget *widget;
//    vtkCellPicker *axlCylinderPicker ;

};

#if (VTK_MAJOR_VERSION <= 5)
vtkCxxRevisionMacro(axlActorCylinder, "$Revision: 0.0.1 $");
#endif

vtkStandardNewMacro(axlActorCylinder);

dtkAbstractData *axlActorCylinder::data(void)
{
    return d->cylinder;
}

vtkCylinderSource *axlActorCylinder::cylinder(void)
{
    return d->cylinderSource;
}

void axlActorCylinder::setQVTKWidget(QVTKOpenGLWidget *widget)
{
    d->widget = widget;
}


void axlActorCylinder::setData(dtkAbstractData *cylinder1)
{
    axlCylinder *cylinder = dynamic_cast<axlCylinder *>(cylinder1);
    if(cylinder)
    {
        d->cylinder = cylinder;
        this->setMapper(vtkSmartPointer<vtkPolyDataMapper>::New());
        this->setActor(vtkSmartPointer<vtkActor>::New());

        d->cylinderSource = vtkCylinderSource::New();
        //        d->cylinderSource->SetPoint1(d->cylinder->firstPoint()->coordinates());
        //        d->cylinderSource->SetPoint2(d->cylinder->secondPoint()->coordinates());

        d->cylinderSource->SetResolution(200);
        //this->SetPosition(d->cylinder->coordinates());

        // connection of data to actor
        //this->getMapper()->SetInput(d->cylinderSource->GetOutput());

        this->onUpdateGeometry();
#if (VTK_MAJOR_VERSION <= 5)
        this->getMapper()->SetInput(d->cylinderSource->GetOutput());
#else
        this->getMapper()->SetInputData(d->cylinderSource->GetOutput());
#endif

        this->getActor()->SetMapper(this->getMapper());

        this->AddPart(this->getActor());

        if(!d->cylinderObserver)
        {
//            d->axlCylinderPicker = vtkCellPicker::New();
            d->cylinderObserver = axlActorCylinderObserver::New() ;
            this->getInteractor()->AddObserver(vtkCommand::InteractionEvent, d->cylinderObserver);
            this->getInteractor()->AddObserver(vtkCommand::MouseMoveEvent, d->cylinderObserver);


            d->cylinderObserver->observerDataAssembly = this;
//            d->cylinderObserver->axlCylinderPicker = d->axlCylinderPicker;
            d->cylinderObserver->observerData_cylinderSource = d->cylinderSource;
            d->cylinderObserver->observerData_cylinder = d->cylinder;
            d->cylinderObserver->axlInteractorStyle = dynamic_cast<axlInteractorStyleSwitch *> (this->getInteractor()->GetInteractorStyle());
        }

        QColor color = d->cylinder->color();
        this->getActor()->GetProperty()->SetColor(color.redF(), color.greenF(), color.blueF());

        QString shader = d->cylinder->shader();
        if(!shader.isEmpty())
            this->setShader(shader);

        //this->getActor()->SetScale(d->cylinder->size());

        this->setOpacity(d->cylinder->opacity());

        connect(d->cylinder,SIGNAL(modifiedGeometry()),this,SLOT(onUpdateGeometry()));
        connect(d->cylinder,SIGNAL(modifiedProperty()),this,SLOT(onUpdateProperty()));
    }
    else
        qDebug()<< "no axlCylinder available";

}


void axlActorCylinder::setDisplay(bool display)
{
    axlActor::setDisplay(display);

    if(display && d->lineWidget){
        this->showCylinderWidget(true);
    }

    if(!display && d->lineWidget){
        this->showCylinderWidget(false);
    }
}

void axlActorCylinder::showCylinderWidget(bool show)
{
    if(!d->lineWidget || !d->radiusWidget) {
        qDebug() << "No tet actor computed for this axlActorCylinder.";
        return;
    }

    if(show){
        d->lineWidget->SetEnabled(1);
        d->radiusWidget->SetEnabled(1);
    }

    if(!show){
        d->lineWidget->SetEnabled(0);
        d->radiusWidget->SetEnabled(0);

    }
}

void axlActorCylinder::setCylinderWidget(bool cylinderWidget)
{

    if(cylinderWidget) {
        if(!d->lineWidget || !d->radiusWidget)
        {
            //widget drawing

            d->lineWidget = vtkLineWidget::New();
            d->lineWidget->SetInteractor(this->getInteractor());
            d->lineWidget->SetProp3D(this->getActor());
            d->lineWidget->PlaceWidget();
            d->lineWidget->SetPoint1(d->cylinder->firstPoint()->coordinates());
            d->lineWidget->SetPoint2(d->cylinder->secondPoint()->coordinates());

            d->radiusWidget = vtkPointWidget::New();
            d->radiusWidget->SetInteractor(this->getInteractor());
            d->radiusWidget->SetProp3D(this->getActor());
            d->radiusWidget->PlaceWidget();
            d->radiusWidget->AllOff();
            d->radiusWidget->SetTranslationMode(1);
//            axlPoint v = *(d->cylinder->firstPoint())+(*(d->cylinder->secondPoint()));
//            //normal of v with 0 in x
//            axlPoint n(-v.y(), v.x(), v.z());
//            if(v.x() < 0.0005 && v.y() < 0.0005)
//                n.setCoordinates(1.0, 0.0, 0.0); // need if cylinder is in z direction
//            n.normalize();
//            n *= d->cylinder->radius();
//            v /= 2.0;
//
//            v += n;
//
//            d->radiusWidget->SetPosition(v.x(), v.y(), v.z());
            axlPoint center = (*(d->cylinder->firstPoint())+(*(d->cylinder->secondPoint()))) / 2.0;
            axlPoint p1p2 = (*(d->cylinder->secondPoint())-(*(d->cylinder->firstPoint())));
            axlPoint mp3(1.0, 1.0, 1.0);
            axlPoint pv = axlPoint::crossProduct(mp3, p1p2);
            pv.normalize();
            pv *= d->cylinder->radius();
            pv += center;
            d->radiusWidget->SetPosition(pv.coordinates());
        }

        if(d->cylinderObserver)
        {
            d->lineWidget->AddObserver(vtkCommand::InteractionEvent, d->cylinderObserver);
            d->radiusWidget->AddObserver(vtkCommand::InteractionEvent, d->cylinderObserver);


            d->cylinderObserver->lineWidget = d->lineWidget;
            d->cylinderObserver->radiusWidget = d->radiusWidget;
        }

        // there is always the controlPoints there
        d->lineWidget->SetEnabled(true);
        d->radiusWidget->SetEnabled(true);



    }

    if (!cylinderWidget)
    {
        if (this->getActor()) {
            // this->getMapper()->SetInput(this->getPolyData());

            if(d->lineWidget)
            {
                d->lineWidget->RemoveAllObservers();
                d->lineWidget->SetEnabled(false);
                d->lineWidget->Delete(); // warning not sure
                d->lineWidget = NULL;
            }

            if(d->radiusWidget)
            {
                d->radiusWidget->RemoveAllObservers();
                d->radiusWidget->SetEnabled(false);
                d->radiusWidget->Delete(); // warning not sure
                d->radiusWidget = NULL;
            }
        }
    }
    if (d->cylinderObserver){
        d->cylinderObserver->lineWidget = d->lineWidget;
        d->cylinderObserver->radiusWidget = d->radiusWidget;
    }
}

bool axlActorCylinder::isShowCylinderWidget(void)
{

    if(!d->lineWidget || !d->radiusWidget) {
        qDebug() << "No tet actor computed for this axlActorBSpline.";
        return false;
    }

    return d->lineWidget->GetEnabled() && d->radiusWidget->GetEnabled();
}


void axlActorCylinder::setMode(int state)
{
    this->onModeChanged(state);

    emit stateChanged(this->data(), state);
}

void axlActorCylinder::onModeChanged(int state)
{
    if(state == 0)
    {
        this->setState(axlActor::passive);
        this->getActor()->SetPickable(1);

        if(axlAbstractData *data = dynamic_cast<axlAbstractData *>(this->data()))
        {
            QColor color = data->color();
            this->getActor()->GetProperty()->SetColor(color.redF(), color.greenF(), color.blueF());
        }
        this->setCylinderWidget(false);

    }
    else if(state == 1)
    {

        this->setState(axlActor::selection);
        this->setCylinderWidget(false);
        this->getActor()->SetPickable(1);
        vtkProperty *prop = this->getActor()->GetProperty();

        if(axlAbstractData *data = dynamic_cast<axlAbstractData *>(this->data()))
        {
            QColor color = data->color();
            qreal *h = new qreal(0.0);
            qreal *s = new qreal(0.0);
            qreal *l = new qreal(0.0);
            color.getHslF(h, s, l);
            color.setHslF(*h, *s, *l + (1.0 - *l) / 2.0);
            prop->SetColor(color.redF(), color.greenF(), color.blueF());
            delete l;
            delete s;
            delete h;
        }

    }
    else if(state == 2)
    {
        this->setState(axlActor::edition);
        this->setCylinderWidget(true);
        this->getActor()->SetPickable(0);
        vtkProperty *prop = this->getActor()->GetProperty();
        if(axlAbstractData *data = dynamic_cast<axlAbstractData *>(this->data()))
        {
            QColor color = data->color();
            qreal *h = new qreal(0.0);
            qreal *s = new qreal(0.0);
            qreal *l = new qreal(0.0);
            color.getHslF(h, s, l);
            color.setHslF(*h, *s, *l + (1.0 - *l)/2.0);
            prop->SetColor(color.redF(), color.greenF(), color.blueF());
            delete l;
            delete s;
            delete h;
        }
    }

    this->Modified();
}


void axlActorCylinder::onRemoved()
{
    // if on edit mode, change to selection mode (for stability)
    if (this->getState() == 2)
        setMode(1);
    
    //remove line specificity
    if(d->cylinderObserver)
    {
        this->getInteractor()->RemoveObservers(vtkCommand::InteractionEvent, d->cylinderObserver);
        this->getInteractor()->RemoveObservers(vtkCommand::MouseMoveEvent, d->cylinderObserver);
        d->cylinderObserver->observerDataAssembly = NULL;
//        d->cylinderObserver->axlCylinderPicker = NULL;
        d->cylinderObserver->observerData_cylinderSource = NULL;
        d->cylinderObserver->observerData_cylinder = NULL;
        d->cylinderObserver->axlInteractorStyle = NULL;
        d->cylinderObserver->Delete();
        d->cylinderObserver = NULL;

//        d->axlCylinderPicker->Delete();
//        d->axlCylinderPicker = NULL;

    }
    if(d->cylinderSource)
    {
        d->cylinderSource->Delete();
        d->cylinderSource = NULL;
    }
    if(d->widget)
    {
        d->widget = NULL;
    }
    if(d->cylinder)
    {
        d->cylinder = NULL;
    }
    if(d->lineWidget && d->radiusWidget)
    {
        this->setCylinderWidget(false);
        d->lineWidget = NULL;
        d->radiusWidget = NULL;
    }

    //remove actor specificity
    this->RemoveAllObservers();
    this->RemovePart(this->getActor());
    this->getActor()->RemoveAllObservers();

    if(axlActorComposite *actorComposite = dynamic_cast<axlActorComposite *>(this->parent()) )
        actorComposite->removeActorReference(this);
}

void axlActorCylinder::onUpdateGeometry()
{
    d->cylinderSource->SetHeight(axlPoint::distance(d->cylinder->firstPoint(), d->cylinder->secondPoint()));
    d->cylinderSource->SetRadius(d->cylinder->radius());
    d->cylinderSource->Modified();
    d->cylinderSource->Update();
    if(d->lineWidget)
    {
        d->lineWidget->SetPoint1(d->cylinder->firstPoint()->coordinates());
        d->lineWidget->SetPoint2(d->cylinder->secondPoint()->coordinates());
    }

    axlPoint center = (*(d->cylinder->firstPoint())+(*(d->cylinder->secondPoint()))) / 2.0;

    // Find the good axes
    axlPoint v = *(d->cylinder->firstPoint()) - center;
    v.normalize();

    // vectorial product
    axlPoint n(-v.z(), 0.0, v.x());
    if(!(fabs(n.x()) < 0.00001 && fabs(n.z()) < 0.0001))// vectorial product is not null
    {
        n.normalize();
        //rotation
        this->getActor()->SetOrientation(0.0, 0.0, 0.0);
        this->getActor()->RotateWXYZ(-acos(v.y()) * 180 / 3.14159, n.x(), 0.0, n.z());
    }

    if(d->radiusWidget) {
        axlPoint p1p2 = (*(d->cylinder->secondPoint())-(*(d->cylinder->firstPoint())));
        axlPoint mp3(1.0, 1.0, 1.0);

        //axlPoint pv(mp3.y() * p1p2.z() - mp3.z() * p1p2.y(), mp3.x() * p1p2.z() - mp3.z() * p1p2.x(), mp3.x() * p1p2.y() - mp3.y() * p1p2.x());
        axlPoint pv = axlPoint::crossProduct(mp3, p1p2);
        pv.normalize();
        pv *= d->cylinder->radius();
        pv += center;

        d->radiusWidget->SetPosition(pv.coordinates());
    }

    //Translation
    this->getActor()->SetPosition(center.coordinates());

    if(!d->cylinder->fields().isEmpty())
        d->cylinder->touchField();

    if(d->cylinder->updateView())
        emit updated();
}

axlActorCylinder::axlActorCylinder(void) : axlActor(), d(new axlActorCylinderPrivate)
{
    d->cylinder = NULL;
    d->lineWidget = NULL;
    d->radiusWidget = NULL;
//    d->axlCylinderPicker = NULL;
    d->cylinderObserver = NULL;
    d->cylinderSource =NULL;
    d->widget = NULL;
}

axlActorCylinder::~axlActorCylinder(void)
{
    delete d;

    d = NULL;
}

axlAbstractActor *createAxlActorCylinder(void){

    return axlActorCylinder::New();
}
