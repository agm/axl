/* axlVolumeDiscreteEditor.h ---
 *
 * Author: Julien Wintz
 * Created: Thu Sep 26 14:07:35 2013 (+0200)
 * Version:
 * Last-Updated: Wed Oct  9 19:50:36 2013 (+0200)
 *           By: Julien Wintz
 *     Update #: 24
 */

/* Change Log:
 *
 */


#ifndef AXLVOLUMEDISCRETEEDITOR_H
#define AXLVOLUMEDISCRETEEDITOR_H


//#pragma once

#include "axlVtkViewPluginExport.h"

#include <axlGui/axlAbstractVolumeDiscreteEditor.h>
#include <axlGui/axlInspectorObjectFactory.h>

#include <QtCore>
#include <QtGui>

class axlAbstractView;
class axlAbstractVolumeDiscrete;
class axlVolumeDiscreteEditorPrivate;

class AXLVTKVIEWPLUGIN_EXPORT axlVolumeDiscreteEditor : public axlAbstractVolumeDiscreteEditor
{
    Q_OBJECT

public:
     axlVolumeDiscreteEditor(QWidget *parent = 0);
    ~axlVolumeDiscreteEditor(void);

     static bool registered(void);

public slots:
    void setView(axlAbstractView *view);
    void setVolume(axlAbstractVolumeDiscrete *volume);

protected slots:
    void onLogChecked(bool checked);
    void onSclChecked(bool checked);
    void onShdChecked(bool checked);

protected slots:
    void outlineNone(void);
    void outlineCorners(void);
    void outlineBox(void);
    void outlineContour(void);


private:
    axlVolumeDiscreteEditorPrivate *d;

};

AXLVTKVIEWPLUGIN_EXPORT axlInspectorObjectInterface *createaxlVolumeDiscreteEditor(void);
#endif // AXLVOLUMEDISCRETEEDITOR_H
