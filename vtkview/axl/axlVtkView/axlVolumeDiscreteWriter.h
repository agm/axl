/* axlVolumeDiscreteWriter.h ---
 *
 * Author: Anais Ducoffe
 * Copyright (C) 2013 - Anais Ducoffe, Inria.
 */

/* Commentary:
 *
 */

/* Change log:
 *
 */

#ifndef AXLVOLUMEDISCRETEWRITER_H
#define AXLVOLUMEDISCRETEWRITER_H

#include <axlCore/axlAbstractDataWriter.h>

#include <vtkAssembly.h>
#include <vtkSmartPointer.h>
#include <vtkVersion.h>
#include <vtkImageData.h>
#include "axlVtkViewPluginExport.h"

#include <axlCore/axlAbstractVolumeDiscrete.h>

// ///////////////////////////////////////////////////////////////////
// axlVolumeDiscreteWriter declaration
// ///////////////////////////////////////////////////////////////////

class AXLVTKVIEWPLUGIN_EXPORT axlVolumeDiscreteWriter : public axlAbstractDataWriter
{
    Q_OBJECT

public:
    axlVolumeDiscreteWriter(void);

public:
    ~axlVolumeDiscreteWriter(void);


public:
    QString identifier(void) const;
    QString description(void) const;
    QStringList handled(void) const;

    static bool registered(void);

public:
    bool accept(dtkAbstractData *data);
    bool reject(dtkAbstractData *data);

    QDomElement write(QDomDocument *doc, dtkAbstractData *data);

private :
    QDomElement elementByWriter(axlAbstractDataWriter *axl_writer, QDomDocument *doc, dtkAbstractData *data);
};

// /////////////////////////////////////////////////////////////////
// Instanciation function
// /////////////////////////////////////////////////////////////////

AXLVTKVIEWPLUGIN_EXPORT dtkAbstractDataWriter *createaxlVolumeDiscreteWriter(void);

#endif //AXLVOLUMEDISCRETEWRITER_H
