/* axlVolumeDiscrete.h ---
 *
 * Author: Anais Ducoffe
 * Copyright (C) 2013 - Anais Ducoffe, Inria.
 */

/* Commentary:
 *
 */

/* Change log:
 *
 */

#ifndef AXLVOLUMEDISCRETE_H
#define AXLVOLUMEDISCRETE_H

#include <axlCore/axlAbstractActor.h>

#include <vtkAssembly.h>
#include <vtkSmartPointer.h>
#include <vtkVersion.h>
#include <vtkImageData.h>
#include "axlVtkViewPluginExport.h"



#include <axlCore/axlAbstractVolumeDiscrete.h>
class axlVolumeDiscretePrivate;

// ///////////////////////////////////////////////////////////////////
// axlVolumeDiscrete declaration
// ///////////////////////////////////////////////////////////////////

class AXLVTKVIEWPLUGIN_EXPORT axlVolumeDiscrete : public axlAbstractVolumeDiscrete
{
    Q_OBJECT

public:
    axlVolumeDiscrete(void);
    axlVolumeDiscrete(const axlVolumeDiscrete& other);

public:
    ~axlVolumeDiscrete(void);

public:
    axlVolumeDiscrete *clone(void) const;

public:
    axlVolumeDiscrete& operator = (const axlVolumeDiscrete& other);

public:
    QString identifier(void) const;

public:
    void setData(void *data);

public:
    void *data(void) const;

public:
    int xDimension(void) const;
    int yDimension(void) const;
    int zDimension(void) const;
    void setDimensions(unsigned int x, unsigned int y,unsigned int z);

public:
    double minValue(void) const;
    double midValue(void) const;
    double maxValue(void) const;

public:
    void setMinValue(double min_field_value);
    void setMidValue(double mid_field_value);
    void setMaxValue(double max_field_value);


    //double getValue(int indiceGlobal) const;
    double getValue(int i, int j, int k) const;
    //void setValue(double value, int indiceGlobal);
    void setValue(double value, int i, int j, int k);

private:
    axlVolumeDiscretePrivate *d;
};

// /////////////////////////////////////////////////////////////////
// Instanciation function
// /////////////////////////////////////////////////////////////////

AXLVTKVIEWPLUGIN_EXPORT dtkAbstractData *createaxlVolumeDiscrete(void);

#endif
// AXLVOLUMEDISCRETE_H
