/* axlActorCone.cpp ---
 *
 * Author: Meriadeg Perrinel
 * Copyright (C) 2008 - Meriadeg Perrinel, Inria.
 * Created: Fri Dec 17 10:59:21 2010 (+0100)
 * Version: $Id$
 * Last-Updated: Mon Dec 17 15:01:03 2012 (+0100)
 *           By: Julien Wintz
 *     Update #: 40
 */

/* Commentary:
 *
 */

/* Change log:
 *
 */

#include "axlActorCone.h"
#include "axlActorComposite.h"
#include "axlInteractorStyleSwitch.h"

#include <axlCore/axlCone.h>
#include <axlCore/axlPoint.h>

#include <QVTKOpenGLWidget.h>

#include <vtkActor.h>
#include <vtkAssemblyPath.h>
//#include <vtkCellPicker.h>
#include <vtkCommand.h>
#include <vtkGlyph3D.h>
#include <vtkInteractorStyleSwitch.h>
#include <vtkLight.h>
#include <vtkLightCollection.h>
#include <vtkConeSource.h>
#include <vtkLineWidget.h>
#include <vtkObjectFactory.h>
#include <vtkPoints.h>
#include <vtkPointWidget.h>
#include <vtkPolyData.h>
#include <vtkPolyDataMapper.h>
#include <vtkDataSetMapper.h>
#include <vtkProperty.h>
#include <vtkRenderer.h>
#include <vtkRendererCollection.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkSmartPointer.h>
#include <vtkTransform.h>

class axlActorConeObserver : public vtkCommand
{
public:
    static axlActorConeObserver *New(void)
    {
        return new axlActorConeObserver;
    }

    virtual void Execute(vtkObject *caller, unsigned long event, void *)
    {
        //vtkRenderWindowInteractor *interactor = observerDataAssembly->getInteractor();            //interactor->Render();

        if(event == vtkCommand::InteractionEvent)
        {
            if(!lineWidget || !radiusWidget)
                return;

            // NOTICE : We can improve this code...
            if (caller == lineWidget)
            {
//                axlPoint p1(lineWidget->GetPoint1()[0], lineWidget->GetPoint1()[1], lineWidget->GetPoint1()[2]);
//                axlPoint p2(lineWidget->GetPoint2()[0], lineWidget->GetPoint2()[1], lineWidget->GetPoint2()[2]);
//                if(!(axlPoint::distance(&p1, observerData_cone->apex())< 0.0001 && axlPoint::distance(&p2, observerData_cone->basePoint())<0.0001))
//                {// There we move the line widget
                observerData_coneSource->Update();
                observerData_coneSource->Modified();
                observerData_cone->setApex(lineWidget->GetPoint1());
                observerData_cone->setBasePoint(lineWidget->GetPoint2());
                observerData_cone->touchGeometry();
                observerDataAssembly->onUpdateGeometry();
//                }
            }
            else if (caller == radiusWidget)
            {// There we move the point widget

                axlPoint center = (*(observerData_cone->apex())+(*(observerData_cone->basePoint()))) / 2.0;
                axlPoint p3(radiusWidget->GetPosition()[0], radiusWidget->GetPosition()[1], radiusWidget->GetPosition()[2]);
                axlPoint mp3 = p3 - center;
                axlPoint p1p2 = (*(observerData_cone->basePoint())-(*(observerData_cone->apex())));
                double pv = (mp3.x() * p1p2.x() + mp3.y() * p1p2.y() + mp3.z() * p1p2.z());

                axlPoint ps = p1p2 * pv;
                double norm = observerData_cone->length();

                axlPoint c = p3 - (ps /= (norm*norm));

                radiusWidget->SetPosition(c.coordinates());
                double r = axlPoint::distance(&c, &center);

                observerData_cone->setRadius(r);
                observerData_coneSource->SetRadius(r);
                observerData_coneSource->Update();
                observerData_coneSource->Modified();
                observerData_cone->touchGeometry();
                if(!observerData_cone->fields().isEmpty())
                    observerData_cone->touchField();
            }
        }
    }

    axlInteractorStyleSwitch *axlInteractorStyle;
//    vtkCellPicker *axlConePicker;
    axlCone *observerData_cone;
    axlActorCone *observerDataAssembly;
    vtkConeSource *observerData_coneSource;

    vtkLineWidget *lineWidget=nullptr;
    vtkPointWidget *radiusWidget=nullptr;

};

// /////////////////////////////////////////////////////////////////
// axlActorConePrivate
// /////////////////////////////////////////////////////////////////

class axlActorConePrivate
{
public:
    axlCone *cone;
    vtkConeSource *coneSource;
    vtkPointWidget *radiusWidget;
    vtkLineWidget *lineWidget;
    axlActorConeObserver *coneObserver;
    QVTKOpenGLWidget *widget;
//    vtkCellPicker *axlConePicker ;

};

#if (VTK_MAJOR_VERSION <= 5)
vtkCxxRevisionMacro(axlActorCone, "$Revision: 0.0.1 $");
#endif

vtkStandardNewMacro(axlActorCone);

dtkAbstractData *axlActorCone::data(void)
{
    return d->cone;
}

vtkConeSource *axlActorCone::cone(void)
{
    return d->coneSource;
}

vtkSmartPointer<vtkPolyData> axlActorCone::getPolyData(){
    return d->coneSource->GetOutput();
}

void axlActorCone::setQVTKWidget(QVTKOpenGLWidget *widget)
{
    d->widget = widget;
}


void axlActorCone::setData(dtkAbstractData *cone1)
{
    axlCone *cone = dynamic_cast<axlCone *>(cone1);

    if(cone)
    {
        d->cone = cone;
        this->setMapper(vtkSmartPointer<vtkPolyDataMapper>::New());
        //this->setDataSetMapper(vtkSmartPointer<vtkDataSetMapper>::New());
        this->setActor(vtkSmartPointer<vtkActor>::New());

        d->coneSource = vtkConeSource::New();
        //        d->coneSource->SetPoint1(d->cone->apex()->coordinates());
        //        d->coneSource->SetPoint2(d->cone->basePoint()->coordinates());

        d->coneSource->SetResolution(200);
        d->coneSource->SetDirection(0.0, 1.0, 0.0);

        //this->SetPosition(d->cone->coordinates());

        // connection of data to actor
        //this->getMapper()->SetInput(d->coneSource->GetOutput());
        //this->setPolyData(d->coneSource->GetOutput());

        this->onUpdateGeometry();
#if (VTK_MAJOR_VERSION <= 5)
        this->getMapper()->SetInput(d->coneSource->GetOutput());
        //this->getDataSetMapper()->SetInput(d->coneSource->GetOutput());
#else
        this->getMapper()->SetInputData(d->coneSource->GetOutput());
        //this->getDataSetMapper()->SetInputData(d->coneSource->GetOutput());
#endif
        this->getActor()->SetMapper(this->getMapper());
        //this->getActor()->SetMapper(this->getDataSetMapper());

        this->AddPart(this->getActor());

        if(!d->coneObserver)
        {
//            d->axlConePicker = vtkCellPicker::New();
            d->coneObserver = axlActorConeObserver::New() ;
            this->getInteractor()->AddObserver(vtkCommand::InteractionEvent, d->coneObserver);
            this->getInteractor()->AddObserver(vtkCommand::MouseMoveEvent, d->coneObserver);


            d->coneObserver->observerDataAssembly = this;
//            d->coneObserver->axlConePicker = d->axlConePicker;
            d->coneObserver->observerData_coneSource = d->coneSource;
            d->coneObserver->observerData_cone = d->cone;
            d->coneObserver->axlInteractorStyle = dynamic_cast<axlInteractorStyleSwitch *> (this->getInteractor()->GetInteractorStyle());
        }

        QColor color = d->cone->color();
        this->getActor()->GetProperty()->SetColor(color.redF(), color.greenF(), color.blueF());

        QString shader = d->cone->shader();
        if(!shader.isEmpty())
            this->setShader(shader);

        //this->getActor()->SetScale(d->cone->size());

        this->setOpacity(d->cone->opacity());

        connect(d->cone,SIGNAL(modifiedGeometry()),this,SLOT(onUpdateGeometry()));
        connect(d->cone,SIGNAL(modifiedProperty()),this,SLOT(onUpdateProperty()));
    }
    else
        qDebug()<< "no axlCone available";

}


void axlActorCone::setDisplay(bool display)
{
    axlActor::setDisplay(display);

    if(display && d->lineWidget){
        this->showConeWidget(true);
    }

    if(!display && d->lineWidget){
        this->showConeWidget(false);
    }
}

void axlActorCone::showConeWidget(bool show)
{
    if(!d->lineWidget || !d->radiusWidget) {
        qDebug() << "No tet actor computed for this axlActorCone.";
        return;
    }

    if(show){
        d->lineWidget->SetEnabled(1);
        d->radiusWidget->SetEnabled(1);
    }

    if(!show){
        d->lineWidget->SetEnabled(0);
        d->radiusWidget->SetEnabled(0);

    }
}

void axlActorCone::setConeWidget(bool coneWidget)
{

    if(coneWidget) {
        if(!d->lineWidget || !d->radiusWidget)
        {
            //widget drawing

            d->lineWidget = vtkLineWidget::New();
            d->lineWidget->SetInteractor(this->getInteractor());
            d->lineWidget->SetProp3D(this->getActor());
            d->lineWidget->PlaceWidget();
            d->lineWidget->SetPoint1(d->cone->apex()->coordinates());
            d->lineWidget->SetPoint2(d->cone->basePoint()->coordinates());

            d->radiusWidget = vtkPointWidget::New();
            d->radiusWidget->SetInteractor(this->getInteractor());
            d->radiusWidget->SetProp3D(this->getActor());
            d->radiusWidget->PlaceWidget();
            d->radiusWidget->AllOff();
            d->radiusWidget->SetTranslationMode(1);
//            axlPoint v = *(d->cone->apex())+(*(d->cone->basePoint()));
//            //normal of v with 0 in x
//            axlPoint n(-v.y(), v.x(), v.z());
//            if(v.x() < 0.0005 && v.y() < 0.0005)
//                n.setCoordinates(1.0, 0.0, 0.0); // need if cone is in z direction
//            n.normalize();
//            n *= d->cone->radius();
//            v /= 2.0;
//
//            v += n;
//
//            d->radiusWidget->SetPosition(v.x(), v.y(), v.z());
            axlPoint center = (*(d->cone->apex())+(*(d->cone->basePoint()))) / 2.0;
            axlPoint p1p2 = (*(d->cone->basePoint())-(*(d->cone->apex())));
            axlPoint mp3(1.0, 1.0, 1.0);
            //axlPoint pv(mp3.y() * p1p2.z() - mp3.z() * p1p2.y(), mp3.x() * p1p2.z() - mp3.z() * p1p2.x(), mp3.x() * p1p2.y() - mp3.y() * p1p2.x());
            axlPoint pv = axlPoint::crossProduct(mp3, p1p2);
            pv.normalize();
            pv *= (1 * d->cone->radius());
            pv += center;
            d->radiusWidget->SetPosition(pv.coordinates());
        }

        if(d->coneObserver)
        {
            d->lineWidget->AddObserver(vtkCommand::InteractionEvent, d->coneObserver);
            d->radiusWidget->AddObserver(vtkCommand::InteractionEvent, d->coneObserver);


            d->coneObserver->lineWidget = d->lineWidget;
            d->coneObserver->radiusWidget = d->radiusWidget;
        }

        // there is always the controlPoints there
        d->lineWidget->SetEnabled(true);
        d->radiusWidget->SetEnabled(true);



    }

    if (!coneWidget)
    {
        if (this->getActor()) {
            // this->getMapper()->SetInput(this->getPolyData());

            if(d->lineWidget)
            {
                d->lineWidget->RemoveAllObservers();
                d->lineWidget->SetEnabled(false);
                d->lineWidget->Delete(); // warning not sure
                d->lineWidget = NULL;
            }

            if(d->radiusWidget)
            {
                d->radiusWidget->RemoveAllObservers();
                d->radiusWidget->SetEnabled(false);
                d->radiusWidget->Delete(); // warning not sure
                d->radiusWidget = NULL;
            }
        }
    }
    if (d->coneObserver){
        d->coneObserver->lineWidget = d->lineWidget;
        d->coneObserver->radiusWidget = d->radiusWidget;
    }
}

bool axlActorCone::isShowConeWidget(void)
{

    if(!d->lineWidget || !d->radiusWidget) {
        qDebug() << "No tet actor computed for this axlActorBSpline.";
        return false;
    }

    return d->lineWidget->GetEnabled() && d->radiusWidget->GetEnabled();
}


void axlActorCone::setMode(int state)
{
    this->onModeChanged(state);

    emit stateChanged(this->data(), state);
}

void axlActorCone::onModeChanged(int state)
{
    if(state == 0)
    {
        this->setState(axlActor::passive);
        this->getActor()->SetPickable(1);

        if(axlAbstractData *data = dynamic_cast<axlAbstractData *>(this->data()))
        {
            QColor color = data->color();
            this->getActor()->GetProperty()->SetColor(color.redF(), color.greenF(), color.blueF());
        }
        this->setConeWidget(false);

    }
    else if(state == 1)
    {

        this->setState(axlActor::selection);
        this->setConeWidget(false);
        this->getActor()->SetPickable(1);
        vtkProperty *prop = this->getActor()->GetProperty();

        if(axlAbstractData *data = dynamic_cast<axlAbstractData *>(this->data()))
        {
            QColor color = data->color();
            qreal *h = new qreal(0.0);
            qreal *s = new qreal(0.0);
            qreal *l = new qreal(0.0);
            color.getHslF(h, s, l);
            color.setHslF(*h, *s, *l + (1.0 - *l) / 2.0);
            prop->SetColor(color.redF(), color.greenF(), color.blueF());
            delete l;
            delete s;
            delete h;
        }

    }
    else if(state == 2)
    {
        this->setState(axlActor::edition);
        this->setConeWidget(true);
        this->getActor()->SetPickable(0);
        vtkProperty *prop = this->getActor()->GetProperty();
        if(axlAbstractData *data = dynamic_cast<axlAbstractData *>(this->data()))
        {
            QColor color = data->color();
            qreal *h = new qreal(0.0);
            qreal *s = new qreal(0.0);
            qreal *l = new qreal(0.0);
            color.getHslF(h, s, l);
            color.setHslF(*h, *s, *l + (1.0 - *l)/2.0);
            prop->SetColor(color.redF(), color.greenF(), color.blueF());
            delete l;
            delete s;
            delete h;
        }
    }

    this->Modified();
}


void axlActorCone::onRemoved()
{
    // if on edit mode, change to selection mode (for stability)
    if (this->getState() == 2)
        setMode(1);
    
    //remove line specificity
    if(d->coneObserver)
    {
        this->getInteractor()->RemoveObservers(vtkCommand::InteractionEvent, d->coneObserver);
        this->getInteractor()->RemoveObservers(vtkCommand::MouseMoveEvent, d->coneObserver);
        d->coneObserver->observerDataAssembly = NULL;
//        d->coneObserver->axlConePicker = NULL;
        d->coneObserver->observerData_coneSource = NULL;
        d->coneObserver->observerData_cone = NULL;
        d->coneObserver->axlInteractorStyle = NULL;
        d->coneObserver->Delete();
        d->coneObserver = NULL;

//        d->axlConePicker->Delete();
//        d->axlConePicker = NULL;

    }
    if(d->coneSource)
    {
        d->coneSource->Delete();
        d->coneSource = NULL;
    }
    if(d->widget)
    {
        d->widget = NULL;
    }
    if(d->cone)
    {
        d->cone = NULL;
    }
    if(d->lineWidget && d->radiusWidget)
    {
        this->setConeWidget(false);
        d->lineWidget = NULL;
        d->radiusWidget = NULL;
    }

    //remove actor specificity
    this->RemoveAllObservers();
    this->RemovePart(this->getActor());
    this->getActor()->RemoveAllObservers();

    if(axlActorComposite *actorComposite = dynamic_cast<axlActorComposite *>(this->parent()) )
        actorComposite->removeActorReference(this);
}

void axlActorCone::onUpdateGeometry()
{

    d->coneSource->SetHeight(axlPoint::distance(d->cone->apex(), d->cone->basePoint()));
    d->coneSource->SetRadius(d->cone->radius());
    //d->coneSource->SetResolution(100);
    d->coneSource->Modified();
    d->coneSource->Update();

    if(d->lineWidget)
    {
        d->lineWidget->SetPoint1(d->cone->apex()->coordinates());
        d->lineWidget->SetPoint2(d->cone->basePoint()->coordinates());
    }

    axlPoint center = (*(d->cone->apex())+(*(d->cone->basePoint()))) / 2.0;

    // Find the good axes
    axlPoint v = *(d->cone->apex()) - center;
    v.normalize();

    // vectorial product
    axlPoint n(-v.z(), 0.0, v.x());
    if(!(fabs(n.x()) < 0.00001 && fabs(n.z()) < 0.0001))// vectorial product is not null
    {
        n.normalize();
        //rotation
        this->getActor()->SetOrientation(0.0, 0.0, 0.0);
        this->getActor()->RotateWXYZ(-acos(v.y()) * 180 / 3.14159, n.x(), 0.0, n.z());
    }

    if(d->radiusWidget) {
        axlPoint p1p2 = (*(d->cone->basePoint())-(*(d->cone->apex())));
        axlPoint mp3(1.0, 1.0, 1.0);

        //axlPoint pv(mp3.y() * p1p2.z() - mp3.z() * p1p2.y(), mp3.x() * p1p2.z() - mp3.z() * p1p2.x(), mp3.x() * p1p2.y() - mp3.y() * p1p2.x());
        axlPoint pv = axlPoint::crossProduct(mp3, p1p2);
        pv.normalize();
        pv *= d->cone->radius();
        pv += center;

        d->radiusWidget->SetPosition(pv.coordinates());
    }

    //Translation
    this->getActor()->SetPosition(center.coordinates());

    //Update fields.
    if(!d->cone->fields().isEmpty())
        d->cone->touchField();

    if(d->cone->updateView())
        emit updated();
}

axlActorCone::axlActorCone(void) : axlActor(), d(new axlActorConePrivate)
{
    d->cone = NULL;
    d->lineWidget = NULL;
    d->radiusWidget = NULL;
//    d->axlConePicker = NULL;
    d->coneObserver = NULL;
    d->coneSource =NULL;
    d->widget = NULL;
}

axlActorCone::~axlActorCone(void)
{
    delete d;

    d = NULL;
}


axlAbstractActor *createAxlActorCone(void){

    return axlActorCone::New();
}
