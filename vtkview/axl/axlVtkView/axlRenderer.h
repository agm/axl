/* axlRenderer.h ---
 *
 * Author: Meriadeg Perrinel
 * Copyright (C) 2008 - Meriadeg Perrinel, Inria.
 * Created: Tue Nov  9 16:58:59 2010 (+0100)
 * Version: $Id$
 * Last-Updated: Tue Nov  9 17:09:38 2010 (+0100)
 *           By: Meriadeg Perrinel
 *     Update #: 19
 */

/* Commentary:
 *
 */

/* Change log:
 *
 */



/*=========================================================================

  Program:   Visualization Toolkit
  Module:    axlRenderer.h

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME axlRenderer - OpenGL renderer
// .SECTION Description
// axlRenderer is a concrete implementation of the abstract class
// vtkRenderer. axlRenderer interfaces to the OpenGL graphics library.

#ifndef AXLRENDERER_H
#define AXLRENDERER_H

#include <vtkOpenGLRenderer.h>

#include "vtkViewPluginExport.h"

class vtkHardwareSelector;
class vtkRendererDelegate;
class vtkRenderPass;
class vtkTexture;

class axlRendererPrivate;

class VTK_RENDERING_EXPORT axlRenderer : public vtkOpenGLRenderer
{

public:
  vtkTypeMacro(axlRenderer,vtkOpenGLRenderer);

  static axlRenderer* New() { return new axlRenderer; }

  void PrintSelf(ostream& os, vtkIndent indent);

  //static axlRenderer *New();

  void useGrid(bool use);

  bool grid(void);

  void ResetCamera( double bounds[6] );


  // Description:
  // Automatically set up the camera based on the visible actors.
  // The camera will reposition itself to view the center point of the actors,
  // and move along its initial view plane normal (i.e., vector defined from
  // camera position to focal point) so that all of the actors can be seen.
  void ResetCamera();

  // Alternative version of ResetCamera(bounds[6]);
  void ResetCamera(double xmin, double xmax,
                                double ymin, double ymax,
                                double zmin, double zmax);

  void ResetCameraClippingRange( double bounds[6] );
  void ResetCameraClippingRange();




protected:
  axlRenderer();
  ~axlRenderer();


private:
  axlRenderer(const axlRenderer&);  // Not implemented.
  void operator=(const axlRenderer&);  // Not implemented.

private:
    axlRendererPrivate *d;
};

#endif
