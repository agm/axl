#pragma once

#include "axlActorBSpline.h"

#include "axlVtkViewPluginExport.h"

#include <vtkVersion.h>

class axlAbstractShapeBSpline;

class axlActorShapeBSplinePrivate;

class AXLVTKVIEWPLUGIN_EXPORT axlActorShapeBSpline : public axlActorBSpline
{
public:
#if (VTK_MAJOR_VERSION <= 5)
    vtkTypeRevisionMacro(axlActorShapeBSpline, vtkAssembly);
#endif

    static axlActorShapeBSpline *New(void);

public:
    dtkAbstractData *data(void);

public:
    virtual void setData(dtkAbstractData *spline_shape1);
    void setMapperCollorArray(void);
    void meshProcess(void);
    void normalsUpdate(void);
    void polyDataUpdate(void);
    void pointsUpdate(void);

public slots:
    // re-implemented from axlActorBSpline
    virtual void onUpdateGeometry(void);

    virtual void onSamplingChanged(void);
    void onSelectBoundaryEdge(int numEdge, int previous, int n);

protected:
    axlActorShapeBSpline(void);
    ~axlActorShapeBSpline(void);

private:
    axlActorShapeBSpline(const axlActorShapeBSpline&); // Not implemented.
    void operator = (const axlActorShapeBSpline&); // Not implemented.

private:
    axlActorShapeBSplinePrivate *d;
};

axlAbstractActor *createAxlActorShapeBSpline(void);
