/* axlActorSurfaceBSpline.cpp ---
 *
 * Author: Meriadeg Perrinel
 * Copyright (C) 2008 - Meriadeg Perrinel, Inria.
 * Created: Fri Dec 17 11:06:08 2010 (+0100)
 * Version: $Id$
 * Last-Updated: Mon Dec 17 15:04:10 2012 (+0100)
 *           By: Julien Wintz
 *     Update #: 78
 */

/* Commentary:
 *
 */

/* Change log:
 *
 */

#include "axlActorSurfaceBSpline.h"
#include "axlControlPointsWidget.h"

#include <axlCore/axlAbstractSurfaceBSpline.h>
#include <axlCore/axlAbstractField.h>

#include <dtkMathSupport/dtkVector3D.h>

#include <vtkActor.h>
#include <vtkActorCollection.h>
#include <vtkCellArray.h>
#include <vtkCellData.h>
#include <vtkCommand.h>
#include <vtkDoubleArray.h>
#include <vtkLookupTable.h>
#include <vtkObjectFactory.h>
//#include <vtkPainterPolyDataMapper.h>
#include <vtkPoints.h>
#include <vtkPointData.h>
#include <vtkPolyData.h>
#include <vtkPolyVertex.h>
#include <vtkPolygon.h>
#include <vtkPolyDataMapper.h>
#include <vtkPolyDataNormals.h>
#include <vtkProperty.h>
#include <vtkProp.h>
#include <vtkQuad.h>
#include <vtkTexture.h>
//#include <vtkTriangle.h>
#include <vtkTriangleStrip.h>
#include <vtkTimerLog.h>
#include <vtkSmartPointer.h>
#include <vtkStripper.h>
#include <vtkPointData.h>
#include <vtkTriangleFilter.h>
#include <vtkPNGReader.h>
#include <vtkFloatArray.h>
//#include <vtkOpenGLHardwareSupport.h>
#include <vtkOpenGLRenderWindow.h>
//#include <vtkPainterPolyDataMapper.h>
#include <vtkProperty.h>

#include <vtkFeatureEdges.h>
#include <vtkExtractEdges.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkRendererCollection.h>


//for meshing and create poly data structure.
#include <axlCore/axlAbstractDataConverter.h>
#include <dtkCoreSupport/dtkAbstractDataFactory.h>
#include <axlCore/axlMesh.h>

#include <vtkLine.h>
#include <vtkTriangle.h>

// /////////////////////////////////////////////////////////////////
// axlActorSurfaceBSplinePrivate
// /////////////////////////////////////////////////////////////////

class axlActorSurfaceBSplinePrivate
{
public:
    axlAbstractSurfaceBSpline *splineSurface;
    vtkSmartPointer<vtkActor> edgeActor;

    vtkSmartPointer<vtkCellArray> lines;
};

#if (VTK_MAJOR_VERSION <= 5)
vtkCxxRevisionMacro(axlActorSurfaceBSpline, "$Revision: 0.0.1 $");
#endif

vtkStandardNewMacro(axlActorSurfaceBSpline);

dtkAbstractData *axlActorSurfaceBSpline::data(void)
{
    return d->splineSurface;
}

void axlActorSurfaceBSpline::setData(dtkAbstractData *spline_Surface1)
{
    axlAbstractSurfaceBSpline *spline_Surface = dynamic_cast<axlAbstractSurfaceBSpline *>(spline_Surface1);
    // we compute here points, triangles, and normals
    d->splineSurface = spline_Surface;

    this->setPoints(vtkSmartPointer<vtkPoints>::New());

    this->setNormals(vtkSmartPointer<vtkDoubleArray>::New());

    // qDebug()<<"axlActorSurfaceBSpline::setSurface 2"<<vtkTimerLog::GetCPUTime()<<vtkTimerLog::GetCPUTime()-currentTime;

    this->setActor(vtkSmartPointer<vtkActor>::New());
    this->setCellArray(vtkSmartPointer<vtkCellArray>::New());
    this->setPolyData(vtkSmartPointer<vtkPolyData>::New());
    this->setMapper(vtkSmartPointer<vtkPolyDataMapper>::New());

    //this->pointsUpdate();
    this->normalsUpdate();
    this->polyDataUpdate();

    this->getPolyData()->SetPoints(this->getPoints());
    //this->getPolyData()->SetLines(d->lines);
    this->getPolyData()->SetPolys(this->getCellArray());
    this->getPolyData()->GetPointData()->SetNormals(this->getNormals());

    //vtkSmartPointer<vtkPolyDataNormals> polyDataNormals = vtkSmartPointer<vtkPolyDataNormals>::New();
    //polyDataNormals->AutoOrientNormalsOn();
    //polyDataNormals->FlipNormalsOn();
    //polyDataNormals->SetInput(this->getPolyData());
#if (VTK_MAJOR_VERSION <= 5)
    this->getMapper()->SetInput(this->getPolyData());
#else
    this->getMapper()->SetInputData(this->getPolyData());
#endif
    this->getActor()->SetMapper(this->getMapper());

    this->getMapper()->ScalarVisibilityOff();

    // add sphere param
    // this->initCurrentPoint();
    //vtkSmartPointer<vtkProp3D> prop3d =  this->getActor();
    this->AddPart(this->getActor());

    // add the observer
    if(!this->getObserver())
    {
        this->NewObserver();
        this->setObserverData(d->splineSurface);
    }

    QColor color = d->splineSurface->color();
    this->getActor()->GetProperty()->SetColor(color.redF(), color.greenF(), color.blueF());

    QString shader = d->splineSurface->shader();
    if(!shader.isEmpty())
        this->setShader(shader);


    ////////////////////////////////

    //    char* fname1 =
    //            vtkTestUtilities::ExpandDataFileName(1, new char*(), "Data/ground.png");

    //    vtkPNGReader * imageReaderRed = vtkPNGReader::New();
    //    imageReaderRed->SetFileName(fname1);

    //    imageReaderRed->Update();

    //    vtkFloatArray *TCoords = vtkFloatArray::New();
    //    TCoords->SetNumberOfComponents(2);
    //    TCoords->SetNumberOfTuples(this->getPoints()->GetNumberOfPoints());

    //    for(int j = 0; j < n_v; j++)
    //    {
    //        for(int i = 0; i < n_u; i++)
    //        {
    //            if( i % 2 == 0 )
    //                if( j % 2 == 0 )
    //                {

    //                    TCoords->SetTuple2(j* n_u + i, 1.0, 0.0);
    //                }
    //                else
    //                {

    //                    TCoords->SetTuple2(j* n_u + i,1.0, 1.0);
    //                }
    //            else
    //                if( j % 2 == 0 )
    //                {

    //                    TCoords->SetTuple2(j* n_u + i,0.0, 0.0);
    //                }
    //                else
    //                {

    //                    TCoords->SetTuple2(j* n_u + i, 0.0, 1.0);
    //                }

    //        }
    //    }

    //    TCoords->SetName("MultTCoords");

    //    this->getPolyData()->GetPointData()->SetTCoords(TCoords);

    //    vtkTexture * textureRed =  vtkTexture::New();
    //    textureRed->SetInputConnection(imageReaderRed->GetOutputPort());
    //    textureRed->SetBlendingMode(vtkTexture::VTK_TEXTURE_BLENDING_MODE_REPLACE);
    //    textureRed->SetRepeat(true);

    //    this->getMapper()->SetInput(this->getPolyData());
    //    this->getActor()->SetMapper(this->getMapper());
    //    this->getActor()->SetVisibility(true);


    //    // no multitexturing just show the green texture.
    //    this->getActor()->SetTexture(textureRed);

    //if(d->splineSurface->identifier() == "axlSurfaceBSpline"){
    connect(d->splineSurface,SIGNAL(edgeSelected(int,int,int)),this,SLOT(onSelectBoundaryEdge(int,int,int)));

    //}

    // For updating the geometry (ie. the mesh representation of the spline)
    connect(d->splineSurface,SIGNAL(modifiedGeometry()), this, SLOT(onUpdateGeometry()));
    connect(d->splineSurface,SIGNAL(modifiedProperty()), this, SLOT(onUpdateProperty()));
    // For redrawing the control point widget 
    connect(d->splineSurface,SIGNAL(modifiedStructure()), this, SLOT(onUpdateStructure()));

}


void axlActorSurfaceBSpline::setMapperCollorArray(void)
{
    vtkSmartPointer<vtkDoubleArray> scalarArray = vtkSmartPointer<vtkDoubleArray>::New();
    scalarArray->SetName("mapperCollorArrayDefaultField");
    scalarArray->SetNumberOfComponents(1);

    if(!d->splineSurface->hasCells()){

        double start_u = d->splineSurface->startParam_u();
        double start_v = d->splineSurface->startParam_v();
        double end_u = d->splineSurface->endParam_u();
        double end_v = d->splineSurface->endParam_v();
        double paramCourant_u = start_u;
        double paramCourant_v = start_v;

        int n_u = d->splineSurface->numSamples_u();// need to be superior than 1
        int n_v = d->splineSurface->numSamples_v();

        scalarArray->SetNumberOfTuples(n_u * n_v);

        double interval_u = (double)(end_u - start_u) / (n_u - 1);
        double interval_v = (double)(end_v - start_v) / (n_v - 1);

        for(int i = 0; i < n_v - 1 ; i++)
        {
            for(int j = 0; j < n_u - 1 ; j++)
            {
                scalarArray->SetTuple1(i * n_u + j, d->splineSurface->scalarValue(paramCourant_u, paramCourant_v));
                paramCourant_u += interval_u;
            }

            scalarArray->SetTuple1(i * n_u + (n_u - 1), d->splineSurface->scalarValue(end_u, paramCourant_v));
            paramCourant_u = start_u;
            paramCourant_v += interval_v;
        }
        for(int i = 0; i < n_u - 1; i++)
        {
            scalarArray->SetTuple1(n_u * (n_v - 1) + i, d->splineSurface->scalarValue(paramCourant_u, end_v));
            paramCourant_u += interval_u;
        }
        scalarArray->SetTuple1(n_u * n_v - 1, d->splineSurface->scalarValue(end_u, end_v));


        vtkSmartPointer<vtkPolyData> data = this->getPolyData();
        data->GetPointData()->AddArray(scalarArray);
        data->GetPointData()->SetActiveScalars("mapperCollorArrayDefaultField");

        vtkSmartPointer<vtkLookupTable> lookupTable = vtkSmartPointer<vtkLookupTable>::New();
        lookupTable->SetRange(-1.0, 1.0);
        lookupTable->SetNumberOfTableValues(d->splineSurface->stripes());
        lookupTable->Build();
        for(int i= 0; i < d->splineSurface->stripes(); i+=2)
        {
            lookupTable->SetTableValue(i  , 1.0, 1.0, 1.0);
            lookupTable->SetTableValue(i+1, 0.0, 0.0, 0.0);
        }
        // test to get light direction .... TO DELETE

        vtkSmartPointer<vtkDoubleArray> lightArray = vtkSmartPointer<vtkDoubleArray>::New();
        lightArray->SetName("mapperLightDirection");
        lightArray->SetNumberOfComponents(3);

        paramCourant_u = start_u;
        paramCourant_v = start_v;

        lightArray->SetNumberOfTuples(n_u * n_v);
        double *currentLight = new double[3];
        currentLight[0] = 1.0;
        currentLight[1] = 0.6;
        currentLight[2] = 0.2;
        for(int j = 0; j < n_v - 1; j++)
        {
            for(int i = 0; i < n_u - 1; i++)
            {
                lightArray->SetTuple(j * n_u + i, currentLight);
            }

            lightArray->SetTuple(j * n_u +  n_u - 1, currentLight);
            paramCourant_u = start_u;
            paramCourant_v += interval_v;
        }

        for(int i = 0; i < n_u - 1; i++)
        {
            lightArray->SetTuple((n_v - 1) * n_u + i, currentLight);
            paramCourant_u += interval_u;
        }
        lightArray->SetTuple(n_v * n_u - 1, currentLight);

        data = this->getPolyData();
        data->GetPointData()->AddArray(lightArray);


        //add vertex attrib array
        vtkSmartPointer<vtkPolyDataMapper> mapper = this->getMapper();
        vtkSmartPointer<vtkPolyData> polyData = this->getPolyData();
        mapper->MapDataArrayToVertexAttribute("scalarsattrib",  data->GetPointData()->GetArrayName(2), vtkDataObject::FIELD_ASSOCIATION_POINTS, -1);
        mapper->SetLookupTable(lookupTable);
        mapper->SetInterpolateScalarsBeforeMapping(true);
        mapper->UseLookupTableScalarRangeOn();


    }else {
        int addIndex = 0;
        for(int indice=0; indice < d->splineSurface->countCells();indice++){

            double start_u = d->splineSurface->startParam_u(indice);
            double start_v = d->splineSurface->startParam_v(indice);
            double end_u = d->splineSurface->endParam_u(indice);
            double end_v = d->splineSurface->endParam_v(indice);
            double paramCourant_u = start_u;
            double paramCourant_v = start_v;

            int n_u = d->splineSurface->numSamples_u(indice);// need to be superior than 1
            int n_v = d->splineSurface->numSamples_v(indice);

            scalarArray->SetNumberOfTuples(n_u * n_v);

            double interval_u = (double)(end_u - start_u) / (n_u - 1);
            double interval_v = (double)(end_v - start_v) / (n_v - 1);

            for(int i = 0; i < n_v - 1 ; i++)
            {
                for(int j = 0; j < n_u - 1 ; j++)
                {
                    scalarArray->SetTuple1(i * n_u + j+addIndex, d->splineSurface->scalarValue(paramCourant_u, paramCourant_v));
                    paramCourant_u += interval_u;
                }

                scalarArray->SetTuple1(i * n_u + (n_u - 1)+addIndex, d->splineSurface->scalarValue(end_u, paramCourant_v));
                paramCourant_u = start_u;
                paramCourant_v += interval_v;
            }
            for(int i = 0; i < n_u - 1; i++)
            {
                scalarArray->SetTuple1(n_u * (n_v - 1) + i+addIndex, d->splineSurface->scalarValue(paramCourant_u, end_v));
                paramCourant_u += interval_u;
            }
            scalarArray->SetTuple1(n_u * n_v - 1+addIndex, d->splineSurface->scalarValue(end_u, end_v));


            vtkSmartPointer<vtkPolyData> data = this->getPolyData();
            data->GetPointData()->AddArray(scalarArray);
            data->GetPointData()->SetActiveScalars("mapperCollorArrayDefaultField");

            vtkSmartPointer<vtkLookupTable> lookupTable = vtkSmartPointer<vtkLookupTable>::New();
            lookupTable->SetRange(-1.0, 1.0);
            lookupTable->SetNumberOfTableValues(d->splineSurface->stripes());
            lookupTable->Build();
            for(int i= 0; i < d->splineSurface->stripes(); i+=2)
            {
                lookupTable->SetTableValue(i  , 1.0, 1.0, 1.0);
                lookupTable->SetTableValue(i+1, 0.0, 0.0, 0.0);
            }
            // test to get light direction .... TO DELETE

            vtkSmartPointer<vtkDoubleArray> lightArray = vtkSmartPointer<vtkDoubleArray>::New();
            lightArray->SetName("mapperLightDirection");
            lightArray->SetNumberOfComponents(3);

            paramCourant_u = start_u;
            paramCourant_v = start_v;

            lightArray->SetNumberOfTuples(n_u * n_v);
            double *currentLight = new double[3];
            currentLight[0] = 1.0;
            currentLight[1] = 0.6;
            currentLight[2] = 0.2;
            for(int j = 0; j < n_v - 1; j++)
            {
                for(int i = 0; i < n_u - 1; i++)
                {
                    lightArray->SetTuple(j * n_u + i+addIndex, currentLight);
                }

                lightArray->SetTuple(j * n_u +  n_u - 1+addIndex, currentLight);
                paramCourant_u = start_u;
                paramCourant_v += interval_v;
            }

            for(int i = 0; i < n_u - 1; i++)
            {
                lightArray->SetTuple((n_v - 1) * n_u + i+addIndex, currentLight);
                paramCourant_u += interval_u;
            }
            lightArray->SetTuple(n_v * n_u - 1+addIndex, currentLight);

            data = this->getPolyData();
            data->GetPointData()->AddArray(lightArray);


            //add vertex attrib array
            vtkSmartPointer<vtkPolyDataMapper> mapper = this->getMapper();
            vtkSmartPointer<vtkPolyData> polyData = this->getPolyData();
            mapper->MapDataArrayToVertexAttribute("scalarsattrib",  data->GetPointData()->GetArrayName(2), vtkDataObject::FIELD_ASSOCIATION_POINTS, -1);
            mapper->SetLookupTable(lookupTable);
            mapper->SetInterpolateScalarsBeforeMapping(true);
            mapper->UseLookupTableScalarRangeOn();
            addIndex = addIndex + n_u*n_v;

        }

    }

    this->Modified();
}


void axlActorSurfaceBSpline::onSamplingChanged(void)
{


    if(d->splineSurface)
    {

        this->getMapper()->RemoveAllInputs();
        this->getPolyData()->Initialize();

        // delete current vtkPoint and vtkCellArray
        this->getPoints()->Reset();
        this->getCellArray()->Reset();

        this->getPoints()->Squeeze();
        this->getCellArray()->Squeeze();

        this->getNormals()->Initialize();
        //this->getPolyData()->GetPointData()->RemoveArray("mapperCollorArrayIsophote");//back to solid color

        //this->pointsUpdate();
        this->normalsUpdate();

        this->getPolyData()->GetPointData()->SetNormals(this->getNormals());

        this->polyDataUpdate();


        this->getPolyData()->SetPoints(this->getPoints());
        this->getPolyData()->SetPolys(this->getCellArray());

#if (VTK_MAJOR_VERSION <= 5)
        this->getMapper()->SetInput(this->getPolyData());
#else
        this->getMapper()->SetInputData(this->getPolyData());
#endif

    }

    if(d->splineSurface->fields().count() != 0){
        d->splineSurface->touchField();
        d->splineSurface->touchGeometry();
    }



}

void axlActorSurfaceBSpline::normalsUpdate(void){
    // we consider here that all memory vtk pipeline from points to polydata are free but not deleted...
    if(!d->splineSurface->hasCells()){
        double start_u = d->splineSurface->startParam_u();
        double start_v = d->splineSurface->startParam_v();
        double end_u = d->splineSurface->endParam_u();
        double end_v = d->splineSurface->endParam_v();
        double paramCourant_u = start_u;
        double paramCourant_v = start_v;


        int n_u = d->splineSurface->numSamples_u();// need to be superior than 1
        int n_v = d->splineSurface->numSamples_v();

        double interval_u = (double)(end_u - start_u) / (n_u - 1);
        double interval_v = (double)(end_v - start_v) / (n_v - 1);

        vtkSmartPointer<vtkDoubleArray> normals = this->getNormals();

        normals->SetNumberOfComponents(3);
        normals->SetNumberOfTuples(n_u * n_v);
        normals->SetName("normalArray");
        axlPoint *currentNormal = new axlPoint(0.,0.,1.);

        //double currentTime = vtkTimerLog::GetCPUTime();

        //qDebug()<<"axlActorSurfaceBSpline::setSurface 1";
        int ind1 = 0;
        int ind2 = 0;
        for(int i = 0; i < n_v - 1; i++)
        {
            ind1 = i * n_u;
            for(int j = 0; j < n_u - 1; j++)
            {
                ind2 = ind1 +j;

                d->splineSurface->normal(currentNormal ,paramCourant_u, paramCourant_v);
                currentNormal->normalize();

                normals->SetTuple(ind2, currentNormal->coordinates());

                paramCourant_u += interval_u;
            }
            ind2 = ind1 + (n_u - 1);


            d->splineSurface->normal(currentNormal, end_u, paramCourant_v);
            currentNormal->normalize();

            normals->SetTuple(ind2, currentNormal->coordinates());

            paramCourant_u = start_u;
            paramCourant_v += interval_v;
        }
        ind1 = n_u * (n_v - 1);
        for(int i = 0; i < n_u - 1; i++)
        {
            ind2 = ind1 + i;

            d->splineSurface->normal(currentNormal, paramCourant_u, end_v);
            currentNormal->normalize();

            normals->SetTuple(ind2, currentNormal->coordinates());

            paramCourant_u+=interval_u;
            //qDebug()<< "ind 3"<<ind3;

        }
        ind1 = n_u * n_v - 1;

        d->splineSurface->normal(currentNormal, end_u, end_v);
        currentNormal->normalize();

        normals->SetTuple(ind1, currentNormal->coordinates());


        delete currentNormal;
    }else {

        int numCell = d->splineSurface->countCells();
        int addIndex = 0;
        int dataArraysSize =0;


        for(int in = 0; in <numCell;in++){
            int n_u = d->splineSurface->numSamples_u(in);// need to be superior than 1
            int n_v = d->splineSurface->numSamples_v(in);
            dataArraysSize = dataArraysSize + n_u*n_v;

        }

        vtkSmartPointer<vtkDoubleArray> normals = this->getNormals();
        normals->SetNumberOfComponents(3);
        normals->SetNumberOfTuples(dataArraysSize);
        normals->SetName("normalArray");

        for(int indice =0;indice < numCell;indice ++){

            double start_u = d->splineSurface->startParam_u(indice);
            double start_v = d->splineSurface->startParam_v(indice);
            double end_u = d->splineSurface->endParam_u(indice);
            double end_v = d->splineSurface->endParam_v(indice);
            double paramCourant_u = start_u;
            double paramCourant_v = start_v;

            int n_u = d->splineSurface->numSamples_u(indice);// need to be superior than 1
            int n_v = d->splineSurface->numSamples_v(indice);


            double interval_u = (double)(end_u - start_u) / (n_u - 1);
            double interval_v = (double)(end_v - start_v) / (n_v - 1);

            axlPoint *currentNormal = new axlPoint(0.,0.,1.);

            //double currentTime = vtkTimerLog::GetCPUTime();

            // qDebug()<<"axlActorSurfaceBSpline::setSurface 1" <<currentTime;
            int ind1 = 0;
            int ind2 = 0;
            for(int i = 0; i < n_v - 1; i++)
            {
                ind1 = i * n_u;
                for(int j = 0; j < n_u - 1; j++)
                {
                    ind2 = ind1 +j;

                    d->splineSurface->normal(currentNormal ,paramCourant_u, paramCourant_v,indice);
                    currentNormal->normalize();

                    normals->SetTuple(ind2+addIndex, currentNormal->coordinates());

                    paramCourant_u += interval_u;
                }
                ind2 = ind1 + (n_u - 1);

                d->splineSurface->normal(currentNormal, end_u, paramCourant_v,indice);
                currentNormal->normalize();

                normals->SetTuple(ind2+addIndex, currentNormal->coordinates());

                paramCourant_u = start_u;
                paramCourant_v += interval_v;
            }
            ind1 = n_u * (n_v - 1);
            for(int i = 0; i < n_u - 1; i++)
            {
                ind2 = ind1 + i;

                d->splineSurface->normal(currentNormal, paramCourant_u, end_v,indice);
                currentNormal->normalize();

                normals->SetTuple(ind2 +addIndex, currentNormal->coordinates());

                paramCourant_u+=interval_u;
                //qDebug()<< "ind 3"<<ind3;

            }
            ind1 = n_u * n_v - 1;

            d->splineSurface->normal(currentNormal, end_u, end_v,indice);
            currentNormal->normalize();

            normals->SetTuple(ind1+addIndex, currentNormal->coordinates());


            delete currentNormal;

            addIndex = addIndex + n_u*n_v;

        }
    }
}

void axlActorSurfaceBSpline::pointsUpdate(void)
{
     //qDebug()<<"pointsUpdate()";
     //std::cin.get();

     if(d->splineSurface->rational())
     {
         d->splineSurface->updateRcoeff();
     }

    // we consider here that all memory vtk pipeline from points to polydata are free but not deleted...
    if(!d->splineSurface->hasCells()){

        //std::cout<<"axlActorSurfaceBSpline::pointsUpdate(void)::d->splineSurface->hasCells()=false"<<std::endl;
        //qDebug()<<"rational_="<<d->splineSurface->rational();

        double start_u = d->splineSurface->startParam_u();
        double start_v = d->splineSurface->startParam_v();
        double end_u = d->splineSurface->endParam_u();
        double end_v = d->splineSurface->endParam_v();
        double paramCourant_u = start_u;
        double paramCourant_v = start_v;

        vtkPoints *points = this->getPoints();

        int n_u = d->splineSurface->numSamples_u();// need to be superior than 1
        int n_v = d->splineSurface->numSamples_v();
        points->SetNumberOfPoints(n_u * n_v);

        axlPoint *pointCourant = new axlPoint;
        double interval_u = (double)(end_u - start_u) / (n_u - 1);
        double interval_v = (double)(end_v - start_v) / (n_v - 1);

         //qDebug()<<"axlActorSurfaceBSpline::setSurface 1"
        int ind1 = 0;
        int ind2 = 0;
        for(int i = 0; i < n_v - 1; i++)
        {
            ind1 = i * n_u;
            for(int j = 0; j < n_u - 1; j++)
            {
                ind2 = ind1 +j;
                d->splineSurface->eval(pointCourant, paramCourant_u, paramCourant_v);

                points->SetPoint(ind2, pointCourant->coordinates());

                paramCourant_u += interval_u;
            }
            ind2 = ind1 + (n_u - 1);

            d->splineSurface->eval(pointCourant, end_u, paramCourant_v);
            points->SetPoint(ind2 , pointCourant->coordinates());


            paramCourant_u = start_u;
            paramCourant_v += interval_v;
        }
        ind1 = n_u * (n_v - 1);
        for(int i = 0; i < n_u - 1; i++)
        {
            ind2 = ind1 + i;
            d->splineSurface->eval(pointCourant, paramCourant_u, end_v);
            points->SetPoint(ind2, pointCourant->coordinates());


            paramCourant_u+=interval_u;
            //qDebug()<< "ind 3"<<ind3;

        }
        ind1 = n_u * n_v - 1;
        d->splineSurface->eval(pointCourant, end_u, end_v);
        points->SetPoint(ind1, pointCourant->coordinates());

        delete pointCourant;
    } else {

        std::cout<<"axlActorSurfaceBSpline::pointsUpdate(void)::d->splineSurface->hasCells()=ture"<<std::endl;

        int numCell = d->splineSurface->countCells();
        int addIndex = 0;
        int dataArraysSize =0;


        for(int in = 0; in <numCell;in++){
            int n_u = d->splineSurface->numSamples_u(in);// need to be superior than 1
            int n_v = d->splineSurface->numSamples_v(in);
            dataArraysSize = dataArraysSize + n_u*n_v;

        }

        vtkPoints *points = this->getPoints();
        points->SetNumberOfPoints(dataArraysSize);



        for(int indice =0;indice < numCell;indice ++){

            double start_u = d->splineSurface->startParam_u(indice);
            double start_v = d->splineSurface->startParam_v(indice);
            double end_u = d->splineSurface->endParam_u(indice);
            double end_v = d->splineSurface->endParam_v(indice);
            double paramCourant_u = start_u;
            double paramCourant_v = start_v;

            int n_u = d->splineSurface->numSamples_u(indice);// need to be superior than 1
            int n_v = d->splineSurface->numSamples_v(indice);

            axlPoint *pointCourant = new axlPoint(0.,0.,0.);
            double interval_u = (double)(end_u - start_u) / (n_u - 1);
            double interval_v = (double)(end_v - start_v) / (n_v - 1);


            //double currentTime = vtkTimerLog::GetCPUTime();

            // qDebug()<<"axlActorSurfaceBSpline::setSurface 1" <<currentTime;
            int ind1 = 0;
            int ind2 = 0;
            for(int i = 0; i < n_v - 1; i++)
            {
                ind1 = i * n_u;
                for(int j = 0; j < n_u - 1; j++)
                {
                    ind2 = ind1 +j;
                    d->splineSurface->eval(pointCourant, paramCourant_u, paramCourant_v, indice);
                    points->SetPoint(ind2+addIndex, pointCourant->coordinates());

                    paramCourant_u += interval_u;
                }
                ind2 = ind1 + (n_u - 1);

                d->splineSurface->eval(pointCourant, end_u, paramCourant_v,indice);
                points->SetPoint(ind2+addIndex , pointCourant->coordinates());


                paramCourant_u = start_u;
                paramCourant_v += interval_v;
            }
            ind1 = n_u * (n_v - 1);
            for(int i = 0; i < n_u - 1; i++)
            {
                ind2 = ind1 + i;
                d->splineSurface->eval(pointCourant, paramCourant_u, end_v,indice);
                points->SetPoint(ind2 +addIndex, pointCourant->coordinates());


                paramCourant_u+=interval_u;
                //qDebug()<< "ind 3"<<ind3;

            }
            ind1 = n_u * n_v - 1;
            d->splineSurface->eval(pointCourant, end_u, end_v,indice);
            points->SetPoint(ind1+addIndex, pointCourant->coordinates());

            delete pointCourant;

            addIndex = addIndex + n_u*n_v;

        }
    }


}

//void axlActorSurfaceBSpline::polyDataUpdate(void)
//{

//    vtkSmartPointer<vtkCellArray> cellArray = this->getCellArray();

//    if(!d->splineSurface->hasCells()){
//        int n_u = d->splineSurface->numSamples_u();
//        int n_v = d->splineSurface->numSamples_v();


//        int ind1 = 0;
//        int ind2 = 0;

//        vtkSmartPointer<vtkQuad> currentQuad = vtkSmartPointer<vtkQuad>::New();
//        currentQuad->GetPointIds()->SetNumberOfIds(4);

//        for(int j = 0; j < n_v - 1; j++)
//        {
//            for(int i= 0; i <n_u - 1; i++)
//            {
//                ind1 =  j * n_u + i;
//                ind2 = ind1 + n_u;

//                currentQuad->GetPointIds()->SetId(0, ind1);
//                currentQuad->GetPointIds()->SetId(1, ind1 + 1);
//                currentQuad->GetPointIds()->SetId(2, ind2 + 1);
//                currentQuad->GetPointIds()->SetId(3, ind2);

//                cellArray->InsertNextCell(currentQuad);
//            }
//        }

//    }else{

//        int addIndex = 0;
//        for(int indice = 0;indice < d->splineSurface->countCells();indice++){
//            int n_u = d->splineSurface->numSamples_u(indice);
//            int n_v = d->splineSurface->numSamples_v(indice);

//            int ind1 = 0;
//            int ind2 = 0;

//            vtkSmartPointer<vtkQuad> currentQuad = vtkSmartPointer<vtkQuad>::New();
//            currentQuad->GetPointIds()->SetNumberOfIds(4);

//            for(int j = 0; j < n_v - 1; j++)
//            {
//                for(int i= 0; i < n_u - 1; i++)
//                {
//                    ind1 =  j * n_u + i + addIndex;
//                    ind2 = ind1 + n_u ;

//                    currentQuad->GetPointIds()->SetId(0, ind1);
//                    currentQuad->GetPointIds()->SetId(1, ind1 + 1);
//                    currentQuad->GetPointIds()->SetId(2, ind2 + 1);
//                    currentQuad->GetPointIds()->SetId(3, ind2);

//                    cellArray->InsertNextCell(currentQuad);
//                }
//            }
//            addIndex = addIndex + n_u*n_v;
//        }

//    }

//}

void axlActorSurfaceBSpline::polyDataUpdate(void){


    vtkSmartPointer<vtkCellArray> cellArray =vtkSmartPointer<vtkCellArray>::New();
    //this->getCellArray();
    vtkSmartPointer<vtkCellArray> linesArray = d->lines;
    //vtkSmartPointer<vtkPoints> pointLines = d->linePoints;


    //    //create mesh
    //    QString convertName = d->splineSurface->identifier();
    //    convertName.append("Converter");
    //    axlAbstractDataConverter *converter = dynamic_cast<axlAbstractDataConverter *>(dtkAbstractDataFactory::instance()->converter(convertName));
    //    if(converter){

    //        qDebug() << Q_FUNC_INFO << "enter converter" << converter->identifier();
    //        converter->setData(d->splineSurface);
    //        axlMesh *mesh = converter->toMesh();


    //        vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
    //        //this->getPoints();
    //        points->SetNumberOfPoints(mesh->vertex_count());
    //        double *coord = new double[3];
    //        //add points, replace pointsUpdate();
    //        for(int i= 0 ; i < mesh->vertex_count(); i++){
    //            mesh->vertex(i,coord);
    //            points->SetPoint(i,coord);
    //        }

    //        this->setPoints(points);


    //        // add vtkLines
    //        vtkSmartPointer<vtkLine> lines = vtkSmartPointer<vtkLine>::New();
    //        lines->GetPointIds()->SetNumberOfIds(2);
    //        for(int i = 0; i < mesh->edge_count();i++){

    //            int f = mesh->edge(i).first();
    //            int s = mesh->edge(i).last();
    //            lines->GetPointIds()->SetId(0,f);
    //            lines->GetPointIds()->SetId(1,s);

    //            //linesArray->InsertNextCell(lines);
    //            cellArray->InsertNextCell(lines);

    //        }

    //        // add vtkTriangles or vtkQuad
    //        for(int i = 0; i < mesh->face_count();i++){


    //            vtkSmartPointer<vtkTriangle> triangles = vtkSmartPointer<vtkTriangle>::New();
    //            triangles->GetPointIds()->SetNumberOfIds(3);
    //            vtkSmartPointer<vtkQuad> quad = vtkSmartPointer<vtkQuad>::New();
    //            quad->GetPointIds()->SetNumberOfIds(4);

    //            if(mesh->face(i).size() == 3){
    //                //triangle

    //                triangles->GetPointIds()->SetId(0, mesh->face(i).first());
    //                triangles->GetPointIds()->SetId(1, mesh->face(i).at(1));
    //                triangles->GetPointIds()->SetId(2, mesh->face(i).last());

    //                cellArray->InsertNextCell(triangles);

    //            }else if(mesh->face(i).size() == 4){
    //                //Quad

    //                quad->GetPointIds()->SetId(0, mesh->face(i).first());
    //                quad->GetPointIds()->SetId(1, mesh->face(i).at(1));
    //                quad->GetPointIds()->SetId(2, mesh->face(i).at(2));
    //                quad->GetPointIds()->SetId(3, mesh->face(i).last());

    //                cellArray->InsertNextCell(quad);
    //            }
    //        }



    //    }else{


    this->pointsUpdate();
    if(!d->splineSurface->hasCells()){
        int n_u = d->splineSurface->numSamples_u();
        int n_v = d->splineSurface->numSamples_v();


        int ind1 = 0;
        int ind2 = 0;

        vtkSmartPointer<vtkQuad> currentQuad = vtkSmartPointer<vtkQuad>::New();
        currentQuad->GetPointIds()->SetNumberOfIds(4);

        for(int j = 0; j < n_v - 1; j++)
        {
            for(int i= 0; i <n_u - 1; i++)
            {
                ind1 =  j * n_u + i;
                ind2 = ind1 + n_u;

                currentQuad->GetPointIds()->SetId(0, ind1);
                currentQuad->GetPointIds()->SetId(1, ind1 + 1);
                currentQuad->GetPointIds()->SetId(2, ind2 + 1);
                currentQuad->GetPointIds()->SetId(3, ind2);

                cellArray->InsertNextCell(currentQuad);
            }
        }

    } else {

        int addIndex = 0;
        for(int indice = 0;indice < d->splineSurface->countCells();indice++){
            int n_u = d->splineSurface->numSamples_u(indice);
            int n_v = d->splineSurface->numSamples_v(indice);

            int ind1 = 0;
            int ind2 = 0;

            vtkSmartPointer<vtkQuad> currentQuad = vtkSmartPointer<vtkQuad>::New();
            currentQuad->GetPointIds()->SetNumberOfIds(4);

            for(int j = 0; j < n_v - 1; j++)
            {
                for(int i= 0; i < n_u - 1; i++)
                {
                    ind1 =  j * n_u + i + addIndex;
                    ind2 = ind1 + n_u ;

                    currentQuad->GetPointIds()->SetId(0, ind1);
                    currentQuad->GetPointIds()->SetId(1, ind1 + 1);
                    currentQuad->GetPointIds()->SetId(2, ind2 + 1);
                    currentQuad->GetPointIds()->SetId(3, ind2);

                    cellArray->InsertNextCell(currentQuad);
                }
            }
            addIndex = addIndex + n_u*n_v;
        }

    }
    //}

    this->setCellArray(cellArray);
}

void axlActorSurfaceBSpline::onUpdateGeometry()
{
    // this->pointsUpdate();
    // this->normalsUpdate();
    // this->polyDataUpdate();

    // Note: ideally we should not need to call this ?
    this->onSamplingChanged();

    if (this->getControlPoints())
        this->getControlPoints()->SetControlPointRadius(d->splineSurface->size());

    emit updated();
}


void axlActorSurfaceBSpline::onSelectBoundaryEdge(int numEdge, int previous, int n)
{
    if(numEdge == -1){
        if(d->edgeActor)
            d->edgeActor->SetVisibility(false);
    }else{

        vtkSmartPointer<vtkPolyData> selectedEdge = vtkSmartPointer<vtkPolyData>::New();
        selectedEdge->SetPoints(this->getPoints());
        selectedEdge->SetLines(d->lines);
        int ne = selectedEdge->GetNumberOfLines();

        selectedEdge->BuildLinks();
        for(int i = 0; i < previous;i++){
            selectedEdge->DeleteCell(i);
        }
        for(int i = previous-1+n; i < ne;i++){
            selectedEdge->DeleteCell(i);
        }
        selectedEdge->RemoveDeletedCells();

        // Visualize
        vtkSmartPointer<vtkPolyDataMapper> edgeMapper =
                vtkSmartPointer<vtkPolyDataMapper>::New();
#if (VTK_MAJOR_VERSION <= 5)
        edgeMapper->SetInput(selectedEdge);
#else
        edgeMapper->SetInputData(selectedEdge);
#endif

        d->edgeActor->SetMapper(edgeMapper);
        d->edgeActor->GetProperty()->EdgeVisibilityOn();
        d->edgeActor->GetProperty()->SetEdgeColor(1,0,0);

        d->edgeActor->GetProperty()->SetLineWidth(6);
        d->edgeActor->SetVisibility(true);
    }

    emit updated();


}


axlActorSurfaceBSpline::axlActorSurfaceBSpline(void) : axlActorBSpline(), d(new axlActorSurfaceBSplinePrivate)
{
    d->splineSurface = NULL;
    d->edgeActor = vtkSmartPointer<vtkActor>::New();
    d->edgeActor->SetVisibility(false);
    this->AddPart(d->edgeActor);

    d->lines = vtkSmartPointer<vtkCellArray>::New();

}

axlActorSurfaceBSpline::~axlActorSurfaceBSpline(void)
{

    if(d->splineSurface->identifier() == "axlSurfaceBSpline")
        disconnect(d->splineSurface,SIGNAL(edgeSelected(int)), this, SLOT(onSelectBoundaryEdge(int)));

    delete d;
    d = NULL;
}

axlAbstractActor *createAxlActorSurfaceBSpline(void){

    return axlActorSurfaceBSpline::New();
}
