
Circle arc
==========
A circle arc is defined by a starting point (axlPoint), an ending point (axlPoint), a normal to the plane containing the arc,  a center (axlPoint) or a radius (double) and an orientation (interior or exterior). 

Type: ``axlCircleArc``

Format:

.. literalinclude:: ../../../data/circle_arc.axl 
   :lines: 2-8
   :language: xml
