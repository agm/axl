/* axlActorTorus.cpp ---
 *
 * Author: Valentin Michelet
 * Copyright (C) 2008 - Valentin Michelet, Inria.
 * Created: Tue Nov  9 16:58:59 2010 (+0100)
 * Version: $Id$
 * Last-Updated: Mon Dec 17 15:02:34 2012 (+0100)
 *           By: Julien Wintz
 *     Update #: 26
 */

/* Commentary:
 *
 */

/* Change log:
 *
 */

#include "axlActorTorus.h"

#include "axlActorComposite.h"
#include "axlInteractorStyleSwitch.h"

#include <axlCore/axlTorus.h>
#include <axlCore/axlPoint.h>

#include <QVTKOpenGLWidget.h>

#include <vtkActor.h>
#include <vtkAssemblyPath.h>
//#include <vtkCellPicker.h>
#include <vtkCommand.h>
#include <vtkGlyph3D.h>
#include <vtkInteractorStyleSwitch.h>
#include <vtkLight.h>
#include <vtkLightCollection.h>
#include <vtkParametricFunctionSource.h>
#include <vtkParametricTorus.h>
#include <vtkLineWidget.h>
#include <vtkObjectFactory.h>
#include <vtkPoints.h>
#include <vtkPointWidget.h>
#include <vtkPolyData.h>
#include <vtkPolyDataMapper.h>
#include <vtkProperty.h>
#include <vtkRenderer.h>
#include <vtkRendererCollection.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkSmartPointer.h>
#include <vtkTransform.h>


class axlActorTorusObserver : public vtkCommand {
public:
    static axlActorTorusObserver *New(void) {
        return new axlActorTorusObserver;
    }

    virtual void Execute(vtkObject *caller, unsigned long event, void *) {
        vtkRenderWindowInteractor *interactor = observerDataAssembly->getInteractor();            //interactor->Render();

        if(event == vtkCommand::InteractionEvent) {
            observerData_torusSource->Update();
            observerData_torusSource->Modified();
            interactor->Render();

            if (!ringRadiusPointWidget || !crossSectionRadiusPointWidget || !directionLineWidget)
                return;

//            axlPoint center = *(observerData_torus->centerPoint());
//
////                axlPoint orientationX((*observerDataAssembly->getActor()->GetMatrix())[0][0], (*observerDataAssembly->getActor()->GetMatrix())[1][0], (*observerDataAssembly->getActor()->GetMatrix())[2][0]);
//            axlPoint orientationX(observerDataAssembly->getActor()->GetMatrix()->GetElement(0,0), observerDataAssembly->getActor()->GetMatrix()->GetElement(1,0), observerDataAssembly->getActor()->GetMatrix()->GetElement(2,0));
////                axlPoint orientationY((*observerDataAssembly->getActor()->GetMatrix())[0][1], (*observerDataAssembly->getActor()->GetMatrix())[1][1], (*observerDataAssembly->getActor()->GetMatrix())[2][1]);
//            axlPoint orientationY(observerDataAssembly->getActor()->GetMatrix()->GetElement(0,1), observerDataAssembly->getActor()->GetMatrix()->GetElement(1,1), observerDataAssembly->getActor()->GetMatrix()->GetElement(2,1));
//            //axlPoint orientationZ((*observerDataAssembly->getActor()->GetMatrix())[0][2], (*observerDataAssembly->getActor()->GetMatrix())[1][2], (*observerDataAssembly->getActor()->GetMatrix())[2][2]);
//            axlPoint orientationZ(*(observerData_torus->direction()) - *(observerData_torus->centerPoint()));
//
//            orientationX.normalize();
//            orientationY.normalize();
//            orientationZ.normalize();
//
//            axlPoint ringPoint(ringRadiusPointWidget->GetPosition()[0], ringRadiusPointWidget->GetPosition()[1], ringRadiusPointWidget->GetPosition()[2]);
//            axlPoint ringPointStart((center + (orientationX * (observerData_torus->ringRadius() + observerData_torus->crossSectionRadius()))).coordinates());
//
//            axlPoint crossSectionPoint(crossSectionRadiusPointWidget->GetPosition()[0], crossSectionRadiusPointWidget->GetPosition()[1], crossSectionRadiusPointWidget->GetPosition()[2]);
//            axlPoint crossSectionPointStart((center + (orientationY * (observerData_torus->ringRadius() - observerData_torus->crossSectionRadius()))).coordinates());
//
//            axlPoint directionPoint1(directionLineWidget->GetPoint1()[0], directionLineWidget->GetPoint1()[1], directionLineWidget->GetPoint1()[2]);
//            axlPoint directionPoint2(directionLineWidget->GetPoint2()[0], directionLineWidget->GetPoint2()[1], directionLineWidget->GetPoint2()[2]);
//
//            double ringRadius = observerData_torus->ringRadius();
//            double crossSectionRadius = observerData_torus->crossSectionRadius();

            //            if (axlPoint::distance(&crossSectionPoint, &crossSectionPointStart) > 0.001) {
            if (caller == crossSectionRadiusPointWidget){
                //We are moving the cross section radius point
                axlPoint center = *(observerData_torus->centerPoint());
                axlPoint orientationY(observerDataAssembly->getActor()->GetMatrix()->GetElement(0,1), observerDataAssembly->getActor()->GetMatrix()->GetElement(1,1), observerDataAssembly->getActor()->GetMatrix()->GetElement(2,1));
                orientationY.normalize();
                axlPoint crossSectionPoint(crossSectionRadiusPointWidget->GetPosition()[0], crossSectionRadiusPointWidget->GetPosition()[1], crossSectionRadiusPointWidget->GetPosition()[2]);
                double ringRadius = observerData_torus->ringRadius();

                axlPoint v(orientationY);
                axlPoint AB(crossSectionPoint - center);
                double normABH = (v.x() * AB.x() + v.y() * AB.y() + v.z() * AB.z());
                axlPoint ABH = v*normABH;
                axlPoint BH = center+ABH;
                double newCrossSectionRadius = ringRadius - axlPoint::distance(&BH, &center);
                if (newCrossSectionRadius > 0) {
                    observerData_torus->setCrossSectionRadius(newCrossSectionRadius);
                }
            }//else if (axlPoint::distance(&ringPoint, &ringPointStart) > 0.001) {
            else if (caller == ringRadiusPointWidget){
                //We are moving the ring radius point
                axlPoint center = *(observerData_torus->centerPoint());
                axlPoint orientationX(observerDataAssembly->getActor()->GetMatrix()->GetElement(0,0), observerDataAssembly->getActor()->GetMatrix()->GetElement(1,0), observerDataAssembly->getActor()->GetMatrix()->GetElement(2,0));
                orientationX.normalize();
                axlPoint ringPoint(ringRadiusPointWidget->GetPosition()[0], ringRadiusPointWidget->GetPosition()[1], ringRadiusPointWidget->GetPosition()[2]);

                axlPoint u(orientationX);
                axlPoint AB(ringPoint - center);
                double normABH = (u.x() * AB.x() + u.y() * AB.y() + u.z() * AB.z());
                axlPoint ABH = u*normABH;
                axlPoint BH = center + ABH;
                double newRingRadius = axlPoint::distance(&BH, &center) - observerData_torus->crossSectionRadius();
                observerData_torus->setRingRadius(newRingRadius);
            }
            else if (caller == directionLineWidget){
                axlPoint directionPoint1(directionLineWidget->GetPoint1()[0], directionLineWidget->GetPoint1()[1], directionLineWidget->GetPoint1()[2]);
                axlPoint directionPoint2(directionLineWidget->GetPoint2()[0], directionLineWidget->GetPoint2()[1], directionLineWidget->GetPoint2()[2]);

                observerData_torus->setDirection(directionPoint1.coordinates());
                observerData_torus->setCenter(directionPoint2.coordinates());
            }
            else
                return;

            observerData_torus->touchGeometry();
            observerDataAssembly->onUpdateGeometry();
        }

//        if(event == vtkCommand::MouseMoveEvent) {
//            //qDebug() << "vtkCommand::MouseMoveEvent";
//            if (observerData_torus) {
//                vtkAssemblyPath *path;
//                vtkRenderWindow *renderWindow = interactor->GetRenderWindow();
//                vtkRendererCollection * rendererCollection = renderWindow->GetRenderers();
//                vtkRenderer *render = rendererCollection->GetFirstRenderer();
//                axlInteractorStyle = dynamic_cast<axlInteractorStyleSwitch *>(interactor->GetInteractorStyle());

//                int X = observerDataAssembly->getInteractor()->GetEventPosition()[0];
//                int Y = observerDataAssembly->getInteractor()->GetEventPosition()[1];

//                if (!render || !render->IsInViewport(X, Y)) {
//                    //qDebug()<<" No renderer or bad cursor coordonates";
//                }

//                axlTorusPicker->Pick(X,Y,0.0,render);
//                path = axlTorusPicker->GetPath();

//                if (path != NULL) {
//                    double *position = observerDataAssembly->GetPosition();
//                    (void)position;

//                    for(int j=0;j<3;j++) {
//                        //qDebug()<<"axlActorCylinderObserver :: Execute "<<position[j];
//                        //observerData_line->coordinates()[j] = position[j];
//                    }

//                }
//            }
//        }
    }
    axlInteractorStyleSwitch *axlInteractorStyle;
//    vtkCellPicker *axlTorusPicker;
    axlTorus *observerData_torus;
    axlActorTorus *observerDataAssembly;
    vtkParametricFunctionSource *observerData_torusSource;
    vtkParametricTorus *vtkTorus;

    vtkPointWidget *ringRadiusPointWidget = nullptr;
    vtkPointWidget *crossSectionRadiusPointWidget = nullptr;
    vtkLineWidget *directionLineWidget = nullptr;

};

//// /////////////////////////////////////////////////////////////////
//// axlActorTorusPrivate
//// /////////////////////////////////////////////////////////////////

class axlActorTorusPrivate
{
public:
    axlTorus *torus;
    vtkParametricFunctionSource *torusSource;
    vtkPointWidget *ringRadiusPointWidget;
    vtkPointWidget *crosSectionRadiusPointWidget;
    vtkLineWidget *directionLineWidget;
    axlActorTorusObserver *torusObserver;
    QVTKOpenGLWidget *widget;
//    vtkCellPicker *axlTorusPicker;
    vtkParametricTorus* vtkTorus;
};

#if (VTK_MAJOR_VERSION <= 5)
vtkCxxRevisionMacro(axlActorTorus, "$Revision: 0.0.1 $")
#endif

vtkStandardNewMacro(axlActorTorus)

dtkAbstractData *axlActorTorus::data(void) {
    return d->torus;
}

vtkParametricFunctionSource *axlActorTorus::torus(void) {
    return d->torusSource;
}

void axlActorTorus::setQVTKWidget(QVTKOpenGLWidget *widget) {
    d->widget = widget;
}

void axlActorTorus::setData(dtkAbstractData *torus1) {

    axlTorus *torus = dynamic_cast<axlTorus *>(torus1);
    if(torus) {
        d->torus = torus;
        this->setMapper(vtkSmartPointer<vtkPolyDataMapper>::New());
        this->setActor(vtkSmartPointer<vtkActor>::New());

        d->torusSource = vtkParametricFunctionSource::New();

        d->vtkTorus = vtkParametricTorus::New();
        d->vtkTorus->SetRingRadius(d->torus->ringRadius());
        d->vtkTorus->SetCrossSectionRadius(d->torus->crossSectionRadius());
        d->torusSource->SetParametricFunction(d->vtkTorus);

        this->onUpdateGeometry();
#if (VTK_MAJOR_VERSION <= 5)
        this->getMapper()->SetInput(d->torusSource->GetOutput());
#else
        this->getMapper()->SetInputData(d->torusSource->GetOutput());
#endif
        this->getActor()->SetMapper(this->getMapper());

        this->AddPart(this->getActor());

        if(!d->torusObserver) {
//            d->axlTorusPicker = vtkCellPicker::New();
            d->torusObserver = axlActorTorusObserver::New() ;
            this->getInteractor()->AddObserver(vtkCommand::InteractionEvent, d->torusObserver);
            this->getInteractor()->AddObserver(vtkCommand::MouseMoveEvent, d->torusObserver);

            d->torusObserver->observerDataAssembly = this;
//            d->torusObserver->axlTorusPicker = d->axlTorusPicker;
            d->torusObserver->observerData_torusSource = d->torusSource;
            d->torusObserver->observerData_torus = d->torus;
            d->torusObserver->vtkTorus = d->vtkTorus;
            d->torusObserver->axlInteractorStyle = dynamic_cast<axlInteractorStyleSwitch *> (this->getInteractor()->GetInteractorStyle());
        }

        QColor color = d->torus->color();
        this->getActor()->GetProperty()->SetColor(color.redF(), color.greenF(), color.blueF());

        QString shader = d->torus->shader();
        if(!shader.isEmpty())
            this->setShader(shader);

        this->setOpacity(d->torus->opacity());
        connect(d->torus, SIGNAL(modifiedGeometry()),this, SLOT(onUpdateGeometry()));
        connect(d->torus, SIGNAL(modifiedProperty()),this, SLOT(onUpdateProperty()));
    }
    else
        qDebug()<< "no axlTorus available";
}


void axlActorTorus::setDisplay(bool display) {
    axlActor::setDisplay(display);

    if (display && d->directionLineWidget) {
        this->showTorusWidget(true);
    }

    if (!display && d->directionLineWidget) {
        this->showTorusWidget(false);
    }
}

void axlActorTorus::showTorusWidget(bool show) {
    if (!d->ringRadiusPointWidget || !d->crosSectionRadiusPointWidget || !d->directionLineWidget) {
        qDebug() << "No actor computed for this axlActorTorus.";
        return;
    }

    if (show) {
        d->ringRadiusPointWidget->SetEnabled(1);
        d->crosSectionRadiusPointWidget->SetEnabled(1);
        d->directionLineWidget->SetEnabled(1);
    }

    if (!show) {
        d->ringRadiusPointWidget->SetEnabled(0);
        d->crosSectionRadiusPointWidget->SetEnabled(0);
        d->directionLineWidget->SetEnabled(0);

    }
}

void axlActorTorus::setTorusWidget(bool torusWidget) {

    if(torusWidget) {
        if(!d->ringRadiusPointWidget || !d->crosSectionRadiusPointWidget || !d->directionLineWidget) {
            //widget drawing

            axlPoint center = *(d->torus->centerPoint());

            //Ring Radius pointWidget
            d->ringRadiusPointWidget = vtkPointWidget::New();
            d->ringRadiusPointWidget->SetInteractor(this->getInteractor());
            d->ringRadiusPointWidget->SetProp3D(this->getActor());
            d->ringRadiusPointWidget->PlaceWidget();
            d->ringRadiusPointWidget->AllOff();
            d->ringRadiusPointWidget->SetTranslationMode(1);

//            axlPoint orientationX((*this->getActor()->GetMatrix())[0][0], (*this->getActor()->GetMatrix())[1][0], (*this->getActor()->GetMatrix())[2][0]);
            axlPoint orientationX(this->getActor()->GetMatrix()->GetElement(0,0), this->getActor()->GetMatrix()->GetElement(1,0), this->getActor()->GetMatrix()->GetElement(2,0));
            orientationX.normalize();
            d->ringRadiusPointWidget->SetPosition((center + (orientationX * (d->torus->ringRadius() + d->torus->crossSectionRadius()))).coordinates());

            //Ring Cross Section pointWidget
            d->crosSectionRadiusPointWidget = vtkPointWidget::New();
            d->crosSectionRadiusPointWidget->SetInteractor(this->getInteractor());
            d->crosSectionRadiusPointWidget->SetProp3D(this->getActor());
            d->crosSectionRadiusPointWidget->PlaceWidget();
            d->crosSectionRadiusPointWidget->AllOff();
            d->crosSectionRadiusPointWidget->SetTranslationMode(1);

//            axlPoint orientationY((*this->getActor()->GetMatrix())[0][1], (*this->getActor()->GetMatrix())[1][1], (*this->getActor()->GetMatrix())[2][1]);
            axlPoint orientationY(this->getActor()->GetMatrix()->GetElement(0,1), this->getActor()->GetMatrix()->GetElement(1,1), this->getActor()->GetMatrix()->GetElement(2,1));
            orientationY.normalize();
            d->crosSectionRadiusPointWidget->SetPosition((center + (orientationY * (d->torus->ringRadius() - d->torus->crossSectionRadius()))).coordinates());

            //Direction lineWidget
            d->directionLineWidget = vtkLineWidget::New();
            d->directionLineWidget->SetInteractor(this->getInteractor());
            d->directionLineWidget->SetProp3D(this->getActor());
            d->directionLineWidget->PlaceWidget();

            d->directionLineWidget->SetPoint1(d->torus->direction()->coordinates());
            d->directionLineWidget->SetPoint2(center.coordinates());
            //d->directionLineWidget->SetPoint2((center - (orientationZ * d->torus->ringRadius())).coordinates());
        }

        if(d->torusObserver) {
            d->ringRadiusPointWidget->AddObserver(vtkCommand::InteractionEvent, d->torusObserver);
            d->crosSectionRadiusPointWidget->AddObserver(vtkCommand::InteractionEvent, d->torusObserver);
            d->directionLineWidget->AddObserver(vtkCommand::InteractionEvent, d->torusObserver);

            d->torusObserver->ringRadiusPointWidget = d->ringRadiusPointWidget;
            d->torusObserver->crossSectionRadiusPointWidget = d->crosSectionRadiusPointWidget;
            d->torusObserver->directionLineWidget = d->directionLineWidget;
        }

        // there is always the controlPoints there
        d->ringRadiusPointWidget->SetEnabled(true);
        d->crosSectionRadiusPointWidget->SetEnabled(true);
        d->directionLineWidget->SetEnabled(true);

    }

    if (!torusWidget) {
        if (this->getActor()) {
            // this->getMapper()->SetInput(this->getPolyData());

            if(d->ringRadiusPointWidget) {
                d->ringRadiusPointWidget->RemoveAllObservers();
                d->ringRadiusPointWidget->SetEnabled(false);
                d->ringRadiusPointWidget->Delete(); // warning not sure
                d->ringRadiusPointWidget = NULL;
            }

            if(d->crosSectionRadiusPointWidget) {
                d->crosSectionRadiusPointWidget->RemoveAllObservers();
                d->crosSectionRadiusPointWidget->SetEnabled(false);
                d->crosSectionRadiusPointWidget->Delete(); // warning not sure
                d->crosSectionRadiusPointWidget = NULL;
            }

            if(d->directionLineWidget) {
                d->directionLineWidget->RemoveAllObservers();
                d->directionLineWidget->SetEnabled(false);
                d->directionLineWidget->Delete(); // warning not sure
                d->directionLineWidget = NULL;
            }
        }
    }
    if (d->torusObserver){
        d->torusObserver->ringRadiusPointWidget = d->ringRadiusPointWidget;
        d->torusObserver->crossSectionRadiusPointWidget = d->crosSectionRadiusPointWidget;
        d->torusObserver->directionLineWidget = d->directionLineWidget;
    }
}

bool axlActorTorus::isShowTorusWidget(void) {
    if(!d->ringRadiusPointWidget || !d->crosSectionRadiusPointWidget || !d->directionLineWidget) {
        qDebug() << "No actor computed for this axlActorBSpline.";
        return false;
    }

    return d->ringRadiusPointWidget->GetEnabled() && d->crosSectionRadiusPointWidget->GetEnabled() && d->directionLineWidget->GetEnabled();
}


void axlActorTorus::setMode(int state) {
    this->onModeChanged(state);
    emit stateChanged(this->data(), state);
}

void axlActorTorus::onModeChanged(int state) {
    if(state == 0) {
        this->setState(axlActor::passive);
        this->getActor()->SetPickable(1);

        if(axlAbstractData *data = dynamic_cast<axlAbstractData *>(this->data())) {
            QColor color = data->color();
            this->getActor()->GetProperty()->SetColor(color.redF(), color.greenF(), color.blueF());
        }
        this->setTorusWidget(false);

    } else if(state == 1) {

        this->setState(axlActor::selection);
        this->setTorusWidget(false);
        this->getActor()->SetPickable(1);
        vtkProperty *prop = this->getActor()->GetProperty();

        if(axlAbstractData *data = dynamic_cast<axlAbstractData *>(this->data())) {
            QColor color = data->color();
            qreal *h = new qreal(0.0);
            qreal *s = new qreal(0.0);
            qreal *l = new qreal(0.0);
            color.getHslF(h, s, l);
            color.setHslF(*h, *s, *l + (1.0 - *l) / 2.0);
            prop->SetColor(color.redF(), color.greenF(), color.blueF());
            delete l;
            delete s;
            delete h;
        }

    } else if(state == 2) {
        this->setState(axlActor::edition);
        this->setTorusWidget(true);
        this->getActor()->SetPickable(0);
        vtkProperty *prop = this->getActor()->GetProperty();
        if(axlAbstractData *data = dynamic_cast<axlAbstractData *>(this->data())) {
            QColor color = data->color();
            qreal *h = new qreal(0.0);
            qreal *s = new qreal(0.0);
            qreal *l = new qreal(0.0);
            color.getHslF(h, s, l);
            color.setHslF(*h, *s, *l + (1.0 - *l)/2.0);
            prop->SetColor(color.redF(), color.greenF(), color.blueF());
            delete l;
            delete s;
            delete h;
        }
    }

    this->Modified();
}


void axlActorTorus::onRemoved() 
{
    
    // if on edit mode, change to selection mode (for stability)
    if (this->getState() == 2)
        setMode(1);
    
    //remove line specificity
    if(d->torusObserver) {
        this->getInteractor()->RemoveObservers(vtkCommand::InteractionEvent, d->torusObserver);
        this->getInteractor()->RemoveObservers(vtkCommand::MouseMoveEvent, d->torusObserver);
        d->torusObserver->observerDataAssembly = NULL;
//        d->torusObserver->axlTorusPicker = NULL;
        d->torusObserver->observerData_torusSource = NULL;
        d->torusObserver->observerData_torus = NULL;
        d->torusObserver->axlInteractorStyle = NULL;
        d->torusObserver->Delete();
        d->torusObserver = NULL;

//        d->axlTorusPicker->Delete();
//        d->axlTorusPicker = NULL;

    }
    if(d->torusSource) {
        d->torusSource->Delete();
        d->torusSource = NULL;
    }
    if(d->widget) {
        d->widget = NULL;
    }
    if(d->torus) {
        d->torus = NULL;
    }
    if(d->vtkTorus) {
        d->vtkTorus->Delete();
        d->vtkTorus = NULL;
    }
    if(d->ringRadiusPointWidget && d->crosSectionRadiusPointWidget && d->directionLineWidget) {
        this->setTorusWidget(false);
        d->ringRadiusPointWidget = NULL;
        d->crosSectionRadiusPointWidget = NULL;
        d->directionLineWidget = NULL;
    }

    //remove actor specificity
    this->RemoveAllObservers();
    this->RemovePart(this->getActor());
    this->getActor()->RemoveAllObservers();

    if(axlActorComposite *actorComposite = dynamic_cast<axlActorComposite *>(this->parent()) )
        actorComposite->removeActorReference(this);
}

void axlActorTorus::onUpdateGeometry() {
    d->vtkTorus->SetCrossSectionRadius(d->torus->crossSectionRadius());
    d->vtkTorus->SetRingRadius(d->torus->ringRadius());
    d->torusSource->Update();
    d->torusSource->Modified();

    axlPoint* center = d->torus->centerPoint();
    axlPoint* secondPoint = d->torus->direction();
    axlPoint* direction = new axlPoint((*(secondPoint)) - (*(center)));

    //Set angle
    direction->normalize();

    // Produit vectoriel entre la direction et l'axe du tore, Oz par défaut dans VTK
    axlPoint n(direction->y(), -direction->x(), 0.0); //n = d vectoriel Oz
    //if (!(fabs(n.x()) < 0.0001 && fabs(n.y()) < 0.0001)) {// vectorial product is not null
    n.normalize();
    //Rotation
    this->getActor()->SetOrientation(0.0, 0.0, 0.0);
    this->getActor()->RotateWXYZ(-acos(direction->z()) * 180 / 3.14159, n.x(), n.y(), 0.0);
    //    } else {

    //    }

    //Set center
    this->getActor()->SetPosition(center->coordinates());

    if(d->ringRadiusPointWidget && d->crosSectionRadiusPointWidget) {
//        axlPoint orientationX((*this->getActor()->GetMatrix())[0][0], (*this->getActor()->GetMatrix())[1][0], (*this->getActor()->GetMatrix())[2][0]);
        axlPoint orientationX(this->getActor()->GetMatrix()->GetElement(0,0), this->getActor()->GetMatrix()->GetElement(1,0), this->getActor()->GetMatrix()->GetElement(2,0));
//        axlPoint orientationY((*this->getActor()->GetMatrix())[0][1], (*this->getActor()->GetMatrix())[1][1], (*this->getActor()->GetMatrix())[2][1]);
        axlPoint orientationY(this->getActor()->GetMatrix()->GetElement(0,1), this->getActor()->GetMatrix()->GetElement(1,1), this->getActor()->GetMatrix()->GetElement(2,1));

        d->ringRadiusPointWidget->SetPosition((*center + (orientationX * (d->torus->ringRadius() + d->torus->crossSectionRadius()))).coordinates());
        d->crosSectionRadiusPointWidget->SetPosition((*center + (orientationY * (d->torus->ringRadius() - d->torus->crossSectionRadius()))).coordinates());

        d->directionLineWidget->SetPoint1(secondPoint->x(), secondPoint->y(), secondPoint->z());
        d->directionLineWidget->SetPoint2(center->x(), center->y(), center->z());
    }


    if(!d->torus->fields().isEmpty())
        d->torus->touchField();

    if(d->torus->updateView())
        emit updated();
}

axlActorTorus::axlActorTorus(void) : axlActor(), d(new axlActorTorusPrivate) {
    d->torus = NULL;
    d->ringRadiusPointWidget = NULL;
    d->crosSectionRadiusPointWidget = NULL;
    d->directionLineWidget = NULL;
//    d->axlTorusPicker = NULL;
    d->torusObserver = NULL;
    d->torusSource =NULL;
    d->widget = NULL;
    d->vtkTorus = NULL;
}

axlActorTorus::~axlActorTorus(void) {
    delete d;
    d = NULL;
}

axlAbstractActor *createAxlActorTorus(void){

    return axlActorTorus::New();
}
