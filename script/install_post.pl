#!/usr/bin/perl

sub replace {
  my ($file,$x,$r) = @_;

  open (IN, $file) || die "could not open ".$file." for read";
  @lines=<IN>;
  close IN;

  open (OUT,">", $file) || die "can not open file ".$file." for write";

  foreach $line (@lines)
    {
      $line =~ s/$x/$r/g;
      print OUT $line;
    }
  close (OUT);
}

@files = ("cmake/AxlConfig.cmake",
	  "bin/mmk"
	 );

foreach $file (@files) {
  replace ($file, "\@HomeDir\@", $ENV{"HOME"});
}
