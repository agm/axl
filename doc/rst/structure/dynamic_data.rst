==================== 
Dynamic data in Axl
====================

*Anais Ducoffe*

What is a dynamic object?
=========================

A dynamic object is composed of a list of inputs and a process. These two elements enable to compute its output(s).
Dynamic object owns the process and the outputs. The inputs and the dynamic object are separated.
For instance a dynamic line will be composed of two inputs, two points in axel, a process axlLineCreator, a creative process of line. Its output will be a line.

The main interest of dynamic data is to enable to re-compute its output(s) each time one of its inputs is modified. That's why it is very useful for dynamic geometry.
If we keep our first example of the dynamic line, if one of the input points is moved, then the line is re-computed.
This mechanism uses qt signals. When an input is modified a signal ``modifiedGeometry()`` is send to the dynamic object. This signal in the latter is connected to a slot ``update()`` which recompute outputs with the new values of inputs. Check that the methods which modify your inputs are emitted this signal.
Each time an output is re-computed, it emits the signal ``modifiedGeometry()`` too. That way an output of a dynamic object could be an input of another dynamic object and so on.



How to create dynamic data in Axl?
===================================

This part presents you some methods of ``axlDataDynamic`` you may need to use to create an ``axlDataDynamic`` object.

Create a new dynamic object::

     axlDataDynamic *object = new axlDataDynamic();


**PROCESS:**

Create the dynamic object process

- with a process name::

    object->setProcess("process_name")
    
- copying another process state::

    object->setProcess(axlAbstractProcess *process)



**INPUTS:**

- to add inputs to the list::

    object->setInput(axlAbstractData *data);

- to obtain the current list of inputs::

    object->inputs();

.. note::
   
   If the process you chose needs parameters, these values are entered by using setInput and converting double value (respectively integer) in ``axlDouble`` (resp. ``axlInteger``), two classes which inherits from ``axlAbstractData``.

.. warning::

   Be CAREFUL that you must select inputs in the rigth order. For instance, the inputs necessary to create a dynamic Torus are described as follow : "create a torus with two points: its center and  another one to determine its normal, and two values: the ``ringRadius`` and the ``crossSectionRadius`` (``axlTorusCreator`` decsription method).
   In this case you must add the center point first with setInput and then you use setInput again to add the other extremal point to determine the normal. And then you will setInput an ``axlDouble`` with  the ring radius value and another one with the ``crossSection`` value.


**OUTPUTS:**

to obtain i-th element of output list::

    object->outputs(int i)

to obtain the first output in the list (or the only one)::

    object->outputs()

if you want to know how many outputs were computed::

    int numberOutputs = object->numberOfChannels();


**UPDATE:**

To compute your output(s), you can use the ``update()`` slot.



How dynamic data deletion works ?
---------------------------------
It works similarly to the update system.
Each dynamic object is connected to the signal ``destroy()`` of their inputs. This signal is emitted when the object is deleted. It is connected to the ``onRemoved()`` slot in ``axlDataDynamic`` in which the dynamic data emit the signal ``destroyed()`` and is deleted.

.. warning::
   
   Be CAREFUL as, when you delete an object, all dynamic data which depend on it will be automatically destroyed.

   
The dynamic object is in charge of the destruction of its outputs and its process. It doesn't delete its inputs.



Some conventions you need to follow!
------------------------------------
- when you insert a dynamic data in the view, check that all its inputs are inserted before. It is necessary to respect this convention for deletion and to save your dynamic data.
- if you want to use a dynamic object as an input of another one, the input you enter in setInput must be one of the outputs of the first object and not the dynamic object itself.
