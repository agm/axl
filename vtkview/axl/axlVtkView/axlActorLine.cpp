/* axlActorLine.cpp ---
 *
 * Author: Meriadeg Perrinel
 * Copyright (C) 2008 - Meriadeg Perrinel, Inria.
 * Created: Fri Dec 17 10:59:21 2010 (+0100)
 * Version: $Id$
 * Last-Updated: Mon Dec 17 15:01:34 2012 (+0100)
 *           By: Julien Wintz
 *     Update #: 35
 */

/* Commentary:
 *
 */

/* Change log:
 *
 */
#include "axlActorLine.h"

#include "axlActorComposite.h"
#include "axlInteractorStyleSwitch.h"

#include <axlCore/axlLine.h>
#include <axlCore/axlPoint.h>
#include <axlCore/axlDataDynamic.h>

#include <QVTKOpenGLWidget.h>

#include <vtkActor.h>
#include <vtkAssemblyPath.h>
//#include <vtkCellPicker.h>
#include <vtkCommand.h>
#include <vtkGlyph3D.h>
#include <vtkInteractorStyleSwitch.h>
#include <vtkLight.h>
#include <vtkLightCollection.h>
#include <vtkLineSource.h>
#include <vtkLineWidget.h>
#include <vtkObjectFactory.h>
#include <vtkPoints.h>
#include <vtkPolyData.h>
#include <vtkPolyDataMapper.h>
#include <vtkProperty.h>
#include <vtkRenderer.h>
#include <vtkRendererCollection.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkSmartPointer.h>
#include <vtkTransform.h>
#include <vtkTubeFilter.h>


class axlActorLineObserver : public vtkCommand
{
public:
    axlInteractorStyleSwitch *axlInteractorStyle;

//    vtkCellPicker *axlLinePicker;

    axlActorLine *observerDataAssembly;
    vtkLineSource *observerData_lineSource;

    vtkLineWidget *lineWidget = nullptr;
    axlLine *observerData_line;

public:
    static axlActorLineObserver *New(void)
    {
        return new axlActorLineObserver;
    }

    virtual void Execute(vtkObject *caller, unsigned long event, void *)
    {
        // vtkRenderWindowInteractor *interactor = observerDataAssembly->getInteractor();
        // interactor->Render();

        if(event == vtkCommand::InteractionEvent) {

            observerData_lineSource->Update();
            observerData_lineSource->Modified();

            if (!lineWidget)
                return;

            if(caller == lineWidget) {
                observerData_line->setFirstPoint(lineWidget->GetPoint1());
                observerData_line->setSecondPoint(lineWidget->GetPoint2());
                observerData_line->touchGeometry();
                observerDataAssembly->onUpdateGeometry();
            }
        }

//        if(event == vtkCommand::MouseMoveEvent)
//        {

//            if(observerData_line)
//            {
//                // we need the matrix transform of this actor

//                vtkAssemblyPath *path;
//                vtkRenderWindow *renderWindow = interactor->GetRenderWindow();
//                vtkRendererCollection * rendererCollection = renderWindow->GetRenderers();
//                vtkRenderer *render = rendererCollection->GetFirstRenderer();
//                axlInteractorStyle = dynamic_cast<axlInteractorStyleSwitch *>(interactor->GetInteractorStyle());

//                int X = observerDataAssembly->getInteractor()->GetEventPosition()[0];
//                int Y = observerDataAssembly->getInteractor()->GetEventPosition()[1];

//                if (!render || !render->IsInViewport(X, Y))
//                {
//                    //qDebug()<<" No renderer or bad cursor coordonates";
//                }

//                axlLinePicker->Pick(X,Y,0.0,render);
//                path = axlLinePicker->GetPath();

//                if ( path != NULL)
//                {
//                    double *position = observerDataAssembly->GetPosition();

//                    for(int j=0;j<3;j++)
//                    {
//                        //qDebug()<<"axlActorLineObserver :: Execute "<<position[j];
//                        //observerData_line->coordinates()[j] = position[j];
//                    }

//                }

//            }
//        }
    }

};

// /////////////////////////////////////////////////////////////////
// axlActorLinePrivate
// /////////////////////////////////////////////////////////////////

class axlActorLinePrivate
{
public:
    axlLine *line;
    vtkLineSource *lineSource;
    vtkLineWidget *lineWidget;
    axlActorLineObserver *lineObserver;
    QVTKOpenGLWidget *widget;
//    vtkCellPicker *axlLinePicker ;

    vtkSmartPointer<vtkTubeFilter> lineTubeFilter;
};

#if (VTK_MAJOR_VERSION <= 5)
vtkCxxRevisionMacro(axlActorLine, "$Revision: 0.0.1 $");
#endif

vtkStandardNewMacro(axlActorLine);

dtkAbstractData *axlActorLine::data(void)
{
    return d->line;
}

vtkLineSource *axlActorLine::line(void)
{
    return d->lineSource;
}

void axlActorLine::setQVTKWidget(QVTKOpenGLWidget *widget)
{
    d->widget = widget;
}

void axlActorLine::setData(dtkAbstractData *line1)
{
    axlLine *line = dynamic_cast<axlLine *>(line1);
    if(line) {
        d->line = line;
        this->setMapper(vtkSmartPointer<vtkPolyDataMapper>::New());
        this->setActor(vtkSmartPointer<vtkActor>::New());

        d->lineSource = vtkLineSource::New();
        //        d->lineSource->SetPoint1(d->line->firstPoint()->coordinates());
        //        d->lineSource->SetPoint2(d->line->secondPoint()->coordinates());

        d->lineSource->SetResolution(200);
        //this->SetPosition(d->line->coordinates());

        // connection of data to actor
        //this->getMapper()->SetInput(d->lineSource->GetOutput());

        this->getActor()->SetMapper(this->getMapper());

        this->AddPart(this->getActor());

        if (!d->lineObserver) {
//            d->axlLinePicker = vtkCellPicker::New();
            d->lineObserver = axlActorLineObserver::New() ;
            this->getInteractor()->AddObserver(vtkCommand::InteractionEvent, d->lineObserver);
            this->getInteractor()->AddObserver(vtkCommand::MouseMoveEvent, d->lineObserver);

            d->lineObserver->observerDataAssembly = this;
//            d->lineObserver->axlLinePicker = d->axlLinePicker;
            d->lineObserver->observerData_lineSource = d->lineSource;
            d->lineObserver->observerData_line = d->line;
            d->lineObserver->axlInteractorStyle = dynamic_cast<axlInteractorStyleSwitch *> (this->getInteractor()->GetInteractorStyle());
        }

        QColor color = d->line->color();
        this->getActor()->GetProperty()->SetColor(color.redF(), color.greenF(), color.blueF());

        QString shader = d->line->shader();
        if (!shader.isEmpty())
            this->setShader(shader);

        //this->getActor()->SetScale(d->line->size());

        this->setOpacity(d->line->opacity());

        // refresh the actor
        this->onUpdateGeometry();

        // signals connecting
        connect(d->line, SIGNAL(modifiedGeometry()), this, SLOT(onUpdateGeometry()));
        connect(d->line, SIGNAL(modifiedProperty()), this, SLOT(onUpdateProperty()));
    }
    else
        qDebug()<< "no axlLine available";
}

void axlActorLine::setDisplay(bool display)
{
    axlActor::setDisplay(display);

    if(display && d->lineWidget){
        this->showLineWidget(true);
    }

    if(!display && d->lineWidget){
        this->showLineWidget(false);
    }
}

void axlActorLine::showLineWidget(bool show)
{
    if(!d->lineWidget) {
        qDebug() << "No tet actor computed for this axlActorLine.";
        return;
    }

    if(show){
        d->lineWidget->SetEnabled(1);
    }

    if(!show){
        d->lineWidget->SetEnabled(0);
    }
}

void axlActorLine::setLineWidget(bool lineWidget)
{
    if (lineWidget) {
        if (!d->lineWidget) {
            //widget drawing

            d->lineWidget = vtkLineWidget::New();
            d->lineWidget->SetInteractor(this->getInteractor());
            d->lineWidget->SetProp3D(this->getActor());
            d->lineWidget->PlaceWidget();
            d->lineWidget->SetPoint1(d->line->firstPoint()->coordinates());
            d->lineWidget->SetPoint2(d->line->secondPoint()->coordinates());
        }

        if(d->lineObserver)
        {
            d->lineWidget->AddObserver(vtkCommand::InteractionEvent, d->lineObserver);
            d->lineObserver->lineWidget = d->lineWidget;
        }

        // there is always the controlPoints there
        d->lineWidget->SetEnabled(true);

    } else { // if (!lineWidget)
        if (this->getActor()) {
            // this->getMapper()->SetInput(this->getPolyData());

            if(d->lineWidget)
            {
                d->lineWidget->RemoveAllObservers();
                d->lineWidget->SetEnabled(false);
                d->lineWidget->Delete(); // warning not sure
                d->lineWidget = NULL;
            }
        }
    }
    if (d->lineObserver)
        d->lineObserver->lineWidget = d->lineWidget;
}

bool axlActorLine::isShowLineWidget(void)
{

    if(!d->lineWidget) {
        qDebug() << "No tet actor computed for this axlActorBSpline.";
        return false;
    }

    return d->lineWidget->GetEnabled();
}


void axlActorLine::setMode(int state)
{
    this->onModeChanged(state);

    emit stateChanged(this->data(), state);
}

void axlActorLine::onModeChanged(int state)
{
    if(state == 0)
    {
        this->setState(axlActor::passive);
        this->getActor()->SetPickable(1);

        if(axlAbstractData *data = dynamic_cast<axlAbstractData *>(this->data()))
        {
            QColor color = data->color();
            this->getActor()->GetProperty()->SetColor(color.redF(), color.greenF(), color.blueF());
        }
        this->setLineWidget(false);

    }
    else if(state == 1)
    {

        this->setState(axlActor::selection);
        this->setLineWidget(false);
        this->getActor()->SetPickable(1);
        vtkProperty *prop = this->getActor()->GetProperty();

        if(axlAbstractData *data = dynamic_cast<axlAbstractData *>(this->data()))
        {
            QColor color = data->color();
            qreal *h = new qreal(0.0);
            qreal *s = new qreal(0.0);
            qreal *l = new qreal(0.0);
            color.getHslF(h, s, l);
            color.setHslF(*h, *s, *l + (1.0 - *l) / 2.0);
            prop->SetColor(color.redF(), color.greenF(), color.blueF());
            delete l;
            delete s;
            delete h;
        }

    }
    else if(state == 2)
    {
        this->setState(axlActor::edition);
        this->setLineWidget(true);
        this->getActor()->SetPickable(0);
        vtkProperty *prop = this->getActor()->GetProperty();
        if(axlAbstractData *data = dynamic_cast<axlAbstractData *>(this->data()))
        {
            QColor color = data->color();
            qreal *h = new qreal(0.0);
            qreal *s = new qreal(0.0);
            qreal *l = new qreal(0.0);
            color.getHslF(h, s, l);
            color.setHslF(*h, *s, *l + (1.0 - *l)/2.0);
            prop->SetColor(color.redF(), color.greenF(), color.blueF());
            delete l;
            delete s;
            delete h;
        }
    }

    this->Modified();
}


void axlActorLine::onRemoved()
{
    // if on edit mode, change to selection mode (for stability)
    if (this->getState() == 2)
        setMode(1);
    
    //remove line specificity
    if(d->lineObserver) {
        this->getInteractor()->RemoveObservers(vtkCommand::InteractionEvent, d->lineObserver);
        this->getInteractor()->RemoveObservers(vtkCommand::MouseMoveEvent, d->lineObserver);
        d->lineObserver->observerDataAssembly = NULL;
//        d->lineObserver->axlLinePicker = NULL;
        d->lineObserver->observerData_lineSource = NULL;
        d->lineObserver->observerData_line = NULL;
        d->lineObserver->axlInteractorStyle = NULL;
        d->lineObserver->Delete();
        d->lineObserver = NULL;

//        d->axlLinePicker->Delete();
//        d->axlLinePicker = NULL;
    }
    if(d->lineSource) {
        d->lineSource->Delete();
        d->lineSource = NULL;
    }

    if(d->widget) {
        d->widget = NULL;
    }
    if(d->line) {
        d->line = NULL;
    }
    if(d->lineWidget) {
        this->setLineWidget(false);
        d->lineWidget = NULL;
    }

    //remove actor specificity
    this->RemoveAllObservers();
    this->RemovePart(this->getActor());
    this->getActor()->RemoveAllObservers();

    if(axlActorComposite *actorComposite = dynamic_cast<axlActorComposite *>(this->parent()) )
        actorComposite->removeActorReference(this);
}

void axlActorLine::onUpdateGeometry()
{
    d->lineSource->SetPoint1(d->line->firstPoint()->coordinates());
    d->lineSource->SetPoint2(d->line->secondPoint()->coordinates());

    d->lineSource->Update();
    d->lineSource->Modified();

    if (d->lineWidget) {
         d->lineWidget->SetPoint1(d->line->firstPoint()->coordinates());
         d->lineWidget->SetPoint2(d->line->secondPoint()->coordinates());
    }

    //    double bounds[6]; this->getActor()->GetBounds(bounds);
    //    double side = qAbs(bounds[1]-bounds[0]);
    //    side += qAbs(bounds[3]-bounds[2]);
    //    side += qAbs(bounds[5]-bounds[4]);
    //    side /= 100;


    // add the tube filter to the line
#if (VTK_MAJOR_VERSION <= 5)
    if(d->line->size() < 0.001) {
        this->getMapper()->SetInput(d->lineSource->GetOutput());
    } else {
        d->lineTubeFilter->SetInput(d->lineSource->GetOutput());
        this->getMapper()->SetInput(d->lineTubeFilter->GetOutput());
        d->lineTubeFilter->SetRadius(d->line->size());
        d->lineTubeFilter->Update();
    }
#else
    if(d->line->size() < 0.001) {
        this->getMapper()->SetInputData(d->lineSource->GetOutput());
    } else {
        d->lineTubeFilter->SetInputData(d->lineSource->GetOutput());
        this->getMapper()->SetInputData(d->lineTubeFilter->GetOutput());
        d->lineTubeFilter->SetRadius(d->line->size());
        d->lineTubeFilter->Update();
    }
#endif

    //this->SetPosition(d->line->coordinates());
    // d->line->touch();

    if(!d->line->fields().isEmpty())
        d->line->touchField();

    if(d->line->updateView())
        emit updated();
}

void axlActorLine::update(void)
{

}

void axlActorLine::setSize(double size){

    d->line->setSize(size);
#if (VTK_MAJOR_VERSION <= 5)
    if(d->line->size() < 0.001) {
        this->getMapper()->SetInput(d->lineSource->GetOutput());
    } else {
        d->lineTubeFilter->SetInput(d->lineSource->GetOutput());
        this->getMapper()->SetInput(d->lineTubeFilter->GetOutput());
        d->lineTubeFilter->SetRadius(d->line->size());
        d->lineTubeFilter->Update();
    }
#else
    if(d->line->size() < 0.001) {
        this->getMapper()->SetInputData(d->lineSource->GetOutput());
    } else {
        d->lineTubeFilter->SetInputData(d->lineSource->GetOutput());
        this->getMapper()->SetInputData(d->lineTubeFilter->GetOutput());
        d->lineTubeFilter->SetRadius(d->line->size());
        d->lineTubeFilter->Update();
    }
#endif

    //recompute fields if necessary
    if(!d->line->fields().isEmpty())
        d->line->touchField();

}

axlActorLine::axlActorLine(void) : axlActor(), d(new axlActorLinePrivate)
{

    d->lineTubeFilter = vtkSmartPointer<vtkTubeFilter>::New();
    d->lineTubeFilter->SetNumberOfSides(8);
    d->line = NULL;
    d->lineWidget = NULL;
//    d->axlLinePicker = NULL;
    d->lineObserver = NULL;
    d->lineSource =NULL;
    d->widget = NULL;
}

axlActorLine::~axlActorLine(void)
{
    delete d;

    d = NULL;
}

// /////////////////////////////////////////////////////////////////
// Type instanciation
// /////////////////////////////////////////////////////////////////

axlAbstractActor *createAxlActorLine(void)
{
    return axlActorLine::New();
}
