// /////////////////////////////////////////////////////////////////
// Generated by dtkPluginGenerator
// /////////////////////////////////////////////////////////////////

#ifndef VTKVIEWPLUGINEXPORT_H
#define VTKVIEWPLUGINEXPORT_H

#ifdef WIN32
    #ifdef vtkView_EXPORTS
        #define VTKVIEWPLUGIN_EXPORT __declspec(dllexport) 
    #else
        #define VTKVIEWPLUGIN_EXPORT __declspec(dllimport)
    #endif
#else
    #define VTKVIEWPLUGIN_EXPORT
#endif

#endif
