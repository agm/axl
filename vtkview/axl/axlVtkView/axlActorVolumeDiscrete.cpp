/* axlActorVolumeDiscrete.cpp ---
 *
 * Author: Thibaud Kloczko
 * Copyright (C) 2008 - Thibaud Kloczko, >Inria.
 * Created: Fri Feb  4 21:35:45 2011 (+0100)
 * Version: $Id$
 * Last-Updated: Wed Oct  9 19:46:14 2013 (+0200)
 *           By: Julien Wintz
 *     Update #: 383
 */

/* Commentary:
 *
 */

/* Change log:
 *
 */

#include "axlVolumeDiscrete.h"
#include "axlActorVolumeDiscrete.h"

#include <vtkAbstractVolumeMapper.h>
#include <vtkActor.h>
#include <vtkProperty.h>
#include <vtkColorTransferFunction.h>
#include <vtkConfigure.h>
#include <vtkDoubleArray.h>
#include <vtkGPUVolumeRayCastMapper.h>
#include <vtkObjectFactory.h>
#include <vtkOpenGLGPUVolumeRayCastMapper.h>
#include <vtkOutlineCornerFilter.h>
#include <vtkOutlineFilter.h>
#include <vtkImageData.h>
//#include <vtkImageResize.h>
#include <vtkPiecewiseFunction.h>
#include <vtkPointData.h>
#include <vtkPolyData.h>
#include <vtkPolyDataMapper.h>
#include <vtkProp3DCollection.h>
#include <vtkSmartPointer.h>
#include <vtkSmartVolumeMapper.h>
//#include <vtkVolumeTextureMapper3D.h>
#include <vtkVolume.h>
#include <vtkVolumeProperty.h>

#include <vtkCommand.h>

#include <vtkVersion.h>




class axlActorVolumeDiscreteObserver : public vtkCommand
{
public:
    static axlActorVolumeDiscreteObserver *New(void)
    {
        return new axlActorVolumeDiscreteObserver;
    }

    virtual void Execute(vtkObject *caller, unsigned long event, void *)
    {
//                vtkRenderWindowInteractor *interactor = observerDataAssembly->getInteractor();            //interactor->Render();


//                if(event == vtkCommand::InteractionEvent)
//                {
//                    if(lineWidget && pointWidget)
//                    {// NOTICE : We can improve this code...
//                        axlPoint p1(lineWidget->GetPoint1()[0], lineWidget->GetPoint1()[1], lineWidget->GetPoint1()[2]);
//                        axlPoint p2(lineWidget->GetPoint2()[0], lineWidget->GetPoint2()[1], lineWidget->GetPoint2()[2]);
//                        if(!(axlPoint::distance(&p1, observerData_cylinder->firstPoint())< 0.0001 && axlPoint::distance(&p2, observerData_cylinder->secondPoint())<0.0001))
//                        {// There we move the line widget
//                            observerData_cylinderSource->Update();
//                            observerData_cylinderSource->Modified();
//                            observerData_cylinder->modifyFirstPoint(lineWidget->GetPoint1());
//                            observerData_cylinder->modifySecondPoint(lineWidget->GetPoint2());
//                            observerDataAssembly->onUpdateGeometry();
//                        }
//                        else
//                        {// There we move the point widget

//                            axlPoint center = (*(observerData_cylinder->firstPoint())+(*(observerData_cylinder->secondPoint()))) / 2.0;
//                            axlPoint p3(pointWidget->GetPosition()[0], pointWidget->GetPosition()[1], pointWidget->GetPosition()[2]);
//                            axlPoint mp3 = p3 - center;
//                            axlPoint p1p2 = (*(observerData_cylinder->secondPoint())-(*(observerData_cylinder->firstPoint())));
//                            double pv = (mp3.x() * p1p2.x() + mp3.y() * p1p2.y() + mp3.z() * p1p2.z());

//                            axlPoint ps = p1p2 * pv;
//                            double norm = observerData_cylinder->length();

//                            axlPoint c = p3 - (ps /= (norm*norm));

//                            pointWidget->SetPosition(c.coordinates());
//                            double r = axlPoint::distance(&c, &center);

//                            observerData_cylinder->setRadius(0.9 * r);
//                            observerData_cylinderSource->SetRadius(0.9 * r);
//                            if(!observerData_cylinder->fields().isEmpty())
//                                observerData_cylinder->runFieldUpdated();
//                        }
//                    }
//                }

        //        if(event == vtkCommand::MouseMoveEvent)
        //        {
        //            if(observerData_cylinder)
        //            {
        //                // we need the matrix transform of this actor

        //                vtkAssemblyPath *path;
        //                vtkRenderWindow *renderWindow = interactor->GetRenderWindow();
        //                vtkRendererCollection * rendererCollection = renderWindow->GetRenderers();
        //                vtkRenderer *render = rendererCollection->GetFirstRenderer();
        //                axlInteractorStyle = dynamic_cast<axlInteractorStyleSwitch *>(interactor->GetInteractorStyle());

        //                int X = observerDataAssembly->getInteractor()->GetEventPosition()[0];
        //                int Y = observerDataAssembly->getInteractor()->GetEventPosition()[1];

        //                if (!render || !render->IsInViewport(X, Y))
        //                {
        //                    //qDebug()<<" No renderer or bad cursor coordonates";
        //                }

        //                axlCylinderPicker->Pick(X,Y,0.0,render);
        //                path = axlCylinderPicker->GetPath();

        //                if ( path != NULL)
        //                {
        //                    double *position = observerDataAssembly->GetPosition();
        //                    (void)position;

        //                    for(int j=0;j<3;j++)
        //                    {
        //                        //qDebug()<<"axlActorCylinderObserver :: Execute "<<position[j];
        //                        //observerData_line->coordinates()[j] = position[j];
        //                    }

        //                }

        //            }
        //        }
    }

    //    axlInteractorStyleSwitch *axlInteractorStyle;
    vtkCellPicker *axlVolumePicker;
    axlVolumeDiscrete *observerData_volume;
    axlActorVolumeDiscrete *observerDataAssembly;


};


// /////////////////////////////////////////////////////////////////
// axlActorVolumeDiscretePrivate
// /////////////////////////////////////////////////////////////////

class axlActorVolumeDiscretePrivate
{
public:
    axlVolumeDiscrete *volume;

public:
    vtkSmartPointer<vtkVolume> vol;
    vtkSmartPointer<vtkVolumeProperty> volProperty;
    vtkSmartPointer<vtkColorTransferFunction> colorFunction;
    vtkSmartPointer<vtkPiecewiseFunction> opacityTransferFunction;
    vtkSmartPointer<vtkSmartVolumeMapper> mapper;

public:
    vtkSmartPointer<vtkOutlineCornerFilter> outline_corner;
    vtkSmartPointer<vtkOutlineFilter> outline_box;
    vtkSmartPointer<vtkOutlineFilter> outline_contour;

    vtkSmartPointer<vtkPolyDataMapper> outline_corner_mapper;
    vtkSmartPointer<vtkPolyDataMapper> outline_box_mapper;
    vtkSmartPointer<vtkPolyDataMapper> outline_contour_mapper;

    vtkSmartPointer<vtkActor> outline_corner_actor;
    vtkSmartPointer<vtkActor> outline_box_actor;
    vtkSmartPointer<vtkActor> outline_contour_actor;

public:
    vtkImageData * image;
#if(VTK_VERSION_MINOR >= 10)
    vtkImageResize *filter;
#endif
};

// /////////////////////////////////////////////////////////////////
// axlActorVolumeDiscrete
// /////////////////////////////////////////////////////////////////
#if (VTK_MAJOR_VERSION <= 5)
vtkCxxRevisionMacro(axlActorVolumeDiscrete, "$Revision: 0.0.1 $");
#endif

vtkStandardNewMacro(axlActorVolumeDiscrete);

void axlActorVolumeDiscrete::setData(dtkAbstractData *volume)
{
    if (!dynamic_cast<axlVolumeDiscrete *>(volume)) {
        dtkWarn() << "No volume, can't set volume!";
        return;
    }

    d->volume = dynamic_cast<axlVolumeDiscrete *>(volume);
    vtkSmartVolumeMapper *mapper = d->mapper;

    d->image = static_cast<vtkImageData *>(d->volume->data());

    int spacing_x = d->image->GetSpacing()[0];
    // int spacing_y = d->image->GetSpacing()[1];
    // int spacing_z = d->image->GetSpacing()[2];

    // int dim_x = d->image->GetDimensions()[0];
    // int dim_y = d->image->GetDimensions()[1];
    // int dim_z = d->image->GetDimensions()[2];


    if(spacing_x != 1) {
#if(VTK_VERSION_MINOR >= 10)
        d->filter = vtkImageResize::New();
#if (VTK_MAJOR_VERSION <= 5)
        d->filter->SetInput(d->image);
#else
        d->filter->SetInputData(d->image);
#endif
        d->filter->SetResizeMethodToOutputDimensions();
        d->filter->SetOutputDimensions(spacing_x*dim_x, spacing_y*dim_y, spacing_z*dim_z);
        d->filter->SetOutputSpacing(1, 1, 1);
        d->filter->Update();
#if (VTK_MAJOR_VERSION <= 5)
        mapper->SetInput(d->filter->GetOutput());
#else
        mapper->SetInputData(d->filter->GetOutput());
#endif
#endif

    } else {
#if (VTK_MAJOR_VERSION <= 5)
        mapper->SetInput(d->image);
#else
        mapper->SetInputData(d->image);
#endif
    }

    double min = d->volume->minValue();
    double mid = d->volume->midValue();
    double max = d->volume->maxValue();

    d->colorFunction = vtkSmartPointer<vtkColorTransferFunction>::New();
    d->colorFunction->SetColorSpaceToRGB();
    d->colorFunction->AddRGBPoint(min, 0.0, 0.0, 1.0);
    d->colorFunction->AddRGBPoint(mid, 0.0, 1.0, 0.0);
    d->colorFunction->AddRGBPoint(max, 1.0, 0.0, 0.0);

    d->opacityTransferFunction = vtkSmartPointer<vtkPiecewiseFunction>::New();
    d->opacityTransferFunction->AddPoint(min, 0.5);
    d->opacityTransferFunction->AddPoint(mid, 0.5);
    d->opacityTransferFunction->AddPoint(max, 0.5);

    d->volProperty = vtkSmartPointer<vtkVolumeProperty>::New();
    d->volProperty->SetColor(d->colorFunction);
    d->volProperty->SetScalarOpacity(d->opacityTransferFunction);
    d->volProperty->ShadeOn();
    d->volProperty->SetSpecular(1.0);

    d->vol = vtkSmartPointer<vtkVolume>::New();
    d->vol->SetMapper(d->mapper);
    d->vol->SetProperty(d->volProperty);

    d->vol->SetVisibility(1);


    { // Building corner outline actor

        if(!d->outline_corner)
            d->outline_corner = vtkSmartPointer<vtkOutlineCornerFilter>::New();
#if (VTK_MAJOR_VERSION <= 5)
        d->outline_corner->SetInput(d->image);
#else
        d->outline_corner->SetInputData(d->image);
#endif
        d->outline_corner->Update();

        if(!d->outline_corner_mapper)
            d->outline_corner_mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
        d->outline_corner_mapper->SetInputConnection(d->outline_corner->GetOutputPort());

        if(!d->outline_corner_actor)
            d->outline_corner_actor = vtkSmartPointer<vtkActor>::New();
        d->outline_corner_actor->SetMapper(d->outline_corner_mapper);
        d->outline_corner_actor->GetProperty()->SetColor(1, 0, 0);
    }

    { // Building outline box actor

        if(!d->outline_box)
            d->outline_box = vtkSmartPointer<vtkOutlineFilter>::New();
#if (VTK_MAJOR_VERSION <= 5)
        d->outline_box->SetInput(d->image);
#else
        d->outline_box->SetInputData(d->image);
#endif
        d->outline_box->Update();

        if(!d->outline_box_mapper)
            d->outline_box_mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
        d->outline_box_mapper->SetInputConnection(d->outline_box->GetOutputPort());

        if(!d->outline_box_actor)
            d->outline_box_actor = vtkSmartPointer<vtkActor>::New();
        d->outline_box_actor->SetMapper(d->outline_box_mapper);
        d->outline_box_actor->GetProperty()->SetColor(1, 0, 0);
    }

    { // Building outline contour actor

        if(!d->outline_contour)
            d->outline_contour = vtkSmartPointer<vtkOutlineFilter>::New();
#if (VTK_MAJOR_VERSION <= 5)
        d->outline_contour->SetInput(d->image);
#else
        d->outline_contour->SetInputData(d->image);
#endif
        d->outline_contour->Update();

        if(!d->outline_contour_mapper)
            d->outline_contour_mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
        d->outline_contour_mapper->SetInputConnection(d->outline_contour->GetOutputPort());

        if(!d->outline_contour_actor)
            d->outline_contour_actor = vtkSmartPointer<vtkActor>::New();
        d->outline_contour_actor->SetMapper(d->outline_contour_mapper);
        d->outline_contour_actor->GetProperty()->SetColor(1, 0, 0);
    }

    d->outline_contour_actor->SetVisibility(1);
    d->outline_box_actor->SetVisibility(1);
    d->outline_corner_actor->SetVisibility(1);

    this->AddPart(d->outline_corner_actor);
    this->AddPart(d->outline_box_actor);
    this->AddPart(d->outline_contour_actor);

    this->setActor(vtkSmartPointer<vtkActor>::New());
    this->getActor()->SetMapper(this->getMapper());
    this->AddPart(this->getActor());

}

dtkAbstractData *axlActorVolumeDiscrete::data(void)
{
    return d->volume;
}

void axlActorVolumeDiscrete::touch(void)
{
    if (!d->volume)
        return;

#if(VTK_VERSION_MINOR >= 10)
    if(d->filter)
        d->filter->Update();
#endif

    d->vol->Modified();
    d->volProperty->Modified();
    d->colorFunction->Modified();
    d->opacityTransferFunction->Modified();
    d->mapper->Modified();
    d->mapper->Update();
}

void axlActorVolumeDiscrete::update(void)
{

}

void axlActorVolumeDiscrete::setDisplay(bool display)
{
    if(display)
        if(!this->GetParts()->IsItemPresent(d->vol))
            this->AddPart(d->vol);

    if(!display)
        if (this->GetParts()->IsItemPresent(d->vol))
            this->RemovePart(d->vol);
}


void axlActorVolumeDiscrete::setMode(int state)
{
    this->onModeChanged(state);

    emit stateChanged(this->data(), state);
}

void axlActorVolumeDiscrete::onModeChanged(int state)
{
    if(state == 0)
    {
        this->setState(axlActor::passive);
        this->getActor()->SetPickable(1);

        if(axlAbstractData *data = dynamic_cast<axlAbstractData *>(this->data()))
        {
            QColor color = data->color();
            this->getActor()->GetProperty()->SetColor(color.redF(), color.greenF(), color.blueF());
        }

    }
    else if(state == 1)
    {

        this->setState(axlActor::selection);
        this->getActor()->SetPickable(1);
        vtkProperty *prop = this->getActor()->GetProperty();

        if(axlAbstractData *data = dynamic_cast<axlAbstractData *>(this->data()))
        {
            QColor color = data->color();
            qreal *h = new qreal(0.0);
            qreal *s = new qreal(0.0);
            qreal *l = new qreal(0.0);
            color.getHslF(h, s, l);
            color.setHslF(*h, *s, *l + (1.0 - *l) / 2.0);
            prop->SetColor(color.redF(), color.greenF(), color.blueF());
            delete l;
            delete s;
            delete h;
        }

    }
    else if(state == 2)
    {
        this->setState(axlActor::edition);
        this->getActor()->SetPickable(0);
        vtkProperty *prop = this->getActor()->GetProperty();
        if(axlAbstractData *data = dynamic_cast<axlAbstractData *>(this->data()))
        {
            QColor color = data->color();
            qreal *h = new qreal(0.0);
            qreal *s = new qreal(0.0);
            qreal *l = new qreal(0.0);
            color.getHslF(h, s, l);
            color.setHslF(*h, *s, *l + (1.0 - *l)/2.0);
            prop->SetColor(color.redF(), color.greenF(), color.blueF());
            delete l;
            delete s;
            delete h;
        }
    }

    this->Modified();
}


void axlActorVolumeDiscrete::onRemoved()
{

}

void axlActorVolumeDiscrete::onUpdateGeometry()
{
    this->touch();
    this->update();
}



void axlActorVolumeDiscrete::outlineNone(void)
{
    d->outline_corner_actor->SetVisibility(0);
    d->outline_box_actor->SetVisibility(0);
    d->outline_contour_actor->SetVisibility(0);
}

void axlActorVolumeDiscrete::outlineCorners(void)
{
    d->outline_corner_actor->SetVisibility(1);
    d->outline_box_actor->SetVisibility(0);
    d->outline_contour_actor->SetVisibility(0);
}

void axlActorVolumeDiscrete::outlineBox(void)
{
    d->outline_corner_actor->SetVisibility(0);
    d->outline_box_actor->SetVisibility(1);
    d->outline_contour_actor->SetVisibility(0);
}

void axlActorVolumeDiscrete::outlineContour(void)
{
    d->outline_corner_actor->SetVisibility(0);
    d->outline_box_actor->SetVisibility(0);
    d->outline_contour_actor->SetVisibility(1);
}

void *axlActorVolumeDiscrete::colorTransferFunction(void)
{
    return d->colorFunction;
}

void *axlActorVolumeDiscrete::opacityTransferFunction(void)
{
    return d->opacityTransferFunction;
}

void *axlActorVolumeDiscrete::mapper(void)
{
    return d->mapper;
}

void *axlActorVolumeDiscrete::vol(void)
{
    return d->vol;
}

void *axlActorVolumeDiscrete::volumeProperty(void)
{
    return d->volProperty;
}

void *axlActorVolumeDiscrete::image(void)
{
    return d->image;
}

axlActorVolumeDiscrete::axlActorVolumeDiscrete(void) : axlActor(), d(new axlActorVolumeDiscretePrivate)
{
    d->volume = NULL;
#if(VTK_VERSION_MINOR >= 10)
    d->filter = NULL;
#endif
    d->mapper = vtkSmartVolumeMapper::New();
    //d->mapper->SetRequestedRenderMode(vtkSmartVolumeMapper::TextureRenderMode);
}

axlActorVolumeDiscrete::~axlActorVolumeDiscrete(void)
{
    delete d;

    d = NULL;
}
