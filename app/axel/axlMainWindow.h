/* axlMainWindow.h ---
 *
 * Author: Julien Wintz
 * Copyright (C) 2008 - Julien Wintz, Inria.
 * Created: Mon Dec  6 14:33:39 2010 (+0100)
 * Version: $Id$
 * Last-Updated: Wed Nov  9 11:48:59 2011 (+0100)
 *           By: Julien Wintz
 *     Update #: 24
 */

/* Commentary:
 *
 */

/* Change log:
 *
 */

#ifndef AXLMAINWINDOW_H
#define AXLMAINWINDOW_H

#include <axlCore/axlAbstractData.h>
#include <axlCore/axlAbstractView.h>
#include <axlGui/axlInspector.h>

#include <axlConfig.h>

#include <dtkCoreSupport/dtkAbstractData.h>

#include <QtGui>
#include <QDomElement>

class axlMainWindowPrivate;

class axlMainWindow : public QMainWindow
{
    Q_OBJECT


public:
     axlMainWindow(QWidget *parent = 0);
    ~axlMainWindow(void);

    void readSettings(void);
    void writeSettings(void);

public slots:
    void fileImportCurve(const QString& file = QString());
    void fileImportSurface(const QString& file = QString());
    void fileOpen(const QString& file = QString());
    void fileSave(const QString& file = QString());
    void fileSaveAs(const QString& file = QString());
    void fileImportOFF(const QString &file = QString());
    void fileImportIGES(const QString& file = QString());
    void fileExportTo_OFF(const QString &file = QString());
    void fileReadPythonScript(const QString &file);
    void onBackGroundColorSelected(QColor color);
    void clearScene(void);
    void readScene(QDomElement element);

    //add fileReader and fileWriter slots to read/write files that are not axl files.
    void fileImport(const QString& file = QString());
    void fileExport(const QString& file = QString());


    void scriptInterpreterPython();

    void update();
    void onDataInserted(axlAbstractData *data);
    void onDataSetInserted(QList<axlAbstractData *> dataSet);
    void onFieldsInserted(QList<axlAbstractData *> dataSet, QString fieldName);

    void onShowCurrentPoint(double u, double v,dtkAbstractData *data);
    void onMoveCurrentPoint(double u, double v, dtkAbstractData *data);
    void onHideCurrentPoint(double u, double v,dtkAbstractData *data);
    void showInspector(void);
    void switchFullNormalScreen(void);

    // interpreter slot
    void onCloseDockWidget();
    void onFloatScript();
    void onLoadScript();
    void onSaveScript();
    void onScriptDockWidgetFeaturesChanged(QDockWidget::DockWidgetFeatures);


    //java
    int runMySelf();

    //Useful for RPC protocol
    axlAbstractView *view(void);
    axlInspector *inspector(void);




protected:
    void closeEvent(QCloseEvent *event);

private :
    void setCurrentFileSetting(QString filename);
    void fileSetScene(QDomDocument *doc);

private:
    axlMainWindowPrivate *d;
};

#endif
