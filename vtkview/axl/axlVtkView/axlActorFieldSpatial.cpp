/* axlActorFieldSpatial.cpp ---
 *
 * Author: Anais Ducoffe
 * Copyright (C) 20013- Anais Ducoffe, Inria.
 */

/* Commentary:
 *
 */

/* Change log:
 *
 */

#include <axlCore/axlAbstractFieldSpatial.h>
#include "axlActorFieldSpatial.h"
#include "axlActor.h"
#include "axlActorDataDynamic.h"
//#include "axlActorCone.h"
//#include <axlCore/axlCone.h>
//#include <axlCore/axlPoint.h>
#include <vtkConeSource.h>
#include <axlCore/axlFieldDiscrete.h>
#include <axlCore/axlMesh.h>

#include <axlCore/axlAbstractActorField.h>
#include <axlCore/axlAbstractField.h>

#include <dtkCoreSupport/dtkGlobal.h>

#include <vtkActor.h>
#include <vtkArrowSource.h>
#include <vtkCommand.h>
#include <vtkContourFilter.h>
#include <vtkDataSetMapper.h>
#include <vtkCellData.h>
#include <vtkGlyph3D.h>
#include <vtkHedgeHog.h>
#include <vtkObjectFactory.h>
#include <vtkPointData.h>
#include <vtkPolyDataMapper.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkSmartPointer.h>
#include <vtkSphere.h>
#include <vtkSphereRepresentation.h>
#include <vtkSphereSource.h>
#include <vtkSphereWidget2.h>
#include <vtkStreamTracer.h>
#include <vtkTubeFilter.h>
#include <vtkPolyData.h>
#include <vtkUnstructuredGrid.h>

#include <vtkExtractUnstructuredGrid.h>

#include <vtkIntArray.h>
#include <vtkFloatArray.h>
#include <vtkDoubleArray.h>
#include <vtkDataArray.h>

#include <axlCore/axlAbstractData.h>

// /////////////////////////////////////////////////////////////////
// axlActorFieldSpatialStreamObserver
// /////////////////////////////////////////////////////////////////

class axlActorFieldSpatialStreamObserver : public vtkCommand
{
public:
    static axlActorFieldSpatialStreamObserver *New(void)
    {
        return new axlActorFieldSpatialStreamObserver;
    }

    virtual void Execute(vtkObject *caller, unsigned long event, void *)
    {
//        if(event == vtkCommand::InteractionEvent) {

//            vector_stream_widget_representation->GetPolyData(vector_stream_widget_data);
//#if (VTK_MAJOR_VERSION <= 5)
//            vector_stream_tracer->SetSource(vector_stream_widget_data);
//#else
//            vector_stream_tracer->SetSourceData(vector_stream_widget_data);
//#endif
//            vector_stream_tracer->Update();
//        }
    }

    vtkSmartPointer<vtkSphereRepresentation> vector_stream_widget_representation;
    vtkSmartPointer<vtkPolyData> vector_stream_widget_data;
    vtkSmartPointer<vtkStreamTracer> vector_stream_tracer;
};

// /////////////////////////////////////////////////////////////////
// axlActorFieldSpatialPrivate
// /////////////////////////////////////////////////////////////////

class axlActorFieldSpatialPrivate
{
public:
    axlAbstractFieldSpatial *field;
    axlActor *mesh;
    vtkDataArray *array;
    //vtkSmartPointer<vtkPolyData> polyDataForSources;

public:
    bool scalar_display_as_iso;
    int scalar_iso_count;
    double scalar_iso_range[2];
    vtkSmartPointer<vtkTubeFilter> scalar_iso_tube_filter;
    double isoRadius;
    vtkSmartPointer<vtkContourFilter> scalar_iso;
    vtkSmartPointer<vtkPolyDataMapper> scalar_iso_mapper;
    vtkSmartPointer<vtkActor> scalar_iso_actor;

    vtkSmartPointer<vtkPolyDataMapper> scalar_iso_color_mapper;
    vtkSmartPointer<vtkActor> scalar_iso_color_actor;

    double scalar_color_range[2];
    vtkSmartPointer<vtkDataSetMapper> scalar_color_mapper;
    vtkSmartPointer<vtkActor> scalar_color_actor;

    vtkSmartPointer<vtkScalarBarActor> scalar_bar_actor;

    vtkSmartPointer<vtkHedgeHog> vector_hedgehog;
    vtkSmartPointer<vtkPolyDataMapper> vector_hedgehog_mapper;
    vtkSmartPointer<vtkActor> vector_hedgehog_actor;

    vtkSmartPointer<vtkArrowSource> vector_glyph_source;
    vtkSmartPointer<vtkGlyph3D> vector_glyph;
    vtkSmartPointer<vtkPolyDataMapper> vector_glyph_mapper;
    vtkSmartPointer<vtkActor> vector_glyph_actor;

    vtkSmartPointer<vtkSphereWidget2> vector_stream_widget;
    vtkSmartPointer<vtkSphereRepresentation> vector_stream_widget_representation;
    vtkSmartPointer<vtkPolyData> vector_stream_widget_data;
    vtkSmartPointer<vtkStreamTracer> vector_stream_tracer;
    vtkSmartPointer<vtkTubeFilter> vector_stream_filter;
    vtkSmartPointer<vtkPolyDataMapper> vector_stream_mapper;
    vtkSmartPointer<vtkActor> vector_stream_actor;
    vtkSmartPointer<axlActorFieldSpatialStreamObserver> vector_stream_observer;

    vtkSmartPointer<vtkRenderWindowInteractor> interactor;
};

// /////////////////////////////////////////////////////////////////
// axlActorFieldSpatial
// /////////////////////////////////////////////////////////////////

#if (VTK_MAJOR_VERSION <= 5)
vtkCxxRevisionMacro(axlActorFieldSpatial, "$Revision: 0.0.1 $");
#endif

vtkStandardNewMacro(axlActorFieldSpatial);

dtkAbstractData *axlActorFieldSpatial::data(void)
{
    return NULL;
}

axlAbstractField *axlActorFieldSpatial::field(void)
{
    if(d->field){
        return d->field;
    }
    return nullptr;
}

axlAbstractField *axlActorFieldSpatial::magnitude(void)
{
    if(d->mesh->fields().contains(QString("%1 magnitude").arg(d->field->objectName())))
        return NULL;

    QString field_magnitude_name = QString("%1 magnitude").arg(d->field->objectName());

    double size = 0;// represents the number of vertices in the mesh of the parent's field;
    if(d->mesh->getUnstructuredGrid())
        size = d->mesh->getUnstructuredGrid()->GetNumberOfPoints();
    else
        size = d->mesh->getPolyData()->GetNumberOfPoints();
    double *tuple;
    axlFieldDiscrete *field_magnitude = new axlFieldDiscrete(field_magnitude_name, d->field->type(), axlFieldDiscrete::Scalar, d->field->support(), size);

    for(int i = 0; i < size; i++){
        if(d->mesh->getUnstructuredGrid())
            tuple = d->field->vector(d->mesh->getUnstructuredGrid()->GetPoint(i)[0], d->mesh->getUnstructuredGrid()->GetPoint(i)[1], d->mesh->getUnstructuredGrid()->GetPoint(i)[2]);
        else
            tuple = d->field->vector(d->mesh->getPolyData()->GetPoint(i)[0], d->mesh->getPolyData()->GetPoint(i)[1], d->mesh->getPolyData()->GetPoint(i)[2]);
        field_magnitude->setScalar(i, qSqrt(tuple[0]*tuple[0]+tuple[0]*tuple[0]+tuple[0]*tuple[0]));
    }
    return field_magnitude;
}



axlAbstractActor *axlActorFieldSpatial::actorField(void)
{
    return d->mesh;
}

vtkScalarBarActor *axlActorFieldSpatial::scalarBar(void){
    return d->scalar_bar_actor;
}

double axlActorFieldSpatial::colRangeMin(void)
{
    return d->scalar_color_range[0];
}

double axlActorFieldSpatial::colRangeMax(void)
{
    return d->scalar_color_range[1];
}

int axlActorFieldSpatial::isoCount(void)
{
    return d->scalar_iso->GetNumberOfContours();
}

double axlActorFieldSpatial::isoRangeMin(void)
{
    return d->scalar_iso_range[0];
}

double axlActorFieldSpatial::isoRangeMax(void)
{
    return d->scalar_iso_range[1];
}

double axlActorFieldSpatial::glyphScale(void)
{
    return d->vector_glyph->GetScaleFactor();
}

double axlActorFieldSpatial::streamRadius(void)
{
    return d->vector_stream_filter->GetRadius();
}

void axlActorFieldSpatial::setInteractor(void *interactor)
{
    d->interactor = static_cast<vtkRenderWindowInteractor *>(interactor);

    this->setup();
}


void axlActorFieldSpatial::updateArray(void){


    if(d->field){
        int size = 0;
        if(d->mesh->getUnstructuredGrid()){
            size = d->mesh->getUnstructuredGrid()->GetNumberOfPoints();
        }else if(d->mesh->getPolyData()){
            size = d->mesh->getPolyData()->GetNumberOfPoints();
        }else{
            if(dynamic_cast<axlActorDataDynamic *>(d->mesh)){
                axlActorDataDynamic *actorDD = dynamic_cast<axlActorDataDynamic *>(d->mesh);
                size = dynamic_cast<axlActor *>(actorDD->outputActor())->getMapper()->GetInput()->GetNumberOfPoints();
                d->mesh->setPolyData(dynamic_cast<axlActor *>(actorDD->outputActor())->getMapper()->GetInput());
                //d->mesh->setPoints(d->mesh->getPolyData()->GetPoints());
            }else{
                size = d->mesh->getMapper()->GetInput()->GetNumberOfPoints();
                d->mesh->setPolyData(d->mesh->getMapper()->GetInput());
                //d->mesh->setPoints(d->mesh->getPolyData()->GetPoints());
            }

        }

        d->array->SetNumberOfTuples(size);


        double *tuple1 = NULL;
        double tuple2 = 0;
        for(int i = 0; i < size; i++){
            if(d->field->kind() == axlAbstractFieldSpatial::Scalar){

                if(d->mesh->getUnstructuredGrid())
                    tuple2 = d->field->scalar(d->mesh->getUnstructuredGrid()->GetPoint(i)[0], d->mesh->getUnstructuredGrid()->GetPoint(i)[1], d->mesh->getUnstructuredGrid()->GetPoint(i)[2]);
                else{
                    tuple2 = d->field->scalar(d->mesh->getPolyData()->GetPoint(i)[0], d->mesh->getPolyData()->GetPoint(i)[1], d->mesh->getPolyData()->GetPoint(i)[2]);
                }
                d->array->SetTuple1(i, tuple2);
                d->array->Modified();
            }
            else if(d->field->kind() == axlAbstractFieldSpatial::Vector){

                if(d->mesh->getUnstructuredGrid())
                    tuple1 = d->field->vector(d->mesh->getUnstructuredGrid()->GetPoint(i)[0], d->mesh->getUnstructuredGrid()->GetPoint(i)[1], d->mesh->getUnstructuredGrid()->GetPoint(i)[2]);
                else
                    tuple1 = d->field->vector(d->mesh->getPolyData()->GetPoint(i)[0], d->mesh->getPolyData()->GetPoint(i)[1], d->mesh->getPolyData()->GetPoint(i)[2]);
                d->array->SetTuple(i, tuple1);
                d->array->Modified();
            }
            else if(d->field->kind() == axlAbstractFieldSpatial::Tensor){

                if(d->mesh->getUnstructuredGrid())
                    tuple1 = d->field->tensor(d->mesh->getUnstructuredGrid()->GetPoint(i)[0], d->mesh->getUnstructuredGrid()->GetPoint(i)[1], d->mesh->getUnstructuredGrid()->GetPoint(i)[2]);
                else
                    tuple1 = d->field->tensor(d->mesh->getPolyData()->GetPoint(i)[0], d->mesh->getPolyData()->GetPoint(i)[1], d->mesh->getPolyData()->GetPoint(i)[2]);
                d->array->SetTuple(i, tuple1);
                d->array->Modified();
            }

        }
    }


    //sets the minimum and maximum values of the field.
    d->field->setMin(this->minValue());
    d->field->setMax(this->maxValue());

    //setRange
    if(d->scalar_color_range && d->scalar_color_mapper){
        this->setColRangeMin(d->field->minValue());
        this->setColRangeMax(d->field->maxValue());
    }

}

void axlActorFieldSpatial::setData(dtkAbstractData *field)
{
    d->field = dynamic_cast<axlAbstractFieldSpatial *>(field);

    if(d->field){
        axlAbstractData * parentData = dynamic_cast<axlAbstractData *>(d->field->parent());
        if(parentData){
            connect(parentData, SIGNAL(modifiedGeometry()), this, SLOT(update()));
        }
        axlAbstractData *axlData = dynamic_cast<axlAbstractData *>(d->mesh->data());
        connect(axlData, SIGNAL(modifiedGeometry()), this, SLOT(update()));


        if(d->field->type() == axlAbstractFieldSpatial::Int)
            d->array = vtkIntArray::New();
        else if(d->field->type() == axlAbstractFieldSpatial::Float)
            d->array = vtkFloatArray::New();
        else if(d->field->type() == axlAbstractFieldSpatial::Double)
            d->array = vtkDoubleArray::New();

        axlAbstractFieldSpatial::Kind kind = d->field->kind();

        switch(kind) {
        case axlAbstractFieldSpatial::Scalar:
            d->array->SetNumberOfComponents(1);
            break;
        case axlAbstractFieldSpatial::Vector:
            d->array->SetNumberOfComponents(3);
            break;
        case axlAbstractFieldSpatial::Tensor:
            d->array->SetNumberOfComponents(9);
            break;
        default:
            qDebug() << "Unsupported field kind";
        };

        QString name = d->field->objectName();
        d->array->SetName(qPrintable(name));



        this->updateArray();
        this->setup();
    }
}

void axlActorFieldSpatial::setActorField(axlAbstractActor *actorfield)
{
    d->mesh = dynamic_cast<axlActor *>(actorfield);

    this->setup();
}

void axlActorFieldSpatial::setColRangeMin(double min)
{
    d->scalar_color_range[0] = min;
    d->scalar_color_mapper->SetScalarRange(d->scalar_color_range);
    d->scalar_color_mapper->Update();
}

void axlActorFieldSpatial::setColRangeMax(double max)
{
    d->scalar_color_range[1] = max;
    d->scalar_color_mapper->SetScalarRange(d->scalar_color_range);
    d->scalar_color_mapper->Update();
}

void axlActorFieldSpatial::setIsoCount(int count)
{
    d->scalar_iso_count = count;
    d->scalar_iso->GenerateValues(d->scalar_iso_count, d->scalar_iso_range[0], d->scalar_iso_range[1]);
    d->scalar_iso->Update();

    d->scalar_iso_tube_filter->Update();
}

void axlActorFieldSpatial::setIsoRangeMin(double min)
{
    d->scalar_iso_range[0] = min;
    d->scalar_iso->GenerateValues(d->scalar_iso_count, d->scalar_iso_range[0], d->scalar_iso_range[1]);
    d->scalar_iso->Update();
    d->scalar_iso_color_mapper->SetScalarRange(d->scalar_iso_range);
    d->scalar_iso_color_mapper->Update();

    d->scalar_iso_tube_filter->Update();
}

void axlActorFieldSpatial::setIsoRangeMax(double max)
{
    d->scalar_iso_range[1] = max;
    d->scalar_iso->GenerateValues(d->scalar_iso_count, d->scalar_iso_range[0], d->scalar_iso_range[1]);
    d->scalar_iso->Update();
    d->scalar_iso_color_mapper->SetScalarRange(d->scalar_iso_range);
    d->scalar_iso_color_mapper->Update();

    d->scalar_iso_tube_filter->Update();
}

void axlActorFieldSpatial::setGlyphScale(double scale)
{
    //compute size of controlPoints
    double bounds[6]; d->mesh->GetBounds(bounds);
    double side = qAbs(bounds[1]-bounds[0]);
    side += qAbs(bounds[3]-bounds[2]);
    side += qAbs(bounds[5]-bounds[4]);

    side /= 30;
    //d->vector_glyph_source->SetShaftRadius(scale * side);
    //d->vector_glyph_source->SetTipLength(200 *3 * 3.5 * scale * side);
    //d->vector_glyph_source->SetTipRadius(3 * scale * side);
    d->vector_glyph->SetScaleFactor(scale * side);
    d->vector_hedgehog->SetScaleFactor(scale * side * 0.1);
}

void axlActorFieldSpatial::setStreamPropagation(double propagation)
{
    d->vector_stream_tracer->SetMaximumPropagation(propagation);
    d->vector_stream_tracer->Update();
}

void axlActorFieldSpatial::setStreamRadius(double radius)
{
    d->vector_stream_filter->SetRadius(radius);
    d->vector_stream_filter->Update();
}

void axlActorFieldSpatial::setStreamDirection(int direction)
{
    switch (direction) {
    case 0:
        d->vector_stream_tracer->SetIntegrationDirectionToForward();
        break;
    case 1:
        d->vector_stream_tracer->SetIntegrationDirectionToBackward();
        break;
    case 2:
        d->vector_stream_tracer->SetIntegrationDirectionToBoth();
        break;
    default:
        break;
    }

    d->vector_stream_tracer->Update();
}

void axlActorFieldSpatial::displayAsColor(void)
{
    if(d->mesh->getUnstructuredGrid()){
        d->mesh->getDataSetMapper()->ScalarVisibilityOn();
    }else{
        d->mesh->getMapper()->ScalarVisibilityOn();
    }

    if (d->field->kind() != axlAbstractFieldSpatial::Scalar)
        return;

    if(d->field->support() == axlAbstractFieldSpatial::Point) {
        if(d->mesh->getUnstructuredGrid()){
            static_cast<vtkUnstructuredGrid *>(d->mesh->getUnstructuredGrid())->GetPointData()->SetActiveScalars(qPrintable(d->field->objectName()));
        }else{
            static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetPointData()->SetActiveScalars(qPrintable(d->field->objectName()));
        }
    }
    if(d->field->support() == axlAbstractFieldSpatial::Cell){
        if(d->mesh->getUnstructuredGrid()){
            static_cast<vtkUnstructuredGrid *>(d->mesh->getUnstructuredGrid())->GetCellData()->SetActiveScalars(qPrintable(d->field->objectName()));
        }else{
            static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetCellData()->SetActiveScalars(qPrintable(d->field->objectName()));
        }
    }


    d->scalar_color_actor->SetVisibility(1);
    d->scalar_iso_actor->SetVisibility(0);
    d->scalar_iso_color_actor->SetVisibility(0);
    d->scalar_display_as_iso = false;
    d->scalar_bar_actor->SetVisibility(1);

}

void axlActorFieldSpatial::displayAsNoneScalar(void)
{
    if(d->scalar_color_actor)
    {
        d->scalar_color_actor->SetVisibility(0);
        d->scalar_iso_actor->SetVisibility(0);
        d->scalar_iso_color_actor->SetVisibility(0);
        d->scalar_display_as_iso = false;
        d->scalar_bar_actor->SetVisibility(0);
    }

    if(d->mesh->getUnstructuredGrid()){
        d->mesh->getDataSetMapper()->ScalarVisibilityOn();
    }
    else{
        d->mesh->getMapper()->ScalarVisibilityOn();
    }

    if(d->field->support() == axlAbstractFieldSpatial::Point) {
        if(d->mesh->getUnstructuredGrid()){
            static_cast<vtkUnstructuredGrid *>(d->mesh->getUnstructuredGrid())->GetPointData()->SetActiveScalars("mapperCollorArrayDefaultField");
        } else{
            static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetPointData()->SetActiveScalars("mapperCollorArrayDefaultField");
        }
    }
}

void axlActorFieldSpatial::displayAsIso(void)
{
    if (d->field->kind() != axlAbstractFieldSpatial::Scalar)
        return;

    if(d->field->support() == axlAbstractFieldSpatial::Point) {
        if(d->mesh->getUnstructuredGrid()){
            static_cast<vtkUnstructuredGrid *>(d->mesh->getUnstructuredGrid())->GetPointData()->SetActiveScalars(qPrintable(d->field->objectName()));
        }
        else{
            static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetPointData()->SetActiveScalars(qPrintable(d->field->objectName()));
        }
    }

    if(d->field->support() == axlAbstractFieldSpatial::Cell) {
        if(d->mesh->getUnstructuredGrid()){
            static_cast<vtkUnstructuredGrid *>(d->mesh->getUnstructuredGrid())->GetCellData()->SetActiveScalars(qPrintable(d->field->objectName()));
        }
        else{
            static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetCellData()->SetActiveScalars(qPrintable(d->field->objectName()));
        }
    }

    if(d->field->support() == axlAbstractFieldSpatial::Point) {
        if(d->mesh->getUnstructuredGrid()){
            d->mesh->getDataSetMapper()->ScalarVisibilityOff();
        }
        else{
            d->mesh->getMapper()->ScalarVisibilityOff();
        }
    }

    this->setIsoCount(d->scalar_iso_count);

    d->scalar_color_actor->SetVisibility(0);
    d->scalar_iso_actor->SetVisibility(1);
    d->scalar_iso_color_actor->SetVisibility(0);
    d->scalar_display_as_iso = true;
    d->scalar_bar_actor->SetVisibility(0);
}

void axlActorFieldSpatial::displayAsNoneVector(void)
{
    if (d->field->kind() == axlAbstractFieldSpatial::Vector ) {

        d->vector_hedgehog_actor->SetVisibility(0);
        d->vector_glyph_actor->SetVisibility(0);
        d->vector_stream_actor->SetVisibility(0);
        d->vector_stream_widget->Off();
    }
}

void axlActorFieldSpatial::displayAsHedge(void)
{
    if (d->field->kind() == axlAbstractFieldSpatial::Vector ) {

        d->vector_hedgehog_actor->SetVisibility(1);
        d->vector_glyph_actor->SetVisibility(0);
        d->vector_stream_actor->SetVisibility(0);
        d->vector_stream_widget->Off();
    }
}

void axlActorFieldSpatial::displayAsGlyph(void)
{
    if (d->field->kind() == axlAbstractFieldSpatial::Vector) {

        d->vector_hedgehog_actor->SetVisibility(0);
        d->vector_glyph_actor->SetVisibility(1);
        d->vector_stream_actor->SetVisibility(0);
        d->vector_stream_widget->Off();
    }
}

void axlActorFieldSpatial::displayAsStream(void)
{
    if (d->field->kind() == axlAbstractFieldSpatial::Vector) {

        d->vector_hedgehog_actor->SetVisibility(0);
        d->vector_glyph_actor->SetVisibility(0);
        d->vector_stream_actor->SetVisibility(1);
        d->vector_stream_widget->On();
    }
}

#include <axlCore/axlAbstractSurfaceBSpline.h>

void axlActorFieldSpatial::update(void)
{
    if(!d->mesh)
        return;

    if(d->mesh->getUnstructuredGrid()){
        d->mesh->getUnstructuredGrid()->Modified();
#if (VTK_MAJOR_VERSION <= 5)
        d->mesh->getUnstructuredGrid()->Update();
#endif
    }else{
        d->mesh->getPolyData()->Modified();
#if (VTK_MAJOR_VERSION <= 5)
        d->mesh->getPolyData()->Update();
#endif
    }

    if(!d->field )
        return;

    //update the value of the field.
    this->updateArray();

    if(d->field->support() == axlAbstractFieldSpatial::Point) {
        if(d->mesh->getUnstructuredGrid()){
            static_cast<vtkUnstructuredGrid *>(d->mesh->getUnstructuredGrid())->GetPointData()->RemoveArray(d->array->GetName());
            static_cast<vtkUnstructuredGrid *>(d->mesh->getUnstructuredGrid())->GetPointData()->AddArray(d->array);
        }else{
            static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetPointData()->RemoveArray(d->array->GetName());
            static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetPointData()->AddArray(d->array);
        }
        if(d->field->kind() == axlAbstractFieldSpatial::Scalar){
            //update field vtkActor(s)
            d->scalar_iso_actor->SetOrientation(d->mesh->getActor()->GetOrientation());
            d->scalar_iso_actor->SetPosition(d->mesh->getActor()->GetPosition());
            d->scalar_iso_color_actor->SetOrientation(d->mesh->getActor()->GetOrientation());
            d->scalar_iso_color_actor->SetPosition(d->mesh->getActor()->GetPosition());
            d->scalar_color_actor->SetOrientation(d->mesh->getActor()->GetOrientation());
            d->scalar_color_actor->SetPosition(d->mesh->getActor()->GetPosition());


            if(d->mesh->getUnstructuredGrid()){
                static_cast<vtkUnstructuredGrid *>(d->mesh->getUnstructuredGrid())->GetPointData()->SetActiveScalars(d->array->GetName());
            }else{
                static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetPointData()->SetActiveScalars(d->array->GetName());
            }
        }else if (d->field->kind() == axlAbstractFieldSpatial::Vector){
            //update field vtkActor(s)
            d->vector_glyph_actor->SetOrientation(d->mesh->getActor()->GetOrientation());
            d->vector_glyph_actor->SetPosition(d->mesh->getActor()->GetPosition());
            d->vector_hedgehog_actor->SetOrientation(d->mesh->getActor()->GetOrientation());
            d->vector_hedgehog_actor->SetPosition(d->mesh->getActor()->GetPosition());
            d->vector_stream_actor->SetOrientation(d->mesh->getActor()->GetOrientation());
            d->vector_stream_actor->SetPosition(d->mesh->getActor()->GetPosition());


            if(d->mesh->getUnstructuredGrid()){
                static_cast<vtkUnstructuredGrid *>(d->mesh->getUnstructuredGrid())->GetPointData()->SetActiveVectors(d->array->GetName());
            }else{
                static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetPointData()->SetActiveVectors(d->array->GetName());
            }
        }else if (d->field->kind() == axlAbstractFieldSpatial::Tensor){

            if(d->mesh->getUnstructuredGrid()){
                static_cast<vtkUnstructuredGrid *>(d->mesh->getUnstructuredGrid())->GetPointData()->SetActiveTensors(d->array->GetName());
            }else{
                static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetPointData()->SetActiveTensors(d->array->GetName());
            }
        }

    }



    if(d->field->support() == axlAbstractFieldSpatial::Cell) {
        if(d->mesh->getPolyData()){
            static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetCellData()->RemoveArray(d->array->GetName());
            static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetCellData()->AddArray(d->array);
        }else{
            static_cast<vtkUnstructuredGrid *>(d->mesh->getUnstructuredGrid())->GetCellData()->RemoveArray(d->array->GetName());
            static_cast<vtkUnstructuredGrid *>(d->mesh->getUnstructuredGrid())->GetCellData()->AddArray(d->array);
        }
        if(d->field->kind() == axlAbstractFieldSpatial::Scalar){
            if(d->mesh->getPolyData())
                static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetCellData()->SetActiveScalars(d->array->GetName());
            else
                static_cast<vtkUnstructuredGrid *>(d->mesh->getUnstructuredGrid())->GetCellData()->SetActiveScalars(d->array->GetName());
        }else if (d->field->kind() == axlAbstractFieldSpatial::Vector){
            if(d->mesh)
                static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetCellData()->SetActiveVectors(d->array->GetName());
            else
                static_cast<vtkUnstructuredGrid *>(d->mesh->getUnstructuredGrid())->GetCellData()->SetActiveVectors(d->array->GetName());
        }else if (d->field->kind() == axlAbstractFieldSpatial::Tensor){
            if( d->mesh->getPolyData())
                static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetCellData()->SetActiveTensors(d->array->GetName());
            else
                static_cast<vtkUnstructuredGrid *>(d->mesh->getUnstructuredGrid())->GetCellData()->SetActiveTensors(d->array->GetName());
        }


    }

}

void axlActorFieldSpatial::setActiveFieldKind(void)
{
    if(d->mesh && d->field)
        if(d->field->support() == axlAbstractFieldSpatial::Point) {
            if(d->field->kind() == axlAbstractFieldSpatial::Scalar)
                if(d->mesh->getUnstructuredGrid()){
                    static_cast<vtkUnstructuredGrid *>(d->mesh->getUnstructuredGrid())->GetPointData()->SetActiveScalars(qPrintable(d->field->objectName()));
                }else{
                    static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetPointData()->SetActiveScalars(qPrintable(d->field->objectName()));
                }
            else if(d->field->kind() == axlAbstractFieldSpatial::Vector){
                if(d->mesh->getUnstructuredGrid()){
                    static_cast<vtkUnstructuredGrid *>(d->mesh->getUnstructuredGrid())->GetPointData()->SetActiveVectors(qPrintable(d->field->objectName()));
                }else{
                    static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetPointData()->SetActiveVectors(qPrintable(d->field->objectName()));
                }
            }else if(d->field->kind() == axlAbstractFieldSpatial::Tensor){
                if(d->mesh->getUnstructuredGrid()){
                    static_cast<vtkUnstructuredGrid *>(d->mesh->getUnstructuredGrid())->GetPointData()->SetActiveTensors(qPrintable(d->field->objectName()));
                }else{
                    static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetPointData()->SetActiveTensors(qPrintable(d->field->objectName()));
                }
            }
        }


    if(d->mesh && d->field)
        if(d->field->support() == axlAbstractFieldSpatial::Cell) {
            if(d->field->kind() == axlAbstractFieldSpatial::Scalar)
                static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetCellData()->SetActiveScalars(qPrintable(d->field->objectName()));
            else if(d->field->kind() == axlAbstractFieldSpatial::Vector)
                static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetCellData()->SetActiveVectors(qPrintable(d->field->objectName()));
            else if(d->field->kind() == axlAbstractFieldSpatial::Tensor)
                static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetCellData()->SetActiveTensors(qPrintable(d->field->objectName()));
        }

}

void *axlActorFieldSpatial::scalarColorMapper(void)
{
    return d->scalar_color_mapper;
}

axlActorFieldSpatial::axlActorFieldSpatial(void) : axlAbstractActorField(), d(new axlActorFieldSpatialPrivate)
{
    d->field = NULL;
    d->mesh = NULL;
    d->isoRadius = 0.01;
    d->array = NULL;
}

axlActorFieldSpatial::~axlActorFieldSpatial(void)
{
    delete d;

    d = NULL;
}

void axlActorFieldSpatial::setup(void)
{

    if(!d->mesh)
        return;

    if(!d->field) {
        qDebug() << DTK_PRETTY_FUNCTION << "No field.";
        return;
    }


    if(!d->interactor)
        return;

    // -- Scalar field

    if(d->field->kind() == axlAbstractFieldSpatial::Scalar) {

        if(d->scalar_color_mapper && d->scalar_color_actor && d->scalar_iso_color_actor)
            return;

        if(d->field->support() == axlAbstractFieldSpatial::Point) {
            //remove first eventual array of the same name
            if(d->mesh->getPolyData()){
                static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetPointData()->RemoveArray(d->array->GetName());
                static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetPointData()->AddArray(d->array);
                static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetPointData()->SetActiveScalars(d->array->GetName());
            }
            if(d->mesh->getUnstructuredGrid()){
                static_cast<vtkUnstructuredGrid *>(d->mesh->getUnstructuredGrid())->GetPointData()->RemoveArray(d->array->GetName());
                static_cast<vtkUnstructuredGrid *>(d->mesh->getUnstructuredGrid())->GetPointData()->AddArray(d->array);
                static_cast<vtkUnstructuredGrid *>(d->mesh->getUnstructuredGrid())->GetPointData()->SetActiveScalars(d->array->GetName());
            }
        }

        if(d->field->support() == axlAbstractFieldSpatial::Cell) {

            static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetCellData()->RemoveArray(d->array->GetName());
            static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetCellData()->AddArray(d->array);
            static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetPointData()->SetActiveScalars(d->array->GetName());

        }

        // -- iso contours

        d->scalar_display_as_iso = false;
        d->scalar_iso_count = 10;
        d->array->GetRange(d->scalar_iso_range);

        d->scalar_iso = vtkSmartPointer<vtkContourFilter>::New();
        if(d->mesh->getUnstructuredGrid()){
#if (VTK_MAJOR_VERSION <= 5)
            d->scalar_iso->SetInput(static_cast<vtkUnstructuredGrid *>(d->mesh->getUnstructuredGrid()));
#else
            d->scalar_iso->SetInputData(static_cast<vtkUnstructuredGrid *>(d->mesh->getUnstructuredGrid()));
#endif
        }else{
#if (VTK_MAJOR_VERSION <= 5)
            d->scalar_iso->SetInput(static_cast<vtkPolyData *>(d->mesh->getPolyData()));
#else
            d->scalar_iso->SetInputData(static_cast<vtkPolyData *>(d->mesh->getPolyData()));
#endif
        }
        d->scalar_iso->GenerateValues(d->scalar_iso_count, d->scalar_iso_range[0], d->scalar_iso_range[1]);

        d->scalar_iso_tube_filter = vtkSmartPointer<vtkTubeFilter>::New();
        d->scalar_iso_tube_filter->SetRadius(d->isoRadius);
        d->scalar_iso_tube_filter->SetNumberOfSides(8);
#if (VTK_MAJOR_VERSION <= 5)
        d->scalar_iso_tube_filter->SetInput(d->scalar_iso->GetOutput());
#else
        d->scalar_iso_tube_filter->SetInputData(d->scalar_iso->GetOutput());
#endif
        d->scalar_iso_mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
#if (VTK_MAJOR_VERSION <= 5)
        d->scalar_iso_mapper->SetInput(d->scalar_iso_tube_filter->GetOutput());
#else
        d->scalar_iso_mapper->SetInputData(d->scalar_iso_tube_filter->GetOutput());
#endif

        d->scalar_iso_actor = vtkSmartPointer<vtkActor>::New();
        d->scalar_iso_actor->SetOrientation(d->mesh->getActor()->GetOrientation());
        d->scalar_iso_actor->SetPosition(d->mesh->getActor()->GetPosition());
        d->scalar_iso_actor->SetMapper(d->scalar_iso_mapper);
        d->scalar_iso_actor->SetVisibility(0);

        this->AddPart(d->scalar_iso_actor);

        // -- iso color mapping

        d->scalar_iso_color_mapper = vtkPolyDataMapper::New();
#if (VTK_MAJOR_VERSION <= 5)
        d->scalar_iso_color_mapper->SetInput(d->scalar_iso->GetOutput());
#else
        d->scalar_iso_color_mapper->SetInputData(d->scalar_iso->GetOutput());
#endif
        d->scalar_iso_color_mapper->SetColorModeToMapScalars();

        if(d->field->support() == axlAbstractFieldSpatial::Point)
            d->scalar_iso_color_mapper->SetScalarModeToUsePointData();

        if(d->field->support() == axlAbstractFieldSpatial::Cell)
            d->scalar_iso_color_mapper->SetScalarModeToUseCellData();

        d->scalar_iso_color_mapper->SelectColorArray(d->array->GetName());
        d->scalar_iso_color_mapper->SetScalarVisibility(true);
        d->scalar_iso_color_mapper->SetScalarRange(d->scalar_iso_range);

        d->scalar_iso_color_actor = vtkSmartPointer<vtkActor>::New();
        d->scalar_iso_color_actor->SetOrientation(d->mesh->getActor()->GetOrientation());
        d->scalar_iso_color_actor->SetPosition(d->mesh->getActor()->GetPosition());
        d->scalar_iso_color_actor->SetMapper(d->scalar_iso_color_mapper);
        d->scalar_iso_color_actor->SetVisibility(0);

        this->AddPart(d->scalar_iso_color_actor);

        // -- color mapping

        d->array->GetRange(d->scalar_color_range);

        d->scalar_color_mapper = vtkDataSetMapper::New();
#if (VTK_MAJOR_VERSION <= 5)
        if(d->mesh->getUnstructuredGrid())
            d->scalar_color_mapper->SetInput(static_cast<vtkUnstructuredGrid *>(d->mesh->getUnstructuredGrid()));
        else
            d->scalar_color_mapper->SetInput(static_cast<vtkPolyData *>(d->mesh->getPolyData()));
#else
        if(d->mesh->getUnstructuredGrid())
            d->scalar_color_mapper->SetInputData(static_cast<vtkUnstructuredGrid *>(d->mesh->getUnstructuredGrid()));
        else
            d->scalar_color_mapper->SetInputData(static_cast<vtkPolyData *>(d->mesh->getPolyData()));
#endif

        d->scalar_color_mapper->SetColorModeToMapScalars();

        if(d->field->support() == axlAbstractFieldSpatial::Point)
            d->scalar_color_mapper->SetScalarModeToUsePointData();

        if(d->field->support() == axlAbstractFieldSpatial::Cell)
            d->scalar_color_mapper->SetScalarModeToUseCellData();

        d->scalar_color_mapper->SelectColorArray(d->array->GetName());
        d->scalar_color_mapper->SetScalarVisibility(true);
        d->scalar_color_mapper->SetScalarRange(d->scalar_color_range);

        d->scalar_color_actor = vtkSmartPointer<vtkActor>::New();
        d->scalar_color_actor->SetOrientation(d->mesh->getActor()->GetOrientation());
        d->scalar_color_actor->SetPosition(d->mesh->getActor()->GetPosition());
        d->scalar_color_actor->SetMapper(d->scalar_color_mapper);
        d->scalar_color_actor->SetVisibility(0);


        this->AddPart(d->scalar_color_actor);

        ////Add actor for color bar
        d->scalar_bar_actor = vtkSmartPointer<vtkScalarBarActor>::New();
        d->scalar_bar_actor ->SetLookupTable(d->scalar_color_mapper->GetLookupTable());
        d->scalar_bar_actor ->SetTitle(qPrintable(d->field->name()));
        d->scalar_bar_actor ->SetNumberOfLabels(4);
        d->scalar_bar_actor->SetVisibility(0);

    }

    // -- Vector field


    if(d->field->kind() == axlAbstractFieldSpatial::Vector) {

        // -- Append field and its magnitude into vtkPolyData

        if(d->field->support() == axlAbstractFieldSpatial::Point) {

            static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetPointData()->RemoveArray(d->array->GetName());
            static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetPointData()->AddArray(d->array);
            static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetPointData()->SetActiveVectors(d->array->GetName());

        }

        if(d->field->support() == axlAbstractFieldSpatial::Cell) {
            static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetPointData()->RemoveArray(d->array->GetName());
            static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetCellData()->AddArray(d->array);
            static_cast<vtkPolyData *>(d->mesh->getPolyData())->GetCellData()->SetActiveVectors(d->array->GetName());


        }

        // -- hedgehog

        d->vector_hedgehog = vtkSmartPointer<vtkHedgeHog>::New();
#if (VTK_MAJOR_VERSION <= 5)
        d->vector_hedgehog->SetInput(static_cast<vtkPolyData *>(d->mesh->getPolyData()));
#else
        d->vector_hedgehog->SetInputData(static_cast<vtkPolyData *>(d->mesh->getPolyData()));
#endif
        d->vector_hedgehog->SetVectorModeToUseVector();
        //d->vector_hedgehog->SetScaleFactor(0.05);

        d->vector_hedgehog_mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
        d->vector_hedgehog_mapper->SetInputConnection(d->vector_hedgehog->GetOutputPort());
        d->vector_hedgehog_mapper->ScalarVisibilityOn();

        d->vector_hedgehog_actor = vtkSmartPointer<vtkActor>::New();
        d->vector_hedgehog_actor->SetOrientation(d->mesh->getActor()->GetOrientation());
        d->vector_hedgehog_actor->SetPosition(d->mesh->getActor()->GetPosition());
        d->vector_hedgehog_actor->SetMapper(d->vector_hedgehog_mapper);
        d->vector_hedgehog_actor->SetVisibility(0);


        this->AddPart(d->vector_hedgehog_actor);

        // -- glyphs

        d->vector_glyph_source = vtkArrowSource::New();

        d->vector_glyph = vtkSmartPointer<vtkGlyph3D>::New();
#if (VTK_MAJOR_VERSION <= 5)
        d->vector_glyph->SetInput(static_cast<vtkPolyData *>(d->mesh->getPolyData()));
        d->vector_glyph->SetSource(d->vector_glyph_source->GetOutput());
#else
        d->vector_glyph->SetInputData(static_cast<vtkPolyData *>(d->mesh->getPolyData()));
        d->vector_glyph->SetSourceData(d->vector_glyph_source->GetOutput());
#endif

        d->vector_glyph->SetColorModeToColorByVector();
        d->vector_glyph->SetVectorModeToUseVector();
        //d->vector_glyph->SetScaleModeToScaleByVector();
        d->vector_glyph->SetScaleModeToDataScalingOff();
        //d->vector_glyph->ScalingOff();
        this->setGlyphScale(0.1);
        d->vector_glyph->OrientOn();
        d->vector_glyph->Update();

        d->vector_glyph_mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
        d->vector_glyph_mapper->SetInputConnection(d->vector_glyph->GetOutputPort());

        // vtkSmartPointer<vtkExtractUnstructuredGrid> extractor = vtkSmartPointer<vtkExtractUnstructuredGrid>::New();
        // extractor->SetInput(static_cast<vtkPolyData *>(d->mesh->getPolyData()));

        // vtkSmartPointer<vtkGlyph3DMapper> gpuGlyphMapper = vtkSmartPointer<vtkGlyph3DMapper>::New();
        // gpuGlyphMapper->ScalingOn();
        // gpuGlyphMapper->SetScaleFactor(0.1);
        // gpuGlyphMapper->SetSourceConnection(d->vector_glyph_source->GetOutputPort());
        // gpuGlyphMapper->SetInputConnection(extractor->GetOutputPort());

        d->vector_glyph_actor = vtkSmartPointer<vtkActor>::New();
        d->vector_glyph_actor->SetOrientation(d->mesh->getActor()->GetOrientation());
        d->vector_glyph_actor->SetPosition(d->mesh->getActor()->GetPosition());
        d->vector_glyph_actor->SetMapper(d->vector_glyph_mapper);
        // d->vector_glyph_actor->SetMapper(gpuGlyphMapper);
        d->vector_glyph_actor->SetVisibility(0);

        this->AddPart(d->vector_glyph_actor);

        // -- streams

        d->vector_stream_widget = vtkSmartPointer<vtkSphereWidget2>::New();
        d->vector_stream_widget->SetInteractor(d->interactor);
        d->vector_stream_widget->CreateDefaultRepresentation();
        d->vector_stream_widget->SetTranslationEnabled(true);
        d->vector_stream_widget->SetScalingEnabled(true);
        d->vector_stream_widget->Off();

        d->vector_stream_widget_data = vtkPolyData::New();

        d->vector_stream_widget_representation = vtkSphereRepresentation::SafeDownCast(d->vector_stream_widget->GetRepresentation());
        d->vector_stream_widget_representation->HandleVisibilityOff();
        d->vector_stream_widget_representation->HandleTextOff();
        d->vector_stream_widget_representation->RadialLineOff();
        d->vector_stream_widget_representation->SetPhiResolution(64);
        d->vector_stream_widget_representation->SetThetaResolution(64);
        d->vector_stream_widget_representation->GetPolyData(d->vector_stream_widget_data);

        d->vector_stream_tracer = vtkStreamTracer::New();
#if (VTK_MAJOR_VERSION <= 5)
        d->vector_stream_tracer->SetInput(static_cast<vtkPolyData *>(d->mesh->getPolyData()));
        d->vector_stream_tracer->SetSource(d->vector_stream_widget_data);
#else
        d->vector_stream_tracer->SetInputData(static_cast<vtkPolyData *>(d->mesh->getPolyData()));
        d->vector_stream_tracer->SetSourceData(d->vector_stream_widget_data);
#endif
        d->vector_stream_tracer->SetMaximumPropagation(100);
        d->vector_stream_tracer->SetMinimumIntegrationStep(1.0e-4);
        d->vector_stream_tracer->SetMaximumIntegrationStep(100.0);
        d->vector_stream_tracer->SetIntegrationDirectionToBoth();

        d->vector_stream_filter = vtkTubeFilter::New();
#if (VTK_MAJOR_VERSION <= 5)
        d->vector_stream_filter->SetInput(d->vector_stream_tracer->GetOutput());
#else
        d->vector_stream_filter->SetInputData(d->vector_stream_tracer->GetOutput());
#endif
        d->vector_stream_filter->SetRadius(0.01);
        d->vector_stream_filter->SetNumberOfSides(8);

        d->vector_stream_mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
        d->vector_stream_mapper->SetInputConnection(d->vector_stream_filter->GetOutputPort());
        d->vector_stream_mapper->ScalarVisibilityOn();

        d->vector_stream_actor = vtkSmartPointer<vtkActor>::New();
        d->vector_stream_actor->SetOrientation(d->mesh->getActor()->GetOrientation());
        d->vector_stream_actor->SetPosition(d->mesh->getActor()->GetPosition());
        d->vector_stream_actor->SetMapper(d->vector_stream_mapper);
        d->vector_stream_actor->SetVisibility(0);

        d->vector_stream_observer = axlActorFieldSpatialStreamObserver::New();
        d->vector_stream_observer->vector_stream_widget_representation = d->vector_stream_widget_representation;
        d->vector_stream_observer->vector_stream_widget_data = d->vector_stream_widget_data;
        d->vector_stream_observer->vector_stream_tracer = d->vector_stream_tracer;
        d->vector_stream_widget->AddObserver(vtkCommand::InteractionEvent, d->vector_stream_observer);

        this->AddPart(d->vector_stream_actor);
    }

}

double axlActorFieldSpatial::minValue(void)
{
    double field_range[2];d->array->GetRange(field_range);
    return field_range[0];
}

double axlActorFieldSpatial::maxValue(void)
{
    double field_range[2];d->array->GetRange(field_range);

    return field_range[1];
}

void axlActorFieldSpatial::onIsoRadiusChanged(double radius)
{
    d->isoRadius = radius;
    d->scalar_iso_tube_filter->SetRadius(radius);
    d->scalar_iso_tube_filter->Update();
}
