/* axlActorComposite.cpp ---
 *
 * Author: Meriadeg Perrinel
 * Copyright (C) 2008 - Meriadeg Perrinel, Inria.
 * Created: Fri Dec 17 10:59:21 2010 (+0100)
 * Version: $Id$
 * Last-Updated: Mon Dec 17 15:03:36 2012 (+0100)
 *           By: Julien Wintz
 *     Update #: 28
 */

/* Commentary:
 *
 */

/* Change log:
 *
 */

#include "axlActorComposite.h"
#include "axlActorCone.h"
#include "axlActorCylinder.h"
#include "axlActorTorus.h"
//#include "axlActorCircleArc.h"
#include "axlActorEllipsoid.h"
#include "axlActorFieldDiscrete.h"
#include "axlActorMesh.h"
#include "axlActorLine.h"
#include "axlActorPlane.h"
#include "axlActorPoint.h"
#include "axlActorPointSet.h"
#include "axlActorCurveBSpline.h"
#include "axlActorSphere.h"
#include "axlActorSurfaceBSpline.h"
#include "axlActorVolumeBSpline.h"
#include "axlActorSurfaceTrimmed.h"
#include <axlCore/axlAbstractView.h>
#include <axlCore/axlAbstractDataComposite.h>
#include <axlCore/axlAbstractCurveBSpline.h>
#include <axlCore/axlAbstractSurfaceBSpline.h>
#include <axlCore/axlAbstractVolumeBSpline.h>
#include <axlCore/axlAbstractSurfaceTrimmed.h>
#include <axlCore/axlPoint.h>
#include <axlCore/axlCone.h>
#include <axlCore/axlCylinder.h>
#include <axlCore/axlTorus.h>
//#include <axlCore/axlCircleArc.h>
#include <axlCore/axlEllipsoid.h>
#include <axlCore/axlLine.h>
#include <axlCore/axlPlane.h>
//#include <axlCore/axlPointSet.h>
#include <axlCore/axlMesh.h>
#include <axlCore/axlSphere.h>


#include <vtkActor.h>
#include <vtkAssemblyNode.h>
#include <vtkAssemblyPath.h>
#include <vtkCellArray.h>
#include <vtkCellPicker.h>
#include <vtkDoubleArray.h>
#include <vtkCommand.h>
#include <vtkMatrix4x4.h>
#include <vtkObjectFactory.h>
#include <vtkPoints.h>
#include <vtkPointData.h>
#include <vtkPolyData.h>
#include <vtkPolyDataMapper.h>
#include <vtkProp3DCollection.h>
#include <vtkProperty.h>
#include <vtkRenderer.h>
#include <vtkRendererCollection.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkSmartPointer.h>
#include <vtkTimerLog.h>

class axlActorCompositeObserver : public vtkCommand
{
public:
    static axlActorCompositeObserver *New(void)
    {
        return new axlActorCompositeObserver;
    }

    virtual void Execute(vtkObject *caller, unsigned long event, void *)
    {

    }//execute
};



// /////////////////////////////////////////////////////////////////
// axlActorCompositePrivate
// /////////////////////////////////////////////////////////////////

class axlActorCompositePrivate
{
public:
    vtkSmartPointer<vtkPoints> points;
    vtkSmartPointer<vtkPolyData> polyData;
    vtkSmartPointer<vtkCellArray> cellArray;
    vtkSmartPointer<vtkActor> actor;
    vtkSmartPointer<vtkPolyDataMapper> mapper;
    vtkSmartPointer<vtkRenderWindowInteractor> interactor;
    vtkSmartPointer<vtkCellPicker> CpCursorPicker;
    vtkSmartPointer<axlActorCompositeObserver> observer;
    vtkSmartPointer<vtkRenderer> renderer;

    axlAbstractView *view;
    axlAbstractDataComposite *composite;

    QList<axlAbstractActor *> listCompositeActorChild;

    int State;
};

#if (VTK_MAJOR_VERSION <= 5)
vtkCxxRevisionMacro(axlActorComposite, "$Revision: 0.0.1 $");
#endif

vtkStandardNewMacro(axlActorComposite);

dtkAbstractData *axlActorComposite::data(void)
{
    return d->composite;
}

bool axlActorComposite::removeActorReference(axlAbstractActor *actor)
{
    return d->listCompositeActorChild.removeOne(actor);
}

void axlActorComposite::setData(dtkAbstractData *data1)
{
    axlAbstractDataComposite *data = dynamic_cast<axlAbstractDataComposite *>(data1);
    d->composite = data;

    for (int i = 0; i < d->composite->count(); i++)
    {
        if(axlAbstractDataComposite *currentData = dynamic_cast<axlAbstractDataComposite *>(d->composite->get(i)))
        {
            axlActorComposite *currentComposite = axlActorComposite::New();
            currentComposite->setParent(this);
            currentComposite->setData(currentData);
            d->view->insert(currentData, currentComposite);
            this->AddPart(currentComposite);
            connect(currentData,SIGNAL(samplingChanged()),currentComposite,SLOT(onSamplingChanged()));
            d->listCompositeActorChild << currentComposite;

        }
        else if(axlAbstractCurveBSpline *currentData = dynamic_cast<axlAbstractCurveBSpline *>(d->composite->get(i)))
        {
            axlActorCurveBSpline *currentComposite = axlActorCurveBSpline::New();
            currentComposite->setParent(this);
            currentComposite->setInteractor(d->interactor);
            currentComposite->setData(currentData);
            d->view->insert(currentData, currentComposite);
            this->AddPart(currentComposite);
            connect(currentData,SIGNAL(samplingChanged()),currentComposite,SLOT(onSamplingChanged()));
            d->listCompositeActorChild << currentComposite;
        }
        else if(axlAbstractSurfaceBSpline *currentData = dynamic_cast<axlAbstractSurfaceBSpline *>(d->composite->get(i)))
        {
            axlActorSurfaceBSpline *currentComposite = axlActorSurfaceBSpline ::New();
            currentComposite->setParent(this);
            currentComposite->setInteractor(d->interactor);
            currentComposite->setData(currentData);
            d->view->insert(currentData, currentComposite);
            this->AddPart(currentComposite);
            connect(currentData,SIGNAL(samplingChanged()),currentComposite,SLOT(onSamplingChanged()));
            d->listCompositeActorChild << currentComposite;
        }
        else if(axlAbstractVolumeBSpline *currentData = dynamic_cast<axlAbstractVolumeBSpline *>(d->composite->get(i)))
        {
            axlActorVolumeBSpline *currentComposite = axlActorVolumeBSpline ::New();
            currentComposite->setParent(this);
            currentComposite->setInteractor(d->interactor);
            currentComposite->setData(currentData);
            d->view->insert(currentData, currentComposite);
            this->AddPart(currentComposite);
            connect(currentData,SIGNAL(samplingChanged()),currentComposite,SLOT(onSamplingChanged()));
            d->listCompositeActorChild << currentComposite;
        }
        else if(axlAbstractSurfaceTrimmed *currentData = dynamic_cast<axlAbstractSurfaceTrimmed *>(d->composite->get(i)))
        {
            axlActorSurfaceTrimmed *currentComposite = axlActorSurfaceTrimmed ::New();
            currentComposite->setParent(this);
            currentComposite->setInteractor(d->interactor);
            currentComposite->setSurface(currentData);
            d->view->insert(currentData, currentComposite);
            this->AddPart(currentComposite);
            connect(currentData,SIGNAL(samplingChanged()),currentComposite,SLOT(onSamplingChanged()));
            d->listCompositeActorChild << currentComposite;
        }
        else if(axlCone *currentData = dynamic_cast<axlCone *>(d->composite->get(i)))
        {
            axlActorCone *currentComposite = axlActorCone::New();
            currentComposite->setParent(this);
            currentComposite->setInteractor(d->interactor);
            currentComposite->setData(currentData);
            d->view->insert(currentData, currentComposite);
            this->AddPart(currentComposite);
            d->listCompositeActorChild << currentComposite;

        }
        else if(axlCylinder *currentData = dynamic_cast<axlCylinder *>(d->composite->get(i)))
        {
            axlActorCylinder *currentComposite = axlActorCylinder::New();
            currentComposite->setParent(this);
            currentComposite->setInteractor(d->interactor);
            currentComposite->setData(currentData);
            d->view->insert(currentData, currentComposite);
            this->AddPart(currentComposite);
            d->listCompositeActorChild << currentComposite;

        }
        else if(axlTorus *currentData = dynamic_cast<axlTorus *>(d->composite->get(i)))
        {
            axlActorTorus *currentComposite = axlActorTorus::New();
            currentComposite->setParent(this);
            currentComposite->setInteractor(d->interactor);
            currentComposite->setData(currentData);
            d->view->insert(currentData, currentComposite);
            this->AddPart(currentComposite);
            d->listCompositeActorChild << currentComposite;

        }
//        else if(axlCircleArc *currentData = dynamic_cast<axlCircleArc *>(d->composite->get(i)))
//        {
//            axlActorCircleArc *currentComposite = axlActorCircleArc::New();
//            currentComposite->setParent(this);
//            currentComposite->setInteractor(d->interactor);
//            currentComposite->setCircleArc(currentData);
//            d->view->insert(currentData, currentComposite);
//            this->AddPart(currentComposite);
//            d->listCompositeActorChild << currentComposite;

//        }
        else if(axlEllipsoid *currentData = dynamic_cast<axlEllipsoid *>(d->composite->get(i)))
        {
            axlActorEllipsoid *currentComposite = axlActorEllipsoid::New();
            currentComposite->setParent(this);
            currentComposite->setInteractor(d->interactor);
            currentComposite->setData(currentData);
            d->view->insert(currentData, currentComposite);
            this->AddPart(currentComposite);
            d->listCompositeActorChild << currentComposite;

        }
        else if(axlLine *currentData = dynamic_cast<axlLine *>(d->composite->get(i)))
        {
            axlActorLine *currentComposite = axlActorLine::New();
            currentComposite->setParent(this);
            currentComposite->setInteractor(d->interactor);
            currentComposite->setData(currentData);
            d->view->insert(currentData, currentComposite);
            this->AddPart(currentComposite);
            d->listCompositeActorChild << currentComposite;

        }
        else if(axlPlane *currentData = dynamic_cast<axlPlane *>(d->composite->get(i)))
        {
            axlActorPlane *currentComposite = axlActorPlane::New();
            currentComposite->setParent(this);
            currentComposite->setInteractor(d->interactor);
            currentComposite->setData(currentData);
            d->view->insert(currentData, currentComposite);
            this->AddPart(currentComposite);
            d->listCompositeActorChild << currentComposite;

        }
//        else if(axlPlane *currentData = dynamic_cast<axlPlane *>(d->composite->get(i)))
//        {
//            axlActorPlane *currentComposite = axlActorPlane::New();
//            currentComposite->setParent(this);
//            currentComposite->setInteractor(d->interactor);
//            currentComposite->setData(currentData);
//            d->view->insert(currentData, currentComposite);
//            this->AddPart(currentComposite);
//            d->listCompositeActorChild << currentComposite;

//        }
        else if(axlSphere *currentData = dynamic_cast<axlSphere *>(d->composite->get(i)))
        {
            axlActorSphere *currentComposite = axlActorSphere::New();
            currentComposite->setParent(this);
            currentComposite->setData(currentData);
            currentComposite->setInteractor(d->interactor);

            d->view->insert(currentData, currentComposite);
            this->AddPart(currentComposite);
            d->listCompositeActorChild << currentComposite;

        }
//        else if(axlPointSet *currentData = dynamic_cast<axlPointSet *>(d->composite->get(i)))
//        {
//            axlActorPointSet *currentComposite = axlActorPointSet::New();
//            currentComposite->setParent(this);
//            currentComposite->setPointSet(currentData);
//            d->view->insert(currentData, currentComposite);
//            this->AddPart(currentComposite);
//            d->listCompositeActorChild << currentComposite;
//        }
        else
        {
            axlActorMesh *currentComposite = axlActorMesh::New();
            currentComposite->setParent(this);
            currentComposite->setData(d->composite->get(i));
            d->view->insert(d->composite->get(i), currentComposite);
            this->AddPart(currentComposite);
            d->listCompositeActorChild << currentComposite;
        }
    }
}


void axlActorComposite::onControlPointChanged()
{

    vtkProp3DCollection *actors2 = this->GetParts();
    actors2->InitTraversal();
    for(int i = 0; i<actors2->GetNumberOfPaths(); i++)
    {
        vtkProp *path = actors2->GetNextProp();

        if (path != NULL)
        {
            if(axlAbstractActor *actorProp = dynamic_cast<axlAbstractActor *>(path))
            {
                actorProp->onControlPointChanged();
            }
        }
    }
}
void axlActorComposite::setMode(int state)
{
    this->onModeChanged(state);

    emit stateChanged(this->data(), state);
}


void axlActorComposite::onModeChanged(int state)
{
    if(state == 0)
    {
        this->setState(axlActorComposite::passive);

        vtkSmartPointer<vtkPropCollection> actors = vtkSmartPointer<vtkPropCollection>::New();
        this->GetActors(actors);
        actors->InitTraversal();
        QColor color;
        if(axlAbstractDataComposite *composite = dynamic_cast<axlAbstractDataComposite *>(this->data()))
            color = composite->color();
        else
            color.setRgb(1.0, 1.0, 1.0);

        for(int i = 0; i< actors->GetNumberOfItems(); i++)
        {
            vtkProp *prop = actors->GetNextProp();
            if(vtkActor *actor = dynamic_cast<vtkActor *>(prop))
            {
                actor->GetProperty()->SetColor(color.redF(), color.greenF(), color.blueF());
            }
        }

        // we need to add renderer the axlActorComposite
        // and to remove from the renderer and the  all of the axlActor child of the axlActorComposite
        d->renderer->AddActor(this);

        foreach(axlAbstractActor *abstractActor, d->listCompositeActorChild)
        {
            if(axlActor *actorProp = dynamic_cast<axlActor *>(abstractActor))
            {
                disconnect(actorProp, SIGNAL(stateChanged(dtkAbstractData*,int)), d->view, SIGNAL(stateChanged(dtkAbstractData*,int)));
                d->renderer->RemoveActor(actorProp);
                //      this->AddPart(actorProp);
            }
            if(axlActorComposite *actorProp = dynamic_cast<axlActorComposite *>(abstractActor))
            {
                disconnect(actorProp, SIGNAL(stateChanged(dtkAbstractData*,int)), d->view, SIGNAL(stateChanged(dtkAbstractData*,int)));
                d->renderer->RemoveActor(actorProp);
                // this->AddPart(actorProp);
            }
        }

        //        QList<axlActor *> listActorProp;

        //        vtkProp3DCollection *actors2 = this->GetParts();
        //        actors2->InitTraversal();
        //        for(int i = 0; i<actors2->GetNumberOfPaths(); i++)
        //        {
        //            vtkProp *path = actors2->GetNextProp();

        //            if (path != NULL)
        //            {
        //                if(axlActor *actorProp = dynamic_cast<axlActor *>(path))
        //                {
        //                    disconnect(actorProp, SIGNAL(stateChanged(dtkAbstractData*,int)), d->view, SIGNAL(stateChanged(dtkAbstractData*,int)));
        //                    d->renderer->RemoveActor(actorProp);
        //                    listActorProp << actorProp;
        //                }
        //            }
        //        }

        //        foreach (axlActor *actorProp, listActorProp)
        //            this->AddPart(actorProp);
    }
    else if(state == 1)
    {
        qDebug()<<"avant selection";
        //        mat = this->GetMatrix();
        //        mat->Identity();
        //       vtkMatrix4x4 *mat2 =this->GetMatrix();
        //mat2->Identity();
        //        for(int j = 0; j <4 ; j ++)
        //            for(int i = 0; i <4 ; i ++)
        //                qDebug()<<j<< i << mat2->GetElement(j, i);

        this->setState(axlActorComposite::selection);

        vtkSmartPointer<vtkPropCollection> actors = vtkSmartPointer<vtkPropCollection>::New();
        this->GetActors(actors);
        actors->InitTraversal();

        QColor color;
        if(axlAbstractDataComposite *composite = dynamic_cast<axlAbstractDataComposite *>(this->data()))
        {
            color = composite->color();
            qreal *h = new qreal(0.0);
            qreal *s = new qreal(0.0);
            qreal *l = new qreal(0.0);
            color.getHslF(h, s, l);
            color.setHslF(*h, *s, *l + (1.0 - *l) / 2.0);
        }
        else
            color.setRgb(0.0, 0.0, 1.0);

        for(int i = 0; i< actors->GetNumberOfItems(); i++)
        {
            vtkProp *prop = actors->GetNextProp();
            if(vtkActor *actor = dynamic_cast<vtkActor *>(prop))
                actor->GetProperty()->SetColor(color.redF(), color.greenF(), color.blueF());
        }

        if(!d->renderer->HasViewProp(this))
        {
            d->renderer->AddActor(this);

            foreach(axlAbstractActor *abstractActor, d->listCompositeActorChild)
            {
                if(axlActor *actorProp = dynamic_cast<axlActor *>(abstractActor))
                {
                    disconnect(actorProp, SIGNAL(stateChanged(dtkAbstractData*,int)), d->view, SIGNAL(stateChanged(dtkAbstractData*,int)));
                    d->renderer->RemoveActor(actorProp);
                    //   this->AddPart(actorProp);
                }
                if(axlActorComposite *actorProp = dynamic_cast<axlActorComposite *>(abstractActor))
                {
                    disconnect(actorProp, SIGNAL(stateChanged(dtkAbstractData*,int)), d->view, SIGNAL(stateChanged(dtkAbstractData*,int)));
                    d->renderer->RemoveActor(actorProp);
                    //  this->AddPart(actorProp);
                }
            }

            //                        QList<axlActor *> listActorProp;


            //                        vtkProp3DCollection *actors2 = this->GetParts();
            //                        actors2->InitTraversal();
            //                        for(int i = 0; i<actors2->GetNumberOfPaths(); i++)
            //                        {
            //                            vtkProp *path = actors2->GetNextProp();

            //                            if (path != NULL)
            //                            {
            //                                if(axlActor *actorProp = dynamic_cast<axlActor *>(path))
            //                                {
            //                                    disconnect(actorProp, SIGNAL(stateChanged(dtkAbstractData*,int)), d->view, SIGNAL(stateChanged(dtkAbstractData*,int)));
            //                                    d->renderer->RemoveActor(actorProp);
            //                                    listActorProp << actorProp;
            //                                }
            //                            }
            //                        }

            //                        foreach (axlActor *actorProp, listActorProp)
            //                            this->AddPart(actorProp);
        }
    }
    else if(state == 2)
    {
        this->setState(axlActorComposite::edition);
        vtkSmartPointer<vtkPropCollection> actors = vtkSmartPointer<vtkPropCollection>::New();
        this->GetActors(actors);
        actors->InitTraversal();

        QColor color;
        if(axlAbstractDataComposite *composite = dynamic_cast<axlAbstractDataComposite *>(this->data()))
        {
            color = composite->color();
            qreal *h = new qreal(0.0);
            qreal *s = new qreal(0.0);
            qreal *l = new qreal(0.0);
            color.getHslF(h, s, l);
            color.setHslF(*h, *s, *l + (1.0 - *l) / 2.0);
        }
        else
            color.setRgb(0.0, 0.0, 1.0);

        for(int i = 0; i< actors->GetNumberOfItems(); i++)
        {

            vtkProp *prop = actors->GetNextProp();
            if(vtkActor *actor = dynamic_cast<vtkActor *>(prop))
            {
                actor->GetProperty()->SetColor(color.redF(), color.greenF(), color.blueF());
            }
        };

        //vtkMatrix4x4 *mat = this->GetMatrix();

        qDebug()<<"avant";
        //        for(int j = 0; j <4 ; j ++)
        //            for(int i = 0; i <4 ; i ++)
        //                qDebug()<<j<< i << mat->GetElement(j, i);

        // we need to add renderer all of the axlActor composing the composite
        // and to remove from the renderer the axlActorComposite
        d->renderer->RemoveActor(this);

        vtkProp3DCollection *actors2 = this->GetParts();
        actors2->InitTraversal();

        for(int i = 0 ; i < d->composite->count() ; i ++)
        {
            axlAbstractData *axlData = dynamic_cast<axlAbstractData *>(d->composite->get(i));
            axlData->setColor(d->composite->color());
        }

        foreach(axlAbstractActor *abstractActor, d->listCompositeActorChild)
        {
            if(axlActor *actorProp = dynamic_cast<axlActor *>(abstractActor))
            {
                connect(actorProp, SIGNAL(stateChanged(dtkAbstractData*,int)), d->view, SIGNAL(stateChanged(dtkAbstractData*,int)));
                d->renderer->AddActor(actorProp);
                //this->RemovePart(actorProp);

                //setUp the vtkPropMatrix
                // vtkMatrix4x4 *b = actorProp->GetMatrix();

                //  vtkMatrix4x4::Multiply4x4(mat, b, b);
                //                vtkMatrix4x4 *c = actorProp->GetMatrix();
                //                qDebug()<<"avant";
                //                for(int j = 0; j <4 ; j ++)
                //                    for(int i = 0; i <4 ; i ++)
                //                        qDebug()<<j<< i << b->GetElement(j, i);
                //                qDebug()<<"avant c";
                //                for(int j = 0; j <4 ; j ++)
                //                    for(int i = 0; i <4 ; i ++)
                //                        qDebug()<<j<< i << c->GetElement(j, i);
            }
            if(axlActorComposite *actorProp = dynamic_cast<axlActorComposite *>(abstractActor))
            {
                connect(actorProp, SIGNAL(stateChanged(dtkAbstractData*,int)), d->view, SIGNAL(stateChanged(dtkAbstractData*,int)));
                d->renderer->AddActor(actorProp);
                // this->RemovePart(actorProp);
            }
        }

        //        qDebug()<<"apres edition";
        //        mat = this->GetMatrix();
        //        mat->Identity();
        //        vtkMatrix4x4 *mat2 =this->GetMatrix();
        //mat2->Identity();
        //        for(int j = 0; j <4 ; j ++)
        //            for(int i = 0; i <4 ; i ++)
        //                qDebug()<<j<< i << mat2->GetElement(j, i);


    }
}

void axlActorComposite::setColor(double red, double green, double blue)
{
    Q_UNUSED(red);
    Q_UNUSED(green);
    Q_UNUSED(blue);

    if(axlAbstractData *data = dynamic_cast<axlAbstractData *>(this->data()))
    {
        QColor color = data->color();
        vtkSmartPointer<vtkPropCollection> actors = vtkSmartPointer<vtkPropCollection>::New();
        this->GetActors(actors);
        actors->InitTraversal();

        if(this->getState() == axlActor::passive)
        {
            for(int i = 0; i< actors->GetNumberOfItems(); i++)
            {
                vtkProp *prop = actors->GetNextProp();
                if(vtkActor *actor = dynamic_cast<vtkActor *>(prop))
                {
                    actor->GetProperty()->SetColor(color.redF(), color.greenF(), color.blueF());
                }
            }
        }

        else if(this->getState() == axlActor::selection || this->getState() == axlActor::edition)
        {
            qreal *h = new qreal(0.0);
            qreal *s = new qreal(0.0);
            qreal *l = new qreal(0.0);
            color.getHslF(h, s, l);
            color.setHslF(*h, *s, *l + (1.0 - *l) / 2.0);

            for(int i = 0; i< actors->GetNumberOfItems(); i++)
            {
                vtkProp *prop = actors->GetNextProp();
                if(vtkActor *actor = dynamic_cast<vtkActor *>(prop))
                {
                    actor->GetProperty()->SetColor(color.redF(), color.greenF(), color.blueF());
                }
            }
        }
    }
}

void axlActorComposite::setView(axlAbstractView *view)
{
    d->view = view;
}

void axlActorComposite::setRenderer(vtkRenderer *renderer)
{
    d->renderer = renderer;
}

bool axlActorComposite::isVisible(void)
{
    return (bool)(this->GetVisibility());
}

void axlActorComposite::setInteractor(void *interactor)
{
    d->interactor = static_cast<vtkRenderWindowInteractor *>(interactor);

}

vtkRenderWindowInteractor *axlActorComposite::getInteractor(void)
{
    return d->interactor;
}

void axlActorComposite::NewObserver()
{
    if(d->observer == NULL)
    {
        d->observer = axlActorCompositeObserver::New();

        this->getInteractor()->AddObserver(vtkCommand::LeftButtonPressEvent, d->observer);
        this->getInteractor()->AddObserver(vtkCommand::RightButtonPressEvent, d->observer);
        this->getInteractor()->AddObserver(vtkCommand::InteractionEvent, d->observer);
        this->getInteractor()->AddObserver(vtkCommand::KeyPressEvent, d->observer);
    }
    else
        qDebug()<<"WARNING : You try to create an observer that it's already created";
}

void axlActorComposite::deleteObserver()
{
    if(d->observer != NULL)
    {
        this->getInteractor()->RemoveObservers(vtkCommand::LeftButtonPressEvent, d->observer);
        this->getInteractor()->RemoveObservers(vtkCommand::RightButtonPressEvent, d->observer);
        this->getInteractor()->RemoveObservers(vtkCommand::InteractionEvent, d->observer);
        this->getInteractor()->RemoveObservers(vtkCommand::KeyPressEvent, d->observer);

        d->observer->Delete();
        d->observer = NULL;

    }
    else
        qDebug()<<"WARNING : You try to delete an observer that it's not initialized";

}

int axlActorComposite::getState(void)
{
    return d->State;
}

void axlActorComposite::setState(int state)
{
    d->State = state;
}


void axlActorComposite::setDisplay(bool display)
{
    if(!d->actor) {
        qDebug() << "No tet actor computed for this axlActorComposite";
        return;
    }

    if(display){
        if(!this->GetParts()->IsItemPresent(d->actor)){
            this->VisibilityOn();
        }
    }

    if(!display){
        if (this->GetParts()->IsItemPresent(d->actor)!=0){
            this->VisibilityOff();
        }
    }
}

void axlActorComposite::onSamplingChanged(void)
{
    vtkProp3DCollection *actors2 = this->GetParts();
    actors2->InitTraversal();
    for(int i = 0; i<actors2->GetNumberOfPaths(); i++)
    {
        vtkProp *path = actors2->GetNextProp();

        if (path != NULL)
        {
            if(axlAbstractActor *actorProp = dynamic_cast<axlAbstractActor *>(path))
            {
                actorProp->onSamplingChanged();
            }
        }
    }
}



void axlActorComposite::setOpacity(double opacity)
{
    if(axlAbstractDataComposite *composite = dynamic_cast<axlAbstractDataComposite *>(this->data()))
    {
        double opacity = composite->opacity();

        vtkSmartPointer<vtkPropCollection> actors = vtkSmartPointer<vtkPropCollection>::New();
        this->GetActors(actors);
        actors->InitTraversal();

        for(int i = 0; i< actors->GetNumberOfItems(); i++)
        {
            vtkProp *prop = actors->GetNextProp();
            if(vtkActor *actor = dynamic_cast<vtkActor *>(prop))
            {
                actor->GetProperty()->SetOpacity(opacity);
            }
        }
    }
    this->Modified();
}

void axlActorComposite::setShader(QString xmlfile)
{
    if(QFile::exists(xmlfile))
    {
        vtkSmartPointer<vtkPropCollection> actors = vtkSmartPointer<vtkPropCollection>::New();
        this->GetActors(actors);
        actors->InitTraversal();

        for(int i = 0; i< actors->GetNumberOfItems(); i++)
        {

            vtkProp *prop = actors->GetNextProp();
            if(vtkActor *actor = dynamic_cast<vtkActor *>(prop))
            {
                vtkProperty *prop = actor->GetProperty();
                if(QFile::exists(xmlfile))
                {
                    QString currentShader = dtkReadFile(xmlfile);
                    this->setShaderFromString(currentShader);
                    prop->GetShading();

                }
                else
                {
                    prop->vtkProperty::ShadingOff();
                }
            }
        }

        this->Modified();

    }
}

void axlActorComposite::setShaderFromString(QString xmlfile)
{    vtkSmartPointer<vtkPropCollection> actors = vtkSmartPointer<vtkPropCollection>::New();
     this->GetActors(actors);
     actors->InitTraversal();

      for(int i = 0; i< actors->GetNumberOfItems(); i++)
      {

          vtkProp *prop = actors->GetNextProp();
          if(vtkActor *actor = dynamic_cast<vtkActor *>(prop))
          {
              vtkProperty *prop = actor->GetProperty();
              if(xmlfile != "")
              {
                  //prop->vtkProperty::LoadMaterialFromString(xmlfile.toStdString().c_str());
                  prop->GetShading();
                  prop->vtkProperty::ShadingOn();
              }
              else
              {
                  prop->vtkProperty::ShadingOff();
              }
          }
      }

       this->Modified();
}


// slots---------------------------

void axlActorComposite::hide(void)
{
    this->VisibilityOff();
    this->Modified();
}

void axlActorComposite::show(void)
{
    this->VisibilityOn();
    this->Modified();
}

void axlActorComposite::update(void)
{
    this->Modified();
}


void axlActorComposite::onRemoved(void)
{
    this->getInteractor()->RemoveObservers(vtkCommand::LeftButtonPressEvent, d->observer);
    this->getInteractor()->RemoveObservers(vtkCommand::RightButtonPressEvent, d->observer);
    this->getInteractor()->RemoveObservers(vtkCommand::InteractionEvent, d->observer);
    this->getInteractor()->RemoveObservers(vtkCommand::KeyPressEvent, d->observer);
    this->RemoveObserver(d->observer);

    if(this->getState() == axlActorComposite::edition)
    {
        foreach (axlAbstractActor *abstracActor, d->listCompositeActorChild)
        {
            if(abstracActor)
            {
                if(axlActor *actor = dynamic_cast<axlActor *>(abstracActor))
                    d->renderer->RemoveActor(actor);
                else if(axlActorComposite *actorComposite = dynamic_cast<axlActorComposite *>(abstracActor))
                    d->renderer->RemoveActor(actorComposite);

                d->view->removeData(abstracActor->data());

            }
        }
    }

    foreach (axlAbstractActor *abstracActor, d->listCompositeActorChild)
    {
        if(abstracActor)
        {
            abstracActor->onRemoved();
            if(axlActor *actor = dynamic_cast<axlActor *>(abstracActor))
            {
                if(axlActorBSpline *actorBSlpine = dynamic_cast<axlActorBSpline *>(abstracActor))
                    disconnect(actorBSlpine->data(),SIGNAL(samplingChanged()),actorBSlpine,SLOT(onSamplingChanged()));
                this->RemovePart(actor);
                actor->Delete();
            }
            else if(axlActorComposite *actorComposite = dynamic_cast<axlActorComposite *>(abstracActor))
            {
                this->RemovePart(actorComposite);
                actorComposite->Delete();
            }
            abstracActor=NULL;
            d->listCompositeActorChild.removeOne(abstracActor);
        }
    }

    //this->getActor()->RemoveAllObservers();
    //this->Delete();
}
axlActorComposite::axlActorComposite(void) : axlAbstractActor(),vtkAssembly(), d(new axlActorCompositePrivate)
{
    d->State = axlActorComposite::passive;
    d->CpCursorPicker = vtkSmartPointer<vtkCellPicker>::New();
    d->CpCursorPicker->SetTolerance(0.001);
    d->observer = NULL;
}

axlActorComposite::~axlActorComposite(void)
{
    delete d;

    d = NULL;
}

axlAbstractActor *createAxlActorComposite(void){

    return axlActorComposite::New();
}
