/* axlInspectorObjectVolumeDiscrete.h ---
 *
 * Author: Anais Ducoffe
 * Copyright (C) 2013 - Anais Ducoffe, Inria.

 */

/* Commentary:
 *
 */

/* Change log:
 *
 */

#ifndef AXLINSPECTOROBJECTVOLUMEDISCRETE_H
#define AXLINSPECTOROBJECTVOLUMEDISCRETE_H

#include "axlVtkViewPluginExport.h"

#include <axlGui/axlInspectorObjectFactory.h>

#include <QtWidgets>

class axlVolume;
class axlAbstractView;
class axlInspectorObjectVolumeDiscretePrivate;
class dtkAbstractData;

class AXLVTKVIEWPLUGIN_EXPORT axlInspectorObjectVolumeDiscrete : public axlInspectorObjectInterface
{
    Q_OBJECT

public:
    axlInspectorObjectVolumeDiscrete(QWidget *parent = 0);
    ~axlInspectorObjectVolumeDiscrete(void);

    static bool registered(void);

public slots:
    void setData(dtkAbstractData *data);

private:
    axlInspectorObjectVolumeDiscretePrivate *d;
};

axlInspectorObjectInterface *createaxlVolumeDiscreteDialog(void);

#endif // AXLINSPECTOROBJECTVOLUMEDISCRETE_H
