
How to open the view inspector
==============================

After opening axel, click on the Inspector button on the top (or type Ctrl-i) and then click on 'view' option.

.. image:: ../img/content_inspector2.png 
   :align: center
      
Another possibility is to use keybord shortcuts, type Ctrl + i then Ctrl + 3 .

Changing the background color
=============================

You can change the color of the background by changing the hexadecimal value of the color

.. image:: ../img/content_color1.png
   :align: center 
                                                  

Here you can see that the value changed 

.. image:: ../img/content_color2.png                                                         
   :align: center 


Configuring the spotlight
=========================
By default, Axl uses 5 spotlight directions but you can disable that and choose only one spotlight, you just need to uncheck the "use default light" option as shown here.

.. image:: ../img/content_spotlight1.png                                                    
   :align: center 

                                                
As you see you can also hide the spotlight from the screen.

Changing the spotlight position
-------------------------------
Axl enables you to choose the position of the point from which the light is spread out but keeps it always in the direction of the object you are working with. You can do it either by changing the values of the coordinates of the point or by moving it with your mouse.

.. image:: ../img/content_spotlight2.png                                                           
   :align: center 
                                                     

Changing the Color of the light and the projection
--------------------------------------------------

By default, the spotlight diffuses a white light but you can also choose its color by changing the RGB values as shown in this picture.

.. image:: ../img/content_spotlight3.png                                                           
   :align: center 
    

Configuring the Camera
======================
Projection
----------
You can choose the type of projection of your camera, you have the choice between perspective and parallel projection.

.. image:: ../img/content_camera1.png                                                                
   :align: center 
                                                              

Camera Position
---------------
By default you can change the point of view of your objects simply by moving your mouse and you can also choose the axes from which the camera is viewing.

.. image:: ../img/content_camera2.png  
   :align: center
	   
In the folowing picture we choose the Z direction.

.. image:: ../img/content_camera3.png  
   :align: center 


Some other Options
==================
The grid option enables you to make a grid on a plan associated with the axes you want to take and included in the boundingbox of your object. For instance in the following picture, we check "Y" and "Z" to get two grids in the plan formed by Y and Z and the second is formed by Z and X.

.. image:: ../img/content_grid3.png
   :align: center 

In order to view in deep the sampling of the grid, click on the principal screen then type W, you can change the sampling of the grid simply by moving the scroll.

.. image:: ../img/content_grid4.png                                                     
   :align: center 

If you want to return to the normal viewing just click on the principal screen and type S.

Finally the last option gives you the possibility to hide the axes from the screen.

.. image:: ../img/content_hideaxe.png
   :align: center 
                                            
