Using Axl from another application
===================================

In this section, we illustrate how Axl library and data can be used from an external application. The corresponding
file is ``axl/test/axlExpl/torus.cpp``.

Header files
------------

To define the plugin manager and the objects to be used, some header files need to be included:

.. literalinclude:: ../../../test/axlExpl/torus.cpp 
   :language: cpp 
   :lines: 1-7

The file ``axl-config.h`` define the macro ``AXL_PLUGIN_DIR`` corresponding to the folder of the plugins and
``AXL_DATA_DIR`` corresponding to the folder of the data.

Loading the plugins
-------------------

We set up the plugin manager and load the plugins:

.. literalinclude:: ../../../test/axlExpl/torus.cpp 
   :language: cpp 
   :lines: 11-15


Reading data
------------

We read the file ``torus.axl`` from the folder ``AXL_DATA_DIR``:

.. literalinclude:: ../../../test/axlExpl/torus.cpp 
   :language: cpp 
   :lines: 17-21

The list of objects, which have been read, is ``list``.

Manipulating data
-----------------

We output the description of the first object in the read list, which should be a torus. We cast it into an ``axlTorus`` and get its center. The color of the center point and its size are changed:

.. literalinclude:: ../../../test/axlExpl/torus.cpp 
   :language: cpp 
   :lines: 23-31
	   
Saving data
-----------

In the last steps, we save all the objects, which have been read and the center point in the file ``tmp.axl``:

.. literalinclude:: ../../../test/axlExpl/torus.cpp 
   :language: cpp 
   :lines: 33-37
