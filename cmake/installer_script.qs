function Component()
{
}

Component.prototype.createOperations = function()
{
    component.createOperations();
    component.addOperation("Replace",
		           installer.value("TargetDir")+"/bin/mmk",
   		           "[INSTALLER_TARGET_DIR]",
			   installer.value ("TargetDir"));
    component.addOperation("Replace",
		           installer.value("TargetDir")+"/lib/cmake/AxlConfig.cmake",
   		           "[INSTALLER_TARGET_DIR]",
			   installer.value ("TargetDir"));
    component.addOperation("Replace",
		           installer.value("TargetDir")+"/include/axl-config.h",
   		           "[INSTALLER_TARGET_DIR]",
			   installer.value ("TargetDir"));
    component.addOperation("Replace",
                   installer.value("TargetDir")+"/bin/axl${EXE_SUFFIX}",
   		           "[INSTALLER_TARGET_DIR]",
			   installer.value ("TargetDir"));
}

