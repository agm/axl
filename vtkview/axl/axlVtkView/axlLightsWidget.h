/* axlLightsWidget.h ---
 *
 * Author: Meriadeg Perrinel
 * Copyright (C) 2008 - Meriadeg Perrinel, Inria.
 * Created: Fri Feb 18 17:25:39 2011 (+0100)
 * Version: $Id$
 * Last-Updated: Wed Mar 16 21:46:46 2011 (+0100)
 *           By: Meriadeg Perrinel
 *     Update #: 32
 */

/* Commentary:
 *
 */

/* Change log:
 *
 */

#ifndef AXLLIGHSSWIDGET_H
#define AXLLIGHSSWIDGET_H

#include <vtk3DWidget.h>

#include "axlVtkViewPluginExport.h"

class axlAbstractView;

class axlLightsWidgetPrivate;

class vtkActor;
class vtkActorCollection;
class vtkCellPicker;
class vtkPlanes;
class vtkPoints;
class vtkPolyData;
class vtkPolyDataMapper;
class vtkProp;
class vtkProperty;
class vtkSphereSource;
class vtkTransform;

class AXLVTKVIEWPLUGIN_EXPORT axlLightsWidget : public vtk3DWidget
{

public:
    static axlLightsWidget *New();

    vtkTypeMacro(axlLightsWidget,vtk3DWidget);

    void PrintSelf(ostream& os, vtkIndent indent);

    void PlaceWidget(double bounds[6]);
    void PlaceWidget(void) {
        this->Superclass::PlaceWidget();
    }
    void PlaceWidget(double xmin, double xmax, double ymin, double ymax, double zmin, double zmax) {
        this->Superclass::PlaceWidget(xmin,xmax,ymin,ymax,zmin,zmax);
    }

    void SetEnabled(int);

    void initializePoints(void);
    void resetProperty(void);

    void update();

    vtkActor           *netActor(void);
    vtkActorCollection *ptsActors(void);


public:
    void setView(axlAbstractView *view);


protected:
    axlLightsWidget();
    ~axlLightsWidget(void);

    int State;

    enum WidgetState {
        Start,
        Moving,
        Scaling,
        Translating,
        Outside
    };


    int HighlightHandle(vtkProp *prop);

    static void ProcessEvents(vtkObject* object, unsigned long event, void* clientdata, void* calldata);

    virtual void OnMouseMove(void);
    virtual void OnLeftButtonDown(void);
    virtual void OnLeftButtonUp(void);

    void CreateDefaultProperties(void);


private:
    axlLightsWidget(const axlLightsWidget&); // Not implemented
    void operator=(const axlLightsWidget&); // Not implemented

private:
    axlLightsWidgetPrivate *d;
};

#endif //AXLLIGHSSWIDGET_H
