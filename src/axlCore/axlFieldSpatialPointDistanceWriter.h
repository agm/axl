/* axlFieldSpatialPointDistanceWriter.h ---
 *
 * Author: Anais Ducoffe
 * Copyright (C) 2013 - Anais Ducoffe, Inria.
 */

/* Commentary:
 *
 */

/* Change log:
 *
 */

#ifndef AXLFIELDSPATIALPOINTDISTANCEWRITER_H
#define AXLFIELDSPATIALPOINTDISTANCEWRITER_H

#include <axlCore/axlAbstractDataWriter.h>
#include "axlCoreExport.h"


class dtkAbstractData;

class AXLCORE_EXPORT axlFieldSpatialPointDistanceWriter : public axlAbstractDataWriter
{
    Q_OBJECT

public :
      axlFieldSpatialPointDistanceWriter(void);
     ~axlFieldSpatialPointDistanceWriter(void);

public:
    QString identifier(void) const;
    QString description(void) const;
    QStringList handled(void) const;

    static bool registered(void);

public:
    bool accept(dtkAbstractData *data);
    bool reject(dtkAbstractData *data);

    QDomElement write(QDomDocument *doc, dtkAbstractData *data);

private :
    QDomElement elementByWriter(axlAbstractDataWriter *axl_writer, QDomDocument *doc, dtkAbstractData *data);
};

dtkAbstractDataWriter *createaxlFieldSpatialPointDistanceWriter(void);

#endif  // AXLFIELDSPATIALPOINTDISTANCEWRITER_H
