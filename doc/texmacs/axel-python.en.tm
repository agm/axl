<TeXmacs|1.99.1>

<style|<tuple|mmxdoc|mathemagix|doxygen>>

<\body>
  <doc-data|<doc-title|How to call <name|Axl> from a python application>>

  Thanks to a <samp|JSON RPC> protocol, using the library <samp|qjsonrpc>,
  <name|Axl> can be called from a python application. For further
  information about what's a <samp|JSON RPC> protocol you can take a look
  here.

  This document explains how you can use this functionality.

  <section|How I can get this functionality ?>

  This protocol was added in the version 2.3.2 of <name|Axl>. If you get the
  sources you must have installed <samp|qjsonrpc> and put the cmake variable
  <samp|JSON_RPC> to <samp|ON>. That's way Axl embeds a server which listens
  to all messages send by axlClient data.

  <section|Requirements>

  To communicate with <name|Axl>, the python application must use a client
  to send messages to the <name|Axl> server. The client must be of type
  axlClient defined in <name|Axl> kernel application. To use those kind of
  data you should import some part of <name|Axl> such as described below:

  <\session|python|default>
    <\input|Python] >
      import sys\ 

      sys.path.append("your_path_to_axel_modules")\ 

      <with|color|red|# To get some axel data >

      import axlcore\ 

      from axlcore import *\ 

      <with|color|red|# To get axlClient methods >

      import axlrpc\ 

      from axlrpc import *
    </input>
  </session>

  You'll find <name|Axl> modules folder in the build folder of <name|Axl>
  if you are using sources. Otherwise it must be installed on your computer
  system, for instance <samp|C:\\Program Files (x86)\\axel-2.3.2\\modules> on
  Windows. You should also initialize factories for getting Axl plugins data
  and processes.

  <\session|python|default>
    <\input>
      Python]\ 
    <|input>
      <with|color|red|#Initialize factories for plugins>

      dtkPluginManager = axlcore.dtkPluginManager.instance()
      dtkPluginManager.initializeApplication()\ 

      dtkPluginManager.initialize()\ 

      dtkDataFactory = axlcore.dtkAbstractDataFactory.instance()\ 

      dtkProcessFactory = axlcore.dtkAbstractProcessFactory.instance()
    </input>
  </session>

  A more comprehensive example is given in section 4.

  Once <verbatim|axel-sdk> has been configured, \ the file <verbatim|axel.py>
  is generated in the build directory. It contains the previous instructions
  with the adequate corresponding variables. To set up the axel environment,
  one can use the following command:

  <\session|python|default>
    <\input>
      Python]\ 
    <|input>
      from axel import *
    </input>
  </session>

  The following variables are accessible:

  <\itemize>
    <item><verbatim|axel_dir>: name of the folder which contains the module

    <item><verbatim|axel_app>: complete path of the application
    <verbatim|axel>.\ 
  </itemize>

  <section|What kind of communication is possible ?>

  The communication is bi-directionnal as you can get data created in Axl
  with your python application and sending python application data to Axl
  view. Your python application can also ask Axl to compute an axel
  algorithm/process, with specific inputs and parameters, and getting the
  result(s).

  <subsubsection|Sending data to the Axl View>

  <\itemize>
    <item><samp|sendData(axlAbstractData *data)>, send the object in the Axl
    view.\ 

    <item><samp|modifyData(axlAbstractData *data)>, modify the properties of
    the object in Axl view.

    <item><samp|deleteData(axlAbstractData *data)>, delete the object
    representation in Axl view.
  </itemize>

  <subsubsection|Getting data from Axl>

  <\itemize>
    <item><samp|update(axlAbstractData *data)>, update the properties of the
    corresponding object in the python application. If the object was deleted
    in Axl view delete it.\ 

    <item> <samp|getData(QString name)>, an object was created in Axl,
    create the corresponding object in python application.
  </itemize>

  <subsubsection|Asking Axl to compute an algorithm>

  <\itemize>
    <item><samp|callProcess("processName",listInput, listParam)>, Axl
    computes the algorithm with the inputs list and parameters given. Returns
    the output. ListInput and listParameter are of type
    <samp|axlAbstractDataComposite>.
  </itemize>

  <section|A python script example>

  <\session|python|default>
    <\unfolded-io|Python] >
      #!/usr/bin/python\ 

      from axel import *

      \;

      <with|color|red|# To run Axl. Be careful sometimes Axl open too
      late.>\ 

      <with|color|red|# You should run Axl apart if so. >

      <with|color|red|# or wait more than 3 seconds by changins sleep 5 with
      another value.>\ 

      pid = subprocess.Popen([axel_app , "--verbose"],shell=True)\ 

      p = subprocess.Popen('sleep 3', shell=True)

      <with|color|red|# to wait axel opening.>

      p.wait()

      \;

      <with|color|red|# Create a client to communicate with Axl.>

      c = axlClient()

      \;

      <with|color|red|# Create an axel point>

      point = axlPoint(2,6,5)\ 

      print point.description()\ 

      <with|color|red|# Send it to the Axl view. >

      c.sendData(point)\ 

      <with|color|red|# Modify the coordinates >

      point.setValues(1.8,2.5,0.0)\ 

      print point.description()\ 

      <with|color|red|# Communicate the new values to Axl.>\ 

      c.modifyData(point)\ 

      \;

      <with|color|red|# If a new object was created in Axl, with name
      axlCone,>\ 

      <with|color|red|# the python application can get it.>\ 

      # c.getData("axlCone")\ 

      <with|color|red|# Suppose an axlCone de nom axlCore is created from
      Axl.>\ 

      <with|color|red|# Delete the point representation in Axl view.>\ 

      # c.deleteData(point)

      \;

      <with|color|red|# Here is an example to explain how to call a process
      in Axl and >

      <with|color|red|# get the result. The process called \ must be
      initialized and>\ 

      <with|color|red|# exist in Axl not in python application.>\ 

      point1 = axlPoint(1.5,0.5,2)

      point2 = axlPoint(2,0,2)\ 

      line = axlLine(point1,point2)\ 

      listInput = axlAbstractDataComposite()\ 

      listInput.add(line)\ 

      valueParam = axlDouble(0.25)\ 

      listParam = axlAbstractDataComposite();\ 

      listParam.add(valueParam);\ 

      \;

      <with|color|red|# Method to call process with specific inputs and
      parameters.>\ 

      data = c.callProcess("axlBarycenterProcess",listInput, listParam )\ 

      print data.description()

      \;

      p2 = subprocess.Popen('sleep 5', shell=True)\ 

      p2.wait()
    <|unfolded-io>
      \;
    </unfolded-io>

    <\input|Python] >
      \;
    </input>
  </session>
</body>

<initial|<\collection>
</collection>>