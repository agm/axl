/* axlInspectorActorFactory.h ---
 *
 * Author: Anais Ducoffe
 * Copyright (C) 2013 - Anais Ducoffe, Inria.
 */

/* Commentary:
 *
 */

/* Change log:
 *
 */


#ifndef AXLINSPECTORACTORFACTORY_H
#define AXLINSPECTORACTORFACTORY_H

#include "axlVtkViewPluginExport.h"

#include <axlCore/axlAbstractActor.h>

#include <QtWidgets>

class axlInspectorActorFactory;

// /////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////

class AXLVTKVIEWPLUGIN_EXPORT axlInspectorActorInterface : public QFrame
{
    Q_OBJECT
public:
    axlInspectorActorInterface(QWidget *parent = 0);
    virtual ~axlInspectorActorInterface(void);

    void setActorFactorySingleton(axlInspectorActorFactory *actorFactorySingleton);
    axlInspectorActorFactory *actorFactorySingleton(void);

public slots:
    virtual void setData(axlAbstractActor *data);

protected :
    axlInspectorActorFactory *m_actorFactorySingleton ;
};

// /////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////

class axlInspectorActorFactoryPrivate;

class AXLVTKVIEWPLUGIN_EXPORT axlInspectorActorFactory : public QObject
{
    Q_OBJECT

public:
    typedef axlAbstractActor *(*axlInspectorActorCreator)(void);

    typedef QHash<QString, axlInspectorActorCreator> axlInspectorActorCreatorHash;

public:
    static axlInspectorActorFactory *instance(void);

    bool registerInspectorActor(const QString& interface_name, axlInspectorActorCreator func);

    axlAbstractActor *create(const QString& interface_name);
    void initialize(void);

protected:
    static axlInspectorActorFactory *s_instance;

private:
    axlInspectorActorFactory(void);
    ~axlInspectorActorFactory(void);

private:
    axlInspectorActorFactoryPrivate *d;
};

#endif // AXLINSPECTORACTORFACTORY_H
