/* axlActorFieldParametric.h ---
 *
 * Author: Anais Ducoffe
 * Copyright (C) 2013- Anais Ducoffe, Inria.
 */

/* Commentary:
 *
 */

/* Change log:
 *
 */

#ifndef AXLACTORFIELDPARAMETRIC_H
#define AXLACTORFIELDPARAMETRIC_H

#include "axlVtkViewPluginExport.h"

#include "axlCore/axlAbstractActorField.h"

#include <vtkAssembly.h>
#include <vtkVersion.h>
#include <vtkScalarBarActor.h>

class axlAbstractField;
class axlActor;
class axlActorFieldParametricPrivate;

class AXLVTKVIEWPLUGIN_EXPORT axlActorFieldParametric : public axlAbstractActorField, public vtkAssembly
{
public:
#if (VTK_MAJOR_VERSION <= 5)
    vtkTypeRevisionMacro(axlActorFieldParametric, vtkAssembly);
#endif

    static axlActorFieldParametric *New(void);

public:

    dtkAbstractData *data(void);
    axlAbstractField *field(void);
    axlAbstractField *magnitude(void);
    axlAbstractActor *actorField(void);

    ////scalar bar actor
    vtkScalarBarActor *scalarBar(void);

    double colRangeMin(void);
    double colRangeMax(void);
    double maxValue(void);
    double minValue(void);

    int isoCount(void);
    double isoRangeMin(void);
    double isoRangeMax(void);

    double glyphScale(void);

    double streamRadius(void);

    void setInteractor(void *interactor);
    virtual void setData(dtkAbstractData *field);
    void setActorField(axlAbstractActor *actor);

    void setColRangeMin(double min);
    void setColRangeMax(double max);

    void setIsoCount(int count);
    void setIsoRangeMin(double min);
    void setIsoRangeMax(double max);

    void setGlyphScale(double scale);

    void setStreamPropagation(double propagation);
    void setStreamRadius(double radius);
    void setStreamDirection(int direction);

    void setActiveFieldKind(void);
    void updateArray(void);


public slots:
    void onUpdateGeometry(void) {};

    void displayAsColor(void);
    void displayAsIso(void);
    void displayAsNoneScalar(void);

    void displayAsNoneVector(void);
    void displayAsHedge(void);
    void displayAsGlyph(void);
    void displayAsStream(void);

    void onIsoRadiusChanged(double radius);

    void update(void);

public:
    void *scalarColorMapper(void);

//protected:
public :
     axlActorFieldParametric(void);
    ~axlActorFieldParametric(void);

    void setup(void);

private:
    axlActorFieldParametricPrivate *d;
};

#endif // AXLACTORFIELDPARAMETRIC_H
