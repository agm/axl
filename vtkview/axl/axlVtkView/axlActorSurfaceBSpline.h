/* axlActorSurfaceBSpline.h ---
 *
 * Author: Meriadeg Perrinel
 * Copyright (C) 2008 - Meriadeg Perrinel, Inria.
 * Created: Fri Dec 17 11:01:52 2010 (+0100)
 * Version: $Id$
 * Last-Updated: Mon Dec 17 13:58:43 2012 (+0100)
 *           By: Julien Wintz
 *     Update #: 19
 */

/* Commentary:
 *
 */

/* Change log:
 *
 */

#ifndef AXLACTORSURFACEBSPLINE_H
#define AXLACTORSURFACEBSPLINE_H

#include "axlActorBSpline.h"

#include "axlVtkViewPluginExport.h"

#include <vtkVersion.h>

class axlAbstractSurfaceBSpline;
class axlActorSurfaceBSplinePrivate;

class AXLVTKVIEWPLUGIN_EXPORT axlActorSurfaceBSpline : public axlActorBSpline
{
public:
#if (VTK_MAJOR_VERSION <= 5)
    vtkTypeRevisionMacro(axlActorSurfaceBSpline, vtkAssembly);
#endif

    static axlActorSurfaceBSpline *New(void);

public:
    dtkAbstractData *data(void);

public:
    virtual void setData(dtkAbstractData *spline_Surface1);
    void setMapperCollorArray(void);
    void meshProcess(void);
    void normalsUpdate(void);
    void polyDataUpdate(void);
    void pointsUpdate(void);

public slots:

    // re-implemented from axlActorBSpline
    virtual void onUpdateGeometry(void);

    virtual void onSamplingChanged(void);
    void onSelectBoundaryEdge(int numEdge, int previous, int n);

protected:
    axlActorSurfaceBSpline(void);
    ~axlActorSurfaceBSpline(void);

private:
    axlActorSurfaceBSpline(const axlActorSurfaceBSpline&); // Not implemented.
    void operator = (const axlActorSurfaceBSpline&); // Not implemented.

private:
    axlActorSurfaceBSplinePrivate *d;
};

axlAbstractActor *createAxlActorSurfaceBSpline(void);

#endif
