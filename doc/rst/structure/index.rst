=====================
The structure of Axl
=====================

The Axl Software Development Kit (SDK) contains

  -   the package ``axel``, which provides the geometric modeling components and the graphical interface based on QT.
  -   the library ``dtk``, which provides generic tools for data, processes and views.
  -    the vizualisation plugin ``vtkview``, based on VTK.

       
.. toctree::
   :maxdepth: 1
	      
   reader_writer
   field
   process_dialog
   dynamic_data 
   signals
   abstract_data
   control_points
   axel_test
   structure_plugin 
