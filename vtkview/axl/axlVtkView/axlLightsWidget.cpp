/* axlLightsWidget.Lightp ---
 *
 * Author: Meriadeg Perrinel
 * Copyright (C) 2008 - Meriadeg Perrinel, Inria.
 * Created: Fri Feb 18 17:29:04 2011 (+0100)
 * Version: $Id$
 * Last-Updated: Mon Dec 17 14:40:53 2012 (+0100)
 *           By: Julien Wintz
 *     Update #: 123
 */

/* Commentary:
 *
 */

/* Change log:
 *
 */

#include "axlActorCurveBSpline.h"
#include "axlActorSurfaceBSpline.h"
#include "axlLightsWidget.h"
#include "axlInteractorStyleRubberBandPick.h"

#include <axlCore/axlPoint.h>
#include <axlCore/axlAbstractView.h>

#include <vtkActor.h>
#include <vtkAssemblyNode.h>
#include <vtkAssemblyPath.h>
#include <vtkCallbackCommand.h>
#include <vtkCamera.h>
#include <vtkCellArray.h>
#include <vtkCellPicker.h>
#include <vtkAreaPicker.h>
#include <vtkDoubleArray.h>
#include <vtkFloatArray.h>
#include <vtkLine.h>
#include <vtkLight.h>
#include <vtkLightActor.h>
#include <vtkLightCollection.h>
#include <vtkObjectFactory.h>
#include <vtkPlanes.h>
#include <vtkPoints.h>
#include <vtkPolyData.h>
#include <vtkPolyDataMapper.h>
#include <vtkProperty.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkRenderer.h>
#include <vtkRendererCollection.h>
#include <vtkRenderWindow.h>
#include <vtkSphereSource.h>
#include <vtkTransform.h>
#include <vtkGlyph3D.h>
#include <vtkSmartPointer.h>
#include <vtkTransform.h>
#include <vtkCursor3D.h>
#include <vtkProp3DCollection.h>
#include <vtkProp.h>
#include <vtkPropAssembly.h>
#include <vtkCollectionIterator.h>
#include <vtkCommand.h>

#include <vtkRenderWindow.h>

vtkStandardNewMacro(axlLightsWidget);

class axlLightsWidgetPrivate
{
public:
    vtkActor          **LightActor;
    vtkLightActor     **LightProp;
    vtkPolyDataMapper **LightMapper;
    vtkSphereSource   **LightSphere;
    //vtkActor          *LightCurrentActor;
    vtkActor     *LightCurrentActor;
    int               LightCurrentActorIndex;
    vtkPoints         *LightPoints;  //used by others as well
    vtkCellPicker     *LightCursorPicker;
    vtkAreaPicker     *LightRectanglePicker;
    axlInteractorStyleRubberBandPick *LightRubberBanPick;
    vtkProperty       *LightHandleProperty;
    vtkProperty       *LightSelectedHandleProperty;

    vtkAssemblyNode   *LightCurrentAssemblyNode;

    vtkActorCollection *LightActorCollection;

    int                NbLight;

    // rectangle behavior
    int                XStartPosition;
    int                YStartPosition;



    //the 3dCursor
    vtkPolyDataMapper *LightCursorMapper;
    vtkActor          *LightCursorActor;

    axlAbstractView *view;

};

axlLightsWidget::axlLightsWidget() :d(new axlLightsWidgetPrivate)
{
    this->State = axlLightsWidget::Start;
    this->EventCallbackCommand->SetCallback(axlLightsWidget::ProcessEvents);
    // Set up the initial properties
    this->CreateDefaultProperties();

    d->NbLight = 0;

    // connection for the cursor
    d->LightCursorMapper = vtkPolyDataMapper::New();
    d->LightCursorActor = vtkActor::New();
    d->LightCursorActor->VisibilityOff();
    d->LightCursorActor->SetMapper(d->LightCursorMapper);

    // Define the point coordinates
    double bounds[6];
    bounds[0] = -0.5;
    bounds[1] = 0.5;
    bounds[2] = -0.5;
    bounds[3] = 0.5;
    bounds[4] = -0.5;
    bounds[5] = 0.5;

    d->XStartPosition =-1;
    d->YStartPosition =-1;

    this->PlaceWidget(bounds);
}

axlLightsWidget::~axlLightsWidget()
{
    delete d;

    d = NULL;
}

void axlLightsWidget::setView(axlAbstractView *view)
{
    d->view = view;
}

void axlLightsWidget::initializePoints()
{
    vtkRenderWindow *renderWindow = this->Interactor->GetRenderWindow();
    vtkRendererCollection * rendererCollection = renderWindow->GetRenderers();
    vtkRenderer *render = rendererCollection->GetFirstRenderer();

    vtkLightCollection *lightCollection = render->GetLights();
    d->NbLight = lightCollection->GetNumberOfItems();
    lightCollection->InitTraversal();
    //vtkLight *currentLight = lightCollection->GetNextItem();

    // Construct initial points CellAray and PolyData
    d->LightPoints = vtkPoints::New();

    //Manage the picking stuff
    d->LightCursorPicker = vtkCellPicker::New();
    d->LightRectanglePicker = vtkAreaPicker::New();
    d->LightRubberBanPick  = dynamic_cast<axlInteractorStyleRubberBandPick *>(this->Interactor->GetInteractorStyle());//axlInteractorStyleRubberBandPick::New();
    this->Interactor->SetPicker(d->LightRectanglePicker);

    d->LightMapper = new vtkPolyDataMapper *[d->NbLight];
    d->LightSphere = new vtkSphereSource *[d->NbLight];
    d->LightActor = new vtkActor *[d->NbLight];
    d->LightProp = new vtkLightActor *[d->NbLight];

    d->LightActorCollection = vtkActorCollection::New();

    d->LightPoints->SetNumberOfPoints(d->NbLight);

    vtkLight *currentLight = NULL;

    d->LightCursorPicker->SetTolerance(0.01);

    for (int i = 0; i < d->NbLight; i++)
    {
        currentLight = lightCollection->GetNextItem();
        //storage of points
        d->LightPoints->SetPoint(i, currentLight->GetPosition());

        // Construct the poly data representing Lights points
        d->LightMapper[i] = vtkPolyDataMapper::New();
        d->LightSphere[i] =vtkSphereSource::New();
        d->LightSphere[i]->SetPhiResolution(15);
        d->LightSphere[i]->SetThetaResolution(15);
        d->LightSphere[i]->SetRadius(0.01);
        d->LightSphere[i]->SetCenter(d->LightPoints->GetPoint(i));
#if (VTK_MAJOR_VERSION <= 5)
        d->LightMapper[i]->SetInput( d->LightSphere[i]->GetOutput());
#else
        d->LightMapper[i]->SetInputData( d->LightSphere[i]->GetOutput());
#endif
        d->LightActor[i] = vtkActor::New();

        d->LightActor[i]->SetMapper(d->LightMapper[i]);
        d->LightActor[i]->VisibilityOn();

        d->LightActorCollection->AddItem(d->LightActor[i]);


        d->LightProp[i] = vtkLightActor::New();
        d->LightProp[i]->SetLight(currentLight);
        d->LightProp[i]->SetPosition(currentLight->GetPosition());

        // TO DO : ADD TO RENDERER THE LIGHT
        render->AddActor(d->LightProp[i]);
    }
}


void axlLightsWidget::resetProperty(void)
{
    for (int i = 0; i <d->NbLight; i++)
        d->LightActor[i]->SetProperty(d->LightHandleProperty);
}


vtkActorCollection *axlLightsWidget::ptsActors(void)
{
    return d->LightActorCollection;
}

void axlLightsWidget::SetEnabled(int enabling)
{

    if ( ! this->Interactor )
    {
        vtkErrorMacro(<<"The interactor must be set prior to enabling/disabling widget");
        return;
    }

    if ( enabling ) //------------------------------------------------------------
    {
        vtkDebugMacro(<<"Enabling widget");

        if ( this->Enabled ) //already enabled, just return
        {
            return;
        }

        if ( ! this->CurrentRenderer )
        {
            this->SetCurrentRenderer(this->Interactor->FindPokedRenderer(
                                         this->Interactor->GetLastEventPosition()[0],
                                         this->Interactor->GetLastEventPosition()[1]));
            if (this->CurrentRenderer == NULL)
            {
                return;
            }
        }

        this->Enabled = 1;

        // listen to the following events
        vtkRenderWindowInteractor *i = this->Interactor;
        i->AddObserver(vtkCommand::MouseMoveEvent, this->EventCallbackCommand,
                       this->Priority);
        i->AddObserver(vtkCommand::LeftButtonPressEvent,
                       this->EventCallbackCommand, this->Priority);
        i->AddObserver(vtkCommand::LeftButtonReleaseEvent,
                       this->EventCallbackCommand, this->Priority);


        for (int i = 0; i <d->NbLight; i++)
        {
            d->LightActor[i]->SetVisibility(true);
            d->LightProp[i]->SetVisibility(true);
        }

        d->LightCurrentActorIndex =-1;
        d->XStartPosition =-1;
        d->YStartPosition =-1;


        this->InvokeEvent(vtkCommand::EnableEvent,NULL);
    }

    else //disabling-------------------------------------------------------------
    {
        vtkDebugMacro(<<"Disabling widget");

        if ( ! this->Enabled ) //already disabled, just return
        {
            return;
        }

        this->Enabled = 0;

        // don't listen for events any more
        this->Interactor->RemoveObserver(this->EventCallbackCommand);


        for (int i = 0; i <d->NbLight; i++)
        {
            d->LightActor[i]->SetVisibility(false);
            d->LightProp[i]->SetVisibility(false);
        }

        d->LightCurrentActorIndex =-1;
        d->XStartPosition =-1;
        d->YStartPosition =-1;

        this->InvokeEvent(vtkCommand::DisableEvent,NULL);
        this->SetCurrentRenderer(NULL);
    }

    this->Interactor->Render();
}

int axlLightsWidget::HighlightHandle(vtkProp *prop)
{

    //    if(d->LightRubberBanPick->getCurrentMode()==0)
    //    {//We select point with the cursor
    // first unhighlight anything picked
    this->resetProperty();

    d->LightCurrentActor = static_cast<vtkActor *>(prop);

    if ( d->LightCurrentActor )
    {
        for (int i = 0; i <d->NbLight; i++)
        {
            if ( d->LightCurrentActor == d->LightActor[i] )
            {
                d->LightCurrentActor->SetProperty(d->LightSelectedHandleProperty);
                d->LightCursorPicker->GetPickPosition(this->LastPickPosition);
                d->LightRectanglePicker->GetPickPosition(this->LastPickPosition);
                return i;
            }
        }
    }
    //    }
    //    else// we select points with rectangle
    //    {
    //        d->LightCurrentActor = static_cast<vtkActor *>(prop);
    //        if(d->LightCurrentActor->GetProperty()!=d->LightSelectedHandleProperty)
    //        {
    //            this->resetProperty();
    //            this->State = axlLightsWidget::Outside;
    //        }
    //    }


    return -1;
}



void axlLightsWidget::ProcessEvents(vtkObject* vtkNotUsed(object), unsigned long event, void *clientdata, void *vtkNotUsed(calldata))
{
    axlLightsWidget* self = reinterpret_cast<axlLightsWidget *>( clientdata );

    //okay, let's do the right thing
    switch(event)
    {
    case vtkCommand::LeftButtonPressEvent:
        self->OnLeftButtonDown();
        break;
    case vtkCommand::LeftButtonReleaseEvent:
        self->OnLeftButtonUp();
        break;
    case vtkCommand::MouseMoveEvent:
        self->OnMouseMove();
        break;
    }
}

void axlLightsWidget::PlaceWidget(double bds[6])
{
    int i;
    double bounds[6], center[3];

    this->AdjustBounds(bds, bounds, center);

    for (i=0; i<6; i++)
    {
        this->InitialBounds[i] = bounds[i];
    }

    this->InitialLength = sqrt((bounds[1]-bounds[0])*(bounds[1]-bounds[0]) +
                               (bounds[3]-bounds[2])*(bounds[3]-bounds[2]) +
                               (bounds[5]-bounds[4])*(bounds[5]-bounds[4]));
}

void axlLightsWidget::PrintSelf(ostream& os, vtkIndent indent)
{
    this->Superclass::PrintSelf(os,indent);
}

#define VTK_AVERAGE(a,b,c) \
    c[0] = (a[0] + b[0])/2.0; \
    c[1] = (a[1] + b[1])/2.0; \
    c[2] = (a[2] + b[2])/2.0;

#undef VTK_AVERAGE

void axlLightsWidget::OnLeftButtonDown(void)
{
    // d->interaction = true;
    int X = this->Interactor->GetEventPosition()[0];
    int Y = this->Interactor->GetEventPosition()[1];

    // Okay, we can process this. Try to pick handles first;
    // if no handles picked, then pick the bounding box.
    if (!this->CurrentRenderer || !this->CurrentRenderer->IsInViewport(X, Y))
    {
        this->State = axlLightsWidget::Outside;
        return;
    }

    d->XStartPosition  =X;
    d->YStartPosition = Y;

    vtkAssemblyPath *path = NULL;
    d->LightCursorPicker->Pick(X, Y, 0.0, this->CurrentRenderer);

    path = d->LightCursorPicker->GetPath();

    if ( path != NULL)
    {
        this->State = axlLightsWidget::Moving;
        d->LightCurrentActorIndex=this->HighlightHandle(path->GetLastNode()->GetViewProp());
        d->LightCurrentAssemblyNode = path->GetLastNode();
        d->LightCursorPicker->GetPickPosition(this->LastPickPosition);

        if( d->LightCurrentActorIndex == -1)
        {
            this->resetProperty();
            this->State = axlLightsWidget::Outside;
            return;
        }


    }
    else
    {
        this->resetProperty();
        this->State = axlLightsWidget::Outside;
        return;
    }
    this->EventCallbackCommand->SetAbortFlag(1);
    this->StartInteraction();
    //this->InvokeEvent(vtkCommand::StartInteractionEvent, NULL);
    //this->Interactor->Render();
}

void axlLightsWidget::OnLeftButtonUp()
{
    // d->interaction = true;
    int X = this->Interactor->GetEventPosition()[0];
    int Y = this->Interactor->GetEventPosition()[1];
    // Okay, we can process this. Try to pick handles first;
    // if no handles picked, then pick the bounding box.
    if (!this->CurrentRenderer || !this->CurrentRenderer->IsInViewport(X, Y))
    {
        this->State = axlLightsWidget::Outside;
        return;
    }

    /*if(d->LightRubberBanPick->getCurrentMode()==1)// mode rectangle selection activated
    {
        vtkProp3DCollection *listProp;
        d->LightRectanglePicker->AreaPick(d->LightRubberBanPick->getStartPositionX(),d->LightRubberBanPick->getStartPositionY(),d->LightRubberBanPick->getEndPositionX(),d->LightRubberBanPick->getEndPositionY(),this->CurrentRenderer);
        listProp = d->LightRectanglePicker->GetProp3Ds();
        if ( listProp != NULL )
        {
            vtkCollectionIterator *listPropIterator =listProp->NewIterator();
            listPropIterator->InitTraversal();
            while(!listPropIterator->IsDoneWithTraversal())
            {
                static_cast<vtkActor *>(listPropIterator->GetCurrentObject())->SetProperty(d->LightSelectedHandleProperty);
                listPropIterator->GoToNextItem();
            }
        }

    }*/

    if ( this->State == axlLightsWidget::Outside || this->State == axlLightsWidget::Start )
    {
        return;
    }


    this->State = axlLightsWidget::Start;

    this->EventCallbackCommand->SetAbortFlag(0);
    this->EndInteraction();
    this->InvokeEvent(vtkCommand::EndInteractionEvent, NULL);
}

void axlLightsWidget::OnMouseMove()
{
    // See whether we're active
    if ( this->State == axlLightsWidget::Outside ||
         this->State == axlLightsWidget::Start )
    {
        return;
    }

    int X = this->Interactor->GetEventPosition()[0];
    int Y = this->Interactor->GetEventPosition()[1];

    // Do different things depending on state
    // Calculations everybody does
    double focalPoint[4], pickPoint[4], prevPickPoint[4], motionVector[4];
    double z;

    vtkCamera *camera = this->CurrentRenderer->GetActiveCamera();
    if ( !camera )
    {
        return;
    }

    // Compute the two points defining the motion vector
    this->ComputeWorldToDisplay(this->LastPickPosition[0], this->LastPickPosition[1], this->LastPickPosition[2], focalPoint);
    z = focalPoint[2];
    this->ComputeDisplayToWorld(double(this->Interactor->GetLastEventPosition()[0]),double(this->Interactor->GetLastEventPosition()[1]),
                                z, prevPickPoint);
    this->ComputeDisplayToWorld(double(X), double(Y), z, pickPoint);
    for(int i=0;i<4;i++)
    {
        motionVector[i]=pickPoint[i]-prevPickPoint[i];
    }

    vtkMatrix4x4 *motion = vtkMatrix4x4::New();

    if(d->LightCurrentAssemblyNode)
        motion->DeepCopy(d->LightCurrentAssemblyNode->GetMatrix());
    motion->Invert();

    if ( this->State == axlLightsWidget::Moving )
    { // Okay to process

        vtkRenderWindow *renderWindow = this->Interactor->GetRenderWindow();
        vtkRendererCollection * rendererCollection = renderWindow->GetRenderers();
        vtkRenderer *render = rendererCollection->GetFirstRenderer();

        vtkLightCollection *lightCollection = render->GetLights();
        lightCollection->InitTraversal();

        vtkLight *currentLight = NULL;

        // set CurrentLight to the Light picked
        for(int i = 0; i<= d->LightCurrentActorIndex; i++)
            currentLight = lightCollection->GetNextItem();


        double *newMotion = motion->MultiplyDoublePoint(motionVector);
        double newPosition[4];
        for(int j = 0 ;j < 4; j++)
        {
            newPosition[j]=d->LightPoints->GetPoint(d->LightCurrentActorIndex)[j]+newMotion[j];
        }

        d->LightPoints->SetPoint(d->LightCurrentActorIndex, newPosition[0], newPosition[1], newPosition[2]);
        d->LightPoints->Modified();
        d->LightSphere[d->LightCurrentActorIndex]->SetCenter(d->LightPoints->GetPoint(d->LightCurrentActorIndex));
        currentLight->SetPosition(newPosition[0], newPosition[1], newPosition[2]);

        d->LightProp[d->LightCurrentActorIndex]->SetPosition(newPosition[0], newPosition[1], newPosition[2]);
        d->LightSphere[d->LightCurrentActorIndex]->Update();
        d->LightProp[d->LightCurrentActorIndex]->Modified();

        d->view->onLightPositionChanged(newPosition[0], newPosition[1], newPosition[2]);

    }
    else
    {
        return; //avoid the extra render
    }

    motion->Delete();

    this->EventCallbackCommand->SetAbortFlag(1);
    this->InvokeEvent(vtkCommand::InteractionEvent,NULL);
    this->Interactor->Render();
}


void axlLightsWidget::update()
{
    vtkRenderWindow *renderWindow = this->Interactor->GetRenderWindow();
    vtkRendererCollection * rendererCollection = renderWindow->GetRenderers();
    vtkRenderer *render = rendererCollection->GetFirstRenderer();

    vtkLightCollection *lightCollection = render->GetLights();
    lightCollection->InitTraversal();

    vtkLight *currentLight = NULL;

    for(int i = 0 ; i < d->NbLight ; i++)
    {
        d->LightCurrentActorIndex = i;
        // set CurrentLight to the Light picked
        currentLight = lightCollection->GetNextItem();


        double newPosition[4];
        for(int j = 0 ;j < 4; j++)
        {
            newPosition[j]=currentLight->GetPosition()[j];
        }

        d->LightPoints->SetPoint(d->LightCurrentActorIndex, newPosition[0], newPosition[1], newPosition[2]);
        d->LightPoints->Modified();
        d->LightSphere[d->LightCurrentActorIndex]->SetCenter(d->LightPoints->GetPoint(d->LightCurrentActorIndex));
        currentLight->SetPosition(newPosition[0], newPosition[1], newPosition[2]);
        d->LightSphere[d->LightCurrentActorIndex]->Update();
        d->LightSphere[d->LightCurrentActorIndex]->Modified();
        d->LightProp[d->LightCurrentActorIndex]->SetPosition(newPosition[0], newPosition[1], newPosition[2]);
        //        d->LightSphere[d->LightCurrentActorIndex]->Update();
        d->LightActor[d->LightCurrentActorIndex]->Modified();
    }

    this->Interactor->Render();
}

void axlLightsWidget::CreateDefaultProperties()
{
    // Handle properties
    d->LightHandleProperty = vtkProperty::New();
    d->LightHandleProperty->SetColor(1,1,1);

    d->LightSelectedHandleProperty = vtkProperty::New();
    d->LightSelectedHandleProperty->SetColor(1,0,0);
}

