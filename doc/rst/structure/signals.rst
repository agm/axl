Signals between Axl objects
============================

Signals are exchanged between the objects of the following types:

- **Data**: geometric objects, such as points, lines, planes, ...
- **Inspector**: tools, which display the information on the geometric objects and allow to edit them; 
- **Actor**: tools, which allow to modify graphically the objects; 

The signals are exchanged between data and inspectors and bewteen data and actors, to update the information attached to these classes. 

Data
----

Each Axl data which inherits from ``axlAbstractData`` has signals attached to it:

.. literalinclude:: ../../../src/axlCore/axlAbstractData.h
    :lines: 46-49
   
- ``modifiedGeometry`` indicates that the data of the object have changed. It can be emitted by using::

    this->touchGeometry();

- ``modifiedStructure`` indicates that the structure (e.g., the connection of the control points) has changed. It can be emitted by using::

    this->touchStructure();
    
- ``modifiedField`` indicates that the field attached to the object has changed. It can be emitted by using::

    this->touchField();



Inspector
---------
Two types of messages need to be considered.

- First, when values such as the coordinates of a point are modified from the inspector, the corresponding data are modified. This is done with the connection::

    connect(d->coordinate_x, SIGNAL(valueChanged(double)), this, SLOT(onCoordDataChanged_x(double)));
  
  via the method ``onCoordDataChanged_x``::

    void axlInspectorObjectPoint::onCoordDataChanged_x(double x)
    {
       d->point->x() = x;
       d->point->touchGeometry();
       emit update();
    }

- Secondly, when the object is modified, the data inspector should update its displayed values.
  This is done in the method ``setData`` of the data inspector. A connection between the signal
  ``modifiedGeometry`` and the slot ``updateValues`` allows to update the values displayed by
  the data inspector when the data is modified:

  .. literalinclude:: ../../../src/axlGui/axlInspectorObjectPoint.cpp
       :lines: 88-93

  Here is the method ``updateValues``, which updates the values displayed by the inspector:

  .. literalinclude:: ../../../src/axlGui/axlInspectorObjectPoint.cpp
     :lines: 356-361

  When the signal  ``modifiedGeometry`` is sent, the slot ``updateValues`` is evaluated and the displayed values of the coordinates in the inspector are updated according to the coordinates of the point.


Actor
-----
Here also, two types of messages need to be considered.

- When the widget in the actor Data Observer modifies the data, a signal ``modifiedGeometry`` is emitted. Here is the method ``Execute`` in the class ``axlActorPointObserver``, which send this signal: 

  .. literalinclude:: ../../../vtkview/axl/axlVtkView/axlActorPoint.cpp
     :lines: 67-82
    
- When the data is modified, the actor should be updated. In the method ``setData`` of the corresponding actor, the signal ``modifiedGeometry`` is connected with the slot ``onUpdateGeometry``, which updates the actor data::

    connect(d->point, SIGNAL(modifiedGeometry()),this, SLOT(onUpdateGeometry()));

  Here is the method ``onUpdateGeometry``, which update the source and widget of the actor:

  .. literalinclude:: ../../../vtkview/axl/axlVtkView/axlActorPoint.cpp
     :lines: 375-388

