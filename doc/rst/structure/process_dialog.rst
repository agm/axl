===============================
Dialog interfaces for processes
===============================

`Anais Ducoffe & Bernard Mourrain`


To run a process from the Axl Application Interface, you need a dialog class which is used to select the input and the parameters of the process and then execute it.
Using the mechanism implemented in the ``axlInspectorToolGeneric`` class will help you to generate your dialog thanks to a description provided in the process, and will avoid to write a dedicated dialog process class (though this is still possible).
To use this tool you must first check (or create) two things:

 #. the process must implement the method virtual ``QString axlAbstractProcess::form(void) const``;
 #. the process has to be registered in the factory of processes (with the method ``registered``). 

.. warning::

   The class providing this method ``form`` should derived from ``axlAbstractProcess``
   
Formalism
=========
Overload ``axlAbstractProcess::form(void) const`` with a ``QString`` following this convention:
   
- a line per input ending with "\n", formatted as follows::

       "INPUT[:CHANNEL] TYPE LABEL [VALUE]\n"

  where:
    - CHANNEL is a number identifying the rank of an input, see ``dtkAbstractProcess``.
      [:CHANNEL] is optional. Write this channel identifier if the process method ``setInput`` uses a channel in its parameters. 
    - TYPE is either ``int``, ``double`` or ``data``
    - LABEL is the input's name
    - VALUE is optional. It specifies the default value of the input, when it is an "int" or "double".

- a line per parameter ending with "\n", formatted as follows::

      "PARAMETER[:CHANNEL] TYPE LABEL [VALUE]\n"

  where:
    - CHANNEL is a number identifying the rank of an parameter, see ``dtkAbstractProcess``.
      [:CHANNEL] is optional. Write this channel identifier if the process method ``setParameter`` uses a channel in its parameters.
    - TYPE is either ``int`` or ``double``
    - LABEL is the parameter's name
    - VALUE is optional. It specifies the default value of the parameter when it is an ``int`` or ``double``.

- a line per output, formatted as follows::

      "OUTPUT[:CHANNEL] LABEL"

  where:
    -  CHANNEL is a number identifying the output, see ``dtkAbstractProcess``. [:CHANNEL] is optional.
    -  LABEL is the output's name  

Here is an example of such a declaration::

  QString form(void) const
  {
     return QString(
       " INPUT:0 data StartPoint \n"
       " INPUT:1 data EndPoint \n"
       " PARAMETER:0 double Radius 1.1\n"
       " OUTPUT Cylinder ");
  }


Another example with the axel class `axlBarycenterProcess <../classaxl_barycenter_process.html>`_, which uses
``axlBarycenterProcess::setParameter(double value)`` and not ``axlBarycenterProcess::setParameter(double value, int channel)``::

  QString form(void) const
  {
     return QString(
        " INPUT:0 data line \n"
        " PARAMETER double coefficient 0.5\n"
        " OUTPUT:0 barycenterPoint");
  }

Examples
========

Here is an example used in ``axlBarycenterProcess.h``

.. literalinclude:: ../../../src/axlCore/axlBarycenterProcess.h 
   :lines: 29-69 

This process takes a line defined by two points as input and outputs a point which is the barycenter of the two points. The parameter is the barycentric coordinate used to compute this barycenter. The default value is 0.5, which corresponds to the middle point.

Here is an example used in ``axlFieldSpatialPointDistanceCreator.h``:

.. literalinclude:: ../../../src/axlCore/axlFieldSpatialPointDistanceCreator.h
   :lines: 33-68
   :language: cpp

The process creates a scalar field, from a general input, using 3 parameters which are the coordinates of a points. The value of the field for a point on the input object
is its distance to the parameter point. The default value for the parameter point is
[0, 0, 0].
