Ellipsoid
=========
An ellipsoid is defined by a center (axlPoint), 3 orthogonal vectors (axlPoint) corresponding to the semi-axes of the ellipsoid.

Type: ``axlEllipsoid``

Format:

.. literalinclude:: ../../../data/ellipsoid.axl 
   :lines: 2-7
   :language: xml
	      
.. image:: ../img/ellipsoid.png
   :height: 100px
   :align: center
