Cone
====
A cone is defined by a base point (axlPoint) and an apex point (axlPoint) and a radius (double) of the base circle.

Type: ``axlCone``


Format:

.. literalinclude:: ../../../data/cone.axl
   :lines: 2-6
   :language: xml
	      
.. image:: ../img/cone.png
   :height: 100px
   :align: center


