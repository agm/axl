/* axlActorSurfaceTrimmed.h ---
 *
 * Author: Meriadeg Perrinel
 * Copyright (C) 2008 - Meriadeg Perrinel, Inria.
 * Created: Fri Dec 17 11:01:52 2010 (+0100)
 * Version: $Id$
 * Last-Updated: Mon Dec 17 14:12:40 2012 (+0100)
 *           By: Julien Wintz
 *     Update #: 18
 */

/* Commentary:
 *
 */

/* Change log:
 *
 */

#ifndef AXLACTORSURFACETRIMMED_H
#define AXLACTORSURFACETRIMMED_H

#include "axlActorSurfaceBSpline.h"

#include "axlVtkViewPluginExport.h"

class axlAbstractSurfaceTrimmed;
class axlActorSurfaceTrimmedPrivate;

class AXLVTKVIEWPLUGIN_EXPORT axlActorSurfaceTrimmed : public axlActorSurfaceBSpline
{
public:
#if (VTK_MAJOR_VERSION <= 5)
    vtkTypeRevisionMacro(axlActorSurfaceTrimmed, vtkAssembly);
#endif

    static axlActorSurfaceTrimmed *New(void);

public:
    dtkAbstractData *data(void);

public:
    void setSurface(axlAbstractSurfaceTrimmed *Surface);
    void setControlPolygon(bool control);
    void setMapperCollorArray(void);


public slots:
    virtual void onSamplingChanged(void);

protected:
     axlActorSurfaceTrimmed(void);
    ~axlActorSurfaceTrimmed(void);

private :
     void mehsProcess(void);

private:
    axlActorSurfaceTrimmed(const axlActorSurfaceTrimmed&); // Not implemented.
        void operator = (const axlActorSurfaceTrimmed&); // Not implemented.

private:
    axlActorSurfaceTrimmedPrivate *d;
};

#endif //AXLACTORSURFACETRIMMED_H
