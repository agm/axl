#!/usr/bin/python 
from axl import *

# To run Axl. Be careful sometimes Axl open too late. 
# You should run Axl apart if so. 
# or wait more than 3 seconds by changins sleep 5 with another value. 
pid = subprocess.Popen([axl_app , "--verbose"],shell=True) 
p = subprocess.Popen('sleep 3', shell=True)
# to wait axl opening.
p.wait()

# Create a client to communicate with Axl.
c = axlClient()

# Create an axl point
point = axlPoint(2,6,5) 
print point.description() 
# Send it to the Axl view. 
c.sendData(point) 
# Modify the coordinates 
point.setValues(1.8,2.5,0.0) 
print point.description() 
# Communicate the new values to Axl. 
c.modifyData(point) 

# If a new object was created in Axl, with name axlCone, 
# the python application can get it. 
# c.getData("axlCone") 
# Suppose an axlCone de nom axlCore is created from Axl. 
# Delete the point representation in Axl view. 
# c.deleteData(point)

# Here is an example to explain how to call a process in Axl and 
# get the result. The process called  must be initialized and 
# exist in Axl not in python application. 
point1 = axlPoint(1.5,0.5,2)
point2 = axlPoint(2,0,2) 
line = axlLine(point1,point2) 
listInput = axlAbstractDataComposite() 
listInput.add(line) 
valueParam = axlDouble(0.25) 
listParam = axlAbstractDataComposite(); 
listParam.add(valueParam); 

# Method to call process with specific inputs and parameters. 
data = c.callProcess("axlBarycenterProcess",listInput, listParam ) 
print data.description()

p2 = subprocess.Popen('sleep 5', shell=True) 
p2.wait()
