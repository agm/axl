/* axlVtkView.cpp ---
 *
 * Author: Julien Wintz
 * Copyright (C) 2008 - Julien Wintz, Inria.
 * Created: Wed Mar 30 09:19:37 2011 (+0200)
 * Version: $Id$
 * Last-Updated: Mon Dec 17 16:52:43 2012 (+0100)
 *           By: Julien Wintz
 *     Update #: 172
 */

/* Commentary:
 *
 */

/* Change log:
 *
 */

#include "axlActorDataDynamic.h"
#include "axlActorComposite.h"
#include "axlActorCone.h"
#include "axlActorCylinder.h"
#include "axlActorTorus.h"
//#include "axlActorCircleArc.h"
#include "axlActorEllipsoid.h"
#include "axlActorFieldDiscrete.h"
#include "axlActorFieldSpatial.h"
#include "axlActorFieldParametric.h"
#include "axlActorMesh.h"
#include "axlActorLine.h"
#include "axlActorPlane.h"
#include "axlActorPoint.h"
#include "axlActorPointSet.h"
#include "axlActorCurveBSpline.h"
#include "axlActorSphere.h"
#include "axlActorSurfaceBSpline.h"
#include "axlActorVolumeBSpline.h"
#include "axlActorShapeBSpline.h"
#include "axlActorSurfaceTrimmed.h"
#include "axlInteractorStyleRubberBandPick.h"
#include "axlInteractorStyleSwitch.h"
#include "axlLightsWidget.h"
#include "axlInspectorActorFactory.h"
#include "axlActorVolumeDiscrete.h"
#include "axlVolumeDiscrete.h"

#include "axlVtkView.h"
#include "axlVtkViewPlugin.h"

#include <dtkCoreSupport/dtkAbstractViewFactory.h>
#include <dtkCoreSupport/dtkAbstractDataFactory.h>
#include <dtkCoreSupport/dtkGlobal.h>

#include <axlCore/axlDataDynamic.h>
#include <axlCore/axlAbstractField.h>
#include <axlCore/axlAbstractFieldSpatial.h>
#include <axlCore/axlAbstractFieldParametric.h>
//#include "axlFieldDiscrete.h"
#include <axlCore/axlAbstractDataComposite.h>
#include <axlCore/axlAbstractDataWriter.h>
#include <axlCore/axlAbstractCurveBSpline.h>
#include <axlCore/axlAbstractSurfaceBSpline.h>
#include <axlCore/axlAbstractVolumeBSpline.h>
#include <axlCore/axlShapeBSpline.h>
#include <axlCore/axlAbstractSurfaceTrimmed.h>
#include <axlCore/axlCone.h>
#include <axlCore/axlCylinder.h>
#include <axlCore/axlTorus.h>
#include <axlCore/axlCircleArc.h>
#include <axlCore/axlEllipsoid.h>
#include <axlCore/axlLine.h>
#include <axlCore/axlPlane.h>
//#include <axlCore/axlPointSet.h>
#include <axlCore/axlMesh.h>
#include <axlCore/axlSphere.h>

#include <vtkActor.h>
#include <vtkActorCollection.h>
#include <vtkAssemblyNode.h>
#include <vtkAssemblyPath.h>
#include <vtkAxesActor.h>
#include <vtkCamera.h>
#include <vtkCellPicker.h>
#include <vtkImageData.h>
#include <vtkInteractorStyleTrackballCamera.h>
#include <vtkInteractorStyleJoystickCamera.h>
#include <vtkInteractorStyleRubberBandPick.h>
#include <vtkInteractorStyleJoystickActor.h>
#include <vtkInteractorStyleTrackballActor.h>
#include <vtkLight.h>
#include <vtkLightActor.h>
#include <vtkLightKit.h>
#include <vtkLightCollection.h>
#include <vtkPlaneSource.h>
#include <vtkPolyDataMapper.h>
#include <vtkDataSetMapper.h>
#include <vtkProp3DCollection.h>
#include <vtkRenderer.h>
#include <vtkRendererCollection.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkSmartPointer.h>
#include <vtkTexture.h>
#include <vtkTimerLog.h>
#include <vtkCallbackCommand.h>
#include <vtkVersion.h>
#include <vtkStripper.h>
#include <vtkPointData.h>
#include <vtkTriangleFilter.h>
#include <vtkPNGReader.h>
#include <vtkFloatArray.h>
#include <vtkGenericOpenGLRenderWindow.h>
//#include <vtkOpenGLHardwareSupport.h>
#include <vtkOpenGLRenderWindow.h>
//#include <vtkPainterPolyDataMapper.h>
#include <vtkProperty.h>

#include <QtOpenGL>
#include <QVTKOpenGLWidget.h>
#include <QVTKInteractor.h>

#include "axlVtkViewPlugin.h"

class vtkViewObserver : public vtkCommand
{
public:
    static vtkViewObserver *New(void)
    {
        return new vtkViewObserver;
    }

    virtual void Execute(vtkObject *caller, unsigned long event, void *)
    {
        //qDebug()<<"axlVtkView Execute : "<<observerRenderer->GetNumberOfPropsRendered();
        if(event == vtkCommand::LeftButtonPressEvent && observerInteractor->GetShiftKey())
        {
            //first try to pick an actor

            vtkAssemblyPath *path;
            vtkRenderWindow *renderWindow = observerInteractor->GetRenderWindow();
            vtkRendererCollection * rendererCollection = renderWindow->GetRenderers();
            vtkRenderer *render = rendererCollection->GetFirstRenderer();
            int X = observerInteractor->GetEventPosition()[0];
            int Y = observerInteractor->GetEventPosition()[1];

            if (!render || !render->IsInViewport(X, Y))
            {
                qDebug()<<" No renderer or bad cursor coordonates in axlVtkView";
            }

            observerViewerPicker->Pick(X,Y,0.0,render);

            path = observerViewerPicker->GetPath();

            // we need to iterate on the actors of the renderer

            if (path != NULL)
            {

                vtkPropCollection *actorCollection = observerRenderer->GetViewProps();
                actorCollection->InitTraversal();

                for(int i = 0; i < actorCollection->GetNumberOfItems(); i++)
                {
                    vtkProp *currentActor = actorCollection->GetNextProp();
                    if(path->GetFirstNode()->GetViewProp() == currentActor)
                    {
                        if(axlAbstractActor *actorProp = dynamic_cast<axlAbstractActor *>(currentActor))
                        {/*qDebug()<<"vtkViewObserver::Execute";*/
                            if(actorProp->getState() == axlActorComposite::passive)
                                actorProp->setMode(axlActorComposite::selection);
                            else if(actorProp->getState() == axlActorComposite::selection)
                                actorProp->setMode(axlActorComposite::passive);
                        }
                    }
                }
            }
        }

        if(event == vtkCommand::RightButtonPressEvent && observerInteractor->GetShiftKey())
        {

            //first try to pick an actor

            vtkAssemblyPath *path;
            vtkRenderWindow *renderWindow = observerInteractor->GetRenderWindow();
            vtkRendererCollection * rendererCollection = renderWindow->GetRenderers();
            vtkRenderer *render = rendererCollection->GetFirstRenderer();
            int X = observerInteractor->GetEventPosition()[0];
            int Y = observerInteractor->GetEventPosition()[1];

            if (!render || !render->IsInViewport(X, Y))
            {
                //qDebug()<<" No renderer or bad cursor coordonates";
            }

            observerViewerPicker->Pick(X,Y,0.0,render);

            path = observerViewerPicker->GetPath();

            // we need to iterate on the actors of the renderer

            if (path == NULL)
            {
                vtkPropCollection *actorCollection = observerRenderer->GetViewProps();
                actorCollection->InitTraversal();

                for(int i = 0; i < actorCollection->GetNumberOfItems(); i++)
                {
                    vtkProp *currentActor = actorCollection->GetNextProp();
                    if(axlAbstractActor *actorProp = dynamic_cast<axlAbstractActor *>(currentActor))
                        actorProp->setMode(axlActor::passive);

                }
            }
        }

        if(event == vtkCommand::KeyPressEvent)
        {
            QString modifier(observerInteractor->GetKeySym());
            if(modifier.compare("x") == 0 && observerAxesActor)
            {
                observerAxesActor->SetVisibility(!observerAxesActor->GetVisibility());

                // update view
                widget->update();
                widget->GetRenderWindow()->Render();
            }
            else if(modifier.compare("e") || modifier.compare("u"))
            {
                vtkPropCollection *actorCollection = observerRenderer->GetViewProps();
                actorCollection->InitTraversal();
                for(int i = 0; i < actorCollection->GetNumberOfItems(); i++)
                {
                    vtkProp *currentActor = actorCollection->GetNextProp();
                    if(axlAbstractActor *actorProp = dynamic_cast<axlAbstractActor *>(currentActor))
                    {
                        if(modifier.compare("e") == 0 && actorProp->getState()==axlActor::selection)//Tab Active : mode editable change
                            actorProp->setMode(axlActor::edition);

                        if(modifier.compare("u") == 0 && actorProp->getState()==axlActor::edition)//Tab Active : mode editable change
                            actorProp->setMode(axlActor::selection);
                    }

                }
            }
        }

//        if(event =mmit= vtkCommand::MouseMoveEvent)
//        {
//            qDebug()<< "axlVtkView Execute Mouse Move" ;
//        }

    }

    vtkSmartPointer<vtkCellPicker> observerViewerPicker;
    vtkRenderWindowInteractor *observerInteractor;
    vtkSmartPointer<vtkRenderer> observerRenderer;
    vtkSmartPointer<vtkAxesActor> observerAxesActor;
    QVTKOpenGLWidget *widget;
};

// /////////////////////////////////////////////////////////////////
// vtkViewPrivate
// /////////////////////////////////////////////////////////////////

class vtkViewPrivate
{
public:
    vtkSmartPointer<axlInteractorStyleSwitch> style;
    vtkSmartPointer<vtkCellPicker> viewerPicker;

    vtkRenderer *renderer;

//    vtkSmartPointer<vtkGenericOpenGLRenderWindow> window;
    vtkNew<vtkGenericOpenGLRenderWindow> window;
    vtkSmartPointer<vtkViewObserver> observer;

    QVTKOpenGLWidget *widget;

    vtkSmartPointer<vtkLightKit> lightKit;
    vtkSmartPointer<vtkLight> firstLight;

    axlLightsWidget *lightWidget;

    vtkSmartPointer<vtkLightActor> lightActor;

    double camera_center[3];
    double camera_focal[3];
    double camera_view_up[3];

    bool lastCameraIsDefault;

    vtkSmartPointer<vtkActor> actorX;
    vtkSmartPointer<vtkActor> actorY;
    vtkSmartPointer<vtkActor> actorZ;
    vtkSmartPointer<vtkAxesActor> axesActor;
    vtkSmartPointer<vtkPlaneSource> planeSourceX;
    vtkSmartPointer<vtkPlaneSource> planeSourceY;
    vtkSmartPointer<vtkPlaneSource> planeSourceZ;

    bool isGridX;
    bool isGridY;
    bool isGridZ;
};

// /////////////////////////////////////////////////////////////////
// axlVtkView
// /////////////////////////////////////////////////////////////////

axlVtkView::axlVtkView(void) : axlAbstractView(), d(new vtkViewPrivate)
{

    d->renderer = vtkRenderer::New();
    d->renderer->SetGradientBackground(true);
    d->renderer->SetBackground (0.90, 0.90, 0.90);
    d->renderer->SetBackground2(0.64, 0.64, 0.64);

    QSurfaceFormat::setDefaultFormat(QVTKOpenGLWidget::defaultFormat());
    d->widget = new QVTKOpenGLWidget;
    d->widget->SetRenderWindow(d->window.Get());
//#if (VTK_MAJOR_VERSION <= 5)
//    d->window = vtkSmartPointer<vtkRenderWindow>::New();
//#else
//    d->window = d->widget->GetRenderWindow();
//#endif

    d->window->SetPointSmoothing(1);
    d->window->SetLineSmoothing(1);
    // Uncommenting shows edges on top of objects with certain graphic cards
    // d->window->SetPolygonSmoothing(1);
    d->window->StereoCapableWindowOn();

    // stereo mode
    if(qApp->arguments().contains("--stereo")) {
        QString stereoMode = dtkApplicationArgumentsValue(qApp,"--stereo");
        //--stereo ANAGLYPHIC | QUAD_BUFFER | HORIZONTAL_SPLIT | HORIZONTAL_INTERLACE
        if (stereoMode == "QUAD_BUFFER") {
            d->window->SetStereoTypeToCrystalEyes();
        } else if (stereoMode == "HORIZONTAL_SPLIT") {
            d->window->SetStereoTypeToSplitViewportHorizontal();
        } else if (stereoMode == "HORIZONTAL_INTERLACE") {
            d->window->SetStereoTypeToInterlaced();
        } else if (stereoMode == "ANAGLYPHIC") {
            d->window->SetStereoTypeToAnaglyph();
        } else {
	  qDebug() <<  "WARNING: Unknown stereo mode ("<< stereoMode << "), will use QUAD BUFFER" ;
	  d->window->SetStereoTypeToCrystalEyes();
        }
        d->window->SetStereoRender(1);
    }

    d->window->AddRenderer(d->renderer);

#if (VTK_MAJOR_VERSION <= 5)
    d->widget->SetRenderWindow(d->window);
#endif

    d->observer = NULL;

    d->viewerPicker = vtkSmartPointer<vtkCellPicker>::New();
    d->viewerPicker->SetTolerance(0.001);

    d->style = vtkSmartPointer<axlInteractorStyleSwitch>::New();
    d->style->SetDefaultRenderer(d->renderer);

    d->axesActor = vtkSmartPointer<vtkAxesActor>::New();
    d->axesActor->AxisLabelsOff();
    d->axesActor->SetConeRadius(0.3);
    d->axesActor->SetNormalizedTipLength(0.1, 0.1, 0.1);
    d->axesActor->SetNormalizedShaftLength(0.9, 0.9, 0.9);
    d->axesActor->SetShaftTypeToCylinder();
    d->axesActor->SetCylinderRadius(0.005);

    d->renderer->AddActor(d->axesActor);
    d->renderer->ResetCamera();

    //grid
    d->isGridX = false;
    d->isGridY = false;
    d->isGridZ = false;

    //lightKit->AddLightsToRenderer(d->renderer);

    vtkCamera *camera = d->renderer->GetActiveCamera();
    camera->GetPosition(d->camera_center);
    camera->GetFocalPoint(d->camera_focal);
    camera->GetViewUp(d->camera_view_up);
    d->lastCameraIsDefault = true;

    d->lightWidget=NULL;
    d->lightActor= NULL;

    //observer
    d->observer = vtkViewObserver::New();
    d->observer->observerViewerPicker = d->viewerPicker;
    d->observer->observerRenderer = d->renderer;
    d->observer->observerInteractor =  d->widget->GetInteractor();
    d->observer->observerAxesActor = d->axesActor;
    d->observer->widget = d->widget;

    d->widget->GetInteractor()->AddObserver(vtkCommand::LeftButtonPressEvent, d->observer);
    d->widget->GetInteractor()->AddObserver(vtkCommand::RightButtonPressEvent, d->observer);
    d->widget->GetInteractor()->AddObserver(vtkCommand::KeyPressEvent, d->observer);

    d->widget->setFocusPolicy(Qt::StrongFocus);
}

axlVtkView::~axlVtkView(void)
{

    if(d->lightWidget)
        d->lightWidget->Delete();


    d->widget->GetInteractor()->RemoveObservers(vtkCommand::LeftButtonPressEvent, d->observer);
    d->widget->GetInteractor()->RemoveObservers(vtkCommand::RightButtonPressEvent, d->observer);
    d->widget->GetInteractor()->RemoveObservers(vtkCommand::KeyPressEvent, d->observer);

    d->observer->observerViewerPicker = NULL;
    d->observer->observerRenderer = NULL;
    d->observer->observerInteractor =  NULL;

    d->observer->Delete();
    d->observer = NULL;

    d->renderer->Delete();
    d->renderer = NULL;

    delete d;

    d = NULL;
}

bool axlVtkView::registered(void)
{
    //qDebug()<< "axlVtkView::registered"<<dtkAbstractDataFactory::instance()<<vtkViewPlugin::viewFactSingleton;
    return axlVtkViewPlugin::viewFactSingleton->registerViewType("axlVtkView", createVtkView);
}

QString axlVtkView::description(void) const
{
    return "axlVtkView";
}

void axlVtkView::setWorldCamera(void)
{
    if(dynamic_cast<vtkInteractorStyleJoystickActor *> (d->style->GetCurrentStyle()))
        d->style->SetCurrentStyleToJoystickCamera();

    if(dynamic_cast<vtkInteractorStyleTrackballActor *> (d->style->GetCurrentStyle()))
        d->style->SetCurrentStyleToTrackballCamera();
}

void axlVtkView::setObjectCamera(void)
{
    if(dynamic_cast<vtkInteractorStyleJoystickCamera *> (d->style->GetCurrentStyle()))
        d->style->SetCurrentStyleToJoystickActor();

    if(dynamic_cast<vtkInteractorStyleTrackballCamera *> (d->style->GetCurrentStyle()))
        d->style->SetCurrentStyleToTrackballActor();
}

void axlVtkView::setTrackballInteractor(void)
{
    if(dynamic_cast<vtkInteractorStyleJoystickActor *> (d->style->GetCurrentStyle()))
        d->style->SetCurrentStyleToTrackballActor();

    if(dynamic_cast<vtkInteractorStyleJoystickCamera *> (d->style->GetCurrentStyle()))
        d->style->SetCurrentStyleToTrackballCamera();
}

void axlVtkView::setJoystickInteractor(void)
{
    if(dynamic_cast<vtkInteractorStyleTrackballActor *> (d->style->GetCurrentStyle()))
        d->style->SetCurrentStyleToJoystickActor();

    if(dynamic_cast<vtkInteractorStyleTrackballCamera *> (d->style->GetCurrentStyle()))
        d->style->SetCurrentStyleToJoystickCamera();
}

void axlVtkView::bounds(float& xmin, float& xmax, float& ymin, float& ymax, float& zmin, float& zmax)
{
    double bounds[6]; d->renderer->ComputeVisiblePropBounds(bounds);

    xmin = bounds[0];
    xmax = bounds[1];
    ymin = bounds[2];
    ymax = bounds[3];
    zmin = bounds[4];
    zmax = bounds[5];
}

void axlVtkView::cameraUp(double *coordinates)
{
    vtkCamera *camera = d->renderer->GetActiveCamera();

    camera->GetViewUp(coordinates);
}

void axlVtkView::cameraPosition(double *coordinates)
{
    vtkCamera *camera = d->renderer->GetActiveCamera();

    camera->GetPosition(coordinates);
}

void axlVtkView::cameraFocalPoint(double *coordinates)
{
    vtkCamera *camera = d->renderer->GetActiveCamera();

    camera->GetFocalPoint(coordinates);
}

void axlVtkView::setCameraPosition(double x, double y, double z)
{
    vtkCamera *camera = d->renderer->GetActiveCamera();

    camera->SetPosition(x, y, z);

    d->renderer->ResetCameraClippingRange();
}

QList<axlAbstractActor *> axlVtkView::insertSet(QList<axlAbstractData *> dataSet)
{
    vtkRenderWindowInteractor *iren = d->widget->GetInteractor();
    d->widget->GetInteractor()->SetInteractorStyle(d->style);

    // Move before

    QList<axlAbstractActor *>  result;

    double currentTime = vtkTimerLog::GetCPUTime();
    //double start = clock();
    //qDebug()<<"axlVtkView cpu time "<<start;
    dtkWarn()<<"Start time"<<currentTime;
    QList<axlAbstractData *>::Iterator data;

    for(data = dataSet.begin(); data != dataSet.end(); data++)
    {
        if (axlPoint *point = dynamic_cast<axlPoint *>(*data)) {

            axlActorPoint *actor = axlActorPoint::New();

            actor->setInteractor(iren);
            actor->setQVTKWidget(d->widget);
            actor->setData(point);

            actor->setDisplay(true);

            d->renderer->AddActor(actor);

            axlAbstractView::insert(point, actor);
            connect(actor, SIGNAL(stateChanged(dtkAbstractData*, int)), this, SIGNAL(stateChanged(dtkAbstractData*, int)));
            connect(actor, SIGNAL(updated()), this, SLOT(update()));
            result.push_back(actor);
        }
        else if (axlCone *cone = dynamic_cast<axlCone *>(*data)) {

            axlActorCone *actor = axlActorCone::New();

            actor->setInteractor(iren);
            actor->setQVTKWidget(d->widget);
            actor->setData(cone);

            //actor->setDisplay(true);

            d->renderer->AddActor(actor);

            axlAbstractView::insert(cone, actor);
            connect(actor, SIGNAL(stateChanged(dtkAbstractData*, int)), this, SIGNAL(stateChanged(dtkAbstractData*, int)));
            connect(actor, SIGNAL(updated()), this, SLOT(update()));
            result.push_back(actor);
        }
        else if (axlCylinder *cylinder = dynamic_cast<axlCylinder *>(*data)) {

            axlActorCylinder *actor = axlActorCylinder::New();

            actor->setInteractor(iren);
            actor->setQVTKWidget(d->widget);
            actor->setData(cylinder);

            //actor->setDisplay(true);

            d->renderer->AddActor(actor);

            axlAbstractView::insert(cylinder, actor);
            connect(actor, SIGNAL(stateChanged(dtkAbstractData*, int)), this, SIGNAL(stateChanged(dtkAbstractData*, int)));
            connect(actor, SIGNAL(updated()), this, SLOT(update()));
            result.push_back(actor);
        }
        else if (axlTorus *torus = dynamic_cast<axlTorus *>(*data)) {

            axlActorTorus *actor = axlActorTorus::New();

            actor->setInteractor(iren);
            actor->setQVTKWidget(d->widget);
            actor->setData(torus);

            //actor->setDisplay(true);

            d->renderer->AddActor(actor);

            axlAbstractView::insert(torus, actor);
            connect(actor, SIGNAL(stateChanged(dtkAbstractData*, int)), this, SIGNAL(stateChanged(dtkAbstractData*, int)));
            connect(actor, SIGNAL(updated()), this, SLOT(update()));
            result.push_back(actor);
        }
        //        else if (axlCircleArc *arc = dynamic_cast<axlCircleArc *>(*data)) {

        //            axlActorCircleArc *actor = axlActorCircleArc::New();

        //            actor->setInteractor(iren);
        //            actor->setQVTKWidget(d->widget);
        //            actor->setCircleArc(arc);

        //            //actor->setDisplay(true);

        //            d->renderer->AddActor(actor);

        //            axlAbstractView::insert(arc, actor);
        //            connect(actor, SIGNAL(stateChanged(dtkAbstractData*, int)), this, SIGNAL(stateChanged(dtkAbstractData*, int)));
        //            result.push_back(actor);
        //        }
        else if (axlEllipsoid *ellipsoid = dynamic_cast<axlEllipsoid *>(*data)) {

            axlActorEllipsoid *actor = axlActorEllipsoid::New();

            actor->setInteractor(iren);
            actor->setQVTKWidget(d->widget);
            actor->setData(ellipsoid);

            //actor->setDisplay(true);

            d->renderer->AddActor(actor);

            axlAbstractView::insert(ellipsoid, actor);
            connect(actor, SIGNAL(stateChanged(dtkAbstractData*, int)), this, SIGNAL(stateChanged(dtkAbstractData*, int)));
            connect(actor, SIGNAL(updated()), this, SLOT(update()));
            result.push_back(actor);
        }
        else if (axlLine *line = dynamic_cast<axlLine *>(*data)) {

            axlActorLine *actor = axlActorLine::New();

            actor->setInteractor(iren);
            actor->setQVTKWidget(d->widget);
            actor->setData(line);

            //actor->setDisplay(true);

            d->renderer->AddActor(actor);

            axlAbstractView::insert(line, actor);
            connect(actor, SIGNAL(stateChanged(dtkAbstractData*, int)), this, SIGNAL(stateChanged(dtkAbstractData*, int)));
            connect(actor, SIGNAL(updated()), this, SLOT(update()));
            result.push_back(actor);
        }
        else if (axlPlane *plane = dynamic_cast<axlPlane *>(*data)) {

            axlActorPlane *actor = axlActorPlane::New();

            actor->setInteractor(iren);
            actor->setQVTKWidget(d->widget);
            actor->setRenderer(d->renderer);
            actor->setData(plane);

            //actor->setDisplay(true);

            d->renderer->AddActor(actor);

            axlAbstractView::insert(plane, actor);
            connect(actor, SIGNAL(stateChanged(dtkAbstractData*, int)), this, SIGNAL(stateChanged(dtkAbstractData*, int)));
            connect(actor, SIGNAL(updated()), this, SLOT(update()));
            result.push_back(actor);
        }
        else if (axlSphere *sphere = dynamic_cast<axlSphere *>(*data)) {

            axlActorSphere *actor = axlActorSphere::New();

            actor->setInteractor(iren);
            actor->setData(sphere);

            actor->setDisplay(true);

            d->renderer->AddActor(actor);

            axlAbstractView::insert(sphere, actor);
            connect(actor, SIGNAL(stateChanged(dtkAbstractData*, int)), this, SIGNAL(stateChanged(dtkAbstractData*, int)));
            connect(actor, SIGNAL(updated()), this, SLOT(update()));
            result.push_back(actor);
        }
        else if (axlAbstractCurveBSpline *spline_curve = dynamic_cast<axlAbstractCurveBSpline *>(*data)) {

            axlActorCurveBSpline *actor = axlActorCurveBSpline::New();

            actor->setInteractor(iren);
            actor->setData(spline_curve);

            d->renderer->AddActor(actor);

            axlAbstractView::insert(spline_curve, actor);
            connect(actor, SIGNAL(stateChanged(dtkAbstractData *, int)), this, SIGNAL(stateChanged(dtkAbstractData *, int)));
            connect(spline_curve, SIGNAL(samplingChanged()), actor, SLOT(onSamplingChanged()));
            connect(actor, SIGNAL(updated()), this, SLOT(update()));
            result.push_back(actor);;
        }
        else if (axlAbstractSurfaceTrimmed *spline_surface = dynamic_cast<axlAbstractSurfaceTrimmed *>(*data)) {

            axlActorSurfaceTrimmed *actor = axlActorSurfaceTrimmed::New();
            actor->setInteractor(iren);

            actor->setSurface(spline_surface);
            d->renderer->AddActor(actor);

            axlAbstractView::insert(spline_surface, actor);
            connect(actor, SIGNAL(stateChanged(dtkAbstractData*, int)), this, SIGNAL(stateChanged(dtkAbstractData*, int)));
            connect(spline_surface,SIGNAL(samplingChanged()),actor,SLOT(onSamplingChanged()));

            result.push_back(actor);
        }
        else if (axlAbstractSurfaceBSpline *spline_surface = dynamic_cast<axlAbstractSurfaceBSpline *>(*data)) {

            axlActorSurfaceBSpline *actor = axlActorSurfaceBSpline::New();
            actor->setInteractor(iren);

            actor->setData(spline_surface);
            d->renderer->AddActor(actor);

            axlAbstractView::insert(spline_surface, actor);
            connect(actor, SIGNAL(stateChanged(dtkAbstractData*, int)), this, SIGNAL(stateChanged(dtkAbstractData*, int)));
            connect(spline_surface,SIGNAL(samplingChanged()),actor,SLOT(onSamplingChanged()));
            connect(actor, SIGNAL(updated()), this, SLOT(update()));
            //connect(spline_surface, SIGNAL(touch()), this, SLOT(update()));

            //this->insertFields(spline_surface, actor);

            result.push_back(actor);
        }
        else if (axlAbstractVolumeBSpline *spline_volume = dynamic_cast<axlAbstractVolumeBSpline *>(*data)) {

            axlActorVolumeBSpline *actor = axlActorVolumeBSpline::New();
            actor->setInteractor(iren);

            actor->setData(spline_volume);
            d->renderer->AddActor(actor);

            axlAbstractView::insert(spline_volume, actor);
            connect(actor, SIGNAL(stateChanged(dtkAbstractData*, int)), this, SIGNAL(stateChanged(dtkAbstractData*, int)));
            connect(spline_volume,SIGNAL(samplingChanged()),actor,SLOT(onSamplingChanged()));
            connect(actor, SIGNAL(updated()), this, SLOT(update()));
            //this->insertFields(spline_volume, actor);


            result.push_back(actor);

        }
        else if (axlShapeBSpline *spline_surface = dynamic_cast<axlShapeBSpline *>(*data)) {

            axlActorShapeBSpline *actor = axlActorShapeBSpline::New();
            actor->setInteractor(iren);

            actor->setData(spline_surface);
            d->renderer->AddActor(actor);

            axlAbstractView::insert(spline_surface, actor);
            connect(actor, SIGNAL(stateChanged(dtkAbstractData*, int)), this, SIGNAL(stateChanged(dtkAbstractData*, int)));
            connect(spline_surface,SIGNAL(samplingChanged()),actor,SLOT(onSamplingChanged()));
            //connect(spline_surface, SIGNAL(touch()), this, SLOT(update()));

            //this->insertFields(spline_surface, actor);

            result.push_back(actor);
        }
        else if(axlDataDynamic *dynamicData = dynamic_cast<axlDataDynamic *>(*data)) {
            axlActorDataDynamic *actor = axlActorDataDynamic::New();
            actor->setInteractor(iren);
            actor->setView(this); // need for the insertion of the composite child
            actor->setRenderer(d->renderer);

            actor->setData(dynamicData);
            d->renderer->AddActor(actor);


            axlAbstractView::insert(dynamicData, actor);
            connect(actor, SIGNAL(stateChanged(dtkAbstractData*, int)), this, SIGNAL(stateChanged(dtkAbstractData*, int)));

            result.push_back(actor);

        }
        else if(axlAbstractDataComposite *composite = dynamic_cast<axlAbstractDataComposite *>(*data)) {
            axlActorComposite *actor = axlActorComposite::New();
            actor->setInteractor(iren);
            actor->setView(this); // need for the insertion of the composite child
            actor->setRenderer(d->renderer);

            actor->setData(composite);
            d->renderer->AddActor(actor);

            axlAbstractView::insert(composite, actor);
            connect(actor, SIGNAL(stateChanged(dtkAbstractData*, int)), this, SIGNAL(stateChanged(dtkAbstractData*, int)));
            connect(composite,SIGNAL(samplingChanged()),actor,SLOT(onSamplingChanged()));

            result.push_back(actor);

        }
        else if(axlVolumeDiscrete *vol = dynamic_cast<axlVolumeDiscrete *>(*data)) {

            axlActorVolumeDiscrete *actor = axlActorVolumeDiscrete::New();
            actor->setInteractor(iren);

            actor->setData(vol);
            actor->setDisplay(true);
            d->renderer->AddActor(actor);

            axlAbstractView::insert(vol, actor);
            connect(actor, SIGNAL(stateChanged(dtkAbstractData*, int)), this, SIGNAL(stateChanged(dtkAbstractData*, int)));

            result.push_back(actor);
        }

        else {

            axlActorMesh *actor = axlActorMesh::New();

            actor->setInteractor(iren);
            actor->setData(*data);

            actor->setDisplay(true);

            d->renderer->AddActor(actor);

            axlAbstractView::insert(*data, actor);
            connect(actor, SIGNAL(stateChanged(dtkAbstractData*, int)), this, SIGNAL(stateChanged(dtkAbstractData*, int)));
            connect(actor, SIGNAL(updated()), this, SLOT(update()));
            result.push_back(actor);

            //this->insertFields(*data, actor);
        }
    }// for each

    d->renderer->ResetCamera();

    iren = NULL;

    dtkWarn()<<"End Time "<<vtkTimerLog::GetCPUTime()<< vtkTimerLog::GetCPUTime() - currentTime;
    //double end = clock();
    //double cpuTime = difftime(end, start)/CLOCKS_PER_SEC;
    //qDebug()<<"axlVtkView cpu time "<<end <<cpuTime;    //d->widget->update() ;
    return result;
}

void axlVtkView::removeSetFields(QList<axlAbstractActorField *> actorSet)
{

    for(int i = 0; i < actorSet.size() ; i++)
    {
        axlAbstractView::removeField(actorSet.at(i)->data());
        if(axlActorFieldSpatial *currentactor = dynamic_cast<axlActorFieldSpatial *>(actorSet.value(i))){

            disconnect(currentactor, SIGNAL(stateChanged(dtkAbstractData*, int)), this, SIGNAL(stateChanged(dtkAbstractData*, int)));
            //currentactor->onRemoved(); // a construire
            if(currentactor->scalarBar()){
                d->renderer->RemoveActor2D(currentactor->scalarBar());
            }
            d->renderer->RemoveActor(currentactor);
            currentactor->setInteractor(NULL);
            currentactor->Delete();

            currentactor = NULL;

        }else if(axlActorFieldParametric *currentactor = dynamic_cast<axlActorFieldParametric *>(actorSet.value(i))){

            disconnect(currentactor, SIGNAL(stateChanged(dtkAbstractData*, int)), this, SIGNAL(stateChanged(dtkAbstractData*, int)));
            //currentactor->onRemoved(); // a construire
            if(currentactor->scalarBar()){
                d->renderer->RemoveActor2D(currentactor->scalarBar());
            }
            d->renderer->RemoveActor(currentactor);
            currentactor->setInteractor(NULL);

            currentactor->Delete();

            currentactor = NULL;

        }else if(axlActorFieldDiscrete *currentactor = dynamic_cast<axlActorFieldDiscrete *>(actorSet.value(i))){

            disconnect(currentactor, SIGNAL(stateChanged(dtkAbstractData*, int)), this, SIGNAL(stateChanged(dtkAbstractData*, int)));
            //currentactor->onRemoved(); // a construire
            if(currentactor->scalarBar()){
                d->renderer->RemoveActor2D(currentactor->scalarBar());
            }
            d->renderer->RemoveActor(currentactor);
            currentactor->setInteractor(NULL);

            currentactor->Delete();

            currentactor = NULL;

        }


    }

    d->renderer->ResetCamera();
    this->update();
}



void axlVtkView::removeSet(QList<axlAbstractActor *>  actorSet)
{

    for(int i = 0; i < actorSet.size() ; i++)

    {

        axlAbstractView::removeData(actorSet.at(i)->data());

        if(axlActor *currentactor = dynamic_cast<axlActor *>(actorSet.value(i)))

        {
            // qDebug()<<currentactor->GetReferenceCount();

            //            qDebug()<<"numnber of reference 0"<<currentactor->GetReferenceCount();

            if(axlActorBSpline *actorBSlpine = dynamic_cast<axlActorBSpline *>(currentactor))
                disconnect(actorBSlpine->data(),SIGNAL(samplingChanged()),actorBSlpine,SLOT(onSamplingChanged()));
            //            qDebug()<<"numnber of reference 1"<<currentactor->GetReferenceCount();


            disconnect(currentactor, SIGNAL(stateChanged(dtkAbstractData*, int)), this, SIGNAL(stateChanged(dtkAbstractData*, int)));
            if(currentactor->parent() == NULL || dynamic_cast<axlActorDataDynamic *>(currentactor)){
                currentactor->onRemoved();
                d->renderer->RemoveActor(currentactor);
                currentactor->setInteractor(NULL);
                //            qDebug()<<"numnber of reference 2"<<currentactor->GetReferenceCount();
                currentactor->Delete();
                //int i = 0;
                //  qDebug()<<currentactor->GetReferenceCount();
                //            qDebug()<<"numnber of reference 3"<<currentactor->GetReferenceCount();
                currentactor = NULL;
            }

        }

        else if(axlActorComposite *currentactor = dynamic_cast<axlActorComposite *>(actorSet.value(i)))

        {

            disconnect(currentactor, SIGNAL(stateChanged(dtkAbstractData*, int)), this, SIGNAL(stateChanged(dtkAbstractData*, int)));
            currentactor->onRemoved();


            if(currentactor->getState() == (axlActorComposite::selection | axlActorComposite::passive))
                d->renderer->RemoveActor(currentactor);

            currentactor->Delete();
            currentactor = NULL;
        }
    }

    d->renderer->ResetCamera();
    this->update();

}

axlAbstractActor *axlVtkView::insert(axlAbstractData *data)
{
    vtkRenderWindowInteractor *iren = d->widget->GetInteractor();
    d->widget->GetInteractor()->SetInteractorStyle(d->style);

    if (axlPoint *point = dynamic_cast<axlPoint *>(data)) {

        axlActorPoint *actor = axlActorPoint::New();

        actor->setInteractor(iren);
        actor->setQVTKWidget(d->widget);
        actor->setData(point);

        // actor->setDisplay(true);

        d->renderer->AddActor(actor);

        axlAbstractView::insert(point, actor);
        connect(actor, SIGNAL(stateChanged(dtkAbstractData*,int)), this, SIGNAL(stateChanged(dtkAbstractData*,int)));
        connect(actor, SIGNAL(updated()), this, SLOT(update()));
        d->renderer->ResetCamera();

        iren = NULL;
        return actor;
    }

    else if (axlCone *cone = dynamic_cast<axlCone *>(data)) {

        axlActorCone *actor = axlActorCone::New();

        actor->setInteractor(iren);
        actor->setQVTKWidget(d->widget);
        actor->setData(cone);

        //actor->setDisplay(true);

        d->renderer->AddActor(actor);

        axlAbstractView::insert(cone, actor);
        connect(actor, SIGNAL(stateChanged(dtkAbstractData*, int)), this, SIGNAL(stateChanged(dtkAbstractData*, int)));
        connect(actor, SIGNAL(updated()), this, SLOT(update()));
        return actor;
    }

    else if (axlCylinder *cylinder = dynamic_cast<axlCylinder *>(data)) {

        axlActorCylinder *actor = axlActorCylinder::New();

        actor->setInteractor(iren);
        actor->setQVTKWidget(d->widget);
        actor->setData(cylinder);

        //actor->setDisplay(true);

        d->renderer->AddActor(actor);
        iren = NULL;


        axlAbstractView::insert(cylinder, actor);
        connect(actor, SIGNAL(stateChanged(dtkAbstractData*, int)), this, SIGNAL(stateChanged(dtkAbstractData*, int)));
        connect(actor, SIGNAL(updated()), this, SLOT(update()));
        return actor;
    }

    else if (axlTorus *torus = dynamic_cast<axlTorus *>(data)) {

        axlActorTorus *actor = axlActorTorus::New();

        actor->setInteractor(iren);
        actor->setQVTKWidget(d->widget);
        actor->setData(torus);

        //actor->setDisplay(true);

        d->renderer->AddActor(actor);
        iren = NULL;


        axlAbstractView::insert(torus, actor);
        connect(actor, SIGNAL(stateChanged(dtkAbstractData*, int)), this, SIGNAL(stateChanged(dtkAbstractData*, int)));
        connect(actor, SIGNAL(updated()), this, SLOT(update()));
        return actor;
    }

    //    else if (axlCircleArc *arc = dynamic_cast<axlCircleArc *>(data)) {

    //        axlActorCircleArc *actor = axlActorCircleArc::New();

    //        actor->setInteractor(iren);
    //        actor->setQVTKWidget(d->widget);
    //        actor->setCircleArc(arc);

    //        //actor->setDisplay(true);

    //        d->renderer->AddActor(actor);

    //        axlAbstractView::insert(arc, actor);
    //        connect(actor, SIGNAL(stateChanged(dtkAbstractData*, int)), this, SIGNAL(stateChanged(dtkAbstractData*, int)));
    //        return actor;
    //    }

    else if (axlEllipsoid *ellipsoid = dynamic_cast<axlEllipsoid *>(data)) {

        axlActorEllipsoid *actor = axlActorEllipsoid::New();

        actor->setInteractor(iren);
        actor->setQVTKWidget(d->widget);
        actor->setData(ellipsoid);

        //actor->setDisplay(true);

        d->renderer->AddActor(actor);
        iren = NULL;

        axlAbstractView::insert(ellipsoid, actor);
        connect(actor, SIGNAL(stateChanged(dtkAbstractData*, int)), this, SIGNAL(stateChanged(dtkAbstractData*, int)));
        connect(actor, SIGNAL(updated()), this, SLOT(update()));
        return actor;
    }


    else if (axlLine *line = dynamic_cast<axlLine *>(data)) {

        axlActorLine *actor = axlActorLine::New();

        actor->setInteractor(iren);
        actor->setQVTKWidget(d->widget);
        actor->setData(line);

        //actor->setDisplay(true);

        d->renderer->AddActor(actor);
        iren = NULL;

        axlAbstractView::insert(line, actor);
        connect(actor, SIGNAL(stateChanged(dtkAbstractData*,int)), this, SIGNAL(stateChanged(dtkAbstractData*,int)));
        connect(actor, SIGNAL(updated()), this, SLOT(update()));
        d->renderer->ResetCamera();

        return actor;
    }
    else if (axlPlane *plane = dynamic_cast<axlPlane *>(data)) {

        axlActorPlane *actor = axlActorPlane::New();

        actor->setInteractor(iren);
        actor->setQVTKWidget(d->widget);
        actor->setRenderer(d->renderer);
        actor->setData(plane);

        actor->setDisplay(true);

        d->renderer->AddActor(actor);
        iren = NULL;

        axlAbstractView::insert(plane, actor);
        connect(actor, SIGNAL(stateChanged(dtkAbstractData*, int)), this, SIGNAL(stateChanged(dtkAbstractData*, int)));
        connect(actor, SIGNAL(updated()), this, SLOT(update()));
        d->renderer->ResetCamera();
    }
    //    else if (axlPointSet *pointSet = dynamic_cast<axlPointSet *>(data)) {

    //        axlActorPointSet *actor = axlActorPointSet::New();

    //        actor->setInteractor(iren);
    //        actor->setPointSet(pointSet);

    //        //actor->setDisplay(true);

    //        d->renderer->AddActor(actor);

    //        axlAbstractView::insert(pointSet, actor);
    //        connect(actor, SIGNAL(stateChanged(dtkAbstractData*,int)), this, SIGNAL(stateChanged(dtkAbstractData*,int)));
    //        d->renderer->ResetCamera();


    //        return actor;
    //    }
    else if (axlSphere *sphere = dynamic_cast<axlSphere *>(data)) {

        axlActorSphere *actor = axlActorSphere::New();

        actor->setInteractor(iren);
        actor->setQVTKWidget(d->widget);
        actor->setData(sphere);

        actor->setDisplay(true);

        d->renderer->AddActor(actor);
        iren = NULL;

        axlAbstractView::insert(sphere, actor);
        connect(actor, SIGNAL(stateChanged(dtkAbstractData*, int)), this, SIGNAL(stateChanged(dtkAbstractData*, int)));
        connect(actor, SIGNAL(updated()), this, SLOT(update()));
        d->renderer->ResetCamera();

        return actor;
    }

    else if (axlAbstractCurveBSpline *spline_curve = dynamic_cast<axlAbstractCurveBSpline *>(data)) {

        axlActorCurveBSpline *actor = axlActorCurveBSpline::New();

        actor->setInteractor(iren);
        actor->setData(spline_curve);

        d->renderer->AddActor(actor);
        iren = NULL;

        axlAbstractView::insert(spline_curve, actor);
        connect(actor, SIGNAL(stateChanged(dtkAbstractData *, int)), this, SIGNAL(stateChanged(dtkAbstractData *, int)));
        connect(spline_curve, SIGNAL(samplingChanged()), actor, SLOT(onSamplingChanged()));
        connect(actor, SIGNAL(updated()), this, SLOT(update()));
        d->renderer->ResetCamera();

        return actor;
    }

    else if(axlAbstractSurfaceBSpline *spline_surface = dynamic_cast<axlAbstractSurfaceBSpline *>(data)) {

        axlActorSurfaceBSpline *actor = axlActorSurfaceBSpline::New();

        actor->setInteractor(iren);
        actor->setData(spline_surface);

        d->renderer->AddActor(actor);
        iren = NULL;

        axlAbstractView::insert(spline_surface, actor);
        connect(actor, SIGNAL(stateChanged(dtkAbstractData*,int)), this, SIGNAL(stateChanged(dtkAbstractData*,int)));
        connect(spline_surface,SIGNAL(samplingChanged()),actor,SLOT(onSamplingChanged()));
        connect(spline_surface, SIGNAL(updated()), this, SLOT(update()));
        connect(actor, SIGNAL(updated()), this, SLOT(update()));
        //this->insertFields(spline_surface, actor);

        d->renderer->ResetCamera();

        return actor;
    }
    else if(axlAbstractVolumeBSpline *spline_volume = dynamic_cast<axlAbstractVolumeBSpline *>(data)) {

        axlActorVolumeBSpline *actor = axlActorVolumeBSpline::New();

        actor->setInteractor(iren);
        actor->setData(spline_volume);

        d->renderer->AddActor(actor);
        iren = NULL;

        axlAbstractView::insert(spline_volume, actor);
        connect(actor, SIGNAL(stateChanged(dtkAbstractData*,int)), this, SIGNAL(stateChanged(dtkAbstractData*,int)));
        connect(spline_volume,SIGNAL(samplingChanged()),actor,SLOT(onSamplingChanged()));
        connect(actor, SIGNAL(updated()), this, SLOT(update()));
        //this->insertFields(spline_volume, actor);


        d->renderer->ResetCamera();

        return actor;
    }
    else if(axlShapeBSpline *spline_surface = dynamic_cast<axlShapeBSpline *>(data)) {

        axlActorShapeBSpline *actor = axlActorShapeBSpline::New();

        actor->setInteractor(iren);
        actor->setData(spline_surface);

        d->renderer->AddActor(actor);
        iren = NULL;

        axlAbstractView::insert(spline_surface, actor);
        connect(actor, SIGNAL(stateChanged(dtkAbstractData*,int)), this, SIGNAL(stateChanged(dtkAbstractData*,int)));
        connect(spline_surface,SIGNAL(samplingChanged()),actor,SLOT(onSamplingChanged()));
        connect(spline_surface, SIGNAL(updated()), this, SLOT(update()));

        //this->insertFields(spline_surface, actor);

        d->renderer->ResetCamera();

        return actor;
    }

    else if (axlDataDynamic *line = dynamic_cast<axlDataDynamic *>(data)) {

        axlActorDataDynamic *actor = axlActorDataDynamic::New();

        actor->setInteractor(iren);
        actor->setQVTKWidget(d->widget);
        actor->setView(this); // need for the insertion of the composite child
        actor->setRenderer(d->renderer);

        actor->setData(line);

        d->renderer->AddActor(actor);
        iren = NULL;

        axlAbstractView::insert(line, actor);
        connect(actor, SIGNAL(stateChanged(dtkAbstractData*,int)), this, SIGNAL(stateChanged(dtkAbstractData*,int)));

        d->renderer->ResetCamera();

        return actor;
    }
    else if(axlAbstractDataComposite *composite = dynamic_cast<axlAbstractDataComposite *>(data))
    {
        axlActorComposite *actor = axlActorComposite::New();

        actor->setInteractor(iren);
        actor->setView(this); // need for the insertion of the composite child
        actor->setRenderer(d->renderer);

        actor->setData(composite);

        d->renderer->AddActor(actor);
        iren = NULL;

        axlAbstractView::insert(composite, actor);
        connect(actor, SIGNAL(stateChanged(dtkAbstractData*,int)), this, SIGNAL(stateChanged(dtkAbstractData*,int)));
        //connect(composite,SIGNAL(samplingChanged()),actor,SLOT(onSamplingChanged()));

        d->renderer->ResetCamera();


        return actor;
    }else if (axlVolumeDiscrete *vol = dynamic_cast<axlVolumeDiscrete *>(data)) {
        axlActorVolumeDiscrete *actor = axlActorVolumeDiscrete::New();
        actor->setInteractor(iren);

        actor->setData(vol);

        actor->setDisplay(true);

        d->renderer->AddActor(actor);
        iren = NULL;

        axlAbstractView::insert(vol, actor);
        connect(actor, SIGNAL(stateChanged(dtkAbstractData*, int)), this, SIGNAL(stateChanged(dtkAbstractData*, int)));
    }
    else
    {
        axlActorMesh *actor = axlActorMesh::New();
        actor->setInteractor(iren);
        actor->setData(data);

        d->renderer->AddActor(actor);
        iren = NULL;

        axlAbstractView::insert(data, actor);
        connect(actor, SIGNAL(stateChanged(dtkAbstractData*, int)), this, SIGNAL(stateChanged(dtkAbstractData*, int)));
        connect(actor, SIGNAL(updated()), this, SLOT(update()));
        //this->insertFields(data, actor);

        d->renderer->ResetCamera();

        return actor;
    }
    iren = NULL;

    return NULL;
}

void axlVtkView::onBackgroundGradientChange(bool gradient){
    if(d->renderer)
        d->renderer->SetGradientBackground(gradient);

    this->update();
}

void axlVtkView::setParallelScale(double parallelScale){
    d->renderer->GetActiveCamera()->SetParallelScale(parallelScale);
}

double axlVtkView::getParallelScale(){
    return d->renderer->GetActiveCamera()->GetParallelScale();
}

bool axlVtkView::getBackgroundGradient(){
        return d->renderer->GetGradientBackground();
}

double *axlVtkView::getWorldCameraPosition(){
    return d->renderer->GetActiveCamera()->GetPosition();
}

bool axlVtkView::getAxesVisibility(){
    return d->axesActor->GetVisibility();
}

bool axlVtkView::getParallelProjection(){
    return d->renderer->GetActiveCamera()->GetParallelProjection();
}

double *axlVtkView::getCameraFocalPoint(){
    return d->renderer->GetActiveCamera()->GetFocalPoint();
}

void axlVtkView::setCameraFocalPoint(double x, double y, double z){
    d->renderer->GetActiveCamera()->SetFocalPoint(x, y, z);
}

double *axlVtkView::getCameraUp(){
    return d->renderer->GetActiveCamera()->GetViewUp();
}

void axlVtkView::setCameraUp(double x, double y, double z){
    d->renderer->GetActiveCamera()->SetViewUp(x, y, z);
}

double axlVtkView::getCameraViewAngle(){
    return d->renderer->GetActiveCamera()->GetViewAngle();
}

void axlVtkView::setCameraViewAngle(double angle){
    d->renderer->GetActiveCamera()->SetViewAngle(angle);
}

double *axlVtkView::getBackgroundColor(){
    return d->renderer->GetBackground();
}

void axlVtkView::setBackgroundColor(double red, double green, double blue)
{
    if(d->renderer)
        d->renderer->SetBackground (red, green, blue);

    this->update();

    //    vtkSmartPointer<vtkImageData> image = vtkSmartPointer<vtkImageData>::New();

    //    unsigned int dim = 20;

    //    image->SetNumberOfScalarComponents(3);
    //    image->SetScalarTypeToUnsignedChar();
    //    image->SetDimensions(dim, dim, 1);

    //    image->AllocateScalars();

    //    for(unsigned int x = 0; x < dim; x++)
    //    {
    //        for(unsigned int y = 0; y < dim; y++)
    //        {
    //            unsigned char* pixel = static_cast<unsigned char*>(image->GetScalarPointer(x,y,0));
    //            if(x < dim/2)
    //            {
    //                pixel[0] = 255;
    //            }
    //            else
    //            {
    //                pixel[0] = 0;
    //            }
    //            pixel[1] = 0;
    //            pixel[2] = 255;
    //        }
    //    }

    //    image->Modified();


    //    vtkSmartPointer<vtkTexture> texture = vtkSmartPointer<vtkTexture>::New();
    //    texture->SetInputConnection(image->GetProducerPort());

    //    d->renderer->SetBackgroundTexture(texture);
    //    d->renderer->TexturedBackgroundOn();


}

void axlVtkView::activeDefaultLigh(bool useDefaultLight)
{
    if(useDefaultLight)
    {
        if(d->lightWidget != NULL)
        {
            d->lightWidget->SetEnabled(0);
            d->lightWidget->Delete();
            d->lightWidget = NULL;
        }
        d->renderer->RemoveAllLights();
        d->lightKit = vtkSmartPointer<vtkLightKit>::New();
        d->lightKit->AddLightsToRenderer(d->renderer);

//        d->widget->update();

    }
    else
    {

        //test with axlLightsWidgets
        d->renderer->RemoveAllLights();
        d->firstLight = vtkSmartPointer<vtkLight>::New();
        d->firstLight->SetAmbientColor(1.0, 1.0, 1.0);
        d->firstLight->SetDiffuseColor(1.0, 1.0, 1.0);
        d->firstLight->SetSpecularColor(1.0, 1.0, 1.0);
        d->firstLight->SetExponent(50);
        d->firstLight->SetIntensity(1);
        // d->firstLight->SetConeAngle(170);
        d->firstLight->SetPosition(0.0, 0.0, 0.0);

        double bounds[6]; d->renderer->ComputeVisiblePropBounds(bounds);
        double center[3];
        center[0] = (bounds[1]+bounds[0])/2;
        center[1] = (bounds[3]+bounds[2])/2;
        center[2] = (bounds[5]+bounds[4])/2;
        d->firstLight->SetFocalPoint(center);
        d->firstLight->SetLightTypeToSceneLight();
        d->firstLight->SetPositional(1);
        d->renderer->AddLight(d->firstLight);
        if(d->lightWidget == NULL)
        {
            d->lightWidget = axlLightsWidget::New();
            d->lightWidget->setView(this);
            d->lightWidget->SetInteractor(d->widget->GetInteractor());
            d->lightWidget->initializePoints();

            d->lightWidget->ptsActors()->InitTraversal();

            for(vtkIdType i = 0; i < d->lightWidget->ptsActors()->GetNumberOfItems(); i++)
            {
                d->renderer->AddActor(d->lightWidget->ptsActors()->GetNextActor());
            }

            d->lightWidget->SetEnabled(1);

        }

//        d->widget->update();


    }

    this->update();

    //    if(useDefaultLight)
    //    {
    //        if(d->lightActor != NULL)
    //        {
    //            d->renderer->RemoveActor(d->lightActor);
    //            //d->lightActor->Delete();
    //            d->lightActor = NULL;
    //        }
    //        d->renderer->RemoveAllLights();
    //        d->lightKit = vtkSmartPointer<vtkLightKit>::New();
    //        d->lightKit->AddLightsToRenderer(d->renderer);

    //        d->widget->update();

    //    }
    //    else
    //    {

    //        //test with axlLightsWidgets
    //        d->renderer->RemoveAllLights();
    //        d->firstLight = vtkSmartPointer<vtkLight>::New();
    //        d->firstLight->SetAmbientColor(1.0, 1.0, 1.0);
    //        d->firstLight->SetDiffuseColor(1.0, 1.0, 1.0);
    //        d->firstLight->SetSpecularColor(1.0, 1.0, 1.0);
    //        d->firstLight->SetExponent(50);
    //        d->firstLight->SetPosition(0.0, 0.0, 0.0);
    //        d->firstLight->SetFocalPoint(d->renderer->GetActiveCamera()->GetFocalPoint());
    //        d->firstLight->SetLightTypeToSceneLight();
    //        d->firstLight->SetPositional(1);
    //        d->renderer->AddLight(d->firstLight);

    //        if(d->lightActor == NULL)
    //        {
    //            d->lightActor = vtkSmartPointer<vtkLightActor>::New();
    //            d->lightActor->SetLight(d->firstLight);
    //            d->renderer->AddActor(d->lightActor);
    //        }



    //        d->widget->update();


    //    }

}

void axlVtkView::setLight(int i, bool hide, double *position, double *ambiant, double *diffuse, double *specular, int exponent)
{
    Q_UNUSED(i); // be used later for select the good light

    d->firstLight->SetAmbientColor(ambiant);
    d->firstLight->SetDiffuseColor(diffuse);
    d->firstLight->SetSpecularColor(specular);
    d->firstLight->SetExponent(exponent);
    d->firstLight->SetPosition(position);

    d->firstLight->Modified();

    d->lightWidget->update();

    d->lightWidget->SetEnabled(!hide);

//    d->widget->update();
    this->update();
}

void axlVtkView::setCameraViewDefaut(void)
{
    d->lastCameraIsDefault = true;

    vtkCamera *camera = d->renderer->GetActiveCamera();
    camera->SetParallelProjection(false);
    camera->SetPosition(d->camera_center);
    camera->SetFocalPoint(d->camera_focal);
    camera->SetViewUp(d->camera_view_up);

    d->renderer->ResetCamera();

    this->update();

}

void axlVtkView::setCameraViewNegativeX(void)
{
    vtkCamera *camera = d->renderer->GetActiveCamera();
    camera->SetParallelProjection(true);

    if(d->lastCameraIsDefault)
    {
        camera->GetPosition(d->camera_center);
        camera->GetFocalPoint(d->camera_focal);
        camera->GetViewUp(d->camera_view_up);
        d->lastCameraIsDefault = false;
    }

    double bounds[6]; d->renderer->ComputeVisiblePropBounds(bounds);

    double side = qAbs(bounds[1]-bounds[0]);

    double center[3];
    center[0] = (bounds[1]+bounds[0])/2;
    center[1] = (bounds[3]+bounds[2])/2;
    center[2] = (bounds[5]+bounds[4])/2;

    camera->SetPosition(center[0]+side, center[1], center[2]);
    camera->SetFocalPoint(center[0], center[1], center[2]);
    camera->SetViewUp(0, 1, 0);

    d->renderer->ResetCamera();

    this->update();
}

void axlVtkView::setCameraViewPositiveX(void)
{
    vtkCamera *camera = d->renderer->GetActiveCamera();
    camera->SetParallelProjection(true);

    if(d->lastCameraIsDefault)
    {
        camera->GetPosition(d->camera_center);
        camera->GetFocalPoint(d->camera_focal);
        camera->GetViewUp(d->camera_view_up);
        d->lastCameraIsDefault = false;
    }

    double bounds[6]; d->renderer->ComputeVisiblePropBounds(bounds);

    double side = qAbs(bounds[1]-bounds[0]);

    double center[3];
    center[0] = (bounds[1]+bounds[0])/2;
    center[1] = (bounds[3]+bounds[2])/2;
    center[2] = (bounds[5]+bounds[4])/2;

    camera->SetPosition(center[0]-side, center[1], center[2]);
    camera->SetFocalPoint(center[0], center[1], center[2]);
    camera->SetViewUp(0, 1, 0);

    d->renderer->ResetCamera();

    this->update();
}

void axlVtkView::setCameraViewNegativeY(void)
{
    vtkCamera *camera = d->renderer->GetActiveCamera();
    camera->SetParallelProjection(true);

    if(d->lastCameraIsDefault)
    {
        camera->GetPosition(d->camera_center);
        camera->GetFocalPoint(d->camera_focal);
        camera->GetViewUp(d->camera_view_up);
        d->lastCameraIsDefault = false;
    }

    double bounds[6]; d->renderer->ComputeVisiblePropBounds(bounds);

    double side = qAbs(bounds[3]-bounds[2]);

    double center[3];
    center[0] = (bounds[1]+bounds[0])/2;
    center[1] = (bounds[3]+bounds[2])/2;
    center[2] = (bounds[5]+bounds[4])/2;

    camera->SetPosition(center[0], center[1]+side, center[2]);
    camera->SetFocalPoint(center[0], center[1], center[2]);
    camera->SetViewUp(0, 0, 1);

    d->renderer->ResetCamera();

    this->update();
}

void axlVtkView::setCameraViewPositiveY(void)
{
    vtkCamera *camera = d->renderer->GetActiveCamera();
    camera->SetParallelProjection(true);

    if(d->lastCameraIsDefault)
    {
        camera->GetPosition(d->camera_center);
        camera->GetFocalPoint(d->camera_focal);
        camera->GetViewUp(d->camera_view_up);
        d->lastCameraIsDefault = false;
    }

    double bounds[6]; d->renderer->ComputeVisiblePropBounds(bounds);

    double side = qAbs(bounds[3]-bounds[2]);

    double center[3];
    center[0] = (bounds[1]+bounds[0])/2;
    center[1] = (bounds[3]+bounds[2])/2;
    center[2] = (bounds[5]+bounds[4])/2;

    camera->SetPosition(center[0], center[1]-side, center[2]);
    camera->SetFocalPoint(center[0], center[1], center[2]);
    camera->SetViewUp(0, 0, 1);

    d->renderer->ResetCamera();

    this->update();
}

void axlVtkView::setCameraViewNegativeZ(void)
{
    vtkCamera *camera = d->renderer->GetActiveCamera();
    camera->SetParallelProjection(true);

    if(d->lastCameraIsDefault)
    {
        camera->GetPosition(d->camera_center);
        camera->GetFocalPoint(d->camera_focal);
        camera->GetViewUp(d->camera_view_up);
        d->lastCameraIsDefault = false;
    }

    double bounds[6]; d->renderer->ComputeVisiblePropBounds(bounds);

    double side = qAbs(bounds[5]-bounds[4]);

    double center[3];
    center[0] = (bounds[1]+bounds[0])/2;
    center[1] = (bounds[3]+bounds[2])/2;
    center[2] = (bounds[5]+bounds[4])/2;

    camera->SetPosition(center[0], center[1], center[2]+side);
    camera->SetFocalPoint(center[0], center[1], center[2]);
    camera->SetViewUp(0, 1, 0);

    d->renderer->ResetCamera();

    this->update();
}

void axlVtkView::setCameraViewPositiveZ(void)
{
    vtkCamera *camera = d->renderer->GetActiveCamera();
    camera->SetParallelProjection(true);

    if(d->lastCameraIsDefault)
    {
        camera->GetPosition(d->camera_center);
        camera->GetFocalPoint(d->camera_focal);
        camera->GetViewUp(d->camera_view_up);
        d->lastCameraIsDefault = false;
    }

    double bounds[6]; d->renderer->ComputeVisiblePropBounds(bounds);

    double side = qAbs(bounds[5]-bounds[4]);

    double center[3];
    center[0] = (bounds[1]+bounds[0])/2;
    center[1] = (bounds[3]+bounds[2])/2;
    center[2] = (bounds[5]+bounds[4])/2;

    camera->SetPosition(center[0], center[1], center[2]-side);
    camera->SetFocalPoint(center[0], center[1], center[2]);
    camera->SetViewUp(0, 1, 0);

    d->renderer->ResetCamera();

    this->update();
}

void axlVtkView::setGrid(bool isGridX, bool isGridY, bool isGridZ)
{

    //axis

    if(!d->isGridX)
    {
        //vtkSmartPointer<vtkPNGReader> imageReaderRedX;
        vtkSmartPointer<vtkPolyData> polyDataX;
        //        vtkSmartPointer<vtkFloatArray> TCoordsX;
        //        vtkSmartPointer<vtkTexture> textureRedX;
        vtkSmartPointer<vtkPolyDataMapper> mapperX;

        // char* fname1 = vtkTestUtilities::ExpandDataFileName(1, new char*(), "Data/ground.png");

        //        imageReaderRedX = vtkPNGReader::New();
        //        imageReaderRedX->SetFileName(fname1);
        //        imageReaderRedX->Update();

        d->planeSourceX = vtkSmartPointer<vtkPlaneSource>::New();

        polyDataX = d->planeSourceX->GetOutput();
        //        TCoordsX = vtkFloatArray::New();
        //        textureRedX =  vtkTexture::New();

        mapperX = vtkSmartPointer<vtkPolyDataMapper>::New();
        d->actorX = vtkSmartPointer<vtkActor>::New();

        double bounds[6]; d->renderer->ComputeVisiblePropBounds(bounds);

        d->planeSourceX->SetPoint1(bounds[1], bounds[2], 0);
        d->planeSourceX->SetPoint2(bounds[0], bounds[3], 0);
        d->planeSourceX->SetOrigin(bounds[0], bounds[2], 0);
        d->planeSourceX->SetResolution(1, 1);
        d->planeSourceX->Update();

        polyDataX->Register(NULL);
        polyDataX->GetPointData()->SetNormals(NULL);

        //        TCoordsX->SetNumberOfComponents(2);
        //        TCoordsX->SetNumberOfTuples(4);
        //        TCoordsX->SetTuple2(0, 0.0, 0.0);
        //        TCoordsX->SetTuple2(1, 0.0, 10.0);
        //        TCoordsX->SetTuple2(2, 10.0, 0.0);
        //        TCoordsX->SetTuple2(3, 10.0, 10.0);
        //        TCoordsX->SetName("MultTCoordsX");

        //        polyDataX->GetPointData()->SetTCoords(TCoordsX);

        // textureRedX->SetInputConnection(imageReaderRedX->GetOutputPort());
        // textureRedX->RepeatOn();

#if (VTK_MAJOR_VERSION <= 5)
        mapperX->SetInput(polyDataX);
#else
        mapperX->SetInputData(polyDataX);
#endif
        d->actorX->SetMapper(mapperX);
        d->actorX->SetUseBounds(false);

        // no multitexturing just show the green texture.
        //d->actorX->SetTexture(textureRedX);
        d->renderer->AddActor(d->actorX);
        d->isGridX = true;
    }

    if(!d->isGridY)
    {
        // vtkSmartPointer<vtkPNGReader> imageReaderRedY;
        vtkSmartPointer<vtkPolyData> polyDataY;
        //        vtkSmartPointer<vtkFloatArray> TCoordsY;
        //        vtkSmartPointer<vtkTexture> textureRedY;
        vtkSmartPointer<vtkPolyDataMapper> mapperY;

        //        char* fname1 = vtkTestUtilities::ExpandDataFileName(1, new char*(), "Data/ground.png");

        //        imageReaderRedY = vtkPNGReader::New();
        //        imageReaderRedY->SetFileName(fname1);
        //        imageReaderRedY->Update();

        d->planeSourceY = vtkSmartPointer<vtkPlaneSource>::New();
        polyDataY = d->planeSourceY->GetOutput();
        //  TCoordsY = vtkFloatArray::New();
        //        textureRedY =  vtkTexture::New();

        mapperY = vtkSmartPointer<vtkPolyDataMapper>::New();
        d->actorY = vtkSmartPointer<vtkActor>::New();

        double bounds[6]; d->renderer->ComputeVisiblePropBounds(bounds);

        d->planeSourceY->SetPoint1(0, bounds[2], bounds[5]);
        d->planeSourceY->SetPoint2(0, bounds[3], bounds[4]);
        d->planeSourceY->SetOrigin(0, bounds[2], bounds[4]);
        d->planeSourceY->SetResolution(1, 1);
        d->planeSourceY->Update();

        polyDataY->Register(NULL);
        polyDataY->GetPointData()->SetNormals(NULL);

        //        TCoordsY->SetNumberOfComponents(2);
        //        TCoordsY->SetNumberOfTuples(4);
        //        TCoordsY->SetTuple2(0, 0.0, 0.0);
        //        TCoordsY->SetTuple2(1, 0.0, 10.0);
        //        TCoordsY->SetTuple2(2, 10.0, 0.0);
        //        TCoordsY->SetTuple2(3, 10.0, 10.0);
        //        TCoordsY->SetName("MultTCoordsY");

        //        polyDataY->GetPointData()->SetTCoords(TCoordsY);

        //        textureRedY->SetInputConnection(imageReaderRedY->GetOutputPort());
        //        textureRedY->RepeatOn();

#if (VTK_MAJOR_VERSION <= 5)
        mapperY->SetInput(polyDataY);
#else
        mapperY->SetInputData(polyDataY);
#endif
        d->actorY->SetMapper(mapperY);
        d->actorY->SetUseBounds(false);

        // no multitexturing just show the green texture.
        //d->actorY->SetTexture(textureRedY);
        d->renderer->AddActor(d->actorY);
        d->isGridY = true;
    }

    if(!d->isGridZ)
    {
        //  vtkSmartPointer<vtkPNGReader> imageReaderRedZ;
        vtkSmartPointer<vtkPolyData> polyDataZ;
        //        vtkSmartPointer<vtkFloatArray> TCoordsZ;
        //        vtkSmartPointer<vtkTexture> textureRedZ;
        vtkSmartPointer<vtkPolyDataMapper> mapperZ;

        //        char* fname1 = vtkTestUtilities::ExpandDataFileName(1, new char*(), "Data/ground.png");

        //        imageReaderRedZ = vtkPNGReader::New();
        //        imageReaderRedZ->SetFileName(fname1);
        //        imageReaderRedZ->Update();

        d->planeSourceZ = vtkSmartPointer<vtkPlaneSource>::New();
        polyDataZ = d->planeSourceZ->GetOutput();
        //        TCoordsZ = vtkFloatArray::New();
        //        textureRedZ =  vtkTexture::New();

        mapperZ = vtkSmartPointer<vtkPolyDataMapper>::New();
        d->actorZ = vtkSmartPointer<vtkActor>::New();

        double bounds[6]; d->renderer->ComputeVisiblePropBounds(bounds);

        d->planeSourceZ->SetPoint1(bounds[1], 0, bounds[4]);
        d->planeSourceZ->SetPoint2(bounds[0], 0, bounds[5]);
        d->planeSourceZ->SetOrigin(bounds[0], 0, bounds[4]);
        d->planeSourceZ->SetResolution(1, 1);
        d->planeSourceZ->Update();

        polyDataZ->Register(NULL);
        polyDataZ->GetPointData()->SetNormals(NULL);

        //        TCoordsZ->SetNumberOfComponents(2);
        //        TCoordsZ->SetNumberOfTuples(4);
        //        TCoordsZ->SetTuple2(0, 0.0, 0.0);
        //        TCoordsZ->SetTuple2(1, 0.0, 10.0);
        //        TCoordsZ->SetTuple2(2, 10.0, 0.0);
        //        TCoordsZ->SetTuple2(3, 10.0, 10.0);
        //        TCoordsZ->SetName("MultTCoordsZ");

        //        polyDataZ->GetPointData()->SetTCoords(TCoordsZ);

        //        textureRedZ->SetInputConnection(imageReaderRedZ->GetOutputPort());
        //        textureRedZ->RepeatOn();

#if (VTK_MAJOR_VERSION <= 5)
        mapperZ->SetInput(polyDataZ);
#else
        mapperZ->SetInputData(polyDataZ);
#endif
        d->actorZ->SetMapper(mapperZ);
        d->actorZ->SetUseBounds(false);

        // no multitexturing just show the green texture.
        //d->actorZ->SetTexture(textureRedZ);
        d->renderer->AddActor(d->actorZ);
        d->isGridZ = true;
    }


    if(d->isGridX)
    {
        if(isGridX)
        {
            double bounds[6]; d->renderer->ComputeVisiblePropBounds(bounds);


            d->planeSourceX->SetPoint1(bounds[1], bounds[2], 0);
            d->planeSourceX->SetPoint2(bounds[0], bounds[3], 0);
            d->planeSourceX->SetOrigin(bounds[0], bounds[2], 0);
            d->planeSourceX->Update();
            d->actorX->SetVisibility(true);
        }
        else
            d->actorX->SetVisibility(false);
    }


    if(d->isGridY)
    {
        if(isGridY)
        {
            double bounds[6]; d->renderer->ComputeVisiblePropBounds(bounds);

            d->planeSourceY->SetPoint1(0, bounds[2], bounds[5]);
            d->planeSourceY->SetPoint2(0, bounds[3], bounds[4]);
            d->planeSourceY->SetOrigin(0, bounds[2], bounds[4]);
            d->planeSourceY->Update();
            d->actorY->SetVisibility(true);
        }
        else
            d->actorY->SetVisibility(false);
    }

    if(d->isGridZ)
    {
        if(isGridZ)
        {
            double bounds[6]; d->renderer->ComputeVisiblePropBounds(bounds);

            d->planeSourceZ->SetPoint1(bounds[1], 0, bounds[4]);
            d->planeSourceZ->SetPoint2(bounds[0], 0, bounds[5]);
            d->planeSourceZ->SetOrigin(bounds[0], 0, bounds[4]);
            d->planeSourceZ->Update();
            d->actorZ->SetVisibility(true);
        }
        else
            d->actorZ->SetVisibility(false);
    }

    this->update();

}

void axlVtkView::onGridResolutionChanged(int resolution)
{
    // if conditions true, we assumed that grid actor and source are already instancied
    if(d->isGridX)
        d->planeSourceX->SetResolution(resolution, resolution);

    if(d->isGridY)
        d->planeSourceY->SetResolution(resolution, resolution);

    if(d->isGridZ)
        d->planeSourceZ->SetResolution(resolution, resolution);

    this->update();

}

void axlVtkView::onShowAxis(bool show)
{
    if(d->axesActor)
        d->axesActor->SetVisibility(show);

    this->update();
}

void axlVtkView::setParallelCamera(bool parallel)
{
    vtkCamera *camera = d->renderer->GetActiveCamera();

    camera->SetParallelProjection(parallel);

    this->update();
}


void axlVtkView::update(void)
{
    d->widget->update();
    d->widget->GetRenderWindow()->Render();
}

QWidget *axlVtkView::widget(void)
{
    //qDebug() << DTK_PRETTY_FUNCTION << (d->widget != NULL);

    return d->widget;
}


// Update field considering data is already created
void axlVtkView::onUpdateActorField(QList<axlAbstractData *> axlDataSet, QString fieldName)
{
    //Q_UNUSED(fieldName);

    foreach(axlAbstractData *axlData, axlDataSet)
    {
        foreach(axlAbstractField *field, axlData->fields())
        {
            axlAbstractActorField *actorField = dynamic_cast<axlAbstractActorField *> (axlAbstractView::actor(field));

            if(!actorField)// new field detected : lets create the actor
            {
                axlAbstractActor *axlActor = axlAbstractView::actor(axlData);

                if(dynamic_cast<axlAbstractFieldSpatial *>(field)){
                    axlAbstractFieldSpatial *spatialField = dynamic_cast<axlAbstractFieldSpatial *>(field);
                    axlActorFieldSpatial *fieldActor = axlActorFieldSpatial::New();
                    fieldActor->setActorField(axlActor);
                    fieldActor->setData(spatialField);
                    fieldActor->setInteractor(d->widget->GetInteractor());
                    axlAbstractView::insert(spatialField, fieldActor);
                    connect(spatialField, SIGNAL(updated()), fieldActor, SLOT(update()));
                    connect(axlData, SIGNAL(modifiedField()),fieldActor, SLOT(update()));
                    d->renderer->AddActor(fieldActor);
                    if(fieldActor->scalarBar()){
                        // Add the scalar bar actor to the scene
                        d->renderer->AddActor2D(fieldActor->scalarBar());
                    }
                    //                    if(field == axlData->fields().at(0)){
                    //                        axlAbstractField::Kind kind = field->kind();
                    //                        if(kind == axlAbstractField::Scalar){
                    //                            fieldActor->displayAsColor();
                    //                        }else if(kind == axlAbstractField::Vector){
                    //                            fieldActor->displayAsGlyph();
                    //                        }
                    //                    }
                }else if(dynamic_cast<axlAbstractFieldParametric *>(field)){
                    axlAbstractFieldParametric *parametricField = dynamic_cast<axlAbstractFieldParametric *>(field);
                    axlActorFieldParametric *fieldActor = axlActorFieldParametric::New();
                    fieldActor->setActorField(axlActor);
                    fieldActor->setData(parametricField);
                    fieldActor->setInteractor(d->widget->GetInteractor());
                    axlAbstractView::insert(parametricField, fieldActor);
                    connect(parametricField, SIGNAL(updated()), fieldActor, SLOT(update()));
                    connect(axlData, SIGNAL(modifiedField()),fieldActor, SLOT(update()));
                    d->renderer->AddActor(fieldActor);
                    if(fieldActor->scalarBar()){
                        // Add the scalar bar actor to the scene
                        d->renderer->AddActor2D(fieldActor->scalarBar());
                    }

                    //                    if(field == axlData->fields().at(0)){
                    //                        axlAbstractField::Kind kind = field->kind();
                    //                        if(kind == axlAbstractField::Scalar){
                    //                            fieldActor->displayAsColor();
                    //                        }else if(kind == axlAbstractField::Vector){
                    //                            fieldActor->displayAsGlyph();
                    //                        }
                    //                    }
                }else{
                    axlActorFieldDiscrete *fieldActor = axlActorFieldDiscrete::New();
                    fieldActor->setActorField(axlActor);
                    fieldActor->setData(field);
                    fieldActor->setInteractor(d->widget->GetInteractor());
                    axlAbstractView::insert(field, fieldActor);
                    connect(field, SIGNAL(updated()), fieldActor, SLOT(update()));
                    connect(fieldActor, SIGNAL(updated()), this, SLOT(update()));
                    connect(axlData, SIGNAL(modifiedField()),fieldActor, SLOT(update()));
                    d->renderer->AddActor(fieldActor);
                    if(fieldActor->scalarBar()){
                        // Add the scalar bar actor to the scene
                        d->renderer->AddActor2D(fieldActor->scalarBar());
                    }
                    //                    if(field == axlData->fields().at(0)){
                    //                        axlAbstractField::Kind kind = field->kind();
                    //                        if(kind == axlAbstractField::Scalar){
                    //                            fieldActor->displayAsColor();
                    //                        }else if(kind == axlAbstractField::Vector){
                    //                            fieldActor->displayAsGlyph();
                    //                        }
                    //                    }
                }
            }
            else{
                actorField->update();
            }
        }
    }
}

///////////PRIVATE

// When creating data, we need to check if it contains fields

void axlVtkView::insertFields(axlAbstractData *data, axlAbstractActor *actor)
{
    foreach(axlAbstractField *field, data->fields())
    {
        axlAbstractActor *axlActor = axlAbstractView::actor(data);

        if(dynamic_cast<axlAbstractFieldSpatial *>(field)){
            axlAbstractFieldSpatial *spatialField = dynamic_cast<axlAbstractFieldSpatial *>(field);
            axlActorFieldSpatial *fieldActor = axlActorFieldSpatial::New();
            fieldActor->setActorField(axlActor);
            fieldActor->setData(spatialField);
            fieldActor->setInteractor(d->widget->GetInteractor());
            axlAbstractView::insert(spatialField, fieldActor);
            connect(spatialField, SIGNAL(updated()), fieldActor, SLOT(update()));
            connect(data, SIGNAL(modifiedField()),fieldActor, SLOT(update()));
            d->renderer->AddActor(fieldActor);
            if(fieldActor->scalarBar()){
                // Add the scalar bar actor to the scene
                d->renderer->AddActor2D(fieldActor->scalarBar());
            }

        }else if(dynamic_cast<axlAbstractFieldParametric *>(field)){
            axlAbstractFieldParametric *parametricField = dynamic_cast<axlAbstractFieldParametric *>(field);
            axlActorFieldParametric *fieldActor = axlActorFieldParametric::New();
            fieldActor->setActorField(axlActor);
            fieldActor->setData(parametricField);
            fieldActor->setInteractor(d->widget->GetInteractor());
            axlAbstractView::insert(parametricField, fieldActor);
            connect(parametricField, SIGNAL(updated()), fieldActor, SLOT(update()));
            connect(data, SIGNAL(modifiedField()),fieldActor, SLOT(update()));
            d->renderer->AddActor(fieldActor);
            if(fieldActor->scalarBar()){
                // Add the scalar bar actor to the scene
                d->renderer->AddActor2D(fieldActor->scalarBar());
            }

        }else{
            axlActorFieldDiscrete *fieldActor = axlActorFieldDiscrete::New();
            fieldActor->setActorField(axlActor);
            fieldActor->setData(field);
            fieldActor->setInteractor(d->widget->GetInteractor());
            axlAbstractView::insert(field, fieldActor);
            connect(field, SIGNAL(updated()), fieldActor, SLOT(update()));
            connect(data, SIGNAL(modifiedField()),fieldActor, SLOT(update()));
            d->renderer->AddActor(fieldActor);
            if(fieldActor->scalarBar()){
                // Add the scalar bar actor to the scene
                d->renderer->AddActor2D(fieldActor->scalarBar());
            }
        }
    }
}

void axlVtkView::updateFieldActor(dtkAbstractData *data)
{
    if(axlAbstractData *axlData = dynamic_cast<axlAbstractData *>(data))
    {
        foreach(axlAbstractField *field, axlData->fields())
        {
            axlActorFieldDiscrete *actorField = dynamic_cast<axlActorFieldDiscrete *> (axlAbstractView::actor(field));

            if(!actorField)// new field detected : lets create the actor
            {
                axlAbstractActor *axlActor = axlAbstractView::actor(data);

                if(dynamic_cast<axlAbstractFieldSpatial *>(field)){
                    qDebug() << Q_FUNC_INFO << field->identifier();
                    axlAbstractFieldSpatial *spatialField = dynamic_cast<axlAbstractFieldSpatial *>(field);
                    axlActorFieldSpatial *fieldActor = axlActorFieldSpatial::New();
                    fieldActor->setActorField(axlActor);
                    fieldActor->setData(spatialField);
                    fieldActor->setInteractor(d->widget->GetInteractor());
                    axlAbstractView::insert(spatialField, fieldActor);
                    connect(spatialField, SIGNAL(updated()), fieldActor, SLOT(update()));
                    connect(data, SIGNAL(modifiedField()),fieldActor, SLOT(update()));
                    d->renderer->AddActor(fieldActor);
                    if(fieldActor->scalarBar()){
                        // Add the scalar bar actor to the scene
                        d->renderer->AddActor2D(fieldActor->scalarBar());
                    }

                }else if(dynamic_cast<axlAbstractFieldParametric *>(field)){
                    axlAbstractFieldParametric *parametricField = dynamic_cast<axlAbstractFieldParametric *>(field);
                    axlActorFieldParametric *fieldActor = axlActorFieldParametric::New();
                    fieldActor->setActorField(axlActor);
                    fieldActor->setData(parametricField);
                    fieldActor->setInteractor(d->widget->GetInteractor());
                    axlAbstractView::insert(parametricField, fieldActor);
                    connect(parametricField, SIGNAL(updated()), fieldActor, SLOT(update()));
                    connect(data, SIGNAL(modifiedField()),fieldActor, SLOT(update()));
                    d->renderer->AddActor(fieldActor);
                    if(fieldActor->scalarBar()){
                        // Add the scalar bar actor to the scene
                        d->renderer->AddActor2D(fieldActor->scalarBar());
                    }

                }else{
                    axlActorFieldDiscrete *fieldActor = axlActorFieldDiscrete::New();
                    fieldActor->setActorField(axlActor);
                    fieldActor->setData(field);
                    fieldActor->setInteractor(d->widget->GetInteractor());
                    axlAbstractView::insert(field, fieldActor);
                    connect(field, SIGNAL(updated()), fieldActor, SLOT(update()));
                    connect(data, SIGNAL(modifiedField()),fieldActor, SLOT(update()));
                    d->renderer->AddActor(fieldActor);
                    if(fieldActor->scalarBar()){
                        // Add the scalar bar actor to the scene
                        d->renderer->AddActor2D(fieldActor->scalarBar());
                    }

                }
            }
        }
    }
    else
        qDebug()<< " data is not an axlAbstractData *";
}


// /////////////////////////////////////////////////////////////////
// Type instanciation
// /////////////////////////////////////////////////////////////////

dtkAbstractView *createVtkView(void)
{
    return new axlVtkView;
}
