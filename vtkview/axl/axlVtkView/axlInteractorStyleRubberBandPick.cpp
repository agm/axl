/* axlInteractorStyleRubberBandPick.h ---
 *
 * Author: Meriadeg Perrinel
 * Copyright (C) 2008 - Meriadeg Perrinel, Inria.
 * Created: Fri Feb 18 17:25:39 2011 (+0100)
 * Version: $Id$
 * Last-Updated: Fri Feb 18 17:28:34 2011 (+0100)
 *           By: Meriadeg Perrinel
 *     Update #: 18
 */

/* Commentary:
 *
 */

/* Change log:
 *
 */



#include "axlInteractorStyleRubberBandPick.h"

#include <vtkVersion.h>
#include <vtkSmartPointer.h>
#include <vtkPointData.h>
#include <vtkIdTypeArray.h>
#include <vtkDataSetSurfaceFilter.h>
#include <vtkRendererCollection.h>
#include <vtkProperty.h>
#include <vtkPlanes.h>
#include <vtkObjectFactory.h>
#include <vtkPolyDataMapper.h>
#include <vtkActor.h>
#include <vtkRenderWindow.h>
#include <vtkRenderer.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkPolyData.h>
#include <vtkPointSource.h>
#include <vtkAreaPicker.h>
#include <vtkExtractGeometry.h>
#include <vtkDataSetMapper.h>
#include <vtkUnstructuredGrid.h>
#include <vtkVertexGlyphFilter.h>
#include <vtkIdFilter.h>
#include <vtkLine.h>
#include <vtkCellArray.h>

#include <vtkFeatureEdges.h>


#include <QDebug>


vtkStandardNewMacro(axlInteractorStyleRubberBandPick);

#define VTKISRBP_ORIENT 0
#define VTKISRBP_SELECT 1

class axlInteractorStyleRubberBandPickPrivate
{
public:
    vtkSmartPointer<vtkPolyData> Points;
    vtkSmartPointer<vtkActor> SelectedActor;
    vtkSmartPointer<vtkDataSetMapper> SelectedMapper;

    QVector<int> ids;
};

void axlInteractorStyleRubberBandPick::OnLeftButtonUp()
{
    // Forward events
    vtkInteractorStyleRubberBandPick::OnLeftButtonUp();

    vtkPlanes* frustum = static_cast<vtkAreaPicker*>(this->GetInteractor()->GetPicker())->GetFrustum();

    vtkSmartPointer<vtkExtractGeometry> extractGeometry =
            vtkSmartPointer<vtkExtractGeometry>::New();
    extractGeometry->SetImplicitFunction(frustum);
#if VTK_MAJOR_VERSION <= 5
    extractGeometry->SetInput(d->Points);
#else
    extractGeometry->SetInputData(d->Points);
#endif
    extractGeometry->Update();

    //Create a polyData with points only.
    vtkSmartPointer<vtkVertexGlyphFilter> glyphFilter =
            vtkSmartPointer<vtkVertexGlyphFilter>::New();
    glyphFilter->SetInputConnection(extractGeometry->GetOutputPort());
    glyphFilter->Update();

    //    //Create a polyData with edges only.
    //    vtkSmartPointer<vtkExtractEdges> glyphFilter =
    //            vtkSmartPointer<vtkExtractEdges>::New();
    //    glyphFilter->SetInputConnection(extractGeometry->GetOutputPort());
    //    glyphFilter->Update();

    vtkPolyData* selected = glyphFilter->GetOutput();
    std::cout << "Selected " << selected->GetNumberOfPoints() << " points." << std::endl;
    std::cout << "Selected " << selected->GetNumberOfCells() << " cells." << std::endl;
#if VTK_MAJOR_VERSION <= 5
    d->SelectedMapper->SetInput(selected);
#else
    d->SelectedMapper->SetInputData(selected);
#endif
    d->SelectedMapper->ScalarVisibilityOff();

    d->ids.clear();
    vtkIdTypeArray* ids = vtkIdTypeArray::SafeDownCast(selected->GetPointData()->GetArray("OriginalIds"));
    if(ids){
        for(vtkIdType i = 0; i < ids->GetNumberOfTuples(); i++)
        {
            std::cout << "Id " << i << " : " << ids->GetValue(i) << std::endl;
            d->ids.append(ids->GetValue(i));
        }
    }

    d->SelectedActor->GetProperty()->SetColor(1.0, 0.0, 0.0); //(R,G,B)
    d->SelectedActor->GetProperty()->SetPointSize(3);

    this->CurrentRenderer->AddActor(d->SelectedActor);

    this->GetInteractor()->GetRenderWindow()->Render();
    this->HighlightProp(NULL);

    emit IdsSelectedChanged();
}

void axlInteractorStyleRubberBandPick::SetPolyData(vtkSmartPointer<vtkPolyData> points){

    vtkSmartPointer<vtkFeatureEdges> featureEdges =
            vtkSmartPointer<vtkFeatureEdges>::New();
#if (VTK_MAJOR_VERSION <= 5)
    featureEdges->SetInput(points);
#else
    featureEdges->SetInputData(points);
#endif
    featureEdges->BoundaryEdgesOn();
    featureEdges->FeatureEdgesOff();
    featureEdges->ManifoldEdgesOff();
    featureEdges->NonManifoldEdgesOff();
    featureEdges->Update();

//    vtkSmartPointer<vtkPolyDataMapper> edgeMapper =
//            vtkSmartPointer<vtkPolyDataMapper>::New();
//    edgeMapper->SetInputConnection(featureEdges->GetOutputPort());
//    vtkSmartPointer<vtkActor> edgeActor =
//            vtkSmartPointer<vtkActor>::New();
//    edgeActor->SetMapper(edgeMapper);

    vtkSmartPointer<vtkPolyData> poly = featureEdges->GetOutput();

    d->Points = poly;
}

QVector<int> axlInteractorStyleRubberBandPick::ids(void){
    return d->ids;
}

axlInteractorStyleRubberBandPick::axlInteractorStyleRubberBandPick(QObject *parent) : QObject(parent), d(new axlInteractorStyleRubberBandPickPrivate)
{
    d->SelectedMapper = vtkSmartPointer<vtkDataSetMapper>::New();
    d->SelectedActor = vtkSmartPointer<vtkActor>::New();
    d->SelectedActor->SetMapper(d->SelectedMapper);
}

axlInteractorStyleRubberBandPick::~axlInteractorStyleRubberBandPick()
{
    delete d;
    d = NULL;
}
