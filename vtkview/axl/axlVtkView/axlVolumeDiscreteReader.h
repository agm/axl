/* axlVolumeDiscreteReader.h ---
 *
 * Author: Anais Ducoffe
 * Copyright (C) 2013 - Anais Ducoffe, Inria.
 */

/* Commentary:
 *
 */

/* Change log:
 *
 */

#ifndef AXLVOLUMEDISCRETEREADER_H
#define AXLVOLUMEDISCRETEREADER_H

#include <axlCore/axlAbstractDataReader.h>

#include <vtkAssembly.h>
#include <vtkSmartPointer.h>
#include <vtkVersion.h>
#include <vtkImageData.h>
#include "axlVtkViewPluginExport.h"

#include <axlCore/axlAbstractVolumeDiscrete.h>

// ///////////////////////////////////////////////////////////////////
// axlVolumeDiscreteReader declaration
// ///////////////////////////////////////////////////////////////////

class AXLVTKVIEWPLUGIN_EXPORT axlVolumeDiscreteReader : public axlAbstractDataReader
{
    Q_OBJECT

public:
    axlVolumeDiscreteReader(void);

public:
    ~axlVolumeDiscreteReader(void);


public:
    QString identifier(void) const;
    QString description(void) const;
    QStringList handled(void) const;

    static bool registered(void);

public:
    bool accept(const QDomNode& node);
    bool reject(const QDomNode& node);

    axlAbstractData *read(const QDomNode& node);

    axlAbstractData *dataByReader(axlAbstractDataReader *axl_reader, const QDomNode& node);
};

// /////////////////////////////////////////////////////////////////
// Instanciation function
// /////////////////////////////////////////////////////////////////

AXLVTKVIEWPLUGIN_EXPORT dtkAbstractDataReader *createaxlVolumeDiscreteReader(void);

#endif // AXLVOLUMEDISCRETEREADER_H
