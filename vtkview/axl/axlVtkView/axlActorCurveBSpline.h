/* axlActorCurveBSpline.h ---
 *
 * Author: Meriadeg Perrinel
 * Copyright (C) 2008 - Meriadeg Perrinel, Inria.
 * Created: Fri Dec 17 11:01:52 2010 (+0100)
 * Version: $Id$
 * Last-Updated: Mon Dec 17 13:58:31 2012 (+0100)
 *           By: Julien Wintz
 *     Update #: 21
 */

/* Commentary:
 *
 */

/* Change log:
 *
 */

#ifndef AXLACTORCURVEBSPLINE_H
#define AXLACTORCURVEBSPLINE_H

#include "axlActorBSpline.h"

#include "axlVtkViewPluginExport.h"

#include <vtkVersion.h>

class axlAbstractCurveBSpline;
class axlActorCurveBSplinePrivate;

class AXLVTKVIEWPLUGIN_EXPORT axlActorCurveBSpline : public axlActorBSpline
{
public:
#if (VTK_MAJOR_VERSION <= 5)
    vtkTypeRevisionMacro(axlActorCurveBSpline, vtkAssembly);
#endif

    static axlActorCurveBSpline *New(void);

public:
    dtkAbstractData *data(void);

public:
    virtual void setData(dtkAbstractData *spline_curve1);

    void meshProcess(void);
    void pointsUpdate(void);
    void polyDataUpdate(void);
    vtkPolyData *getCurveMapperInput(void);

public slots:
    virtual void onSamplingChanged(void);
    virtual void onTubeFilterRadiusChanged(double radius);
    void onUpdateGeometry(void);


protected:
     axlActorCurveBSpline(void);
    ~axlActorCurveBSpline(void);

private:
    axlActorCurveBSpline(const axlActorCurveBSpline&); // Not implemented.
        void operator = (const axlActorCurveBSpline&); // Not implemented.

private:
    axlActorCurveBSplinePrivate *d;
};

axlAbstractActor *createAxlActorCurveBSpline(void);

#endif
