Sphere
======
A sphere is defined by a center (axlPoint) and a radius (double).

Type: ``axlSphere``

Format:

.. literalinclude:: ../../../data/sphere.axl 
   :lines: 2-5
   :language: xml
	      
.. image:: ../img/sphere.png
   :height: 100px
   :align: center
