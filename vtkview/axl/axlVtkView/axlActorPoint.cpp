/* axlActorPoint.cpp ---
 *
 * Author: Meriadeg Perrinel
 * Copyright (C) 2008 - Meriadeg Perrinel, Inria.
 * Created: Fri Dec 17 10:59:21 2010 (+0100)
 * Version: $Id$
 * Last-Updated: Mon Dec 17 15:02:23 2012 (+0100)
 *           By: Julien Wintz
 *     Update #: 30
 */

/* Commentary:
 *
 */

/* Change log:
 *
 */
#include "axlActorPoint.h"

#include "axlActorComposite.h"
#include "axlInteractorStyleSwitch.h"

#include <axlCore/axlPoint.h>

#include <QVTKOpenGLWidget.h>

#include <vtkActor.h>
#include <vtkAssemblyPath.h>
//#include <vtkCellPicker.h>
#include <vtkCommand.h>
#include <vtkGlyph3D.h>
#include <vtkInteractorStyleSwitch.h>
#include <vtkLight.h>
#include <vtkLightCollection.h>
#include <vtkObjectFactory.h>
#include <vtkPoints.h>
#include <vtkPointWidget.h>
#include <vtkPolyData.h>
#include <vtkPolyDataMapper.h>
#include <vtkProperty.h>
#include <vtkRenderer.h>
#include <vtkRendererCollection.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkSmartPointer.h>
#include <vtkSphereSource.h>
#include <vtkTransform.h>


class axlActorPointObserver : public vtkCommand
{
public:
    axlInteractorStyleSwitch *axlInteractorStyle;
//    vtkCellPicker *axlPointPicker;
    axlPoint *observerData_point;
    axlActorPoint *observerDataAssembly;
    vtkSphereSource *observerData_sphereSource;
    
    vtkPointWidget *pointWidget = nullptr;

public:
    static axlActorPointObserver *New(void)
    {
        return new axlActorPointObserver;
    }

    virtual void Execute(vtkObject *caller, unsigned long event, void *)
    {
        vtkRenderWindowInteractor *interactor = observerDataAssembly->getInteractor();            //interactor->Render();

        if(event == vtkCommand::InteractionEvent) {
            observerData_sphereSource->Update();
            observerData_sphereSource->Modified();
            interactor->Render();

            if (!pointWidget)
                return;

            if(caller == pointWidget) {
                observerData_point->setCoordinates(pointWidget->GetPosition()[0], pointWidget->GetPosition()[1], pointWidget->GetPosition()[2]);
                observerData_point->touchGeometry();
                observerDataAssembly->onUpdateGeometry();
            }
        }
    }
};

// /////////////////////////////////////////////////////////////////
// axlActorPointPrivate
// /////////////////////////////////////////////////////////////////

class axlActorPointPrivate
{
public:
    axlPoint *point;
    vtkSphereSource *sphereSource;
    vtkPointWidget *pointWidget;
    axlActorPointObserver *pointObserver;
    QVTKOpenGLWidget *widget;
//    vtkCellPicker *axlPointPicker ;
};

#if (VTK_MAJOR_VERSION <= 5)
vtkCxxRevisionMacro(axlActorPoint, "$Revision: 0.0.1 $");
#endif

vtkStandardNewMacro(axlActorPoint);

dtkAbstractData *axlActorPoint::data(void)
{
    return d->point;
}

vtkSphereSource *axlActorPoint::sphere(void)
{
    return d->sphereSource;
}

void axlActorPoint::setQVTKWidget(QVTKOpenGLWidget *widget)
{
    d->widget = widget;
}


void axlActorPoint::setData(dtkAbstractData *point1)
{
    axlPoint *point = dynamic_cast<axlPoint *>(point1);

    if(point)
    {
        d->point = point;
        this->setMapper(vtkSmartPointer<vtkPolyDataMapper>::New());
        this->setActor(vtkSmartPointer<vtkActor>::New());

        d->sphereSource = vtkSphereSource::New();
        d->sphereSource->SetPhiResolution(20);
        d->sphereSource->SetThetaResolution(20);

        // connection of data to actor
#if (VTK_MAJOR_VERSION <= 5)
        this->getMapper()->SetInput(d->sphereSource->GetOutput());
#else
        this->getMapper()->SetInputData(d->sphereSource->GetOutput());
#endif
        this->getActor()->SetMapper(this->getMapper());

        this->AddPart(this->getActor());

        if(!d->pointObserver)
        {
//            d->axlPointPicker = vtkCellPicker::New();
            d->pointObserver = axlActorPointObserver::New() ;
            this->getInteractor()->AddObserver(vtkCommand::InteractionEvent, d->pointObserver);
            this->getInteractor()->AddObserver(vtkCommand::MouseMoveEvent, d->pointObserver);

            d->pointObserver->observerDataAssembly = this;
//            d->pointObserver->axlPointPicker = d->axlPointPicker;
            d->pointObserver->observerData_sphereSource = d->sphereSource;
            d->pointObserver->observerData_point = d->point;
            d->pointObserver->axlInteractorStyle = dynamic_cast<axlInteractorStyleSwitch *> (this->getInteractor()->GetInteractorStyle());
        }

        QColor color = d->point->color();
        this->getActor()->GetProperty()->SetColor(color.redF(), color.greenF(), color.blueF());

        QString shader = d->point->shader();
        if(!shader.isEmpty())
            this->setShader(shader);

        this->setOpacity(d->point->opacity());

        this->onUpdateGeometry();
        connect(d->point, SIGNAL(modifiedGeometry()), this, SLOT(onUpdateGeometry()));
        connect(d->point, SIGNAL(modifiedProperty()), this, SLOT(onUpdateProperty()));
    }
    else
        qDebug()<< "no axlPoint available";
}

void axlActorPoint::setDisplay(bool display)
{
    axlActor::setDisplay(display);

    if (display && d->pointWidget) {
        this->showPointWidget(true);
    }

    if (!display && d->pointWidget) {
        this->showPointWidget(false);
    }
}

void axlActorPoint::showPointWidget(bool show)
{
    if (!d->pointWidget) {
        qDebug() << "No tet actor computed for this axlActorPoint.";
        return;
    }

    if (show) {
        d->pointWidget->SetEnabled(1);
    }

    if (!show) {
        d->pointWidget->SetEnabled(0);
    }
}

void axlActorPoint::setPointWidget(bool pointWidget)
{
    if (pointWidget) {
        if (!d->pointWidget) {
            //widget drawing
            d->pointWidget = vtkPointWidget::New();
            d->pointWidget->SetInteractor(this->getInteractor());
            d->pointWidget->SetProp3D(this->getActor());
            d->pointWidget->PlaceWidget();
            d->pointWidget->AllOff();
            d->pointWidget->SetTranslationMode(1);

            d->pointWidget->SetPosition(d->sphereSource->GetCenter());

        }

        if(d->pointObserver) {
            d->pointWidget->AddObserver(vtkCommand::InteractionEvent, d->pointObserver);
            d->pointObserver->pointWidget = d->pointWidget;
        }

        // there is always the controlPoints there
        d->pointWidget->SetEnabled(true);
    }
    else {// (!pointWidget)
        if (this->getActor()) {
            // this->getMapper()->SetInput(this->getPolyData());

            if(d->pointWidget) {
                d->pointWidget->RemoveAllObservers();
                d->pointWidget->SetEnabled(false);
                d->pointWidget->Delete(); // warning not sure
                d->pointWidget = NULL;
            }
        }
    }
    if (d->pointObserver)
        d->pointObserver->pointWidget = d->pointWidget;
}



bool axlActorPoint::isShowPointWidget(void)
{

    if(!d->pointWidget) {
        qDebug() << "No tet actor computed for this axlActorBSpline.";
        return false;
    }

    return  d->pointWidget->GetEnabled();
}


void axlActorPoint::setMode(int state)
{
    this->onModeChanged(state);

    emit stateChanged(this->data(), state);
}

void axlActorPoint::onModeChanged(int state)
{
    if(state == 0)
    {
        this->setState(axlActor::passive);
        this->getActor()->SetPickable(1);

        if(axlAbstractData *data = dynamic_cast<axlAbstractData *>(this->data()))
        {
            QColor color = data->color();
            this->getActor()->GetProperty()->SetColor(color.redF(), color.greenF(), color.blueF());
        }
        this->setPointWidget(false);


    }
    else if(state == 1)
    {

        this->setState(axlActor::selection);
        this->setPointWidget(false);
        this->getActor()->SetPickable(1);
        vtkProperty *prop = this->getActor()->GetProperty();

        if(axlAbstractData *data = dynamic_cast<axlAbstractData *>(this->data()))
        {
            QColor color = data->color();
            qreal *h = new qreal(0.0);
            qreal *s = new qreal(0.0);
            qreal *l = new qreal(0.0);
            color.getHslF(h, s, l);
            color.setHslF(*h, *s, *l + (1.0 - *l) / 2.0);
            prop->SetColor(color.redF(), color.greenF(), color.blueF());
            delete l;
            delete s;
            delete h;
        }

    }
    else if(state == 2)
    {
        this->setState(axlActor::edition);
        this->setPointWidget(true);
        this->getActor()->SetPickable(0);
        vtkProperty *prop = this->getActor()->GetProperty();
        if(axlAbstractData *data = dynamic_cast<axlAbstractData *>(this->data()))
        {
            QColor color = data->color();
            qreal *h = new qreal(0.0);
            qreal *s = new qreal(0.0);
            qreal *l = new qreal(0.0);
            color.getHslF(h, s, l);
            color.setHslF(*h, *s, *l + (1.0 - *l)/2.0);
            prop->SetColor(color.redF(), color.greenF(), color.blueF());
            delete l;
            delete s;
            delete h;
        }
    }

    this->Modified();
}


void axlActorPoint::onRemoved()
{
    // if on edit mode, change to selection mode (for stability)
    if (this->getState() == 2)
        setMode(1);
    
    //remove point specificity
    if(d->pointObserver)
    {
        this->getInteractor()->RemoveObservers(vtkCommand::InteractionEvent, d->pointObserver);
        this->getInteractor()->RemoveObservers(vtkCommand::MouseMoveEvent, d->pointObserver);
        d->pointObserver->observerDataAssembly = NULL;
//        d->pointObserver->axlPointPicker = NULL;
        d->pointObserver->observerData_sphereSource = NULL;
        d->pointObserver->observerData_point = NULL;
        d->pointObserver->axlInteractorStyle = NULL;
        d->pointObserver->Delete();
        d->pointObserver = NULL;

//        d->axlPointPicker->Delete();
//        d->axlPointPicker = NULL;

    }
    if(d->sphereSource)
    {
        d->sphereSource->Delete();
        d->sphereSource = NULL;
    }
    if(d->widget)
    {
        d->widget = NULL;
    }
    if(d->point)
    {
        d->point = NULL;
    }
    if(d->pointWidget)
    {
        this->setPointWidget(false);
        d->pointWidget = NULL;
    }

    //remove actor specificity
    this->RemoveAllObservers();
    this->RemovePart(this->getActor());
    this->getActor()->RemoveAllObservers();

    if(axlActorComposite *actorComposite = dynamic_cast<axlActorComposite *>(this->parent()) )
        actorComposite->removeActorReference(this);
}

void axlActorPoint::onUpdateGeometry(void)
{
    d->sphereSource->SetCenter(d->point->coordinates());
    d->sphereSource->SetRadius(d->point->size());
    d->sphereSource->Modified();
    d->sphereSource->Update();

    if(d->pointWidget){
        d->pointWidget->SetPosition(d->sphereSource->GetCenter());
    }

    if(!d->point->fields().isEmpty())
        d->point->touchField();

    if(d->point->updateView())
        emit updated();
}

void axlActorPoint::setSize(double size){

    d->sphereSource->SetRadius(d->point->size());

    if(!d->point->fields().isEmpty())
        d->point->touchField();
}


axlActorPoint::axlActorPoint(void) : axlActor(), d(new axlActorPointPrivate)
{
    d->point = NULL;
    d->pointWidget = NULL;
//    d->axlPointPicker = NULL;
    d->pointObserver = NULL;
    d->sphereSource =NULL;
    d->widget = NULL;
}

axlActorPoint::~axlActorPoint(void)
{
    //disconnect(d->point, SIGNAL(modified()), this, SLOT(onUpdateGeometry()));
    delete d;

    d = NULL;
}


//// /////////////////////////////////////////////////////////////////
//// Type instanciation
//// /////////////////////////////////////////////////////////////////

axlAbstractActor *createAxlActorPoint(void)
{
    return axlActorPoint::New();
}
